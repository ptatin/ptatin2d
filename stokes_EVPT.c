//
//  stokes_EVPT.c
//  
//
//  Created by le pourhiet laetitia on 8/25/17.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include <stdio.h>
#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"
#include "rheology.h"
#include "stokes_VPT.h"
#include "stokes_EVPT.h"
#include "element_type_Q1.h"

typedef enum { YTYPE_NONE=0, YTYPE_MISES=1, YTYPE_DP=2, YTYPE_TENSILE_FAILURE=3 } YieldTypeDefinition;

static inline void ComputeStressIsotropic2d(PetscReal eta,double D[2][2],double T[2][2])
{
	
}
static inline void ComputeStrainRate2d(double nx[],double ny[],double D[2][2])
{
	
	
}
static inline void ComputeDeformationGradient2d(double nx[],double ny[],double L[2][2])
{
	
	
}
static inline void ComputeSecondInvariant2d(double A[2][2],double *A2)
{
	
	
}
static inline void ComputeAverageTrace2d(double A[2][2],double *A2)
{
	
	
}



#undef __FUNCT__
#define __FUNCT__ "StokesEvaluateRheologyNonLinearitiesMarkers_EVPT"
PetscErrorCode StokesEvaluateRheologyNonLinearitiesMarkers_EVPT(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[])
{	
	PetscErrorCode ierr;
	int         pidx,n_mp_points;
	DataBucket  db;
	DataField   PField_std, PField_stokes, PField_thermal, PField_stokespl, PField_stokesmelt, PField_stokeselas;
	PetscScalar GNIu[2][Q2_NODES_PER_EL_2D],NIp[P_BASIS_FUNCTIONS];
	PetscScalar NIu[U_BASIS_FUNCTIONS];
	PetscScalar NIT[NODES_PER_EL_Q1_2D];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen_u,nen_p,nen_T,e,i,j,k;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	const PetscInt *elnidx_T;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	PetscScalar elT[NODES_PER_EL_Q1_2D];
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	PetscLogDouble t0,t1;
	PetscScalar min_eta,max_eta,min_eta_g,max_eta_g;
	PetscScalar min_rho,max_rho,min_rho_g,max_rho_g;
	PetscInt npoints_yielded_g, npoints_yielded_tens_g,npoints_yielded,npoints_yielded_tens;
	
	PetscScalar eta_lower_cutoff_global_mp,eta_upper_cutoff_global_mp;
	
	BTruth plastic = PETSC_FALSE;
	BTruth melt    = PETSC_FALSE;
    BTruth elastic  = PETSC_FALSE;
	RheologyConstants *rheology;
	
	PetscBool         active_thermal_energy = PETSC_FALSE;    
	PhysCompEnergyCtx phys_energy;
	Vec               T_vec,T_local_vec;
	DM                daT;
	PetscScalar       *LA_T;
	pTatinUnits *units;	
	
	PetscFunctionBegin;
	
	units   = &user->units;
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	/////////////////////* get physics */////////////////
	
	/* stokes */
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
	
	db = user->db;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));
	
	DataBucketQueryDataFieldByName(db,MPntPStokesPl_classname,&plastic);
	npoints_yielded = 0;    
	if (plastic){
		DataBucketGetDataFieldByName(db,MPntPStokesPl_classname,&PField_stokespl);    
		DataFieldGetAccess(PField_stokespl);
		DataFieldVerifyAccess(PField_stokespl,sizeof(MPntPStokesPl));
	} 
	
	DataBucketQueryDataFieldByName(db,MPntPStokesMelt_classname,&melt);
    
	if (melt){
		DataBucketGetDataFieldByName(db,MPntPStokesMelt_classname,&PField_stokesmelt);    
		DataFieldGetAccess(PField_stokesmelt);
		DataFieldVerifyAccess(PField_stokesmelt,sizeof(MPntPStokesMelt));
	} 

    	DataBucketQueryDataFieldByName(db,MPntPStokesElas_classname,&elastic);
    if (elastic){
        DataBucketGetDataFieldByName(db,MPntPStokesElas_classname,&PField_stokeselas);
        DataFieldGetAccess(PField_stokeselas);
        DataFieldVerifyAccess(PField_stokeselas,sizeof(MPntPStokesElas));
    } 
    

    
    /* energy */
	active_thermal_energy = PETSC_FALSE;
	ierr = pTatinPhysCompActivated(user,PhysComp_Energy,&active_thermal_energy);CHKERRQ(ierr);
	if (active_thermal_energy) {
		phys_energy = user->phys_energy;
		T_vec       = phys_energy->T;
		daT         = phys_energy->daT;
		
		ierr = DMGetLocalVector(daT,&T_local_vec);CHKERRQ(ierr);		
		ierr = DMGlobalToLocalBegin(daT,T_vec,INSERT_VALUES,T_local_vec);CHKERRQ(ierr);
		ierr = DMGlobalToLocalEnd(  daT,T_vec,INSERT_VALUES,T_local_vec);CHKERRQ(ierr);
		
		ierr = VecGetArray(T_local_vec,&LA_T);CHKERRQ(ierr);
		ierr = DMDAGetElementsQ1(daT,&nel,&nen_T,&elnidx_T);CHKERRQ(ierr);
		DataBucketGetDataFieldByName(db,MPntPThermal_classname,&PField_thermal);
		DataFieldGetAccess(PField_thermal);
		DataFieldVerifyAccess(PField_thermal,sizeof(MPntPThermal));
	}
	/* new piece of physics to be hook here following energy exemple*/
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	min_eta = 1.0e100;
	max_eta = 1.0e-100;
	min_rho = 1.0e100;
	max_rho = 1.0e-100;
	
	PetscGetTime(&t0);
	rheology = &user->rheology_constants;
	
	UnitsApplyInverseScaling(units->si_viscosity,rheology->eta_upper_cutoff_global,&eta_upper_cutoff_global_mp);
	
	UnitsApplyInverseScaling(units->si_viscosity,rheology->eta_lower_cutoff_global,&eta_lower_cutoff_global_mp);
	/*
	 In this file, all properties marked with mp are scaled, other properties are in SI units.
	 */
	/* marker loop */
	
	for (pidx=0; pidx<n_mp_points; pidx++) {
		MPntStd       *material_point;
		MPntPStokes   *mpprop_stokes;
		MPntPStokesPl *mpprop_stokespl;
	    MPntPStokesMelt *mpprop_stokesmelt;
        MPntPStokesElas *mpprop_stokeselas;
		char          is_yielding, yield_type;        
		MPntPThermal  *mpprop_thermal;
		double        *position, *xip;
		int           wil,phase_mp;
		PetscScalar J[2][2], iJ[2][2];
		PetscScalar J_p,ojp;
		PetscScalar  d1,exx_mp,eyy_mp,exy_mp,eta_mp,strainrateinv_mp;
		PetscScalar  sxx_mp,syy_mp,sxy_mp,u_mp,v_mp,trace_mp,pressure_mp;
		PetscScalar T_mp,rho_mp,rho;
		PetscScalar T_eq, P_eq; 
		PetscScalar alpha_mp;
		
		DataFieldAccessPoint(PField_std,    pidx,(void**)&material_point);
		DataFieldAccessPoint(PField_stokes, pidx,(void**)&mpprop_stokes);
        if (elastic) { DataFieldAccessPoint(PField_stokeselas,pidx,(void**)&mpprop_stokeselas); }
		if (plastic) { DataFieldAccessPoint(PField_stokespl,pidx,(void**)&mpprop_stokespl); }
		if (melt) { DataFieldAccessPoint(PField_stokesmelt,pidx,(void**)&mpprop_stokesmelt); }		
		MPntStdGetField_local_element_index(material_point,&wil);
		MPntStdGetField_global_coord(material_point,&position);
		MPntStdGetField_local_coord(material_point,&xip);
		MPntStdGetField_phase_index(material_point,&phase_mp);
		
		e = wil;
		
		/* get nodal properties for element e */
		/* coordinates*/
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		/* velocity*/
		ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],u);CHKERRQ(ierr);
		
		PTatinConstructNI_Q2_2D(xip,NIu);
		PTatinConstructGNI_Q2_2D(xip,GNIu);
		
		
		/* get element velocity/coordinates in component form */
		for (i=0; i<Q2_NODES_PER_EL_2D; i++) {
			ux[i] = elu[2*i  ];
			uy[i] = elu[2*i+1];
			
			xc[i] = elcoords[2*i  ];
			yc[i] = elcoords[2*i+1];
		}
		
		/* coord transformation */
		for (i=0; i<2; i++) {
			for (j=0; j<2; j++) { J[i][j] = 0.0; }
		}
		for (k=0; k<Q2_NODES_PER_EL_2D; k++) {
			J[0][0] += GNIu[0][k] * xc[k] ;
			J[0][1] += GNIu[0][k] * yc[k] ;
			
			J[1][0] += GNIu[1][k] * xc[k] ;
			J[1][1] += GNIu[1][k] * yc[k] ;
		}
		J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
		ojp = 1.0/J_p;
		iJ[0][0] =  J[1][1]*ojp;
		iJ[0][1] = -J[0][1]*ojp;
		iJ[1][0] = -J[1][0]*ojp;
		iJ[1][1] =  J[0][0]*ojp;
		
		/* global derivs at each quadrature point */
		for (k=0; k<Q2_NODES_PER_EL_2D; k++) {
			nx[k] = iJ[0][0]*GNIu[0][k] + iJ[0][1]*GNIu[1][k];
			ny[k] = iJ[1][0]*GNIu[0][k] + iJ[1][1]*GNIu[1][k];
		}
		
		
		
		
		
		
		/* strain rate (e= B u) at gp */
		exx_mp = eyy_mp = exy_mp = 0.0;
		for (k=0; k<U_BASIS_FUNCTIONS; k++) {
			exx_mp += nx[k] * ux[k];
			eyy_mp += ny[k] * uy[k];
			exy_mp += ny[k] * ux[k] + nx[k] * uy[k];
		}
		exy_mp = 0.5 * exy_mp;
		strainrateinv_mp = sqrt(0.25*(exx_mp-eyy_mp)*(exx_mp-eyy_mp)+exy_mp*exy_mp);
		/* viscous stress predictor */ 
		//MPntPStokesGetField_eta_effective(mpprop_stokes,&eta_mp);
		//d1 = 2.0 * eta_mp;
		//sxx_mp = d1 * exx_mp;
		//syy_mp = d1 * eyy_mp;
		/* if the strainrate is 0 the Arhenius reaches a viscosity of 0 and a 0/0 may arrise in the plasticity */ 
		/*pressure*/
		ierr = DMDAGetScalarElementField_2D(elp,nen_p,(PetscInt*)&elnidx_p[nen_p*e],p);CHKERRQ(ierr);
		ConstructNi_pressure(xip,elcoords,NIp);
		pressure_mp = 0.0;
		for (k=0; k<P_BASIS_FUNCTIONS; k++) {
			pressure_mp += NIp[k] * elp[k];
		}
		//pressure_mp = pressure_mp -(sxx_mp+syy_mp)/2; 
		
		
		
		/* temperature */
		T_mp = 0; 
		alpha_mp = 0; 
		if (active_thermal_energy) {
			DataFieldAccessPoint(PField_thermal,pidx,(void**)&mpprop_thermal);
			ierr = DMDAGetScalarElementFieldQ1_2D(elT,(PetscInt*)&elnidx_T[nen_T*e],LA_T);CHKERRQ(ierr);
			ConstructNi_Q1_2D(xip,NIT);
			for (k=0; k<NODES_PER_EL_Q1_2D; k++) {
				T_mp = T_mp + NIT[k] * elT[k];
			}
			
		}
		
		/* phase change */ 
		T_eq = T_mp; 
		P_eq = pressure_mp;
		
		/*density*/ // TO BE PLACE In the tk update maybe 
		switch (rheology->density_type[phase_mp]) {
			case DENSITY_CONSTANT:{
				rho = rheology->const_rho0[phase_mp];
				UnitsApplyInverseScaling(units->si_force_per_volume,rho,&rho_mp);
			}
				break; 
			case DENSITY_FUNCTION:{
				PetscScalar alpha_mp = rheology->temp_alpha[phase_mp];
				PetscScalar beta_mp; 
				UnitsApplyScaling(units->si_stress,rheology->temp_beta[phase_mp],&beta_mp);
				
				rho = rheology->const_rho0[phase_mp]*(1-alpha_mp*T_mp+beta_mp*pressure_mp);        
				UnitsApplyInverseScaling(units->si_force_per_volume,rho,&rho_mp);
			}
				break; 
			case DENSITY_TABLE:{
				double position[2],rhodummy;
				char name[PETSC_MAX_PATH_LEN];
				rho = rheology->const_rho0[phase_mp];
				UnitsApplyInverseScaling(units->si_force_per_volume,rho,&rho_mp);
				
				sprintf(&name,"densmap_%d",phase_mp);
				TempMap map = PETSC_NULL;
				ierr = pTatinCtxGetMap(user,&map,name);CHKERRQ(ierr);
				
				position[0] = T_eq;
				position[1] = P_eq;
				RheolMapGetDouble(map,position,&rhodummy);
				if (rhodummy == -1){
					PetscPrintf(PETSC_COMM_WORLD,"marker outside PT array,P= %e T=%e \n",pressure_mp,T_mp);
				}else if(rhodummy==1) {
					PetscPrintf(PETSC_COMM_WORLD,"I don' get what PT array does");                    
				}else{
					rho_mp = rhodummy;
					
				}
			}
				break;     
				
		}
		
		/* viscosity */;
		switch (rheology->viscous_type[phase_mp]) {
				
			case VISCOUS_CONSTANT: {
				//eta_mp = scaling_eta*rheology->const_eta0[phase_mp];
				UnitsApplyInverseScaling(units->si_viscosity,rheology->const_eta0[phase_mp],&eta_mp);
			}
				break;
				
			case VISCOUS_FRANKK: {
				PetscReal eta;
				eta  = rheology->const_eta0[phase_mp]*exp(-rheology->temp_theta[phase_mp]*T_mp);
				UnitsApplyInverseScaling(units->si_viscosity,eta,&eta_mp);
				
			}
				break;
				
			case VISCOUS_ARRHENIUS: {
				PetscScalar R       = 8.31440;
				PetscReal nexp      = rheology->arrh_nexp[phase_mp];
				PetscReal entalpy   = rheology->arrh_entalpy[phase_mp];
				PetscReal preexpA   = rheology->arrh_preexpA[phase_mp];
				PetscReal Vmol      = rheology->arrh_Vmol[phase_mp];
				PetscReal Tref      = rheology->arrh_Tref[phase_mp];
				PetscReal Ascale    = rheology->arrh_Ascale[phase_mp];
				PetscReal T_arrh    = T_mp + Tref ;
				PetscReal sr, eta, pressure; 
				
				UnitsApplyScaling(units->si_strainrate,strainrateinv_mp,&sr);
				
				if (sr < 1.0e-17) {
					sr = 1.0e-17;
				}
				UnitsApplyScaling(units->si_stress,pressure_mp,&pressure);
				
				
				entalpy = entalpy + pressure*Vmol;
				eta  = Ascale*0.25*pow(sr,1.0/nexp - 1.0)*pow(0.75*preexpA,-1.0/nexp)*exp(entalpy/(nexp*R*T_arrh));
				UnitsApplyInverseScaling(units->si_viscosity,eta,&eta_mp);
			}
				break;
				
			case VISCOUS_POWERLAW:{
					PetscReal I2, eta; 
			        PetscReal nexp      = rheology->arrh_nexp[phase_mp];
					PetscReal preexpA   = rheology->arrh_preexpA[phase_mp];			        
			        UnitsApplyScaling(units->si_strainrate,strainrateinv_mp,&I2);
					
					if (I2 < 1.0e-32) {
						I2 = 1.0e-32;
					}
			        
			        eta = preexpA*pow(I2,1.0/(nexp)-1.0); 
			        UnitsApplyInverseScaling(units->si_viscosity,eta,&eta_mp);
			        }
			    break;
			    
			 case VISCOUS_ARRHENIUS_MIXING:{		 
				PetscScalar R       = 8.31440;
				double position[2],preexpA,entalpy,nexp;
				char name[PETSC_MAX_PATH_LEN];
				TempMap map = PETSC_NULL;
				PetscReal Tref      = rheology->arrh_Tref[phase_mp];
				PetscReal Ascale    = rheology->arrh_Ascale[phase_mp];
				PetscReal T_arrh    = T_mp + Tref ;
				PetscReal sr, eta,pressure; 
				PetscReal Vmol      = rheology->arrh_Vmol[phase_mp];
                
				position[0] = T_eq;
				position[1] = P_eq; /// might be better to store a converged P for convergence
				
				sprintf(&name,"Amap_%d",phase_mp);
				ierr = pTatinCtxGetMap(user,&map,name);CHKERRQ(ierr);
				RheolMapGetDouble(map,position,&preexpA);
				
				sprintf(&name,"Qmap_%d",phase_mp);
				ierr = pTatinCtxGetMap(user,&map,name);CHKERRQ(ierr);
				RheolMapGetDouble(map,position,&entalpy);
				
				sprintf(&name,"nexpmap_%d",phase_mp);
				ierr = pTatinCtxGetMap(user,&map,name);CHKERRQ(ierr);
				RheolMapGetDouble(map,position,&nexp);
				

				UnitsApplyScaling(units->si_strainrate,strainrateinv_mp,&sr);
				
				if (sr < 1.0e-32) {
					sr = 1.0e-32;
				}
				UnitsApplyScaling(units->si_stress,pressure_mp,&pressure);
				entalpy = entalpy + pressure*Vmol;
				eta  = Ascale*0.25*pow(sr,1.0/nexp - 1.0)*pow(0.75*preexpA,-1.0/nexp)*exp(entalpy/(nexp*R*T_arrh));
				UnitsApplyInverseScaling(units->si_viscosity,eta,&eta_mp);
			}
				break;   
				
		}
        
       
        
        
        
		if (rheology->store_melt & active_thermal_energy) {
			 
			switch (rheology->melt_type[phase_mp]) {
		     	case MELT_NONE: {
				
				}
					break;
					
				case MELT_MUMU: {
				PetscScalar eta_melt,rho_melt;
				PetscScalar eta_melt_mp,rho_melt_mp;
				float melt_fraction;   
				
				Compute_melt_fraction_mumu(rheology,phase_mp,T_mp,pressure_mp,&melt_fraction);
			    MPntPStokesMeltSetField_melt_portion(mpprop_stokesmelt,melt_fraction);		
				eta_melt      = rheology->melt_eta[phase_mp];
				UnitsApplyInverseScaling(units->si_viscosity,eta_melt,&eta_melt_mp);
				eta_mp   = 1.0/(melt_fraction/eta_melt_mp+(1.0-melt_fraction)/eta_mp);
				
				rho_melt      = rheology->melt_rho[phase_mp];
				UnitsApplyInverseScaling(units->si_force_per_volume,rho_melt,&rho_melt_mp);
				rho_mp   = rho_mp*(1.0-melt_fraction)+melt_fraction*rho_melt_mp;
				}
				break; 
			
			}
          }
        
        if (elastic) {
            PetscReal G_mp,sxx_r,syy_r,sxy_r,sxx_old,syy_old,sxy_old,theta_mp;
            PetscReal G = rheology->const_shearmod[phase_mp];
            
            if (G > 0.0) {
            UnitsApplyScaling(units->si_stress,G,&G_mp);
            //PetscPrintf(PETSC_COMM_WORLD," eta= %1.2e, G = %1.2e,dt = %1.2e \n",  eta_mp, G_mp, user->dt);
            theta_mp  = 1.0/( 1.0 + G_mp*(user->dt)/eta_mp);
            eta_mp =  G * (theta_mp) * (user->dt);// check dt is scaled not sure about that.
            //PetscPrintf(PETSC_COMM_WORLD," eta= %1.2e, theta = %1.2e \n",  eta_mp, theta_mp);
            MPntPStokesElasGetField_sxx_old(mpprop_stokeselas,&sxx_old);
            MPntPStokesElasGetField_syy_old(mpprop_stokeselas,&syy_old);
            MPntPStokesElasGetField_sxy_old(mpprop_stokeselas,&sxy_old);
            sxx_r = theta_mp* sxx_old;
            syy_r = theta_mp* syy_old;
            sxy_r = theta_mp* sxy_old;
            
            sxx_mp = 2.0 * eta_mp * exx_mp + sxx_r;
            syy_mp = 2.0 * eta_mp * eyy_mp + syy_r;
            sxy_mp = 2.0 * eta_mp * exy_mp + sxy_r;
            
            MPntPStokesElasSetField_sxx_r(mpprop_stokeselas,sxx_r);
            MPntPStokesElasSetField_syy_r(mpprop_stokeselas,syy_r);
            MPntPStokesElasSetField_sxy_r(mpprop_stokeselas,sxy_r);
            //PetscPrintf(PETSC_COMM_WORLD," %1.2e \n",  eta_mp);
            }else{
                sxx_mp = 2.0 * eta_mp * exx_mp ;
                syy_mp = 2.0 * eta_mp * eyy_mp ;
                sxy_mp = 2.0 * eta_mp * exy_mp ;
                
                MPntPStokesElasSetField_sxx_r(mpprop_stokeselas,0.0);
                MPntPStokesElasSetField_syy_r(mpprop_stokeselas,0.0);
                MPntPStokesElasSetField_sxy_r(mpprop_stokeselas,0.0);
            }
        }else{
            /* viscous stress predictor */
            d1 = 2.0 * eta_mp;
            sxx_mp = d1 * exx_mp;
            syy_mp = d1 * eyy_mp;
            sxy_mp = d1 * exy_mp;
            
            
        }
        
        
        
		if (plastic) {
			is_yielding = (char)YTYPE_NONE;
			switch (rheology->plastic_type[phase_mp]) {
					
				case PLASTIC_NONE: {
				}
					break;
					
				case PLASTIC_M: {
					PetscScalar tauyield,dummy,tauyield_mp,stressinv_mp;
                    stressinv_mp = sqrt(0.25*(sxx_mp-syy_mp)*(sxx_mp-syy_mp)+sxy_mp*sxy_mp);
					ApplyPlasticSofteningMarker(rheology,phase_mp, &tauyield, &dummy, mpprop_stokespl, 0.0,user->step);
					UnitsApplyInverseScaling(units->si_stress,tauyield,&tauyield_mp);
					yield_type = YTYPE_MISES;
                    /* apply plastic correction when marker is yielding */
					if (stressinv_mp > tauyield_mp) {
						//PetscScalar eta_vis = eta_mp;
						eta_mp = 0.5*tauyield_mp/strainrateinv_mp;
						//PetscPrintf(PETSC_COMM_WORLD," eta_vis %e eta_plas %e tauyield %e stress_inv %e \n", eta_vis,eta_mp,tauyield_mp, stressinv_mp);
						npoints_yielded++;
						is_yielding = (char)yield_type; 
					}
				}
					break;
					
				case PLASTIC_DP: {
					PetscScalar tens_cutoff_mp,Hst_cutoff_mp,phi,Co,A,B,Co_mp;
					PetscScalar tauyield_mp,stressinv_mp,eta_mp_pred,eps;    
					MPntPStokesGetField_eta_effective(mpprop_stokes,&eta_mp_pred);
					PetscScalar eta_0 = eta_lower_cutoff_global_mp;
					PetscScalar factor = 1.0; 
					UnitsApplyInverseScaling(units->si_stress,rheology->tens_cutoff[phase_mp],&tens_cutoff_mp);
					UnitsApplyInverseScaling(units->si_stress,rheology->Hst_cutoff[phase_mp],&Hst_cutoff_mp);
					if (eta_mp_pred > eta_mp) {
						eps = user->dt*strainrateinv_mp;
					}
					ApplyPlasticSofteningMarker(rheology,phase_mp, &Co, &phi, mpprop_stokespl,eps,user->step);
					A           = sin(phi/180.0*M_PI);
					B           = cos(phi/180.0*M_PI);	
					UnitsApplyInverseScaling(units->si_stress,Co,&Co_mp);
					tauyield_mp = A * pressure_mp + B * Co_mp; //+stressinv_mp/1000.;    
					yield_type  = YTYPE_DP; 
					
					if (tauyield_mp < tens_cutoff_mp) {
						/* failure in tension cutoff */
						tauyield_mp = tens_cutoff_mp;
						yield_type = YTYPE_TENSILE_FAILURE;
					} else if (tauyield_mp > Hst_cutoff_mp) {   
						/* failure at High stress cut off à la boris */
						tauyield_mp = Hst_cutoff_mp;
						yield_type = YTYPE_MISES;
					}  
					
					/* apply plastic correction when marker is yielding */ 
                    stressinv_mp = sqrt(0.25*(sxx_mp-syy_mp)*(sxx_mp-syy_mp)+sxy_mp*sxy_mp);
					if (stressinv_mp > tauyield_mp) {
					    PetscScalar eta_vis = eta_mp; 
						eta_mp = 0.5*tauyield_mp/strainrateinv_mp;
						// PetscPrintf(PETSC_COMM_WORLD," eta_vis %e eta_plas %e tauyield %e stress_inv %e \n", eta_vis,eta_mp,tauyield_mp, stressinv_mp);
	
						//if (eta_mp/eta_vis < 0.00001){
						npoints_yielded++;
						//if ( yield_type > 1) PetscPrintf(PETSC_COMM_WORLD," yield type %d \n",yield_type);
						is_yielding = (char)yield_type;
						//}
						 /*Apply regularisation à la Dave */                        
                        eta_mp = eta_mp;//+ 100.0*eta_0; 
                        //eta_mp = pow( eta_mp, -factor) + pow(eta_0, -factor);
                        //eta_mp = pow( eta_mp, -1.0/factor );
					}
					
				}
					break;
			}
			MPntPStokesPlSetField_yield_indicator(mpprop_stokespl,is_yielding);
		}
#if 0
        /* Global cuttoffs for viscosity*/
		if (eta_mp > eta_upper_cutoff_global_mp) {
			eta_mp = eta_upper_cutoff_global_mp;
		}
		if (eta_mp < eta_lower_cutoff_global_mp) {
			eta_mp = eta_lower_cutoff_global_mp;
		}
#endif
		/* update viscosity on marker */
		MPntPStokesSetField_eta_effective(mpprop_stokes,eta_mp);
		
		/* update density on marker */
		/* density at mp */
		
		MPntPStokesSetField_density(mpprop_stokes,rho_mp);

		/* monitor bounds */
		if (eta_mp > max_eta) { max_eta = eta_mp; }
		if (eta_mp < min_eta) { min_eta = eta_mp; }
		/* monitor bounds */
		if (rho_mp > max_rho) { max_rho = rho_mp; }
		if (rho_mp < min_rho) { min_rho = rho_mp; }
       
	}
	
	PetscGetTime(&t1);
    
#if 1
	ierr = MPI_Allreduce(&min_eta,&min_eta_g,1, MPI_DOUBLE, MPI_MIN, PETSC_COMM_WORLD);CHKERRQ(ierr);
	ierr = MPI_Allreduce(&max_eta,&max_eta_g,1, MPI_DOUBLE, MPI_MAX, PETSC_COMM_WORLD);CHKERRQ(ierr);
	
	ierr = MPI_Allreduce(&min_rho,&min_rho_g,1, MPI_DOUBLE, MPI_MIN, PETSC_COMM_WORLD);CHKERRQ(ierr);
	ierr = MPI_Allreduce(&max_rho,&max_rho_g,1, MPI_DOUBLE, MPI_MAX, PETSC_COMM_WORLD);CHKERRQ(ierr);
    
	PetscPrintf(PETSC_COMM_WORLD,"Update stokes elas non linearities (VPT) ND [mpoint]: (min,max)_eta %1.2e,%1.2e; log10(max/min) %1.2e; (min,max)_rho %1.2e,%1.2e; cpu time %1.2e (sec)\n",  min_eta_g, max_eta_g, log10(max_eta_g/min_eta_g),min_rho_g, max_rho_g, t1-t0 );
#endif
	
	UnitsApplyScaling(units->si_viscosity,min_eta_g,&min_eta_g);
	UnitsApplyScaling(units->si_viscosity,max_eta_g,&max_eta_g);
	UnitsApplyScaling(units->si_force_per_volume,min_rho_g,&min_rho_g);
	UnitsApplyScaling(units->si_force_per_volume,max_rho_g,&max_rho_g);
	
    //  PetscPrintf(PETSC_COMM_WORLD,"Update stokes non linearities (VPT) SI [mpoint]: (min,max)_eta %1.2e,%1.2e; log10(max/min) %1.2e; (min,max)_rho %1.2e,%1.2e; cpu time %1.2e (sec)\n",  min_eta_g, max_eta_g, log10(max_eta_g/min_eta_g),min_rho_g, max_rho_g, t1-t0 );
	
	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokes);
	
	
	if (plastic) { 
		DataFieldRestoreAccess(PField_stokespl);
#if 0		
		ierr = MPI_Allreduce(&npoints_yielded,&npoints_yielded_g,1, MPI_INT, MPI_SUM, PETSC_COMM_WORLD);CHKERRQ(ierr);
		PetscPrintf(PETSC_COMM_WORLD,"Update stokes non linearities (VPT) [mpoint]: npoints_yielded %d\n", npoints_yielded_g);
#endif		
	}
	
	if (melt) { 
		DataFieldRestoreAccess(PField_stokesmelt);
	}
    
    if (elastic) {
        DataFieldRestoreAccess(PField_stokeselas);
    }
	if (active_thermal_energy) {
		DataFieldRestoreAccess(PField_thermal);
		ierr = VecRestoreArray(T_local_vec,&LA_T);CHKERRQ(ierr);
		ierr = DMRestoreLocalVector(daT,&T_local_vec);CHKERRQ(ierr);		
	}
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TkUpdateRheologyMarkerStokes_EVPT"
PetscErrorCode TkUpdateRheologyMarkerStokes_EVPT(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[])
{	
	PetscErrorCode ierr;
	int         pidx,n_mp_points;
	DataBucket  db;
	DataField   PField_std, PField_stokes,PField_stokeselas, PField_stokespl, PField_thermal,PField_stokesmelt;
	PetscScalar GNIu[2][Q2_NODES_PER_EL_2D],NIp[P_BASIS_FUNCTIONS];
	PetscScalar NIu[U_BASIS_FUNCTIONS];
	PetscScalar NIT[NODES_PER_EL_Q1_2D];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen_u,nen_p,nen_T,e,i,j,k;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	const PetscInt *elnidx_T;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	PetscScalar elT[NODES_PER_EL_Q1_2D];
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	PetscInt vel_el_lidx[U_BASIS_FUNCTIONS*2];
	PetscInt *gidx;
	PetscScalar  d1,exx_mp,eyy_mp,exy_mp,eta_mp,strainrateinv_mp,u_mp,v_mp;
    PetscReal     sxx_r,syy_r,sxy_r,sxx_old,syy_old,sxy_old;
	RheologyConstants *rheology;
    pTatinUnits *units;
	BTruth plastic;
	BTruth melt    = PETSC_FALSE;
	PetscBool         active_thermal_energy = PETSC_FALSE;    
	PhysCompEnergyCtx phys_energy;
	Vec               T_vec,T_local_vec;
	DM                daT;
	PetscScalar       *LA_T;
	
	
	PetscFunctionBegin;
	db = user->db;
    units   = &user->units;
	
	DataBucketQueryDataFieldByName(db,MPntPStokesPl_classname,&plastic);
    ierr = pTatinPhysCompActivated(user,PhysComp_Energy,&active_thermal_energy);CHKERRQ(ierr);
    
	
		
		/* setup for coords */
		ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
		ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
		ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
		
		ierr = DMDAGetGlobalIndices(dau,0,&gidx);CHKERRQ(ierr);
		
		ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
		ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
		
		/* setup for rheological parameters depending on phases */
		rheology  = &user->rheology_constants;
		
		/* marker loop */
		
		
		DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
		DataFieldGetAccess(PField_std);
		DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
		
		DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
		DataFieldGetAccess(PField_stokes);
		DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));
		
    
        DataBucketGetDataFieldByName(db,MPntPStokesElas_classname,&PField_stokeselas);
        DataFieldGetAccess(PField_stokeselas);
        DataFieldVerifyAccess(PField_stokeselas,sizeof(MPntPStokesElas));
		
        if(plastic){
           DataBucketGetDataFieldByName(db,MPntPStokesPl_classname,&PField_stokespl);
		   DataFieldGetAccess(PField_stokespl);
		   DataFieldVerifyAccess(PField_stokespl,sizeof(MPntPStokesPl));
        }
		if (active_thermal_energy){
		   DataBucketGetDataFieldByName(db,MPntPThermal_classname,&PField_thermal);
		   DataFieldGetAccess(PField_thermal);
		   DataFieldVerifyAccess(PField_thermal,sizeof(MPntPThermal));
		}
		
		DataBucketGetSizes(db,&n_mp_points,0,0);
		
		
		for (pidx=0; pidx<n_mp_points; pidx++) {
			MPntStd         *material_point;
			MPntPStokes     *mpprop_stokes;
            MPntPStokesElas *mpprop_stokeselas;
			MPntPStokesPl   *mpprop_stokespl;
			MPntPThermal    *mpprop_thermal;
			double          *position, *xip;
			
			int           wil,phase_mp;
			char          is_yielding;
			float         eplastic;
			PetscScalar   J[2][2], iJ[2][2];
			PetscScalar   J_p,ojp;
			
			
			DataFieldAccessPoint(PField_std,     pidx,(void**)&material_point);
			DataFieldAccessPoint(PField_stokes,  pidx,(void**)&mpprop_stokes);
			DataFieldAccessPoint(PField_stokeselas,pidx,(void**)&mpprop_stokeselas);
            
            MPntStdGetField_local_element_index(material_point,&wil);
            MPntStdGetField_global_coord(material_point,&position);
            MPntStdGetField_local_coord(material_point,&xip);
            MPntStdGetField_phase_index(material_point,&phase_mp);
            MPntPStokesPlGetField_plastic_strain(mpprop_stokespl,&eplastic);
            MPntPStokesGetField_eta_effective(mpprop_stokes,&eta_mp);
            
            e = wil;
            /* get nodal properties for element e */
            ierr = StokesVelocity_GetElementLocalIndices(vel_el_lidx,(PetscInt*)&elnidx_u[nen_u*e]);CHKERRQ(ierr);
            ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
            ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],u);CHKERRQ(ierr);
            ierr = DMDAGetScalarElementField_2D(elp,nen_p,(PetscInt*)&elnidx_p[nen_p*e],p);CHKERRQ(ierr);
            
            /* get element velocity/coordinates in component form */
            for (i=0; i<Q2_NODES_PER_EL_2D; i++) {
                ux[i] = elu[2*i  ];
                uy[i] = elu[2*i+1];
                
                xc[i] = elcoords[2*i  ];
                yc[i] = elcoords[2*i+1];
            }
            
            PTatinConstructNI_Q2_2D(xip,NIu);
            PTatinConstructGNI_Q2_2D(xip,GNIu);
            ConstructNi_pressure(xip,elcoords,NIp);
            
            
            
            /* coord transformation */
            for (i=0; i<2; i++) {
                for (j=0; j<2; j++) { J[i][j] = 0.0; }
            }
            for (k=0; k<Q2_NODES_PER_EL_2D; k++) {
                J[0][0] += GNIu[0][k] * xc[k] ;
                J[0][1] += GNIu[0][k] * yc[k] ;
                
                J[1][0] += GNIu[1][k] * xc[k] ;
                J[1][1] += GNIu[1][k] * yc[k] ;
            }
            J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
            ojp = 1.0/J_p;
            iJ[0][0] =  J[1][1]*ojp;
            iJ[0][1] = -J[0][1]*ojp;
            iJ[1][0] = -J[1][0]*ojp;
            iJ[1][1] =  J[0][0]*ojp;
            
            /* global derivs at each quadrature point */
            for (k=0; k<Q2_NODES_PER_EL_2D; k++) {
                nx[k] = iJ[0][0]*GNIu[0][k] + iJ[0][1]*GNIu[1][k];
                ny[k] = iJ[1][0]*GNIu[0][k] + iJ[1][1]*GNIu[1][k];
            }
            
            /* u,v and strain rate (e= B u) at gp */
            u_mp = v_mp = 0.0;
            exx_mp = eyy_mp = exy_mp = 0.0;
            for (k=0; k<U_BASIS_FUNCTIONS; k++) {
                u_mp   += NIu[k] * ux[k];
                v_mp   += NIu[k] * uy[k];
                
                exx_mp += nx[k] * ux[k];
                eyy_mp += ny[k] * uy[k];
                exy_mp += ny[k] * ux[k] + nx[k] * uy[k];
            }
            exy_mp = 0.5 * exy_mp;
            
            MPntPStokesElasGetField_sxx_r(mpprop_stokeselas,&sxx_r);
            MPntPStokesElasGetField_syy_r(mpprop_stokeselas,&syy_r);
            MPntPStokesElasGetField_sxy_r(mpprop_stokeselas,&sxy_r);
            // compute stress update with last viscosity and sxx_r stress
            sxx_old = 2.0 * eta_mp * exx_mp + sxx_r;
            syy_old = 2.0 * eta_mp * eyy_mp + syy_r;
            sxy_old = 2.0 * eta_mp * exy_mp + sxy_r;
            // correct for rotation and advection
            // TODO
            /*if (phase_mp==1){
                ierr=PetscPrintf(PETSC_COMM_WORLD,"sxx_old= %e syy_old= %e syx_old= %e \n",sxx_old,syy_old,sxy_old);CHKERRQ(ierr);
                ierr=PetscPrintf(PETSC_COMM_WORLD,"sxx_r= %e syy_r= %e syx_r= %e \n",sxx_r,syy_r,sxy_r);CHKERRQ(ierr);
                ierr=PetscPrintf(PETSC_COMM_WORLD,"exx_mp= %e eyy_mp= %e eyx_mp= %e \n",exx_mp,eyy_mp,exy_mp);CHKERRQ(ierr);
                        }*/
            MPntPStokesElasSetField_sxx_old(mpprop_stokeselas,sxx_old);
            MPntPStokesElasSetField_syy_old(mpprop_stokeselas,syy_old);
            MPntPStokesElasSetField_sxy_old(mpprop_stokeselas,sxy_old);
            
            if (plastic){
			DataFieldAccessPoint(PField_stokespl,pidx,(void**)&mpprop_stokespl);
			MPntPStokesPlGetField_yield_indicator(mpprop_stokespl,&is_yielding);
			strainrateinv_mp = sqrt(0.25*(exx_mp-eyy_mp)*(exx_mp-eyy_mp)+exy_mp*exy_mp);
                if (is_yielding != YTYPE_NONE) {
                    /* community accepted version */
                    eplastic += strainrateinv_mp * user->dt;
				
                    MPntPStokesPlSetField_plastic_strain(mpprop_stokespl,eplastic);
                }
            }
            
            if(active_thermal_energy){
                PetscScalar eta_mp,H_mp,H0_mp,rho_mp, cp, cp_mp,Wshear_mp, rho;
                PetscScalar fac =0.0;
                cp    = 1000.0; // J/kg/K --> m^2s-2 --> m^2s-1 *s-1
                UnitsApplyInverseScaling(units->si_heatcapacity,cp,&cp_mp);
                
                ierr  = PetscOptionsGetReal(PETSC_NULL,"-shear_heating_factor",&fac,0);CHKERRQ(ierr);
                DataFieldAccessPoint(PField_thermal,pidx,(void**)&mpprop_thermal);
                DataFieldAccessPoint(PField_stokes,pidx,(void**)&mpprop_stokes);
                MPntPThermalGetField_heat_prod(mpprop_thermal,&H0_mp);
                MPntPStokesGetField_eta_effective(mpprop_stokes,&eta_mp);
                MPntPStokesGetField_density(mpprop_stokes,&rho_mp);
                UnitsApplyScaling(units->si_force_per_volume,rho_mp,&rho);
                UnitsApplyInverseScaling(units->si_density,rho,&rho_mp);
                
                Wshear_mp = 2*eta_mp*(exx_mp*exx_mp + 2.0*exy_mp*exy_mp +eyy_mp*eyy_mp);
                H_mp = H0_mp+Wshear_mp/rho_mp/cp_mp*fac;
                MPntPThermalSetField_heat_source(mpprop_thermal,H_mp);
            }

		/// this needs to be integrated here and removed for non linearity
	
#if 0           
			switch (rheology->density_type[phase_mp]) {
				case DENSITY_CONSTANT:{
					rho = rheology->const_rho0[phase_mp];
					UnitsApplyInverseScaling(units->si_force_per_volume,rho,&rho_mp);
				}
					break; 
				case DENSITY_FUNCTION:{
					PetscScalar alpha_mp = rheology->temp_alpha[phase_mp];
					PetscScalar beta_mp; 
					UnitsApplyScaling(units->si_stress,rheology->temp_beta[phase_mp],&beta_mp);
					
					rho = rheology->const_rho0[phase_mp]*(1-alpha_mp*T_mp+beta_mp*pressure_mp);        
					UnitsApplyInverseScaling(units->si_force_per_volume,rho,&rho_mp);
				}
					break; 
				case DENSITY_TABLE:{
					double position[2],rhodummy;
					char *name;
					rho = rheology->const_rho0[phase_mp];
					UnitsApplyInverseScaling(units->si_force_per_volume,rho,&rho_mp);
					
					asprintf(&name,"densmap_%d",phase_mp);
					TempMap map = PETSC_NULL;
					ierr = pTatinCtxGetMap(user,&map,name);CHKERRQ(ierr);
					position[0] = T_mp;
					position[1] = pressure_mp;
					TempMapGetDouble(map,position,&rhodummy);
					if (rhodummy == -1){
						PetscPrintf(PETSC_COMM_WORLD,"marker outside PT array,P= %e T=%e \n",pressure_mp,T_mp);
					}else if(rhodummy==1) {
						PetscPrintf(PETSC_COMM_WORLD,"I don' get what PT array does");                    
					}else{
						rho_mp = rhodummy;
						
					}
				}
					break;     
					
			}
#endif		
		
	}
    DataFieldRestoreAccess(PField_std);
    DataFieldRestoreAccess(PField_stokes);
    DataFieldRestoreAccess(PField_stokeselas);
    if(plastic){
        DataFieldRestoreAccess(PField_stokespl);
    }
    if(active_thermal_energy){
        DataFieldRestoreAccess(PField_thermal);
    }
    
    ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	PetscFunctionReturn(0);
}














