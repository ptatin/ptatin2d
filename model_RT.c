/*
 
 Model Description:

 These files define the setup for a Rayleigh Taylor instability.
 The default options run the van-keken, chemical convection benchmark;
 Domain is [0,0.9142] x [0,1] with free slip top and bottom boundaries, with no slip vertical sides.
 The interface between the two fluids is given by

 interface_y = yoffset + amp * cos( 2.0 * M_PI * lambda * position[0] / 0.9142 );

 
 Input / command line parameters:
 -rt_eta0 : viscosity of the lower layer
 -rt_eta1 : viscosity of the upper layer
 -rt_eta0 : density of the lower layer
 -rt_eta1 : density of the upper layer
 -rt_amp  : amplitude of the interface
 -rt_lambda : wavelenght of the interface
 -rt_yoffset : vertical shift in the interface
 
 
 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"

/* ================= RT: Rayleigh Taylor example ================= */

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelOutput_RT"
PetscErrorCode pTatin2d_ModelOutput_RT(pTatinCtx ctx,Vec X,const char prefix[])
{
	char *pvdfilename;
	char *name;
	PetscErrorCode ierr;

	
	if (prefix == PETSC_NULL) {
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,"qpoints");CHKERRQ(ierr);
		ierr = pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(ctx,ctx->outputpath,"faceqpoints");CHKERRQ(ierr);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,"vp");CHKERRQ(ierr);
		ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,"int_fields");CHKERRQ(ierr);
		ierr = SwarmOutputParaView_MPntPStokes(ctx->db,ctx->outputpath,"mpoints_stk");CHKERRQ(ierr);
	} else { /* exanple of adding a different prefix to the file names - not sure this is useful */
		
		///////////////////////////////////////////////////////////////////////////
		// PVD
		asprintf(&pvdfilename,"%s/timeseries_qpoints.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;
			
			asprintf(&vtkfilename, "%s_qpoints.pvtu",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			free(vtkfilename);
		}
		free(pvdfilename);
		// PVTU
		asprintf(&name,"%s_qpoints",prefix);
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);

		///////// 
		asprintf(&name,"%s_faceqpoints",prefix);
		ierr = pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
		
		///////////////////////////////////////////////////////////////////////////
		// PVD
		asprintf(&pvdfilename,"%s/timeseries_vp.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;
			
			asprintf(&vtkfilename, "%s_vp.pvts",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			free(vtkfilename);
		}
		free(pvdfilename);
		// PVTS
		asprintf(&name,"%s_vp",prefix);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);

		///////////////////////////////////////////////////////////////////////////
		// PVD
		asprintf(&pvdfilename,"%s/timeseries_int_fields.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;
			
			asprintf(&vtkfilename, "%s_int_fields.pvts",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			free(vtkfilename);
		}
		free(pvdfilename);
		// PVTS
		asprintf(&name,"%s_int_fields",prefix);
		ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
		
		///////////////////////////////////////////////////////////////////////////
		// PVD
		asprintf(&pvdfilename,"%s/timeseries_mpoints_stk.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;
			
			asprintf(&vtkfilename, "%s_mpoints_stk.pvtu",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			free(vtkfilename);
		}
		free(pvdfilename);
		// PVTU
		asprintf(&name,"%s_mpoints_stk",prefix);
		ierr = SwarmOutputParaView_MPntPStokes(ctx->db,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
		//////////////
		
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_RT"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_RT(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
	
	ierr = DMDASetUniformCoordinates(ctx->dav,0.0,0.9142, 0.0,1.0, 0.0,0.0);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_RT"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_RT(pTatinCtx ctx)
{
	PetscInt               e,ncells,n_mp_points;
	PetscInt               p,ngp;
	GaussPointCoefficientsStokes *gausspoints;
	PetscReal              opts_eta0,opts_eta1,opts_rho0,opts_rho1;
	PetscReal              opts_amp, opts_lambda,opts_yoffset;
	DataBucket             db;
	DataField              PField_std,PField_stokes;
	double                 eta,rho,interface_y;
	int                    phase_init,phase;
	PetscErrorCode         ierr;
	
	PetscFunctionBegin;
	
	opts_eta0 = 1.0;
	opts_eta1 = 1.0;

	opts_rho0 = 0.0;
	opts_rho1 = 1.0;
	
	opts_lambda  = 0.5;
	opts_amp     = 0.02;
	opts_yoffset = 0.2;
	
	ierr = PetscOptionsGetReal(PETSC_NULL,"-rt_eta0",  &opts_eta0,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-rt_eta1",  &opts_eta1,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-rt_rho0",  &opts_rho0,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-rt_rho1",  &opts_rho1,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-rt_amp",   &opts_amp,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-rt_lambda",&opts_lambda,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-rt_yoffset",&opts_yoffset,0);CHKERRQ(ierr);
	
	ncells = ctx->Q->ncells;
	ngp    = ctx->Q->ngp;
	ctx->rheology_constants.rheology_type = RHEOLOGY_VISCOUS;
	
	/* define properties on material points */
	db = ctx->db;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));
	
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	for (p=0; p<n_mp_points; p++) {
		MPntStd     *material_point;
		MPntPStokes *mpprop_stokes;
		double  *position;
		
		DataFieldAccessPoint(PField_std,p,(void**)&material_point);
		DataFieldAccessPoint(PField_stokes,p,(void**)&mpprop_stokes);
		
		/* Access using the getter function provided for you (recommeneded for beginner user) */
		MPntStdGetField_global_coord(material_point,&position);
		// OR
		/* Access directly into material point struct (not recommeneded for beginner user) */
		/* Direct access only recommended for efficiency */
		/* position = material_point->coor; */
		
		MPntStdGetField_phase_index(material_point,&phase_init);
		

		interface_y = opts_yoffset + opts_amp * cos( 2.0 * M_PI * opts_lambda * position[0] / 0.9142 );
		
		if (position[1] < interface_y) { /* fluid region 0 */
			rho = opts_rho0;
			eta = opts_eta0;
		} else { /* fluid region 0 */
			rho = opts_rho1;
			eta = opts_eta1;
		}
		
		/* for visualisation only */
		phase = 0;
		if (position[0] > 0.5) {
			phase = 1;
		}
		if ( (position[0]>0.24) && (position[0]<0.26) ) {
			phase = 2;
		}
		if ( (position[1]>0.49) && (position[1]<0.51) ) {
			phase = 3;
		}
		
		/* user the setters provided for you */
		MPntStdSetField_phase_index(material_point,phase);
		
		MPntPStokesSetField_eta_effective(mpprop_stokes,eta);
		MPntPStokesSetField_density(mpprop_stokes,rho);
	}
	
	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokes);
	
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_RT"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_RT(pTatinCtx ctx)
{
	BCList         ubclist;
	PetscScalar    bcval;
	DM             dav;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ubclist = ctx->u_bclist;
	dav     = ctx->dav;
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	
	//	bcval = 1.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}


