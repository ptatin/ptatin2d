/*
 
 Model Description:

 These functions define the setup for an isoviscous simple shear problem with free surface in a gravity field assuming le shear is localised on a ramp.
Domain is [0,1,-1, 0] with free surface at the jmax, free slip at the jmin 
imin and imax have a step of velocity of value one which is shifted.
 
  Input / command line parameters:
 -stepShearFree_eta   : viscosity (default =1.0);
 -stepShearFree_body  : rho*g (default =-1.0); 
 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"

/* ================= StepShearFree ================= */

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelOutput_StepShearFree"
PetscErrorCode pTatin2d_ModelOutput_StepShearFree(pTatinCtx ctx,Vec X,const char prefix[])
{
	PetscErrorCode ierr;

	ierr = pTatin2d_ModelOutput_Default(ctx,X,prefix);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_StepShearFree"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_StepShearFree(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
	
	ierr = DMDASetUniformCoordinates(ctx->dav,0.0,1.0, -1.0 ,0.0, 0.0,0.0);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_StepShearFree"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_StepShearFree(pTatinCtx ctx)
{
	PetscInt e,ncells;
	PetscInt p,ngp;
	GaussPointCoefficientsStokes *gausspoints;
	PetscReal opts_eta,opts_body;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	opts_eta  = 1.0;
	opts_body = 1.0;

	
	ierr = PetscOptionsGetReal(PETSC_NULL,"-StepShearFree_eta",&opts_eta,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-StepShearFree_body",&opts_body,0);CHKERRQ(ierr);

	
	ncells = ctx->Q->ncells;
	ngp    = ctx->Q->ngp;
	ctx->rheology_constants.rheology_type = RHEOLOGY_VISCOUS;

	for (e=0;e<ncells;e++) {
		ierr = QuadratureStokesGetCell(ctx->Q,e,&gausspoints);CHKERRQ(ierr);
		
		for (p=0; p<ngp; p++) {
			/*viscosity*/
			gausspoints[p].eta     = opts_eta;
			gausspoints[p].eta_ref = opts_eta;
			/* rhs */
			gausspoints[p].Fu[0] = 0.0;
			gausspoints[p].Fu[1] = opts_body;
		}
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_StepShearFree"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_StepShearFree(pTatinCtx ctx)
{
	BCList         ubclist;
	DM             dav;
	PetscErrorCode ierr;
	BC_Step     Step_data;
	PetscScalar    bcval;	
	PetscFunctionBegin;
	
	ubclist = ctx->u_bclist;
	dav     = ctx->dav;

	/* allocate memory for the user structure for the bc info */
	ierr = PetscMalloc(sizeof(struct _p_BC_Step),&Step_data);CHKERRQ(ierr);
	Step_data->dim =1;
	Step_data->beg  =-1.0;
	Step_data->end  =0.0;
	Step_data->a    =-0.55;	 
	Step_data->b    =-0.45;
	Step_data->v0   =0.0;	 
	Step_data->v1   =1.0;
	
	ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_Step,(void*)Step_data);CHKERRQ(ierr);	
	
	Step_data->a    =-0.25;	 
	Step_data->b    =-0.35;
	
	ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_Step,(void*)Step_data);CHKERRQ(ierr);
	
	bcval = 0;
	ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	
	/* clean up user struct */
	ierr = PetscFree(Step_data);CHKERRQ(ierr);

	PetscFunctionReturn(0);
}

 



