
#include <stdio.h>
#include <stdlib.h>
#include <petsc.h>
#include "pTatin2d.h"

const char *PTatinFieldNames[] =  {
  "pressure", 
  "viscosity", 
  "s_xx","s_xy","s_yy",
  "t_xx","t_xy","t_yy",
  "e_xx","e_xy","e_yy",
  "s_II",
  "t_II",
  "e_II",
  0 };


#undef __FUNCT__  
#define __FUNCT__ "pTatinLoadFieldList"
PetscErrorCode pTatinLoadFieldList(PTatinField fieldlist[])
{
	PetscInt k,L;
	PetscBool flg,hasField;
	static int been_here=0;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	L = (int)__PField_TERMINATOR__;
	for (k=0; k<L; k++) {
		fieldlist[k] = -1;
	}
	
	/* add defaults */
	fieldlist[PField_Viscosity] = PField_Viscosity;
	fieldlist[PField_S2]        = PField_S2;
	fieldlist[PField_E2]        = PField_E2;
	fieldlist[PField_T2]        = PField_T2;

	
	hasField = PETSC_FALSE; ierr = PetscOptionsGetBool(PETSC_NULL,"-view_int_field_pressure",&hasField,&flg);CHKERRQ(ierr);
	if (hasField) { fieldlist[PField_Pressure] = PField_Pressure; }

	hasField = PETSC_FALSE; ierr = PetscOptionsGetBool(PETSC_NULL,"-view_int_field_viscosity",&hasField,&flg);CHKERRQ(ierr);
	if (hasField) { fieldlist[PField_Viscosity] = PField_Viscosity; }
	// sigma
	hasField = PETSC_FALSE; ierr = PetscOptionsGetBool(PETSC_NULL,"-view_int_field_s_xx",&hasField,&flg);CHKERRQ(ierr);
	if (hasField) { fieldlist[PField_Sxx] = PField_Sxx; }

	hasField = PETSC_FALSE; ierr = PetscOptionsGetBool(PETSC_NULL,"-view_int_field_s_xy",&hasField,&flg);CHKERRQ(ierr);
	if (hasField) { fieldlist[PField_Sxy] = PField_Sxy; }
	
	hasField = PETSC_FALSE; ierr = PetscOptionsGetBool(PETSC_NULL,"-view_int_field_s_yy",&hasField,&flg);CHKERRQ(ierr);
	if (hasField) { fieldlist[PField_Syy] = PField_Syy; }

	hasField = PETSC_FALSE; ierr = PetscOptionsGetBool(PETSC_NULL,"-view_int_field_s_II",&hasField,&flg);CHKERRQ(ierr);
	if (hasField) { fieldlist[PField_S2] = PField_S2; }
	
	// tau
	hasField = PETSC_FALSE; ierr = PetscOptionsGetBool(PETSC_NULL,"-view_int_field_t_xx",&hasField,&flg);CHKERRQ(ierr);
	if (hasField) { fieldlist[PField_Txx] = PField_Txx; }
	
	hasField = PETSC_FALSE; ierr = PetscOptionsGetBool(PETSC_NULL,"-view_int_field_t_xy",&hasField,&flg);CHKERRQ(ierr);
	if (hasField) { fieldlist[PField_Txy] = PField_Txy; }
	
	hasField = PETSC_FALSE; ierr = PetscOptionsGetBool(PETSC_NULL,"-view_int_field_t_yy",&hasField,&flg);CHKERRQ(ierr);
	if (hasField) { fieldlist[PField_Tyy] = PField_Tyy; }

	hasField = PETSC_FALSE; ierr = PetscOptionsGetBool(PETSC_NULL,"-view_int_field_t_II",&hasField,&flg);CHKERRQ(ierr);
	if (hasField) { fieldlist[PField_T2] = PField_T2; }
	
	// epsilon
	hasField = PETSC_FALSE; ierr = PetscOptionsGetBool(PETSC_NULL,"-view_int_field_e_xx",&hasField,&flg);CHKERRQ(ierr);
	if (hasField) { fieldlist[PField_Exx] = PField_Exx; }
	
	hasField = PETSC_FALSE; ierr = PetscOptionsGetBool(PETSC_NULL,"-view_int_field_e_xy",&hasField,&flg);CHKERRQ(ierr);
	if (hasField) { fieldlist[PField_Exy] = PField_Exy; }
	
	hasField = PETSC_FALSE; ierr = PetscOptionsGetBool(PETSC_NULL,"-view_int_field_e_yy",&hasField,&flg);CHKERRQ(ierr);
	if (hasField) { fieldlist[PField_Eyy] = PField_Eyy; }
	
	hasField = PETSC_FALSE; ierr = PetscOptionsGetBool(PETSC_NULL,"-view_int_field_e_II",&hasField,&flg);CHKERRQ(ierr);
	if (hasField) { fieldlist[PField_E2] = PField_E2; }

	/* special cases */
	hasField = PETSC_FALSE; ierr = PetscOptionsGetBool(PETSC_NULL,"-view_int_field_all",&hasField,&flg);CHKERRQ(ierr);
	if (hasField) { 
		for (k=0; k<L; k++) {
			fieldlist[k] = k;
		}
	}
	
	hasField = PETSC_FALSE; ierr = PetscOptionsGetBool(PETSC_NULL,"-view_int_field_none",&hasField,&flg);CHKERRQ(ierr);
	if (hasField) { 
		for (k=0; k<L; k++) {
			fieldlist[k] = -1;
		}
	}
	
	if (been_here==0) {
		PetscPrintf(PETSC_COMM_WORLD,"*** pFieldOutputReport ***\n");
		for (k=0; k<L; k++) {
			switch (fieldlist[k]) {
				case PField_Pressure:
					PetscPrintf(PETSC_COMM_WORLD,"    IntField: \"%s\" \n",PTatinFieldNames[PField_Pressure]); 
					break;
				case PField_Viscosity:
					PetscPrintf(PETSC_COMM_WORLD,"    IntField: \"%s\" \n",PTatinFieldNames[PField_Viscosity]); 
					break;
					
				case PField_Sxx:
					PetscPrintf(PETSC_COMM_WORLD,"    IntField: \"%s\" \n",PTatinFieldNames[PField_Sxx]); 
					break;
				case PField_Sxy:
					PetscPrintf(PETSC_COMM_WORLD,"    IntField: \"%s\" \n",PTatinFieldNames[PField_Sxy]); 
					break;
				case PField_Syy:
					PetscPrintf(PETSC_COMM_WORLD,"    IntField: \"%s\" \n",PTatinFieldNames[PField_Syy]); 
					break;
					
				case PField_Txx:
					PetscPrintf(PETSC_COMM_WORLD,"    IntField: \"%s\" \n",PTatinFieldNames[PField_Txx]); 
					break;
				case PField_Txy:
					PetscPrintf(PETSC_COMM_WORLD,"    IntField: \"%s\" \n",PTatinFieldNames[PField_Txy]); 
					break;
				case PField_Tyy:
					PetscPrintf(PETSC_COMM_WORLD,"    IntField: \"%s\" \n",PTatinFieldNames[PField_Tyy]); 
					break;

				case PField_Exx:
					PetscPrintf(PETSC_COMM_WORLD,"    IntField: \"%s\" \n",PTatinFieldNames[PField_Exx]); 
					break;
				case PField_Exy:
					PetscPrintf(PETSC_COMM_WORLD,"    IntField: \"%s\" \n",PTatinFieldNames[PField_Exy]); 
					break;
				case PField_Eyy:
					PetscPrintf(PETSC_COMM_WORLD,"    IntField: \"%s\" \n",PTatinFieldNames[PField_Eyy]); 
					break;

				case PField_S2:
					PetscPrintf(PETSC_COMM_WORLD,"    IntField: \"%s\" \n",PTatinFieldNames[PField_S2]); 
					break;
				case PField_T2:
					PetscPrintf(PETSC_COMM_WORLD,"    IntField: \"%s\" \n",PTatinFieldNames[PField_T2]); 
					break;
				case PField_E2:
					PetscPrintf(PETSC_COMM_WORLD,"    IntField: \"%s\" \n",PTatinFieldNames[PField_E2]); 
					break;
					
			}
		}
		been_here = 1;
	}
	
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "pTatinGenerateParallelVTKName"
PetscErrorCode pTatinGenerateParallelVTKName(const char prefix[],const char suffix[],char **name)
{
	char *nn;
	int rank;
	
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	if (prefix!=NULL) {
		asprintf(&nn,"%s-subdomain%1.5d.%s",prefix,rank,suffix);
	} else {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"You must specify a prefix");
	}
	*name = nn;
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatinGenerateVTKName"
PetscErrorCode pTatinGenerateVTKName(const char prefix[],const char suffix[],char **name)
{
	char *nn;
	if (prefix!=NULL) {
		asprintf(&nn,"%s.%s",prefix,suffix);
	} else {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"You must specify a prefix");
	}
	*name = nn;
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatinOutputQuadratureStokesPointsRawVTU"
PetscErrorCode pTatinOutputQuadratureStokesPointsRawVTU(pTatinCtx user,const char name[])
{
	PetscErrorCode ierr;
	DM dau,dap;
	PetscInt nel,nen_u,nen_p,e,n,c,ngp,npoints;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	GaussPointCoefficientsStokes *gausspoints;
	FILE*	fp = NULL;
	
	if ((fp = fopen ( name, "w")) == NULL)  {
		SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
	}
	
	dau = user->dav;
	dap = user->dap;
	ngp = user->Q->ngp;

	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	npoints = nel * ngp;
	
	/* VTU HEADER - OPEN */
	fprintf(fp, "<?xml version=\"1.0\"?>\n");
	fprintf(fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
  fprintf(fp, "  <UnstructuredGrid>\n");
	fprintf(fp, "    <Piece NumberOfPoints=\"%d\" NumberOfCells=\"%d\" >\n",npoints,npoints);
	
	/* POINT COORDS */
	fprintf(fp, "    <Points>\n");
	fprintf(fp, "      <DataArray type=\"Float32\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	for (e=0;e<nel;e++) {
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		for (n=0; n<ngp; n++) {
			fprintf(fp, "      %1.4e %1.4e %1.4e \n", gausspoints[n].coord[0], gausspoints[n].coord[1], 0.0 );
		}
	}
	fprintf(fp, "      </DataArray>\n");
	fprintf(fp, "    </Points>\n");
	
	/* POINT-DATA HEADER - OPEN */
	fprintf(fp, "    <PointData>\n");

	/* POINT-DATA FIELDS */
	fprintf(fp, "      <DataArray type=\"Int32\" Name=\"phase\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	for (e=0;e<nel;e++) {
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		fprintf(fp, "      ");
		for (n=0; n<ngp; n++) {
			fprintf(fp, "%d ", gausspoints[n].phase );
		}
		fprintf(fp, "\n");
	}
	fprintf(fp, "      </DataArray>\n");

  
  
	fprintf(fp, "      <DataArray type=\"Float32\" Name=\"eta_eff\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	for (e=0;e<nel;e++) {
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		fprintf(fp, "      ");
		for (n=0; n<ngp; n++) {
			fprintf(fp, "%1.4e ", gausspoints[n].eta );
		}
		fprintf(fp, "\n");
	}
	fprintf(fp, "      </DataArray>\n");
	

  fprintf(fp, "      <DataArray type=\"Float32\" Name=\"plas_sr\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	for (e=0;e<nel;e++) {
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		fprintf(fp, "      ");
		for (n=0; n<ngp; n++) {
			fprintf(fp, "%1.4e ", gausspoints[n].plsr );
		}
		fprintf(fp, "\n");
	}
	fprintf(fp, "      </DataArray>\n");
 
  
  fprintf(fp, "      <DataArray type=\"Float32\" Name=\"plas_s\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	for (e=0;e<nel;e++) {
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		fprintf(fp, "      ");
		for (n=0; n<ngp; n++) {
			fprintf(fp, "%1.4e ", gausspoints[n].pls );
		}
		fprintf(fp, "\n");
	}
  
	fprintf(fp, "      </DataArray>\n");
  

  fprintf(fp, "      <DataArray type=\"Float32\" Name=\"sxx\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	for (e=0;e<nel;e++) {
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		fprintf(fp, "      ");
		for (n=0; n<ngp; n++) {
			fprintf(fp, "%1.4e ", gausspoints[n].stress_old[0] );
		}
		fprintf(fp, "\n");
	}
	fprintf(fp, "      </DataArray>\n");
 
  
  fprintf(fp, "      <DataArray type=\"Float32\" Name=\"syy\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	for (e=0;e<nel;e++) {
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		fprintf(fp, "      ");
		for (n=0; n<ngp; n++) {
			fprintf(fp, "%1.4e ", gausspoints[n].stress_old[1]);
		}
		fprintf(fp, "\n");
	}
	fprintf(fp, "      </DataArray>\n");
  
  
  fprintf(fp, "      <DataArray type=\"Float32\" Name=\"sxy\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	for (e=0;e<nel;e++) {
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		fprintf(fp, "      ");
		for (n=0; n<ngp; n++) {
			fprintf(fp, "%1.4e ", gausspoints[n].stress_old[2]);
		}
		fprintf(fp, "\n");
	}
	fprintf(fp, "      </DataArray>\n");

#if 0
	/* DEBUG */
  fprintf(fp, "      <DataArray type=\"Float32\" Name=\"Fu_x\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	for (e=0;e<nel;e++) {
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		fprintf(fp, "      ");
		for (n=0; n<ngp; n++) {
			fprintf(fp, "%1.4e ", gausspoints[n].Fu[0]);
		}
		fprintf(fp, "\n");
	}
	fprintf(fp, "      </DataArray>\n");
  fprintf(fp, "      <DataArray type=\"Float32\" Name=\"Fu_y\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	for (e=0;e<nel;e++) {
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		fprintf(fp, "      ");
		for (n=0; n<ngp; n++) {
			fprintf(fp, "%1.4e ", gausspoints[n].Fu[1]);
		}
		fprintf(fp, "\n");
	}
	fprintf(fp, "      </DataArray>\n");
  fprintf(fp, "      <DataArray type=\"Float32\" Name=\"Fp\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	for (e=0;e<nel;e++) {
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		fprintf(fp, "      ");
		for (n=0; n<ngp; n++) {
			fprintf(fp, "%1.4e ", gausspoints[n].Fp);
		}
		fprintf(fp, "\n");
	}
	fprintf(fp, "      </DataArray>\n");
	/* ENDDEBUG */
#endif  
	
  /* POINT-DATA HEADER - CLOSE */
	fprintf(fp, "    </PointData>\n");
	
	
	
	/* UNSTRUCTURED GRID DATA */
	fprintf(fp, "    <Cells>\n");
	
	// connectivity //
	fprintf(fp, "      <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");
	fprintf(fp,"      ");		
	for (c=0; c<npoints; c++) {
		fprintf(fp,"%d ", c);
	}
	fprintf(fp,"\n");		
	fprintf(fp, "      </DataArray>\n");
	
	// offsets //
	fprintf(fp, "      <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	fprintf(fp,"      ");		
	for (c=0; c<npoints; c++) {
		fprintf(fp,"%d ", (c+1));		
	}
	fprintf(fp,"\n");		
	fprintf(fp, "      </DataArray>\n");
	
	// types //
	fprintf(fp, "      <DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\">\n");
	fprintf(fp,"      ");		
	for (c=0; c<npoints; c++) {
		fprintf(fp,"%d ", 1);		
	}
	fprintf(fp,"\n");		
	fprintf(fp, "      </DataArray>\n");
	
	fprintf(fp, "    </Cells>\n");
	
	
	/* VTU HEADER - CLOSE */
	fprintf(fp, "    </Piece>\n");
  fprintf(fp, "  </UnstructuredGrid>\n");
	fprintf(fp, "</VTKFile>\n");
	
	fclose( fp );
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatinOutputQuadratureStokesPointsRawPVTU"
PetscErrorCode pTatinOutputQuadratureStokesPointsRawPVTU(pTatinCtx user,const char prefix[],const char name[])
{
	PetscErrorCode ierr;
	DM dau,dap;
	PetscInt nel,nen_u,nen_p,e,n,c,ngp,npoints;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	GaussPointCoefficientsStokes *gausspoints;
	FILE*	fp = NULL;
	PetscMPIInt i,nproc;
	char *sourcename;
	
	if ((fp = fopen ( name, "w")) == NULL)  {
		SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
	}
	
	dau = user->dav;
	dap = user->dap;
	ngp = user->Q->ngp;
	
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	npoints = nel * ngp;
	
	/* PVTU HEADER - OPEN */
	fprintf(fp, "<?xml version=\"1.0\"?>\n");
	fprintf(fp, "<VTKFile type=\"PUnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
	/* define size of the nodal mesh based on the cell DM */
	fprintf(fp, "  <PUnstructuredGrid GhostLevel=\"0\">\n" ); /* note overlap = 0 */
	
	/* POINT COORDS */
	fprintf(fp, "    <PPoints>\n");
	fprintf(fp, "      <PDataArray type=\"Float32\" Name=\"Points\" NumberOfComponents=\"3\"/>\n");
	fprintf(fp, "    </PPoints>\n");

	/* CELL-DATA HEADER - OPEN */
	fprintf(fp, "    <PCellData>\n");
	/* CELL-DATA HEADER - CLOSE */
	fprintf(fp, "    </PCellData>\n");
	
	/* POINT-DATA HEADER - OPEN */
	fprintf(fp, "    <PPointData>\n");
	/* POINT-DATA FIELDS */
	fprintf(fp,"      <PDataArray type=\"Int32\" Name=\"phase\" NumberOfComponents=\"1\"/>\n");
	fprintf(fp,"      <PDataArray type=\"Float32\" Name=\"eta_eff\" NumberOfComponents=\"1\"/>\n");
  fprintf(fp,"      <PDataArray type=\"Float32\" Name=\"plas_sr\" NumberOfComponents=\"1\"/>\n");
	fprintf(fp,"      <PDataArray type=\"Float32\" Name=\"plas_s\" NumberOfComponents=\"1\"/>\n");
  
	fprintf(fp,"      <PDataArray type=\"Float32\" Name=\"sxx\" NumberOfComponents=\"1\"/>\n");
  fprintf(fp,"      <PDataArray type=\"Float32\" Name=\"syy\" NumberOfComponents=\"1\"/>\n");
  fprintf(fp,"      <PDataArray type=\"Float32\" Name=\"sxy\" NumberOfComponents=\"1\"/>\n");

#if 0
	/* DEBUG */
	fprintf(fp,"      <PDataArray type=\"Float32\" Name=\"Fu_x\" NumberOfComponents=\"1\"/>\n");
  fprintf(fp,"      <PDataArray type=\"Float32\" Name=\"Fu_y\" NumberOfComponents=\"1\"/>\n");
  fprintf(fp,"      <PDataArray type=\"Float32\" Name=\"Fp\" NumberOfComponents=\"1\"/>\n");
	/* ENDDEBUG */
#endif
	
  /* POINT-DATA HEADER - CLOSE */
	fprintf(fp, "    </PPointData>\n");
	

	/* PVTU write sources */
	MPI_Comm_size(PETSC_COMM_WORLD,&nproc);
	for (i=0; i<nproc; i++) {
		asprintf( &sourcename, "%s-subdomain%1.5d.vtu", prefix, i );
		fprintf( fp, "    <Piece Source=\"%s\"/>\n",sourcename);
		free(sourcename);
	}
	
	
	/* PVTU HEADER - CLOSE */
  fprintf(fp, "  </PUnstructuredGrid>\n");
	fprintf(fp, "</VTKFile>\n");
	
	fclose( fp );
	PetscFunctionReturn(0);
}

/*
 prefix = output_path/timestepXXXX/pp_subdomainXXXXX.vtu
*/
#undef __FUNCT__  
#define __FUNCT__ "pTatinOutputParaViewQuadratureStokesPointsRaw"
PetscErrorCode pTatinOutputParaViewQuadratureStokesPointsRaw(pTatinCtx user,const char path[],const char prefix[])
{
	char *vtkfilename,*filename;
	PetscMPIInt rank;
	PetscErrorCode ierr;
	
	ierr = pTatinGenerateParallelVTKName(prefix,"vtu",&vtkfilename);CHKERRQ(ierr);
	if (path) {
		asprintf(&filename,"%s/%s",path,vtkfilename);
	} else {
		asprintf(&filename,"./%s",vtkfilename);
	}

	ierr = pTatinOutputQuadratureStokesPointsRawVTU(user,filename);CHKERRQ(ierr);
	free(filename);
	free(vtkfilename);

	ierr = pTatinGenerateVTKName(prefix,"pvtu",&vtkfilename);CHKERRQ(ierr);
	if (path) {
		asprintf(&filename,"%s/%s",path,vtkfilename);
	} else {
		asprintf(&filename,"./%s",vtkfilename);
	}

	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	if (rank==0) {
		ierr = pTatinOutputQuadratureStokesPointsRawPVTU(user,prefix,filename);CHKERRQ(ierr);
	}
	free(filename);
	free(vtkfilename);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CellIntegration"
PetscErrorCode CellIntegration(PetscScalar celldata[],PTatinField FIELD,pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[])
{	
	PetscErrorCode ierr;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
	PetscScalar GNIu[MAX_QUAD_PNTS][2][Q2_NODES_PER_EL_2D],NIp[MAX_QUAD_PNTS][P_BASIS_FUNCTIONS];
	PetscScalar NIu[MAX_QUAD_PNTS][U_BASIS_FUNCTIONS];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen_u,nen_p,e,n,k;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D],el_eta[MAX_QUAD_PNTS];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	PetscScalar Fe[Q2_NODES_PER_EL_2D*2],Be[Q2_NODES_PER_EL_2D*2];
	PetscInt vel_el_lidx[U_BASIS_FUNCTIONS*2];
	PetscInt *gidx,elgidx[2*Q2_NODES_PER_EL_2D];
	PetscInt nbcs,i,j;
	const PetscInt *ubcidx;
	Vec diag;
	PetscScalar *LA_diag;
	BCList u_bclist = user->u_bclist;
	GaussPointCoefficientsStokes *gausspoints;
	PetscScalar integral,volume;
	PetscScalar  d1,exx_gp,eyy_gp,exy_gp,eta_gp,theta_gp;
	PetscScalar  sxx_gp,syy_gp,sxy_gp,u_gp,v_gp,trace_p,pressure_gp,tau2_gp,sr2_gp,s2_gp;
  RheologyConstants *rheo;

	PetscFunctionBegin;
	
	/* quadrature */
	/*QuadratureCreateGauss_3pnt_2D(&ngp,gp_xi,gp_weight);*/
	ngp       = user->Q->ngp;
	gp_xi     = user->Q->xi;
	gp_weight = user->Q->weight;
	for (k=0; k<ngp; k++) {
		PetscScalar *xip = &gp_xi[2*k];
		
		PTatinConstructNI_Q2_2D(xip,NIu[k]);
		PTatinConstructGNI_Q2_2D(xip,GNIu[k]);
	}
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetGlobalIndices(dau,0,&gidx);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);

	ierr = PetscMemzero(celldata,sizeof(PetscScalar)*nel);CHKERRQ(ierr);
	
	for (e=0;e<nel;e++) {
		
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		ierr = StokesVelocity_GetElementLocalIndices(vel_el_lidx,(PetscInt*)&elnidx_u[nen_u*e]);CHKERRQ(ierr);
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],u);CHKERRQ(ierr);
		ierr = DMDAGetScalarElementField_2D(elp,nen_p,(PetscInt*)&elnidx_p[nen_p*e],p);CHKERRQ(ierr);
		
		/* get element velocity */
		for( i=0; i<Q2_NODES_PER_EL_2D; i++ ) {
			ux[i] = elu[2*i  ];
			uy[i] = elu[2*i+1];
			
			xc[i] = elcoords[2*i  ];
			yc[i] = elcoords[2*i+1];
		}
		/* get element pressure */
		for (n=0; n<ngp; n++) {
			PetscScalar *xip = &gp_xi[2*n];
			
			PTatinConstructNI_Q2_2D(xip,NIu[n]);
			ConstructNi_pressure(xip,elcoords,NIp[n]);
		}
		
		integral = 0.0;
		volume = 0.0;
		for (n=0; n<ngp; n++) {
			PetscScalar  J[2][2], iJ[2][2];
			PetscScalar J_p,ojp,fac;
			
			/* coord transformation */
			for( i=0; i<2; i++ ) {
				for( j=0; j<2; j++ ) { J[i][j] = 0.0; }
			}
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				J[0][0] += GNIu[n][0][k] * xc[k] ;
				J[0][1] += GNIu[n][0][k] * yc[k] ;
				
				J[1][0] += GNIu[n][1][k] * xc[k] ;
				J[1][1] += GNIu[n][1][k] * yc[k] ;
			}
			J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
			ojp = 1.0/J_p;
			iJ[0][0] =  J[1][1]*ojp;
			iJ[0][1] = -J[0][1]*ojp;
			iJ[1][0] = -J[1][0]*ojp;
			iJ[1][1] =  J[0][0]*ojp;
			
			/* global derivs at each quadrature point */
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				nx[k] = iJ[0][0]*GNIu[n][0][k] + iJ[0][1]*GNIu[n][1][k];
				ny[k] = iJ[1][0]*GNIu[n][0][k] + iJ[1][1]*GNIu[n][1][k];
			}
			
			/* u,v and strain rate (e= B u) at gp */
			u_gp = v_gp = 0.0;
			exx_gp = eyy_gp = exy_gp = 0.0;
			for( i=0; i<U_BASIS_FUNCTIONS; i++ ) {
				u_gp   += NIu[n][i] * ux[i];
				v_gp   += NIu[n][i] * uy[i];
				
				exx_gp += nx[i] * ux[i];
				eyy_gp += ny[i] * uy[i];
				exy_gp += ny[i] * ux[i] + nx[i] * uy[i];
			}
			exy_gp = 0.5 * exy_gp;
			
			/* viscosity @ gp */
			eta_gp = gausspoints[n].eta; 
			/* stress @ gp */
			d1 = 2.0 * eta_gp;
			sxx_gp = d1 * exx_gp;
			syy_gp = d1 * eyy_gp;
			sxy_gp = d1 * exy_gp;
			
      rheo = &user->rheology_constants;
      switch (rheo->rheology_type) {
        case RHEOLOGY_VISCOUS:
          break;
          
        case RHEOLOGY_VISCO_PLASTIC:
          break;
          
        case RHEOLOGY_VISCO_ELASTIC_PLASTIC:
          /* stress relax @ gp */
          theta_gp = gausspoints[n].theta;  
          sxx_gp += theta_gp*gausspoints[n].stress_old[0]; 
          syy_gp += theta_gp*gausspoints[n].stress_old[1];
          sxy_gp += theta_gp*gausspoints[n].stress_old[2];     
          break;
          
        default:
          
          break;
      }
      
      
      
      
			trace_p = 0.5 * ( sxx_gp + syy_gp );
			sxx_gp = sxx_gp - trace_p;
			syy_gp = syy_gp - trace_p;
			
			/* pressure @ gp */
			pressure_gp = 0.0;
			for( i=0; i<P_BASIS_FUNCTIONS; i++ ) {
				pressure_gp += NIp[n][i] * elp[i];
			}
			pressure_gp = pressure_gp + trace_p;
			
			/* stress_inv @ gp */
			s2_gp = sqrt( 0.5*((sxx_gp-pressure_gp)*(sxx_gp-pressure_gp) + 2.0*sxy_gp*sxy_gp + (syy_gp-pressure_gp)*(syy_gp-pressure_gp)) );
			
			/* dev stress_inv @ gp */
			tau2_gp = sqrt( 0.5*(sxx_gp*sxx_gp + 2.0*sxy_gp*sxy_gp + syy_gp*syy_gp) );

			/* strainrate_inv @ gp */
			sr2_gp = sqrt( 0.5*(exx_gp*exx_gp + 2.0*exy_gp*exy_gp + eyy_gp*eyy_gp) );

			fac = J_p * gp_weight[n];
			volume = volume + 1.0 * fac;
			switch (FIELD) {
				case PField_Pressure:
					integral += pressure_gp * fac;
					break;
				case PField_Viscosity:
					integral += eta_gp * fac;
					break;
					
				case PField_Sxx:
					integral += (sxx_gp-pressure_gp) * fac;
					break;
				case PField_Sxy:
					integral += sxy_gp * fac;
					break;
				case PField_Syy:
					integral += (syy_gp-pressure_gp) * fac;
					break;
					
				case PField_Txx:
					integral += sxx_gp * fac;
					break;
				case PField_Txy:
					integral += sxy_gp * fac;
					break;
				case PField_Tyy:
					integral += syy_gp * fac;
					break;

				case PField_Exx:
					integral += exx_gp * fac;
					break;
				case PField_Exy:
					integral += exy_gp * fac;
					break;
				case PField_Eyy:
					integral += eyy_gp * fac;
					break;

				case PField_S2:
					integral += s2_gp * fac;
					break;
				case PField_T2:
					integral += tau2_gp * fac;
					break;
				case PField_E2:
					integral += sr2_gp * fac;
					break;
					
			}
		}
		celldata[e] = integral/volume;
		
	}
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatinOutputMeshVelocityPressureVTS"
PetscErrorCode pTatinOutputMeshVelocityPressureVTS(pTatinCtx ctx,Vec X,const char name[])
{
	PetscErrorCode ierr;
	DM dau,dap;
	Vec velocity,pressure;
  Vec local_fieldsU;
  PetscScalar *LA_fieldsU;
  Vec local_fieldsP;
  PetscScalar *LA_fieldsP;
	DM cda;
	Vec gcoords;
	DMDACoor2d **LA_gcoords;	
	PetscInt mx,my,cnt;
	PetscInt ei,ej,i,j,esi,esj;
  PetscScalar *LA_cell_data;
	FILE*	vtk_fp = NULL;
	PetscInt gsi,gsj,gm,gn;
	
	if ((vtk_fp = fopen ( name, "w")) == NULL)  {
		SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
	}
	
	dau = ctx->dav;
	dap = ctx->dap;
	
	ierr = DMDAGetGhostCorners(dau,&gsi,&gsj,0,&gm,&gn,0);CHKERRQ(ierr);
	ierr = DMDAGetCornersElementQ2(dau,&esi,&esj,0,&mx,&my,0);CHKERRQ(ierr);

	ierr = DMDAGetCoordinateDA(dau,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(dau,&gcoords);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,gcoords,&LA_gcoords);CHKERRQ(ierr);

	ierr = DMCompositeGetAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);

	ierr = PetscMalloc(sizeof(PetscScalar)*mx*my,&LA_cell_data);CHKERRQ(ierr);
	
  ierr = DMGetLocalVector(dau,&local_fieldsU);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dau,velocity,INSERT_VALUES,local_fieldsU);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dau,velocity,INSERT_VALUES,local_fieldsU);CHKERRQ(ierr);
  ierr = VecGetArray(local_fieldsU,&LA_fieldsU);CHKERRQ(ierr);

  ierr = DMGetLocalVector(dap,&local_fieldsP);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dap,pressure,INSERT_VALUES,local_fieldsP);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dap,pressure,INSERT_VALUES,local_fieldsP);CHKERRQ(ierr);
  ierr = VecGetArray(local_fieldsP,&LA_fieldsP);CHKERRQ(ierr);
	
	
	/* VTS HEADER - OPEN */	
	fprintf( vtk_fp, "<VTKFile type=\"StructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
	fprintf( vtk_fp, "  <StructuredGrid WholeExtent=\"%d %d %d %d %d %d\">\n", esi,esi+2*mx+1-1, esj,esj+2*my+1-1, 0,0);
	fprintf( vtk_fp, "    <Piece Extent=\"%d %d %d %d %d %d\">\n", esi,esi+2*mx+1-1, esj,esj+2*my+1-1, 0,0);

	/* VTS COORD DATA */	
	fprintf( vtk_fp, "    <Points>\n");
	fprintf( vtk_fp, "      <DataArray Name=\"coords\" type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	for (j=esj; j<esj+2*my+1; j++) {
		for (i=esi; i<esi+2*mx+1; i++) {
			fprintf( vtk_fp,"      %1.6e %1.6e %1.6e\n", LA_gcoords[j][i].x, LA_gcoords[j][i].y, 0.0 );
		}
	}
	fprintf( vtk_fp, "      </DataArray>\n");
	fprintf( vtk_fp, "    </Points>\n");
	
	/* VTS CELL DATA */	
	fprintf( vtk_fp, "    <CellData>\n");

	/* integrated pressure */
	ierr = CellIntegration(LA_cell_data,PField_Pressure,ctx, dau,LA_fieldsU, dap,LA_fieldsP);CHKERRQ(ierr);

	fprintf( vtk_fp, "      <DataArray Name=\"int_avg pressure\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	fprintf( vtk_fp,"      ");
	for (ej=0; ej<my; ej++) { for (j=0; j<2; j++) {
		for (ei=0; ei<mx; ei++) { for (i=0; i<2; i++) {
			fprintf( vtk_fp,"%1.6e ", LA_cell_data[ei+ej*mx] );
		}}
	}}
	fprintf( vtk_fp,"\n");
	fprintf( vtk_fp, "      </DataArray>\n");

		/* =============================================================================== */
	/* integrated viscosity */
	ierr = CellIntegration(LA_cell_data,PField_Viscosity,ctx, dau,LA_fieldsU, dap,LA_fieldsP);CHKERRQ(ierr);
	
	fprintf( vtk_fp, "      <DataArray Name=\"int_avg viscosity\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	fprintf( vtk_fp,"      ");
	for (ej=0; ej<my; ej++) { for (j=0; j<2; j++) {
		for (ei=0; ei<mx; ei++) { for (i=0; i<2; i++) {
			fprintf( vtk_fp,"%1.6e ", LA_cell_data[ei+ej*mx] );
		}}
	}}
	fprintf( vtk_fp,"\n");
	fprintf( vtk_fp, "      </DataArray>\n");

	/* =============================================================================== */
	/* integrated sigma_II */
	ierr = CellIntegration(LA_cell_data,PField_S2,ctx, dau,LA_fieldsU, dap,LA_fieldsP);CHKERRQ(ierr);
	fprintf( vtk_fp, "      <DataArray Name=\"int_avg sigma_II\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	fprintf( vtk_fp,"      ");
	for (ej=0; ej<my; ej++) { for (j=0; j<2; j++) {
		for (ei=0; ei<mx; ei++) { for (i=0; i<2; i++) {
			fprintf( vtk_fp,"%1.6e ", LA_cell_data[ei+ej*mx] );
		}}
	}}
	fprintf( vtk_fp,"\n");
	fprintf( vtk_fp, "      </DataArray>\n");
	
	/* =============================================================================== */
	/* integrated tau_II */
	ierr = CellIntegration(LA_cell_data,PField_T2,ctx, dau,LA_fieldsU, dap,LA_fieldsP);CHKERRQ(ierr);
	fprintf( vtk_fp, "      <DataArray Name=\"int_avg tau_II\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	fprintf( vtk_fp,"      ");
	for (ej=0; ej<my; ej++) { for (j=0; j<2; j++) {
		for (ei=0; ei<mx; ei++) { for (i=0; i<2; i++) {
			fprintf( vtk_fp,"%1.6e ", LA_cell_data[ei+ej*mx] );
		}}
	}}
	fprintf( vtk_fp,"\n");
	fprintf( vtk_fp, "      </DataArray>\n");
	
	/* =============================================================================== */
	/* integrated epsilon_II */
	ierr = CellIntegration(LA_cell_data,PField_E2,ctx, dau,LA_fieldsU, dap,LA_fieldsP);CHKERRQ(ierr);
	fprintf( vtk_fp, "      <DataArray Name=\"int_avg epsilon_II\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	fprintf( vtk_fp,"      ");
	for (ej=0; ej<my; ej++) { for (j=0; j<2; j++) {
		for (ei=0; ei<mx; ei++) { for (i=0; i<2; i++) {
			fprintf( vtk_fp,"%1.6e ", LA_cell_data[ei+ej*mx] );
		}}
	}}
	fprintf( vtk_fp,"\n");
	fprintf( vtk_fp, "      </DataArray>\n");
    
    /* =============================================================================== */
    /* integrated epsilon_II */
    ierr = CellIntegration(LA_cell_data,PField_Sxx,ctx, dau,LA_fieldsU, dap,LA_fieldsP);CHKERRQ(ierr);
    fprintf( vtk_fp, "      <DataArray Name=\"int_avg sigma_xx\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
    fprintf( vtk_fp,"      ");
    for (ej=0; ej<my; ej++) { for (j=0; j<2; j++) {
        for (ei=0; ei<mx; ei++) { for (i=0; i<2; i++) {
            fprintf( vtk_fp,"%1.6e ", LA_cell_data[ei+ej*mx] );
        }}
    }}
    fprintf( vtk_fp,"\n");
    fprintf( vtk_fp, "      </DataArray>\n");

    /* =============================================================================== */
    /* integrated epsilon_II */
    ierr = CellIntegration(LA_cell_data,PField_Sxy,ctx, dau,LA_fieldsU, dap,LA_fieldsP);CHKERRQ(ierr);
    fprintf( vtk_fp, "      <DataArray Name=\"int_avg sigmaxy\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
    fprintf( vtk_fp,"      ");
    for (ej=0; ej<my; ej++) { for (j=0; j<2; j++) {
        for (ei=0; ei<mx; ei++) { for (i=0; i<2; i++) {
            fprintf( vtk_fp,"%1.6e ", LA_cell_data[ei+ej*mx] );
        }}
    }}
    fprintf( vtk_fp,"\n");
    fprintf( vtk_fp, "      </DataArray>\n");
    /* =============================================================================== */
    /* integrated epsilon_II */
    ierr = CellIntegration(LA_cell_data,PField_Syy,ctx, dau,LA_fieldsU, dap,LA_fieldsP);CHKERRQ(ierr);
    fprintf( vtk_fp, "      <DataArray Name=\"int_avg sigma_yy\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
    fprintf( vtk_fp,"      ");
    for (ej=0; ej<my; ej++) { for (j=0; j<2; j++) {
        for (ei=0; ei<mx; ei++) { for (i=0; i<2; i++) {
            fprintf( vtk_fp,"%1.6e ", LA_cell_data[ei+ej*mx] );
        }}
    }}
    fprintf( vtk_fp,"\n");
    fprintf( vtk_fp, "      </DataArray>\n");

	
	
	fprintf( vtk_fp, "    </CellData>\n");




	/* VTS NODAL DATA */
	fprintf( vtk_fp, "    <PointData>\n");
	/* velocity */
	fprintf( vtk_fp, "      <DataArray Name=\"velocity\" type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	for (j=esj; j<esj+2*my+1; j++) {
		for (i=esi; i<esi+2*mx+1; i++) {
			cnt = (i-gsi) + (j-gsj)*gm;
			fprintf( vtk_fp,"      %1.6e %1.6e %1.6e\n", LA_fieldsU[2*cnt+0], LA_fieldsU[2*cnt+1], 0.0 );
		}
	}
	fprintf( vtk_fp, "      </DataArray>\n");
	fprintf( vtk_fp, "    </PointData>\n");

	/* VTS HEADER - CLOSE */	
	fprintf( vtk_fp, "    </Piece>\n");
	fprintf( vtk_fp, "  </StructuredGrid>\n");
	fprintf( vtk_fp, "</VTKFile>\n");
	
  ierr = VecRestoreArray(local_fieldsU,&LA_fieldsU);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(dau,&local_fieldsU);CHKERRQ(ierr);

  ierr = VecRestoreArray(local_fieldsP,&LA_fieldsP);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(dap,&local_fieldsP);CHKERRQ(ierr);
	
	ierr = PetscFree(LA_cell_data);CHKERRQ(ierr);
	ierr = DMCompositeRestoreAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(cda,gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	fclose( vtk_fp );
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatinOutputSIMeshVelocityPressureVTS"
PetscErrorCode pTatinOutputSIMeshVelocityPressureVTS(pTatinCtx ctx,Vec X,const char name[])
{
	PetscErrorCode ierr;
	DM dau,dap;
	Vec velocity,pressure;
  Vec local_fieldsU;
  PetscScalar *LA_fieldsU;
  Vec local_fieldsP;
  PetscScalar *LA_fieldsP;
	DM cda;
	Vec gcoords;
	DMDACoor2d **LA_gcoords;	
	PetscInt mx,my,cnt;
	PetscInt ei,ej,i,j,esi,esj;
  PetscScalar *LA_cell_data;
	FILE*	vtk_fp = NULL;
	PetscInt gsi,gsj,gm,gn;
	pTatinUnits *units;
	
	if ((vtk_fp = fopen ( name, "w")) == NULL)  {
		SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
	}
	
	units = &ctx->units;
	
	dau = ctx->dav;
	dap = ctx->dap;
	
	ierr = DMDAGetGhostCorners(dau,&gsi,&gsj,0,&gm,&gn,0);CHKERRQ(ierr);
	ierr = DMDAGetCornersElementQ2(dau,&esi,&esj,0,&mx,&my,0);CHKERRQ(ierr);
	
	ierr = DMDAGetCoordinateDA(dau,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(dau,&gcoords);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMCompositeGetAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
	
	ierr = PetscMalloc(sizeof(PetscScalar)*mx*my,&LA_cell_data);CHKERRQ(ierr);
	
  ierr = DMGetLocalVector(dau,&local_fieldsU);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dau,velocity,INSERT_VALUES,local_fieldsU);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dau,velocity,INSERT_VALUES,local_fieldsU);CHKERRQ(ierr);
  ierr = VecGetArray(local_fieldsU,&LA_fieldsU);CHKERRQ(ierr);
	
  ierr = DMGetLocalVector(dap,&local_fieldsP);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dap,pressure,INSERT_VALUES,local_fieldsP);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dap,pressure,INSERT_VALUES,local_fieldsP);CHKERRQ(ierr);
  ierr = VecGetArray(local_fieldsP,&LA_fieldsP);CHKERRQ(ierr);
	
	
	/* VTS HEADER - OPEN */	
	fprintf( vtk_fp, "<VTKFile type=\"StructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
	fprintf( vtk_fp, "  <StructuredGrid WholeExtent=\"%d %d %d %d %d %d\">\n", esi,esi+2*mx+1-1, esj,esj+2*my+1-1, 0,0);
	fprintf( vtk_fp, "    <Piece Extent=\"%d %d %d %d %d %d\">\n", esi,esi+2*mx+1-1, esj,esj+2*my+1-1, 0,0);
	
	/* VTS COORD DATA */	
	fprintf( vtk_fp, "    <Points>\n");
	fprintf( vtk_fp, "      <DataArray Name=\"coords (%s)\" type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\">\n",units->si_length->unit_name);
	for (j=esj; j<esj+2*my+1; j++) {
		for (i=esi; i<esi+2*mx+1; i++) {
			double xcoor,ycoor;
			
			xcoor = (double)LA_gcoords[j][i].x;
			ycoor = (double)LA_gcoords[j][i].y;
			
			UnitsApplyScaling(units->si_length,xcoor,&xcoor);
			UnitsApplyScaling(units->si_length,ycoor,&ycoor);
			
			fprintf( vtk_fp,"      %1.6e %1.6e %1.6e\n", xcoor, ycoor, 0.0 );
		}
	}
	fprintf( vtk_fp, "      </DataArray>\n");
	fprintf( vtk_fp, "    </Points>\n");
	
	/* VTS CELL DATA */	
	fprintf( vtk_fp, "    <CellData>\n");
	//================================================================
	/* integrated pressure */
	ierr = CellIntegration(LA_cell_data,PField_Pressure,ctx, dau,LA_fieldsU, dap,LA_fieldsP);CHKERRQ(ierr);
	fprintf( vtk_fp, "      <DataArray Name=\"int_avg pressure (%s)\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n",units->si_stress->unit_name);
	fprintf( vtk_fp,"      ");
	for (ej=0; ej<my; ej++) { for (j=0; j<2; j++) {
		for (ei=0; ei<mx; ei++) { for (i=0; i<2; i++) {
			double pressure;
			
			pressure = (double)LA_cell_data[ei+ej*mx];
			UnitsApplyScaling(units->si_stress,pressure,&pressure);
			
			fprintf( vtk_fp,"%1.6e ", pressure );
		}}
	}}
	fprintf( vtk_fp,"\n");
	fprintf( vtk_fp, "      </DataArray>\n");
	
		/* =============================================================================== */
	/* integrated viscosity */
	ierr = CellIntegration(LA_cell_data,PField_Viscosity,ctx, dau,LA_fieldsU, dap,LA_fieldsP);CHKERRQ(ierr);
	
	fprintf( vtk_fp, "      <DataArray Name=\"int_avg viscosity (%s)\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n",units->si_viscosity->unit_name);
	fprintf( vtk_fp,"      ");
	for (ej=0; ej<my; ej++) { for (j=0; j<2; j++) {
		for (ei=0; ei<mx; ei++) { for (i=0; i<2; i++) {
		double viscosity; 
		viscosity= (double)LA_cell_data[ei+ej*mx];
		UnitsApplyScaling(units->si_viscosity,viscosity,&viscosity);
			fprintf( vtk_fp,"%1.6e ", viscosity );
		}}
	}}
	fprintf( vtk_fp,"\n");
	fprintf( vtk_fp, "      </DataArray>\n");

	/* =============================================================================== */
	/* integrated sigma_II */
	ierr = CellIntegration(LA_cell_data,PField_S2,ctx, dau,LA_fieldsU, dap,LA_fieldsP);CHKERRQ(ierr);
	fprintf( vtk_fp, "      <DataArray Name=\"int_avg sigma_II (%s)\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n",units->si_stress->unit_name);
	fprintf( vtk_fp,"      ");
	for (ej=0; ej<my; ej++) { for (j=0; j<2; j++) {
		for (ei=0; ei<mx; ei++) { for (i=0; i<2; i++) {
		     double pressure; 
		     pressure = (double)LA_cell_data[ei+ej*mx];
			UnitsApplyScaling(units->si_stress,pressure,&pressure);
			fprintf( vtk_fp,"%1.6e ", pressure );
		}}
	}}
	fprintf( vtk_fp,"\n");
	fprintf( vtk_fp, "      </DataArray>\n");
	
	/* =============================================================================== */
	/* integrated tau_II */
	ierr = CellIntegration(LA_cell_data,PField_T2,ctx, dau,LA_fieldsU, dap,LA_fieldsP);CHKERRQ(ierr);
	fprintf( vtk_fp, "      <DataArray Name=\"int_avg tau_II (%s)\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n",units->si_stress->unit_name);
	fprintf( vtk_fp,"      ");
	for (ej=0; ej<my; ej++) { for (j=0; j<2; j++) {
		for (ei=0; ei<mx; ei++) { for (i=0; i<2; i++) {
			 double pressure; 
		     pressure = (double)LA_cell_data[ei+ej*mx];
			UnitsApplyScaling(units->si_stress,pressure,&pressure);
			fprintf( vtk_fp,"%1.6e ", pressure );
		}}
	}}
	fprintf( vtk_fp,"\n");
	fprintf( vtk_fp, "      </DataArray>\n");
	
	/* =============================================================================== */
	/* integrated epsilon_II */
	ierr = CellIntegration(LA_cell_data,PField_E2,ctx, dau,LA_fieldsU, dap,LA_fieldsP);CHKERRQ(ierr);
	fprintf( vtk_fp, "      <DataArray Name=\"int_avg epsilon_II(%s)\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n",units->si_strainrate->unit_name);
	fprintf( vtk_fp,"      ");
	for (ej=0; ej<my; ej++) { for (j=0; j<2; j++) {
		for (ei=0; ei<mx; ei++) { for (i=0; i<2; i++) {
			 double pressure; 
		     pressure = (double)LA_cell_data[ei+ej*mx];
			UnitsApplyScaling(units->si_strainrate,pressure,&pressure);
			fprintf( vtk_fp,"%1.6e ", pressure );
		}}
	}}
	fprintf( vtk_fp,"\n");
	fprintf( vtk_fp, "      </DataArray>\n");

	
    /* =============================================================================== */
    /* integrated epsilon_II */
    ierr = CellIntegration(LA_cell_data,PField_Sxx,ctx, dau,LA_fieldsU, dap,LA_fieldsP);CHKERRQ(ierr);
    fprintf( vtk_fp, "      <DataArray Name=\"int_avg sigma_xx(%s)\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n",units->si_stress->unit_name);
    fprintf( vtk_fp,"      ");
    for (ej=0; ej<my; ej++) { for (j=0; j<2; j++) {
        for (ei=0; ei<mx; ei++) { for (i=0; i<2; i++) {
            double pressure;
            pressure = (double)LA_cell_data[ei+ej*mx];
            UnitsApplyScaling(units->si_stress,pressure,&pressure);
            fprintf( vtk_fp,"%1.6e ", pressure );
        }}
    }}
    fprintf( vtk_fp,"\n");
    fprintf( vtk_fp, "      </DataArray>\n");
    
    /* =============================================================================== */
    /* integrated epsilon_II */
    ierr = CellIntegration(LA_cell_data,PField_Sxy,ctx, dau,LA_fieldsU, dap,LA_fieldsP);CHKERRQ(ierr);
    fprintf( vtk_fp, "      <DataArray Name=\"int_avg sigmaxy(%s)\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n",units->si_stress->unit_name);
    fprintf( vtk_fp,"      ");
    for (ej=0; ej<my; ej++) { for (j=0; j<2; j++) {
        for (ei=0; ei<mx; ei++) { for (i=0; i<2; i++) {
            double pressure;
            pressure = (double)LA_cell_data[ei+ej*mx];
            UnitsApplyScaling(units->si_stress,pressure,&pressure);
            fprintf( vtk_fp,"%1.6e ", pressure );
        }}
    }}
    fprintf( vtk_fp,"\n");
    fprintf( vtk_fp, "      </DataArray>\n");
    /* =============================================================================== */
    /* integrated epsilon_II */
    ierr = CellIntegration(LA_cell_data,PField_Syy,ctx, dau,LA_fieldsU, dap,LA_fieldsP);CHKERRQ(ierr);
    fprintf( vtk_fp, "      <DataArray Name=\"int_avg sigma_yy(%s)\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n",units->si_stress->unit_name);
    fprintf( vtk_fp,"      ");
    for (ej=0; ej<my; ej++) { for (j=0; j<2; j++) {
        for (ei=0; ei<mx; ei++) { for (i=0; i<2; i++) {
            double pressure;
            pressure = (double)LA_cell_data[ei+ej*mx];
            UnitsApplyScaling(units->si_stress,pressure,&pressure);
            fprintf( vtk_fp,"%1.6e ", pressure );
        }}
    }}
    fprintf( vtk_fp,"\n");
    fprintf( vtk_fp, "      </DataArray>\n");
    
    
    
    
	fprintf( vtk_fp, "    </CellData>\n");
	
	/* VTS NODAL DATA */
	fprintf( vtk_fp, "    <PointData>\n");
	/* velocity */
	fprintf( vtk_fp, "      <DataArray Name=\"velocity (%s)\" type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\">\n",units->si_velocity->unit_name);
	for (j=esj; j<esj+2*my+1; j++) {
		for (i=esi; i<esi+2*mx+1; i++) {
			double xvel,yvel;
			
			cnt = (i-gsi) + (j-gsj)*gm;
			xvel = (double)LA_fieldsU[2*cnt+0];
			yvel = (double)LA_fieldsU[2*cnt+1];
			
			UnitsApplyScaling(units->si_velocity,xvel,&xvel);
			UnitsApplyScaling(units->si_velocity,yvel,&yvel);
			
			fprintf( vtk_fp,"      %1.6e %1.6e %1.6e\n", xvel, yvel, 0.0 );
		}
	}
	fprintf( vtk_fp, "      </DataArray>\n");
	fprintf( vtk_fp, "    </PointData>\n");
	
	/* VTS HEADER - CLOSE */	
	fprintf( vtk_fp, "    </Piece>\n");
	fprintf( vtk_fp, "  </StructuredGrid>\n");
	fprintf( vtk_fp, "</VTKFile>\n");
	
  ierr = VecRestoreArray(local_fieldsU,&LA_fieldsU);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(dau,&local_fieldsU);CHKERRQ(ierr);
	
  ierr = VecRestoreArray(local_fieldsP,&LA_fieldsP);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(dap,&local_fieldsP);CHKERRQ(ierr);
	
	ierr = PetscFree(LA_cell_data);CHKERRQ(ierr);
	ierr = DMCompositeRestoreAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(cda,gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	fclose( vtk_fp );
	PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "DAViewVTK_write_PieceExtend"
PetscErrorCode DAViewVTK_write_PieceExtend( FILE *vtk_fp, int indent_level, DM da, const char local_file_prefix[] )
{
	PetscMPIInt nproc,rank;
	MPI_Comm comm;
	const PetscInt *lx,*ly,*lz;
	PetscInt M,N,P,pM,pN,pP,sum,*olx,*oly,*olz;
	PetscInt *osx,*osy,*osz,*oex,*oey,*oez;
	PetscInt i,j,k,II,stencil;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	/* create file name */
	PetscObjectGetComm( (PetscObject)da, &comm );
	MPI_Comm_size( comm, &nproc );
	MPI_Comm_rank( comm, &rank );
	
	ierr = DMDAGetInfo( da, 0, &M,&N,&P, &pM,&pN,&pP, 0, &stencil, 0,0,0, 0 );CHKERRQ(ierr);
	ierr = DMDAGetOwnershipRanges( da,&lx,&ly,&lz );CHKERRQ(ierr);
	
	/*generate start,end list */
	ierr = PetscMalloc( sizeof(PetscInt)*(pM+1), &olx );CHKERRQ(ierr);
	ierr = PetscMalloc( sizeof(PetscInt)*(pN+1), &oly );CHKERRQ(ierr);
	ierr = PetscMalloc( sizeof(PetscInt)*(pP+1), &olz );CHKERRQ(ierr);
	sum = 0;
	for( i=0; i<pM+1; i++ ) {
		olx[i] = sum;
		sum = sum + lx[i];
	}
	sum = 0;
	for( i=0; i<pN+1; i++ ) {
		oly[i] = sum;
		sum = sum + ly[i];
	}
	sum = 0;
	for( i=0; i<pP+1; i++ ) {
		olz[i] = sum;
		sum = sum + lz[i];
	}
	ierr = PetscMalloc( sizeof(PetscInt)*(pM), &osx );CHKERRQ(ierr);
	ierr = PetscMalloc( sizeof(PetscInt)*(pN), &osy );CHKERRQ(ierr);
	ierr = PetscMalloc( sizeof(PetscInt)*(pP), &osz );CHKERRQ(ierr);
	ierr = PetscMalloc( sizeof(PetscInt)*(pM), &oex );CHKERRQ(ierr);
	ierr = PetscMalloc( sizeof(PetscInt)*(pN), &oey );CHKERRQ(ierr);
	ierr = PetscMalloc( sizeof(PetscInt)*(pP), &oez );CHKERRQ(ierr);
	for( i=0; i<pM; i++ ) {
		osx[i] = olx[i]   - stencil;
		oex[i] = olx[i] + lx[i] + stencil;
		if(osx[i]<0)osx[i]=0;
		if(oex[i]>M)oex[i]=M;
	}
	
	for( i=0; i<pN; i++ ) {
		osy[i] = oly[i]   - stencil;
		oey[i] = oly[i] + ly[i] + stencil;
		if(osy[i]<0)osy[i]=0;
		if(oey[i]>M)oey[i]=N;
	}
	for( i=0; i<pP; i++ ) {
		osz[i] = olz[i]   - stencil;
		oez[i] = olz[i] + lz[i] + stencil;
		if(osz[i]<0)osz[i]=0;
		if(oez[i]>P)oez[i]=P;
	}
	
	
	
	if (pP != -1) {
		for( k=0;k<pP;k++ ) {
			for( j=0;j<pN;j++ ) {
				for( i=0;i<pM;i++ ) {
					char *name;
					PetscInt procid = i + j*pM + k*pM*pN; /* convert proc(i,j,k) to pid */
					asprintf( &name, "%s-subdomain%1.5d.vts", local_file_prefix, procid );
					for( II=0; II<indent_level; II++ ) {
						fprintf(vtk_fp,"  ");
					}
					fprintf( vtk_fp, "<Piece Extent=\"%d %d %d %d %d %d\"      Source=\"%s\"/>\n",
									osx[i],oex[i]-1,
									osy[j],oey[j]-1,
									osz[k],oez[k]-1, name);
					free(name);
				}
			}
		}
	} else {
		for( j=0;j<pN;j++ ) {
			for( i=0;i<pM;i++ ) {
				char *name;
				PetscInt procid = i + j*pM; /* convert proc(i,j,k) to pid */
				asprintf( &name, "%s-subdomain%1.5d.vts", local_file_prefix, procid );
				for( II=0; II<indent_level; II++ ) {
					fprintf(vtk_fp,"  ");
				}
				fprintf( vtk_fp, "<Piece Extent=\"%d %d %d %d 0 0\"      Source=\"%s\"/>\n",
								osx[i],oex[i]-1,
								osy[j],oey[j]-1, name);
				free(name);
			}
		}
	}
	
	ierr = PetscFree( olx );CHKERRQ(ierr);
	ierr = PetscFree( oly );CHKERRQ(ierr);
	ierr = PetscFree( olz );CHKERRQ(ierr);
	ierr = PetscFree( osx );CHKERRQ(ierr);
	ierr = PetscFree( osy );CHKERRQ(ierr);
	ierr = PetscFree( osz );CHKERRQ(ierr);
	ierr = PetscFree( oex );CHKERRQ(ierr);
	ierr = PetscFree( oey );CHKERRQ(ierr);
	ierr = PetscFree( oez );CHKERRQ(ierr);
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatinOutputMeshVelocityPressurePVTS"
PetscErrorCode pTatinOutputMeshVelocityPressurePVTS(pTatinCtx ctx,PetscBool use_si_units,const char prefix[],const char name[])
{
	PetscErrorCode ierr;
	DM dau,dap;
	Vec velocity,pressure;
  Vec local_fieldsU;
  PetscScalar *LA_fieldsU;
  Vec local_fieldsP;
  PetscScalar *LA_fieldsP;
	DM cda;
	Vec gcoords;
	DMDACoor2d **LA_gcoords;	
	PetscInt mx,my,cnt;
	PetscInt ei,ej,i,j,esi,esj;
  PetscScalar *LA_cell_data;
	FILE*	vtk_fp = NULL;
	PetscInt M,N,P,swidth;
	PetscMPIInt rank;
	
	
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	vtk_fp = NULL;
	if (rank==0) {
		if ((vtk_fp = fopen ( name, "w")) == NULL)  {
			SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
		}
	}
	
	
	dau = ctx->dav;
	dap = ctx->dap;
	
	/* VTS HEADER - OPEN */	
	if(vtk_fp) fprintf( vtk_fp, "<?xml version=\"1.0\"?>\n");
	if(vtk_fp) fprintf( vtk_fp, "<VTKFile type=\"PStructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");

	DMDAGetInfo( dau, 0, &M,&N,&P, 0,0,0, 0,&swidth, 0,0,0, 0 );
	if(vtk_fp) fprintf( vtk_fp, "  <PStructuredGrid GhostLevel=\"%d\" WholeExtent=\"%d %d %d %d %d %d\">\n", swidth, 0,M-1, 0,N-1, 0,P-1 ); /* note overlap = 1 for Q1 */
//	fprintf( vtk_fp, "    <Piece Extent=\"%d %d %d %d %d %d\">\n", esi,esi+2*mx+1-1, esj,esj+2*my+1-1, 0,0);
	
	/* VTS COORD DATA */	
	if(vtk_fp) fprintf( vtk_fp, "    <PPoints>\n");
	if (use_si_units == PETSC_FALSE) {
		if(vtk_fp) fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\"/>\n");
	} else {
		if(vtk_fp) fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"Points (%s)\" NumberOfComponents=\"3\"/>\n",ctx->units.si_length->unit_name);
	}
	if(vtk_fp) fprintf( vtk_fp, "    </PPoints>\n");

	
	/* VTS CELL DATA */	
	if(vtk_fp) fprintf( vtk_fp, "    <PCellData>\n");
	if (use_si_units == PETSC_FALSE) {
		if(vtk_fp) fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"int_avg pressure\" NumberOfComponents=\"1\"/>\n");
        if(vtk_fp) fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"int_avg viscosity\" NumberOfComponents=\"1\"/>\n");
        if(vtk_fp)fprintf( vtk_fp, "       <PDataArray type=\"Float64\" Name=\"int_avg sigma_II\"  NumberOfComponents=\"1\"/>\n");
        if(vtk_fp)fprintf( vtk_fp, "       <PDataArray type=\"Float64\" Name=\"int_avg tau_II\"    NumberOfComponents=\"1\"/>\n");
        if(vtk_fp)fprintf( vtk_fp, "       <PDataArray type=\"Float64\" Name=\"int_avg epsilon_II\" NumberOfComponents=\"1\"/>\n");

	} else {
		if(vtk_fp) fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"int_avg pressure (%s)\"  NumberOfComponents=\"1\"/>\n",ctx->units.si_stress->unit_name);
	    if(vtk_fp) fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"int_avg viscosity (%s)\" NumberOfComponents=\"1\"/>\n",ctx->units.si_viscosity->unit_name);
		if(vtk_fp)fprintf( vtk_fp, "       <PDataArray type=\"Float64\" Name=\"int_avg sigma_II (%s)\"  NumberOfComponents=\"1\"/>\n",ctx->units.si_stress->unit_name);
		if(vtk_fp)fprintf( vtk_fp, "       <PDataArray type=\"Float64\" Name=\"int_avg tau_II (%s)\"    NumberOfComponents=\"1\"/>\n",ctx->units.si_stress->unit_name);
		if(vtk_fp)fprintf( vtk_fp, "       <PDataArray type=\"Float64\" Name=\"int_avg epsilon_II(%s)\" NumberOfComponents=\"1\"/>\n",ctx->units.si_strainrate->unit_name);
        if(vtk_fp) fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"int_avg sigma_xx (%s)\"  NumberOfComponents=\"1\"/>\n",ctx->units.si_stress->unit_name);
        if(vtk_fp) fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"int_avg sigma_xy (%s)\"  NumberOfComponents=\"1\"/>\n",ctx->units.si_stress->unit_name);
        if(vtk_fp) fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"int_avg sigma_yy (%s)\"  NumberOfComponents=\"1\"/>\n",ctx->units.si_stress->unit_name);

	}
	if(vtk_fp) fprintf( vtk_fp, "    </PCellData>\n");
	
	
	/* VTS NODAL DATA */
	if(vtk_fp) fprintf( vtk_fp, "    <PPointData>\n");
	if (use_si_units == PETSC_FALSE) {
		if(vtk_fp) fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"velocity\" NumberOfComponents=\"3\"/>\n");
	} else {
		if(vtk_fp) fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"velocity (%s)\" NumberOfComponents=\"3\"/>\n",ctx->units.si_velocity->unit_name);
	}
	if(vtk_fp) fprintf( vtk_fp, "    </PPointData>\n");
	
	/* write out the parallel information */
	//DAViewVTK_write_PieceExtend(vtk_fp,2,dau,prefix);
	ierr = DAQ2PieceExtendForGhostLevelZero(vtk_fp,2,dau,prefix);CHKERRQ(ierr);

	
	/* VTS HEADER - CLOSE */	
	if(vtk_fp) fprintf( vtk_fp, "  </PStructuredGrid>\n");
	if(vtk_fp) fprintf( vtk_fp, "</VTKFile>\n");
	
	if(vtk_fp) fclose( vtk_fp );
	PetscFunctionReturn(0);
}

/*
 prefix = output_path/timestepXXXX/pp_subdomainXXXXX.vtu
 */
#undef __FUNCT__  
#define __FUNCT__ "pTatinOutputParaViewMeshVelocityPressure"
PetscErrorCode pTatinOutputParaViewMeshVelocityPressure(pTatinCtx user,Vec X,const char path[],const char prefix[])
{
	char *vtkfilename,*filename;
	PetscMPIInt rank;
	PetscBool use_dimensional_units = PETSC_FALSE;
	PetscErrorCode ierr;
	
	ierr = PetscOptionsGetBool(PETSC_NULL,"-use_dimensional_units",&use_dimensional_units,PETSC_NULL);CHKERRQ(ierr);
	
	ierr = pTatinGenerateParallelVTKName(prefix,"vts",&vtkfilename);CHKERRQ(ierr);
	if (path) {
		asprintf(&filename,"%s/%s",path,vtkfilename);
	} else {
		asprintf(&filename,"./%s",vtkfilename);
	}
	
	if (use_dimensional_units == PETSC_FALSE) {
		ierr = pTatinOutputMeshVelocityPressureVTS(user,X,filename);CHKERRQ(ierr);
	} else {
		ierr = pTatinOutputSIMeshVelocityPressureVTS(user,X,filename);CHKERRQ(ierr);
	}
	free(filename);
	free(vtkfilename);
		
	ierr = pTatinGenerateVTKName(prefix,"pvts",&vtkfilename);CHKERRQ(ierr);
	if (path) {
		asprintf(&filename,"%s/%s",path,vtkfilename);
	} else {
		asprintf(&filename,"./%s",vtkfilename);
	}
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
//	if (rank==0) {
		ierr = pTatinOutputMeshVelocityPressurePVTS(user,use_dimensional_units,prefix,filename);CHKERRQ(ierr);
//	}
	free(filename);
	free(vtkfilename);
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "pTatinOutputQuadratureStokesPointsIntegratedFieldsVTS"
PetscErrorCode pTatinOutputQuadratureStokesPointsIntegratedFieldsVTS(pTatinCtx ctx,PTatinField fieldlist[],Vec X,const char name[])
{
	PetscErrorCode ierr;
	DM dau,dap;
	Vec velocity,pressure;
  Vec local_fieldsU;
  PetscScalar *LA_fieldsU;
  Vec local_fieldsP;
  PetscScalar *LA_fieldsP;
	DM cda;
	Vec gcoords;
	DMDACoor2d **LA_gcoords;	
	PetscInt mx,my,cnt,field_idx;
	PetscInt ei,ej,i,j,esi,esj;
  PetscScalar *LA_cell_data;
	FILE*	vtk_fp = NULL;
	
	if ((vtk_fp = fopen ( name, "w")) == NULL)  {
		SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
	}
	
	dau = ctx->dav;
	dap = ctx->dap;
	
	ierr = DMDAGetCornersElementQ2(dau,&esi,&esj,0,&mx,&my,0);CHKERRQ(ierr);
	
	ierr = DMDAGetCoordinateDA(dau,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(dau,&gcoords);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMCompositeGetAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
	
	ierr = PetscMalloc(sizeof(PetscScalar)*mx*my,&LA_cell_data);CHKERRQ(ierr);
	
  ierr = DMGetLocalVector(dau,&local_fieldsU);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dau,velocity,INSERT_VALUES,local_fieldsU);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dau,velocity,INSERT_VALUES,local_fieldsU);CHKERRQ(ierr);
  ierr = VecGetArray(local_fieldsU,&LA_fieldsU);CHKERRQ(ierr);
	
  ierr = DMGetLocalVector(dap,&local_fieldsP);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dap,pressure,INSERT_VALUES,local_fieldsP);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dap,pressure,INSERT_VALUES,local_fieldsP);CHKERRQ(ierr);
  ierr = VecGetArray(local_fieldsP,&LA_fieldsP);CHKERRQ(ierr);
	
	
	/* VTS HEADER - OPEN */	
	fprintf( vtk_fp, "<VTKFile type=\"StructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
	fprintf( vtk_fp, "  <StructuredGrid WholeExtent=\"%d %d %d %d %d %d\">\n", esi,esi+2*mx+1-1, esj,esj+2*my+1-1, 0,0);
	fprintf( vtk_fp, "    <Piece Extent=\"%d %d %d %d %d %d\">\n", esi,esi+2*mx+1-1, esj,esj+2*my+1-1, 0,0);

	/* VTS COORD DATA */	
	fprintf( vtk_fp, "    <Points>\n");
	fprintf( vtk_fp, "      <DataArray Name=\"coords\" type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	for (j=esj; j<esj+2*my+1; j++) {
		for (i=esi; i<esi+2*mx+1; i++) {
			fprintf( vtk_fp,"      %1.6e %1.6e %1.6e\n", LA_gcoords[j][i].x, LA_gcoords[j][i].y, 0.0 );
		}
	}
	fprintf( vtk_fp, "      </DataArray>\n");
	fprintf( vtk_fp, "    </Points>\n");
	
	/* VTS CELL DATA */	
	fprintf( vtk_fp, "    <CellData>\n");

#if 0
	/* =============================================================================== */
	/* integrated viscosity */
	ierr = CellIntegration(LA_cell_data,PField_Viscosity,ctx, dau,LA_fieldsU, dap,LA_fieldsP);CHKERRQ(ierr);
	
	fprintf( vtk_fp, "      <DataArray Name=\"int_avg viscosity\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	fprintf( vtk_fp,"      ");
	for (ej=0; ej<my; ej++) { for (j=0; j<2; j++) {
		for (ei=0; ei<mx; ei++) { for (i=0; i<2; i++) {
			fprintf( vtk_fp,"%1.6e ", LA_cell_data[ei+ej*mx] );
		}}
	}}
	fprintf( vtk_fp,"\n");
	fprintf( vtk_fp, "      </DataArray>\n");

	/* =============================================================================== */
	/* integrated sigma_II */
	ierr = CellIntegration(LA_cell_data,PField_S2,ctx, dau,LA_fieldsU, dap,LA_fieldsP);CHKERRQ(ierr);
	fprintf( vtk_fp, "      <DataArray Name=\"int_avg sigma_II\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	fprintf( vtk_fp,"      ");
	for (ej=0; ej<my; ej++) { for (j=0; j<2; j++) {
		for (ei=0; ei<mx; ei++) { for (i=0; i<2; i++) {
			fprintf( vtk_fp,"%1.6e ", LA_cell_data[ei+ej*mx] );
		}}
	}}
	fprintf( vtk_fp,"\n");
	fprintf( vtk_fp, "      </DataArray>\n");
	
	/* =============================================================================== */
	/* integrated tau_II */
	ierr = CellIntegration(LA_cell_data,PField_T2,ctx, dau,LA_fieldsU, dap,LA_fieldsP);CHKERRQ(ierr);
	fprintf( vtk_fp, "      <DataArray Name=\"int_avg tau_II\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	fprintf( vtk_fp,"      ");
	for (ej=0; ej<my; ej++) { for (j=0; j<2; j++) {
		for (ei=0; ei<mx; ei++) { for (i=0; i<2; i++) {
			fprintf( vtk_fp,"%1.6e ", LA_cell_data[ei+ej*mx] );
		}}
	}}
	fprintf( vtk_fp,"\n");
	fprintf( vtk_fp, "      </DataArray>\n");
	
	/* =============================================================================== */
	/* integrated epsilon_II */
	ierr = CellIntegration(LA_cell_data,PField_E2,ctx, dau,LA_fieldsU, dap,LA_fieldsP);CHKERRQ(ierr);
	fprintf( vtk_fp, "      <DataArray Name=\"int_avg epsilon_II\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	fprintf( vtk_fp,"      ");
	for (ej=0; ej<my; ej++) { for (j=0; j<2; j++) {
		for (ei=0; ei<mx; ei++) { for (i=0; i<2; i++) {
			fprintf( vtk_fp,"%1.6e ", LA_cell_data[ei+ej*mx] );
		}}
	}}
	fprintf( vtk_fp,"\n");
	fprintf( vtk_fp, "      </DataArray>\n");
	/* =============================================================================== */
#endif
	
	for (field_idx=0; field_idx<__PField_TERMINATOR__; field_idx++) {
		if (fieldlist[field_idx]==-1) { continue; }
		
		ierr = CellIntegration(LA_cell_data, fieldlist[field_idx] ,ctx, dau,LA_fieldsU, dap,LA_fieldsP);CHKERRQ(ierr);
		fprintf( vtk_fp, "      <DataArray Name=\"%s\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n",PTatinFieldNames[field_idx]);
		fprintf( vtk_fp,"      ");
		for (ej=0; ej<my; ej++) { for (j=0; j<2; j++) {
			for (ei=0; ei<mx; ei++) { for (i=0; i<2; i++) {
				fprintf( vtk_fp,"%1.6e ", LA_cell_data[ei+ej*mx] );
			}}
		}}
		fprintf( vtk_fp,"\n");
		fprintf( vtk_fp, "      </DataArray>\n");
	}
	
	
	fprintf( vtk_fp, "    </CellData>\n");
	
	/* VTS NODAL DATA */
	fprintf( vtk_fp, "    <PointData>\n");
	fprintf( vtk_fp, "    </PointData>\n");
	
	/* VTS HEADER - CLOSE */	
	fprintf( vtk_fp, "    </Piece>\n");
	fprintf( vtk_fp, "  </StructuredGrid>\n");
	fprintf( vtk_fp, "</VTKFile>\n");
	
  ierr = VecRestoreArray(local_fieldsU,&LA_fieldsU);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(dau,&local_fieldsU);CHKERRQ(ierr);

  ierr = VecRestoreArray(local_fieldsP,&LA_fieldsP);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(dap,&local_fieldsP);CHKERRQ(ierr);
	
	ierr = PetscFree(LA_cell_data);CHKERRQ(ierr);
	ierr = DMCompositeRestoreAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(cda,gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	fclose( vtk_fp );
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DAQ2PieceExtendForGhostLevelZero"
PetscErrorCode DAQ2PieceExtendForGhostLevelZero( FILE *vtk_fp, int indent_level, DM dau, const char local_file_prefix[] )
{
	PetscMPIInt nproc,rank;
	MPI_Comm comm;
	const PetscInt *lx,*ly,*lz;
	PetscInt M,N,P,pM,pN,pP,sum;
	PetscInt i,j,k,II,dim,esi,esj,esk,mx,my,mz;
	PetscInt *olx,*oly,*olz;
	PetscInt *lmx,*lmy,*lmz,*tmp;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	/* create file name */
	PetscObjectGetComm( (PetscObject)dau, &comm );
	MPI_Comm_size( comm, &nproc );
	MPI_Comm_rank( comm, &rank );
	
	ierr = DMDAGetInfo( dau, &dim, &M,&N,&P, &pM,&pN,&pP, 0, 0, 0,0,0, 0 );CHKERRQ(ierr);
	ierr = DMDAGetOwnershipRangesElementQ2(dau,&pM,&pN,&pP,&olx,&oly,&olz,&lmx,&lmy,&lmz);CHKERRQ(ierr);

	/*
	if (dim >= 1) {
		for (i=0; i<pM; i++) {
			printf("I=%d v = %d mx = %d \n", i, olx[i],lmx[i] );
		}
	}
	if (dim >= 2) {
		for (i=0; i<pN; i++) {
			printf("J=%d v = %d my = %d \n", i, oly[i],lmy[i] );
		}
	}
	if (dim==3) {
		for (i=0; i<pP; i++) {
			printf("K=%d v = %d my = %d \n", i, olz[i],lmz[i] );
		}
	}
	*/
	 
	if (dim==3) {
		for( k=0;k<pP;k++ ) {
			for( j=0;j<pN;j++ ) {
				for( i=0;i<pM;i++ ) {
					char *name;
					PetscInt procid = i + j*pM + k*pM*pN; /* convert proc(i,j,k) to pid */
					asprintf( &name, "%s-subdomain%1.5d.vts", local_file_prefix, procid );
					for( II=0; II<indent_level; II++ ) {
						if(vtk_fp) fprintf(vtk_fp,"  ");
					}
					if(vtk_fp) fprintf( vtk_fp, "<Piece Extent=\"%d %d %d %d %d %d\"      Source=\"%s\"/>\n",
									olx[i],olx[i]+lmx[i]*2,
									oly[j],oly[j]+lmy[j]*2,
									olz[k],olz[k]+lmz[k]*2,
									name);
					free(name);
				}
			}
		}
	} else if (dim==2) {
		for( j=0;j<pN;j++ ) {
			for( i=0;i<pM;i++ ) {
				char *name;
				PetscInt procid = i + j*pM; /* convert proc(i,j,k) to pid */
				asprintf( &name, "%s-subdomain%1.5d.vts", local_file_prefix, procid );
				for( II=0; II<indent_level; II++ ) {
					if(vtk_fp) fprintf(vtk_fp,"  ");
				}
				if(vtk_fp) fprintf( vtk_fp, "<Piece Extent=\"%d %d %d %d 0 0\"      Source=\"%s\"/>\n",
								olx[i],olx[i]+lmx[i]*2,
								oly[j],oly[j]+lmy[j]*2,
								name);
				free(name);
			}
		}
	}
	ierr = PetscFree(olx);CHKERRQ(ierr);
	ierr = PetscFree(oly);CHKERRQ(ierr);
	ierr = PetscFree(olz);CHKERRQ(ierr);

	ierr = PetscFree(lmx);CHKERRQ(ierr);
	ierr = PetscFree(lmy);CHKERRQ(ierr);
	ierr = PetscFree(lmz);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatinOutputQuadratureStokesPointsIntegratedFieldsPVTS"
PetscErrorCode pTatinOutputQuadratureStokesPointsIntegratedFieldsPVTS(pTatinCtx ctx,PTatinField fieldlist[],const char prefix[],const char name[])
{
	PetscErrorCode ierr;
	DM dau,dap;
	FILE*	vtk_fp = NULL;
	PetscInt M,N,P,swidth,k;
	PetscMPIInt rank;
	
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	vtk_fp = NULL;
	if (rank==0) {
		if ((vtk_fp = fopen ( name, "w")) == NULL)  {
			SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
		}
	}
	
	dau = ctx->dav;
	dap = ctx->dap;
	
	/* VTS HEADER - OPEN */	
	if (vtk_fp) fprintf( vtk_fp, "<?xml version=\"1.0\"?>\n");
	if (vtk_fp) fprintf( vtk_fp, "<VTKFile type=\"PStructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
	
	DMDAGetInfo( dau, 0, &M,&N,&P, 0,0,0, 0,&swidth, 0,0,0, 0 );
	if (vtk_fp) fprintf( vtk_fp, "  <PStructuredGrid GhostLevel=\"%d\" WholeExtent=\"%d %d %d %d %d %d\">\n", 0, 0,M-1, 0,N-1, 0,P-1 );
	
	/* VTS COORD DATA */	
	if (vtk_fp) fprintf( vtk_fp, "    <PPoints>\n");
	if (vtk_fp) fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\"/>\n");
	if (vtk_fp) fprintf( vtk_fp, "    </PPoints>\n");
	
	
	/* VTS CELL DATA */	
	if (vtk_fp) fprintf( vtk_fp, "    <PCellData>\n");
//	fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"int_avg viscosity\" NumberOfComponents=\"1\"/>\n");
//	fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"int_avg sigma_II\" NumberOfComponents=\"1\"/>\n");
//	fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"int_avg tau_II\" NumberOfComponents=\"1\"/>\n");
//	fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"int_avg epsilon_II\" NumberOfComponents=\"1\"/>\n");
	for (k=0; k<__PField_TERMINATOR__; k++) {
		if (fieldlist[k]!=-1) {
			if (vtk_fp) fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"%s\" NumberOfComponents=\"1\"/>\n", PTatinFieldNames[k]);
		}
	}
	if (vtk_fp) fprintf( vtk_fp, "    </PCellData>\n");
	
	
	/* write out the parallel information */
	ierr = DAQ2PieceExtendForGhostLevelZero(vtk_fp,2,dau,prefix);CHKERRQ(ierr);
	
	/* VTS HEADER - CLOSE */	
	if (vtk_fp) fprintf( vtk_fp, "  </PStructuredGrid>\n");
	if (vtk_fp) fprintf( vtk_fp, "</VTKFile>\n");
	
	if (vtk_fp) fclose( vtk_fp );
	PetscFunctionReturn(0);
}

/*
 prefix = output_path/timestepXXXX/pp_subdomainXXXXX.vtu
 */
#undef __FUNCT__  
#define __FUNCT__ "pTatinParaViewOutputQuadratureStokesPointsIntegratedFields"
PetscErrorCode pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(pTatinCtx user,Vec X,const char path[],const char prefix[])
{
	char *vtkfilename,*filename;
	PetscMPIInt rank;
	PTatinField fieldlist[__PField_TERMINATOR__];
	
	PetscErrorCode ierr;
	ierr = pTatinLoadFieldList(fieldlist);CHKERRQ(ierr);
	
	ierr = pTatinGenerateParallelVTKName(prefix,"vts",&vtkfilename);CHKERRQ(ierr);
	if (path) {
		asprintf(&filename,"%s/%s",path,vtkfilename);
	} else {
		asprintf(&filename,"./%s",vtkfilename);
	}
	
	ierr = pTatinOutputQuadratureStokesPointsIntegratedFieldsVTS(user,fieldlist,X,filename);CHKERRQ(ierr);
	free(filename);
	free(vtkfilename);
		
	ierr = pTatinGenerateVTKName(prefix,"pvts",&vtkfilename);CHKERRQ(ierr);
	if (path) {
		asprintf(&filename,"%s/%s",path,vtkfilename);
	} else {
		asprintf(&filename,"./%s",vtkfilename);
	}
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
//	if (rank==0) {
		ierr = pTatinOutputQuadratureStokesPointsIntegratedFieldsPVTS(user,fieldlist,prefix,filename);CHKERRQ(ierr);
//	}
	free(filename);
	free(vtkfilename);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatinCreateOutputDirectory"
PetscErrorCode pTatinCreateOutputDirectory(void)
{
	char dirname[PETSC_MAX_PATH_LEN];
	PetscBool flg;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	flg = PETSC_FALSE;
	ierr = PetscOptionsGetString(PETSC_NULL,"-output_path",dirname,PETSC_MAX_PATH_LEN-1,&flg);CHKERRQ(ierr);
	if (flg == PETSC_FALSE) { 
		sprintf(dirname,"./output");
	}
	ierr = pTatinCreateDirectory(dirname);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatinCreateOutputStepDirectory"
PetscErrorCode pTatinCreateOutputStepDirectory(const char dirname[],const int step)
{
	char *name;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	asprintf(&name,"%s/timestep_%.6d",dirname,step);
	ierr = pTatinCreateDirectory(name);CHKERRQ(ierr);
	free(name);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ParaviewPVDOpen"
PetscErrorCode ParaviewPVDOpen(const char pvdfilename[])
{
	PetscMPIInt rank;
	FILE *fp;
	
	PetscFunctionBegin;
	/* only master generates this file */
	MPI_Comm_rank( PETSC_COMM_WORLD, &rank );
	if( rank != 0 ) { PetscFunctionReturn(0); }
	
	fp = fopen(pvdfilename,"w");
	fprintf(fp,"<?xml version=\"1.0\"?>\n");
#ifdef WORDSIZE_BIGENDIAN
	fprintf(fp,"<VTKFile type=\"Collection\" version=\"0.1\" byte_order=\"BigEndian\">\n");
#else
	fprintf(fp,"<VTKFile type=\"Collection\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
#endif
	
	fprintf(fp,"<Collection>\n");
	
	fprintf(fp,"</Collection>\n");
	fprintf(fp,"</VTKFile>\n");
	fclose(fp);
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ParaviewPVDAppend"
PetscErrorCode ParaviewPVDAppend(const char pvdfilename[],double time,const char datafile[], const char DirectoryName[])
{
	PetscMPIInt rank;
	FILE *fp;
	char line[10000];
	int key_L;
	char key[] = "</Collection>";
	char *copy,*tmp;
	
	PetscFunctionBegin;
	/* only master generates this file */
	MPI_Comm_rank( PETSC_COMM_WORLD, &rank );
	if( rank != 0 ) { PetscFunctionReturn(0); }
	
	
	fp = fopen(pvdfilename,"r");
	/* reset to start of file */
	rewind(fp);
	
	copy = NULL;
	key_L = strlen( key );
	while( !feof(fp) ) {
		fgets( line, 10000-1, fp );
		if ( strncmp(key,line,key_L)!=0 ) {
			
			/* copy line */
			if (copy!=NULL) {
			  asprintf(&tmp,"%s",copy);
			  free(copy);
				asprintf(&copy,"%s%s",tmp,line);
				free(tmp);
			}
			else {
			  asprintf(&copy,"%s",line);
			}
		}
		else {
			break;
		}
	}
	fclose(fp);
	
	/* open new file - clobbering the old */
	fp = fopen(pvdfilename,"w");
	
	/* write all copied chars */
	fprintf(fp,"%s",copy);
	
	/* write new data */
	fprintf(fp,"  <DataSet timestep=\"%1.6e\" file=\"./%s/%s\"/>\n",time, DirectoryName, datafile );
	
	/* close tag */
	fprintf(fp,"</Collection>\n");
	fprintf(fp,"</VTKFile>\n");
	
	fclose(fp);
	free(copy);
	
	PetscFunctionReturn(0);
}
