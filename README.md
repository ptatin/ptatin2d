# pTatin2D

## Overview
pTatin2D is a software package designed for studying long time-scale processes
relevant to geodynamics. The original motivation for this development was to
provide the community with an open-source toolkit capable of studying
high-resolution, two-dimensional models of lithospheric deformation.

## Functionality

* mixed finite elements (Q2-P1_disc) for the Stokes problem
* material points for tracking Lagrangian state and history variables
* energy equation solved with Q1 elements + SUPG
* ALE formulations (with a variety of remeshing solutions)
* extensible rheology components (currently supports: iso-viscous, Frank-Kamenetskii,
   Arrhenius, power-law, von Mises, Drucker Prager)
* full support for spatio-temporal Dirichlet and non-zero Neumann boundary conditions
* coupling with landscape evolution models
* full support for Newton and Picard non-linear solvers
* massively parallel implementation

pTatin2D leverages functionality from [PETSc](http://www.mcs.anl.gov/petsc)

## Requirements

* PETSc (only support for version 3.2)