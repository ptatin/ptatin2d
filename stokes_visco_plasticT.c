/*
 *  stokes_visco_plastic.c
 *  
 *
 *  Created by laetitia le pourhiet on 6/11/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"
#include "rheology.h"
#include "stokes_visco_plastic.h"
#include "element_type_Q1.h"


#undef __FUNCT__
#define __FUNCT__ "EvaluateRheologyNonlinearitiesMarkers_ViscoPlasticT"
PetscErrorCode EvaluateRheologyNonlinearitiesMarkers_ViscoPlasticT(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[],DM daT,PetscScalar LA_T[])
{	
	PetscErrorCode ierr;
	int         pidx,n_mp_points;
	DataBucket  db;
	DataField   PField_std, PField_stokes, PField_thermal;
	PetscScalar GNIu[2][Q2_NODES_PER_EL_2D],NIp[P_BASIS_FUNCTIONS];
	PetscScalar NIu[U_BASIS_FUNCTIONS];
	PetscScalar NIT[NODES_PER_EL_Q1_2D];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen_u,nen_p,nen_T,e,i,j,k;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	const PetscInt *elnidx_T;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	PetscScalar elT[NODES_PER_EL_Q1_2D];
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	PetscLogDouble t0,t1;
	PetscScalar  d1,exx_mp,eyy_mp,exy_mp,eta_mp;
	PetscScalar  sxx_mp,syy_mp,sxy_mp,u_mp,v_mp,trace_mp,pressure_mp;
	PetscScalar min_eta,max_eta,min_eta_g,max_eta_g;
	PetscInt npoints_yielded_g, npoints_yielded_tens_g,npoints_yielded,npoints_yielded_tens;
	PetscScalar Co, phi, A, B, tauyield_mp,strainrateinv_mp,stressinv_mp,contrast; 
	PetscScalar T_mp,rho_mp; 
	
	RheologyConstants *rheology;
	PetscScalar eta_eff_continuation_min,eta_eff_continuation_max,min_eta_cut,max_eta_cut,alpha_m;
	PhysCompEnergyCtx phys_energy;
	
	PetscFunctionBegin;
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	/* get physics */
	/* stokes */
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
	/* energy */
	phys_energy = user->phys_energy;
	ierr = DMDAGetElementsQ1(daT,&nel,&nen_T,&elnidx_T);CHKERRQ(ierr);
	
	min_eta = 1.0e100;
	max_eta = 1.0e-100;
	PetscGetTime(&t0);
	/* setup for rheological parameters depending on phases */
	rheology  = &user->rheology_constants;
	
	/* continuation parameter */
	
	min_eta_cut = rheology->eta_lower_cutoff_global;
	max_eta_cut = rheology->eta_upper_cutoff_global;  
	alpha_m = (double)(user->continuation_m+1)/( (double)(user->continuation_M+1) );
	contrast=log10(max_eta_cut/min_eta_cut);
	eta_eff_continuation_max = min_eta_cut*pow(10.0,(alpha_m*contrast));
	eta_eff_continuation_min = min_eta_cut;
	//PetscPrintf(PETSC_COMM_WORLD," continuation viscosity cut-off:max  %f  min %f \n", eta_eff_continuation_max,eta_eff_continuation_min);
	
 	/* marker loop */
	db = user->db;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));

	DataBucketGetDataFieldByName(db,MPntPThermal_classname,&PField_thermal);
	DataFieldGetAccess(PField_thermal);
	DataFieldVerifyAccess(PField_thermal,sizeof(MPntPThermal));
	
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	npoints_yielded = 0;
	npoints_yielded_tens = 0; 
	
	for (pidx=0; pidx<n_mp_points; pidx++) {
		MPntStd      *material_point;
		MPntPStokes  *mpprop_stokes;
		MPntPThermal *mpprop_thermal;
		double      *position, *xip;
		int          wil,phase_mp;
		char         is_yielding, yield_type;
		PetscScalar J[2][2], iJ[2][2];
		PetscScalar J_p,ojp;
		
		
		DataFieldAccessPoint(PField_std,    pidx,(void**)&material_point);
		DataFieldAccessPoint(PField_stokes, pidx,(void**)&mpprop_stokes);
		DataFieldAccessPoint(PField_thermal,pidx,(void**)&mpprop_thermal);
		
		MPntStdGetField_local_element_index(material_point,&wil);
		MPntStdGetField_global_coord(material_point,&position);
		MPntStdGetField_local_coord(material_point,&xip);
		MPntStdGetField_phase_index(material_point,&phase_mp);
		
		e = wil;
		
		/* get nodal properties for element e */
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);

		ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],u);CHKERRQ(ierr);
		ierr = DMDAGetScalarElementField_2D(elp,nen_p,(PetscInt*)&elnidx_p[nen_p*e],p);CHKERRQ(ierr);
		/* temperature */
		ierr = DMDAGetScalarElementFieldQ1_2D(elT,(PetscInt*)&elnidx_T[nen_T*e],LA_T);CHKERRQ(ierr);
		
		/* get element velocity/coordinates in component form */
		for( i=0; i<Q2_NODES_PER_EL_2D; i++ ) {
			ux[i] = elu[2*i  ];
			uy[i] = elu[2*i+1];
			
			xc[i] = elcoords[2*i  ];
			yc[i] = elcoords[2*i+1];
		}
		
		
		PTatinConstructNI_Q2_2D(xip,NIu);
		PTatinConstructGNI_Q2_2D(xip,GNIu);
		ConstructNi_pressure(xip,elcoords,NIp);

		/* temperature */
		ConstructNi_Q1_2D(xip,NIT);
		
		/* coord transformation */
		for( i=0; i<2; i++ ) {
			for( j=0; j<2; j++ ) { J[i][j] = 0.0; }
		}
		for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
			J[0][0] += GNIu[0][k] * xc[k] ;
			J[0][1] += GNIu[0][k] * yc[k] ;
			
			J[1][0] += GNIu[1][k] * xc[k] ;
			J[1][1] += GNIu[1][k] * yc[k] ;
		}
		J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
		ojp = 1.0/J_p;
		iJ[0][0] =  J[1][1]*ojp;
		iJ[0][1] = -J[0][1]*ojp;
		iJ[1][0] = -J[1][0]*ojp;
		iJ[1][1] =  J[0][0]*ojp;
		
		/* global derivs at each quadrature point */
		for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
			nx[k] = iJ[0][0]*GNIu[0][k] + iJ[0][1]*GNIu[1][k];
			ny[k] = iJ[1][0]*GNIu[0][k] + iJ[1][1]*GNIu[1][k];
		}
		
		/* u,v and strain rate (e= B u) at gp */
		u_mp = v_mp = 0.0;
		exx_mp = eyy_mp = exy_mp = 0.0;
		for( k=0; k<U_BASIS_FUNCTIONS; k++ ) {
			u_mp   += NIu[k] * ux[k];
			v_mp   += NIu[k] * uy[k];
			
			exx_mp += nx[k] * ux[k];
			eyy_mp += ny[k] * uy[k];
			exy_mp += ny[k] * ux[k] + nx[k] * uy[k];
		}
		exy_mp = 0.5 * exy_mp;
		strainrateinv_mp = sqrt(0.25*(exx_mp-eyy_mp)*(exx_mp-eyy_mp)+exy_mp*exy_mp);
		
		/* compute current stress at mp to get the pressure*/
		eta_mp = mpprop_stokes->eta;
		d1 = 2.0 * eta_mp;
		
		sxx_mp = d1 * exx_mp;
		syy_mp = d1 * eyy_mp;
		sxy_mp = d1 * exy_mp;
		
		trace_mp = 0.5 * ( sxx_mp + syy_mp );
		sxx_mp = sxx_mp - trace_mp;
		syy_mp = syy_mp - trace_mp;
		
		/* pressure at mp */
		pressure_mp = 0.0;
		for( k=0; k<P_BASIS_FUNCTIONS; k++ ) {
			pressure_mp += NIp[k] * elp[k];
		}
		pressure_mp = pressure_mp + trace_mp;
		
		/* temperature at mp */
		/* HERE TO ADD THE TEMP INTERPOLATION*/ 
		T_mp = 0.0;
		for (k=0; k<NODES_PER_EL_Q1_2D; k++) {
				T_mp = T_mp + NIT[k] * elT[k];
		}
		
		/* density at mp */
		rho_mp = rheology->const_rho0[phase_mp]*(1-rheology->temp_alpha[phase_mp]*T_mp);
		
		/* get the plastic properties*/
		Co          = rheology->mises_tau_yield[phase_mp]; 
		phi         = rheology->dp_pressure_dependance[phase_mp];
		
		/* check the yield criteria   */ 
		/* regular failure in shear  */
		A           = sin(phi);
		B           = cos(phi);	
		tauyield_mp = A * pressure_mp + B *Co;
		yield_type  = 1; 
		
		if ( tauyield_mp < rheology->tens_cutoff[phase_mp]){
			/* failure in tension cutoff */
			tauyield_mp = rheology->tens_cutoff[phase_mp];
			yield_type = 2;
		} else if (tauyield_mp > rheology->Hst_cutoff[phase_mp]){   
			/* failure at High stress cut off à la boris */
			tauyield_mp = rheology->Hst_cutoff[phase_mp];
			yield_type = 3;
		}  
		
		/* viscosity */
		eta_mp = rheology->const_eta0[phase_mp];
		eta_mp = eta_mp*exp(-rheology->temp_theta[phase_mp]*T_mp);
      //  printf("%1.4e %1.4e %1.4e %1.4e %1.4e \n", material_point->coor[0],material_point->coor[1],T_mp,eta_mp,rheology->temp_theta[phase_mp]);
	
		/* viscous stress predictor */ 
		d1 = 2.0 * eta_mp;
		sxx_mp = d1 * exx_mp;
		syy_mp = d1 * eyy_mp;
		sxy_mp = d1 * exy_mp;
		trace_mp = 0.5 * ( sxx_mp + syy_mp );
		sxx_mp = sxx_mp - trace_mp;
		syy_mp = syy_mp - trace_mp;
		
		stressinv_mp = sqrt(0.25*(sxx_mp-syy_mp)*(sxx_mp-syy_mp)+sxy_mp*sxy_mp);
		
		/* apply plastic correction when marker is yielding */ 
		is_yielding = 0; 
		if (stressinv_mp > tauyield_mp){
			eta_mp = 0.5*tauyield_mp/strainrateinv_mp;
			npoints_yielded++;
			is_yielding = yield_type; 
		}
		
		
		/* Continuation cutt off */ 
		if (eta_mp > eta_eff_continuation_max) eta_mp = eta_eff_continuation_max ;
		if (eta_mp < eta_eff_continuation_min) eta_mp = eta_eff_continuation_min ;    
		
		
		/* Global cuttoffs for viscosity*/
		if (eta_mp > rheology->eta_upper_cutoff_global) {
			eta_mp = rheology->eta_upper_cutoff_global;
		}
		if (eta_mp < rheology->eta_lower_cutoff_global) {
			eta_mp = rheology->eta_lower_cutoff_global;
		}
    
		/* update viscosity on marker */
		MPntPStokesSetField_eta_effective(mpprop_stokes,eta_mp);
		
		/* update density on marker */
		MPntPStokesSetField_density(mpprop_stokes,-rho_mp);
		
		/* monitor bounds */
		if (eta_mp > max_eta) { max_eta = eta_mp; }
		if (eta_mp < min_eta) { min_eta = eta_mp; }
	}
	
	
	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokes);
	DataFieldRestoreAccess(PField_thermal);
	
	PetscGetTime(&t1);
  MPI_Allreduce(&min_eta,&min_eta_g,1, MPI_DOUBLE, MPI_MIN, PETSC_COMM_WORLD);
  MPI_Allreduce(&max_eta,&max_eta_g,1, MPI_DOUBLE, MPI_MAX, PETSC_COMM_WORLD);
  MPI_Allreduce(&npoints_yielded,&npoints_yielded_g,1, MPI_INT, MPI_SUM, PETSC_COMM_WORLD);
	
	 PetscPrintf(PETSC_COMM_WORLD,"Update rheology (visco-plasticT) [mpoint]: npoints_yielded %d; (min,max)_eta %1.2e,%1.2e; log10(max/min) %1.2e; cpu time %1.2e (sec)\n", npoints_yielded_g,  min_eta_g, max_eta_g, log10(max_eta_g/min_eta_g), t1-t0 );
	 
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvaluateRheologyNonlinearitiesMarkers_ViscoPlasticStrainWeakeningT"
PetscErrorCode EvaluateRheologyNonlinearitiesMarkers_ViscoPlasticStrainWeakeningT(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[],DM daT,PetscScalar LA_T[])
{	
	PetscErrorCode ierr;
	int         pidx,n_mp_points;
	DataBucket  db;
	DataField   PField_std, PField_stokes, PField_stokespl, PField_thermal;
	PetscScalar GNIu[2][Q2_NODES_PER_EL_2D],NIp[P_BASIS_FUNCTIONS];
	PetscScalar NIu[U_BASIS_FUNCTIONS];
	PetscScalar NIT[NODES_PER_EL_Q1_2D];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen_u,nen_p,nen_T,e,i,j,k;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	const PetscInt *elnidx_T;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	PetscScalar elT[NODES_PER_EL_Q1_2D];
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	PetscLogDouble t0,t1;
	PetscScalar  d1,exx_mp,eyy_mp,exy_mp,eta_mp;
	PetscScalar  sxx_mp,syy_mp,sxy_mp,u_mp,v_mp,trace_mp,pressure_mp;
	PetscScalar min_eta,max_eta,min_eta_g,max_eta_g;
	PetscInt npoints_yielded_g,npoints_yielded;
	PetscScalar phi, Co, A, B, tauyield_mp,strainrateinv_mp,stressinv_mp;
	PetscScalar T_mp,rho_mp;     
	RheologyConstants *rheology;
	PetscScalar eta_eff_continuation_max,eta_eff_continuation_min,min_eta_cut,max_eta_cut,alpha_m,contrast;
	PhysCompEnergyCtx phys_energy;
	
	PetscFunctionBegin;
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	/* get physics */
	/* stokes */
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
	/* energy */
	phys_energy = user->phys_energy;
	ierr = DMDAGetElementsQ1(daT,&nel,&nen_T,&elnidx_T);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
	min_eta = 1.0e100;
	max_eta = 1.0e-100;
	PetscGetTime(&t0);
	/* setup for rheological parameters depending on phases */
	rheology  = &user->rheology_constants;
	
	/* continuation parameter */
	
	min_eta_cut = rheology->eta_lower_cutoff_global;
	max_eta_cut = rheology->eta_upper_cutoff_global;  
	alpha_m = (double)(user->continuation_m+1)/( (double)(user->continuation_M+1) );
	contrast=log10(max_eta_cut/min_eta_cut);
	eta_eff_continuation_max = min_eta_cut*pow(10.0,(alpha_m*contrast));
	eta_eff_continuation_min = min_eta_cut;
	PetscPrintf(PETSC_COMM_WORLD," continuation viscosity cut-off:max  %1.6e  min %1.6e \n", eta_eff_continuation_max,eta_eff_continuation_min);
	
 	/* marker loop */
	db = user->db;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));
	
	DataBucketGetDataFieldByName(db,MPntPStokesPl_classname,&PField_stokespl);
	DataFieldGetAccess(PField_stokespl);
	DataFieldVerifyAccess(PField_stokespl,sizeof(MPntPStokesPl));
	
	DataBucketGetDataFieldByName(db,MPntPThermal_classname,&PField_thermal);
	DataFieldGetAccess(PField_thermal);
	DataFieldVerifyAccess(PField_thermal,sizeof(MPntPThermal));
	
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	npoints_yielded = 0; 
	
	for (pidx=0; pidx<n_mp_points; pidx++) {
		MPntStd       *material_point;
		MPntPStokes   *mpprop_stokes;
		MPntPStokesPl *mpprop_stokespl;
		MPntPThermal  *mpprop_thermal;
		double        *position, *xip;
		int           wil,phase_mp;
		char          is_yielding, yield_type;
		float         eplastic;
		PetscScalar   epl_softmin,epl_softmax;
		PetscScalar   J[2][2], iJ[2][2];
		PetscScalar   J_p,ojp;
		
		
		DataFieldAccessPoint(PField_std,     pidx,(void**)&material_point);
		DataFieldAccessPoint(PField_stokes,  pidx,(void**)&mpprop_stokes);
		DataFieldAccessPoint(PField_stokespl,pidx,(void**)&mpprop_stokespl);
		
		MPntStdGetField_local_element_index(material_point,&wil);
		MPntStdGetField_global_coord(material_point,&position);
		MPntStdGetField_local_coord(material_point,&xip);
		MPntStdGetField_phase_index(material_point,&phase_mp);
		
		MPntPStokesPlGetField_plastic_strain(mpprop_stokespl,&eplastic);
		MPntPStokesPlGetField_yield_indicator(mpprop_stokespl,&is_yielding);
		
		e = wil;
		
		/* get nodal properties for element e */
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		
		ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],u);CHKERRQ(ierr);
		ierr = DMDAGetScalarElementField_2D(elp,nen_p,(PetscInt*)&elnidx_p[nen_p*e],p);CHKERRQ(ierr);
		/* temperature */
		ierr = DMDAGetScalarElementFieldQ1_2D(elT,(PetscInt*)&elnidx_T[nen_T*e],LA_T);CHKERRQ(ierr);
		
		/* get element velocity/coordinates in component form */
		for( i=0; i<Q2_NODES_PER_EL_2D; i++ ) {
			ux[i] = elu[2*i  ];
			uy[i] = elu[2*i+1];
			
			xc[i] = elcoords[2*i  ];
			yc[i] = elcoords[2*i+1];
		}
		
		
		PTatinConstructNI_Q2_2D(xip,NIu);
		PTatinConstructGNI_Q2_2D(xip,GNIu);
		ConstructNi_pressure(xip,elcoords,NIp);

		/* temperature */
		ConstructNi_Q1_2D(xip,NIT);
		
		/* coord transformation */
		for( i=0; i<2; i++ ) {
			for( j=0; j<2; j++ ) { J[i][j] = 0.0; }
		}
		for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
			J[0][0] += GNIu[0][k] * xc[k] ;
			J[0][1] += GNIu[0][k] * yc[k] ;
			
			J[1][0] += GNIu[1][k] * xc[k] ;
			J[1][1] += GNIu[1][k] * yc[k] ;
		}
		J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
		ojp = 1.0/J_p;
		iJ[0][0] =  J[1][1]*ojp;
		iJ[0][1] = -J[0][1]*ojp;
		iJ[1][0] = -J[1][0]*ojp;
		iJ[1][1] =  J[0][0]*ojp;
		
		/* global derivs at each quadrature point */
		for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
			nx[k] = iJ[0][0]*GNIu[0][k] + iJ[0][1]*GNIu[1][k];
			ny[k] = iJ[1][0]*GNIu[0][k] + iJ[1][1]*GNIu[1][k];
		}
		
		/* u,v and strain rate (e= B u) at gp */
		u_mp = v_mp = 0.0;
		exx_mp = eyy_mp = exy_mp = 0.0;
		for( k=0; k<U_BASIS_FUNCTIONS; k++ ) {
			u_mp   += NIu[k] * ux[k];
			v_mp   += NIu[k] * uy[k];
			
			exx_mp += nx[k] * ux[k];
			eyy_mp += ny[k] * uy[k];
			exy_mp += ny[k] * ux[k] + nx[k] * uy[k];
		}
		exy_mp = 0.5 * exy_mp;
		strainrateinv_mp = sqrt(0.25*(exx_mp-eyy_mp)*(exx_mp-eyy_mp)+exy_mp*exy_mp);
		
		
        /* stress */
		eta_mp = mpprop_stokes->eta;
        d1 = 2.0 * eta_mp;
		sxx_mp = d1 * exx_mp;
		syy_mp = d1 * eyy_mp;
		sxy_mp = d1 * exy_mp;
		
		trace_mp = 0.5 * ( sxx_mp + syy_mp );
		sxx_mp = sxx_mp - trace_mp;
		syy_mp = syy_mp - trace_mp;
		
		stressinv_mp = sqrt(0.25*(sxx_mp-syy_mp)*(sxx_mp-syy_mp)+sxy_mp*sxy_mp);
		
		/* pressure at mp */
		pressure_mp = 0.0;
		for( k=0; k<P_BASIS_FUNCTIONS; k++ ) {
			pressure_mp += NIp[k] * elp[k];
		}
		pressure_mp = pressure_mp + trace_mp;
		
		/* temperature at mp */
		/* HERE TO ADD THE TEMP INTERPOLATION*/ 
		T_mp = 0.0;
		for (k=0; k<NODES_PER_EL_Q1_2D; k++) {
			T_mp = T_mp + NIT[k] * elT[k];
		}
        
		/* density at mp */
		rho_mp = rheology->const_rho0[phase_mp]*(1-rheology->temp_alpha[phase_mp]*T_mp);
		
		
		/*compute yield stress env*/
		Co          = rheology->mises_tau_yield[phase_mp]; 
		phi         = rheology->dp_pressure_dependance[phase_mp];
		
		/* softening bits could be replace by more specific functions in the future */  
		epl_softmin = rheology->soft_min_strain_cutoff[phase_mp]; 
		epl_softmax = rheology->soft_max_strain_cutoff[phase_mp];
		
		if (eplastic > epl_softmin){
			if (eplastic > epl_softmax) {
				Co  = rheology->soft_Co_inf[phase_mp];
				phi = rheology->soft_phi_inf[phase_mp];
			} else {
				float X  = (eplastic-epl_softmin)/(epl_softmax-epl_softmin);
				Co  = Co  - X * (Co-rheology->soft_Co_inf[phase_mp]);
				phi = phi - X * (phi-rheology->soft_phi_inf[phase_mp]);
			}
		}
		
		/* check the yield criteria   */ 
		/* regular failure in shear  */
		A           = sin(phi);
		B           = cos(phi);	
		tauyield_mp = A * pressure_mp + B *Co;
		yield_type  = 1; 
		
		if ( tauyield_mp < rheology->tens_cutoff[phase_mp]){
			/* failure in tension cutoff */
			tauyield_mp = rheology->tens_cutoff[phase_mp];
			yield_type = 2;
		} else if (tauyield_mp > rheology->Hst_cutoff[phase_mp]){   
			/* failure at High stress cut off à la boris */
			tauyield_mp = rheology->Hst_cutoff[phase_mp];
			yield_type = 3;
		}  
		
		/* viscosity */
		eta_mp = rheology->const_eta0[phase_mp];
		eta_mp = eta_mp*exp(-rheology->temp_theta[phase_mp]*T_mp);
        //printf("%1.4e %1.4e %1.4e %1.4e %1.4e \n", material_point->coor[0],material_point->coor[1],T_mp,eta_mp,rheology->temp_theta[phase_mp],rho_mp);

        
		/* viscous stress predictor */ 
		d1 = 2.0 * eta_mp;
		sxx_mp = d1 * exx_mp;
		syy_mp = d1 * eyy_mp;
		sxy_mp = d1 * exy_mp;
		trace_mp = 0.5 * ( sxx_mp + syy_mp );
		sxx_mp = sxx_mp - trace_mp;
		syy_mp = syy_mp - trace_mp;
		
		stressinv_mp = sqrt(0.25*(sxx_mp-syy_mp)*(sxx_mp-syy_mp)+sxy_mp*sxy_mp);
		
		/* apply plastic correction when marker is yielding */ 
		is_yielding = 0; 
		if (stressinv_mp > tauyield_mp){
			eta_mp = 0.5*tauyield_mp/strainrateinv_mp;
			npoints_yielded++;
			is_yielding = yield_type; 
		}
		
		/* Continuation cutt off */ 
		if (eta_mp > eta_eff_continuation_max) eta_mp = eta_eff_continuation_max ;
		if (eta_mp < eta_eff_continuation_min) eta_mp = eta_eff_continuation_min ;    
		
		
		/* Global cuttoffs for viscosity*/
		if (eta_mp > rheology->eta_upper_cutoff_global) {
			eta_mp = rheology->eta_upper_cutoff_global;
		}
		if (eta_mp < rheology->eta_lower_cutoff_global) {
			eta_mp = rheology->eta_lower_cutoff_global;
		}
		
		/* update viscosity on marker */
		MPntPStokesSetField_eta_effective(mpprop_stokes,eta_mp);
		
		/* update density on marker */
		MPntPStokesSetField_density(mpprop_stokes,-rho_mp);
		
		/* set plastic properties */
		MPntPStokesPlSetField_yield_indicator(mpprop_stokespl,is_yielding);
		
		
		/* monitor bounds */
		if (eta_mp > max_eta) { max_eta = eta_mp; }
		if (eta_mp < min_eta) { min_eta = eta_mp; }
	}
	
	
	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokes);
	DataFieldRestoreAccess(PField_stokespl);
	DataFieldRestoreAccess(PField_thermal);
	
	PetscGetTime(&t1);
  MPI_Allreduce(&min_eta,&min_eta_g,1, MPI_DOUBLE, MPI_MIN, PETSC_COMM_WORLD);
  MPI_Allreduce(&max_eta,&max_eta_g,1, MPI_DOUBLE, MPI_MAX, PETSC_COMM_WORLD);
  MPI_Allreduce(&npoints_yielded,&npoints_yielded_g,1, MPI_INT, MPI_SUM, PETSC_COMM_WORLD);
	
	 PetscPrintf(PETSC_COMM_WORLD,"Update rheology (visco-plastic) [mpoint]: npoints_yielded %d; (min,max)_eta %1.2e,%1.2e; log10(max/min) %1.2e; cpu time %1.2e (sec)\n",
	 npoints_yielded_g, min_eta_g, max_eta_g, log10(max_eta_g/min_eta_g), t1-t0 );
	 
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}





