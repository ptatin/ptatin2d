
#ifndef __pTatin2d_models_h__
#define __pTatin2d_models_h__

/* Add names for your models here - don't add numbers to your models */
typedef enum {
	PTATIN_MODEL_SolCx = 0,
	PTATIN_MODEL_RT,
	PTATIN_MODEL_SN,
	PTATIN_MODEL_MAP_LV,
	PTATIN_MODEL_CrystalChannel,
	PTATIN_MODEL_Delamination,
	PTATIN_MODEL_PureShearFree,
	PTATIN_MODEL_Camembert,
	PTATIN_MODEL_SimpleShearFree,
	PTATIN_MODEL_StepShearFree,
  	PTATIN_MODEL_Notch,
  	PTATIN_MODEL_NotchEl,
    PTATIN_MODEL_NotchGP,
	PTATIN_MODEL_winkler,
	PTATIN_MODEL_mms_linear,
	PTATIN_MODEL_GENE,
	PTATIN_MODEL_Sinker,
	PTATIN_MODEL_AdvDiff,
	PTATIN_MODEL_HotBlob,
	PTATIN_MODEL_Drunk,
    PTATIN_MODEL_G2008,
    PTATIN_MODEL_Schmalholz2011,        
    PTATIN_MODEL_afonso, 
    PTATIN_MODEL_mumu,
    PTATIN_MODEL_WUSA,
    PTATIN_MODEL_anthony,
    PTATIN_MODEL_reza,
    PTATIN_MODEL_paul,
    PTATIN_MODEL_julia,
	PTATIN_MODEL_CrystalJCP,
	PTATIN_MODEL_Elastic,
	PTATIN_MODEL_report,             /* Do not remove or rename this line!! */
	__PTATIN_MODEL_LIST_TERMINATOR__ /* Do not remove or rename this line!! */
} pTatin2dModelType;

extern const char *pTatin2dModelTypeName[];
	

/* helpers */
PetscErrorCode pTatin2d_ModelTypeReport(void);
PetscErrorCode pTatinModelLoad(pTatinCtx ctx);
PetscErrorCode ptatin_match_model_index(const char modelname[],int *index);

/* interfaces */
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelOutput(pTatinCtx ctx,Vec X,const char name[]);
PetscErrorCode pTatin2d_ModelApplyMaterialBoundaryCondition(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelUpdateMeshGeometry(pTatinCtx ctx,Vec X);


/* add model prototypes here */

/* solCx */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_SolCx(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_SolCx(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_SolCx(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelOutput_SolCx(pTatinCtx ctx,Vec X,const char prefix[]);

/* RT */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_RT(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_RT(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_RT(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelOutput_RT(pTatinCtx ctx,Vec X,const char prefix[]);

/* SN */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_SN(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_SN(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_SN(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelOutput_SN(pTatinCtx ctx,Vec X,const char prefix[]);
PetscErrorCode pTatin2d_ModelUpdateMeshGeometry_SN(pTatinCtx ctx,Vec X);

/* MAP_LV */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_MAP_LV(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_MAP_LV(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_MAP_LV(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelOutput_MAP_LV(pTatinCtx ctx,Vec X,const char prefix[]);
PetscErrorCode pTatin2d_ModelUpdateMeshGeometry_MAP_LV(pTatinCtx ctx,Vec X);

/* crystals*/
PetscErrorCode pTatin2d_ModelOutput_CrystalChannel(pTatinCtx ctx,Vec X,const char prefix[]);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_CrystalChannel(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_CrystalChannel(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_CrystalChannel(pTatinCtx ctx);

/* Notch */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_Notch(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_Notch(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_Notch(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelOutput_Notch(pTatinCtx ctx,Vec X,const char prefix[]);
PetscErrorCode pTatin2d_ModelUpdateMeshGeometry_Notch(pTatinCtx ctx,Vec X);

/* NotchGP */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_NotchGP(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_NotchGP(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_NotchGP(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelOutput_NotchGP(pTatinCtx ctx,Vec X,const char prefix[]);
PetscErrorCode pTatin2d_ModelUpdateMeshGeometry_NotchGP(pTatinCtx ctx,Vec X);


/* delamination */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_Delamination(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_Delamination(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_Delamination(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelOutput_Delamination(pTatinCtx ctx,Vec X,const char prefix[]);

/* pureShearFree */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_PureShearFree(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_PureShearFree(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_PureShearFree(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelOutput_PureShearFree(pTatinCtx ctx,Vec X,const char prefix[]);

/* Camembert */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_Camembert(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_Camembert(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_Camembert(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelOutput_Camembert(pTatinCtx ctx,Vec X,const char prefix[]);
/* SimpleShearFree */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_SimpleShearFree(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_SimpleShearFree(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_SimpleShearFree(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelOutput_SimpleShearFree(pTatinCtx ctx,Vec X,const char prefix[]);

/* StepShearFree */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_StepShearFree(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_StepShearFree(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_StepShearFree(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelOutput_StepShearFree(pTatinCtx ctx,Vec X,const char prefix[]);



/* NotchEl */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_NotchEl(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_NotchEl(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_NotchEl(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelOutput_NotchEl(pTatinCtx ctx,Vec X,const char prefix[]);

/* winkler */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_winkler(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_winkler(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_winkler(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelOutput_winkler(pTatinCtx ctx,Vec X,const char prefix[]);

/* manufactured solution */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_mms_linear(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_mms_linear(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_mms_linear(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelOutput_mms_linear(pTatinCtx ctx,Vec X,const char prefix[]);

/* GENE */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_GENE(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_GENE(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_GENE(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelOutput_GENE(pTatinCtx ctx,Vec X,const char prefix[]);
PetscErrorCode pTatin2d_ModelUpdateMeshGeometry_GENE(pTatinCtx ctx,Vec X);

PetscErrorCode pTatin2d_ModelSetMaterialPropertyFromOptions_GENE(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelSetMarkerIndexFromMap_GENE(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelSetInitialStokesVariableOnMarker_GENE(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelDeformUpperSurfaceFromOptions_GENE(pTatinCtx ctx);

/* Sinker */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_Sinker(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_Sinker(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_Sinker(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelOutput_Sinker(pTatinCtx ctx,Vec X,const char prefix[]);

/* AdvDiff tests */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_AdvDiff(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_AdvDiff(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_AdvDiff(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelOutput_AdvDiff(pTatinCtx ctx,Vec X,const char prefix[]);
PetscErrorCode pTatin2d_ModelUpdateMeshGeometry_AdvDiff(pTatinCtx ctx,Vec X);

/* HotBlob tests */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_HotBlob(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_HotBlob(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_HotBlob(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelOutput_HotBlob(pTatinCtx ctx,Vec X,const char prefix[]);
PetscErrorCode pTatin2d_ModelUpdateMeshGeometry_HotBlob(pTatinCtx ctx,Vec X);

/* drunken seamnan test */
PetscErrorCode pTatin2d_ModelOutput_DrunkSeaman(pTatinCtx ctx,Vec X,const char prefix[]);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_DrunkSeamanDeformed(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_DrunkSeaman(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_DrunkSeaman(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelUpdateMeshGeometry_Drunk(pTatinCtx ctx,Vec X);

/* G2008 */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_G2008(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_G2008(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_G2008(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelUpdateMeshGeometry_G2008(pTatinCtx ctx,Vec X);
PetscErrorCode pTatin2d_ModelSetMaterialProperty_G2008(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelOutput_G2008(pTatinCtx ctx,Vec X,const char prefix[]);

/* Schmalholz2011 */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_Schmalholz2011(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_Schmalholz2011(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_Schmalholz2011(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelUpdateMeshGeometry_Schmalholz2011(pTatinCtx ctx,Vec X);
PetscErrorCode pTatin2d_ModelSetMaterialProperty_Schmalholz2011(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelOutput_Schmalholz2011(pTatinCtx ctx,Vec X,const char prefix[]);


/* Elastic */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_Elastic(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_Elastic(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_Elastic(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelUpdateMeshGeometry_Elastic(pTatinCtx ctx,Vec X);
PetscErrorCode pTatin2d_ModelSetMaterialProperty_Elastic(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelOutput_Elastic(pTatinCtx ctx,Vec X,const char prefix[]);

/* afonso */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_afonso(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_afonso(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_afonso(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelSetMaterialProperty_afonso(pTatinCtx ctx);

/* mumu */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_mumu(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_mumu(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_mumu(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelSetMaterialProperty_mumu(pTatinCtx ctx);


/* anthony */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_anthony(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_anthony(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_anthony(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelSetMaterialProperty_anthony(pTatinCtx ctx);

/* julia */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_julia(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_julia(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_julia(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelSetMaterialProperty_julia(pTatinCtx ctx);

/* reza */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_reza(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_reza(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_reza(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelSetMaterialProperty_reza(pTatinCtx ctx);

/* paul */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_paul(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_paul(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_paul(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelSetMaterialProperty_paul(pTatinCtx ctx);

/* afonso */
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_WUSA(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_WUSA(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_WUSA(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelSetMaterialProperty_WUSA(pTatinCtx ctx);


/* crystals */
PetscErrorCode pTatin2d_ModelOutput_CrystalJCP(pTatinCtx ctx,Vec X,const char prefix[]);
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_CrystalJCP(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_CrystalJCP(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_CrystalJCP(pTatinCtx ctx);


#endif
