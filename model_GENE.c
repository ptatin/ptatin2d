/*
 
 Model Description:
 Example reading geometry, rheology and everything from file and command line.
 Model size must be smaller than pmap.
 In case of an air layer, it must be prescribed in the map file! (diff with MAP_LV)  
 
 
 Input / command line parameters:
 
 
 
 
 
 
 
 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"

#define WRITE_VP
#define WRITE_TFIELD
#define WRITE_MPOINTS
//#define WRITE_QPOINTS
//#define WRITE_TCELL
//#define WRITE_INT_FIELDS
#define WRITE_FACEQPOINTS


/* ================= GENE: GENERAL MODEL FROM COMMAND LINE OPTIONS AND MAP ================= */
/*  for the moment only free surface and free slip with initial surface being horizontal, 
 DONE: create a surface adaptor (see original notch model)   
 TODO: Refactor the bc's using a separate file and enum and probably function pointer... 
 DONE  TODO: write a parser of options_file with matlab.  
 DONE  TODO: find where it is good to define the global viscosity cut off, probably within matlab wrapper
 */ 

PetscErrorCode pTatin2d_ModelDeformUpperSurfaceFromOptions_GENE(pTatinCtx ctx);
/*pTatin2d_ModelApplyInitialMeshGeometry_GENE
 this function create the mesh, deforms it with initial topo and
 load all the maps for initial conditions in the context except density map made with perplex
 which are loaded within material parameter functions
 */

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetSPMParameters_GENE"
PetscErrorCode pTatin2d_ModelSetSPMParameters_GENE(pTatinCtx ctx)
{
	pTatinUnits *units;
	RheologyConstants      *rheology;
	PetscScalar kappa,bcval,source,start_time,heat_diffusivity_ND,hprod_ND;
	PhysCompSPMCtx spm;
  
	PetscErrorCode ierr; 
	
	spm         = ctx->phys_spm;
	units       = &ctx->units;
	rheology   = &ctx->rheology_constants;
	
	spm->NSedMarkPerCell = 4; // number of markers deposited by elements 
	ierr  = PetscOptionsGetInt(PETSC_NULL,"-spm_NsedPerCell",&spm->NSedMarkPerCell,0);CHKERRQ(ierr);
   	spm->sed_mark_id     = 0; // phase assigned to sediments
	ierr  = PetscOptionsGetInt(PETSC_NULL,"-spm_sediment_phase",&spm->sed_mark_id,0);CHKERRQ(ierr);
	kappa                = 5.0;
	source               = 0.0; // TODO define scaling for source
	start_time           = 3e12;
	ierr  = PetscOptionsGetReal(PETSC_NULL,"-spm_start_time",&start_time,0);CHKERRQ(ierr);
	ierr  = PetscOptionsGetReal(PETSC_NULL,"-spm_kappa",&kappa,0);CHKERRQ(ierr);
	PetscPrintf(PETSC_COMM_WORLD,"ModelSetSPMParameters: options = [ %1.4e , %1.4e ]  \n", start_time, kappa );
	UnitsApplyInverseScaling(units->si_diffusivity,kappa,&kappa);
	UnitsApplyInverseScaling(units->si_time,start_time,&start_time);
	PetscPrintf(PETSC_COMM_WORLD,"ModelSetSPMParameters: optionsND = [ %1.4e , %1.4e ]  \n", start_time, kappa );
	spm->source_0         = source ;
	spm->kappa_0          = kappa ;
	spm->start_time       = start_time; 
	UnitsApplyInverseScaling(units->si_diffusivity,rheology->temp_kappa[spm->sed_mark_id],&heat_diffusivity_ND);
	spm->heat_diffusivity_ND       = heat_diffusivity_ND;
    UnitsApplyInverseScaling(units->si_heatsource,rheology->temp_prod[spm->sed_mark_id],&hprod_ND);
    //UnitsApplyInverseScaling(units->si_diffusivity,rheology->temp_kappa[spm->sed_mark_id],&heat_diffusivity_ND);	
	spm->heat_productivity_ND       = hprod_ND;
	
	//dh/dt = 0.
	//val = 0.0;ierr = DMDABCListTraverse1D(spm->bclist,spm->daSPM,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	//bcval = 0.0;ierr = DMDABCListTraverse1D(spm->bclist,spm->daSPM,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	
	//bcval = 3.0e-2 ;ierr = DMDABCListTraverse1D(spm->bclist,spm->daSPM,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	//bcval = 3.0e-2 ;ierr = DMDABCListTraverse1D(spm->bclist,spm->daSPM,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	
	// TODO DEFINE THE POINTER FUNCTION THAT ASSIGN DIFFUSIVITY i.e. from phase, constant, as a func of height_old, time etc..//
	
	PetscFunctionReturn(0);
}

 
#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_GENE"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_GENE(pTatinCtx ctx)
{
	PhaseMap phasemap;
	TopoMap topomap;
	PetscInt M,N;
	PetscBool flg;
    PetscBool found,refine;
	PetscErrorCode ierr;
	PetscReal Ox,Oy,Lx,Ly,gravity_acceleration,slope;
	char name[PETSC_MAX_PATH_LEN];
	char map_file[PETSC_MAX_PATH_LEN];
	char topo_file[PETSC_MAX_PATH_LEN];
	pTatinUnits *units;	
	
	PetscFunctionBegin;
	units   = &ctx->units;
	
	/* make a regular non dimensional grid from option file parameters*/
	found = PETSC_TRUE;
    refine = PETSC_FALSE;
	ierr  = PetscOptionsGetReal(PETSC_NULL,"-grav",&gravity_acceleration,&found);CHKERRQ(ierr);
	if (found == PETSC_FALSE){
		SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value for gravity! check your -grav option \n");
	}
    slope = 0.0;
    ierr  = PetscOptionsGetReal(PETSC_NULL,"-slope",&slope,0);CHKERRQ(ierr);
    slope = slope/180.*3.14;
	ierr = QuadratureStokesGravityModelCoordinateAlignedSetValues(ctx->Q,sin(slope)*gravity_acceleration, -cos(slope)*gravity_acceleration);CHKERRQ(ierr);
	
	ierr  = PetscOptionsGetReal(PETSC_NULL,"-Ox",&Ox,&found);CHKERRQ(ierr);
	if (found == PETSC_FALSE){
		SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value for origine in X! check your -Ox option \n");
	}    
	
	ierr  = PetscOptionsGetReal(PETSC_NULL,"-Oy",&Oy,&found);CHKERRQ(ierr);
	if (found == PETSC_FALSE){
		SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value for origine in Y! check your -Oy option \n");
	}    
	ierr  = PetscOptionsGetReal(PETSC_NULL,"-Lx",&Lx,&found);CHKERRQ(ierr);
	if (found == PETSC_FALSE){
		SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value for length in X! check your -Lx option \n");
	}    
	ierr  = PetscOptionsGetReal(PETSC_NULL,"-Ly",&Ly,&found);CHKERRQ(ierr);
	if (found == PETSC_FALSE){
		SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value for  length in Y! check your -Ly option  \n");
	}    
	
	
	
	PetscPrintf(PETSC_COMM_WORLD,"ApplyInitialMeshGeometry_GENE: options Domain_SI = [ %1.4e , %1.4e ] x [ %1.4e , %1.4e ] \n", Ox, Lx,Oy,Ly );
	UnitsApplyInverseScaling(units->si_length,Ox,&Ox);
	UnitsApplyInverseScaling(units->si_length,Oy,&Oy);
	UnitsApplyInverseScaling(units->si_length,Lx,&Lx);
	UnitsApplyInverseScaling(units->si_length,Ly,&Ly);
	
	
    
    
    
    
    PetscPrintf(PETSC_COMM_WORLD,"ApplyInitialMeshGeometry_GENE: options Domain_ND = [ %1.4e , %1.4e ] x [ %1.4e , %1.4e ] \n", Ox, Lx,Oy,Ly );
	ierr = DMDASetUniformCoordinates(ctx->dav,Ox,Lx,Oy,Ly, 0.0,0.0);CHKERRQ(ierr);
    
    
    ierr = PetscOptionsGetBool(PETSC_NULL,"-use_refined_mesh",&refine,&flg);CHKERRQ(ierr);
    if (refine){
    ierr = pTatin2d_ModelRefineMeshFromOptions_GENE(ctx);
    }
	
		
	/* load phase repartition data from file *.pmap */    
	
	flg = PETSC_FALSE;
	ierr = PetscOptionsGetString(PETSC_NULL,"-map_file",map_file,PETSC_MAX_PATH_LEN-1,&flg);CHKERRQ(ierr);
	
	if (flg == PETSC_TRUE) {
	sprintf(name,"%s.pmap",map_file);
	PhaseMapLoadFromFileScale(name,&phasemap,units);
	
	sprintf(name,"%s_phase_map.gp",map_file);
	PhaseMapViewGnuplot(name,phasemap);
	
	/* 
	 Two ways to attach model data
	 1. via the specific data attach/get function
	 2. via the generic ptatin ctx model data method where you have to specify a textual name to set/retrieve the object
	 */
	ierr = pTatinCtxAttachPhaseMap(ctx,phasemap);CHKERRQ(ierr);
	/* the above and below lines are equivalent... but only do one or the other... */
	//ierr = pTatinCtxAttachModelData(ctx,"phasemap",phasemap);CHKERRQ(ierr);
	
	PetscPrintf(PETSC_COMM_WORLD,"ApplyInitialMeshGeometry_GENE: phasemap Domain_ND = [ %1.4e , %1.4e ] x [ %1.4e , %1.4e ] \n", phasemap->x0,phasemap->x1, phasemap->y0,phasemap->y1 );
	
	// Check the size of phase map vs domain
	if (Ox < phasemap->x0 || Oy < phasemap->y0 || Lx > phasemap->x1 || Ly > phasemap->y1 ){
		SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Your phasemap is smaller than the domain defined by -Ox,-Oy,-Lx and -Ly \n");
	}    
    }
	/* Check if the user provided an initial topo file *.tmap */
	/* if a topo file is provided call the function to deform the grid*/
	flg = PETSC_FALSE;
	ierr = PetscOptionsGetString(PETSC_NULL,"-topo_file",topo_file,PETSC_MAX_PATH_LEN-1,&flg);CHKERRQ(ierr);
	if (flg == PETSC_TRUE) {
		sprintf(name,"%s.tmap",topo_file);
		TopoMapLoadFromFileScale(name,&topomap,units);
		sprintf(name,"%s_topo_map.gp",topo_file);
		TopoMapViewGnuplot(name,topomap);
		ierr = pTatinCtxAttachTopoMap(ctx,topomap);CHKERRQ(ierr);
		
		PetscPrintf(PETSC_COMM_WORLD,"ApplyInitialMeshGeometry_GENE: topomap Domain_ND = [ %1.4e , %1.4e ] x [ %1.4e , %1.4e ] x %1.4e \n", topomap->x0,topomap->x1, topomap->z0,topomap->z1,topomap->max_height);
		if ( topomap->max_height > phasemap->y1){
			SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"the surface of phasemap is lower than the maximum topography \n");
		}    
		if ( topomap->max_height < Oy){
			SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"the surface of phasemap is lower than the base of the model \n");
		}
		ierr = pTatin2d_ModelDeformUpperSurfaceFromOptions_GENE(ctx);CHKERRQ(ierr); 
		
	} else {
		PetscPrintf(PETSC_COMM_WORLD,"ApplyInitialMeshGeometry_GENE: starting from a flat topography \n");    
	}
	
	/* check for energy solver */
	{
		PetscBool active = PETSC_FALSE;
		ierr = pTatinPhysCompActivated(ctx,PhysComp_Energy,&active);CHKERRQ(ierr);
		
		if (active) {
			DM da;
			Vec T;
			PhysCompEnergyCtx phys_energy;
			TempMap  map;
			char file[PETSC_MAX_PATH_LEN];
			char prodmapname[PETSC_MAX_PATH_LEN];
			double T0;
			
			phys_energy = ctx->phys_energy;
			da          = phys_energy->daT;
			T           = phys_energy->T;
			
			
			ierr = DMDAProjectCoordinatesQ2toQ1_2d(ctx->dav,da);CHKERRQ(ierr);
			
			flg = PETSC_FALSE;
			ierr = PetscOptionsGetString(PETSC_NULL,"-temp_file",file,PETSC_MAX_PATH_LEN-1,&flg);CHKERRQ(ierr);
			
			if (flg == PETSC_TRUE) {
				sprintf(name,"%s.tempmap",file);
				TempMapLoadFromFile(name,&map);
				TempMapApplyScale(map,units);
                
				//sprintf(name,"%s_temp_map.gp",file);
				//TempMapViewGnuplot(name,map);
				ierr = pTatinCtxAttachTempMap(ctx,map);CHKERRQ(ierr);
			} else {
				ierr = PetscOptionsGetScalar(PETSC_NULL,"-initial_temperature",&T0,&flg);CHKERRQ(ierr);
				if (flg == PETSC_FALSE){
					SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Energy solver is ON and you didn't provide initial conditions for temperature \n");   
				}
			}
			
			flg = PETSC_FALSE;
			ierr = PetscOptionsGetString(PETSC_NULL,"-prod_file",file,PETSC_MAX_PATH_LEN-1,&flg);CHKERRQ(ierr);
			if (flg == PETSC_TRUE) {
				sprintf(name,"%s.prodmap",file);
				TempMapLoadFromFile(name,&map);
				ProdMapApplyScale(map,units);
				
				//sprintf(name,"%s_prod_map.gp",file);
				//TempMapViewGnuplot(name,map);
				sprintf(prodmapname,"prod_map");
				ierr = pTatinCtxAttachMap(ctx,map,prodmapname);CHKERRQ(ierr);
			} else {
				PetscPrintf(PETSC_COMM_WORLD,"ApplyInitialMeshGeometry_GENE: no heat production file found \n");    
			}
			
		}	
		
	}
	
	PetscFunctionReturn(0);
}
/*pTatin2d_ModelDeformUpperSurfaceFromOptions_GENE
 this function is used to deform the upper surface of the mesh 
 */
#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelDeformUpperSurfaceFromOptions_GENE"
PetscErrorCode pTatin2d_ModelDeformUpperSurfaceFromOptions_GENE(pTatinCtx ctx)
{
	DM         cda,da;
	Vec        coordinates,gcoords;
	DMDACoor2d **LA_gcoords;
	PetscInt   i,j,si,sj,ni,nj,M,N;
	double height, xp[3];
	TopoMap topomap;
	PetscErrorCode ierr;
	PetscFunctionBegin;
	
	da = ctx->dav; 
	topomap = PETSC_NULL;
	ierr = pTatinCtxGetTopoMap(ctx,&topomap);CHKERRQ(ierr);
	
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(da,&gcoords);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,gcoords,&LA_gcoords);CHKERRQ(ierr);
	ierr = DMDAGetCorners(cda,&si,&sj,0,&ni,&nj,0);CHKERRQ(ierr);
	ierr = DMDAGetInfo(da,0,&M,&N,0, 0,0,0, 0,0,0, 0,0,0);CHKERRQ(ierr);
	
	j = sj+nj-1;
	if (j == N-1) {
		for (i=si; i<si+ni; i++) {
			xp[0] = LA_gcoords[j][i].x;
			xp[1] = LA_gcoords[j][i].y;
			xp[2] = 0.0;
			
			TopoMapGetHeight(topomap,xp,&height);
			LA_gcoords[j][i].y = height;
		}
	}
	
	ierr = DMDAVecRestoreArray(cda,gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetCoordinates(da,&coordinates);CHKERRQ(ierr);
	ierr = DMLocalToGlobalBegin(cda,gcoords,INSERT_VALUES,coordinates);CHKERRQ(ierr);
	ierr = DMLocalToGlobalEnd  (cda,gcoords,INSERT_VALUES,coordinates);CHKERRQ(ierr);
	
	ierr = DMDARemeshSetUniformCoordinatesBetweenJLayers2d(ctx->dav,0,N);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "pTatin2d_ModelRefineMeshFromOptions_GENE"
PetscErrorCode pTatin2d_ModelRefineMeshFromOptions_GENE(pTatinCtx ctx)
{
    PetscScalar OOx[21],OOy[21];
    PetscInt Nx[20], Ny[20],test,ndomainx,ndomainy;
    PetscInt nlimx,nlimy, i,j;

    PetscErrorCode ierr;
    PetscFunctionBegin;
    
    PetscOptionsGetInt(PETSC_NULL,"-refine_n_domain_x",&ndomainx,0);
    PetscOptionsGetInt(PETSC_NULL,"-refine_n_domain_y",&ndomainy,0);
    
    nlimx=ndomainx+1;
    nlimy=ndomainy+1;
    PetscOptionsGetRealArray(PETSC_NULL,"-refine_x_lim",OOx,&nlimx,PETSC_NULL);
    PetscOptionsGetRealArray(PETSC_NULL,"-refine_y_lim",OOy,&nlimy,PETSC_NULL);
    
    PetscOptionsGetIntArray(PETSC_NULL,"-refine_x_dom_el",Nx,&ndomainx,PETSC_NULL);
    PetscOptionsGetIntArray(PETSC_NULL,"-refine_y_dom_el",Ny,&ndomainy,PETSC_NULL);
    
    test=0;
    for (i=0;i<ndomainx;i++) { test = test+ Nx[i];}
    if (test != ctx->mx) {SETERRQ2(PETSC_COMM_WORLD,PETSC_ERR_SUP,"mx = %d is not equal to the number of element = %d in the refined mesh", ctx->mx, test);}
    test=0;
    for (j=0;j<ndomainy;j++) { test += Ny[j];}
    if (test != ctx->my) {SETERRQ2(PETSC_COMM_WORLD,PETSC_ERR_SUP,"my = %d is not equal to the number of element = %d in the refined mesh", ctx->my, test);}
    if (nlimx > 20 | nlimy >20) {SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"maximum refinement domain is set to 20 in the current version of the code");}
    
    ierr = DMDASetRefinedCoordinates(ctx->dav); CHKERRQ(ierr);
    
    PetscFunctionReturn(0);
}






#if 0
/*
 exemple given by dave to rewrite the parameter parser in a more 
 coder friendly extendable way
 */
PetscErrorCode ParseParameterList(const char model_option_prefix[],PetscInt region_id,const int max_params,const char *param_name[],PetscReal param_value[])
{
	int            param;    
	char           *option_name;
	PetscBool      found;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	for (param=0; param<max_params; param++) {
		asprintf(&option_name,"-%s_%d",param_name[param],region_id);
		found = PETSC_FALSE;
		ierr = PetscOptionsGetReal(model_option_prefix,option_name,&(param_value[param]),&found);CHKERRQ(ierr);
		if (!found) { /* strip off leading dash (-) in the string option_name */
			SETERRQ2(PETSC_COMM_WORLD,PETSC_ERR_USER,"Expected user to provide option name -%s%s \n",model_option_prefix,&option_name[1]);
		}
		free(option_name);
	}
	PetscFunctionReturn(0);
}

/*
 model_option_prefix = "GENE_"
 */
PetscErrorCode RheologySofteningParseParameters(RheologyConstants *R,const char model_option_prefix[],PetscInt region_id)
{
	const int      max_params = 4;
	const char     param_name[] = { "Co_inf", "Phi_inf", "eps_min", "eps_max", 0 };
	PetscReal      *material_property[max_params];
	int            param;    
	char           *option_name;
	PetscBool      found;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	/* assign parameters in struct into an array we can loop through */
	material_property[0] = &R->soft_Co_inf[iregion_id];
	material_property[1] = &R->soft_phi_inf[iregion_id];
	material_property[2] = &R->soft_min_strain_cutoff[iregion_id];
	material_property[3] = &R->soft_max_strain_cutoff[iregion_id];
	
	/*
	 for (param=0; param<max_params; param++) {
	 asprintf(&option_name,"-%s_%d",param_name[param],region_id);
	 found = PETSC_FALSE;
	 ierr = PetscOptionsGetReal(model_option_prefix,option_name,&(material_property[param]),&found);CHKERRQ(ierr);
	 if (!found) { // strip off leading dash (-) in the string option_name //
	 SETERRQ2(PETSC_COMM_WORLD,PETSC_ERR_USER,"Expected user to provide option name -%s%s \n",model_option_prefix,&option_name[1]);
	 }
	 free(option_name);
	 }
	 */
	
	ierr = ParseParameterList(model_option_prefix,region_id,max_params,param_name,material_property);CHKERRQ(ierr); 
	
	PetscFunctionReturn(0);
}
#endif
/*pTatin2d_ModelSetMaterialPropertyFromOptions_GENE
 read the viscosity cut off
 read the type of rheology
 assigne material property to each material index
 all the cases for rheology_type are obsolet except RHEOLOGY_VPT_STD
 they are kept for compatibility with early models but should be remove asap
 in RHEOLOGY_VPT_STD material properties are:  
 density_type : DENSITY_CONSTANT,DENSITY_FUNCTION (rho0*(1-alpha T)*(1+beta P)), DENSITY_TABLE (perplex derived density)
 viscous_type : VISCOUS_CONSTANT, VISCOUS_FRANKK (eta0*exp(-theta T)),VISCOUS_ARRHENIUS (eps = A sig^n exp(Q/RT) 
 plastic_type : PLASTIC_NONE,PLASTIC_M (mises),PLASTIC_DP(drucker prager)
 softening_type :SOFTENING_NONE,SOFTENING_LINEAR,SOFTENING_EXPONENTIAL
 */
#undef __FUNCT__ 
#define __FUNCT__ "pTatin2d_ModelSetMaterialPropertyFromOptions_GENE"
PetscErrorCode pTatin2d_ModelSetMaterialPropertyFromOptions_GENE(pTatinCtx ctx)

{
	RheologyConstants      *rheology;
	PetscInt               nphase, i,rheol_type;
	PetscErrorCode         ierr;
	char   				   *option_name;
	PetscBool              found; 
	
	PetscFunctionBegin;
	
	rheol_type = 0; 
	ierr       = PetscOptionsGetInt("GENE_","-rheol",&rheol_type,&found);CHKERRQ(ierr);
	if (found == PETSC_FALSE){
		SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option -GENE_rheol \n");
	}
	ctx->rheology_constants.rheology_type = rheol_type;
	
	rheology   = &ctx->rheology_constants;
	ierr       = PetscOptionsGetInt("GENE_","-nphase",&nphase,&found);CHKERRQ(ierr);
	if (found == PETSC_FALSE){
		SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option -GENE_nphase \n");
	}
	
	rheology->eta_upper_cutoff_global = 1.e100;
	rheology->eta_lower_cutoff_global = 1.e-100;
	ierr       = PetscOptionsGetReal("GENE_","-eta_max_cut_off",&rheology->eta_upper_cutoff_global,0);CHKERRQ(ierr);
	ierr       = PetscOptionsGetReal("GENE_","-eta_min_cut_off",&rheology->eta_lower_cutoff_global,0);CHKERRQ(ierr);
	
	ierr = PetscOptionsGetBool(PETSC_NULL,"-store_melt",&rheology->store_melt,0);CHKERRQ(ierr);
	
	
	for (i=0; i< nphase ;i++) {
		
		switch  (ctx->rheology_constants.rheology_type) {
				
			case RHEOLOGY_VISCOUS :
			{
				asprintf(&option_name, "-eta_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->const_eta0[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-rho_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->const_rho0[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
			}
				break; 
				
			case RHEOLOGY_VISCO_PLASTIC : 
			{
				asprintf(&option_name, "-eta_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->const_eta0[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-rho_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->const_rho0[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-Co_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->mises_tau_yield[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-Phi_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->dp_pressure_dependance[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-Tens_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->tens_cutoff[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-Hs_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->Hst_cutoff[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
			}
				break; 
				
			case RHEOLOGY_VISCO_PLASTIC_STRAIN_WEAKENING : 
			{
				asprintf(&option_name, "-eta_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->const_eta0[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-rho_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->const_rho0[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-Co_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->mises_tau_yield[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-Phi_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->dp_pressure_dependance[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-Tens_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->tens_cutoff[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-Hs_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->Hst_cutoff[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-Co_inf_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->soft_Co_inf[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-Phi_inf_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->soft_phi_inf[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-eps_min_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->soft_min_strain_cutoff[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-eps_max_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->soft_max_strain_cutoff[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
			}
				break; 
				
			case RHEOLOGY_VISCO_PLASTIC_T : 
			{
				{
					PetscBool active = PETSC_FALSE;
					
					ierr = pTatinPhysCompActivated(ctx,PhysComp_Energy,&active);CHKERRQ(ierr);
					if (active==PETSC_FALSE)  SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide run with thermal driver \n");
				}
				
				asprintf(&option_name, "-eta_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->const_eta0[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-rho_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->const_rho0[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-Co_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->mises_tau_yield[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-Phi_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->dp_pressure_dependance[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-Tens_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->tens_cutoff[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-Hs_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->Hst_cutoff[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
                
				rheology->temp_alpha[i] = 3e-5;
				asprintf(&option_name, "-alpha_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->temp_alpha[i]),&found);CHKERRQ(ierr);
				/*  if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name); */
				free(option_name);
				
				rheology->temp_kappa[i] = 1e-6;
				asprintf(&option_name, "-kappa_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->temp_kappa[i]),&found);CHKERRQ(ierr);
				/*    if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);*/
				free(option_name);
				
				rheology->temp_theta[i] = 0.03;
				asprintf(&option_name, "-theta_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->temp_theta[i]),&found);CHKERRQ(ierr);
				/*   if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);*/
				free(option_name);
			}
				break; 
				
			case RHEOLOGY_VISCO_PLASTIC_STRAIN_WEAKENING_T : 
			{
				
				{
					PetscBool active = PETSC_FALSE;
					
					ierr = pTatinPhysCompActivated(ctx,PhysComp_Energy,&active);CHKERRQ(ierr);
					if (active==PETSC_FALSE)  SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide run with thermal driver \n");
				}
				
				asprintf(&option_name, "-eta_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->const_eta0[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-rho_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->const_rho0[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-Co_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->mises_tau_yield[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-Phi_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->dp_pressure_dependance[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-Tens_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->tens_cutoff[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-Hs_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->Hst_cutoff[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-Co_inf_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->soft_Co_inf[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-Phi_inf_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->soft_phi_inf[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-eps_min_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->soft_min_strain_cutoff[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-eps_max_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->soft_max_strain_cutoff[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-alpha_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->temp_alpha[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-kappa_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->temp_kappa[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				asprintf(&option_name, "-theta_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->temp_theta[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
			}
				break; 
				
			case RHEOLOGY_VISCO_PLASTICY :
			{
				SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"The provided rheology is not working or to be used \n");
			}
				break;
				
			case RHEOLOGY_VPT_STD : 
			{
				
				PetscInt viscous_type,plastic_type,softening_type,density_type,melt_type;
				PetscBool active = PETSC_FALSE;
				
				ierr = pTatinPhysCompActivated(ctx,PhysComp_Energy,&active);CHKERRQ(ierr);
				
				if (active==PETSC_TRUE) {  
					asprintf(&option_name, "-kappa_%d",i);
					ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->temp_kappa[i]),&found);CHKERRQ(ierr);
					if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
					free(option_name);
				}
				
				density_type = 0; 
				asprintf(&option_name, "-density_%d",i);
				ierr       = PetscOptionsGetInt("GENE_",option_name,&density_type,&found);CHKERRQ(ierr);
				rheology->density_type[i] = density_type; 
				free(option_name);
				
				asprintf(&option_name, "-rho_%d",i);
				ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->const_rho0[i]),&found);CHKERRQ(ierr);
				if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
				free(option_name);
				
				switch (rheology->density_type[i]) {
						
					case DENSITY_CONSTANT:
					{
						
					}
						break;
						
					case DENSITY_FUNCTION:
					{
						if (active==PETSC_FALSE)  SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide run with thermal driver \n");
						
						asprintf(&option_name, "-alpha_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->temp_alpha[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) {
							PetscPrintf(PETSC_COMM_WORLD,"no thermal expension coefficient found for rock type %d I set it to 0 \n",i);
							rheology->temp_alpha[i] = 0;
						}
						free(option_name);
						
						asprintf(&option_name, "-beta_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->temp_beta[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) {
							PetscPrintf(PETSC_COMM_WORLD,"no compressibility coefficient found for rock type %d I set it to 0 \n",i);
							rheology->temp_beta[i] = 0;
						}
						free(option_name);
					}
						break;   
						
					case DENSITY_TABLE:
					{   
						char file[PETSC_MAX_PATH_LEN];
						char name[PETSC_MAX_PATH_LEN],densmapname[PETSC_MAX_PATH_LEN];
						pTatinUnits *units;
						TempMap map;
                        
						if (active==PETSC_FALSE) {
							SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide run with thermal driver \n");   
						}
						
						units   = &ctx->units;
						asprintf(&option_name, "-densmapfile_%d",i);
						found = PETSC_FALSE;
						ierr = PetscOptionsGetString("GENE_",option_name,file,PETSC_MAX_PATH_LEN-1,&found);CHKERRQ(ierr);
						free(option_name);
						
						if (found == PETSC_TRUE) {
							sprintf(name,"%s.densmap",file);
							TempMapLoadFromFile(name,&map);
							DensMapApplyScale(map,units);
                            
							//sprintf(name,"%s_dens_map.gp",file);
							//TempMapViewGnuplot(name,map);
							
							sprintf(densmapname,"densmap_%d",i);
							ierr = pTatinCtxAttachMap(ctx,map,densmapname);CHKERRQ(ierr);
						} else { 
							SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"No density file found for rocktype %d \n",i);  
						}
					}
						break;
						
					default:
						break;
				}
				
				viscous_type = 0; 
				asprintf(&option_name, "-viscous_%d",i);
				ierr       = PetscOptionsGetInt("GENE_",option_name,&viscous_type,&found);CHKERRQ(ierr);
				free(option_name);
				
				rheology->viscous_type[i] = viscous_type; 
				
				switch (rheology->viscous_type[i]) {
                        
					case VISCOUS_CONSTANT:
					{
						asprintf(&option_name, "-eta_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->const_eta0[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
						free(option_name);
						
					}
						break;
						
					case VISCOUS_FRANKK: 
					{
						if (active==PETSC_FALSE)  SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide run with thermal driver \n");
						
						asprintf(&option_name, "-eta_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->const_eta0[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
						free(option_name);
						
						asprintf(&option_name, "-theta_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->temp_theta[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
						free(option_name);
					}
						break;   
						
					case VISCOUS_ARRHENIUS:
					{
						if (active==PETSC_FALSE)  SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide run with thermal driver \n");
						
						asprintf(&option_name, "-nexp_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->arrh_nexp[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
						free(option_name);
						
						asprintf(&option_name, "-entalpy_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->arrh_entalpy[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
						free(option_name);
						
						asprintf(&option_name, "-preexpA_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->arrh_preexpA[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
						free(option_name);
						
						asprintf(&option_name, "-Ascale_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->arrh_Ascale[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE){ 
							PetscPrintf(PETSC_COMM_WORLD,"User didn't provide scaling factor for A for phase %d assuming 1 \n",i);
							rheology->arrh_Ascale[i] =1.0;
						}
						free(option_name);
						
						
						
						asprintf(&option_name, "-Vmol_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->arrh_Vmol[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE){ 
							PetscPrintf(PETSC_COMM_WORLD,"User didn't provide activation volume for phase %d assuming 0 \n",i);
							rheology->arrh_Vmol[i] =0.0;
						}
						free(option_name);
						
						asprintf(&option_name, "-Tref_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->arrh_Tref[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE){ 
							PetscPrintf(PETSC_COMM_WORLD,"User didn't reference temperature for phase %d assuming 0 \n",i);
							rheology->arrh_Tref[i] =0.0;
						}
						free(option_name);
					}
						break;
					case VISCOUS_POWERLAW:
					{
						
						asprintf(&option_name, "-nexp_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->arrh_nexp[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
						free(option_name);
						
						asprintf(&option_name, "-preexpA_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->arrh_preexpA[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
						free(option_name);
					}
						break;
				    case VISCOUS_ARRHENIUS_MIXING:
				    
					{
					    char file[PETSC_MAX_PATH_LEN];
						char name[PETSC_MAX_PATH_LEN],mapname[PETSC_MAX_PATH_LEN];
						pTatinUnits *units;
						TempMap map;
						
						if (active==PETSC_FALSE) {
							SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide run with thermal driver \n");   
						}
						
						asprintf(&option_name, "-Ascale_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->arrh_Ascale[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE){ 
							PetscPrintf(PETSC_COMM_WORLD,"User didn't provide scaling factor for A for phase %d assuming 1 \n",i);
							rheology->arrh_Ascale[i] =1.0;
						}
						free(option_name);
						
						
						
						asprintf(&option_name, "-Vmol_%d",i);
						found = PETSC_FALSE;
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->arrh_Vmol[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE){ 
							PetscPrintf(PETSC_COMM_WORLD,"User didn't provide activation volume for phase %d assuming 0 \n",i);
							rheology->arrh_Vmol[i] =0.0;
						}
						free(option_name);
						
						asprintf(&option_name, "-Ascale_%d",i);
						found = PETSC_FALSE;
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->arrh_Ascale[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE){ 
							PetscPrintf(PETSC_COMM_WORLD,"User didn't provide scaling factor for A for phase %d assuming 1 \n",i);
							rheology->arrh_Ascale[i] =1.0;
						}
						free(option_name);
						
						asprintf(&option_name, "-Tref_%d",i);
						found = PETSC_FALSE;
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->arrh_Tref[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE){ 
							PetscPrintf(PETSC_COMM_WORLD,"User didn't reference temperature for phase %d assuming 0 \n",i);
							rheology->arrh_Tref[i] =0.0;
						}
						free(option_name);
						
						units   = &ctx->units;
						asprintf(&option_name, "-rheolmixingfile_%d",i);
						found = PETSC_FALSE;
						ierr = PetscOptionsGetString("GENE_",option_name,file,PETSC_MAX_PATH_LEN-1,&found);CHKERRQ(ierr);
						free(option_name);
						
						if (found == PETSC_TRUE) {
							sprintf(name,"%s.Qmap",file);
							TempMapLoadFromFile(name,&map);
						    RheolMapApplyScale(map,units);
							sprintf(mapname,"Qmap_%d",i);
							ierr = pTatinCtxAttachMap(ctx,map,mapname);CHKERRQ(ierr);
							
							sprintf(name,"%s.Amap",file);
							TempMapLoadFromFile(name,&map);
							RheolMapApplyScale(map,units);
							sprintf(mapname,"Amap_%d",i);
							ierr = pTatinCtxAttachMap(ctx,map,mapname);CHKERRQ(ierr);
							
							sprintf(name,"%s.nexpmap",file);
							TempMapLoadFromFile(name,&map);
							sprintf(mapname,"nexpmap_%d",i);
							ierr = pTatinCtxAttachMap(ctx,map,mapname);CHKERRQ(ierr);
							
							//free(mapname);
							//free(name);
							
						} else { 
							SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"No rheol mixing files found for rocktype %d \n",i);  
						}
						}
						
						break;		
					default:
						break;
				}
				
            if (rheology->store_melt) { 
				melt_type = MELT_NONE; 
				asprintf(&option_name, "-melt_%d",i);
				ierr       = PetscOptionsGetInt("GENE_",option_name,&melt_type,&found);CHKERRQ(ierr);
				free(option_name);
				rheology->melt_type[i] = melt_type; 
				
				switch (rheology->melt_type[i]) {
				case MELT_NONE:
						break;
						
					case MELT_MUMU:
					{
						asprintf(&option_name, "-Tinf_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->melt_Tinf[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
						free(option_name);
						
						asprintf(&option_name, "-Tsol_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->melt_Tsol[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
						free(option_name);
						
						asprintf(&option_name, "-rhomelt_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->melt_rho[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
						free(option_name);
						
						asprintf(&option_name, "-etamelt_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->melt_eta[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
						free(option_name);
 
					}
					break;
                    case MELT_P:
                    {
                        
                        asprintf(&option_name, "-meltP_type_%d",i);
                        ierr = PetscOptionsGetInt("GENE_",option_name,&(rheology->meltP_type[i]),&found);CHKERRQ(ierr);
                        if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
                      
                        free(option_name);
                        
                        asprintf(&option_name, "-rhomelt_%d",i);
                        ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->melt_rho[i]),&found);CHKERRQ(ierr);
                        if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
                        free(option_name);
                        
                        asprintf(&option_name, "-etamelt_%d",i);
                        ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->melt_eta[i]),&found);CHKERRQ(ierr);
                        if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
                        free(option_name);
                    }
                        break;
				}
			}	
				plastic_type = 0; 
				
				ierr       = PetscOptionsGetInt("GENE_","-plastic",&plastic_type,&found);CHKERRQ(ierr);
				asprintf(&option_name, "-plastic_%d",i);
				ierr       = PetscOptionsGetInt("GENE_",option_name,&plastic_type,&found);CHKERRQ(ierr);
				free(option_name);
				
				rheology->plastic_type[i]=plastic_type; 
				
				switch (rheology->plastic_type[i]) {
						
					case PLASTIC_NONE:
						break;
						
					case PLASTIC_M:
					{
						
						asprintf(&option_name, "-Co_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->mises_tau_yield[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
						free(option_name);
						
						asprintf(&option_name, "-Tens_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->tens_cutoff[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
						free(option_name);
					}
						break;    
						
					case PLASTIC_DP:
					{
						asprintf(&option_name, "-Co_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->mises_tau_yield[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
						free(option_name);
						
						asprintf(&option_name, "-Phi_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->dp_pressure_dependance[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
						free(option_name);
						
						asprintf(&option_name, "-Tens_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->tens_cutoff[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
						free(option_name);
						
						asprintf(&option_name, "-Hs_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->Hst_cutoff[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
						free(option_name);
						
					}
						break;
						
				}
				
				softening_type = 0; 
				
				ierr       = PetscOptionsGetInt("GENE_","-softening",&softening_type,&found);CHKERRQ(ierr);
				asprintf(&option_name, "-softening_%d",i);
				ierr       = PetscOptionsGetInt("GENE_",option_name,&softening_type,&found);CHKERRQ(ierr);
				free(option_name);
				
				rheology->softening_type[i]=softening_type; 
				
				switch(rheology->softening_type[i]) {
                        
					case SOFTENING_NONE : 
						break; 
                        
					case SOFTENING_LINEAR : 
					{    
						asprintf(&option_name, "-Co_inf_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->soft_Co_inf[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
						free(option_name);
						
						asprintf(&option_name, "-Phi_inf_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->soft_phi_inf[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
						free(option_name);
						
						asprintf(&option_name, "-eps_min_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->soft_min_strain_cutoff[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
						free(option_name);
						
						asprintf(&option_name, "-eps_max_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->soft_max_strain_cutoff[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
						free(option_name);
					}
						break; 
						
					case SOFTENING_EXPONENTIAL :
					{
						asprintf(&option_name, "-Co_inf_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->soft_Co_inf[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
						free(option_name);
						
						asprintf(&option_name, "-Phi_inf_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->soft_phi_inf[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
						free(option_name);
						
						asprintf(&option_name, "-eps_min_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->soft_min_strain_cutoff[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
						free(option_name);
						
						asprintf(&option_name, "-eps_efold_%d",i);
						ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->soft_max_strain_cutoff[i]),&found);CHKERRQ(ierr);
						if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user to provide value to option %s \n",option_name);
						free(option_name);
					}
						break; 
				}
				
				
			}
				break; 
				
		} 
	}
	
	
	
	
	
	
	
#if 0
	{    
		PetscPrintf(PETSC_COMM_WORLD,"index \t eta0  \t rho    \t Co     \t phi \n");
		for (i=0; i< nphase ;i++) {
			PetscPrintf(PETSC_COMM_WORLD,"%d: %e  %e  %e  %e  \n",i, rheology->const_eta0[i],rheology->const_rho0[i], rheology->mises_tau_yield[i],rheology->dp_pressure_dependance[i]);
		}       
		
		{
			PetscBool active = PETSC_FALSE;
			ierr = pTatinPhysCompActivated(ctx,PhysComp_Energy,&active);CHKERRQ(ierr);
			
			if (active) {
				PetscPrintf(PETSC_COMM_WORLD,"index \t theta     \t kappa     \t alpha \n");
				
				for (i=0; i< nphase ;i++) {
					PetscPrintf(PETSC_COMM_WORLD,"%d: %e  %e  %e  \n",i, rheology->temp_theta[i],rheology->temp_kappa[i], rheology->temp_alpha[i]);
				}    
				
			}
		}
	}
#endif
    
	PetscFunctionReturn(0);
}

/*pTatin2d_ModelSetMarkerIndexFromMap_GENE
 assign index to markers from map that is in context
 */ 
#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetMarkerIndexFromMap_GENE"
PetscErrorCode pTatin2d_ModelSetMarkerIndexFromMap_GENE(pTatinCtx ctx)
{
	PhaseMap               phasemap;
	PetscInt               p,n_mp_points;
	DataBucket             db;
	DataField              PField_std;
	int                    phase_init, phase, phase_index, is_valid;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	phasemap = PETSC_NULL;
	ierr = pTatinCtxGetPhaseMap(ctx,&phasemap);CHKERRQ(ierr);
	
	/* define properties on material points */
	db = ctx->db;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	for (p=0; p<n_mp_points; p++) {
		MPntStd     *material_point;
		
		double  *position;
		
		DataFieldAccessPoint(PField_std,p,(void**)&material_point);
		
		/* Access using the getter function provided for you (recommeneded for beginner user) */
		MPntStdGetField_global_coord(material_point,&position);
		// OR
		/* Access directly into material point struct (not recommeneded for beginner user) */
		/* Direct access only recommended for efficiency */
		/* position = material_point->coor; */
		
		MPntStdGetField_phase_index(material_point,&phase_init);		
		PhaseMapGetPhaseIndex(phasemap,position,&phase_index);
		PhaseMapCheckValidity(phasemap,phase_index,&is_valid);
		
		//PetscPrintf(PETSC_COMM_WORLD,"Phase index : %d  is_valid %d \n", phase_index,is_valid);
		
		if (is_valid==1) { /* point located in the phase map */
			phase = phase_index;
		} else {
			SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"marker outside the domain\n your phasemap is smaller than the domain \n please check your parameters and retry");
		}	
		/* user the setters provided for you */
		MPntStdSetField_phase_index(material_point,phase);
		
	}
	
	DataFieldRestoreAccess(PField_std);
	
	
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetMarkerIndexConstant_GENE"
PetscErrorCode pTatin2d_ModelSetMarkerIndexConstant_GENE(pTatinCtx ctx)
{
	PhaseMap               phasemap;
	PetscInt               p,n_mp_points,iph;
	DataBucket             db;
	DataField              PField_std;
	int                    phase_init, phase, phase_index, is_valid;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
		
	/* define properties on material points */
	db = ctx->db;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	ierr = PetscOptionsGetInt(PETSC_NULL,"-constant_phase",&iph,0);CHKERRQ(ierr);
	for (p=0; p<n_mp_points; p++) {
		MPntStd     *material_point;
		
		double  *position;
		
		DataFieldAccessPoint(PField_std,p,(void**)&material_point);
		
		/* Access using the getter function provided for you (recommeneded for beginner user) */
		MPntStdGetField_global_coord(material_point,&position);
		
		
		MPntStdSetField_phase_index(material_point,iph);
		
	}
	
	DataFieldRestoreAccess(PField_std);
	
	
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetInitialConstantTemperatureOnNodes_GENE"
PetscErrorCode pTatin2d_ModelSetInitialConstantTemperatureOnNodes_GENE(pTatinCtx ctx)
/* define properties on material points */
{
	PetscErrorCode         ierr;
	DM da;
	Vec T;
	PhysCompEnergyCtx phys_energy;
	PetscInt i,j,si,sj,nx,ny;
	Vec coords;
	DMDACoor2d **LA_coords;
	DM cda;
	PetscScalar **LA_T,T0;
	
	
	PetscFunctionBegin;
	
	phys_energy = ctx->phys_energy;
	da          = phys_energy->daT;
	T           = phys_energy->T;
	
	//map = PETSC_NULL;
	//ierr = pTatinCtxGetTempMap(ctx,&map);CHKERRQ(ierr);
	
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-initial_temperature",&T0,0);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(da,&coords);CHKERRQ(ierr);
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,coords,&LA_coords);CHKERRQ(ierr);
	
	ierr = DMDAVecGetArray(da,T,&LA_T);CHKERRQ(ierr);
	
	ierr = DMDAGetCorners(da,&si,&sj,0 , &nx,&ny,0);CHKERRQ(ierr);
	
	for( j=sj; j<sj+ny; j++ ) {
		for( i=si; i<si+nx; i++ ) {
			PetscScalar xn,yn,Tic;
			double position[2];
			
			position[0] = LA_coords[j][i].x;
			position[1] = LA_coords[j][i].y;
				
			LA_T[j][i] = T0 ; 
			
		}
	}
	ierr = DMDAVecRestoreArray(da,T,&LA_T);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(cda,coords,&LA_coords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}



/*pTatin2d_ModelSetInitialTemperatureOnNodes_GENE
 assign temperature to node from map that is in context
 */ 
#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetInitialTemperatureOnNodes_GENE"
PetscErrorCode pTatin2d_ModelSetInitialTemperatureOnNodes_GENE(pTatinCtx ctx)
/* define properties on material points */
{
	TempMap                map;
	PetscErrorCode         ierr;
	DM da;
	Vec T;
	PhysCompEnergyCtx phys_energy;
	PetscInt i,j,si,sj,nx,ny;
	Vec coords;
	DMDACoor2d **LA_coords;
	DM cda;
	PetscScalar **LA_T;
	
	
	PetscFunctionBegin;
	
	phys_energy = ctx->phys_energy;
	da          = phys_energy->daT;
	T           = phys_energy->T;
	
	//map = PETSC_NULL;
	//ierr = pTatinCtxGetTempMap(ctx,&map);CHKERRQ(ierr);
	
	map = PETSC_NULL;
	ierr = pTatinCtxGetModelData(ctx,"tempmap",(void**)&map);CHKERRQ(ierr);
	
	
	ierr = DMDAGetCoordinates(da,&coords);CHKERRQ(ierr);
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,coords,&LA_coords);CHKERRQ(ierr);
	
	ierr = DMDAVecGetArray(da,T,&LA_T);CHKERRQ(ierr);
	
	ierr = DMDAGetCorners(da,&si,&sj,0 , &nx,&ny,0);CHKERRQ(ierr);
	
	for( j=sj; j<sj+ny; j++ ) {
		for( i=si; i<si+nx; i++ ) {
			PetscScalar xn,yn,Tic;
			double position[2];
			
			position[0] = LA_coords[j][i].x;
			position[1] = LA_coords[j][i].y;
			
			TempMapGetTemperature(map,position,&Tic);
			
			LA_T[j][i] = Tic; 
			
		}
	}
	ierr = DMDAVecRestoreArray(da,T,&LA_T);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(cda,coords,&LA_coords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}







/*pTatin2d_ModelSetInitialStokesVariableOnMarker_GENE
 assign initial viscosity and density not compatible with Arrhenius or anything 
 anyway it is set during continuation but would be a good idea to set something not too stupid when using density array and arrhenius
 */ 
#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetInitialStokesVariableOnMarker_GENE"
PetscErrorCode pTatin2d_ModelSetInitialStokesVariableOnMarker_GENE(pTatinCtx ctx)
/* define properties on material points */
{
	PhaseMap               phasemap;
	PetscInt               e,p,n_mp_points;
	DataBucket             db;
	DataField              PField_std,PField_stokes;
	int                    phase_index, i;
	PetscErrorCode         ierr;
	RheologyConstants      *rheology;
	double                 eta_ND,rho_ND; 
	pTatinUnits            *units;	
	
	PetscFunctionBegin;
	units   = &ctx->units;
	
	rheology   = &ctx->rheology_constants;
	db = ctx->db;
	
	
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	for (p=0; p<n_mp_points; p++) {
		MPntStd     *material_point;
		MPntPStokes *mpprop_stokes;
		DataFieldAccessPoint(PField_std,p,(void**)&material_point);
		DataFieldAccessPoint(PField_stokes,p,(void**)&mpprop_stokes);
		MPntStdGetField_phase_index(material_point,&phase_index);
		
		UnitsApplyInverseScaling(units->si_viscosity,rheology->const_eta0[phase_index],&eta_ND);
		UnitsApplyInverseScaling(units->si_force_per_volume,rheology->const_rho0[phase_index],&rho_ND);
		MPntPStokesSetField_eta_effective(mpprop_stokes,eta_ND);
		MPntPStokesSetField_density(mpprop_stokes,rho_ND);	
	}
	
	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokes);
	
	PetscFunctionReturn(0);
}

/*pTatin2d_ModelSetInitialEnergyVariableOnMarker_GENE
 assign initial kappa and prod 
 */ 
#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetInitialEnergyVariableOnMarker_GENE"
PetscErrorCode pTatin2d_ModelSetInitialEnergyVariableOnMarker_GENE(pTatinCtx ctx)
{
	DataField         PField_thermal,PField_std,PField_stokes;
	DataBucket        db;
	int               phase_index;
	PetscInt          p,n_mp_points;
	PetscErrorCode    ierr;
	RheologyConstants *rheology;
	pTatinUnits *units;	
	TempMap                map;
	PetscBool         isproduction;
	char file[PETSC_MAX_PATH_LEN];
	
	PetscFunctionBegin;
	units   = &ctx->units;   
	rheology   = &ctx->rheology_constants;
	db = ctx->db;
	
	/* standard data */
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	/* thermal data */
	DataBucketGetDataFieldByName(db,MPntPThermal_classname,&PField_thermal);
	DataFieldGetAccess(PField_thermal);
	DataFieldVerifyAccess(PField_thermal,sizeof(MPntPThermal));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	isproduction = PETSC_FALSE;
	ierr = PetscOptionsGetString(PETSC_NULL,"-prod_file",file,PETSC_MAX_PATH_LEN-1,&isproduction);CHKERRQ(ierr);
	if (isproduction){
		isproduction = PETSC_TRUE;
		map =  PETSC_NULL;
		ierr = pTatinCtxGetModelData(ctx,"prod_map",(void**)&map);
	}
	
	for (p=0; p<n_mp_points; p++) {
		MPntStd      *material_point;
		MPntPThermal *material_point_thermal;
		double       *position,kappa_ND,prod_ND;
		
		DataFieldAccessPoint(PField_std,    p,(void**)&material_point);
		DataFieldAccessPoint(PField_thermal,p,(void**)&material_point_thermal);
		MPntStdGetField_phase_index(material_point,&phase_index);
		position = material_point->coor;
		
		UnitsApplyInverseScaling(units->si_diffusivity,rheology->temp_kappa[phase_index],&kappa_ND);
		MPntPThermalSetField_diffusivity(material_point_thermal,kappa_ND);
		
		prod_ND = 0.0;
		if (isproduction){
	
			TempMapGetTemperature(map,position,&prod_ND);
			prod_ND=prod_ND;/// what a fuck is that !!!! I guess I divide by cp..
		}

		MPntPThermalSetField_heat_prod(material_point_thermal,prod_ND);
		//PetscPrintf(PETSC_COMM_WORLD,"production set too %e  \n",prod_ND);
	}
	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_thermal);
	PetscFunctionReturn(0);    
}

#if 0

#undef __FUNCT__
#define __FUNCT__ "pTatin2d_ModelSetInitialConstantFluidPressureOnNodes_GENE"
PetscErrorCode pTatin2d_ModelSetInitialConstantFluidPressureOnNodes_GENE(pTatinCtx ctx)
/* define properties on material points */
{
    PetscErrorCode         ierr;
    DM da;
    Vec T;
    PhysCompEnergyCtx phys_energy;
    PetscInt i,j,si,sj,nx,ny;
    Vec coords;
    DMDACoor2d **LA_coords;
    DM cda;
    PetscScalar **LA_T,T0;
    
    
    PetscFunctionBegin;
    
    phys_energy = ctx->phys_energy;
    da          = phys_energy->daT;
    T           = phys_energy->T;
    
    //map = PETSC_NULL;
    //ierr = pTatinCtxGetTempMap(ctx,&map);CHKERRQ(ierr);
    
    ierr = PetscOptionsGetScalar(PETSC_NULL,"-initial_temperature",&T0,0);CHKERRQ(ierr);
    ierr = DMDAGetCoordinates(da,&coords);CHKERRQ(ierr);
    ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
    ierr = DMDAVecGetArray(cda,coords,&LA_coords);CHKERRQ(ierr);
    
    ierr = DMDAVecGetArray(da,T,&LA_T);CHKERRQ(ierr);
    
    ierr = DMDAGetCorners(da,&si,&sj,0 , &nx,&ny,0);CHKERRQ(ierr);
    
    for( j=sj; j<sj+ny; j++ ) {
        for( i=si; i<si+nx; i++ ) {
            PetscScalar xn,yn,Tic;
            double position[2];
            
            position[0] = LA_coords[j][i].x;
            position[1] = LA_coords[j][i].y;
            
            LA_T[j][i] = T0 ;
            
        }
    }
    ierr = DMDAVecRestoreArray(da,T,&LA_T);CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(cda,coords,&LA_coords);CHKERRQ(ierr);
    
    PetscFunctionReturn(0);
}



/*pTatin2d_ModelSetInitialFluidPressureOnNodes_GENE
 assign temperature to node from map that is in context
 */
#undef __FUNCT__
#define __FUNCT__ "pTatin2d_ModelSetInitialFluidPressureOnNodes_GENE"
PetscErrorCode pTatin2d_ModelSetFluidPressureOnNodes_GENE(pTatinCtx ctx)
/* define properties on material points */
{
    TempMap                map;
    PetscErrorCode         ierr;
    DM da;
    Vec T;
    PhysCompEnergyCtx phys_energy;
    PetscInt i,j,si,sj,nx,ny;
    Vec coords;
    DMDACoor2d **LA_coords;
    DM cda;
    PetscScalar **LA_T;
    
    
    PetscFunctionBegin;
    
    phys_energy = ctx->phys_energy;
    da          = phys_energy->daT;
    T           = phys_energy->T;
    
    //map = PETSC_NULL;
    //ierr = pTatinCtxGetTempMap(ctx,&map);CHKERRQ(ierr);
    
    map = PETSC_NULL;
    ierr = pTatinCtxGetModelData(ctx,"tempmap",(void**)&map);CHKERRQ(ierr);
    
    
    ierr = DMDAGetCoordinates(da,&coords);CHKERRQ(ierr);
    ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
    ierr = DMDAVecGetArray(cda,coords,&LA_coords);CHKERRQ(ierr);
    
    ierr = DMDAVecGetArray(da,T,&LA_T);CHKERRQ(ierr);
    
    ierr = DMDAGetCorners(da,&si,&sj,0 , &nx,&ny,0);CHKERRQ(ierr);
    
    for( j=sj; j<sj+ny; j++ ) {
        for( i=si; i<si+nx; i++ ) {
            PetscScalar xn,yn,Tic;
            double position[2];
            
            position[0] = LA_coords[j][i].x;
            position[1] = LA_coords[j][i].y;
            
            TempMapGetTemperature(map,position,&Tic);
            
            LA_T[j][i] = Tic;
            
        }
    }
    ierr = DMDAVecRestoreArray(da,T,&LA_T);CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(cda,coords,&LA_coords);CHKERRQ(ierr);
    
    PetscFunctionReturn(0);
}



{
#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetInitialDarcyVariableOnMarker_GENE"
PetscErrorCode pTatin2d_ModelSetInitialDarcyVariableOnMarker_GENE(pTatinCtx ctx)
{
	DataField         PField_darcy,PField_std;
	DataBucket        db;
	int               phase_index;
	PetscInt          p,n_mp_points;
	PetscErrorCode    ierr;
	DarcyConstants    *darcy;
	pTatinUnits       *units;	
	TempMap           map;
	PetscBool         iswaterinit;
	char file[PETSC_MAX_PATH_LEN];
	
	PetscFunctionBegin;
	units   = &ctx->units;   
	darcy   = &ctx->darcy_constants;
	db = ctx->db;
	
	/* standard data */
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	/* darcy data */
	DataBucketGetDataFieldByName(db,MPntPDarcy_classname,&PField_darcy);
	DataFieldGetAccess(PField_darcy);
	DataFieldVerifyAccess(PField_darcy,sizeof(MPntPDarcy));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	iswaterinit = PETSC_FALSE;
	
	ierr = PetscOptionsGetString(PETSC_NULL,"-source_file",file,PETSC_MAX_PATH_LEN-1,&iswaterinit);CHKERRQ(ierr);
	if (iswaterinit){
		map =  PETSC_NULL;
		ierr = pTatinCtxGetModelData(ctx,"source_map",(void**)&map);
	}
	
	for (p=0; p<n_mp_points; p++) {
		MPntStd      *material_point;
		MPntPDarcy   *material_point_darcy;
		double       *position,kappa_ND,prod_ND;
		
		DataFieldAccessPoint(PField_std,    p,(void**)&material_point);
		DataFieldAccessPoint(PField_darcy,p,(void**)&material_point_darcy);
		MPntStdGetField_phase_index(material_point,&phase_index);
		position = material_point->coor;
		
		UnitsApplyInverseScaling(units->si_diffusivity,rheology->temp_kappa[phase_index],&kappa_ND);
		MPntPThermalSetField_diffusivity(material_point_thermal,kappa_ND);
		
		prod_ND = 0.0;
		if (iswaterinit){
			TempMapGetTemperature(map,position,&prod_ND);
		}

		MPntPThermalSetField_heat_prod(material_point_thermal,prod_ND);
		
		
		
        MPntPDarcySetField_porosity(MPntPDarcy *point,double data);
        MPntPDarcySetField_free_fluid(MPntPDarcy *point,double data);
       
		
		
	}
	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_thermal);
	PetscFunctionReturn(0);    
}
}
#endif


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetInitialPlsVariableOnMarker_GENE"
PetscErrorCode pTatin2d_ModelSetInitialPlsVariableOnMarker_GENE(pTatinCtx ctx)
/* define properties on material points */
{
	PhaseMap               phasemap;
	PetscInt               e,p,n_mp_points;
	DataBucket             db;
	DataField              PField_std,PField_stokes,PField_stokespl;
	int                    phase_index, i;
	PetscErrorCode         ierr;
	RheologyConstants      *rheology;
	PetscBool              addnoise=PETSC_FALSE;
	
	PetscFunctionBegin;
	
	rheology   = &ctx->rheology_constants;
	db = ctx->db;
	
	
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	/* get the plastic variables */
	DataBucketGetDataFieldByName(db,MPntPStokesPl_classname,&PField_stokespl);
	DataFieldGetAccess(PField_stokespl);
	DataFieldVerifyAccess(PField_stokespl,sizeof(MPntPStokesPl));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
    ierr = PetscOptionsGetBool(0,"-GENE_add_noise",&addnoise,0);CHKERRQ(ierr);
		
	if (addnoise){
		PetscScalar sigma,sigma2_1,mu,eps_max,eps_rand,eps_init,xcoor;
		PetscBool   found;
	    pTatinUnits *units;
		units   = &ctx->units;
		
		ierr = PetscOptionsGetReal(0,"-GENE_noise_sigma",&sigma,&found);CHKERRQ(ierr);
		if (found == PETSC_FALSE) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option -GENE_noise_sigma \n");
		ierr = PetscOptionsGetReal(0,"-GENE_noise_centre",&mu,&found);CHKERRQ(ierr);
		if (found == PETSC_FALSE) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option -GENE_noise_centre \n");	
		ierr = PetscOptionsGetReal(0,"-GENE_noise_max_eps",&eps_max,&found);CHKERRQ(ierr);
		if (found == PETSC_FALSE) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option -GENE_noise_max_eps \n");	

	    sigma2_1 = -1.0/(2*pow(sigma,2));
	
	for (p=0; p<n_mp_points; p++) {
		MPntStd       *material_point;
		MPntPStokesPl *mpprop_stokespl; /* plastic variables */
	    PetscScalar   *position;
	    PetscScalar   eps_init;
	    
	    DataFieldAccessPoint(PField_std,p,(void**)&material_point);
		DataFieldAccessPoint(PField_stokespl,p,(void**)&mpprop_stokespl);
	    
	    MPntStdGetField_global_coord(material_point,&position);
	    UnitsApplyScaling(units->si_length,position[0],&xcoor);
	    
	    eps_rand  = eps_max * rand() / (RAND_MAX + 1.0);
		eps_init  = eps_rand*exp(sigma2_1*pow(xcoor-mu,2.0));
        
		
		MPntPStokesPlSetField_plastic_strain(mpprop_stokespl,eps_init);
		
		MPntPStokesPlSetField_yield_indicator(mpprop_stokespl,0.0);
	    }
	}else{
	
	for (p=0; p<n_mp_points; p++) {
		MPntStd     *material_point;
		MPntPStokesPl *mpprop_stokespl; /* plastic variables */
		DataFieldAccessPoint(PField_std,p,(void**)&material_point);
		DataFieldAccessPoint(PField_stokespl,p,(void**)&mpprop_stokespl);
		
		MPntStdGetField_phase_index(material_point,&phase_index);
		MPntPStokesPlSetField_plastic_strain(mpprop_stokespl,0.0);
		
		MPntPStokesPlSetField_yield_indicator(mpprop_stokespl,0.0);
	    }	
	}
	
	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokespl);
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetInitialMeltVariableOnMarker_GENE"
PetscErrorCode pTatin2d_ModelSetInitialMeltVariableOnMarker_GENE(pTatinCtx ctx)
/* define properties on material points */
{
	PetscInt               e,p,n_mp_points;
	DataBucket             db;
	DataField              PField_stokesmelt;
	PetscErrorCode         ierr;
	
	PetscFunctionBegin;
	
	db = ctx->db;
	
	/* get the melt variables */
	DataBucketGetDataFieldByName(db,MPntPStokesMelt_classname,&PField_stokesmelt);
	DataFieldGetAccess(PField_stokesmelt);
	DataFieldVerifyAccess(PField_stokesmelt,sizeof(MPntPStokesMelt));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	for (p=0; p<n_mp_points; p++) {
		MPntPStokesMelt *mpprop_stokesmelt; /* melt variables */
		MPntPStokesMeltSetField_melt_portion(mpprop_stokesmelt,0.0);
		}
	
	DataFieldRestoreAccess(PField_stokesmelt);
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetInitialAgesVariableOnMarker_GENE"
PetscErrorCode pTatin2d_ModelSetInitialAgesVariableOnMarker_GENE(pTatinCtx ctx)
/* define properties on material points */
{
	PetscInt               e,p,n_mp_points;
	DataBucket             db;
	DataField              PField_chrono;
	float  ages[]={0.0,0.0,0.0};
	PetscErrorCode         ierr;
	PetscBool              store_ages=PETSC_FALSE;
	
	PetscFunctionBegin;
	ierr = PetscOptionsGetBool(PETSC_NULL,"-store_ages",&store_ages,0);CHKERRQ(ierr);    
 	  
 	  if (store_ages){
 	  
	db = ctx->db;
	
	/* get the melt variables */
	DataBucketGetDataFieldByName(db,MPntPChrono_classname,&PField_chrono);
	DataFieldGetAccess(PField_chrono);
	DataFieldVerifyAccess(PField_chrono,sizeof(MPntPChrono));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	for (p=0; p<n_mp_points; p++) {
		MPntPChrono *mpprop_chrono; /* ages variables */
		MPntPChronoSetField_age120(mpprop_chrono,-1.0);
		MPntPChronoSetField_age350(mpprop_chrono,-1.0);
		MPntPChronoSetField_age800(mpprop_chrono,-1.0);
		}
	
	DataFieldRestoreAccess(PField_chrono);
	 }
	PetscFunctionReturn(0);
}





#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_GENE"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_GENE(pTatinCtx ctx)
{
	BCList         ubclist;
	PetscScalar    bcval;
	DM             dav;
	PetscErrorCode ierr;
	
	PetscReal jmin[3],jmax[3]; 
	PetscFunctionBegin;
	PetscScalar opts_srH, opts_Lx,opts_Ly,p_base;
	PetscInt opts_bcs;
	PetscScalar alpha_m;
	pTatinUnits *units;
	BC_Alabeaumont bcdata;
	BC_Step   bcdatastep;
	PetscScalar y1,dy,coeff;
    PetscScalar timeSI,time_end;
    PetscScalar xi,xe,x0,vel;
    BC_Godfinger bcgod;
    

	units   = &ctx->units;
	ubclist = ctx->u_bclist;
	dav     = ctx->dav;
	
	opts_srH = 0.0;
 	ierr = PetscOptionsGetScalar(PETSC_NULL,"-vx",&opts_srH,0);CHKERRQ(ierr);
	
	UnitsApplyInverseScaling(units->si_velocity,opts_srH,&opts_srH);
	
	opts_bcs = 0;
	ierr = PetscOptionsGetInt(PETSC_NULL,"-bc_type",&opts_bcs,0);CHKERRQ(ierr);
	
	
	DMDAGetBoundingBox(dav,jmin,jmax);
	opts_Lx = jmax[0]-jmin[0]; 
	opts_Ly = jmax[1]-jmin[1];
    
   
	
	switch(opts_bcs){
			// free surface and free slip all side with symetric horizontal velocity
		case 0 : {
			bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
		}
			break; 
			
			// incompressible symetric bottom fixed 
		case 1 : {
			bcval = -opts_srH*2.0*opts_Ly/opts_Lx;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
		}
			break; 
			
			// incompressible right side  
		case 2 : {
			bcval = -opts_srH*opts_Ly/opts_Lx;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
		} bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			break;
			
			// incompressible 4 sides  
		case 3 : {      
			bcval = -opts_srH*opts_Ly/opts_Lx;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
			bcval = opts_srH*opts_Ly/opts_Lx;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
			bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
		}
			break;
			
			// free slip everywhere  
		case 4 :    
			bcval = 0.0;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			break;      
			// free surface both side with mid point fixed 
			
		case 5 :    
			bcval = opts_srH;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_MIDPOINT,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_MIDPOINT,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);      
			break;
			
			// free surface right side  
		case 6 : {
			
			bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
		}  
			break;
			
			// free surface left side  
		case 7 : {
			
			bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = 0.0;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
		}
			break;
			// free surface and rigid wall on lateral side with symmetric horizontal velocity
		case 8 : {
			bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = 0.0;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = 0.0;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
		}
			break; 
			
		case 9 : {
		    BC_WrinklerData bcdata;
		    PetscScalar p_base,deltarho,yref,gravity_acceleration;
            PetscInt remesh_type; 
            PetscBool found;
            
			bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

			bcval = 0.0;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_BOTLEFTCORNERPOINT,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_BOTRIGHTCORNERPOINT,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr); 	
			
			/* basement pressure */
			PetscOptionsGetInt(PETSC_NULL,"-remesh_type",&remesh_type,0);
			if (remesh_type != 6 && remesh_type!=7){
			SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"In order to use a wrinkler foundation, you need to set -remesh_type option to 6 \n");
			}
		    ierr = PetscMalloc(sizeof(struct _p_BC_WrinklerData),&bcdata);CHKERRQ(ierr);
			ierr = PetscOptionsGetScalar(PETSC_NULL,"-winkler_p_base",&p_base,0);CHKERRQ(ierr);
		    UnitsApplyInverseScaling(units->si_stress,p_base,&p_base);
		    bcdata->Pisos=p_base;
		    
		    ierr = PetscOptionsGetScalar(PETSC_NULL,"-winkler_yref",&yref,0);CHKERRQ(ierr);
		    UnitsApplyInverseScaling(units->si_length,yref,&yref);
		    bcdata->yref=yref;
		    ierr  = PetscOptionsGetReal(PETSC_NULL,"-grav",&gravity_acceleration,&found);CHKERRQ(ierr);
		    if (found == PETSC_FALSE){
			SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"In order to use a wrinkler foundation, you need to set -grav option  \n");
			}
		    
		    ierr = PetscOptionsGetScalar(PETSC_NULL,"-winkler_deltarho",&deltarho,0);CHKERRQ(ierr);
		    UnitsApplyInverseScaling(units->si_force_per_volume,deltarho,&deltarho);
		    bcdata->deltarhog=deltarho*gravity_acceleration;
			
			ierr = SurfaceQuadratureStokesTraverseSetTraction(ctx->surfQ,DMDATracLoc_JMIN_LOC,SurfaceQuadatureEvaluator_Wrinkler,(void*)bcdata);CHKERRQ(ierr);
		    ierr = PetscFree(bcdata);CHKERRQ(ierr);
		}
			break; 	
			
	    case 10 : {
		    BC_WrinklerData bcdata;
		    PetscScalar p_base,deltarho,yref,gravity_acceleration;
            PetscInt remesh_type; 
            PetscBool found;
            
			bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

			bcval = 0.0;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_MIDPOINT,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_MIDPOINT,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
			/* basement pressure */
			PetscOptionsGetInt(PETSC_NULL,"-remesh_type",&remesh_type,0);
			if (remesh_type != 7){
			SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"In order to use a wrinkler foundation, you need to set -remesh_type option to 7 \n");
			}
		    ierr = PetscMalloc(sizeof(struct _p_BC_WrinklerData),&bcdata);CHKERRQ(ierr);
			ierr = PetscOptionsGetScalar(PETSC_NULL,"-winkler_p_base",&p_base,0);CHKERRQ(ierr);
		    UnitsApplyInverseScaling(units->si_stress,p_base,&p_base);
		    bcdata->Pisos=p_base;
		    
		    ierr = PetscOptionsGetScalar(PETSC_NULL,"-winkler_yref",&yref,0);CHKERRQ(ierr);
		    UnitsApplyInverseScaling(units->si_length,yref,&yref);
		    bcdata->yref=yref;
		    ierr  = PetscOptionsGetReal(PETSC_NULL,"-grav",&gravity_acceleration,&found);CHKERRQ(ierr);
		    if (found == PETSC_FALSE){
			SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"In order to use a wrinkler foundation, you need to set -grav option  \n");
			}
		    
		    ierr = PetscOptionsGetScalar(PETSC_NULL,"-winkler_deltarho",&deltarho,0);CHKERRQ(ierr);
		    UnitsApplyInverseScaling(units->si_force_per_volume,deltarho,&deltarho);
		    bcdata->deltarhog=deltarho*gravity_acceleration;
			
			ierr = SurfaceQuadratureStokesTraverseSetTraction(ctx->surfQ,DMDATracLoc_JMIN_LOC,SurfaceQuadatureEvaluator_Wrinkler,(void*)bcdata);CHKERRQ(ierr);
		    ierr = PetscFree(bcdata);CHKERRQ(ierr);
		}
			break;
            
        case 11 : {
			//opts_srH0 = opts_srH;
			//bctime_0  = 0.0;
			//bctime_1  = 0.0;
			//ierr = PetscOptionsGetScalar(PETSC_NULL,"-vx0",&opts_srH0,0);CHKERRQ(ierr);
			//ierr = PetscOptionsGetScalar(PETSC_NULL,"-bctime0",&bc_time_0,0);CHKERRQ(ierr);
			//ierr = PetscOptionsGetScalar(PETSC_NULL,"-bctime1",&bctime_1,0);CHKERRQ(ierr);
			/*incompressible free surface, basically fixe the velocity at the bottom to fill up or empty down with material that leaves on the side*/
			//if ctx->time 
			//opts_srH = opts_srH0+(ctx->time-bctime_0)/(bctime_1-bctime_0)*(opts_srH-opts_srH0);
			
			bcval = 2*opts_srH*opts_Ly/opts_Lx;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
			bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
		}
			break;
     
		case 12 : {
			ierr = PetscMalloc(sizeof(struct _p_BC_Alabeaumont),&bcdata);CHKERRQ(ierr);
			
			/*incompressible free surface à la beaumont*/
			ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_ylitho",&y1,0);CHKERRQ(ierr);
			ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_deltay",&dy,0);CHKERRQ(ierr);
			
			UnitsApplyInverseScaling(units->si_length,y1,&y1);
			UnitsApplyInverseScaling(units->si_length,dy,&dy);
			
			bcval = 0.0;
			bcdata->dim = 1;   
			bcdata->x0 = y1;   
			bcdata->x1 = y1-dy;
			coeff = (2*(jmax[1]-y1)+dy)/(2*(y1-dy-jmin[1]));
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
			bcdata->v0 = -opts_srH; bcdata->v1 = opts_srH*coeff; 
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_Alabeaumont,(void*)bcdata);CHKERRQ(ierr);	
			bcdata->v0 = opts_srH; bcdata->v1 = -opts_srH*coeff;
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_Alabeaumont,(void*)bcdata);CHKERRQ(ierr);
			ierr = PetscFree(bcdata);CHKERRQ(ierr);
		}
			break;	
			
		case 13 : {
			ierr = PetscMalloc(sizeof(struct _p_BC_Alabeaumont),&bcdata);CHKERRQ(ierr);
			
			/*incompressible free surface à la beaumont*/
			ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_ylitho",&y1,0);CHKERRQ(ierr);
			ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_deltay",&dy,0);CHKERRQ(ierr);
			
			UnitsApplyInverseScaling(units->si_length,y1,&y1);
			UnitsApplyInverseScaling(units->si_length,dy,&dy);
			
			bcval = 0.0;
			bcdata->dim = 1;   
			bcdata->x0 = y1;   
			bcdata->x1 = y1-dy;
			coeff = (2*(jmax[1]-y1)+dy)/(2*(y1-dy-jmin[1]));
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
			bcdata->v0 = -opts_srH; bcdata->v1 = opts_srH*coeff; 
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_Alabeaumont,(void*)bcdata);CHKERRQ(ierr);	
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcdata->v0 = opts_srH; bcdata->v1 = -opts_srH*coeff;
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_Alabeaumont,(void*)bcdata);CHKERRQ(ierr);
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			ierr = PetscFree(bcdata);CHKERRQ(ierr);
		}
			break;	
			
			
		case 14 : {
			ierr = PetscMalloc(sizeof(struct _p_BC_Alabeaumont),&bcdata);CHKERRQ(ierr);
			ierr =  PetscMalloc(sizeof(struct _p_BC_Step),&bcdatastep);CHKERRQ(ierr);
			
			/*incompressible free surface à la beaumont*/
			ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_ylitho",&y1,0);CHKERRQ(ierr);
			ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_deltay",&dy,0);CHKERRQ(ierr);
			
			UnitsApplyInverseScaling(units->si_length,y1,&y1);
			UnitsApplyInverseScaling(units->si_length,dy,&dy);
			
			bcval = 0.0;
			bcdata->dim = 1;   
			bcdata->x0 = y1;   
			bcdata->x1 = y1-dy;
			bcdatastep->beg = y1-dy; 
			bcdatastep->end = y1;
			bcdatastep->dim = 1; 
			bcdatastep->v0 = 0.0;
			bcdatastep->v1 = 0.0;
			bcdatastep->b = 0; 
			bcdatastep->a = 1.0;
			
			coeff = (dy+2*jmax[1]-2*y1)/(2*y1-2*jmin[1]-dy);
			
			
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
			bcdata->v0 = -opts_srH; bcdata->v1 = opts_srH*coeff; 
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_Alabeaumont,(void*)bcdata);CHKERRQ(ierr);	
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_Step,(void*)bcdatastep);CHKERRQ(ierr);
			bcdata->v0 = opts_srH; bcdata->v1 = -opts_srH*coeff;
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_Alabeaumont,(void*)bcdata);CHKERRQ(ierr);
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,1,BCListEvaluator_Step,(void*)bcdatastep);CHKERRQ(ierr);
			ierr = PetscFree(bcdata);CHKERRQ(ierr);
			ierr = PetscFree(bcdatastep);CHKERRQ(ierr);
		}
			break;	
			
		case 15 : {
			ierr = PetscMalloc(sizeof(struct _p_BC_Alabeaumont),&bcdata);CHKERRQ(ierr);
			ierr =  PetscMalloc(sizeof(struct _p_BC_Step),&bcdatastep);CHKERRQ(ierr);
			
			/*incompressible free surface à la beaumont*/
			ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_ylitho",&y1,0);CHKERRQ(ierr);
			ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_deltay",&dy,0);CHKERRQ(ierr);
			
			UnitsApplyInverseScaling(units->si_length,y1,&y1);
			UnitsApplyInverseScaling(units->si_length,dy,&dy);
			
			bcval = 0.0;
			bcdata->dim = 1;   
			bcdata->x0 = y1;   
			bcdata->x1 = y1-dy;
			bcdatastep->beg = y1-dy; 
			bcdatastep->end = y1;
			bcdatastep->dim = 1; 
			bcdatastep->v0 = 0.0;
			bcdatastep->v1 = 0.0;
			bcdatastep->b = 0; 
			bcdatastep->a = 1.0;
			
			coeff = (dy+2*jmax[1]-2*y1)/(2*y1-2*jmin[1]-dy);
			
			
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
			
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			
			bcdata->v0 = opts_srH; bcdata->v1 = -opts_srH*coeff;
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_Alabeaumont,(void*)bcdata);CHKERRQ(ierr);
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			ierr = PetscFree(bcdata);CHKERRQ(ierr);
			ierr = PetscFree(bcdatastep);CHKERRQ(ierr);
		}
			break;	
			
        case 16 : {
			PetscInt rigid,freeslip;
			// Geomod BC's
            
			ierr =  PetscMalloc(sizeof(struct _p_BC_Step),&bcdatastep);CHKERRQ(ierr);
			bcval = 0.0;
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
            ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
            ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
            
            rigid = 0;
            freeslip = 0;
            ierr = PetscOptionsGetInt(PETSC_NULL,"-bc_rigid",&rigid,0);CHKERRQ(ierr);  
            ierr = PetscOptionsGetInt(PETSC_NULL,"-bc_freeslip",&freeslip,0);CHKERRQ(ierr); 
            if (rigid==1){
                bcdatastep->beg = -1.0; 
                bcdatastep->end = 2.8;
                bcdatastep->dim = 1; 
                bcdatastep->v0 = bcval;
                bcdatastep->v1 = bcval;
                bcdatastep->b = -0.0; 
                bcdatastep->a = -0.5;
                ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,1,BCListEvaluator_Step,(void*)bcdatastep);CHKERRQ(ierr);
            }
            
            if (rigid ==2){       
                ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);    
            }
            
            if (freeslip == 0){
                bcval = 0.0;
                ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
                bcval = -2.5; 
                bcdatastep->beg = -1.0; 
                bcdatastep->end = 10;
                bcdatastep->dim = 1; 
                bcdatastep->v0 = bcval;
                bcdatastep->v1 = 0.0;
                bcdatastep->b = -0.0; 
                bcdatastep->a = -0.2;
                ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_Step,(void*)bcdatastep);CHKERRQ(ierr);
            } 
            
            if (freeslip ==1){
                bcval = -2.5; 
                ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
                ierr = PetscFree(bcdatastep);CHKERRQ(ierr);            
            }
        }
            break;
            
         case 17 : {
   
      UnitsApplyScaling(units->si_time,ctx->time,&timeSI);
      ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_Godfinger_time",&time_end,0);CHKERRQ(ierr);

			if (timeSI < time_end) {

          PetscPrintf(PETSC_COMM_WORLD,"pTatin2d_MODEL_APPLYBC_GENE in bcgodfinger\n");
	
					ierr = PetscMalloc(sizeof(struct _p_BC_Godfinger),&bcgod);CHKERRQ(ierr);
					ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_Godfinger_begin",&xi,0);CHKERRQ(ierr);
					ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_Godfinger_end"  ,&xe,0);CHKERRQ(ierr);
					ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_Godfinger_center",&x0,0);CHKERRQ(ierr);
					ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_Godfinger_velocity",&vel,0);CHKERRQ(ierr);
          UnitsApplyInverseScaling(units->si_velocity,vel,&vel);
          UnitsApplyInverseScaling(units->si_length,xe,&xe);
          UnitsApplyInverseScaling(units->si_length,x0,&x0);
          UnitsApplyInverseScaling(units->si_length,xi,&xi);
					bcgod->vx = vel;   
					bcgod->x0 = x0;   
					bcgod->xi = xi;
					bcgod->xe = xe;
					ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,0,BCListEvaluator_Godfinger,(void*)bcgod);CHKERRQ(ierr);
          bcval = 0;
          ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			    ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			    ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      }  else {
       /*incompressible free surface, basically fixe the velocity at the bottom to fill up or empty down with material that leaves on the side*/
       /* basically apply bctype 11*/
			bcval = 2*opts_srH*opts_Ly/opts_Lx;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
			bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      } 
		}
			break; 
			/* 3 next are strange BC to test the ALE component for DD*/ 
        case 18 : {
            bcval = 0.0;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
            }
            break;
            
        case 19 : {
            bcval = 0.0;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
            }
            break;
            
        case 20 : {
            bcval = opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
            }
            break;
            /* SIMPLE  */
        case 21 : {
            PetscInt fixvel  = 0; 
            PetscScalar conf = 0.0; 
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_IO_confining",&conf,0);CHKERRQ(ierr);
			UnitsApplyInverseScaling(units->si_stress,conf,&conf);
            conf = -conf; 
            
            bcval = opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = -opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			ierr = SurfaceQuadratureStokesTraverseSetTraction(ctx->surfQ,DMDATracLoc_IMIN_LOC,SurfaceQuadatureEvaluator_Isotropic,(void*)&conf);CHKERRQ(ierr);
            ierr = SurfaceQuadratureStokesTraverseSetTraction(ctx->surfQ,DMDATracLoc_IMAX_LOC,SurfaceQuadatureEvaluator_Isotropic,(void*)&conf);CHKERRQ(ierr);
            
            ierr = PetscOptionsGetInt(PETSC_NULL,"-bc_IO_vy0",&fixvel,0);CHKERRQ(ierr);
			
			if(fixvel == 1) {
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
            ierr = SurfaceQuadratureStokesTraverseSetTraction(ctx->surfQ,DMDATracLoc_JMAX_LOC,SurfaceQuadatureEvaluator_Isotropic,(void*)&conf);CHKERRQ(ierr);
			ierr = SurfaceQuadratureStokesTraverseSetTraction(ctx->surfQ,DMDATracLoc_JMIN_LOC,SurfaceQuadatureEvaluator_Isotropic,(void*)&conf);CHKERRQ(ierr);
			}else if (fixvel==2){
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			}else if(fixvel ==0) {
			ierr = SurfaceQuadratureStokesTraverseSetTraction(ctx->surfQ,DMDATracLoc_JMAX_LOC,SurfaceQuadatureEvaluator_Isotropic,(void*)&conf);CHKERRQ(ierr);
			//ierr = SurfaceQuadratureStokesTraverseSetTraction(ctx->surfQ,DMDATracLoc_JMIN_LOC,SurfaceQuadatureEvaluator_Isotropic,(void*)&conf);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			}
            }
            break;
            
        case 22 : {
            PetscScalar delta_t, delta_v,time_change;
            
            UnitsApplyScaling(units->si_time,ctx->time,&timeSI);
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_Godfinger_time",&time_end,0);CHKERRQ(ierr);
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_plate_time",&time_change,0);CHKERRQ(ierr);
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_plate_delta_t",&delta_t,0);CHKERRQ(ierr);
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_plate_delta_v",&delta_v,0);CHKERRQ(ierr);
            UnitsApplyInverseScaling(units->si_velocity,delta_v,&delta_v);
        
            
            if (timeSI > time_change+delta_t) {
                opts_srH = opts_srH + delta_v;
            } else {
                if (timeSI > time_change) {
                    opts_srH = opts_srH+delta_v*(timeSI-time_change)/delta_t;
                }
            }
            
            PetscPrintf(PETSC_COMM_WORLD,"pTatin2d_MODEL_APPLYBC_GENE vx=%e, time=%e \n",opts_srH,ctx->time);
             ierr = PetscMalloc(sizeof(struct _p_BC_Alabeaumont),&bcdata);CHKERRQ(ierr);
            
            if (timeSI < time_end) {
                
                PetscPrintf(PETSC_COMM_WORLD,"pTatin2d_MODEL_APPLYBC_GENE in bcgodfinger\n");
                
                ierr = PetscMalloc(sizeof(struct _p_BC_Godfinger),&bcgod);CHKERRQ(ierr);
                ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_Godfinger_begin",&xi,0);CHKERRQ(ierr);
                ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_Godfinger_end"  ,&xe,0);CHKERRQ(ierr);
                ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_Godfinger_center",&x0,0);CHKERRQ(ierr);
                
                UnitsApplyInverseScaling(units->si_length,xe,&xe);
                UnitsApplyInverseScaling(units->si_length,x0,&x0);
                UnitsApplyInverseScaling(units->si_length,xi,&xi);
                bcgod->vx = opts_srH;
                bcgod->x0 = x0;
                bcgod->xi = xi;
                bcgod->xe = xe;
                ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,0,BCListEvaluator_Godfinger,(void*)bcgod);CHKERRQ(ierr);
            }
            
            
            
                
                /*incompressible free surface à la beaumont*/
                ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_ylitho",&y1,0);CHKERRQ(ierr);
                ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_deltay",&dy,0);CHKERRQ(ierr);
                
                UnitsApplyInverseScaling(units->si_length,y1,&y1);
                UnitsApplyInverseScaling(units->si_length,dy,&dy);
                
                bcval = 0.0;
                bcdata->dim = 1;
                bcdata->x0 = y1;
                bcdata->x1 = y1-dy;
            
                
                coeff = (dy+2*jmax[1]-2*y1)/(2*y1-2*jmin[1]-dy);
                
                
                ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
                
                
                ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
                
                bcdata->v0 = opts_srH; bcdata->v1 = -opts_srH*coeff;
                ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_Alabeaumont,(void*)bcdata);CHKERRQ(ierr);
                ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
                ierr = PetscFree(bcdata);CHKERRQ(ierr);
            
        }
            break;
            
        case 23 : {
            PetscScalar delta_t, delta_v,time_change;
            
            UnitsApplyScaling(units->si_time,ctx->time,&timeSI);
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_Godfinger_time",&time_end,0);CHKERRQ(ierr);
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_plate_time",&time_change,0);CHKERRQ(ierr);
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_plate_delta_t",&delta_t,0);CHKERRQ(ierr);
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_plate_delta_v",&delta_v,0);CHKERRQ(ierr);
            UnitsApplyInverseScaling(units->si_velocity,delta_v,&delta_v);
            
            
            if (timeSI > time_change+delta_t) {
                opts_srH = opts_srH + delta_v;
            } else {
                if (timeSI > time_change) {
                    opts_srH = opts_srH+delta_v*(timeSI-time_change)/delta_t;
                }
            }
            
            PetscPrintf(PETSC_COMM_WORLD,"pTatin2d_MODEL_APPLYBC_GENE vx=%e, time=%e \n",opts_srH,ctx->time);
            ierr = PetscMalloc(sizeof(struct _p_BC_Alabeaumont),&bcdata);CHKERRQ(ierr);
            
            if (timeSI < time_end) {
                
                PetscPrintf(PETSC_COMM_WORLD,"pTatin2d_MODEL_APPLYBC_GENE in bcgodfinger\n");
                
                ierr = PetscMalloc(sizeof(struct _p_BC_Godfinger),&bcgod);CHKERRQ(ierr);
                ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_Godfinger_begin",&xi,0);CHKERRQ(ierr);
                ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_Godfinger_end"  ,&xe,0);CHKERRQ(ierr);
                ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_Godfinger_center",&x0,0);CHKERRQ(ierr);
                
                UnitsApplyInverseScaling(units->si_length,xe,&xe);
                UnitsApplyInverseScaling(units->si_length,x0,&x0);
                UnitsApplyInverseScaling(units->si_length,xi,&xi);
                bcgod->vx = opts_srH;
                bcgod->x0 = x0;
                bcgod->xi = xi;
                bcgod->xe = xe;
                ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,0,BCListEvaluator_Godfinger,(void*)bcgod);CHKERRQ(ierr);
            }
            
            
            
            
            /*incompressible free surface à la beaumont*/
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_ylitho",&y1,0);CHKERRQ(ierr);
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_deltay",&dy,0);CHKERRQ(ierr);
            
            UnitsApplyInverseScaling(units->si_length,y1,&y1);
            UnitsApplyInverseScaling(units->si_length,dy,&dy);
            
            bcval = 0.0;
            bcdata->dim = 1;
            bcdata->x0 = y1;
            bcdata->x1 = y1-dy;
            
            
            coeff = (dy+2*jmax[1]-2*y1)/(2*y1-2*jmin[1]-dy);
            
            
            ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
            
            
            ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
            
            bcdata->v0 = opts_srH; bcdata->v1 = -opts_srH*coeff;
            ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_Alabeaumont,(void*)bcdata);CHKERRQ(ierr);
            ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
            ierr = PetscFree(bcdata);CHKERRQ(ierr);
            
        }
            break;
            
        case 24 : {
            BC_Geomod3   bcdatag;
            PetscScalar Lx,Ly;
/*
            PetscBool BCListEvaluator_Geomod3(PetscScalar pos[],PetscScalar *value,void *ctx)
            {
                PetscBool apply_constraint=PETSC_FALSE;
                BC_Geomod3 data = (BC_Geomod3)ctx;
                PetscScalar posdisc;
                
                posdisc = data->x0 + data->vx*data->time;
                
                if (pos[data->dim] >= posdisc) {
                    *value = data->vx;
                    apply_constraint = PETSC_TRUE;
                } 
                
                return apply_constraint;
            }
 */
            
            ierr =  PetscMalloc(sizeof(struct _p_BC_Geomod3),&bcdatag);CHKERRQ(ierr);
            ierr  = PetscOptionsGetScalar(PETSC_NULL,"-Lx",&Lx,0);CHKERRQ(ierr);
            ierr  = PetscOptionsGetScalar(PETSC_NULL,"-Oy",&Ly,0);CHKERRQ(ierr);
            UnitsApplyInverseScaling(units->si_length,Lx+2*Ly,&bcdatag->x0);
            
            PetscPrintf(PETSC_COMM_WORLD,"pTatin2d_BC => %f\n",Lx);
            /*incompressible free surface à la beaumont*/
            /*
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_ylitho",&y1,0);CHKERRQ(ierr);
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_deltay",&dy,0);CHKERRQ(ierr);
            
            UnitsApplyInverseScaling(units->si_length,y1,&y1);
            UnitsApplyInverseScaling(units->si_length,dy,&dy);
            */
            
           
            bcdatag->vx = -opts_srH;
            bcdatag->time = ctx->time;
            bcdatag->dim = 0;
            
            PetscPrintf(PETSC_COMM_WORLD,"pTatin2d_BC => %f\n",Lx);
            ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
           // ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,0,BCListEvaluator_Geomod3bis,(void*)bcdatag);CHKERRQ(ierr);
            
            bcval= -opts_srH;
            
            //ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
            ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
            
            bcval= 0;//opts_srH;
            ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
           
           
            ierr = PetscFree(bcdatag);CHKERRQ(ierr);
        }
            break;
            
        case 25 : {
                PetscInt nphase_max = 30;
                PetscReal X0[30];
                PetscReal W0[30];
                PetscReal t_ini[30];
                PetscReal t_end[30];
                PetscReal vx_left[30];
                PetscReal vx_right[30];
                PetscBool apply_alabeaumont = PETSC_FALSE;
                PetscBool relax             = PETSC_TRUE;
                PetscReal alabeaumonty_litho;
                PetscScalar time_i,time_e,dummy;
                PetscInt iphase=0,i;
                
                PetscOptionsGetRealArray(PETSC_NULL,"-kine_X0",X0,&nphase_max,PETSC_NULL);
                PetscOptionsGetRealArray(PETSC_NULL,"-kine_half_width",W0,&nphase_max,PETSC_NULL);
                PetscOptionsGetRealArray(PETSC_NULL,"-kine_time_start",t_ini,&nphase_max,PETSC_NULL);
                PetscOptionsGetRealArray(PETSC_NULL,"-kine_time_end",t_end,&nphase_max,PETSC_NULL);
                PetscOptionsGetRealArray(PETSC_NULL,"-kine_vx_left",vx_left,&nphase_max,PETSC_NULL);
                PetscOptionsGetRealArray(PETSC_NULL,"-kine_vx_right",vx_right,&nphase_max,PETSC_NULL);
                PetscPrintf(PETSC_COMM_WORLD,"pTatin2d_BC => kinematic with %d \n",nphase_max);
            
            
            
            for (i=0;i<nphase_max; i++) {
                UnitsApplyInverseScaling(units->si_time,t_ini[i],&time_i);
                UnitsApplyInverseScaling(units->si_time,t_end[i],&time_e);
                if (ctx->time > time_i & ctx->time <=time_e) {
                     iphase = i;
                     relax   = PETSC_FALSE;
                    
                }
            }
            
            
            UnitsApplyInverseScaling(units->si_time,t_ini[iphase],&time_i);
            UnitsApplyInverseScaling(units->si_time,t_end[iphase],&time_e);
            if (relax) {
                    // entering a phase of quiescence no extension and free surface
                    
                    bcval=   0.0;
                    ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
                    ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
                    ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
                    //ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,0,BCListEvaluator_none,(void*)&bcval);CHKERRQ(ierr);
                    PetscPrintf(PETSC_COMM_WORLD,"pTatin2d_BC => quiescence \n");
                    
                } else {
                    BC_Geomod3 bcdata_l, bcdata_r;
                    PetscScalar bcval_l,bcval_r;
                    ierr = PetscMalloc(sizeof(struct _p_BC_Geomod3),&bcdata_l);CHKERRQ(ierr);
                    ierr = PetscMalloc(sizeof(struct _p_BC_Geomod3),&bcdata_r);CHKERRQ(ierr);
                    PetscPrintf(PETSC_COMM_WORLD,"pTatin2d_BC => applying kinematic BC's phase %d until %e \n", iphase,time_e);
                    UnitsApplyInverseScaling(units->si_velocity,vx_left[iphase],&bcval_l);
                    UnitsApplyInverseScaling(units->si_velocity,vx_right[iphase],&bcval_r);
                    
                    PetscOptionsGetReal(PETSC_NULL,"-kine_alabeaumont_ylitho",&alabeaumonty_litho, &apply_alabeaumont);
                    if (apply_alabeaumont) {
                        dy = 30e3;
                        
                        
                        UnitsApplyInverseScaling(units->si_length,alabeaumonty_litho,&y1);
                        UnitsApplyInverseScaling(units->si_length,dy,&dy);
                        
                        bcval = 0.0;
                        ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
                        
                        
                        bcdata->dim = 1;
                        bcdata->x0 = y1;
                        bcdata->x1 = y1-dy;
                        coeff = (2*(jmax[1]-y1)+dy)/(2*(y1-dy-jmin[1]));
                        
                        
                        bcdata->v0 = bcval_l; bcdata->v1 = -bcval_l*coeff;
                        ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_Alabeaumont,(void*)bcdata);CHKERRQ(ierr);
                        ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
                        
                        bcdata->v0 = bcval_r; bcdata->v1 = -bcval_r*coeff;
                        ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_Alabeaumont,(void*)bcdata);CHKERRQ(ierr);
                        ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
                        
                        ierr = PetscFree(bcdata);CHKERRQ(ierr);
                        
                        
                    }else{
                        
                        ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval_l);CHKERRQ(ierr);
                        
                        ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval_r);CHKERRQ(ierr);
                        bcval = (-bcval_l+bcval_r)*opts_Ly/opts_Lx;
                        ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
                        
                    }
                    

                    bcdata_l->vx = bcval_l;
                    bcdata_l->time = ctx->time - time_i;
                    bcdata_l->time = ctx->time - time_i;
                    bcdata_l->dim = 0;
                    UnitsApplyInverseScaling(units->si_length,X0[iphase]-W0[iphase],&bcdata_l->x0);
                    
                    
                    
                  
                    ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,0,BCListEvaluator_Geomod3R,(void*)bcdata_l);CHKERRQ(ierr);
                    

                    bcdata_r->vx = bcval_r;
                    bcdata_r->time = ctx->time-time_i;
                    bcdata_r->dim = 0;
                    UnitsApplyInverseScaling(units->si_length,X0[iphase]+W0[iphase],&bcdata_r->x0);
                    ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,0,BCListEvaluator_Geomod3,(void*)bcdata_r);CHKERRQ(ierr);
                  

                    
                    ierr = PetscFree(bcdata_l);  ierr = PetscFree(bcdata_r);
                    
                }
            ierr = BCListUpdateCache(ubclist);
            }
            break;
    }

   
    
    
    
    /* check for energy solver */
    {
        PetscBool active = PETSC_FALSE;
        ierr = pTatinPhysCompActivated(ctx,PhysComp_Energy,&active);CHKERRQ(ierr);
        
        if (active) {
            DM da;
            Vec T;
            PhysCompEnergyCtx phys_energy;
            PetscScalar bcval;
            PetscBool   found;
            
            phys_energy = ctx->phys_energy;
            da          = phys_energy->daT;
            T           = phys_energy->T;
            
            
            
            bcval = 0.0;
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-Ttop",&bcval,&found);CHKERRQ(ierr);
            if (found){
                ierr = DMDABCListTraverse(phys_energy->T_bclist,da,DMDABCList_JMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
            }
            
            bcval = 0.0;
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-Tbot",&bcval,&found);CHKERRQ(ierr);
            if (found){
                ierr = DMDABCListTraverse(phys_energy->T_bclist,da,DMDABCList_JMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
            }
            
            bcval = 0.0;
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-Tleft",&bcval,&found);CHKERRQ(ierr);
            if (found){
                ierr = DMDABCListTraverse(phys_energy->T_bclist,da,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);        
            }
            
            bcval = 0.0;
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-Tright",&bcval,&found);CHKERRQ(ierr);
            if (found){
                ierr = DMDABCListTraverse(phys_energy->T_bclist,da,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
            }        
            
            
        }
    }


  /* check for Darcy solver */
    {
        PetscBool active = PETSC_FALSE;
        ierr = pTatinPhysCompActivated(ctx,PhysComp_Darcy,&active);CHKERRQ(ierr);
        
        if (active) {
            DM da;
            Vec Pf;
            PhysCompDarcyCtx phys_darcy;
            PetscScalar bcval;
            PetscBool   found;
            
            phys_darcy = ctx->phys_darcy;
            da         = phys_darcy->daPf;
            Pf         = phys_darcy->Pf;
            
            
            
            bcval = 0.0;

            ierr = DMDABCListTraverse(phys_darcy->Pf_bclist,da,DMDABCList_JMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
            
            //ierr = DMDABCListTraverse(phys_darcy->Pf_bclist,da,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);        
            
            //ierr = DMDABCListTraverse(phys_darcy->Pf_bclist,da,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
            
            UnitsApplyInverseScaling(units->si_force_per_volume,opts_Ly*2000*10,&bcval);
            ierr = DMDABCListTraverse(phys_darcy->Pf_bclist,da,DMDABCList_JMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
            
            
        }
    }

    
    
    PetscFunctionReturn(0);
}




#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelUpdateMeshGeometry_GENE"
PetscErrorCode pTatin2d_ModelUpdateMeshGeometry_GENE(pTatinCtx ctx,Vec X)
{
	static int     beenhere=0;
	PetscReal      step;
	Vec            velocity,pressure;
	DM             dav,dap;
	PetscScalar    Unormal[2],Vnormal_base;
	PetscInt       remesh_type;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	step = ctx->dt;
	
	ierr = DMCompositeGetEntries(ctx->pack,&dav,&dap);CHKERRQ(ierr);
	ierr = DMCompositeGetAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
	
	//	ierr = UpdateMeshGeometry_FullLagSurface(dav,velocity,step);CHKERRQ(ierr);
	
	remesh_type = 2;
	PetscOptionsGetInt(PETSC_NULL,"-remesh_type",&remesh_type,0);
	
	switch (remesh_type) {
	    case 0: if (beenhere) { PetscPrintf(PETSC_COMM_WORLD,"pTatin2d_ModelUpdateMeshGeometry_GENE => RemeshType \"full_lagrange\"\n"); }
	           ierr = UpdateMeshGeometry_FullLagSurface(dav,velocity,step);CHKERRQ(ierr);
	           break; 
		case 1:
			if (beenhere) { PetscPrintf(PETSC_COMM_WORLD,"pTatin2d_ModelUpdateMeshGeometry_GENE => RemeshType \"VerticalDisplacemen\"\n"); }
			ierr = UpdateMeshGeometry_VerticalDisplacement(dav,velocity,step);CHKERRQ(ierr);
			break;
			
		case 2:
			if (beenhere) { PetscPrintf(PETSC_COMM_WORLD,"pTatin2d_ModelUpdateMeshGeometry_GENE => RemeshType \"FullLagSurfaceJMAX_RemeshInteriorJMIN_JMAX\"\n"); }
			ierr = UpdateMeshGeometry_FullLagSurfaceJMAX_RemeshInteriorJMIN_JMAX(dav,velocity,step);CHKERRQ(ierr);
			break;
			
		case 3:
			if (beenhere) { PetscPrintf(PETSC_COMM_WORLD,"pTatin2d_ModelUpdateMeshGeometry_GENE => RemeshType \"NormalVPrescribedWithLagrangianFreeSurface\"\n"); }
			Unormal[0]   = 0.0;
			Unormal[1]   = 0.0;
			Vnormal_base = 0.0;
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-remeshtype3_vx_left",&Unormal[0],0);CHKERRQ(ierr);
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-remeshtype3_vx_right",&Unormal[1],0);CHKERRQ(ierr);
			ierr = PetscOptionsGetScalar(PETSC_NULL,"-remeshtype3_vy_bot",&Vnormal_base,0);CHKERRQ(ierr);
            ierr = UpdateMeshGeometry_NormalVPrescribedWithLagrangianFreeSurface(dav,velocity,Unormal,Vnormal_base,step);CHKERRQ(ierr);
			break;
            
        case 4: {
            PetscScalar delta_i,delta_j,jmin[3],jmax[3],Ly;
            PetscInt node_index_j,node_index_i; 
            PetscInt M,N,P;
           
            if (beenhere) { PetscPrintf(PETSC_COMM_WORLD,"pTatin2d_ModelUpdateMeshGeometry_GENE => RemeshType \"UpdateMeshGeometry_DecoupledHorizontalVerticalMeshMovement\"\n"); }
            ierr = DMDAGetBoundingBox(dav,jmin,jmax);CHKERRQ(ierr);
            ierr = DMDAGetInfo(dav,0,&M,&N,&P,0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);
            node_index_i = M-4; 
            delta_i        = jmax[0]-0.2;            
            node_index_j = 4; /* top of second element */
            delta_j        = 0.00; 
            
            
            ierr = UpdateMeshGeometry_DecoupledHorizontalVerticalMeshMovement(dav,velocity,step);CHKERRQ(ierr);
            
            ierr = DMDARemeshSetUniformCoordinatesIJ(dav,0,node_index_i,delta_i);CHKERRQ(ierr);
           // ierr = RMTSetUniformCoordinatesBetweenILayers2d(dav,0,node_index_i);CHKERRQ(ierr);
           // ierr = RMTSetUniformCoordinatesBetweenILayers2d(dav,node_index_i-1,M);CHKERRQ(ierr);
            ierr = DMDARemeshSetUniformCoordinatesIJ(dav,1,node_index_j,delta_j);CHKERRQ(ierr);
            ierr = DMDARemeshSetUniformCoordinatesBetweenJLayers2d(dav,0,node_index_j+1);CHKERRQ(ierr);
            ierr = DMDARemeshSetUniformCoordinatesBetweenJLayers2d(dav,node_index_j,N);CHKERRQ(ierr);
            /*
            ierr = RMTSetUniformCoordinatesRange(dav,0,jmin[0],node_index_i+1,delta_i,
                                                          0,jmin[1],node_index_j+1,delta_j,PETSC_TRUE);
            
            ierr = RMTSetUniformCoordinatesRange(dav,
                                                         0,jmin[0],node_index_i,delta_i,
                                                         node_index_j,delta_j,N,jmax[1],PETSC_TRUE);
            ierr = RMTSetUniformCoordinatesRange(dav,
                                                         node_index_i,delta_i,M,jmax[0],
                                                         0,jmin[1],node_index_j+1,delta_j,PETSC_TRUE);
            ierr = RMTSetUniformCoordinatesRange(dav,
                                                         node_index_i,delta_i,M,jmax[0],
                                                         node_index_j,delta_j,N,jmax[1],PETSC_TRUE);
            */

            /*
            
            
            // clean up spacing inside 
            
            
            ierr = DMDARemeshSetUniformCoordinatesBetweenJLayers2d(dav,0,node_index_j+1);CHKERRQ(ierr);
            ierr = DMDARemeshSetUniformCoordinatesBetweenJLayers2d(dav,node_index_j,N);CHKERRQ(ierr);
            
           
            ierr = DMDARemeshSetUniformCoordinatesIJ(dav,0,node_index_i,delta);CHKERRQ(ierr);
            ierr = RMTSetUniformCoordinatesBetweenILayers2d(dav,0,node_index_i+1);CHKERRQ(ierr);
            ierr = RMTSetUniformCoordinatesBetweenILayers2d(dav,node_index_i,M);CHKERRQ(ierr);
            */
            
			break; 
		}
			
		case 5:
		{
			PetscReal gmin[3],gmax[3];
			DM cdm;
			Vec coordinates,new_mesh_coords;
			PetscScalar delta_x,delta_y;
			PetscInt M,N,P;
			PetscInt span_node_x,span_node_y;
			
			/* update mesh with the full velocity field */
			ierr = UpdateMeshGeometry_FullLagSurface(dav,velocity,step);CHKERRQ(ierr);

			/* define new mesh onto which we wish to interpolate onto */
			ierr = DMDAGetBoundingBox(dav,gmin,gmax);CHKERRQ(ierr);
			
			ierr = DMDAGetCoordinateDA(dav,&cdm);CHKERRQ(ierr);
			ierr = DMDAGetCoordinates(dav,&coordinates);CHKERRQ(ierr);
			ierr = DMCreateGlobalVector(cdm,&new_mesh_coords);CHKERRQ(ierr);
			
			ierr = DMDAGetInfo(dav,0,&M,&N,&P,0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);
			
			/* set uniform mesh */
			ierr = RMTSetUniformCoordinatesRange_X(cdm,new_mesh_coords,0,gmin[0],M,gmax[0]);CHKERRQ(ierr);
			ierr = RMTSetUniformCoordinatesRange_Y(cdm,new_mesh_coords,0,gmin[1],N,gmax[1]);CHKERRQ(ierr);

			/* graduate mesh in y */
			delta_y = 0.2;
			span_node_y = 4;
			ierr = RMTSetUniformCoordinatesRange_Y(cdm,new_mesh_coords,0,          gmin[1],        span_node_y+1,gmin[1]+delta_y);CHKERRQ(ierr);
			ierr = RMTSetUniformCoordinatesRange_Y(cdm,new_mesh_coords,span_node_y,gmin[1]+delta_y,N,            gmax[1]);CHKERRQ(ierr);
			
			/* graduate mesh in x */
			delta_x = 0.2;
			span_node_x = M - 5;
			ierr = RMTSetUniformCoordinatesRange_X(cdm,new_mesh_coords,0,          gmin[0],        span_node_x+1,gmax[0]-delta_x);CHKERRQ(ierr);
			ierr = RMTSetUniformCoordinatesRange_X(cdm,new_mesh_coords,span_node_x,gmax[0]-delta_x,M,            gmax[0]);CHKERRQ(ierr);

//			ierr = RMTSetCoordinates(dav,new_mesh_coords);CHKERRQ(ierr);
//			ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,"test_rms");CHKERRQ(ierr);

			/* interpolate free surface from dav->coords onto new_mesh_coords */
			ierr = UpdateMeshGeometry_Interp(dav,new_mesh_coords);CHKERRQ(ierr);

			/* remesh nodes using upper and lower surfaces */
			ierr = DMDARemeshSetUniformCoordinatesBetweenJLayers2d(dav,0,span_node_y+1);CHKERRQ(ierr);
			ierr = DMDARemeshSetUniformCoordinatesBetweenJLayers2d(dav,span_node_y,N);CHKERRQ(ierr);
			//ierr = DMDARemeshSetUniformCoordinatesBetweenJLayers2d(dav,0,NY);CHKERRQ(ierr);
			
//			ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,"test_rms");CHKERRQ(ierr);
//			exit(0);
			
			ierr = VecDestroy(&new_mesh_coords);CHKERRQ(ierr);
			
		}
			break;
			
		
            case 6:
			if (beenhere) { PetscPrintf(PETSC_COMM_WORLD,"pTatin2d_ModelUpdateMeshGeometry_GENE => RemeshType \"FullLagSurfaceJMIN_JMAX_RemeshInteriorJMIN_JMAX\"\n"); }
            ierr = UpdateMeshGeometry_FullLagSurfaceJMIN_JMAX_RemeshInteriorJMIN_JMAX(dav,velocity,step);CHKERRQ(ierr);
			break;

			case 7:
			if (beenhere) { PetscPrintf(PETSC_COMM_WORLD,"pTatin2d_ModelUpdateMeshGeometry_GENE => RemeshType \"LatInflowLagSurfaceJMIN_JMAX_RemeshInteriorJMIN_JMAX\"\n"); }
			ierr = UpdateMeshGeometry_LatInflowLagSurfaceJMIN_JMAX_RemeshInteriorJMIN_JMAX(dav,velocity,step);CHKERRQ(ierr);
			break;
            
            case 8:
            if (beenhere) { PetscPrintf(PETSC_COMM_WORLD,"pTatin2d_ModelUpdateMeshGeometry_GENE => RemeshType \"LagSurfaceJMIN_JMAX_RemeshInteriorRefineJMIN_JMAX\"\n"); }
            ierr = UpdateMeshGeometry_LagSurfaceJMIN_JMAX_RemeshInteriorRefineJMIN_JMAX(dav,velocity,step);CHKERRQ(ierr);
            break;
            
            case 9:
            if (beenhere) { PetscPrintf(PETSC_COMM_WORLD,"pTatin2d_ModelUpdateMeshGeometry_GENE => RemeshType \"LatInflowLagSurfaceJMIN_JMAX_RemeshInteriorRefineJMIN_JMAX\"\n"); }
            ierr = UpdateMeshGeometry_LatInflowLagSurfaceJMIN_JMAX_RemeshInteriorRefineJMIN_JMAX(dav,velocity,step);CHKERRQ(ierr);
            break;
            
            
        case 10:
            if (beenhere) { PetscPrintf(PETSC_COMM_WORLD,"pTatin2d_ModelUpdateMeshGeometry_GENE => RemeshType \"FullLagSurfaceJMAX_RemeshInteriorRefineJMIN_JMAX\"\n"); }
            ierr = UpdateMeshGeometry_FullLagSurfaceJMAX_RemeshInteriorRefineJMIN_JMAX(dav,velocity,step);CHKERRQ(ierr);
            break;
			
        default:
			ierr = UpdateMeshGeometry_FullLagSurfaceJMAX_RemeshInteriorJMIN_JMAX(dav,velocity,step);CHKERRQ(ierr);
			break;
	}
	
	ierr = DMCompositeRestoreAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
	
	/* snappy bilinearize mesh */
	ierr = UpdateMeshGeometry_BilinearizeQ2Elements(dav);CHKERRQ(ierr);
	
	
	/* check for energy solver - copy velocity coordinates */
	{
		PetscBool active = PETSC_FALSE;
		ierr = pTatinPhysCompActivated(ctx,PhysComp_Energy,&active);CHKERRQ(ierr);
		
		if (active) {
			DM da;
			Vec T;
			PhysCompEnergyCtx phys_energy;
			
			phys_energy = ctx->phys_energy;
			da          = phys_energy->daT;
			T           = phys_energy->T;
			
			ierr = DMDAProjectCoordinatesQ2toQ1_2d(ctx->dav,da);CHKERRQ(ierr);
		}
	}
	
	beenhere = 1;
	PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelUpdateMeshGeometry_GENE_local_global"
PetscErrorCode pTatin2d_ModelUpdateMeshGeometry_GENE_local_global(pTatinCtx ctx,Vec X)
{
	Vec velocity,pressure,coordinates,vel_advect_mesh;
	PetscInt M,N;
	PetscReal step;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	step = ctx->dt;
	
	ierr = DMCompositeGetAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
	
	ierr = DMGetGlobalVector(ctx->dav,&vel_advect_mesh);CHKERRQ(ierr);
	
	ierr = VecCopy(velocity,vel_advect_mesh);CHKERRQ(ierr);
	ierr = VecStrideScale(vel_advect_mesh,0,0.0);CHKERRQ(ierr); /* zero out the x-componenet */
	
	ierr = DMDAGetCoordinates(ctx->dav,&coordinates);CHKERRQ(ierr);
	ierr = VecAXPY(coordinates,step,vel_advect_mesh);CHKERRQ(ierr); /* x = x + dt.vel_advect_mesh */
	
	ierr = DMRestoreGlobalVector(ctx->dav,&vel_advect_mesh);CHKERRQ(ierr);
	
	ierr = DMCompositeRestoreAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
	//
	//
	ierr = DMGetLocalVector(ctx->dav,&vel_advect_mesh);CHKERRQ(ierr);
	ierr = DMGlobalToLocalBegin(ctx->dav,velocity,INSERT_VALUES,vel_advect_mesh);CHKERRQ(ierr);
	ierr = DMGlobalToLocalEnd(  ctx->dav,velocity,INSERT_VALUES,vel_advect_mesh);CHKERRQ(ierr);
	
	ierr = VecStrideScale(vel_advect_mesh,0,0.0);CHKERRQ(ierr); /* zero out the x-componenet */
	
	ierr = DMDAGetGhostedCoordinates(ctx->dav,&coordinates);CHKERRQ(ierr);
	ierr = VecAXPY(coordinates,step,vel_advect_mesh);CHKERRQ(ierr); /* x = x + dt.vel_advect_mesh */
	
	ierr = DMRestoreLocalVector(ctx->dav,&vel_advect_mesh);CHKERRQ(ierr);
	
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_GENE"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_GENE(pTatinCtx ctx)
{
	PetscErrorCode         ierr;
	
	pTatinUnits *units;	
	PetscBool flg; 
	char map_file[PETSC_MAX_PATH_LEN];
	PetscFunctionBegin;
	units   = &ctx->units;  
	UnitsApplyInverseScaling(units->si_time,ctx->dt_min,&ctx->dt_min);
	UnitsApplyInverseScaling(units->si_time,ctx->dt,&ctx->dt);
	UnitsApplyInverseScaling(units->si_time,ctx->dt_adv,&ctx->dt_adv);
	UnitsApplyInverseScaling(units->si_time,ctx->dt_max,&ctx->dt_max);
	UnitsApplyInverseScaling(units->si_time,ctx->time_max,&ctx->time_max);
	UnitsApplyInverseScaling(units->si_length,ctx->max_disp_y,&ctx->max_disp_y);
	
	
	ierr = pTatin2d_ModelSetMaterialPropertyFromOptions_GENE(ctx); CHKERRQ(ierr);
	flg = PETSC_FALSE;
	ierr = PetscOptionsGetString(PETSC_NULL,"-map_file",map_file,PETSC_MAX_PATH_LEN-1,&flg);CHKERRQ(ierr);
	if (flg == PETSC_TRUE){
	ierr = pTatin2d_ModelSetMarkerIndexFromMap_GENE(ctx); CHKERRQ(ierr);
	}else{
	ierr = pTatin2d_ModelSetMarkerIndexConstant_GENE(ctx); CHKERRQ(ierr);
	}
	ierr = pTatin2d_ModelSetInitialStokesVariableOnMarker_GENE(ctx); CHKERRQ(ierr);
	ierr = pTatin2d_ModelSetInitialPlsVariableOnMarker_GENE(ctx); CHKERRQ(ierr);
	
	{
		PetscBool active = PETSC_FALSE;
		ierr = pTatinPhysCompActivated(ctx,PhysComp_Energy,&active);CHKERRQ(ierr);
		
		if (active) {
		   flg = PETSC_FALSE;
		   ierr = PetscOptionsGetString(PETSC_NULL,"-temp_file",map_file,PETSC_MAX_PATH_LEN-1,&flg);CHKERRQ(ierr);
		   if (flg == PETSC_TRUE){
	          ierr= pTatin2d_ModelSetInitialTemperatureOnNodes_GENE(ctx); CHKERRQ(ierr);
	       }else{
	           ierr= pTatin2d_ModelSetInitialConstantTemperatureOnNodes_GENE(ctx); CHKERRQ(ierr);
	       }
		
			ierr = pTatin2d_ModelSetInitialEnergyVariableOnMarker_GENE(ctx); CHKERRQ(ierr);
			
			
		}
	}
	#if 0
	{
		PetscBool active = PETSC_FALSE;
		ierr = pTatinPhysCompActivated(ctx,PhysComp_Darcy,&active);CHKERRQ(ierr);
		
		if (active) {
			ierr= pTatin2d_ModelSetInitialFluidPressureOnNodes_GENE(ctx); CHKERRQ(ierr);
			
			ierr = pTatin2d_ModelSetInitialDarcyVariableOnMarker_GENE(ctx); CHKERRQ(ierr);
			
			
		}
	}
	#endif
	
#if 1	
	{
		PetscBool active = PETSC_FALSE;
		ierr = pTatinPhysCompActivated(ctx,PhysComp_SPM,&active);CHKERRQ(ierr);
		
		if (active) {
		
			ierr= pTatin2d_ModelSetSPMParameters_GENE(ctx); CHKERRQ(ierr);
		}
	}
#endif	


	PetscFunctionReturn(0);
}





#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelOutput_GENE"
PetscErrorCode pTatin2d_ModelOutput_GENE(pTatinCtx ctx,Vec X,const char prefix[])
{
	static int     beenhere = 0;
	char           pvdfilename[PETSC_MAX_PATH_LEN];
	PetscBool      active,use_dimensional_units;
	char           vtkfilename[PETSC_MAX_PATH_LEN];
	char           filename[PETSC_MAX_PATH_LEN];
	char           name[PETSC_MAX_PATH_LEN];
	PetscErrorCode ierr;
	
	use_dimensional_units = PETSC_FALSE;
	ierr = PetscOptionsGetBool(PETSC_NULL,"-use_dimensional_units",&use_dimensional_units,PETSC_NULL);CHKERRQ(ierr);
	
	if (use_dimensional_units) {
		ierr = pTatin_MaterialPointScaleProperties(ctx->db,&ctx->units);CHKERRQ(ierr);
		UnitsApplyScaling(ctx->units.si_time,ctx->time,&ctx->time);
		
	}
	
	if (prefix == PETSC_NULL) {
#ifdef WRITE_QPOINTS
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,"qpoints");CHKERRQ(ierr);
#endif
#ifdef WRITE_FACEQPOINTS
		ierr = pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(ctx,ctx->outputpath,"faceqpoints");CHKERRQ(ierr);
#endif
#ifdef WRITE_VP
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,"vp");CHKERRQ(ierr);
#endif
#ifdef WRITE_INT_FIELDS
		ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,"int_fields");CHKERRQ(ierr);
#endif
#ifdef WRITE_MPOINTS
		ierr = SwarmOutputParaView_MPntPStokes(ctx->db,ctx->outputpath,"mpoints_stk");CHKERRQ(ierr);
#endif
	} else { /* example of adding a different prefix to the file names - not sure this is useful */
		
#ifdef WRITE_QPOINTS
		sprintf(pvdfilename,"%s/timeseries_qpoints.pvd",ctx->outputpath);
		if (beenhere==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		sprintf(vtkfilename, "%s_qpoints.pvtu",prefix);
		ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
		
		sprintf(name,"%s_qpoints",prefix);
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
#endif
		
#ifdef WRITE_FACEQPOINTS
		/*		
		 if (beenhere==0) {
		 ierr = ParaviewPVDOpen("faceqpoints_timeseries.pvd");CHKERRQ(ierr);
		 }
		 sprintf(vtkfilename, "%s_faceqpoints.pvtu",prefix);
		 ierr = ParaviewPVDAppend("faceqpoints_timeseries.pvd",ctx->time, vtkfilename, ctx->outputpath);CHKERRQ(ierr);
		 */ 
		sprintf(name,"%s_faceqpoints",prefix);
		ierr = pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
#endif
        
#ifdef WRITE_VP
		sprintf(pvdfilename,"%s/timeseries_vp.pvd",ctx->outputpath);
		if (beenhere==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		sprintf(vtkfilename, "%s_vp.pvts",prefix);
		ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
		
		sprintf(name,"%s_vp",prefix);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
#endif
        
#ifdef WRITE_INT_FIELDS
		sprintf(pvdfilename,"%s/timeseries_int_fields.pvd",ctx->outputpath);
		if (beenhere==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		sprintf(vtkfilename, "%s_int_fields.pvts",prefix);
		ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
		
		sprintf(name,"%s_int_fields",prefix);
		ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
#endif
		
#ifdef WRITE_MPOINTS
		sprintf(pvdfilename,"%s/timeseries_mpoints_stk.pvd",ctx->outputpath);
		if (beenhere==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		sprintf(vtkfilename, "%s_mpoints_stk.pvtu",prefix);
		ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
		
		sprintf(name,"%s_mpoints_stk",prefix);
		ierr = GENE_SwarmOutputParaView_MPntP(ctx->db,ctx->outputpath,name);CHKERRQ(ierr);
#endif
		
	}
	
	
	ierr = pTatinPhysCompActivated(ctx,PhysComp_Energy,&active);CHKERRQ(ierr);
	if (active) {
		DM da;
		Vec T;
		PhysCompEnergyCtx phys_energy;
		
		phys_energy = ctx->phys_energy;
		da          = phys_energy->daT;
		T           = phys_energy->T;
		
		/* temperature */
#ifdef WRITE_TFIELD
		if (prefix == PETSC_NULL) {
			ierr = pTatinOutputParaViewMeshTemperature(ctx,T,ctx->outputpath,"Tfield");CHKERRQ(ierr);
		} else {
			// PVD
			sprintf(pvdfilename,"%s/timeseries_Tfield.pvd",ctx->outputpath);
			if (beenhere==0) {
				ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
			}
			sprintf(vtkfilename, "%s_Tfield.pvts",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			
			// PVTS
			sprintf(name,"%s_Tfield",prefix);
			ierr = pTatinOutputParaViewMeshTemperature(ctx,T,ctx->outputpath,name);CHKERRQ(ierr);
		}
#endif
        
		/* average thermal properties */
#ifdef WRITE_TCELL		
		/* quad props */
		if (prefix == PETSC_NULL) {
			sprintf(name,"Tcell");
		} else {
			sprintf(name,"%s_Tcell",prefix);
			
			// PVD
			sprintf(pvdfilename,"%s/timeseries_Tcell.pvd",ctx->outputpath);
			if (beenhere==0) {
				ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
			}
			sprintf(vtkfilename, "%s_Tcell.pvts",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
		}
		// PVTS
		ierr = pTatinParaViewOutputQuadratureThermalAveragedFields(ctx,ctx->outputpath,name);
#endif
        
	}
	
	ierr = pTatinPhysCompActivated(ctx,PhysComp_Darcy,&active);CHKERRQ(ierr);
	if (active) {
		DM da;
		Vec Pf;
		PhysCompDarcyCtx phys_darcy;
		
		phys_darcy = ctx->phys_darcy;
		da          = phys_darcy->daPf;
		Pf          = phys_darcy->Pf;

		if (prefix == PETSC_NULL) {
			ierr = pTatinOutputParaViewMeshFluidPressure(ctx,Pf,ctx->outputpath,"Pffield");CHKERRQ(ierr);
		} else {
			// PVD
			sprintf(pvdfilename,"%s/timeseries_Pffield.pvd",ctx->outputpath);
			if (beenhere==0) {
				ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
			}
			sprintf(vtkfilename, "%s_Pffield.pvts",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			
			// PVTS
			sprintf(name,"%s_Pffield",prefix);
			ierr = pTatinOutputParaViewMeshFluidPressure(ctx,Pf,ctx->outputpath,name);CHKERRQ(ierr);
		}
		
		/* quad props */
		if (prefix == PETSC_NULL) {
			sprintf(name,"Pfcell");
		} else {
			sprintf(name,"%s_Pfcell",prefix);
			
			// PVD
			sprintf(pvdfilename,"%s/timeseries_Pfcell.pvd",ctx->outputpath);
			if (beenhere==0) {
				ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
			}
			sprintf(vtkfilename, "%s_Pfcell.pvts",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
		}
		// PVTS
		ierr = pTatinParaViewOutputQuadratureDarcyAveragedFields(ctx,ctx->outputpath,name);
		
		


}
	

	
	active = PETSC_FALSE; 
	ierr = PetscOptionsGetBool(PETSC_NULL,"-use_passive_tracers",&active,PETSC_NULL);CHKERRQ(ierr);
    if (active) {
    	if (use_dimensional_units) {
			ierr = pTatin_MaterialPointScaleProperties(ctx->db_passive,&ctx->units);CHKERRQ(ierr);
		}
    	pTatin_OutputMarker_Passive(ctx);
     	if (use_dimensional_units) {
			ierr = pTatin_MaterialPointUnScaleProperties(ctx->db_passive,&ctx->units);CHKERRQ(ierr);
		}
    }
	
	if (use_dimensional_units) {
		ierr = pTatin_MaterialPointUnScaleProperties(ctx->db,&ctx->units);CHKERRQ(ierr);
		UnitsApplyInverseScaling(ctx->units.si_time,ctx->time,&ctx->time);
	}
	
	beenhere = 1;
	PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "GENE_SwarmOutputParaView_MPntP"
PetscErrorCode GENE_SwarmOutputParaView_MPntP(DataBucket db,const char path[],const char prefix[])
{ 
	char           *vtkfilename,*filename;
	PetscMPIInt    rank;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ierr = pTatinGenerateParallelVTKName(prefix,"vtu",&vtkfilename);CHKERRQ(ierr);
	if (path) {
		asprintf(&filename,"%s/%s",path,vtkfilename);
	} else {
		asprintf(&filename,"./%s",vtkfilename);
	}
	
	
	ierr = GENE_SwarmView_MPntP_VTKappended_binary(db,filename);CHKERRQ(ierr);
	
	free(filename);
	free(vtkfilename);
	
	
	ierr = pTatinGenerateVTKName(prefix,"pvtu",&vtkfilename);CHKERRQ(ierr);
	if (path) {
		asprintf(&filename,"%s/%s",path,vtkfilename);
	} else {
		asprintf(&filename,"./%s",vtkfilename);
	}
	
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	if (rank==0) {
		ierr = __GENE_SwarmView_MPntP_PVTU( prefix, filename,db );CHKERRQ(ierr);
	}
	free(filename);
	free(vtkfilename);
	
	PetscFunctionReturn(0);
}



#undef __FUNCT__
#define __FUNCT__ "GENE_SwarmView_MPntP_VTKappended_binary"
PetscErrorCode GENE_SwarmView_MPntP_VTKappended_binary(DataBucket db,const char name[])
{
	PetscMPIInt rank;
	FILE *vtk_fp;
	PetscInt k;
	int npoints;
	PetscLogDouble t0,t1;
	DataField PField_std;
	DataField PField_stokes;
	DataField PField_stokespl;
	DataField PField_stokesmelt;
	DataField PField_thermal;
	DataField PField_chrono;
	DataField PField_darcy;
	int byte_offset,length;
	BTruth foundplastic,foundmelt,foundthermal,foundchrono,founddarcy;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	
	DataBucketQueryDataFieldByName(db,MPntPStokesPl_classname,&foundplastic);
	DataBucketQueryDataFieldByName(db,MPntPStokesMelt_classname,&foundmelt);
	DataBucketQueryDataFieldByName(db,MPntPThermal_classname,&foundthermal);	
	DataBucketQueryDataFieldByName(db,MPntPChrono_classname,&foundchrono);	
	DataBucketQueryDataFieldByName(db,MPntPDarcy_classname,&founddarcy);	
	
	ierr = PetscGetTime(&t0);CHKERRQ(ierr);
	
	if ((vtk_fp = fopen ( name, "w")) == NULL)  {
		SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
	}
	
	fprintf( vtk_fp, "<?xml version=\"1.0\"?>\n");
	
#ifdef WORDSIZE_BIGENDIAN
	fprintf( vtk_fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
#else
	fprintf( vtk_fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
#endif
	fprintf( vtk_fp, "\t<UnstructuredGrid>\n" );
	
	DataBucketGetSizes(db,&npoints,PETSC_NULL,PETSC_NULL);
	fprintf( vtk_fp, "\t\t<Piece NumberOfPoints=\"%d\" NumberOfCells=\"%d\">\n",npoints,npoints );
	
	
	fprintf( vtk_fp, "\n");
	fprintf( vtk_fp, "\t\t\t<Cells>\n");
	
	byte_offset = 0;
	
	// connectivity //
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Int32\" Name=\"connectivity\" format=\"appended\" offset=\"%d\" />\n",byte_offset);
	byte_offset = byte_offset + sizeof(int) + npoints * sizeof(int);
	
	// offsets //
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Int32\" Name=\"offsets\" format=\"appended\" offset=\"%d\" />\n",byte_offset);
	byte_offset = byte_offset + sizeof(int) + npoints * sizeof(int);
	
	// types //
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"UInt8\" Name=\"types\" format=\"appended\" offset=\"%d\" />\n",byte_offset);
	byte_offset = byte_offset + sizeof(int) + npoints * sizeof(unsigned char);
	
	fprintf( vtk_fp, "\t\t\t</Cells>\n");
	
	fprintf( vtk_fp, "\n");
	fprintf( vtk_fp, "\t\t\t<CellData>\n");
	fprintf( vtk_fp, "\t\t\t</CellData>\n");
	fprintf( vtk_fp, "\n");
	
	
	
	DataBucketGetDataFieldByName(db, MPntStd_classname      ,&PField_std);
	DataBucketGetDataFieldByName(db, MPntPStokes_classname ,&PField_stokes);
	
	if(foundplastic==BTRUE) {
	DataBucketGetDataFieldByName(db, MPntPStokesPl_classname ,&PField_stokespl);
	}
	if(foundmelt==BTRUE) {
	DataBucketGetDataFieldByName(db, MPntPStokesMelt_classname ,&PField_stokesmelt);
	}
	if(foundthermal==BTRUE) {
	DataBucketGetDataFieldByName(db, MPntPThermal_classname ,&PField_thermal);
	}
	
	if(foundchrono==BTRUE) {
	DataBucketGetDataFieldByName(db, MPntPChrono_classname ,&PField_chrono);
	}
	
    if(founddarcy==BTRUE) {
	DataBucketGetDataFieldByName(db, MPntPDarcy_classname ,&PField_darcy);
	}	
	
	/* point coordinates */
	fprintf( vtk_fp, "\t\t\t<Points>\n");
	
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\" format=\"appended\" offset=\"%d\" />\n",byte_offset);
	byte_offset = byte_offset + sizeof(int) + npoints * 3 * sizeof(double);
	
	fprintf( vtk_fp, "\t\t\t</Points>\n");
	fprintf( vtk_fp, "\n");
	
	/* point data BEGIN */
	fprintf( vtk_fp, "\t\t\t<PointData>\n");
	
	/* auto generated shit goes here */
	{
		MPntStd          *marker_std     = PField_std->data; /* should write a function to do this */
		MPntPStokes      *marker_stokes  = PField_stokes->data; /* should write a function to do this */

		MPntStdVTKWriteBinaryAppendedHeaderAllFields(vtk_fp,&byte_offset,(const int)npoints,(const MPntStd*)marker_std );
		MPntPStokesVTKWriteBinaryAppendedHeaderAllFields(vtk_fp,&byte_offset,(const int)npoints,(const MPntPStokes*)marker_stokes);
	}
	
	
	
	if(foundthermal==BTRUE) {
			MPntPThermal     *marker_thermal = PField_thermal->data; /* should write a function to do this */
			MPntPThermalVTKWriteBinaryAppendedHeaderAllFields(vtk_fp,&byte_offset,(const int)npoints,(const MPntPThermal*)marker_thermal);
		}
	
	
	if(foundplastic==BTRUE) 
	   {
		MPntPStokesPl    *marker_stokespl = PField_stokespl->data; /* should write a function to do this */
		MPntPStokesPlVTKWriteBinaryAppendedHeaderAllFields(vtk_fp,&byte_offset,(const int)npoints,(const MPntPStokesPl*)marker_stokespl);
		}	
	
	if(foundmelt==BTRUE) {
		MPntPStokesMelt *marker_stokesmelt = PField_stokesmelt->data; /* should write a function to do this */
	    MPntPStokesMeltVTKWriteBinaryAppendedHeaderAllFields(vtk_fp,&byte_offset,(const int)npoints,(const MPntPStokesMelt*)marker_stokesmelt);
		}
    
    if(foundchrono==BTRUE) {
		MPntPChrono *marker_chrono = PField_chrono->data; /* should write a function to do this */
	    MPntPChronoVTKWriteBinaryAppendedHeaderAllFields(vtk_fp,&byte_offset,(const int)npoints,(const MPntPChrono*)marker_chrono);
		}
    if(founddarcy==BTRUE) {
		MPntPDarcy *marker_darcy = PField_darcy->data; /* should write a function to do this */
	    MPntPDarcyVTKWriteBinaryAppendedHeaderAllFields(vtk_fp,&byte_offset,(const int)npoints,(const MPntPDarcy*)marker_darcy);
		}
		
	fprintf( vtk_fp, "\t\t\t</PointData>\n");
	fprintf( vtk_fp, "\n");
	/* point data END */
	
	
	fprintf( vtk_fp, "\t\t</Piece>\n");
	fprintf( vtk_fp, "\t</UnstructuredGrid>\n");
	
	/* WRITE APPENDED DATA HERE */
	fprintf( vtk_fp,"\t<AppendedData encoding=\"raw\">\n");
	fprintf( vtk_fp,"_");
	
	/* connectivity, offsets, types, coords */
	////////////////////////////////////////////////////////
	/* write connectivity */
	length = sizeof(int)*npoints;
	fwrite( &length,sizeof(int),1,vtk_fp);
	for (k=0; k<npoints; k++) {
		int idx = k;
		fwrite( &idx, sizeof(int),1, vtk_fp );
	}
	////////////////////////////////////////////////////////
	/* write offset */
	length = sizeof(int)*npoints;
	fwrite( &length,sizeof(int),1,vtk_fp);
	for (k=0; k<npoints; k++) {
		int idx = k+1;
		fwrite( &idx, sizeof(int),1, vtk_fp );
	}
	////////////////////////////////////////////////////////
	/* write types */
	length = sizeof(unsigned char)*npoints;
	fwrite( &length,sizeof(int),1,vtk_fp);
	for (k=0; k<npoints; k++) {
		unsigned char idx = 1; /* VTK_VERTEX */
		fwrite( &idx, sizeof(unsigned char),1, vtk_fp );
	}
	////////////////////////////////////////////////////////
	/* write coordinates */
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	length = sizeof(double)*npoints*3;
	fwrite( &length,sizeof(int),1,vtk_fp);
	for (k=0; k<npoints; k++) {
		MPntStd *marker;
		double  *coor;
		double  coords_k[] = {0.0, 0.0, 0.0};
		
		DataFieldAccessPoint(PField_std,k,(void**)&marker);
		MPntStdGetField_global_coord(marker,&coor);
		coords_k[0] = coor[0];
		coords_k[1] = coor[1];
		
		fwrite( coords_k, sizeof(double), 3, vtk_fp );
	}
	DataFieldRestoreAccess(PField_std);
	
	/* auto generated shit for the marker data goes here */
	{
		MPntStd      *markers_std     = PField_std->data;
		MPntPStokes   *markers_stokes   = PField_stokes->data;
		MPntStdVTKWriteBinaryAppendedDataAllFields(vtk_fp,npoints,markers_std);
		MPntPStokesVTKWriteBinaryAppendedDataAllFields(vtk_fp,npoints,markers_stokes);
	}


	if (foundthermal==BTRUE){
		MPntPThermal *markers_thermal = PField_thermal->data;
		MPntPThermalVTKWriteBinaryAppendedDataAllFields(vtk_fp,npoints,markers_thermal);
	}
	if (foundplastic==BTRUE){
	    MPntPStokesPl *markers_stokespl = PField_stokespl->data;
		MPntPStokesPlVTKWriteBinaryAppendedDataAllFields(vtk_fp,npoints,markers_stokespl);	
	}
	if (foundmelt==BTRUE){
		MPntPStokesMelt *markers_stokesmelt = PField_stokesmelt->data; /* should write a function to do this */
		MPntPStokesMeltVTKWriteBinaryAppendedDataAllFields(vtk_fp,npoints,markers_stokesmelt);	
	}
    
    if (foundchrono==BTRUE){
		MPntPChrono *markers_chrono = PField_chrono->data; /* should write a function to do this */
		MPntPChronoVTKWriteBinaryAppendedDataAllFields(vtk_fp,npoints,markers_chrono);	
	}

    if (founddarcy==BTRUE){
		MPntPDarcy *markers_darcy = PField_darcy->data; /* should write a function to do this */
		MPntPDarcyVTKWriteBinaryAppendedDataAllFields(vtk_fp,npoints,markers_darcy);	
	}
	
	
	fprintf( vtk_fp,"\n\t</AppendedData>\n");
	
	
	fprintf( vtk_fp, "</VTKFile>\n");
	
	
	if( vtk_fp!= NULL ) {
		fclose( vtk_fp );
		vtk_fp = NULL;
	}
	
	ierr = PetscGetTime(&t1);CHKERRQ(ierr);
#ifdef PROFILE_TIMING
	PetscPrintf(PETSC_COMM_WORLD,"VTKWriter(%s): Time %1.4e sec\n",__FUNCT__,t1-t0);
#endif
	PetscFunctionReturn(0);
}
#undef __FUNCT__
#define __FUNCT__ "__GENE_SwarmView_MPntP_PVTU"
PetscErrorCode __GENE_SwarmView_MPntP_PVTU(const char prefix[],const char name[],DataBucket db)
{
	PetscMPIInt nproc,rank;
	FILE *vtk_fp;
	PetscInt i;
	char *sourcename;
	
	BTruth foundplastic,foundmelt,foundthermal,foundchrono,founddarcy;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	
	DataBucketQueryDataFieldByName(db,MPntPStokesPl_classname,&foundplastic);
	DataBucketQueryDataFieldByName(db,MPntPStokesMelt_classname,&foundmelt);
	DataBucketQueryDataFieldByName(db,MPntPThermal_classname,&foundthermal);	
	DataBucketQueryDataFieldByName(db,MPntPChrono_classname,&foundchrono);
	DataBucketQueryDataFieldByName(db,MPntPDarcy_classname,&founddarcy);
	
	if ((vtk_fp = fopen ( name, "w")) == NULL)  {
		SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
	}
	
	/* (VTK) generate pvts header */
	fprintf( vtk_fp, "<?xml version=\"1.0\"?>\n");
	
#ifdef WORDSIZE_BIGENDIAN
	fprintf( vtk_fp, "<VTKFile type=\"PUnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
#else
	fprintf( vtk_fp, "<VTKFile type=\"PUnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
#endif
	
	/* define size of the nodal mesh based on the cell DM */
	fprintf( vtk_fp, "  <PUnstructuredGrid GhostLevel=\"0\">\n" ); /* note overlap = 0 */
	
	
	/* DUMP THE CELL REFERENCES */
	fprintf( vtk_fp, "    <PCellData>\n");
	fprintf( vtk_fp, "    </PCellData>\n");
	
	
	///////////////
	fprintf( vtk_fp, "    <PPoints>\n");
	fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\"/>\n");
	fprintf( vtk_fp, "    </PPoints>\n");
	///////////////
	
	///////////////
	fprintf(vtk_fp, "    <PPointData>\n");
	MPntStdPVTUWriteAllPPointDataFields(vtk_fp);
	MPntPStokesPVTUWriteAllPPointDataFields(vtk_fp);
	if (foundthermal==BTRUE){
	MPntPThermalPVTUWriteAllPPointDataFields(vtk_fp);
	}
	if (foundplastic==BTRUE){
	MPntPStokesPlPVTUWriteAllPPointDataFields(vtk_fp);
	}
	if (foundmelt==BTRUE){
	MPntPStokesMeltPVTUWriteAllPPointDataFields(vtk_fp);
	}
	if (foundchrono==BTRUE){
	MPntPChronoPVTUWriteAllPPointDataFields(vtk_fp);
	}
	if (founddarcy==BTRUE){
	MPntPDarcyPVTUWriteAllPPointDataFields(vtk_fp);
	}
	fprintf(vtk_fp, "    </PPointData>\n");
	///////////////
	
	
	/* write out the parallel information */
	MPI_Comm_size(PETSC_COMM_WORLD,&nproc);
	for (i=0; i<nproc; i++) {
		asprintf( &sourcename, "%s-subdomain%1.5d.vtu", prefix, i );
		fprintf( vtk_fp, "    <Piece Source=\"%s\"/>\n",sourcename);
		free(sourcename);
	}
	
	/* close the file */
	fprintf( vtk_fp, "  </PUnstructuredGrid>\n");
	fprintf( vtk_fp, "</VTKFile>\n");
	
	if(vtk_fp!=NULL){
		fclose( vtk_fp );
		vtk_fp = NULL;
	}
	
	PetscFunctionReturn(0);
}

