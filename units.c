
#include "petsc.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "units.h"

#define _UNITS_APPLY_SCALING(U,X) (U)->characteristic_scale * (X)
#define _UNITS_APPLY_INVERSE_SCALING(U,X) (X) / (U)->characteristic_scale


/* ystar = L x y */
void UnitsApplyScaling(Units u,double y,double *ystar)
{
	*ystar = _UNITS_APPLY_SCALING(u,y);
}

void UnitsApplyScalingToArray(Units u,int n,double y[],double ystar[])
{
	int i;
	
	for (i=0; i<n; i++) {
		ystar[i] = _UNITS_APPLY_SCALING(u,y[i]);
	}
}

void UnitsApplyInverseScaling(Units u,double y,double *ystar)
{
	*ystar = _UNITS_APPLY_INVERSE_SCALING(u,y);
}

void UnitsApplyInverseScalingToArray(Units u,int n,double y[],double ystar[])
{
	int i;
	
	for (i=0; i<n; i++) {
		ystar[i] = _UNITS_APPLY_INVERSE_SCALING(u,y[i]);
	}
}

void UnitsConvertSI2Kilo(double si,double *ksi)
{
	(*ksi) = 1.0e3*si;
}

void UnitsConvertSI2Mega(double si,double *ksi)
{
	(*ksi) = 1.0e6*si;
}

void UnitsConvertSI2Giga(double si,double *ksi)
{
	(*ksi) = 1.0e9*si;
}

void UnitsConvertSI2Milli(double si,double *ksi)
{
	(*ksi) = 1.0e-3*si;
}

void UnitsConvertSI2Micro(double si,double *ksi)
{
	(*ksi) = 1.0e-6*si;
}

void UnitsConvertSI2Pico(double si,double *ksi)
{
	(*ksi) = 1.0e-9*si;
}

void UnitsCreate(Units *_u,const char quantity_name[],const char unit_name[],double scale)
{
	Units u;
	
	u = malloc( sizeof(struct _p_Units) );
	memset( u, 0, sizeof(struct _p_Units) );
	
	asprintf(&u->quantity_name,"%s",quantity_name);
	asprintf(&u->unit_name,"%s",unit_name);
	u->characteristic_scale = scale;
	
	*_u = u;
}

void UnitsDestroy(Units *_u)
{
	Units u;
	
	if (!_u){ return; }
	u = *_u;
	free(u);
	
	*_u = NULL;
}

void UnitsSetScale(Units u,double scale)
{
	u->characteristic_scale = scale;
}

void UnitsGetScale(Units u,double *scale)
{
	*scale = u->characteristic_scale;
}

void UnitsView(Units u)
{
	PetscPrintf(PETSC_COMM_WORLD,"Unit: %s (%s) - scale %1.4e \n", u->quantity_name,u->unit_name,u->characteristic_scale );
}

void UnitsCreateSIVelocity(Units *u)
{
	UnitsCreate(u,"velocity","m/s",1.0);
}

void UnitsCreateSILength(Units *u)
{
	UnitsCreate(u,"length","m",1.0);
}

void UnitsCreateSITime(Units *u)
{
	UnitsCreate(u,"time","s",1.0);
}

void UnitsCreateSIStress(Units *u)
{
	UnitsCreate(u,"stress","Pa",1.0);
}

void UnitsCreateSIViscosity(Units *u)
{
	UnitsCreate(u,"viscosity","Pa.s",1.0);
}

void UnitsCreateSITemperature(Units *u)
{
	UnitsCreate(u,"temperature","K",1.0);
}

