In order to reproduce the dat file
 this will run all the experiments one after each other, should take 8 hours approx
>> ./run_test.sh path_to_ptat &
 this will go through all directory and produce the *.dat file
>>./create_datfiles.sh
 this will produce the figs of the paper
and save them as eps files
>> python residuals.py
>> python CPUtime.py

the linear run plot on the fig is the one from the directory surfaceplot
all dat files are obtained from 1 CPU run on octopus.
