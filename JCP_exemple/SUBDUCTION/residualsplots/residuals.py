import numpy as np
import matplotlib.pyplot as plt
import sys
import os.path
n=[]
rtollist=[2,3,4,5,6]
tick=['*','>','s','d','o']
j=0
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rc('legend',**{'fontsize':8})
plt.rc('lines', linewidth=0.5)
fig = plt.figure(figsize=(3,4), dpi=300)
ax  = plt.axes([0.15,0.1,0.80,0.85]) 
plt.xlim(0,61)
plt.ylim(-8,-1)
for rtol in rtollist:
	i=0
	truc = 'ew_0'+str(rtol)   
	snes_it=[]
	F=[]
	dt=[]
	cputime=[]
	for filename in os.listdir ('./'):
		if filename.endswith('.dat') and truc in filename:
			print filename
			data    = np.loadtxt(filename)
                        dt.append(float(filename[17:19])*3)
			cputime.append(np.sum(data[:,2])/1000)
			F.append(np.log10(data[-1,4]))
			snes_it.append(np.mean(data[:,3]))
			n.append(i)
			i+=1
	print dt
	print cputime
	myorder=[0,3,5,1,2,4]
        
	dt = [ dt[i] for i in myorder]
	F = [ F[i] for i in myorder]
        str_l = 'rtol=$10^{-'+str(rtol)+'}$'
        print str_l
	plt.plot(dt,F ,'-k'+tick[j],markersize=3.5,label= str_l)
        j+=1
plt.plot([0 ,61],[-4,-4],'-.k')
plt.legend(loc='upper left',numpoints=1,frameon=False) 
plt.xlabel(r'$\Delta t$ ($10^{10}$ s)',fontsize=10)
plt.ylabel(r'$\log_{10} ||F||$',fontsize=10)
ax.tick_params(axis='both', which='major', labelsize=8)
plt.savefig('residuals.eps', format='eps')
plt.show()


