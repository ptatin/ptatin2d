#! /bin/bash
declare -a rtollist=('2' '3' '4' '5' '6');
declare -a dtlist=('1' '2' '5' '10' '15' '20');

for rtol in ${rtollist[@]}; do
   for dt in ${dtlist[@]}; do
   output=`echo 'subduction_ew_0'$rtol'_'$dt`
   stdout=`echo 'outew_0'$rtol'_'$dt` 
   dt=`echo "$(($dt * 3))"'e10' `
   srtol=`echo '1.e-'$rtol` 
   $1/pTatin2d_two_stage.app -options_file subduction_stabilised_ew.opts -snes_rtol $srtol -output_path $output  -dt_max $dt > $stdout 
done
done

