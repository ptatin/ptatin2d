#!bin/sh
#reads all the ptatin.log files in all the outputdirectories
#generate *.dat file compatible with residuals.py and CPUtime.py

for file in subduction_ew_*_*/; do
output=`echo $file| sed '$s/.$//' | sed  's/$/\.dat/'`
awk '/dt_min/ { print $2 }' $file/ptatin.log > tmp1
awk '/SNES/ { print $12 }' $file/ptatin.log > tmp2
awk '/CPU.*(stk_nwt)/ { print $4 }' $file/ptatin.log  > tmp3
awk '/SNES/ { print $8  }' $file/ptatin.log  > tmp4
awk '/SNES/ { print $6  }' $file/ptatin.log  > tmp5
paste tmp1 tmp2 tmp3 tmp4 tmp5 > tmp6
sed 's/;//g' tmp6 > $output
\rm tmp1 tmp2 tmp3 tmp4 tmp5 tmp6
done
