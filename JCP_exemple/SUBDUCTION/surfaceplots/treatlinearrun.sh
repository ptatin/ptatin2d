#!/bin/sh
# exectute in the output file and give the output file as argument
# cd my_output_path
# ../treatlinearrun.sh ../L2.dat
# creates the file L2.dat that contains model time and number of linear iteration at each time step
awk '/dt_min/ { print $2 }' ptatin.log > tmp1
awk '/pre_/ { print $12 }' ptatin.log  > tmp2
paste tmp1 tmp2  > $1
\rm tmp1 tmp2

