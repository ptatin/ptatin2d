to reproduce the figure of the paper

drunk subduction with no correction and time step 2 kyr
$PATH_TO_PTAT/pTatin_two_stage -options_file subduction_linear.ops -dt_max 6e10 -nsteps 50 -output_path subduction_sick

stable subduction with no correction and  time step 1kyr L4
$PATH_TO_PTAT/pTatin_two_stage -options_file subduction_linear.ops  -output_path subduction_L4 -max_time 

LONG TERM STABLE SUBDUCTION PICTURES
stable subduction with correction and  time step 20 kyr 
$PATH_TO_PTAT/pTatin_two_stage -options_file subduction_stabilised.ops -nsteps 1200 -output_frequency 50  -output_path subduction_longterm




