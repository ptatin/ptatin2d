#!/bin/sh
# this scripts takes the file containing the std output of ptat2D
# and output a dat file containing 3 column  newton step // non linear residual // number of linear its 
# during the newton stage of a one step 
fout=$1.dat
awk '/newton/{i++}i' $1  > tmp
awk '/iteration/ { print $8 }' tmp > tmp4
awk 'BEGIN{print"0"}1' tmp4 > tmp3
awk '/SNES/ { print $5 }'  tmp > tmp2
awk '/SNES/ { print $1 }' tmp > tmp1
paste tmp1 tmp2 tmp3  > $fout 
\rm tmp*
