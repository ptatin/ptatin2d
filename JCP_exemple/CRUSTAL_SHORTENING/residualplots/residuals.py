import numpy as np
import matplotlib.pyplot as plt
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rc('legend',**{'fontsize':8})

plt.figure(figsize=(6.3,4.5), dpi=72)

data = np.loadtxt("out_crustal_shortening_M.dat")
nl_its_N = data[:,0]
res_N    = np.log10(data[:,1])
l_its_N  = data[:,2]

data = np.loadtxt("out_crustal_shortening_M_picard.dat")
nl_its_P = data[:,0]
res_P    = np.log10(data[:,1])
l_its_P  = data[:,2]


ax1 = plt.axes([0.1,0.1,0.25,0.85])
ax1.plot(nl_its_P, res_P, 'k-d',markersize=3,markeredgecolor='k',markerfacecolor='w')
ax1.plot(nl_its_N, res_N, 'k-o',markersize=3,markeredgecolor='k',markerfacecolor='w')
ax1.set_xlabel('non-linear its',fontsize=10)
ax1.set_title('Mises',fontsize=10)
ax1.set_ylabel("$\log_{10}||F||$",fontsize=10)
ax1.set_ylim(-13,-3)
ax1.set_xlim(1,50)
ax1.set_yticks([])


ax2 = ax1.twinx()
ax2.bar(nl_its_P,l_its_P, facecolor='k', edgecolor='k')
ax2.bar(nl_its_N,l_its_N, facecolor='w', edgecolor='k')
#ax2.set_ylabel('linear iteration',fontsize=10)
ax2.set_ylim(0,110)
ax2.set_xlim(1,50)
ax2.set_yticks([])

data = np.loadtxt("out_crustal_shortening_DP.dat")
nl_its_N = data[:,0]
res_N    = np.log10(data[:,1])
l_its_N  = data[:,2]

data = np.loadtxt("out_crustal_shortening_DP_picard.dat")
nl_its_P = data[:,0]
res_P    = np.log10(data[:,1])
l_its_P  = data[:,2]


ax3 = plt.axes([0.38,0.1,0.25,0.85])
ax3.plot(nl_its_P, res_P, 'k-d',markersize=3,markeredgecolor='k',markerfacecolor='w')
ax3.plot(nl_its_N, res_N, 'k-o',markersize=3,markeredgecolor='k',markerfacecolor='w')
ax3.set_xlabel('non-linear its',fontsize=10)
ax3.set_title('DP $g$= 0 ms$^{-2}$',fontsize=10)
ax3.set_ylim(-13,-3)
ax3.set_xlim(0,50)
ax3.set_yticks([])

ax4 = ax3.twinx()
ax4.bar(nl_its_P,l_its_P, facecolor='k', edgecolor='k')
ax4.bar(nl_its_N,l_its_N, facecolor='w', edgecolor='k')
ax4.set_ylim(0,110)
ax4.set_xlim(0,50)
ax4.set_yticks([])


data = np.loadtxt("out_crustal_shortening_DP_grav.dat")
nl_its_N = data[:,0]
res_N    = np.log10(data[:,1])
l_its_N  = data[:,2]

data = np.loadtxt("out_crustal_shortening_DP_grav_picard.dat")
nl_its_P = data[:,0]
res_P    = np.log10(data[:,1])
l_its_P  = data[:,2]

ax5 = plt.axes([0.66,0.1,0.25,0.85])
ax5.plot(nl_its_P, res_P, 'k-d',markersize=3,markeredgecolor='k',markerfacecolor='w',label='picard')
ax5.plot(nl_its_N, res_N, 'k-o',markersize=3,markeredgecolor='k',markerfacecolor='w',label='newton')
ax5.set_xlabel('non-linear its',fontsize=10)
ax5.set_title('DP $g$= 10 ms$^{-2}$',fontsize=10)
ax5.set_ylim(-13,-3)
ax5.set_xlim(0,50)
ax5.set_yticks([])
ax5.legend(loc='center right',numpoints=1,frameon=False)

ax6 = ax5.twinx()
ax6.bar(nl_its_P,l_its_P, facecolor='k', edgecolor='k')
ax6.bar(nl_its_N,l_its_N, facecolor='w', edgecolor='k')
ax6.set_ylim(0,110)
ax6.set_xlim(0,50)
ax6.set_ylabel('linear its',fontsize=10)

ax6.set_yticks([10,20,30,40,50])        
ax1.set_yticks([-10,-9,-8,-7,-6,-5,-4,-3])
ax1.set_xticks([1,10,20,30,40,50])
ax3.set_xticks([1,10,20,30,40,50])
ax5.set_xticks([1,10,20,30,40,50])
plt.savefig('nonlinresiduals.eps', format='eps')
plt.show()
