all these models are run with driver pTatin2d_two_stage for one time step
model dimension is  120 km x 25 km
and represents a layered upper crust and a bit of ductile lower crust 
run with 128x32 elements to generate figures

DP_grav has Co = 2e7  Pa  Phi = 30°  g = 10 m/s2
DP      has Co = 9e7  Pa  Phi = 30°  g = 0 m/s2
M       has Co = 1e8  Pa  Phi = 0°   g = 0 m/s2
Linear does not yield and but has gravity 

the set up is drawn on figure 1
 




