/*
  
 
 Input / command line parameters:
 -Ttop
 -Tbot 
 -mx
 -my
 -vx -3.000000e-10
 -bc_type 0
 -output_frequency 1
 -output_path mumu
 -remesh_type 2
 -stokes_scale_length 1.000000e+04
-stokes_scale_velocity 1.000000e-10
-stokes_scale_viscosity 1.000000e+27
-dt_max 3e11
-nsteps 500
-time_max 2.000000e+15
-output_frequency 20

 
 
 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"

#define WRITE_VP
#define WRITE_TFIELD
#define WRITE_MPOINTS
//#define WRITE_QPOINTS
#define WRITE_TCELL
//#define WRITE_INT_FIELDS
//#define WRITE_FACEQPOINTS


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_mumu"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_mumu(pTatinCtx ctx)
{
//	PhaseMap phasemap; //use a phase map
//	TopoMap topomap;   //use initial topogrpahy
	PetscInt M,N;      /* number of elments in x and y */
	PetscBool flg;
	PetscBool found; 
	PetscErrorCode ierr;
	PetscReal Ox,Oy,Lx,Ly,gravity_acceleration;
	char name[PETSC_MAX_PATH_LEN];
	char map_file[PETSC_MAX_PATH_LEN];
	char topo_file[PETSC_MAX_PATH_LEN];
	pTatinUnits *units;	
	
	PetscFunctionBegin;
	units = &ctx->units;
	gravity_acceleration = 10.0;
    Ox = 0.0;
    Oy = -200.e3;
    Lx = 600.e3;
    Ly = 0.0;
    
	/* make a regular non dimensional grid from option file parameters*/
    ierr = PetscOptionsGetReal(PETSC_NULL,"-grav",&gravity_acceleration,&found);CHKERRQ(ierr);
    ierr = QuadratureStokesGravityModelCoordinateAlignedSetValues(ctx->Q,0.0, -gravity_acceleration);CHKERRQ(ierr);
    	
	ierr  = PetscOptionsGetReal(PETSC_NULL,"-Ox",&Ox,&found);CHKERRQ(ierr);
	ierr  = PetscOptionsGetReal(PETSC_NULL,"-Oy",&Oy,&found);CHKERRQ(ierr);
	ierr  = PetscOptionsGetReal(PETSC_NULL,"-Lx",&Lx,&found);CHKERRQ(ierr);
	ierr  = PetscOptionsGetReal(PETSC_NULL,"-Ly",&Ly,&found);CHKERRQ(ierr);
	
	
	PetscPrintf(PETSC_COMM_WORLD,"ApplyInitialMeshGeometry: options Domain_SI = [ %1.4e , %1.4e ] x [ %1.4e , %1.4e ] \n", Ox, Lx,Oy,Ly );	
	UnitsApplyInverseScaling(units->si_length,Ox,&Ox);
	UnitsApplyInverseScaling(units->si_length,Oy,&Oy);
	UnitsApplyInverseScaling(units->si_length,Lx,&Lx);
	UnitsApplyInverseScaling(units->si_length,Ly,&Ly);
	
	PetscPrintf(PETSC_COMM_WORLD,"ApplyInitialMeshGeometry : options Domain_ND = [ %1.4e , %1.4e ] x [ %1.4e , %1.4e ] \n", Ox, Lx,Oy,Ly );
	ierr = DMDASetUniformCoordinates(ctx->dav,Ox,Lx,Oy,Ly, 0.0,0.0);CHKERRQ(ierr);
	
		{
		PetscBool active = PETSC_FALSE;
		ierr = pTatinPhysCompActivated(ctx,PhysComp_Energy,&active);CHKERRQ(ierr);
		
		if (active) {
			DM da;
			Vec T;
			PhysCompEnergyCtx phys_energy;
			TempMap  map;
			char file[PETSC_MAX_PATH_LEN];
			char prodmapname[PETSC_MAX_PATH_LEN];
			double T0;
			
			phys_energy = ctx->phys_energy;
			da          = phys_energy->daT;
			T           = phys_energy->T;
			
			
			ierr = DMDAProjectCoordinatesQ2toQ1_2d(ctx->dav,da);CHKERRQ(ierr);
		}
		}
	
	
	
	PetscFunctionReturn(0);
}



#undef __FUNCT__ 
#define __FUNCT__ "pTatin2d_ModelSetMaterialProperty_mumu"
PetscErrorCode pTatin2d_ModelSetMaterialProperty_mumu(pTatinCtx ctx)

{
	RheologyConstants      *rheology;
	PetscInt               nphase, i,rheol_type;
	PetscErrorCode         ierr;
	char   		       *option_name;
	PetscBool              found; 
	
	PetscFunctionBegin;
	
	rheol_type = 6; 
	ctx->rheology_constants.rheology_type = rheol_type;
	rheology   = &ctx->rheology_constants;
	nphase = 5;    /* or marker index  */
	
	rheology->eta_upper_cutoff_global = 1.e100;
	rheology->eta_lower_cutoff_global = 1.e-100;
	ierr       = PetscOptionsGetReal(PETSC_NULL,"-eta_max_cut_off",&rheology->eta_upper_cutoff_global,0);CHKERRQ(ierr);
	ierr       = PetscOptionsGetReal(PETSC_NULL,"-eta_min_cut_off",&rheology->eta_lower_cutoff_global,0);CHKERRQ(ierr);
	
	/* O croute Mafic granulite */
	rheology->temp_kappa[0] = 1e-6;
	rheology->temp_prod[0] = 0.;
        rheology->density_type[0] = 0;
        ierr = PetscOptionsGetInt(0,"-density_type_0",&(rheology->density_type[0]),0);CHKERRQ(ierr);
	rheology->const_rho0[0] = 2850.; 
	rheology->temp_alpha[0] = 3e-5;
	rheology->temp_beta[0] = 2e-12;
	rheology->viscous_type[0] = 2;
	rheology->arrh_nexp[0]    = 4.2;
	rheology->arrh_entalpy[0] = 445.e3;
	rheology->arrh_preexpA[0] = 1.4e+4;
	rheology->arrh_Ascale[0]  = 1e6;
	rheology->arrh_Vmol[0]    = 0.0;
	rheology->arrh_Tref[0]    = 273; 
	rheology->plastic_type[0] =2;
	rheology->mises_tau_yield[0]= 2.e7;
	rheology->dp_pressure_dependance[0]=30.;
	rheology->tens_cutoff[0] =1.e7;
	rheology->Hst_cutoff[0] = 3.e8;
	rheology->softening_type[0]=0;
	rheology->soft_Co_inf[0] = 2.e7;
	rheology->soft_phi_inf[0] = 0.1;
	rheology->soft_min_strain_cutoff[0] = 0.0 ;
	rheology->soft_max_strain_cutoff[0] = 1.0;
        rheology->melt_type[0] = 0;

    /* 1 manteau */
        rheology->temp_kappa[1] = 1e-6; 
	rheology->temp_prod[1] = 0.;
        rheology->density_type[1] = 0; 
        ierr = PetscOptionsGetInt(0,"-density_type_1",&(rheology->density_type[1]),0);CHKERRQ(ierr);
	rheology->const_rho0[1] = 3200.; 
	rheology->temp_alpha[1] = 2e-5;
	rheology->temp_beta[1]  = 2e-12;
	rheology->viscous_type[1] = 2;
	rheology->arrh_nexp[1] = 3;
	rheology->arrh_entalpy[1] = 520.e3;
	rheology->arrh_preexpA[1] = 70000.;
	rheology->arrh_Ascale[1]  = 1.e6; /* si A[MPA-n] Ascale=1.e6 else if A[Pa-n] then A=1. */
	rheology->arrh_Vmol[1]    = 0.0;
	rheology->arrh_Tref[1]    = 273; /* celcius 273 Kelvin 0 */
	rheology->plastic_type[1] = 2; /* Mises 1 DruckerPraeger 2  No plasticity 0 */
	rheology->mises_tau_yield[1]= 3.e8; /* cohesion or pressure independent */
	rheology->dp_pressure_dependance[1] = 0.0; /* friction in radian or tan phi  aller verifier */
	rheology->tens_cutoff[1] = 1.e8;  /* valeur pour manteau  en tension*/
	rheology->Hst_cutoff[1]  = 3.e8; /*  max general*/
	rheology->softening_type[1]= 0; /* linear soft */
	rheology->soft_Co_inf[1] = 2.e8;  /* infini strain*/ 
	rheology->soft_phi_inf[1] = 0.0;  /* infini strain */
	rheology->soft_min_strain_cutoff[1] = 0.0; /* beginning */
	rheology->soft_max_strain_cutoff[1] = 1.0;  /* beginning */
    rheology->melt_type[1] = 0;

    /* 2 bassin Qtz */
        rheology->temp_kappa[2] = 1e-6;
	rheology->temp_prod[2] = 0.;
        rheology->density_type[2] = 0; 
        ierr = PetscOptionsGetInt(0,"-density_type_2",&(rheology->density_type[2]),0);CHKERRQ(ierr);
	rheology->const_rho0[2] = 2850.; 
	rheology->temp_alpha[2] = 3e-5;
	rheology->temp_beta[2] = 2e-12;
	rheology->viscous_type[2] = 2;
	rheology->arrh_nexp[2]    = 2;
	rheology->arrh_entalpy[2] = 167.e3;
	rheology->arrh_preexpA[2] = 1.e-3;
	rheology->arrh_Ascale[2]  = 1e6;
	rheology->arrh_Vmol[2]    = 0.0;
	rheology->arrh_Tref[2]    = 273; 
	rheology->plastic_type[2] =2;
	rheology->mises_tau_yield[2]= 5.e6;
	rheology->dp_pressure_dependance[2]=25.;
	rheology->tens_cutoff[2] =1.e7;
	rheology->Hst_cutoff[2] = 3.e8;
	rheology->softening_type[2]=0;
	rheology->soft_Co_inf[2] = 2.e7;
	rheology->soft_phi_inf[2] = 0.1;
	rheology->soft_min_strain_cutoff[2] = 0.0 ;
	rheology->soft_max_strain_cutoff[2] = 1.0;
	rheology->melt_type[2] = 0;
	
	/* 3 TTG wet granite */
	rheology->temp_kappa[3] = 1e-6;
	rheology->temp_prod[3] = 0.;
        rheology->density_type[3] = 0; 
        ierr = PetscOptionsGetInt(0,"-density_type_3",&(rheology->density_type[3]),0);CHKERRQ(ierr);
	rheology->const_rho0[3] = 2850.; 
	rheology->temp_alpha[3] = 3e-5;
	rheology->temp_beta[3] = 2e-12;
	rheology->viscous_type[3] = 2;
	rheology->arrh_nexp[3]    = 1.9;
	rheology->arrh_entalpy[3] = 137e3;
	rheology->arrh_preexpA[3] = 2.e-4;
	rheology->arrh_Ascale[3]  = 1e6;
	rheology->arrh_Vmol[3]    = 0.0;
	rheology->arrh_Tref[3]    = 273; 
	rheology->plastic_type[3] = 2;
	rheology->mises_tau_yield[3]= 1e7;
	rheology->dp_pressure_dependance[3]=20.;
	rheology->tens_cutoff[3] =1.e7;
	rheology->Hst_cutoff[3] = 3.e8;
	rheology->softening_type[3]=1;
	rheology->soft_Co_inf[3] = 2.e7;
	rheology->soft_phi_inf[3] = 0.1;
	rheology->soft_min_strain_cutoff[3] = 0.0 ;
	rheology->soft_max_strain_cutoff[3] = 1.0;
        rheology->melt_type[3] = 1;
        rheology->melt_Tinf[3] = 1200.0;
        rheology->melt_Tsol[3] = 600.0;
        rheology->melt_rho[3]  = 2600.0;
        rheology->melt_eta[3]  = 5.0e19;
        ierr = PetscOptionsGetInt(0,"-melt_type_3",&(rheology->melt_type[3]),0);CHKERRQ(ierr);
        ierr = PetscOptionsGetReal(0,"-T_inf_3",&(rheology->melt_Tinf[3]),0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(0,"-Tsol_3",&(rheology->melt_Tsol[3]),0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(0,"-rhomelt_3",&(rheology->melt_rho[3]),0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(0,"-etamelt_3",&(rheology->melt_eta[3]),0);CHKERRQ(ierr);
	
	/* 4 Sediments wet granite */
	rheology->temp_kappa[4] = 1e-6;
	rheology->temp_prod[4] = 0.;
        rheology->density_type[4] = 0; 
        ierr = PetscOptionsGetInt(0,"-density_type_4",&(rheology->density_type[4]),0);CHKERRQ(ierr);
	rheology->const_rho0[4] = 2850; 
	rheology->temp_alpha[4] = 3e-5;
	rheology->temp_beta[4] = 2e-12;
	rheology->viscous_type[4] = 2;
	rheology->arrh_nexp[4]    = 1.9;
	rheology->arrh_entalpy[4] = 137e3;
	rheology->arrh_preexpA[4] = 2e-4;
	rheology->arrh_Ascale[4]  = 1e6;
	rheology->arrh_Vmol[4]    = 0.0;
	rheology->arrh_Tref[4]    = 273; 
	rheology->plastic_type[4] = 2;
	rheology->mises_tau_yield[4]= 1e7;
	rheology->dp_pressure_dependance[4]=20.;
	rheology->tens_cutoff[4] =1.e7;
	rheology->Hst_cutoff[4] = 3.e8;
	rheology->softening_type[4]=0;
	rheology->soft_Co_inf[4] = 1.e7;
	rheology->soft_phi_inf[4] = 0.6;
	rheology->soft_min_strain_cutoff[4] = 0.0 ;
	rheology->soft_max_strain_cutoff[4] = 1.0;
        rheology->melt_type[4] = 0;
    
	PetscFunctionReturn(0);
}

/*pTatin2d_ModelSetMarkerIndexFromMap_GENE
 assign index to markers from map that is in context
 */ 
#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetMarkerIndex_mumu"
PetscErrorCode pTatin2d_ModelSetMarkerIndex_mumu(pTatinCtx ctx)
{

	PetscInt               p,n_mp_points;
	DataBucket             db;
	DataField              PField_std;
	int                    phase;
	PetscScalar            ymoho,a_elips1,b_elips1,a_elips2,b_elips2,min[2],max[2],midpoint;
	DM da; 
	PetscErrorCode ierr;
	pTatinUnits *units;

	
	PetscFunctionBegin;
	
	units=&ctx->units; 
	da=ctx->dav;
	
	ymoho = -20.0e3; // SI units
	UnitsApplyInverseScaling(units->si_length,ymoho,&ymoho); // scaled with the rest DONT FORGET
	
	a_elips1 = 150.0e3; // ellipse basin phase 2 centered at 0 0
	b_elips1 = 15.0e3;  // ellipse basin phase 2
	a_elips2 = 150.0e3; // ellipse ttg phase 3 centered at 0 ymoho
	b_elips2 = 5.e3;   // ellipse ttg phase 3
        ierr  = PetscOptionsGetReal(PETSC_NULL,"-a_elips1",&a_elips1,0);CHKERRQ(ierr);
        ierr  = PetscOptionsGetReal(PETSC_NULL,"-b_elips1",&b_elips1,0);CHKERRQ(ierr);
        ierr  = PetscOptionsGetReal(PETSC_NULL,"-a_elips2",&a_elips2,0);CHKERRQ(ierr);
        ierr  = PetscOptionsGetReal(PETSC_NULL,"-b_elips2",&b_elips2,0);CHKERRQ(ierr);

	UnitsApplyInverseScaling(units->si_length,a_elips1,&a_elips1); 
	UnitsApplyInverseScaling(units->si_length,b_elips1,&b_elips1); 
	UnitsApplyInverseScaling(units->si_length,a_elips2,&a_elips2); 
	UnitsApplyInverseScaling(units->si_length,b_elips2,&b_elips2); 
	
	DMDAGetBoundingBox(da,min,max);
	midpoint = (min[0]+max[0])/2;
	
	/* define properties on material points */
	db = ctx->db;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	for (p=0; p<n_mp_points; p++) {
		MPntStd     *material_point;
		double  *position;
		DataFieldAccessPoint(PField_std,p,(void**)&material_point);		
		/* Access using the getter function provided for you (recommeneded for beginner user) */
		MPntStdGetField_global_coord(material_point,&position);
		if (position[1] > ymoho){
            phase = 0;
		} else {
            phase = 1;
		}
		
		/* basin phase 2 centered at 0 0 */
	 if ( pow((position[0]-midpoint),2)/pow(a_elips1,2) + pow(position[1],2)/pow(b_elips1,2) < 1) {
            phase = 2;
		} 
		/* TTG phase 3 centered at 0 ymoho */
	 if ( pow((position[0]-midpoint),2)/pow(a_elips2,2) + pow((position[1]-ymoho),2)/pow((b_elips2),2) < 1) {
            phase = 3;
		} 
		
		MPntStdSetField_phase_index(material_point,phase);		
	}
	
	DataFieldRestoreAccess(PField_std);

	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetInitialTemperatureOnNodes_mumu"
PetscErrorCode pTatin2d_ModelSetInitialTemperatureOnNodes_mumu(pTatinCtx ctx)
/* define properties on material points */
{
	PetscErrorCode         ierr;
	DM da;
	Vec T;
	PhysCompEnergyCtx phys_energy;
	PetscInt i,j,si,sj,nx,ny;
	Vec coords;
	DMDACoor2d **LA_coords;
	DM cda;
	PetscScalar **LA_T;
    PetscScalar Ttop,Tbot,jmin[2],jmax[2], age,hltherm, diffm;
	pTatinUnits *units;
	
	PetscFunctionBegin;
	
	phys_energy = ctx->phys_energy;
	da          = phys_energy->daT;
	T           = phys_energy->T;
	
	units = &ctx->units;
	Tbot  = 1500.0;
	Ttop  = 0.0;
	age   = 30.;
	hltherm = -6.e4;
	diffm= 1.e-6;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-Ttop",&Ttop,PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsGetScalar(PETSC_NULL,"-Tbot",&Tbot,PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsGetScalar(PETSC_NULL, "-age",&age,PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsGetScalar(PETSC_NULL, "-hltherm",&hltherm,PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsGetScalar(PETSC_NULL, "-diffm",&diffm,PETSC_NULL);CHKERRQ(ierr);
    age= age * 3.14e13; 
    UnitsApplyInverseScaling(units->si_time,age,&age);
    UnitsApplyInverseScaling(units->si_length,hltherm,&hltherm);
    UnitsApplyInverseScaling(units->si_diffusivity,diffm,&diffm);
    
	ierr = DMDAGetCoordinates(da,&coords);CHKERRQ(ierr);
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,coords,&LA_coords);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(da,T,&LA_T);CHKERRQ(ierr);
	ierr = DMDAGetCorners(da,&si,&sj,0 , &nx,&ny,0);CHKERRQ(ierr);
	
	DMDAGetBoundingBox(da,jmin,jmax);
	
	for( j=sj; j<sj+ny; j++ ) {
		for( i=si; i<si+nx; i++ ) {
//			PetscScalar xn,yn,Tic;
			double position[2];
			
			
			position[0] = LA_coords[j][i].x;
			position[1] = LA_coords[j][i].y;
			
			
			/* linear gradient from top 0 to bottom temp Tic*/ 
			LA_T[j][i] = (jmax[1]-position[1])/(jmax[1]-jmin[1])*(Tbot-Ttop)+Ttop; 
			
			/* Age effect from mars 4 2014*/ 
			LA_T[j][i] = Tbot; 
		    if (position[1] >= hltherm) {
			LA_T[j][i] = erf((jmax[1]-position[1])/sqrt(2.*diffm*age))*(Tbot-Ttop)+Ttop; 
//			LA_T[j][i] = 500; 
			} 
		}
	}
	ierr = DMDAVecRestoreArray(da,T,&LA_T);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(cda,coords,&LA_coords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetInitialStokesVariableOnMarker_mumu"
PetscErrorCode pTatin2d_ModelSetInitialStokesVariableOnMarker_mumu(pTatinCtx ctx)
/* define properties on material points */
{
	PhaseMap               phasemap;
	PetscInt               e,p,n_mp_points;
	DataBucket             db;
	DataField              PField_std,PField_stokes;
	int                    phase_index, i;
	PetscErrorCode         ierr;
	RheologyConstants      *rheology;
	double                 eta_ND,rho_ND; 
	pTatinUnits            *units;	
	
	PetscFunctionBegin;
	units   = &ctx->units;
	
	rheology   = &ctx->rheology_constants;
	db = ctx->db;
	
	
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	for (p=0; p<n_mp_points; p++) {
		MPntStd     *material_point;
		MPntPStokes *mpprop_stokes;
		DataFieldAccessPoint(PField_std,p,(void**)&material_point);
		DataFieldAccessPoint(PField_stokes,p,(void**)&mpprop_stokes);
		MPntStdGetField_phase_index(material_point,&phase_index);
		
		UnitsApplyInverseScaling(units->si_viscosity,1.e+21,&eta_ND);
		UnitsApplyInverseScaling(units->si_force_per_volume,rheology->const_rho0[phase_index],&rho_ND);
		MPntPStokesSetField_eta_effective(mpprop_stokes,eta_ND);
		MPntPStokesSetField_density(mpprop_stokes,rho_ND);	
	}
	
	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokes);
	
	PetscFunctionReturn(0);
}

/*pTatin2d_ModelSetInitialEnergyVariableOnMarker_GENE
 assign initial kappa and prod 
 */ 
#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetInitialEnergyVariableOnMarker_mumu"
PetscErrorCode pTatin2d_ModelSetInitialEnergyVariableOnMarker_mumu(pTatinCtx ctx)
{
	DataField         PField_thermal,PField_std,PField_stokes;
	DataBucket        db;
	int               phase_index;
	PetscInt          p,n_mp_points;
	PetscErrorCode    ierr;
	RheologyConstants *rheology;
	pTatinUnits *units;	
//	TempMap                map;
	PetscBool         isproduction;
//	char file[PETSC_MAX_PATH_LEN];
	
	PetscFunctionBegin;
	units   = &ctx->units;   
	rheology= &ctx->rheology_constants;
	db = ctx->db;
	
	/* standard data */
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	/* thermal data */
	DataBucketGetDataFieldByName(db,MPntPThermal_classname,&PField_thermal);
	DataFieldGetAccess(PField_thermal);
	DataFieldVerifyAccess(PField_thermal,sizeof(MPntPThermal));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	for (p=0; p<n_mp_points; p++) {
		MPntStd      *material_point;
		MPntPThermal *material_point_thermal;
		double       *position,kappa_ND,prod_ND;
		
		DataFieldAccessPoint(PField_std,    p,(void**)&material_point);
		DataFieldAccessPoint(PField_thermal,p,(void**)&material_point_thermal);
		MPntStdGetField_phase_index(material_point,&phase_index);
		position = material_point->coor;
		
		UnitsApplyInverseScaling(units->si_diffusivity,rheology->temp_kappa[phase_index],&kappa_ND);
		MPntPThermalSetField_diffusivity(material_point_thermal,kappa_ND);
		
		UnitsApplyInverseScaling(units->si_heatsource,rheology->temp_prod[phase_index],&prod_ND);
		MPntPThermalSetField_heat_source(material_point_thermal,prod_ND);
		
	}
	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_thermal);
	PetscFunctionReturn(0);    
}


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetInitialPlsVariableOnMarker_mumu"
PetscErrorCode pTatin2d_ModelSetInitialPlsVariableOnMarker_mumu(pTatinCtx ctx)
/* define properties on material points */
{
	PhaseMap               phasemap;
	PetscInt               e,p,n_mp_points;
	DataBucket             db;
	DataField              PField_std,PField_stokes,PField_stokespl;
	int                    phase_index, i;
	PetscErrorCode         ierr;
	RheologyConstants      *rheology;
	
	PetscFunctionBegin;
	
	rheology   = &ctx->rheology_constants;
	db = ctx->db;
	
	
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	/* get the plastic variables */
	DataBucketGetDataFieldByName(db,MPntPStokesPl_classname,&PField_stokespl);
	DataFieldGetAccess(PField_stokespl);
	DataFieldVerifyAccess(PField_stokespl,sizeof(MPntPStokesPl));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	for (p=0; p<n_mp_points; p++) {
		MPntStd     *material_point;
		MPntPStokesPl *mpprop_stokespl; /* plastic variables */
		DataFieldAccessPoint(PField_std,p,(void**)&material_point);
		DataFieldAccessPoint(PField_stokespl,p,(void**)&mpprop_stokespl);
		
		MPntStdGetField_phase_index(material_point,&phase_index);
		MPntPStokesPlSetField_plastic_strain(mpprop_stokespl,0.0);
		//TODO  Add noise if needed
		MPntPStokesPlSetField_yield_indicator(mpprop_stokespl,0.0);
	}
	
	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokespl);
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_mumu"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_mumu(pTatinCtx ctx)
{
	BCList         ubclist;
	PetscScalar    bcval;
	DM             dav;
	PetscErrorCode ierr;
	
	PetscReal   jmin[3],jmax[3]; 	
	PetscScalar opts_srH, opts_Lx,opts_Ly,p_base;
	PetscInt    opts_bcs;
	PetscScalar alpha_m;
	pTatinUnits *units;
	BC_Alabeaumont bcdata;
	BC_Step        bcdatastep;
	PetscScalar   y1,dy,coeff;
    
	PetscFunctionBegin;
	
	units   = &ctx->units;
	ubclist = ctx->u_bclist;
	dav     = ctx->dav;
	
	opts_srH = 1.e-10;
 	ierr = PetscOptionsGetScalar(PETSC_NULL,"-vx",&opts_srH,0);CHKERRQ(ierr);
	
	UnitsApplyInverseScaling(units->si_velocity,opts_srH,&opts_srH);
	
	opts_bcs = 0;
	ierr = PetscOptionsGetInt(PETSC_NULL,"-bc_type",&opts_bcs,0);CHKERRQ(ierr);
	
	
	DMDAGetBoundingBox(dav,jmin,jmax);
	opts_Lx = jmax[0]-jmin[0]; 
	opts_Ly = jmax[1]-jmin[1];
	
	switch(opts_bcs){
			// free surface and free slip all side with symetric horizontal velocity
		case 0 : {
			bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
		}
			break; 
			
			// incompressible symetric bottom fixed 
		case 1 : {
			bcval = -opts_srH*2.0*opts_Ly/opts_Lx;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
		}
			break; 
			
			// incompressible right side  
		case 2 : {
			bcval = -opts_srH*opts_Ly/opts_Lx;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
		} bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			break;
			
			// incompressible 4 sides  
		case 3 : {      
			bcval = -opts_srH*opts_Ly/opts_Lx;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
			bcval = opts_srH*opts_Ly/opts_Lx;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
			bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
		}
			break;
			
			// free slip everywhere  
		case 4 :    
			bcval = 0.0;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			break;      
			// free surface both side with mid point fixed 
			
		case 5 :    
			bcval = opts_srH;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_MIDPOINT,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_MIDPOINT,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);      
			break;
			
			// free surface right side  
		case 6 : {
			
			bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
		}  
			break;
			
			// free surface left side  
		case 7 : {
			
			bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = 0.0;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
		}
			break;
			// free surface and rigid wall on lateral side with symmetric horizontal velocity
		case 8 : {
			bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = 0.0;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = 0.0;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
		}
			break; 
			
		case 9 : {
		    BC_WrinklerData bcdata;
		    PetscScalar p_base,deltarho,yref,gravity_acceleration;
            PetscInt remesh_type; 
            PetscBool found;
            
			bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

			bcval = 0.0;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_BOTLEFTCORNERPOINT,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_BOTRIGHTCORNERPOINT,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr); 	
			
			/* basement pressure */
			PetscOptionsGetInt(PETSC_NULL,"-remesh_type",&remesh_type,0);
			if (remesh_type != 6 && remesh_type!=7){
			SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"In order to use a wrinkler foundation, you need to set -remesh_type option to 6 \n");
			}
		    ierr = PetscMalloc(sizeof(struct _p_BC_WrinklerData),&bcdata);CHKERRQ(ierr);
			ierr = PetscOptionsGetScalar(PETSC_NULL,"-winkler_p_base",&p_base,0);CHKERRQ(ierr);
		    UnitsApplyInverseScaling(units->si_stress,p_base,&p_base);
		    bcdata->Pisos=p_base;
		    
		    ierr = PetscOptionsGetScalar(PETSC_NULL,"-winkler_yref",&yref,0);CHKERRQ(ierr);
		    UnitsApplyInverseScaling(units->si_length,yref,&yref);
		    bcdata->yref=yref;
		    ierr  = PetscOptionsGetReal(PETSC_NULL,"-grav",&gravity_acceleration,&found);CHKERRQ(ierr);
		    if (found == PETSC_FALSE){
			SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"In order to use a wrinkler foundation, you need to set -grav option  \n");
			}
		    
		    ierr = PetscOptionsGetScalar(PETSC_NULL,"-winkler_deltarho",&deltarho,0);CHKERRQ(ierr);
		    UnitsApplyInverseScaling(units->si_force_per_volume,deltarho,&deltarho);
		    bcdata->deltarhog=deltarho*gravity_acceleration;
			
			ierr = SurfaceQuadratureStokesTraverseSetTraction(ctx->surfQ,DMDATracLoc_JMIN_LOC,SurfaceQuadatureEvaluator_Wrinkler,(void*)bcdata);CHKERRQ(ierr);
		    ierr = PetscFree(bcdata);CHKERRQ(ierr);
		}
			break; 	
			
	case 10 : {
		    BC_WrinklerData bcdata;
		    PetscScalar p_base,deltarho,yref,gravity_acceleration;
            PetscInt remesh_type; 
            PetscBool found;
            
			bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

			bcval = 0.0;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_MIDPOINT,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_MIDPOINT,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
			/* basement pressure */
			PetscOptionsGetInt(PETSC_NULL,"-remesh_type",&remesh_type,0);
			if (remesh_type != 7){
			SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"In order to use a wrinkler foundation, you need to set -remesh_type option to 7 \n");
			}
		    ierr = PetscMalloc(sizeof(struct _p_BC_WrinklerData),&bcdata);CHKERRQ(ierr);
			ierr = PetscOptionsGetScalar(PETSC_NULL,"-winkler_p_base",&p_base,0);CHKERRQ(ierr);
		    UnitsApplyInverseScaling(units->si_stress,p_base,&p_base);
		    bcdata->Pisos=p_base;
		    
		    ierr = PetscOptionsGetScalar(PETSC_NULL,"-winkler_yref",&yref,0);CHKERRQ(ierr);
		    UnitsApplyInverseScaling(units->si_length,yref,&yref);
		    bcdata->yref=yref;
		    ierr  = PetscOptionsGetReal(PETSC_NULL,"-grav",&gravity_acceleration,&found);CHKERRQ(ierr);
		    if (found == PETSC_FALSE){
			SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"In order to use a wrinkler foundation, you need to set -grav option  \n");
			}
		    
		    ierr = PetscOptionsGetScalar(PETSC_NULL,"-winkler_deltarho",&deltarho,0);CHKERRQ(ierr);
		    UnitsApplyInverseScaling(units->si_force_per_volume,deltarho,&deltarho); /* dimension rho.g appliquée sur rho dimensionné car g l'est */
		    bcdata->deltarhog=deltarho*gravity_acceleration;
			
			ierr = SurfaceQuadratureStokesTraverseSetTraction(ctx->surfQ,DMDATracLoc_JMIN_LOC,SurfaceQuadatureEvaluator_Wrinkler,(void*)bcdata);CHKERRQ(ierr);
		    /* a la difference de DMDABCListTraverse on integre cette BC sur les faces plutot que sur les noeuds */
		    ierr = PetscFree(bcdata);CHKERRQ(ierr);
		}
			break; 	
			case 11 : {
			/*incompressible free surface, basically fixe the velocity at the bottom to fill up or empty down with material that leaves on the side*/
			bcval = 2*opts_srH*opts_Ly/opts_Lx;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
			bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
		}
			break;
		case 12 : {
			ierr = PetscMalloc(sizeof(struct _p_BC_Alabeaumont),&bcdata);CHKERRQ(ierr);
			
			/*incompressible free surface à la beaumont*/
			ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_ylitho",&y1,0);CHKERRQ(ierr);
			ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_deltay",&dy,0);CHKERRQ(ierr);
			
			UnitsApplyInverseScaling(units->si_length,y1,&y1);
			UnitsApplyInverseScaling(units->si_length,dy,&dy);
			
			bcval = 0.0;
			bcdata->dim = 1;   
			bcdata->x0 = y1;   
			bcdata->x1 = y1-dy;
			coeff = (2*(jmax[1]-y1)+dy)/(2*(y1-dy-jmin[1]));
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
			bcdata->v0 = -opts_srH; bcdata->v1 = opts_srH*coeff; 
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_Alabeaumont,(void*)bcdata);CHKERRQ(ierr);	
			bcdata->v0 = opts_srH; bcdata->v1 = -opts_srH*coeff;
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_Alabeaumont,(void*)bcdata);CHKERRQ(ierr);
			ierr = PetscFree(bcdata);CHKERRQ(ierr);
		}
			break;	
			
		case 13 : {
			ierr = PetscMalloc(sizeof(struct _p_BC_Alabeaumont),&bcdata);CHKERRQ(ierr);
			
			/*incompressible free surface à la beaumont*/
			ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_ylitho",&y1,0);CHKERRQ(ierr);
			ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_deltay",&dy,0);CHKERRQ(ierr);
			
			UnitsApplyInverseScaling(units->si_length,y1,&y1);
			UnitsApplyInverseScaling(units->si_length,dy,&dy);
			
			bcval = 0.0;
			bcdata->dim = 1;   
			bcdata->x0 = y1;   
			bcdata->x1 = y1-dy;
			coeff = (2*(jmax[1]-y1)+dy)/(2*(y1-dy-jmin[1]));
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
			bcdata->v0 = -opts_srH; bcdata->v1 = opts_srH*coeff; 
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_Alabeaumont,(void*)bcdata);CHKERRQ(ierr);	
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcdata->v0 = opts_srH; bcdata->v1 = -opts_srH*coeff;
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_Alabeaumont,(void*)bcdata);CHKERRQ(ierr);
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			ierr = PetscFree(bcdata);CHKERRQ(ierr);
		}
			break;	
			
			
		case 14 : {
			ierr = PetscMalloc(sizeof(struct _p_BC_Alabeaumont),&bcdata);CHKERRQ(ierr);
			ierr =  PetscMalloc(sizeof(struct _p_BC_Step),&bcdatastep);CHKERRQ(ierr);
			
			/*incompressible free surface à la beaumont*/
			ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_ylitho",&y1,0);CHKERRQ(ierr);
			ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_deltay",&dy,0);CHKERRQ(ierr);
			
			UnitsApplyInverseScaling(units->si_length,y1,&y1);
			UnitsApplyInverseScaling(units->si_length,dy,&dy);
			
			bcval = 0.0;
			bcdata->dim = 1;   
			bcdata->x0 = y1;   
			bcdata->x1 = y1-dy;
			bcdatastep->beg = y1-dy; 
			bcdatastep->end = y1;
			bcdatastep->dim = 1; 
			bcdatastep->v0 = 0.0;
			bcdatastep->v1 = 0.0;
			bcdatastep->b = 0; 
			bcdatastep->a = 1.0;
			
			coeff = (dy+2*jmax[1]-2*y1)/(2*y1-2*jmin[1]-dy);
			
			
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
			bcdata->v0 = -opts_srH; bcdata->v1 = opts_srH*coeff; 
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_Alabeaumont,(void*)bcdata);CHKERRQ(ierr);	
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_Step,(void*)bcdatastep);CHKERRQ(ierr);
			bcdata->v0 = opts_srH; bcdata->v1 = -opts_srH*coeff;
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_Alabeaumont,(void*)bcdata);CHKERRQ(ierr);
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,1,BCListEvaluator_Step,(void*)bcdatastep);CHKERRQ(ierr);
			ierr = PetscFree(bcdata);CHKERRQ(ierr);
			ierr = PetscFree(bcdatastep);CHKERRQ(ierr);
		}
			break;	
			
		case 15 : {
			ierr = PetscMalloc(sizeof(struct _p_BC_Alabeaumont),&bcdata);CHKERRQ(ierr);
			ierr =  PetscMalloc(sizeof(struct _p_BC_Step),&bcdatastep);CHKERRQ(ierr);
			
			/*incompressible free surface à la beaumont*/
			ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_ylitho",&y1,0);CHKERRQ(ierr);
			ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_deltay",&dy,0);CHKERRQ(ierr);
			
			UnitsApplyInverseScaling(units->si_length,y1,&y1);
			UnitsApplyInverseScaling(units->si_length,dy,&dy);
			
			bcval = 0.0;
			bcdata->dim = 1;   
			bcdata->x0 = y1;   
			bcdata->x1 = y1-dy;
			bcdatastep->beg = y1-dy; 
			bcdatastep->end = y1;
			bcdatastep->dim = 1; 
			bcdatastep->v0 = 0.0;
			bcdatastep->v1 = 0.0;
			bcdatastep->b = 0; 
			bcdatastep->a = 1.0;
			
			coeff = (dy+2*jmax[1]-2*y1)/(2*y1-2*jmin[1]-dy);
			
			
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
			
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			
			bcdata->v0 = opts_srH; bcdata->v1 = -opts_srH*coeff;
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_Alabeaumont,(void*)bcdata);CHKERRQ(ierr);
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			ierr = PetscFree(bcdata);CHKERRQ(ierr);
			ierr = PetscFree(bcdatastep);CHKERRQ(ierr);
		}
			break;	
			
        case 16 : {
			PetscInt rigid,freeslip;
			// Geomod BC's
            
			ierr =  PetscMalloc(sizeof(struct _p_BC_Step),&bcdatastep);CHKERRQ(ierr);
			bcval = 0.0;
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
            ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
            ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
            
            rigid = 0;
            freeslip = 0;
            ierr = PetscOptionsGetInt(PETSC_NULL,"-bc_rigid",&rigid,0);CHKERRQ(ierr);  
            ierr = PetscOptionsGetInt(PETSC_NULL,"-bc_freeslip",&freeslip,0);CHKERRQ(ierr); 
            if (rigid==1){
                bcdatastep->beg = -1.0; 
                bcdatastep->end = 2.8;
                bcdatastep->dim = 1; 
                bcdatastep->v0 = bcval;
                bcdatastep->v1 = bcval;
                bcdatastep->b = -0.0; 
                bcdatastep->a = -0.5;
                ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,1,BCListEvaluator_Step,(void*)bcdatastep);CHKERRQ(ierr);
            }
            
            if (rigid ==2){       
                ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);    
            }
            
            if (freeslip == 0){
                bcval = 0.0;
                ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
                bcval = -2.5; 
                bcdatastep->beg = -1.0; 
                bcdatastep->end = 10;
                bcdatastep->dim = 1; 
                bcdatastep->v0 = bcval;
                bcdatastep->v1 = 0.0;
                bcdatastep->b = -0.0; 
                bcdatastep->a = -0.2;
                ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_Step,(void*)bcdatastep);CHKERRQ(ierr);
            } 
            
            if (freeslip ==1){
                bcval = -2.5; 
                ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
                ierr = PetscFree(bcdatastep);CHKERRQ(ierr);            
            }
        }
            break;	
            
            
    } 
    
    
    
    /* check for energy solver */
    {
        PetscBool active = PETSC_FALSE;
        ierr = pTatinPhysCompActivated(ctx,PhysComp_Energy,&active);CHKERRQ(ierr);
        
        if (active) {
            DM da;
            Vec T;
            PhysCompEnergyCtx phys_energy;
            PetscScalar bcval;
            PetscBool   found;
            
            phys_energy = ctx->phys_energy;
            da          = phys_energy->daT;
            T           = phys_energy->T;
            
            
            
            bcval = 0.0;
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-Ttop",&bcval,&found);CHKERRQ(ierr);
            ierr = DMDABCListTraverse(phys_energy->T_bclist,da,DMDABCList_JMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
            bcval = 1400.0;
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-Tbot",&bcval,&found);CHKERRQ(ierr);
            ierr = DMDABCListTraverse(phys_energy->T_bclist,da,DMDABCList_JMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
                    
            
            
        }
    }
    
    
    PetscFunctionReturn(0);
}



#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_mumu"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_mumu(pTatinCtx ctx)
{
	PetscErrorCode         ierr;
	
	pTatinUnits *units;	
	
	PetscFunctionBegin;
	units   = &ctx->units;  
	UnitsApplyInverseScaling(units->si_time,ctx->dt_min,&ctx->dt_min);
	UnitsApplyInverseScaling(units->si_time,ctx->dt,&ctx->dt);
	UnitsApplyInverseScaling(units->si_time,ctx->dt_adv,&ctx->dt_adv);
	UnitsApplyInverseScaling(units->si_time,ctx->dt_max,&ctx->dt_max);
	UnitsApplyInverseScaling(units->si_time,ctx->time_max,&ctx->time_max);
	UnitsApplyInverseScaling(units->si_length,ctx->max_disp_y,&ctx->max_disp_y);
	
	
	ierr = pTatin2d_ModelSetMaterialProperty_mumu(ctx); CHKERRQ(ierr);
	ierr = pTatin2d_ModelSetMarkerIndex_mumu(ctx); CHKERRQ(ierr);
	ierr = pTatin2d_ModelSetInitialStokesVariableOnMarker_mumu(ctx); CHKERRQ(ierr);
	ierr= pTatin2d_ModelSetInitialTemperatureOnNodes_mumu(ctx); CHKERRQ(ierr);
	ierr = pTatin2d_ModelSetInitialEnergyVariableOnMarker_mumu(ctx); CHKERRQ(ierr);
	ierr = pTatin2d_ModelSetInitialPlsVariableOnMarker_mumu(ctx); CHKERRQ(ierr);
	
	// Erosion
    {
      PetscBool active = PETSC_FALSE;
      ierr = pTatinPhysCompActivated(ctx,PhysComp_SPM,&active);CHKERRQ(ierr);
      if (active) {
          ierr= pTatin2d_ModelSetSPMParameters_GENE(ctx); CHKERRQ(ierr);
      }
    }

	PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetSPMParameters_mumu"
PetscErrorCode pTatin2d_ModelSetSPMParameters_mumu(pTatinCtx ctx)
{
 pTatinUnits *units;
 PetscScalar kappa,bcval,source,start_time;
 PhysCompSPMCtx spm;
 PetscErrorCode ierr; 
 
spm         = ctx->phys_spm;
units       = &ctx->units;

spm->NSedMarkPerCell = 4; // number of markers deposited by elements 
spm->sed_mark_id     = 4; // phase assigned to sediments
kappa                = 5.0;
source               = 0.0; // TODO define scaling for source
start_time           = 3e12;
ierr  = PetscOptionsGetReal(PETSC_NULL,"-spm_start_time",&start_time,0);CHKERRQ(ierr);
ierr  = PetscOptionsGetReal(PETSC_NULL,"-spm_kappa",&kappa,0);CHKERRQ(ierr);
PetscPrintf(PETSC_COMM_WORLD,"ModelSetSPMParameters: options = [ %1.4e , %1.4e ]  \n", start_time, kappa );
UnitsApplyInverseScaling(units->si_diffusivity,kappa,&kappa);
UnitsApplyInverseScaling(units->si_time,start_time,&start_time);
PetscPrintf(PETSC_COMM_WORLD,"ModelSetSPMParameters: optionsND = [ %1.4e , %1.4e ]  \n", start_time, kappa );
spm->source_0         = source ;
spm->kappa_0          = kappa ;
spm->start_time       = start_time; 


// Impossible to declare boundary condition like this because I need a traverse 1D function... 
// I think the simplest is to actually set these 2 fucking node without all the fancy BC traverse style..
// too much crap to write for 2 dofs.
//bcval = 0.0;ierr = DMDABCListTraverse(spm->bclist,spm->daSPM,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
//bcval = 0.0;ierr = DMDABCListTraverse(spm->bclist,spm->daSPM,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);


// TODO DEFINE THE POINTER FUNCTION THAT ASSIGN DIFFUSIVITY i.e. from phase, constant, as a func of height_old, time etc..//


PetscFunctionReturn(0);
}
 
