
#ifndef __material_point_population_control_h__
#define __material_point_population_control_h__

PetscErrorCode MPPC_AVDPatch(PetscInt np_lower,PetscInt np_upper,PetscInt patch_extend,PetscInt nxp,PetscInt nyp,PetscScalar pertub,DM da,DataBucket db);
PetscErrorCode MaterialPointPopulationControl(pTatinCtx ctx);

#endif