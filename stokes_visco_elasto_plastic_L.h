/*
 *  stokes_visco_elasto_plastic.h
 *  
 *
 *  Created by laetitia le pourhiet on 7/17/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef __pTatin_stokes_visco_elasto_plastic_h__
#define __pTatin_stokes_visco_elasto_plastic_h__

void Objective_Update_Jaumann (PetscReal theta_gp,PetscReal dt,PetscReal gradU[],PetscReal stress[]);
void Objective_Update_Oldyrod(PetscReal theta_gp,PetscReal dt,PetscReal gradU[],PetscReal stress[]);

void StokesVEP2d_Compute_Stress(pTatinCtx user,
                                RheologyConstants *rheology,PetscInt phase_gp, 
                                PetscReal position[],PetscReal velocity[],PetscReal gradU[],PetscReal pressure,
                                PetscReal pls, PetscReal *plsr,PetscInt *npointsyielded,PetscReal *eta,
                                PetscReal stress_old[], PetscReal stress[]);


void StokesIsotropicVE2d_Compute_Stress(pTatinCtx user,
                                        RheologyConstants *rheology,PetscInt phase_gp, 
                                        PetscReal position[],PetscReal velocity[],PetscReal gradU[],PetscReal pressure,
                                        PetscReal stress_old[], PetscReal stress[],PetscReal eta,PetscReal theta);

void StokesIsotropicVEP2d_Compute_VEP(
                                      pTatinCtx user,
                                      RheologyConstants *rheology,PetscInt phase_gp, 
                                      PetscReal position[],PetscReal velocity[],PetscReal gradU[],PetscReal pressure,
                                      PetscReal pls, PetscReal *plsr, PetscInt *npointsyielded, 
                                      PetscReal stress_old[], PetscReal stress[], PetscReal *eta, PetscReal *theta);

void Stokes2d_Check_Yield_DP(RheologyConstants *rheology, PetscInt phase_gp, 
                             PetscReal stress_pred[], PetscReal pressure,PetscReal pls, 
                             PetscReal *tau_yield, PetscBool *yielded);

void StokesIsotropic_Rigid_plastic(PetscReal tau_yield, PetscReal eta0, PetscReal gradU[],PetscReal *eta ,PetscReal *theta,PetscReal *plsr);

void StokesIsotropic2d_Compute_VE(
                                  pTatinCtx user,
                                  RheologyConstants *rheology,PetscInt phase_gp, 
                                  PetscReal position[],PetscReal velocity[],PetscReal gradU[],PetscReal pressure,
                                  PetscReal *theta,PetscReal *eta);

void Stokes2d_Compute_Creep(pTatinCtx user,
                            RheologyConstants *rheology, PetscInt phase_gp,   
                            PetscReal position[],PetscReal velocity[],PetscReal gradU[],PetscReal pressure, 
                            PetscReal *eta);

PetscErrorCode FormFunctionLocal_Stokes_ViscoElastoPlastic_U(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[],PetscScalar Ru[]);


PetscErrorCode EvaluateRheologyNonlinearitiesQuadratureStokes_ViscoElastoPlastic_A(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[]);
PetscErrorCode EvaluateRheologyNonlinearitiesQuadratureStokes_ViscoElastoPlastic(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[]);
PetscErrorCode TkUpdateRheologyQuadratureStokes_ViscoElastoPlastic_stress_history(pTatinCtx user,DM dau,PetscScalar u[]);
PetscErrorCode pTatin_TkUpdateRheologyQuadratureStokes_ViscoElastoPlastic(pTatinCtx user,DM dau,PetscScalar LA_Uloc[],DM dap,PetscScalar LA_Ploc[]);
PetscErrorCode FormFunctionLocal_Stokes_ViscoElastoPlastic_GradSigma(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[],PetscScalar Ru[]);
PetscErrorCode FormFunction_Stokes_ViscoElastoPlastic(SNES snes,Vec X,Vec F,void *ctx);

#endif
