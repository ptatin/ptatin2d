
#ifndef __pTatin2d_h__
#define __pTatin2d_h__

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscpc.h"
#include "petscdm.h"


#define MAX_QUAD_PNTS_1D   10
#define MAX_QUAD_PNTS      MAX_QUAD_PNTS_1D * MAX_QUAD_PNTS_1D
#define Q2_NODES_PER_EL_2D 9
#define Q2_NODES_PER_EL_1D 3

#define NSD    2
#define U_DOFS NSD
#define P_DOFS 1
#define NSTRESS 3

#define U_BASIS_FUNCTIONS Q2_NODES_PER_EL_2D


#define PBASIS_P1_GLOBAL_RELATIVE
//#define PBASIS_P0

/* choose a default pressure space if nothing is already defined */
#ifndef PBASIS_P0
#ifndef PBASIS_P1_GLOBAL
#ifndef PBASIS_P1_LOCAL
#ifndef PBASIS_P1_GLOBAL_RELATIVE
#define PBASIS_P0
#endif
#endif
#endif
#endif

#ifdef PBASIS_P0
#define P_BASIS_FUNCTIONS 1
#define ConstructNi_pressure(_xi,coords,Ni) ConstructNi_P0_2D(_xi,coords,Ni)
#endif
#ifdef PBASIS_P1_GLOBAL
#define P_BASIS_FUNCTIONS 3
#define ConstructNi_pressure(_xi,coords,Ni) ConstructNi_P1G_2D(_xi,coords,Ni)
#endif
#ifdef PBASIS_P1_LOCAL
#define P_BASIS_FUNCTIONS 3
#define ConstructNi_pressure(_xi,coords,Ni) ConstructNi_P1L_2D(_xi,coords,Ni)
#endif
#ifdef PBASIS_P1_GLOBAL_RELATIVE
#define P_BASIS_FUNCTIONS 3
#define ConstructNi_pressure(_xi,coords,Ni) ConstructNi_P1GRel_2D(_xi,coords,Ni)
#endif



typedef enum { PhysComp_Stokes=1, PhysComp_Energy,PhysComp_SPM,PhysComp_Darcy } PhysComponents;

typedef enum { DMDABCList_INTERIOR_LOC=1, DMDABCList_IMIN_LOC,DMDABCList_IMIN_MIDPOINT,DMDABCList_IMAX_LOC,DMDABCList_IMAX_MIDPOINT,DMDABCList_JMIN_LOC,DMDABCList_JMIN2_LOC,DMDABCList_JMAX_LOC,DMDABCList_BOTLEFTCORNERPOINT,DMDABCList_BOTRIGHTCORNERPOINT,DMDABCList_TOPLEFTCORNERPOINT,DMDABCList_TOPRIGHTCORNERPOINT,DMDABCList_IMIN_LOC_NC,DMDABCList_IMAX_LOC_NC,DMDABCList_JMIN_LOC_NC,DMDABCList_JMAX_LOC_NC} DMDABCListConstraintLoc;

typedef enum { BCList_UNCONSTRAINED=-1, BCList_DIRICHLET=-2 } BCListConstraintType;

typedef enum { 
	QRule_GaussLegendre1=1,
	QRule_GaussLegendre2,
	QRule_GaussLegendre3,
	QRule_Subcell2x2GaussLegendre3,
	Qrule_SubcellMxMGaussLegendreNp  } QuadratureStokesRule;

typedef enum { 
	Basis_Q1=1, 
	Basis_Q2,
	Basis_P0,
	Basis_P1L,
	Basis_P1G,
	Basis_P1GR } BasisType;

typedef struct _p_BCList *BCList;

typedef struct _p_QuadratureStokes *QuadratureStokes;
typedef struct _p_GaussPointCoefficientsStokes GaussPointCoefficientsStokes;

typedef struct _p_SurfaceQuadratureStokes *SurfaceQuadratureStokes;
typedef struct _p_SurfaceQPointCoefficientsStokes SurfaceQPointCoefficientsStokes;

typedef struct _p_pTatinCtx *pTatinCtx;

typedef struct _p_RheologyConstants RheologyConstants;
typedef struct _p_DarcyConstants DarcyConstants;
typedef struct _p_SurfaceRheologyConstants SurfaceRheologyConstants;
typedef struct _p_pTatinUnits pTatinUnits;


#include "dmda_compare.h"
#include "dmda_update_coords.h"
#include "dmda_duplicate.h"
#include "dmda_redundant.h"
#include "dmda_remesh.h"

#include "mesh_update.h"
#include "dmdae.h"
#include "element_type_Q1.h"
#include "element_type_Q2.h"
#include "quadrature.h"
#include "rheology.h"
#include "output_paraview.h"
#include "data_exchanger.h"
#include "swarm_fields.h"
#include "phase_map.h"
#include "topo_map.h"
#include "temperature_map.h"
#include "ptatin_log.h"

#include "MPntPStokesElas_def.h"
#include "MPntStd_def.h"
#include "material_point_std_utils.h"
#include "MPntPStokes_def.h"
#include "material_point_stokes_utils.h"
#include "material_point_population_control.h"
#include "MPntPStokesPl_def.h"
#include "material_point_stokespl_utils.h"
#include "MPntPThermal_def.h"
#include "material_point_thermal_utils.h"
#include "MPntPStokesMelt_def.h"
#include "material_point_stokesmelt_utils.h"
#include "MPntPChrono_def.h"
#include "material_point_chrono_utils.h"
#include "MPntPPassive_def.h"
#include "material_point_passive_utils.h"
#include "MPntPDarcy_def.h"
#include "material_point_darcy_utils.h"
#include "phys_darcy_equation.h"
#include "phys_energy_equation.h"
#include "physcomp_SPM.h"
#include "units.h"

struct _p_BCList {
	DM          dm;
	PetscInt    blocksize;
	PetscInt    N,L; /* L = N * blocksize */
	PetscInt    N_local,L_local;
	PetscScalar *vals_global;
	PetscInt    *dofidx_global;
	PetscInt    *dofidx_local;
	PetscScalar *vals_local;
	/* PetscScalar *scale_local; */
	PetscScalar *scale_global;
	PetscBool   allEmpty;
};

/* Gauss point based evaluation */
struct _p_GaussPointCoefficientsStokes {
  PetscScalar Fu[NSD];
  PetscScalar Fp;
  PetscScalar eta;
	/* This should below in a DataBucket */
	PetscScalar eta_ref,theta;
	PetscScalar gravity[NSD];
  PetscScalar coord[NSD];
  PetscInt    phase;
  PetscScalar pls,plsr;
  PetscScalar stress[NSTRESS];
  PetscScalar stress_old[NSTRESS];
};

struct _p_QuadratureStokes {
	/* for each element */
	PetscInt    ngp;
	PetscScalar *xi;
	PetscScalar *weight;
	/* basis function evaluations */
	PetscInt               u_basis,p_basis;
	PetscScalar            **U_Ni; /* U_Ni[p][...] */
	PetscScalar            **p_Ni;
	PetscScalar            ***U_GNi;
	PetscInt               ncells;
	GaussPointCoefficientsStokes *cellproperties;
};	

struct _p_SurfaceQPointCoefficientsStokes {
  PetscScalar coord[NSD];
	PetscScalar normal[NSD];
	PetscScalar tangent[NSD];
	PetscScalar traction[NSD];
	/* This should below in a DataBucket */
  PetscInt    phase;
	PetscReal   vx,vy,pressure,effective_eta,effective_rho;
};

struct _p_SurfaceQuadratureStokes {
	ConformingElementFamily e;
	QuadElementEdge edge_id;
	/* for each element face */
	/* quadrature */
	PetscInt    ngp;
	QPoint1d    gp1[3]; /* s coordinates */
	QPoint2d    gp2[3]; /* xi,eta coordinates */
	/* basis function for u at each point */
	//PetscInt    u_basis;
	//PetscReal   U_Ni[3][3];
	/* face data */
	PetscInt    nfaces;
	PetscInt    *cell_list; /* list of cells connected to the edge */
	SurfaceQPointCoefficientsStokes *surfproperties;
};	

#if 0
struct _p_PhysComp_StokesCtx {
  DM                stokes_pack,dav,dap;
	BCList            u_bclist,p_bclist;
	QuadratureStokes  Q;
	SurfaceQuadratureStokes surfQ[QUAD_EDGES]; /* four edges */
	PetscInt          mx,my; /* global mesh size */
	PetscBool         use_mf_stokes;
	PetscInt          coefficient_projection_type;
};

/*
  dx/dt = v

  x^{k+1} = x^k + dt . [ 0.5*( v^{k+1} + v^k) ]
*/
struct _p_PhysComp_CoordinatesCtx {
  DM       dax;
	BCList   x_bclist;
	PetscInt mx,my; /* global mesh size */
	/* additional data */
	Vec      vlast; /* velocity from previous time step */
};

struct _p_PhysComp_EnergyCtx {
  DM                 daT;
	BCList             T_bclist;
//	QuadratureStokesT        Q;
//	SurfaceQuadratureStokesT surfQ[QUAD_EDGES]; /* four edges */
	PetscInt           mx,my; /* global mesh size */
};
#endif

struct _p_pTatinUnits {
	Units si_velocity; /* prescribed */
	Units si_length; /* prescribed */
	Units si_viscosity; /* prescribed */
	Units si_time; /* L/U */
	Units si_stress; /* L/(ETA.U) */
	Units si_force_per_volume; /* (ETA.U)/L^2 */
	Units si_strainrate; /* 1/time */
	Units si_temperature; /* prescribed */
	Units si_diffusivity; /* ? L^2/T prescribed*/
	Units si_heatsource; /* Ks-1 */ 
	Units si_heatcapacity; /* J kg -1 K-1 = m^2 s-2  K-1 */ 
        Units si_acceleration; /* L/t^2 */
        Units si_density; /*ETA/L^2*t */
	Units velocity_si2mm_per_year;
	Units pressure_si2MPa;
	Units si_permeability; /* prescribed*/
};

struct _p_pTatinCtx {
	char       outputpath[PETSC_MAX_PATH_LEN];
  DM         pack,dav,dap;
	BCList     u_bclist,p_bclist;
	QuadratureStokes Q;
	SurfaceQuadratureStokes surfQ[QUAD_EDGES]; /* four edges */
//	void       *data;
//  void       *data2; 
	/* material points */
	DataBucket db;
	DataEx     ex;	
	/* passive markers */
	DataBucket db_passive;
	DataEx     ex_passive;		
	PetscInt   coefficient_projection_type;
	/* energy equation */
	PhysCompEnergyCtx phys_energy;
	/* SPM equation */
	PhysCompSPMCtx phys_spm;
	/* Darcy equation */
	PhysCompDarcyCtx phys_darcy;
	DarcyConstants darcy_constants;
	/* options */
	PetscInt   mx,my;
	PetscBool  use_mf_stokes;
	PetscBool  pre_use_mf_stokes;
	PetscBool  solverstatistics;
	/* snes continuation paramter */
	PetscInt continuation_m, continuation_M; 
	/* time stepping */
	PetscInt nsteps,step;
	PetscReal dt,dt_max,dt_min,dt_adv;
    PetscReal courant_surf,max_disp_y;
	PetscInt output_frequency,ve_incremental;
	PetscReal time_max,time;
	/* rheology */
  RheologyConstants rheology_constants;
	SurfaceRheologyConstants surf_rheology_constants;
	/* units */
	pTatinUnits units;
	/* model function pointers */
	PetscInt model_index;
	PetscErrorCode (*FP_pTatin2d_ModelApplyBoundaryCondition)(pTatinCtx);
	PetscErrorCode (*FP_pTatin2d_ModelApplyMaterialBoundaryCondition)(pTatinCtx);
	PetscErrorCode (*FP_pTatin2d_ModelApplyInitialMeshGeometry)(pTatinCtx);
	PetscErrorCode (*FP_pTatin2d_ModelApplyInitialMaterialGeometry)(pTatinCtx);
	PetscErrorCode (*FP_pTatin2d_ModelOutput)(pTatinCtx,Vec,const char*);
	PetscErrorCode (*FP_pTatin2d_ModelUpdateMeshGeometry)(pTatinCtx,Vec);
	PetscContainer model_data;
	PetscViewer log;
};

/* user define boundary condition */
typedef struct _p_BC_Poly *BC_Poly;
typedef struct _p_BC_Step *BC_Step;
typedef struct _p_BC_Geomod3 *BC_Geomod3;
typedef struct _p_BC_Alabeaumont *BC_Alabeaumont;
typedef struct _p_BC_Godfinger *BC_Godfinger;
typedef struct _p_BC_WrinklerData *BC_WrinklerData;

struct _p_BC_Poly {
	PetscInt dim;
	PetscScalar a,b,c;
	PetscScalar beg,end;
};

struct _p_BC_Step {
	PetscInt dim;
	PetscScalar beg,end;
	PetscScalar a,b;
	PetscScalar v0,v1;
};

struct _p_BC_Geomod3 {
	PetscInt dim;
	PetscScalar vx,time,x0;
};

struct _p_BC_Alabeaumont {
	PetscInt dim;
	PetscScalar x0,x1;
	PetscScalar v0,v1;
};
struct _p_BC_Godfinger {
	PetscScalar x0,xi,xe;
	PetscScalar vx;
};
struct _p_BC_WrinklerData {
	PetscScalar yref,deltarhog,Pisos;
};



/* prototypes for functions */
PetscErrorCode pTatinCreateDirectory(const char dirname[]);
PetscErrorCode pTatinWriteOptionsFile(const char filename[]);

PetscErrorCode BCListIsDirichlet(PetscInt value,PetscBool *flg);
PetscErrorCode BCListInitialize(BCList list);
PetscErrorCode BCListCreate(BCList *list);
PetscErrorCode BCListDestroy(BCList *list);
PetscErrorCode BCListSetSizes(BCList list,PetscInt bs,PetscInt N,PetscInt N_local);
PetscErrorCode BCListUpdateCache(BCList list);
PetscErrorCode BCListInitGlobal(BCList list);
PetscErrorCode BCListGlobalToLocal(BCList list);
PetscErrorCode DMDABCListCreate(DM da,BCList *list);
PetscErrorCode DMDABCListTraverse(BCList list,DM da,DMDABCListConstraintLoc doflocation,PetscInt dof_idx,PetscBool (*eval)(PetscScalar*,PetscScalar*,void*),void *ctx);
PetscErrorCode DMDABCListTraverse1D(BCList list,DM da,DMDABCListConstraintLoc doflocation,PetscInt dof_idx,PetscBool (*eval)(PetscScalar*,PetscScalar*,void*),void *ctx);
PetscBool BCListEvaluator_constant( PetscScalar position[], PetscScalar *value, void *ctx );
PetscBool BCListEvaluator_none( PetscScalar position[], PetscScalar *value, void *ctx );
PetscBool BCListEvaluator_Alabeaumont(PetscScalar pos[],PetscScalar *value,void *ctx);
PetscBool BCListEvaluator_Godfinger(PetscScalar pos[],PetscScalar *value,void *ctx);
PetscBool BCListEvaluator_Poly(PetscScalar pos[],PetscScalar *value,void *ctx);
PetscBool BCListEvaluator_Step(PetscScalar pos[],PetscScalar *value,void *ctx);
PetscBool BCListEvaluator_Geomod3(PetscScalar pos[],PetscScalar *value,void *ctx);
PetscBool BCListEvaluator_Geomod3bis(PetscScalar pos[],PetscScalar *value,void *ctx);
PetscBool BCListEvaluator_Geomod3R(PetscScalar pos[],PetscScalar *value,void *ctx);
PetscBool BCListEvaluator_Geomod3_fric(PetscScalar pos[],PetscScalar *value,void *ctx);
PetscErrorCode BCListResidualDirichlet(BCList list,Vec X,Vec F);

PetscErrorCode BCListGetGlobalValues(BCList list,PetscInt *n,PetscScalar **vals);
PetscErrorCode BCListRestoreGlobalIndices(BCList list,PetscInt *n,PetscInt **idx);
PetscErrorCode BCListGetGlobalIndices(BCList list,PetscInt *n,PetscInt **idx);
PetscErrorCode BCListGetDofIdx(BCList list,PetscInt *Lg,PetscInt **dofidx_global,PetscInt *Ll,PetscInt **dofidx_local);

PetscErrorCode BCListInsert(BCList list,Vec y);
PetscErrorCode BCListInsertLocal(BCList list,Vec y);

void PTatinConstructNI_Q2_2D(const double _xi[],double Ni[]);
void PTatinConstructGNI_Q2_2D(const double _xi[],double GNi[2][Q2_NODES_PER_EL_2D]);
void PTatinConstructGNX_Q2_2D(const double _xi[],double GNx[2][Q2_NODES_PER_EL_2D],double coords[],double *det_J);

void ConstructNi_P0_2D(const double _xi[],double coords[],double Ni[]);
void ConstructNi_P1L_2D(const double _xi[],double coords[],double Ni[]);
void ConstructNi_P1G_2D(const double _xi[],double coords[],double Ni[]);
void ConstructNi_P1GRel_2D(const double _xi[],double coords[],double Ni[]);

void QuadratureStokesCreateSubCells(PetscInt ngp,PetscScalar xi[][2],PetscScalar we[],PetscInt M[],PetscInt *_NGP,PetscScalar XI[][2],PetscScalar WE[]);
void QuadratureCreateGauss_1pnt_2D(PetscInt *ngp,PetscScalar gp_xi[][2],PetscScalar gp_weight[]);
void QuadratureCreateGauss_2pnt_2D(PetscInt *ngp,PetscScalar gp_xi[][2],PetscScalar gp_weight[]);
void QuadratureCreateGauss_3pnt_2D(PetscInt *ngp,PetscScalar gp_xi[][2],PetscScalar gp_weight[]);
void QuadratureCreateGauss_2x2pnt_2D(PetscInt *ngp,PetscScalar gp_xi[][2],PetscScalar gp_weight[]);
void QuadratureCreateGauss_2x3pnt_2D(PetscInt *ngp,PetscScalar gp_xi[][2],PetscScalar gp_weight[]);
PetscErrorCode QuadratureStokesDestroy(QuadratureStokes *qq);
PetscErrorCode QuadratureStokesCreate(QuadratureStokesRule rule,PetscInt ncells,QuadratureStokes *Q);
PetscErrorCode QuadratureStokesBasisSetUp(QuadratureStokes Q,BasisType ubasis,BasisType pbasis);
PetscErrorCode QuadratureStokesCoordinateSetUp(QuadratureStokes Q,DM da);
PetscErrorCode QuadratureStokesGetCell(QuadratureStokes Q,PetscInt cidx,GaussPointCoefficientsStokes **points);
PetscErrorCode QuadratureStokesView(QuadratureStokes Q,const char prefix[]);

PetscErrorCode QuadratureStokesGravityModelCoordinateAlignedSetValues(QuadratureStokes quadrature,PetscReal gx,PetscReal gy);
PetscErrorCode QuadratureStokesLoadGravityModel_CoordinateAligned(QuadratureStokes quadrature);
PetscErrorCode QuadratureStokesLoadGravityModel(QuadratureStokes quadrature);

PetscErrorCode DMDAGetSizeElementQ2(DM da,PetscInt *MX,PetscInt *MY,PetscInt *MZ);
PetscErrorCode DMDAGetLocalSizeElementQ2(DM da,PetscInt *mx,PetscInt *my,PetscInt *mz);
PetscErrorCode DMDAGetCornersElementQ2(DM da,PetscInt *sei,PetscInt *sej,PetscInt *sek,PetscInt *mx,PetscInt *my,PetscInt *mz);
PetscErrorCode DMDAGetOwnershipRangesElementQ2(DM da,PetscInt *m,PetscInt *n,PetscInt *p,PetscInt **si,PetscInt **sj,PetscInt **sk,PetscInt **_mx,PetscInt **_my,PetscInt **_mz);
PetscErrorCode DMDAGetElements_DA_Q2_2D(DM dm,PetscInt *nel,PetscInt *npe,const PetscInt **eidx);
PetscErrorCode DMDAGetElements_DA_P0MD_2D(DM dm,PetscInt *nel,PetscInt *npe,const PetscInt **eidx);
PetscErrorCode DMDAGetElements_DA_Q2(DM dm,PetscInt *nel,PetscInt *nen,const PetscInt *e[]);
PetscErrorCode DMDAGetElements_DA_P1(DM dm,PetscInt *nel,PetscInt *nen,const PetscInt *e[]);
PetscErrorCode DMDAGetElements_pTatin(DM dm,PetscInt *nel,PetscInt *nen,const PetscInt *e[]);
PetscErrorCode StokesVelocity_GetElementLocalIndices(PetscInt el_localIndices[],PetscInt elnid[]);
PetscErrorCode StokesPressure_GetElementLocalIndices(PetscInt el_localIndices[],PetscInt elnid[]);
PetscErrorCode DMDAGetElementCoordinatesQ2_2D(PetscScalar elcoords[],PetscInt elnid[],PetscScalar LA_gcoords[]);
PetscErrorCode DMDAGetScalarElementField_2D(PetscScalar elfield[],PetscInt npe,PetscInt elnid[],PetscScalar LA_gfield[]);
PetscErrorCode DMDAGetScalarElementFieldQ2_2D(PetscScalar elfield[],PetscInt elnid[],PetscScalar LA_gfield[]);
PetscErrorCode DMDAGetVectorElementFieldQ2_2D(PetscScalar elfield[],PetscInt elnid[],PetscScalar LA_gfield[]);
PetscErrorCode GetElementEqnIndicesQ2(PetscInt elnidx[],PetscInt globalindices[],PetscInt eldofidx[]);
PetscErrorCode GetElementEqnIndicesPressure(PetscInt npe,PetscInt elnidx[],PetscInt globalindices[],PetscInt eldofidx[]);
PetscErrorCode DMDASetElementType_Q2(DM da);
PetscErrorCode DMDASetElementType_P1(DM da);

PetscErrorCode DMGetMatrix_DMCompositeStokesQkPm(DM pack,const MatType mtype,Mat *B);
PetscErrorCode DMGetMatrix_DMCompositeStokesPCQkPm(DM pack,const MatType mtype,Mat *B);
PetscErrorCode DMDASetValuesLocalStencil_AddValues_Stokes_Velocity(PetscScalar *fields_F,PetscInt u_eqn[],PetscScalar Fe_u[]);

PetscErrorCode DMCompositeStokes_MatCreate(DM pack,const MatType mtypeA11,Mat *A11,const MatType mtypeA12,Mat *A12,const MatType mtypeA21,Mat *A21,const MatType mtypeS,Mat *A22);
PetscErrorCode DMCompositeStokesCreateNest(DM pack,Mat Auu,Mat Aup,Mat Apu,Mat App,Mat *B);

PetscErrorCode DMDAViewGnuplot2d(DM da,Vec fields,const char comment[],const char prefix[]);
PetscErrorCode DMDAViewPressureGnuplot2d(DM da,DM dap,Vec X,const char prefix[]);
PetscErrorCode pTatin2d_StokesFieldOutput_DefaultGnuplot(pTatinCtx ctx,Vec X,const char prefix[]);
PetscErrorCode pTatin2d_ModelOutput_Default(pTatinCtx ctx,Vec X,const char prefix[]);


PetscErrorCode FormFunctionLocal_U(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[],PetscScalar Ru[]);
PetscErrorCode FormFunctionLocal_P(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[],PetscScalar Rp[]);

PetscErrorCode Q2GetElementLocalIndicesDOF(PetscInt el_localIndices[],PetscInt ndof,PetscInt elnid[]);
PetscErrorCode DMDASetValuesLocalStencil_AddValues_DOF(PetscScalar *fields_F,PetscInt ndof,PetscInt eqn[],PetscScalar Fe[]);

PetscErrorCode FormFunction_Stokes(SNES snes,Vec X,Vec F,void *ctx);
PetscErrorCode FormJacobian_Stokes(SNES snes,Vec X,Mat *A,Mat *B,MatStructure *mstr,void *ctx);

PetscErrorCode pTatin2dCreateContext(pTatinCtx *ctx);
PetscErrorCode pTatin2dParseOptions(pTatinCtx ctx);
PetscErrorCode pTatin2dCreateQ2PmMesh(pTatinCtx ctx);
PetscErrorCode pTatin2dCreateBoundaList(pTatinCtx ctx);
PetscErrorCode pTatin2dCreateQuadratureStokes(pTatinCtx ctx);
PetscErrorCode pTatin2dCreateMaterialPoints(pTatinCtx ctx);
PetscErrorCode pTatin2dCreatePassiveMarkers(pTatinCtx ctx);
PetscErrorCode pTatin2dUpdateMaterialPoints(pTatinCtx ctx,Vec X);
PetscErrorCode pTatin2dUpdatePassiveMarkers(pTatinCtx ctx,Vec X);
PetscErrorCode MaterialPointCoordinateSetUp(DataBucket db,DM da);
PetscErrorCode pTatinSetSNESContMonitor(SNES snes,pTatinCtx ctx);
PetscErrorCode pTatin2d_ComputeTimestep(pTatinCtx ctx,DM pack,Vec X);
PetscErrorCode pTatinSetSNESMonitor(SNES snes,pTatinCtx ctx);
PetscErrorCode pTatin2dSolverStatistics(pTatinCtx ctx,SNES snes,Vec X);
PetscErrorCode pTatin2dDestroyContext(pTatinCtx *ctx);

PetscErrorCode pTatinUnitsCreate(pTatinUnits *units);
PetscErrorCode pTatinUnitsDestroy(pTatinUnits *units);

PetscErrorCode pTatinCtxGetModelData(pTatinCtx ctx,const char name[],void **data);
PetscErrorCode pTatinCtxAttachModelData(pTatinCtx ctx,const char name[],void *data);
PetscErrorCode pTatinPhysCompActivated(pTatinCtx ctx,PhysComponents phys,PetscBool *active);

PetscErrorCode pTatin2d_SetTimestep(pTatinCtx ctx,const char timescale_name[],PetscReal dt_trial);
PetscErrorCode SNESLineSearchRelaxed(SNES snes,void *lsctx,Vec x,Vec f,Vec g,Vec y,Vec w,PetscReal fnorm,PetscReal xnorm,PetscReal *ynorm,PetscReal *gnorm,PetscBool  *flag);
PetscErrorCode SNESLineSearchAdaptiveRelaxed(SNES snes,void *lsctx,Vec x,Vec f,Vec g,Vec y,Vec w,PetscReal fnorm,PetscReal xnorm,PetscReal *ynorm,PetscReal *gnorm,PetscBool  *flag);

PetscErrorCode StokesKSPFSMGuSetUp(DM dm,KSP stokes_ksp,PetscInt nlevels,Mat interpolation[]);

#endif
