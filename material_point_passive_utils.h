
#ifndef __MATERIAL_POINT_PASSIVE_UTILS_H__
#define __MATERIAL_POINT_PASSIVE_UTILS_H__

#include "swarm_fields.h"
#include "data_exchanger.h"
#include "MPntStd_def.h"
#include "MPntPPassive_def.h"


PetscErrorCode SwarmUpdateProperties_MPntPPassive(DataBucket db,pTatinCtx user,Vec X);
PetscErrorCode pTatin_TkUpdateMarker_Passive(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[]);
PetscErrorCode pTatin_OutputMarker_Passive(pTatinCtx user);
#endif