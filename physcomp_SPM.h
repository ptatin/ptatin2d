/*

Design choices:
 + 
 + 
 
 
*/
#ifndef __phys_spm_equation_h__
#define __phys_spm_equation_h__

typedef struct _p_PhysCompSPMCtx      *PhysCompSPMCtx;
typedef struct _p_QuadratureSPM       *QuadratureSPM;
typedef struct _p_CoefficientsSPMEq   CoefficientsSPMEq;

struct _p_PhysCompSPMCtx {
    DM                      daSPM;
	Vec                     height,height_old;
	BCList                  bclist;
	QuadratureSPM           Q;
	PetscInt                mx; /* global mesh size */
	PetscInt                NSedMarkPerCell;
	PetscInt                sed_mark_id;
	PetscScalar             kappa_0; 
	PetscScalar             source_0;
	PetscScalar             start_time;
	PetscScalar             time;
	PetscScalar             heat_diffusivity_ND;
	PetscScalar             heat_productivity_ND;
};

struct _p_CoefficientsSPMEq {
  PetscScalar diffusivity;
  PetscScalar sediment_source;
};

struct _p_QuadratureSPM {
	/* for each element */
	PetscInt    ngp;
	PetscScalar *xi;
	PetscScalar *weight;
	/* basis function evaluations */
	PetscInt             u_basis;
	PetscScalar          *U_Ni; /* U_Ni[p] */
	PetscScalar          **U_GNi;
	PetscInt             ncells;
	CoefficientsSPMEq   *cellproperties;
};	


PetscErrorCode QuadratureSPMGetCell(QuadratureSPM Q,PetscInt cidx,CoefficientsSPMEq **points);
PetscErrorCode PhysComp_SPMEquationCreate1(PhysCompSPMCtx *p);
PetscErrorCode PhysComp_SPMEquationCreate2(DM davq2,PhysCompSPMCtx *p);
PetscErrorCode SPMFormJacobian(PetscReal time,Vec X,PetscReal dt,Mat *A,Mat *B,MatStructure *mstr,void *ctx);
PetscErrorCode SPMFormFunction(PetscReal time,Vec X,PetscReal dt,Vec F,void *ctx);
PetscErrorCode pTatinLoadPhysics_SPMEquation(pTatinCtx ctx);
PetscErrorCode PhysComp_SPMEquationDestroy(PhysCompSPMCtx *p);
PetscErrorCode QuadratureSPMDestroy(QuadratureSPM *Q);
PetscErrorCode PhysComp_SPMSetConstantParams(PhysCompSPMCtx p);
PetscErrorCode PhysCompSPMExtractInitialGeometry(PhysCompSPMCtx p,DM dav);
PetscErrorCode PhysCompSPMUpdateSurfaceGeometry(PhysCompSPMCtx p,DM dav);
PetscErrorCode PhysCompSPMAddSedimentMarkersOnTopSurface(PhysCompSPMCtx p,DM da,DataBucket db, PetscBool active);
#endif

