

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"

#include "petscksp.h"
#include "petscdm.h"

#include "pTatin2d.h"
#include "swarm_fields.h"
#include "data_exchanger.h"
#include "MPntStd_def.h"
#include "AVD2d.h"


typedef struct _p_PSortCtx {
	long int point_index;
	long int cell_index;
} PSortCtx;

int sort_ComparePSortCtx(const void *dataA,const void *dataB)
{
	PSortCtx *pointA;
	PSortCtx *pointB;
	
	pointA = (PSortCtx*)dataA;
	pointB = (PSortCtx*)dataB;
	
	if (pointA->cell_index < pointB->cell_index) {
		return -1;
	} else if (pointA->cell_index > pointB->cell_index) {
		return 1;
	} else {
		return 0;
	}
}
void sort_PSortCx(const PetscInt np, PSortCtx list[])
{
	PetscLogDouble t0,t1;

	PetscGetTime(&t0);
	qsort( list, np, sizeof(PSortCtx), sort_ComparePSortCtx );
	PetscGetTime(&t1);
	//[LOG]PetscPrintf(PETSC_COMM_WORLD,"  sort_PSortCx -> qsort %1.4e [npoints = %ld] (sec)\n", t1-t0,np );

	/*
	 nparticles = 25000000
	 ncells     = 1000000

	 times = 
	*/
}

/*

 Note, due to the non-affine nature of the Q2 element transformation, 
 it is possible that the min x,y coords within the element will not 
 occur at the nodal points. Thus the bounding box is always defined using
 the markers - and not the nodes.
 
*/
#undef __FUNCT__  
#define __FUNCT__ "do_avd_patch"
PetscErrorCode do_avd_patch(
									PetscInt ncells, PetscInt pcell_list[],
									PetscInt np, PSortCtx plist[],
									PetscInt np_lower, PetscInt np_upper,
									PetscInt patch_extend,PetscInt nxp,PetscInt nyp,PetscScalar perturb,DM da,DataBucket db)
{
	Aint avd_mx,avd_my;
	AVD2d A;
	AVDPoint2d points;
	PetscInt np_per_cell_max,mx,my;
	AVDPoint2d avd_point_list;
	PetscInt c,i,j,cell_index_i,cell_index_j,point_count,points_per_patch;
	PetscInt p,n;
	const PetscInt  *elnidx;
	PetscInt        nel,nen;
	Vec             gcoords;
	PetscScalar     *LA_coords;
	PetscScalar     el_coords[Q2_NODES_PER_EL_2D*NSD];
	DM cda;
	double xmin,ymin,xmax,ymax;
	DataField    PField,PField_stokes;
	int Lnew,npoints_current,npoints_init;
	int cells_needing_new_points,cells_needing_new_points_g;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	

	ierr = DMDAGetElements_pTatin(da,&nel,&nen,&elnidx);CHKERRQ(ierr);
	
	avd_mx = 10*(2*patch_extend + 1);
	avd_my = 10*(2*patch_extend + 1);
	AVD2dCreate(avd_mx,avd_my,10,&A);
	
	/* get mx,my from the da */
	ierr = DMDAGetLocalSizeElementQ2(da,&mx,&my,PETSC_NULL);CHKERRQ(ierr);
	
	/* find max np_per_cell I will need */
	np_per_cell_max = 0;
	cells_needing_new_points = 0;
	DataBucketGetSizes(db,&Lnew,PETSC_NULL,PETSC_NULL);
	for (c=0; c<nel; c++) {
		PetscInt points_per_cell;
		PetscInt points_per_patch;
		
		points_per_cell = pcell_list[c+1] - pcell_list[c];
		//printf("cell[%d] ppc = %d \n",c,points_per_cell );
		
		if (points_per_cell > np_lower) { continue; }

		cell_index_j = c / mx;
		cell_index_i = c - cell_index_j * mx;
		
		points_per_patch = 0;
		for ( j=cell_index_j - patch_extend; j<=cell_index_j + patch_extend; j++ ) {
			for ( i=cell_index_i - patch_extend; i<=cell_index_i + patch_extend; i++ ) {
				PetscInt patch_cell_id;
				
				if (i>=mx) { continue; }
				if (j>=my) { continue; }
				if (i<0) { continue; }
				if (j<0) { continue; }

				patch_cell_id = i + j * mx;
				
				points_per_patch = points_per_patch + (pcell_list[patch_cell_id+1] - pcell_list[patch_cell_id]);
			}
		}
		
		if (points_per_patch > np_per_cell_max) {
			np_per_cell_max = points_per_patch;
		}

		Lnew = Lnew + nxp * nyp;
		cells_needing_new_points++;
	}
	
	//printf("  np_per_cell_max = %d \n", np_per_cell_max );
	MPI_Allreduce( &cells_needing_new_points, &cells_needing_new_points_g, 1, MPI_INT, MPI_SUM, PETSC_COMM_WORLD );
	if (cells_needing_new_points_g==0) {
//		PetscPrintf(PETSC_COMM_WORLD,"!! No population control required <global>!!\n");
		PetscFunctionReturn(0);
	}
	PetscPrintf(PETSC_COMM_WORLD,"!! Population control required <global>!!\n");
	
	DataBucketSetSizes(db,Lnew,-1);
	
	//printf("  mx,my = %d,%d \n", mx,my );
	
  /* setup for coords */
  ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
  ierr = DMDAGetGhostedCoordinates(da,&gcoords);CHKERRQ(ierr);
  ierr = VecGetArray(gcoords,&LA_coords);CHKERRQ(ierr);

	AVDPoint2dCreate(np_per_cell_max,&avd_point_list);
	
	DataBucketGetDataFieldByName(db, MPntStd_classname ,&PField);
	//DataFieldGetAccess(PField);

	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	//DataFieldGetAccess(PField_stokes);

	npoints_init    = np;
	npoints_current = np;
	for (c=0; c<nel; c++) {
		PetscInt points_per_cell;
		
		points_per_cell = pcell_list[c+1] - pcell_list[c];
		
		if (points_per_cell > np_lower) { continue; }

		
		cell_index_j = c / mx;
		cell_index_i = c - cell_index_j * mx;

		/* load points */
		memset( avd_point_list, 0, sizeof(struct _p_AVDPoint2d)*(np_per_cell_max) );
		
		point_count = 0;
		xmin = PETSC_MAX_REAL;
		xmax = PETSC_MIN_REAL;
		ymin = PETSC_MAX_REAL;
		ymax = PETSC_MIN_REAL;

		DataFieldGetAccess(PField);
		for ( j=cell_index_j - patch_extend; j<=cell_index_j + patch_extend; j++ ) {
			for ( i=cell_index_i - patch_extend; i<=cell_index_i + patch_extend; i++ ) {
				PetscInt patch_cell_id;
				
				if (i>=mx) { continue; }
				if (j>=my) { continue; }
				if (i<0) { continue; }
				if (j<0) { continue; }
				
				patch_cell_id = i + j * mx;
				points_per_patch = (pcell_list[patch_cell_id+1] - pcell_list[patch_cell_id]);
				//printf("patch(%d)-(%d,%d) cell(%d)-(%d,%d)  : ppcell = %d \n", c, cell_index_i,cell_index_j, patch_cell_id,i,j,points_per_patch);
				
				for (p=0; p<points_per_patch; p++) {
					MPntStd *marker_p;
					PetscInt pid, pid_unsorted;
					
					pid = pcell_list[patch_cell_id] + p;
					pid_unsorted = plist[pid].point_index;
					
					DataFieldAccessPoint(PField, pid_unsorted ,(void**)&marker_p);
					
					avd_point_list[point_count].x     = marker_p->coor[0];
					avd_point_list[point_count].y     = marker_p->coor[1];
					avd_point_list[point_count].phase = pid_unsorted;
					//printf("    p=%d(wil=%d): (x,y) %lf %lf \n", p, marker_p->wil, marker_p->coor[0], marker_p->coor[1] );
/*
					{
						int kk;
						double interpX[] = {0.0,0.0};
						double Ni[Q2_NODES_PER_EL_2D];
						
						ierr = DMDAGetElementCoordinatesQ2_2D(el_coords,(PetscInt*)&elnidx[nen*marker_p->wil],LA_coords);CHKERRQ(ierr);
						PTatinConstructNI_Q2_2D(marker_p->xi,Ni);
						for (kk=0; kk<Q2_NODES_PER_EL_2D; kk++) {
							interpX[0] += Ni[kk] * el_coords[NSD*kk+0];
							interpX[1] += Ni[kk] * el_coords[NSD*kk+1];
						}
						printf("        interp coords = %lf %lf \n", interpX[0], interpX[1] );
					}
*/					
					xmin = PetscMin(xmin, marker_p->coor[0]);
					xmax = PetscMax(xmax, marker_p->coor[0]);

					ymin = PetscMin(ymin, marker_p->coor[1]);
					ymax = PetscMax(ymax, marker_p->coor[1]);
					
					//printf("patch(%d)/cell(%d) -> p:p->wil,x,y = %d %lf %lf \n", c, patch_cell_id, marker_p->wil, marker_p->coor[0],marker_p->coor[1] );
					
					point_count++;
				}
				
			}
		}
		DataFieldRestoreAccess(PField);
		//printf("  cell = %d: total points per patch = %d \n", c,point_count);
		//printf("  BBox(markers): [%lf %lf] x [%lf %lf] \n", xmin,xmax, ymin,ymax );


		/* build patch coordinates */
//#if 0
		//xmin = PETSC_MAX_REAL;
		//xmax = PETSC_MIN_REAL;
		//
		//ymin = PETSC_MAX_REAL;
		//ymax = PETSC_MIN_REAL;
		for ( j=cell_index_j - patch_extend; j<=cell_index_j + patch_extend; j++ ) {
			for ( i=cell_index_i - patch_extend; i<=cell_index_i + patch_extend; i++ ) {
				PetscInt patch_cell_id;
				
				if (i>=mx) { continue; }
				if (j>=my) { continue; }
				if (i<0) { continue; }
				if (j<0) { continue; }
				patch_cell_id = i + j * mx;

				ierr = DMDAGetElementCoordinatesQ2_2D(el_coords,(PetscInt*)&elnidx[nen*patch_cell_id],LA_coords);CHKERRQ(ierr);
				//printf("  patch(%d),element(%d) \n", c,patch_cell_id );
				
				for (n=0; n<Q2_NODES_PER_EL_2D; n++) {
					//printf("    n=%d: %lf %lf \n", n,el_coords[2*n+0],el_coords[2*n+1] );
					xmin = PetscMin(xmin, el_coords[2*n+0]);
					ymin = PetscMin(ymin, el_coords[2*n+1]);
					
					xmax = PetscMax(xmax, el_coords[2*n+0]);
					ymax = PetscMax(ymax, el_coords[2*n+1]);
				}
			}
		}
		//printf("  BBox(nodes): [%lf %lf] x [%lf %lf] \n", xmin,xmax, ymin,ymax );
//#endif
		/* just a little buffer to make sure we don't have th BB domain exactly on the particle */
		xmin = xmin - 1.0e-8;
		xmax = xmax + 1.0e-8;
		ymin = ymin - 1.0e-8;
		ymax = ymax + 1.0e-8;
		
		
		/* build avd */
		AVD2dSetDomainSize(A,xmin,xmax,ymin,ymax);

		{
			Aint i,count,claimed,iterations;
			double t0,t1,tubc,tcc;

			/* set points */
			AVD2dSetPoints(A,point_count,avd_point_list);
			
			/* reset cells */
			AVDCell2dReset(A);
			
			AVDGetTime(&t0);
			AVD2dInit(A,point_count,avd_point_list);
			AVDGetTime(&t1);
			//printf("Total time (AVD2dInit) %1.6e \n", t1-t0);
			
			//printf("generating approximate voronoi diagram... [%d points, %dx%d avd cells]\n",point_count,A->mx,A->my);
			tubc = tcc = 0.0;
			claimed = 1;
			iterations = 0;
			while (claimed != 0){
				claimed = 0 ;
				for (p=0; p<point_count; p++){
					AVDGetTime(&t0);
					AVD2dClaimCells(A,p);
					claimed += A->chains[p].num_claimed;
					AVDGetTime(&t1);
					tcc = tcc + (t1-t0);
					
					AVDGetTime(&t0);
					AVD2dUpdateChain(A,p);
					AVDGetTime(&t1);
					tubc = tubc + (t1-t0);
				}
				//printf("\tAVD it. %.4d - (ClaimCells) %1.4e , (UpdateChain) %1.4e \n", iterations, tcc, tubc );
				iterations++;
			}
			/*
			printf("Total time (AVD2dClaimCells) %1.6e \n", tcc);
			printf("Total time (AVD2dUpdateChain) %1.6e \n", tubc);
			printf("Total time (AVD2d) %1.6e \n", tcc+tubc);
			printf("Total AVD its. %d \n", iterations);	
			*/
		}		
		

		{
			PetscInt Nxp[2],pi,pj,k;
			PetscScalar dxi,deta;
			PetscInt avd_i,avd_j,avd_cell;
			PetscInt avd_point_index,marker_index;
			
			Nxp[0] = nxp;
			Nxp[1] = nyp;
			
			dxi   = 2.0/(PetscReal)Nxp[0];
			deta  = 2.0/(PetscReal)Nxp[1];

			ierr = DMDAGetElementCoordinatesQ2_2D(el_coords,(PetscInt*)&elnidx[nen*c],LA_coords);CHKERRQ(ierr);
			for (pj=0; pj<Nxp[1]; pj++) {
				for (pi=0; pi<Nxp[0]; pi++) {
					double xip[NSD],xip_shift[NSD],xip_rand[NSD],xp_rand[NSD],Ni[Q2_NODES_PER_EL_2D];
					MPntStd *marker_p,*marker_nearest;
					MPntPStokes *mpprop_stokes_p, *mpprop_stokes_nearest;
					
					xip[0] = -1.0 + dxi   * (pi + 0.5);
					xip[1] = -1.0 + deta  * (pj + 0.5);
					
					/* random between -0.5 <= shift <= 0.5 */
					xip_shift[0] = 1.0*(rand()/(RAND_MAX+1.0)) - 0.5;
					xip_shift[1] = 1.0*(rand()/(RAND_MAX+1.0)) - 0.5;
					
					xip_rand[0] = xip[0] + perturb * dxi  * xip_shift[0];
					xip_rand[1] = xip[1] + perturb * deta * xip_shift[1];
					
					if (fabs(xip_rand[0]) > 1.0) {
						SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"fabs(x-point coord) greater than 1.0");
					}
					if (fabs(xip_rand[1]) > 1.0) {
						SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"fabs(y-point coord) greater than 1.0");
					}
					
					PTatinConstructNI_Q2_2D(xip_rand,Ni);
					
					xp_rand[0] = xp_rand[1] = 0.0;
					for (k=0; k<Q2_NODES_PER_EL_2D; k++) {
						xp_rand[0] += Ni[k] * el_coords[NSD*k+0];
						xp_rand[1] += Ni[k] * el_coords[NSD*k+1];
					}
					
					
					
					/* locate point in AVD */
					avd_i = (xp_rand[0] - (A->x0 - A->dx))/A->dx;
					avd_j = (xp_rand[1] - (A->y0 - A->dy))/A->dy;
					if (avd_i == A->mx_mesh-1) { avd_i--; }
					if (avd_j == A->my_mesh-1) { avd_j--; }
					if (avd_i == 0) { avd_i++; }
					if (avd_j == 0) { avd_j++; }
					
					avd_cell = avd_i + avd_j * A->mx_mesh;
				
					avd_point_index = A->cells[ avd_cell ].p;
					marker_index    = A->points[avd_point_index].phase;
					
/*
					{
						int LL;
						DataBucketGetSizes(db,&LL,PETSC_NULL,PETSC_NULL);
						printf("LL = %d: pc =  %d marker_index = %d \n", LL, npoints_current,marker_index );
					}
*/
/*					
					DataFieldGetAccess(PField_stokes);
					
					DataFieldAccessPoint(PField_stokes,npoints_current,(void**)&mpprop_stokes_p);
					DataFieldAccessPoint(PField_stokes,marker_index,(void**)&mpprop_stokes_nearest);
					
					mpprop_stokes_p->eta   = mpprop_stokes_nearest->eta;
					mpprop_stokes_p->rho   = mpprop_stokes_nearest->rho;
					
					DataFieldRestoreAccess(PField_stokes);
*/
					DataBucketCopyPoint(db,marker_index, db,npoints_current);
					
					DataFieldGetAccess(PField);

					DataFieldAccessPoint(PField,npoints_current,(void**)&marker_p);
					DataFieldAccessPoint(PField,marker_index,(void**)&marker_nearest);
					
					//marker_p->pid     = 0;
					marker_p->phase   = marker_nearest->phase;
					//marker_p->phase   = 100;

					marker_p->coor[0] = xp_rand[0];
					marker_p->coor[1] = xp_rand[1];
					marker_p->wil     = c;
					marker_p->xi[0]   = xip_rand[0];
					marker_p->xi[1]   = xip_rand[1];
					
					DataFieldRestoreAccess(PField);


					npoints_current++;
				}
			}
		}
		
		
			
	}
  ierr = VecRestoreArray(gcoords,&LA_coords);CHKERRQ(ierr);

	AVDPoint2dDestroy(&A->points);
	AVD2dDestroy(&A);

	
	DataBucketGetSizes(db,&Lnew,PETSC_NULL,PETSC_NULL);

	ierr = SwarmMPntStd_AssignUniquePointIdentifiers(((PetscObject)da)->comm,db,npoints_init,Lnew);CHKERRQ(ierr);
	
	printf("  npoints_init = %d \n", npoints_init);
	printf("  npoints_current-1 = %d \n", npoints_current);
	printf("  npoints_current-2 = %d \n", Lnew);
	
	PetscFunctionReturn(0);
}

/*
 
 1) create list of markers, sorted by element
 2) for any cell c, which has less than np_lower points
 3)  get cell patch bounds using nodal coordinates
 4)  assemble avd using marker global coordinates
 5)  inject nxp x nyp into cell

*/
#undef __FUNCT__  
#define __FUNCT__ "MPPC_AVDPatch"
PetscErrorCode MPPC_AVDPatch(PetscInt np_lower,PetscInt np_upper,PetscInt patch_extend,PetscInt nxp,PetscInt nyp,PetscScalar pertub,DM da,DataBucket db)
{
	PetscInt *pcell_list;
	PSortCtx *plist;
	PetscInt p,npoints;
	PetscErrorCode ierr;
	PetscInt tmp,c,count,cells_np_lower,cells_np_upper;
	const PetscInt  *elnidx;
	PetscInt        nel,nen;
	DataField    PField;
	PetscLogDouble t0,t1;
	
	PetscFunctionBegin;
	
	//[LOG]PetscPrintf(PETSC_COMM_WORLD,"%s: \n", __FUNCTION__);
	ierr = DMDAGetElements_pTatin(da,&nel,&nen,&elnidx);CHKERRQ(ierr);

	ierr = PetscMalloc( sizeof(PetscInt)*(nel+1),&pcell_list );CHKERRQ(ierr);
	
	DataBucketGetSizes(db,&npoints,PETSC_NULL,PETSC_NULL);
	ierr = PetscMalloc( sizeof(PSortCtx)*(npoints), &plist);CHKERRQ(ierr);
	
	DataBucketGetDataFieldByName(db, MPntStd_classname ,&PField);
	DataFieldGetAccess(PField);
	DataFieldVerifyAccess( PField,sizeof(MPntStd));
	for (p=0; p<npoints; p++) {
		MPntStd *marker_p;
		
		DataFieldAccessPoint(PField,p,(void**)&marker_p);
		plist[p].point_index = p;
		plist[p].cell_index  = marker_p->wil;
	}
	DataFieldRestoreAccess(PField);

	sort_PSortCx(npoints,plist);
	
	/* sum points per cell */
	ierr = PetscMemzero( pcell_list,sizeof(PetscInt)*(nel+1) );CHKERRQ(ierr);
	for (p=0; p<npoints; p++) {
		pcell_list[ plist[p].cell_index ]++;
	}
	/*
	for (c=0; c<nel; c++) {
		printf("pcell_list = %d \n", pcell_list[c] );
	}
	*/
	
	/* create offset list */
	count = 0;
	for (c=0; c<nel; c++) {
		tmp = pcell_list[c];
		pcell_list[c] = count;
		count = count + tmp;
	}
	pcell_list[c] = count;
	
	cells_np_lower = 0;
	cells_np_upper = 0;
	for (c=0; c<nel; c++) {
		int points_per_cell = pcell_list[c+1] - pcell_list[c];
		if (points_per_cell <= np_lower) { cells_np_lower++; }
		if (points_per_cell > np_upper) { cells_np_upper++; }
	}		
	//[LOG]PetscPrintf(PETSC_COMM_WORLD,"  cells with points < np_lower (%d) \n", cells_np_lower );
	//[LOG]PetscPrintf(PETSC_COMM_WORLD,"  cells with points > np_upper (%d) \n", cells_np_upper );
	
	PetscGetTime(&t0);
	ierr = do_avd_patch(
										nel, pcell_list,
										npoints, plist,
										np_lower, np_upper,
										patch_extend, nxp,nyp, pertub, da,db);CHKERRQ(ierr);
	PetscGetTime(&t1);
	//[LOG]PetscPrintf(PETSC_COMM_WORLD,"  time(do_avd_patch): %1.4e \n", t1-t0);
	
	
	ierr = PetscFree(plist);CHKERRQ(ierr);
	ierr = PetscFree(pcell_list);CHKERRQ(ierr);
	
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "MaterialPointPopulationControl"
PetscErrorCode MaterialPointPopulationControl(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	PetscInt np_lower,np_upper,patch_extent,nxp,nyp;
	PetscScalar perturb;
	PetscBool flg;
	PetscFunctionBegin;
	
	np_lower = 0;
	np_upper = 100;
	ierr = PetscOptionsGetInt(PETSC_NULL,"-mp_popctrl_np_lower",&np_lower,&flg);CHKERRQ(ierr);
	ierr = PetscOptionsGetInt(PETSC_NULL,"-mp_popctrl_np_upper",&np_upper,&flg);CHKERRQ(ierr);
	
	nxp = 2;
	nyp = 2;
	ierr = PetscOptionsGetInt(PETSC_NULL,"-mp_popctrl_nxp",&nxp,&flg);CHKERRQ(ierr);
	ierr = PetscOptionsGetInt(PETSC_NULL,"-mp_popctrl_nyp",&nyp,&flg);CHKERRQ(ierr);
	
	perturb = 0.1;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-mp_popctrl_perturb",&perturb,&flg);CHKERRQ(ierr);
	
	patch_extent = 1;
	ierr = PetscOptionsGetInt(PETSC_NULL,"-mp_popctrl_patch_extent",&patch_extent,&flg);CHKERRQ(ierr);
	
	ierr = MPPC_AVDPatch(np_lower,np_upper,patch_extent,nxp,nyp,perturb,ctx->dav,ctx->db);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}
