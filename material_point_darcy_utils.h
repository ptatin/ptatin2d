#ifndef __MATERIAL_POINT_DARCY_UTILS_H__
#define __MATERIAL_POINT_DARCY_UTILS_H__

#include "swarm_fields.h"
#include "data_exchanger.h"
#include "MPntStd_def.h"
#include "MPntPDarcy_def.h"
#include "phys_darcy_equation.h"

PetscErrorCode SwarmOutputParaView_MPntPDarcy(DataBucket db,const char path[],const char prefix[]);
PetscErrorCode SwarmUpdateGaussPropertiesLocalL2Projection_P0_MPntPDarcy(const int npoints,MPntStd mp_std[],MPntPDarcy mp_darcy[],DM da,QuadratureVolumeDarcy Q);
PetscErrorCode SwarmUpdateQuadraturePropertiesLocalL2Projection_Q1_MPntPDarcy(const int npoints,MPntStd mp_std[],MPntPDarcy mp_thermal[],DM da,QuadratureVolumeDarcy Q);

PetscErrorCode SwarmUpdateProperties_MPntPDarcy(DataBucket db,pTatinCtx user,Vec X);



#endif
