
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"

#include "petscksp.h"
#include "petscdm.h"

#include "pTatin2d.h"
#include "swarm_fields.h"
#include "data_exchanger.h"
#include "MPntStd_def.h"
#include "MPntPThermal_def.h"
#include "phys_energy_equation.h"
#include "element_type_Q1.h"
#include "dmdae.h"


#undef __FUNCT__
#define __FUNCT__ "SwarmView_MPntPThermal_VTKappended_binary"
PetscErrorCode SwarmView_MPntPThermal_VTKappended_binary(DataBucket db,const char name[])
{
	PetscMPIInt rank;
	FILE *vtk_fp;
	PetscInt k;
	int npoints;
	PetscLogDouble t0,t1;
	DataField PField_std;
	DataField PField_thermal;
	int byte_offset,length;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	ierr = PetscGetTime(&t0);CHKERRQ(ierr);
	
	if ((vtk_fp = fopen ( name, "w")) == NULL)  {
		SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
	}
	
	fprintf( vtk_fp, "<?xml version=\"1.0\"?>\n");
	
#ifdef WORDSIZE_BIGENDIAN
	fprintf( vtk_fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
#else
	fprintf( vtk_fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
#endif
	fprintf( vtk_fp, "\t<UnstructuredGrid>\n" );
	
	DataBucketGetSizes(db,&npoints,PETSC_NULL,PETSC_NULL);
	fprintf( vtk_fp, "\t\t<Piece NumberOfPoints=\"%d\" NumberOfCells=\"%d\">\n",npoints,npoints );
	
	
	fprintf( vtk_fp, "\n");
	fprintf( vtk_fp, "\t\t\t<Cells>\n");
	
	byte_offset = 0;
	
	// connectivity //
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Int32\" Name=\"connectivity\" format=\"appended\" offset=\"%d\" />\n",byte_offset);
  byte_offset = byte_offset + sizeof(int) + npoints * sizeof(int);
	
	// offsets //
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Int32\" Name=\"offsets\" format=\"appended\" offset=\"%d\" />\n",byte_offset);
  byte_offset = byte_offset + sizeof(int) + npoints * sizeof(int);
	
	// types //
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"UInt8\" Name=\"types\" format=\"appended\" offset=\"%d\" />\n",byte_offset);
  byte_offset = byte_offset + sizeof(int) + npoints * sizeof(unsigned char);
	
	fprintf( vtk_fp, "\t\t\t</Cells>\n");
	
	fprintf( vtk_fp, "\n");
	fprintf( vtk_fp, "\t\t\t<CellData>\n");
	fprintf( vtk_fp, "\t\t\t</CellData>\n");
	fprintf( vtk_fp, "\n");
	
	
	DataBucketGetDataFieldByName(db, MPntStd_classname      ,&PField_std);
	DataBucketGetDataFieldByName(db, MPntPThermal_classname ,&PField_thermal);
	
	
	/* point coordinates */
	fprintf( vtk_fp, "\t\t\t<Points>\n");
	
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\" format=\"appended\" offset=\"%d\" />\n",byte_offset);
  byte_offset = byte_offset + sizeof(int) + npoints * 3 * sizeof(double);
	
	fprintf( vtk_fp, "\t\t\t</Points>\n");
	fprintf( vtk_fp, "\n");
	
	/* point data BEGIN */
	fprintf( vtk_fp, "\t\t\t<PointData>\n");
	
	/* auto generated shit goes here */
	{
		MPntStd      *marker_std     = PField_std->data; /* should write a function to do this */
		MPntPThermal *marker_thermal = PField_thermal->data; /* should write a function to do this */
		
		MPntStdVTKWriteBinaryAppendedHeaderAllFields(vtk_fp,&byte_offset,(const int)npoints,(const MPntStd*)marker_std );
		MPntPThermalVTKWriteBinaryAppendedHeaderAllFields(vtk_fp,&byte_offset,(const int)npoints,(const MPntPThermal*)marker_thermal);
	}
	fprintf( vtk_fp, "\t\t\t</PointData>\n");
	fprintf( vtk_fp, "\n");
	/* point data END */
	
	
	fprintf( vtk_fp, "\t\t</Piece>\n");
	fprintf( vtk_fp, "\t</UnstructuredGrid>\n");
	
	/* WRITE APPENDED DATA HERE */
	fprintf( vtk_fp,"\t<AppendedData encoding=\"raw\">\n");
	fprintf( vtk_fp,"_");
	
	/* connectivity, offsets, types, coords */
	////////////////////////////////////////////////////////
	/* write connectivity */
	length = sizeof(int)*npoints;
	fwrite( &length,sizeof(int),1,vtk_fp);
	for (k=0; k<npoints; k++) {
		int idx = k;
		fwrite( &idx, sizeof(int),1, vtk_fp );
	}
	////////////////////////////////////////////////////////
	/* write offset */
	length = sizeof(int)*npoints;
	fwrite( &length,sizeof(int),1,vtk_fp);
	for (k=0; k<npoints; k++) {
		int idx = k+1;
		fwrite( &idx, sizeof(int),1, vtk_fp );
	}
	////////////////////////////////////////////////////////
	/* write types */
	length = sizeof(unsigned char)*npoints;
	fwrite( &length,sizeof(int),1,vtk_fp);
	for (k=0; k<npoints; k++) {
		unsigned char idx = 1; /* VTK_VERTEX */
		fwrite( &idx, sizeof(unsigned char),1, vtk_fp );
	}
	////////////////////////////////////////////////////////
	/* write coordinates */
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	length = sizeof(double)*npoints*3;
	fwrite( &length,sizeof(int),1,vtk_fp);
	for (k=0; k<npoints; k++) {
		MPntStd *marker;
		double  *coor;
		double  coords_k[] = {0.0, 0.0, 0.0};
		
		DataFieldAccessPoint(PField_std,k,(void**)&marker);
		MPntStdGetField_global_coord(marker,&coor);
		coords_k[0] = coor[0];
		coords_k[1] = coor[1];
		
		fwrite( coords_k, sizeof(double), 3, vtk_fp );
	}
	DataFieldRestoreAccess(PField_std);
	
	/* auto generated shit for the marker data goes here */
	{
		MPntStd      *markers_std     = PField_std->data;
		MPntPThermal *markers_thermal = PField_thermal->data;
		
		MPntStdVTKWriteBinaryAppendedDataAllFields(vtk_fp,npoints,markers_std);
		MPntPThermalVTKWriteBinaryAppendedDataAllFields(vtk_fp,npoints,markers_thermal);
	}
	
	fprintf( vtk_fp,"\n\t</AppendedData>\n");
	
	
	fprintf( vtk_fp, "</VTKFile>\n");
	
	
	if( vtk_fp!= NULL ) {
		fclose( vtk_fp );
		vtk_fp = NULL;
	}
	
	ierr = PetscGetTime(&t1);CHKERRQ(ierr);
#ifdef PROFILE_TIMING
	PetscPrintf(PETSC_COMM_WORLD,"VTKWriter(%s): Time %1.4e sec\n",__FUNCT__,t1-t0);
#endif
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "__SwarmView_MPntPThermal_PVTU"
PetscErrorCode __SwarmView_MPntPThermal_PVTU(const char prefix[],const char name[])
{
	PetscMPIInt nproc,rank;
	FILE *vtk_fp;
	PetscInt i;
	char *sourcename;
	
	PetscFunctionBegin;
	
	if ((vtk_fp = fopen ( name, "w")) == NULL)  {
		SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
	}
	
	/* (VTK) generate pvts header */
	fprintf( vtk_fp, "<?xml version=\"1.0\"?>\n");
	
#ifdef WORDSIZE_BIGENDIAN
	fprintf( vtk_fp, "<VTKFile type=\"PUnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
#else
	fprintf( vtk_fp, "<VTKFile type=\"PUnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
#endif
	
	/* define size of the nodal mesh based on the cell DM */
	fprintf( vtk_fp, "  <PUnstructuredGrid GhostLevel=\"0\">\n" ); /* note overlap = 0 */
	
	
	/* DUMP THE CELL REFERENCES */
	fprintf( vtk_fp, "    <PCellData>\n");
	fprintf( vtk_fp, "    </PCellData>\n");
	
	
	///////////////
	fprintf( vtk_fp, "    <PPoints>\n");
	fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\"/>\n");
	fprintf( vtk_fp, "    </PPoints>\n");
	///////////////
	
	///////////////
  fprintf(vtk_fp, "    <PPointData>\n");
	MPntStdPVTUWriteAllPPointDataFields(vtk_fp);
	MPntPThermalPVTUWriteAllPPointDataFields(vtk_fp);
  fprintf(vtk_fp, "    </PPointData>\n");
	///////////////
	
	
	/* write out the parallel information */
	MPI_Comm_size(PETSC_COMM_WORLD,&nproc);
	for (i=0; i<nproc; i++) {
		asprintf( &sourcename, "%s-subdomain%1.5d.vtu", prefix, i );
		fprintf( vtk_fp, "    <Piece Source=\"%s\"/>\n",sourcename);
		free(sourcename);
	}
	
	/* close the file */
	fprintf( vtk_fp, "  </PUnstructuredGrid>\n");
	fprintf( vtk_fp, "</VTKFile>\n");
	
	if(vtk_fp!=NULL){
		fclose( vtk_fp );
		vtk_fp = NULL;
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SwarmOutputParaView_MPntPThermal"
PetscErrorCode SwarmOutputParaView_MPntPThermal(DataBucket db,const char path[],const char prefix[])
{ 
	char           *vtkfilename,*filename;
	PetscMPIInt    rank;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ierr = pTatinGenerateParallelVTKName(prefix,"vtu",&vtkfilename);CHKERRQ(ierr);
	if (path) {
		asprintf(&filename,"%s/%s",path,vtkfilename);
	} else {
		asprintf(&filename,"./%s",vtkfilename);
	}
	
	//#ifdef __VTK_ASCII__
	//	ierr = SwarmView_MPntPThermal_VTKascii( db,filename );CHKERRQ(ierr);
	//#endif
	//#ifndef __VTK_ASCII__
	ierr = SwarmView_MPntPThermal_VTKappended_binary(db,filename);CHKERRQ(ierr);
	//#endif
	free(filename);
	free(vtkfilename);
	
	
	ierr = pTatinGenerateVTKName(prefix,"pvtu",&vtkfilename);CHKERRQ(ierr);
	if (path) {
		asprintf(&filename,"%s/%s",path,vtkfilename);
	} else {
		asprintf(&filename,"./%s",vtkfilename);
	}
	
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	if (rank==0) {
		ierr = __SwarmView_MPntPThermal_PVTU( prefix, filename );CHKERRQ(ierr);
	}
	free(filename);
	free(vtkfilename);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SwarmUpdateGaussPropertiesLocalL2Projection_P0_MPntPThermal"
PetscErrorCode SwarmUpdateGaussPropertiesLocalL2Projection_P0_MPntPThermal(const int npoints,MPntStd mp_std[],MPntPThermal mp_thermal[],DM da,QuadratureVolumeEnergy Q)
{
	PetscInt p,i,ngp;
	PetscInt nel,nen,e;
	const PetscInt *elnidx;
	int *el_pcount;
	double *el_avg_kappa, *el_avg_H;
	CoefficientsEnergyEq *quadraturepoints;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ierr = DMDAGetElementsQ1(da,&nel,&nen,&elnidx);CHKERRQ(ierr);
	
	ierr = PetscMalloc( sizeof(int)*nel, &el_pcount );CHKERRQ(ierr);
	ierr = PetscMalloc( sizeof(double)*nel, &el_avg_kappa );CHKERRQ(ierr);
	ierr = PetscMalloc( sizeof(double)*nel, &el_avg_H );CHKERRQ(ierr);
	
	/* init */
	for (e=0; e<nel; e++) {
		el_pcount[e]    = 0;
		el_avg_kappa[e] = 0.0;
		el_avg_H[e]     = 0.0;
	}
	
	for (p=0; p<npoints; p++) {
		double kappa_p  = mp_thermal[p].kappa;
		double H_p      = mp_thermal[p].H;
		
		e = mp_std[p].wil;
		if (e >= nel) {
			SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"marker->wil >= num local elements");
		}
		
		el_avg_kappa[e] = el_avg_kappa[e] + kappa_p;
		el_avg_H[e]     = el_avg_H[e]     + H_p;
		
		el_pcount[e]++;
	}
	for (e=0; e<nel; e++) {
		if (el_pcount[e]==0) {
			SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Cell doesn't contain any material points");
		}
		
		el_avg_kappa[e] = el_avg_kappa[e] / (double)(el_pcount[e]);
		el_avg_H[e]     = el_avg_H[e]     / (double)(el_pcount[e]);
	}
	
	
	/* traverse elements and interpolate */
	ngp = Q->ngp;
	for (e=0;e<nel;e++) {
		ierr = QuadratureVolumeEnergyGetCell(Q,e,&quadraturepoints);CHKERRQ(ierr);
		
		for (p=0; p<ngp; p++) {
			quadraturepoints[p].diffusivity = el_avg_kappa[e];
			quadraturepoints[p].heat_source = el_avg_H[e];
		}
	}
	
	ierr = PetscFree(el_pcount);CHKERRQ(ierr);
	ierr = PetscFree(el_avg_kappa);CHKERRQ(ierr);
	ierr = PetscFree(el_avg_H);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_SwarmUpdateGaussPropertiesLocalL2ProjectionQ1_MPntPThermal"
PetscErrorCode _SwarmUpdateGaussPropertiesLocalL2ProjectionQ1_MPntPThermal(
																																					DM clone,Vec properties_A1,Vec properties_A2,Vec properties_B,
																																					const int npoints,MPntStd mp_std[],MPntPThermal mp_thermal[],QuadratureVolumeEnergy Q) 
{
	PetscScalar Ni_p[NODES_PER_EL_Q1_2D];
	PetscScalar Ae1[NODES_PER_EL_Q1_2D], Ae2[NODES_PER_EL_Q1_2D], Be[NODES_PER_EL_Q1_2D];
	PetscInt el_lidx[NODES_PER_EL_Q1_2D];
	Vec Lproperties_A1, Lproperties_A2, Lproperties_B;
	PetscScalar *LA_properties_A1, *LA_properties_A2, *LA_properties_B;
	PetscLogDouble t0,t1;
	PetscInt p,i;
	PetscInt nel,nen,e;
	const PetscInt *elnidx;
	
	PetscInt ngp;
	PetscScalar *xi_mp;
	PetscScalar NIu[MAX_QUAD_PNTS][U_BASIS_FUNCTIONS];
	CoefficientsEnergyEq *quadraturepoints;
	PetscErrorCode ierr;
	
	
	PetscFunctionBegin;
	
	
	ierr = DMGetLocalVector(clone,&Lproperties_A1);CHKERRQ(ierr);		ierr = VecZeroEntries(Lproperties_A1);CHKERRQ(ierr);
	ierr = DMGetLocalVector(clone,&Lproperties_A2);CHKERRQ(ierr);		ierr = VecZeroEntries(Lproperties_A2);CHKERRQ(ierr);
	ierr = DMGetLocalVector(clone,&Lproperties_B);CHKERRQ(ierr);		ierr = VecZeroEntries(Lproperties_B);CHKERRQ(ierr);
	
	ierr = VecGetArray(Lproperties_A1,&LA_properties_A1);CHKERRQ(ierr);
	ierr = VecGetArray(Lproperties_A2,&LA_properties_A2);CHKERRQ(ierr);
	ierr = VecGetArray(Lproperties_B, &LA_properties_B);CHKERRQ(ierr);
	
	ierr = DMDAGetElementsQ1(clone,&nel,&nen,&elnidx);CHKERRQ(ierr);
	
	PetscGetTime(&t0);
	for (p=0; p<npoints; p++) {
		double *xi_p  = &mp_std[p].xi[0];
		double kappa_p = mp_thermal[p].kappa;
		double H_p     = mp_thermal[p].H;
		
		ierr = PetscMemzero(Ae1,sizeof(PetscScalar)*NODES_PER_EL_Q1_2D);CHKERRQ(ierr);
		ierr = PetscMemzero(Ae2,sizeof(PetscScalar)*NODES_PER_EL_Q1_2D);CHKERRQ(ierr);
		ierr = PetscMemzero(Be, sizeof(PetscScalar)*NODES_PER_EL_Q1_2D);CHKERRQ(ierr);
		
		ConstructNi_Q1_2D(xi_p,Ni_p);
		
		for (i=0; i<NODES_PER_EL_Q1_2D; i++) {
			Ae1[i] = Ni_p[i] * kappa_p;
			Ae2[i] = Ni_p[i] * H_p;
			Be[i]  = Ni_p[i];
		}
	
		
		/* sum into local vectors */
		e = mp_std[p].wil;
		ierr = GetElementLocalIndicesQ1(el_lidx,(PetscInt*)&elnidx[nen*e]);CHKERRQ(ierr);
		
		ierr = DMDASetValuesLocalStencil_AddValues_Scalar_Q1(LA_properties_A1, el_lidx,Ae1);CHKERRQ(ierr);
		ierr = DMDASetValuesLocalStencil_AddValues_Scalar_Q1(LA_properties_A2, el_lidx,Ae2);CHKERRQ(ierr);
		ierr = DMDASetValuesLocalStencil_AddValues_Scalar_Q1(LA_properties_B,  el_lidx,Be);CHKERRQ(ierr);
		
	}
	PetscGetTime(&t1);
	//	PetscPrintf(PETSC_COMM_WORLD,"  [ L2 projectionQ1 (summation): %1.4lf ]\n",t1-t0);
	
  ierr = VecRestoreArray(Lproperties_B,&LA_properties_B);CHKERRQ(ierr);
  ierr = VecRestoreArray(Lproperties_A2,&LA_properties_A2);CHKERRQ(ierr);
  ierr = VecRestoreArray(Lproperties_A1,&LA_properties_A1);CHKERRQ(ierr);
	
	
	/* scatter to quadrature points */
	ierr = DMLocalToGlobalBegin(clone,Lproperties_A1,ADD_VALUES,properties_A1);CHKERRQ(ierr);
	ierr = DMLocalToGlobalEnd(  clone,Lproperties_A1,ADD_VALUES,properties_A1);CHKERRQ(ierr);
	
	ierr = DMLocalToGlobalBegin(clone,Lproperties_A2,ADD_VALUES,properties_A2);CHKERRQ(ierr);
	ierr = DMLocalToGlobalEnd(  clone,Lproperties_A2,ADD_VALUES,properties_A2);CHKERRQ(ierr);
	
	ierr = DMLocalToGlobalBegin(clone,Lproperties_B,ADD_VALUES,properties_B);CHKERRQ(ierr);
	ierr = DMLocalToGlobalEnd(  clone,Lproperties_B,ADD_VALUES,properties_B);CHKERRQ(ierr);
	
	/* scale */
	ierr = VecPointwiseDivide( properties_A1, properties_A1, properties_B );CHKERRQ(ierr);
	ierr = VecPointwiseDivide( properties_A2, properties_A2, properties_B );CHKERRQ(ierr);
	/* ========================================= */
	
	/* scatter result back to local array and do the interpolation onto the quadrature points */
	ngp       = Q->ngp;
	xi_mp     = Q->xi;
	for (p=0; p<ngp; p++) {
		PetscScalar *xip = &xi_mp[2*p];
		
//		ierr = PetscMemzero(NIu[p], sizeof(PetscScalar)*NODES_PER_EL_Q1_2D);CHKERRQ(ierr);
		ConstructNi_Q1_2D(xip,NIu[p]);
	}
	
	
	PetscGetTime(&t0);
	ierr = VecZeroEntries(Lproperties_A1);CHKERRQ(ierr);
	ierr = VecZeroEntries(Lproperties_A2);CHKERRQ(ierr);
	
	ierr = DMGlobalToLocalBegin(clone,properties_A1,INSERT_VALUES,Lproperties_A1);CHKERRQ(ierr);
	ierr = DMGlobalToLocalEnd(  clone,properties_A1,INSERT_VALUES,Lproperties_A1);CHKERRQ(ierr);
	
	ierr = DMGlobalToLocalBegin(clone,properties_A2,INSERT_VALUES,Lproperties_A2);CHKERRQ(ierr);
	ierr = DMGlobalToLocalEnd(  clone,properties_A2,INSERT_VALUES,Lproperties_A2);CHKERRQ(ierr);
	PetscGetTime(&t1);
	//	PetscPrintf(PETSC_COMM_WORLD,"  [ L2 projectionQ1 (scatter): %1.4lf ]\n",t1-t0);
	
	PetscGetTime(&t0);
	ierr = VecGetArray(Lproperties_A1,&LA_properties_A1);CHKERRQ(ierr);
	ierr = VecGetArray(Lproperties_A2,&LA_properties_A2);CHKERRQ(ierr);
	
	/* traverse elements and interpolate */
	for (e=0;e<nel;e++) {
		ierr = QuadratureVolumeEnergyGetCell(Q,e,&quadraturepoints);CHKERRQ(ierr);
		
		ierr = GetElementLocalIndicesQ1(el_lidx,(PetscInt*)&elnidx[nen*e]);CHKERRQ(ierr);
		
		ierr = DMDAGetScalarElementFieldQ1_2D(Ae1,(PetscInt*)&elnidx[nen*e],LA_properties_A1);CHKERRQ(ierr);
		ierr = DMDAGetScalarElementFieldQ1_2D(Ae2,(PetscInt*)&elnidx[nen*e],LA_properties_A2);CHKERRQ(ierr);
		
		/* zero out the junk */
		
		for (p=0; p<ngp; p++) {
			quadraturepoints[p].diffusivity = 0.0;
			quadraturepoints[p].heat_source = 0.0;
			for (i=0; i<NODES_PER_EL_Q1_2D; i++) {
				quadraturepoints[p].diffusivity  += NIu[p][i] * Ae1[i];
				quadraturepoints[p].heat_source  += NIu[p][i] * Ae2[i];
			}
		}
	}
	
  ierr = VecRestoreArray(Lproperties_A2,&LA_properties_A2);CHKERRQ(ierr);
  ierr = VecRestoreArray(Lproperties_A1,&LA_properties_A1);CHKERRQ(ierr);
	
	PetscGetTime(&t1);
	//	PetscPrintf(PETSC_COMM_WORLD,"  [ L2 projectionQ1 (interpolation): %1.4lf ]\n",t1-t0);
	
	ierr = DMRestoreLocalVector(clone,&Lproperties_B);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(clone,&Lproperties_A2);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(clone,&Lproperties_A1);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SwarmUpdateQuadraturePropertiesLocalL2Projection_Q1_MPntPThermal"
PetscErrorCode SwarmUpdateQuadraturePropertiesLocalL2Projection_Q1_MPntPThermal(const int npoints,MPntStd mp_std[],MPntPThermal mp_thermal[],DM da,QuadratureVolumeEnergy Q)
{
	PetscInt  dof;
	DM        clone;
	Vec       properties_A1, properties_A2, properties_B;
	PetscBool view;
	DMDAE     dmdae;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	/* check we have a valid Q1 DMDA passed in here */
	ierr = DMGetDMDAE(da,&dmdae);CHKERRQ(ierr);
	
	/* setup */
	clone  = da;
	ierr = DMGetGlobalVector(clone,&properties_A1);CHKERRQ(ierr);  ierr = PetscObjectSetName( (PetscObject)properties_A1, "LocalL2ProjQ1_kappa");CHKERRQ(ierr);
	ierr = DMGetGlobalVector(clone,&properties_A2);CHKERRQ(ierr);  ierr = PetscObjectSetName( (PetscObject)properties_A2, "LocalL2ProjQ1_H");CHKERRQ(ierr);
	ierr = DMGetGlobalVector(clone,&properties_B);CHKERRQ(ierr);
	
	ierr = VecZeroEntries(properties_A1);CHKERRQ(ierr);
	ierr = VecZeroEntries(properties_A2);CHKERRQ(ierr);
	ierr = VecZeroEntries(properties_B);CHKERRQ(ierr);
	
	/* compute */
	ierr = _SwarmUpdateGaussPropertiesLocalL2ProjectionQ1_MPntPThermal(
																																		da, properties_A1,properties_A2,properties_B, 																																	 
																																		npoints, mp_std,mp_thermal, Q );CHKERRQ(ierr);
	
	/* view */
	view = PETSC_FALSE;
	PetscOptionsGetBool(PETSC_NULL,"-view_projected_marker_fields_thm",&view,PETSC_NULL);
	if (view) {
		PetscViewer viewer;
		
		ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, "SwarmUpdateProperties_LocalL2Proj_PThermal.vtk", &viewer);CHKERRQ(ierr);
		ierr = PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
		ierr = DMView(clone, viewer);CHKERRQ(ierr);
		ierr = VecView(properties_A1, viewer);CHKERRQ(ierr);
		ierr = VecView(properties_A2, viewer);CHKERRQ(ierr);
		ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
	}
	
	/* destroy */
	ierr = DMRestoreGlobalVector(clone,&properties_B);CHKERRQ(ierr);
	ierr = DMRestoreGlobalVector(clone,&properties_A2);CHKERRQ(ierr);
	ierr = DMRestoreGlobalVector(clone,&properties_A1);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}


