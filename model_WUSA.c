/*

 Input / command line parameters:
 -Ttop
 -Tbot
 -mx
 -my
 -vx -3.000000e-10
 -bc_type 0
 -output_frequency 1
 -output_path WUSA
 -remesh_type 2
 -stokes_scale_length 1.000000e+04
 -stokes_scale_velocity 1.000000e-10
 -stokes_scale_viscosity 1.000000e+27
 -dt_max 3e11
 -nsteps 500
 -time_max 5.000000e+15
 -output_frequency 20

 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h"
#include "pTatin2d.h"

#define WRITE_VP
#define WRITE_TFIELD
#define WRITE_MPOINTS
//#define WRITE_QPOINTS
#define WRITE_TCELL
//#define WRITE_INT_FIELDS
//#define WRITE_FACEQPOINTS


#undef __FUNCT__
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_WUSA"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_WUSA(pTatinCtx ctx)
{
  PhaseMap phasemap;
  TopoMap topomap;
  PetscInt M,N;
  PetscBool flg;
  PetscBool found;
  PetscErrorCode ierr;
  PetscReal Ox,Oy,Lx,Ly,gravity_acceleration;
  char name[PETSC_MAX_PATH_LEN];
  char map_file[PETSC_MAX_PATH_LEN];
  char topo_file[PETSC_MAX_PATH_LEN];
  pTatinUnits *units;

  PetscFunctionBegin;
  units   = &ctx->units;
  gravity_acceleration = 10;
  Ox = 0.0;
  Oy = -400.e3;
  Lx = 1000.e3;
  Ly = 0.0;

  /* make a regular non dimensional grid from option file parameters*/
  ierr  = PetscOptionsGetReal(PETSC_NULL,"-grav",&gravity_acceleration,&found);CHKERRQ(ierr);
  ierr = QuadratureStokesGravityModelCoordinateAlignedSetValues(ctx->Q,0.0, -gravity_acceleration);CHKERRQ(ierr);

  ierr  = PetscOptionsGetReal(PETSC_NULL,"-Ox",&Ox,&found);CHKERRQ(ierr);
  ierr  = PetscOptionsGetReal(PETSC_NULL,"-Oy",&Oy,&found);CHKERRQ(ierr);
  ierr  = PetscOptionsGetReal(PETSC_NULL,"-Lx",&Lx,&found);CHKERRQ(ierr);
  ierr  = PetscOptionsGetReal(PETSC_NULL,"-Ly",&Ly,&found);CHKERRQ(ierr);

  PetscPrintf(PETSC_COMM_WORLD,"ApplyInitialMeshGeometry: options Domain_SI = [ %1.4e , %1.4e ] x [ %1.4e , %1.4e ] \n", Ox, Lx,Oy,Ly );
  UnitsApplyInverseScaling(units->si_length,Ox,&Ox);
  UnitsApplyInverseScaling(units->si_length,Oy,&Oy);
  UnitsApplyInverseScaling(units->si_length,Lx,&Lx);
  UnitsApplyInverseScaling(units->si_length,Ly,&Ly);

  PetscPrintf(PETSC_COMM_WORLD,"ApplyInitialMeshGeometry : options Domain_ND = [ %1.4e , %1.4e ] x [ %1.4e , %1.4e ] \n", Ox, Lx,Oy,Ly );
  ierr = DMDASetUniformCoordinates(ctx->dav,Ox,Lx,Oy,Ly, 0.0,0.0);CHKERRQ(ierr);

    {
    PetscBool active = PETSC_FALSE;
    ierr = pTatinPhysCompActivated(ctx,PhysComp_Energy,&active);CHKERRQ(ierr);

    if (active) {
      DM da;
      Vec T;
      PhysCompEnergyCtx phys_energy;
      TempMap  map;
      char file[PETSC_MAX_PATH_LEN];
      char prodmapname[PETSC_MAX_PATH_LEN];
      double T0;

      phys_energy = ctx->phys_energy;
      da          = phys_energy->daT;
      T           = phys_energy->T;

      ierr = DMDAProjectCoordinatesQ2toQ1_2d(ctx->dav,da);CHKERRQ(ierr);
      }
    }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatin2d_ModelSetMaterialProperty_WUSA"
PetscErrorCode pTatin2d_ModelSetMaterialProperty_WUSA(pTatinCtx ctx)

{
  RheologyConstants      *rheology;
  PetscInt               nphase, i,rheol_type;
  PetscErrorCode         ierr;
  char                   *option_name;
  PetscBool              found;
  int                    model_type;
  pTatinUnits            *units;
  
  PetscFunctionBegin;

  rheol_type = 6; // Rheology_VPT_STD
  ctx->rheology_constants.rheology_type = rheol_type;
  rheology   = &ctx->rheology_constants;
  units   = &ctx->units;
  rheology->eta_upper_cutoff_global = 1.e25;
  rheology->eta_lower_cutoff_global = 1.e19;
  ierr       = PetscOptionsGetReal(PETSC_NULL,"-eta_max_cut_off",&rheology->eta_upper_cutoff_global,0);CHKERRQ(ierr);
  ierr       = PetscOptionsGetReal(PETSC_NULL,"-eta_min_cut_off",&rheology->eta_lower_cutoff_global,0);CHKERRQ(ierr);

    nphase = 4;
    
    for (i=0;i<4;i++){
    char file[PETSC_MAX_PATH_LEN];
    char mapname[PETSC_MAX_PATH_LEN];
    char name[PETSC_MAX_PATH_LEN];
    TempMap  map;
    rheology->temp_kappa[i]     = 1e-6;
    rheology->viscous_type[i]   = 4;
    rheology->density_type[i]   = 2;
    rheology->arrh_Ascale[i]    = 1e6; 
    rheology->arrh_Vmol[i]      = 1.0e-5;
    rheology->arrh_Tref[i]      = 273;
    rheology->plastic_type[i]   = 2;
    rheology->tens_cutoff[i]    = 1.e6;
    rheology->Hst_cutoff[i]     = 4.e8;
    rheology->softening_type[i] = 1;
    rheology->soft_min_strain_cutoff[i] = 0.0;
    rheology->soft_max_strain_cutoff[i] = 0.1;
    
    asprintf(&option_name, "-rheolmixingfile_%d",i);
    ierr = PetscOptionsGetString(PETSC_NULL,option_name,file,PETSC_MAX_PATH_LEN-1,&found);CHKERRQ(ierr);
	free(option_name);					
	
                            sprintf(name,"%s.Qmap",file);
							TempMapLoadFromFile(name,&map);
						    RheolMapApplyScale(map,units);
							sprintf(mapname,"Qmap_%d",i);
							ierr = pTatinCtxAttachMap(ctx,map,mapname);CHKERRQ(ierr);
							
							sprintf(name,"%s.Amap",file);
							TempMapLoadFromFile(name,&map);
							RheolMapApplyScale(map,units);
							sprintf(mapname,"Amap_%d",i);
							ierr = pTatinCtxAttachMap(ctx,map,mapname);CHKERRQ(ierr);
							
							sprintf(name,"%s.nexpmap",file);
							TempMapLoadFromFile(name,&map);
							sprintf(mapname,"nexpmap_%d",i);
							ierr = pTatinCtxAttachMap(ctx,map,mapname);CHKERRQ(ierr);

                            sprintf(name,"%s.densmap",file);
							TempMapLoadFromFile(name,&map);
							DensMapApplyScale(map,units);
                            sprintf(mapname,"densmap_%d",i);
							ierr = pTatinCtxAttachMap(ctx,map,mapname);CHKERRQ(ierr);
    }
    
    rheology->const_rho0[0] = 2700; // initial approximation rho
    rheology->const_rho0[1] = 3300;
    rheology->const_rho0[2] = 3300;
    rheology->const_rho0[3] = 2500;

    rheology->const_eta0[0] = 1e22; // initial approximation eta
    rheology->const_eta0[1] = 1e22;
    rheology->const_eta0[2] = 1e20;
    rheology->const_eta0[3] = 1e22;
    //rheology->arrh_Ascale[0]    = 1e5;
    //rheology->arrh_Ascale[3]    = 1e5;
    rheology->mises_tau_yield[0]=2.e7; // cohesion 
    rheology->mises_tau_yield[1]=4.e8; // cohesion
    rheology->mises_tau_yield[2]=4.e8; // cohesion
    rheology->mises_tau_yield[3]=2.e7; // cohesion
    
    rheology->dp_pressure_dependance[0] = 30.; // Angle for MISSES behaviour
    rheology->dp_pressure_dependance[1] = 2.0; // using Huismans numbers SP282 Fig3 Top Right
    rheology->dp_pressure_dependance[2] = 2.0;
    rheology->dp_pressure_dependance[3] = 10.0;
    rheology->temp_kappa[3]     = 1e-7;
    rheology->soft_Co_inf[0] = 2.e7; // Cohesion 
    rheology->soft_Co_inf[1] = 3.e8;
    rheology->soft_Co_inf[2] = 3.e8;
    rheology->soft_Co_inf[3] = 2.e7;

    rheology->soft_phi_inf[0] = 10.; // min angle of internal friction
    rheology->soft_phi_inf[1] = 2.0; // SP282
    rheology->soft_phi_inf[2] = 2.0;
    rheology->soft_phi_inf[3] = 10.;
    
    
  PetscFunctionReturn(0);
}

/*pTatin2d_ModelSetMarkerIndexFromMap_GENE
 assign index to markers from map that is in context
 */
#undef __FUNCT__
#define __FUNCT__ "pTatin2d_ModelSetMarkerIndex_WUSA"
PetscErrorCode pTatin2d_ModelSetMarkerIndex_WUSA(pTatinCtx ctx)
{

  PetscInt               p,n_mp_points;
  DataBucket             db;
  DataField              PField_std;
  int                    phase;
  PetscScalar            ymoho,ylowercrust,yastenosphere,ysedim;
  PetscErrorCode ierr;
  pTatinUnits *units;

  PetscFunctionBegin;
  units=&ctx->units;
  yastenosphere=-100.e3;
  ymoho = -35.0e3; // SI units
  UnitsApplyInverseScaling(units->si_length,yastenosphere,&yastenosphere); // scaled with the rest
  UnitsApplyInverseScaling(units->si_length,ymoho,&ymoho); // scaled with the rest
 
  /* define properties on material points */
  db = ctx->db;
  DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
  DataFieldGetAccess(PField_std);
  DataFieldVerifyAccess(PField_std,sizeof(MPntStd));

  DataBucketGetSizes(db,&n_mp_points,0,0);

  for (p=0; p<n_mp_points; p++) {
    MPntStd     *material_point;
    double  *position;
    DataFieldAccessPoint(PField_std,p,(void**)&material_point);
    /* Access using the getter function provided for you (recommeneded for beginner user) */
    MPntStdGetField_global_coord(material_point,&position);
 
 
    phase = 2;
    if (position[1] > yastenosphere){phase=1;}
    if (position[1] > ymoho){phase=0;}
    
    MPntStdSetField_phase_index(material_point,phase);
  }

  DataFieldRestoreAccess(PField_std);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatin2d_ModelSetInitialTemperatureOnNodes_WUSA"
PetscErrorCode pTatin2d_ModelSetInitialTemperatureOnNodes_WUSA(pTatinCtx ctx)
/* define properties on material points */
{
  PetscErrorCode         ierr;
  DM da;
  Vec T;
  PhysCompEnergyCtx phys_energy;
  PetscInt i,j,si,sj,nx,ny;
  Vec coords;
  DMDACoor2d **LA_coords;
  DM cda;
  PetscBool found;
  PetscScalar **LA_T;
  PetscScalar Ttop,Tbot,jmin[2],jmax[2],Tasth;
  PetscReal age0,age_anom,age,L_Bar,Cx,Wx,kappa,yastenosphere;
  pTatinUnits *units;
  
  PetscFunctionBegin;

  phys_energy = ctx->phys_energy;
  da          = phys_energy->daT;
  T           = phys_energy->T;
        units   = &ctx->units;

  Tbot  = 1300.0;
  Ttop  = 0.0;
  Tasth  = 800.0;
  yastenosphere = -100.e3;
  UnitsApplyInverseScaling(units->si_length,yastenosphere,&yastenosphere); // scaled with the rest

  ierr = PetscOptionsGetScalar(PETSC_NULL,"-Ttop",&Ttop,PETSC_NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetScalar(PETSC_NULL,"-Tbot",&Tbot,PETSC_NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetScalar(PETSC_NULL,"-Tasth",&Tasth,PETSC_NULL);CHKERRQ(ierr);

  ierr = DMDAGetCoordinates(da,&coords);CHKERRQ(ierr);
  ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(cda,coords,&LA_coords);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da,T,&LA_T);CHKERRQ(ierr);
  ierr = DMDAGetCorners(da,&si,&sj,0 , &nx,&ny,0);CHKERRQ(ierr);

  DMDAGetBoundingBox(da,jmin,jmax);

  for( j=sj; j<sj+ny; j++ ) {
    for( i=si; i<si+nx; i++ ) {
      PetscScalar xn,yn,Tic;
      double position[2];
      position[0] = LA_coords[j][i].x;
      position[1] = LA_coords[j][i].y;

      /* linear gradient from top 0 to bottom temp Tic*/
       LA_T[j][i] = (jmax[1]-position[1])/(jmax[1]-yastenosphere)*(Tasth-Ttop)+Ttop;
      if (position[1] < yastenosphere ) {LA_T[j][i]=Tbot;}
      
    }
  }
  ierr = DMDAVecRestoreArray(da,T,&LA_T);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(cda,coords,&LA_coords);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatin2d_ModelSetInitialStokesVariableOnMarker_WUSA"
PetscErrorCode pTatin2d_ModelSetInitialStokesVariableOnMarker_WUSA(pTatinCtx ctx)
/* define properties on material points */
{
  PhaseMap               phasemap;
  PetscInt               e,p,n_mp_points;
  DataBucket             db;
  DataField              PField_std,PField_stokes;
  int                    phase_index, i;
  PetscErrorCode         ierr;
  RheologyConstants      *rheology;
  double                 eta_ND,rho_ND;
  pTatinUnits            *units;

  PetscFunctionBegin;
  units   = &ctx->units;

  rheology   = &ctx->rheology_constants;
  db = ctx->db;

  DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
  DataFieldGetAccess(PField_std);
  DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
  DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
  DataFieldGetAccess(PField_stokes);
  DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));
  DataBucketGetSizes(db,&n_mp_points,0,0);

  for (p=0; p<n_mp_points; p++) {
    MPntStd     *material_point;
    MPntPStokes *mpprop_stokes;
    DataFieldAccessPoint(PField_std,p,(void**)&material_point);
    DataFieldAccessPoint(PField_stokes,p,(void**)&mpprop_stokes);
    MPntStdGetField_phase_index(material_point,&phase_index);

    UnitsApplyInverseScaling(units->si_viscosity,rheology->const_eta0[phase_index],&eta_ND);
    UnitsApplyInverseScaling(units->si_force_per_volume,rheology->const_rho0[phase_index],&rho_ND);
    MPntPStokesSetField_eta_effective(mpprop_stokes,eta_ND);
    MPntPStokesSetField_density(mpprop_stokes,rho_ND);
  }

  DataFieldRestoreAccess(PField_std);
  DataFieldRestoreAccess(PField_stokes);

  PetscFunctionReturn(0);
}

/*pTatin2d_ModelSetInitialEnergyVariableOnMarker_GENE
 assign initial kappa and prod
 */
#undef __FUNCT__
#define __FUNCT__ "pTatin2d_ModelSetInitialEnergyVariableOnMarker_WUSA"
PetscErrorCode pTatin2d_ModelSetInitialEnergyVariableOnMarker_WUSA(pTatinCtx ctx)
{
  DataField         PField_thermal,PField_std,PField_stokes;
  DataBucket        db;
  int               phase_index;
  PetscInt          p,n_mp_points;
  PetscErrorCode    ierr;
  RheologyConstants *rheology;
  pTatinUnits *units;
  TempMap                map;
  PetscBool         isproduction;
  char file[PETSC_MAX_PATH_LEN];

  PetscFunctionBegin;
  units   = &ctx->units;
  rheology   = &ctx->rheology_constants;
  db = ctx->db;

  /* standard data */
  DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
  DataFieldGetAccess(PField_std);
  DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
  /* thermal data */
  DataBucketGetDataFieldByName(db,MPntPThermal_classname,&PField_thermal);
  DataFieldGetAccess(PField_thermal);
  DataFieldVerifyAccess(PField_thermal,sizeof(MPntPThermal));

  DataBucketGetSizes(db,&n_mp_points,0,0);

  for (p=0; p<n_mp_points; p++) {
    MPntStd      *material_point;
    MPntPThermal *material_point_thermal;
    double       *position,kappa_ND,prod_ND;

    DataFieldAccessPoint(PField_std,    p,(void**)&material_point);
    DataFieldAccessPoint(PField_thermal,p,(void**)&material_point_thermal);
    MPntStdGetField_phase_index(material_point,&phase_index);
    position = material_point->coor;

    UnitsApplyInverseScaling(units->si_diffusivity,rheology->temp_kappa[phase_index],&kappa_ND);
    MPntPThermalSetField_diffusivity(material_point_thermal,kappa_ND);

    prod_ND = 0.0;
    MPntPThermalSetField_heat_source(material_point_thermal,prod_ND);

  }
  DataFieldRestoreAccess(PField_std);
  DataFieldRestoreAccess(PField_thermal);
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "pTatin2d_ModelSetInitialPlsVariableOnMarker_WUSA"
PetscErrorCode pTatin2d_ModelSetInitialPlsVariableOnMarker_WUSA(pTatinCtx ctx)
/* define properties on material points */
{
  PhaseMap               phasemap;
  PetscInt               e,p,n_mp_points;
  DataBucket             db;
  DataField              PField_std,PField_stokes,PField_stokespl;
  int                    phase_index, i;
  PetscErrorCode         ierr;
  RheologyConstants      *rheology;

  PetscFunctionBegin;

  rheology   = &ctx->rheology_constants;
  db = ctx->db;


  DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
  DataFieldGetAccess(PField_std);
  DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
  /* get the plastic variables */
  DataBucketGetDataFieldByName(db,MPntPStokesPl_classname,&PField_stokespl);
  DataFieldGetAccess(PField_stokespl);
  DataFieldVerifyAccess(PField_stokespl,sizeof(MPntPStokesPl));

  DataBucketGetSizes(db,&n_mp_points,0,0);

  for (p=0; p<n_mp_points; p++) {
    MPntStd     *material_point;
    MPntPStokesPl *mpprop_stokespl; /* plastic variables */
    DataFieldAccessPoint(PField_std,p,(void**)&material_point);
    DataFieldAccessPoint(PField_stokespl,p,(void**)&mpprop_stokespl);

    MPntStdGetField_phase_index(material_point,&phase_index);
    MPntPStokesPlSetField_plastic_strain(mpprop_stokespl,0.0);
    //TODO  Add noise if needed
    MPntPStokesPlSetField_yield_indicator(mpprop_stokespl,0.0);
  }

  DataFieldRestoreAccess(PField_std);
  DataFieldRestoreAccess(PField_stokespl);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_WUSA"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_WUSA(pTatinCtx ctx)
{
  BCList         ubclist;
  PetscScalar    bcval;
  DM             dav;
  PetscErrorCode ierr;

  PetscReal jmin[3],jmax[3];
  PetscFunctionBegin;
  PetscScalar opts_srH, opts_Lx,opts_Ly,p_base;
  PetscInt opts_bcs;
  PetscScalar alpha_m;
  pTatinUnits *units;
  BC_Alabeaumont bcdata;
  BC_Step   bcdatastep;
  PetscScalar y1,dy,coeff;

  units   = &ctx->units;
  ubclist = ctx->u_bclist;
  dav     = ctx->dav;

  opts_srH = 0.0;
  ierr = PetscOptionsGetScalar(PETSC_NULL,"-vx",&opts_srH,0);CHKERRQ(ierr);

  UnitsApplyInverseScaling(units->si_velocity,opts_srH,&opts_srH);

  DMDAGetBoundingBox(dav,jmin,jmax);
  opts_Lx = jmax[0]-jmin[0];
  opts_Ly = jmax[1]-jmin[1];

  
      /*incompressible free surface, basically fixe the velocity at the bottom to fill up or empty down with material that leaves on the side*/
      bcval = 2*opts_srH*opts_Ly/opts_Lx;
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

      bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
   

    /* check for energy solver */
    {
        PetscBool active = PETSC_FALSE;
        ierr = pTatinPhysCompActivated(ctx,PhysComp_Energy,&active);CHKERRQ(ierr);

        if (active) {
            DM da;
            Vec T;
            PhysCompEnergyCtx phys_energy;
            PetscScalar bcval;
            PetscBool   found;

            phys_energy = ctx->phys_energy;
            da          = phys_energy->daT;
            T           = phys_energy->T;

            bcval = 0.0;
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-Ttop",&bcval,&found);CHKERRQ(ierr);
            ierr = DMDABCListTraverse(phys_energy->T_bclist,da,DMDABCList_JMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
            bcval = 1400.0;
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-Tbot",&bcval,&found);CHKERRQ(ierr);
            ierr = DMDABCListTraverse(phys_energy->T_bclist,da,DMDABCList_JMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
        }
    }


    PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_WUSA"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_WUSA(pTatinCtx ctx)
{
  PetscErrorCode         ierr;
  PetscBool   store_ages=PETSC_FALSE;
  pTatinUnits *units;

  PetscFunctionBegin;
  units   = &ctx->units;
  UnitsApplyInverseScaling(units->si_time,ctx->dt_min,&ctx->dt_min);
  UnitsApplyInverseScaling(units->si_time,ctx->dt,&ctx->dt);
  UnitsApplyInverseScaling(units->si_time,ctx->dt_adv,&ctx->dt_adv);
  UnitsApplyInverseScaling(units->si_time,ctx->dt_max,&ctx->dt_max);
  UnitsApplyInverseScaling(units->si_time,ctx->time_max,&ctx->time_max);
  UnitsApplyInverseScaling(units->si_length,ctx->max_disp_y,&ctx->max_disp_y);

  ierr = pTatin2d_ModelSetMaterialProperty_WUSA(ctx); CHKERRQ(ierr);
  ierr = pTatin2d_ModelSetMarkerIndex_WUSA(ctx); CHKERRQ(ierr);
  ierr = pTatin2d_ModelSetInitialStokesVariableOnMarker_WUSA(ctx); CHKERRQ(ierr);
  ierr= pTatin2d_ModelSetInitialTemperatureOnNodes_WUSA(ctx); CHKERRQ(ierr);
  ierr = pTatin2d_ModelSetInitialEnergyVariableOnMarker_WUSA(ctx); CHKERRQ(ierr);
  ierr = pTatin2d_ModelSetInitialPlsVariableOnMarker_GENE(ctx); CHKERRQ(ierr);
  ierr =pTatin2d_ModelSetInitialAgesVariableOnMarker_GENE(ctx); CHKERRQ(ierr);
// Erosion
    {
      PetscBool active = PETSC_FALSE;
      ierr = pTatinPhysCompActivated(ctx,PhysComp_SPM,&active);CHKERRQ(ierr);
      if (active) {
          ierr= pTatin2d_ModelSetSPMParameters_GENE(ctx); CHKERRQ(ierr);
      }
    }
  PetscFunctionReturn(0);
}
#undef __FUNCT__
