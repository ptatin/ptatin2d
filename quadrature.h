
#ifndef __pTatin2d_quadrature_h__
#define __pTatin2d_quadrature_h__

typedef enum { DMDATracLoc_IMIN_LOC=0,DMDATracLoc_IMAX_LOC,DMDATracLoc_JMIN_LOC,DMDATracLoc_JMAX_LOC } DMDATractionConstraintLoc;


PetscErrorCode _SurfaceQuadratureStokesCreate(QuadElementEdge index,PetscInt nfaces,SurfaceQuadratureStokes *Q);
PetscErrorCode SurfaceQuadratureStokesCreate(DM da,QuadElementEdge index,SurfaceQuadratureStokes *Q);
PetscErrorCode SurfaceQuadratureStokesDestroy(SurfaceQuadratureStokes *Q);
PetscErrorCode SurfaceQuadratureStokesCellIndexSetUp(SurfaceQuadratureStokes Q,QuadElementEdge index,DM da);
PetscErrorCode SurfaceQuadratureStokesCoordinatesSetUp(SurfaceQuadratureStokes Q,DM da);
PetscErrorCode SurfaceQuadratureStokesOrientationSetUp(SurfaceQuadratureStokes Q,DM da);
PetscErrorCode SurfaceQuadratureStokesGeometrySetUp(SurfaceQuadratureStokes Q,DM da);

PetscErrorCode SurfaceQuadratureStokesGetCell(SurfaceQuadratureStokes Q,PetscInt cidx,SurfaceQPointCoefficientsStokes **points);

PetscErrorCode pTatinOutputSurfaceQuadratureStokesPointsVTU(SurfaceQuadratureStokes surfQ,const char name[]);
PetscErrorCode pTatinOutputSurfaceQuadratureStokesPointsRawPVTU(const char prefix[],const char name[]);
PetscErrorCode pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(pTatinCtx user,const char path[],const char prefix[]);

PetscErrorCode pTatin2d_TestSurfaceQuadratureStokes(pTatinCtx ctx);
PetscErrorCode pTatin2d_TestSurfaceQuadratureStokes2(pTatinCtx ctx);

PetscBool SurfaceQuadatureEvaluator_constant(PetscScalar position[],PetscScalar normal[], PetscScalar tangent[],PetscScalar *value,void *ctx);
PetscErrorCode SurfaceQuadratureStokesTraverseSetTractionComponent(SurfaceQuadratureStokes surfQ[],DMDATractionConstraintLoc edge,PetscInt dof_idx,
									PetscBool (*eval)(PetscScalar*,PetscScalar*,PetscScalar*,PetscScalar*,void*),void *ctx);

PetscBool SurfaceQuadatureEvaluator_Wrinkler(PetscScalar position[],PetscScalar normal[], PetscScalar tangent[],PetscScalar value[],void *ctx) ;
PetscBool SurfaceQuadatureEvaluator_Isotropic(PetscScalar position[],PetscScalar normal[], PetscScalar tangent[],PetscScalar value[],void *ctx) ;
PetscErrorCode SurfaceQuadratureStokesTraverseSetTraction(SurfaceQuadratureStokes surfQ[],DMDATractionConstraintLoc edge,
									PetscBool (*eval)(PetscScalar*,PetscScalar*,PetscScalar*,PetscScalar*,void*),void *ctx);

#endif
