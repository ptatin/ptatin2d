
#include "pTatin2d.h"
#include "dmdae.h"
#include "element_type_Q2.h"
#include "element_type_Q1.h"
#include "phys_darcy_equation.h"

#define SUPG_EPS 1.0e-10


#undef __FUNCT__
#define __FUNCT__ "QuadratureVolumeDarcyGetCell"
PetscErrorCode QuadratureVolumeDarcyGetCell(QuadratureVolumeDarcy Q,PetscInt cidx,CoefficientsDarcyEq **points)
{
  PetscFunctionBegin;
	if (cidx>=Q->ncells) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_SIZ,"cidx > max cells");
	}
	*points = &Q->cellproperties[cidx*Q->ngp];
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureVolumeDarcyCreate"
PetscErrorCode QuadratureVolumeDarcyCreate(QuadratureStokesRule rule,PetscInt ncells,QuadratureVolumeDarcy *Q)
{
	QuadratureVolumeDarcy quadrature;
	PetscInt p,ngp;
	PetscScalar gp_xi[MAX_QUAD_PNTS][2],gp_weight[MAX_QUAD_PNTS];
	PetscErrorCode ierr;
	
  PetscFunctionBegin;
	ierr = PetscMalloc( sizeof(struct _p_QuadratureVolumeDarcy), &quadrature);CHKERRQ(ierr);
	
	PetscPrintf(PETSC_COMM_WORLD,"QuadratureVolumeDarcyCreate:\n");
	switch (rule) {
		case QRule_GaussLegendre1:
			PetscPrintf(PETSC_COMM_WORLD,"\tUsing 1 pnt Gauss Legendre quadrature\n");
			QuadratureCreateGauss_1pnt_2D(&ngp,gp_xi,gp_weight);
			break;
			
		case QRule_GaussLegendre2:
			PetscPrintf(PETSC_COMM_WORLD,"\tUsing 2x2 pnt Gauss Legendre quadrature\n");
			QuadratureCreateGauss_2pnt_2D(&ngp,gp_xi,gp_weight);
			break;
			
		case QRule_GaussLegendre3:
			PetscPrintf(PETSC_COMM_WORLD,"\tUsing 3x3 pnt Gauss Legendre quadrature\n");
			QuadratureCreateGauss_3pnt_2D(&ngp,gp_xi,gp_weight);
			break;
			
		case QRule_Subcell2x2GaussLegendre3:
			PetscPrintf(PETSC_COMM_WORLD,"\tUsing 2x2 subcell, with 3x3 pnt Gauss Legendre quadrature in each\n");
			QuadratureCreateGauss_2x3pnt_2D(&ngp,gp_xi,gp_weight);
			break;
			
		default: /* QRule_GaussLegendre3 */
			PetscPrintf(PETSC_COMM_WORLD,"\tUsing 3x3 pnt Gauss Legendre quadrature\n");
			QuadratureCreateGauss_3pnt_2D(&ngp,gp_xi,gp_weight);
			break;
	}
	
	quadrature->ncells = ncells;
	if (ncells!=0) {
		ierr = PetscMalloc( sizeof(CoefficientsDarcyEq)*ncells*ngp, &quadrature->cellproperties);CHKERRQ(ierr);
		ierr = PetscMemzero(quadrature->cellproperties,sizeof(CoefficientsDarcyEq)*ncells*ngp);CHKERRQ(ierr);

		/* init values to zero */
		for (p=0; p<ncells*ngp; p++) {
			quadrature->cellproperties[p].diff = 0.0;
			quadrature->cellproperties[p].source = 0.0;
		}
		PetscPrintf(PETSC_COMM_WORLD,"\tCoefficientsDarcyEq using %1.2lf MBytes \n", (double)(sizeof(CoefficientsDarcyEq)*ncells*ngp)*1.0e-6 );
	}
	
	
	/* allocate */
	quadrature->ngp = ngp;
	ierr = PetscMalloc( sizeof(PetscScalar)*ngp*NSD, &quadrature->xi );CHKERRQ(ierr);
	ierr = PetscMalloc( sizeof(PetscScalar)*ngp, &quadrature->weight );CHKERRQ(ierr);
	/* copy */
	for (p=0; p<ngp; p++) {
		quadrature->xi[NSD*p  ] = gp_xi[p][0];
		quadrature->xi[NSD*p+1] = gp_xi[p][1];
		
		quadrature->weight[p] = gp_weight[p];
	}
	
	*Q = quadrature;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureVolumeDarcyDestroy"
PetscErrorCode QuadratureVolumeDarcyDestroy(QuadratureVolumeDarcy *Q)
{
	QuadratureVolumeDarcy quadrature;
	PetscInt ncells;
	PetscErrorCode ierr;
	
  PetscFunctionBegin;
	
	if (!Q) { PetscFunctionReturn(0); }
	quadrature = *Q;
	
	ncells = quadrature->ncells;
	if (ncells!=0) {
		ierr = PetscFree(quadrature->cellproperties);CHKERRQ(ierr);
	}
	
	/* allocate */
	ierr = PetscFree( quadrature->xi );CHKERRQ(ierr);
	ierr = PetscFree( quadrature->weight );CHKERRQ(ierr);

	ierr = PetscFree( quadrature );CHKERRQ(ierr);
	*Q = PETSC_NULL;

  PetscFunctionReturn(0);
}



void FormMass_FormResidual_qp(
	PetscScalar Re[],
	PetscReal dt,
	PetscScalar el_coords[],
	PetscScalar el_phi[],PetscScalar el_phi_last[],
	PetscScalar gp_kappa[],PetscScalar gp_Q[],
	PetscInt ngp,PetscScalar gp_xi[],PetscScalar gp_weight[] )
{
  PetscInt    p,i,j;
  PetscScalar Ni_p[NODES_PER_EL_Q1_2D],Ni_supg_p[NODES_PER_EL_Q1_2D];
  PetscScalar GNi_p[NSD][NODES_PER_EL_Q1_2D],GNx_p[NSD][NODES_PER_EL_Q1_2D];
  PetscScalar phi_p,phi_last_p,f_p,v_p[2],kappa_p, gradphi_p[2],gradphiold_p[2],M_dotT_p;
  PetscScalar J_p,fac,gp_detJ[27];
  PetscScalar kappa_hat;
	
  /* compute constants for the element */
  for (p = 0; p < ngp; p++) {
    ConstructGNi_Q1_2D(&gp_xi[2*p],GNi_p);
    ConstructGNx_Q1_2D(GNi_p,GNx_p,el_coords,&gp_detJ[p]);
	}
  /* evaluate integral */
  for (p = 0; p < ngp; p++) {
    ConstructNi_Q1_2D(&gp_xi[2*p],Ni_p);
    ConstructGNi_Q1_2D(&gp_xi[2*p],GNi_p);
    ConstructGNx_Q1_2D(GNi_p,GNx_p,el_coords,&J_p);		
    fac = gp_weight[p]*J_p;
	kappa_p      = gp_kappa[p];
	f_p          = gp_Q[p];
    phi_p        = 0.0;
    phi_last_p   = 0.0;
    gradphi_p[0] = 0.0;
	gradphi_p[1] = 0.0;
    gradphiold_p[0] = 0.0;
	gradphiold_p[1] = 0.0;
    for (j=0; j<NODES_PER_EL_Q1_2D; j++) {
      phi_p        += Ni_p[j] * el_phi[j];      /* compute phi on the particle */
      phi_last_p   += Ni_p[j] * el_phi_last[j];  /* compute phi_dot on the particle */
      
      gradphiold_p[0] += GNx_p[0][j] * el_phi_last[j];
      gradphiold_p[1] += GNx_p[1][j] * el_phi_last[j];
			
      gradphi_p[0] += GNx_p[0][j] * el_phi[j];
      gradphi_p[1] += GNx_p[1][j] * el_phi[j];
			
    }
    		
    /* R = f - m phi_dot - c phi*/
    for (i = 0; i < NODES_PER_EL_Q1_2D; i++) {
//      Re[ i ] += fac * (
//				- dt * Ni_p[i] * f_p 
//				+ dt * kappa_p * ( GNx_p[0][i] * gradphi_p[0] + GNx_p[1][i] * gradphi_p[1] )  /* - C_diffusive . phi */
 //               + Ni_p[i] * phi_p 
 //               - Ni_p[i] * phi_last_p 
//				); 
      Re[ i ] += fac * (
				-  Ni_p[i] * f_p 
				+  kappa_p * ( GNx_p[0][i] * gradphi_p[0] + GNx_p[1][i] * gradphi_p[1] )  /* - C_diffusive . phi */ 
				);
    }
  }
	
}


void AElement_qp( PetscScalar Re[],PetscReal dt,PetscScalar el_coords[],
				  PetscScalar gp_kappa[],PetscInt ngp,PetscScalar gp_xi[],PetscScalar gp_weight[] )
{
  PetscInt    p,i,j;
  PetscScalar Ni_p[NODES_PER_EL_Q1_2D],Ni_supg_p[NODES_PER_EL_Q1_2D];
  PetscScalar GNi_p[NSD][NODES_PER_EL_Q1_2D],GNx_p[NSD][NODES_PER_EL_Q1_2D];
  PetscScalar J_p,fac,gp_detJ[27];
  PetscScalar kappa_p;
	
  /* compute constants for the element */
  for (p = 0; p < ngp; p++) {
    ConstructGNi_Q1_2D(&gp_xi[2*p],GNi_p);
    ConstructGNx_Q1_2D(GNi_p,GNx_p,el_coords,&gp_detJ[p]);
	}
	
  /* evaluate integral */
  for (p = 0; p < ngp; p++) {
    ConstructNi_Q1_2D(&gp_xi[2*p],Ni_p);
    ConstructGNi_Q1_2D(&gp_xi[2*p],GNi_p);
    ConstructGNx_Q1_2D(GNi_p,GNx_p,el_coords,&J_p);
		
    fac = gp_weight[p]*J_p;
    kappa_p = gp_kappa[p];
    	
    /* R = f - m phi_dot - c phi*/
		for (i=0; i<NODES_PER_EL_Q1_2D; i++) {
			for (j=0; j<NODES_PER_EL_Q1_2D; j++) {
				Re[j+i*NODES_PER_EL_Q1_2D] += fac * ( 
				// 	+ dt * kappa_p * ( GNx_p[0][i] * GNx_p[0][j] + GNx_p[1][i] * GNx_p[1][j] )
				// 	+ Ni_p[i] * Ni_p[j]);
				+ kappa_p * ( GNx_p[0][i] * GNx_p[0][j] + GNx_p[1][i] * GNx_p[1][j] ));
			
			}
		}
  } 
}

#undef __FUNCT__
#define __FUNCT__ "DarcyConstantsInitialise"
PetscErrorCode DarcyConstantsInitialise(DarcyConstants *R)
{	
  PetscInt p;
  PetscBool flg;
  PetscScalar perm,phi,truc;
  PetscErrorCode ierr; 
  
  /* Define defaults for the viscosity cut-offs */
    
  R->beta_H20  = 1.0e-9; // SI 
  R->rho_H20   = 1000.; // SI 
  R->vis_H20   = 0.1;   // 1 poise in SI
  
  ierr = PetscOptionsGetReal(PETSC_NULL,"-Darcy_water_compressibility",&truc,&flg);CHKERRQ(ierr);
  if (flg == PETSC_TRUE) { 
     R->beta_H20  = truc;
  }
  ierr = PetscOptionsGetReal(PETSC_NULL,"-Darcy_water_density",&truc,&flg);CHKERRQ(ierr);
  if (flg == PETSC_TRUE) { 
     R->beta_H20  = truc;
  }
  
  ierr = PetscOptionsGetReal(PETSC_NULL,"-Darcy_water_viscosity",&truc,&flg);CHKERRQ(ierr);
  if (flg == PETSC_TRUE) { 
     R->vis_H20  = truc;
  }
  
  PetscPrintf(PETSC_COMM_WORLD,"DARCY : Water: compressibility = %1.6e - Density = %1.3e  - Viscosity = %1.6e \n", R->beta_H20, R->rho_H20, R->vis_H20);

  
  
  R->beta_rock = 1.0e-10;
  R->eta0 = 1.0e+24;
  R->sr0 = 3.0e-12;
  
  flg = PETSC_FALSE;
  ierr = PetscOptionsGetReal(PETSC_NULL,"-Darcy_rock_compressibility",&truc,&flg);CHKERRQ(ierr);
  if (flg == PETSC_TRUE) { 
     R->beta_rock  = truc;
  }
  
  flg = PETSC_FALSE;
  ierr = PetscOptionsGetReal(PETSC_NULL,"-eta_upper_cutoff_global",&truc,&flg);CHKERRQ(ierr);
  if (flg == PETSC_TRUE) {
      R->eta0 = truc;
  }
  
  flg = PETSC_FALSE;
  ierr = PetscOptionsGetReal(PETSC_NULL,"-Darcy_char_viscosity",&truc,&flg);CHKERRQ(ierr);
  if (flg == PETSC_TRUE) { 
     R->eta0  = truc;
  }
  
  flg = PETSC_FALSE;
  ierr = PetscOptionsGetReal(PETSC_NULL,"-Darcy_char_strain_rate",&truc,&flg);CHKERRQ(ierr);
  if (flg == PETSC_TRUE) { 
     R->sr0  = truc;
  }
  
  PetscPrintf(PETSC_COMM_WORLD,"DARCY : Rock  compressibility = %1.6e - char. strain rate = %1.6e - char. viscosity = %1.6e \n", R->beta_rock,R->sr0, R->eta0);

  
  R->apply_perm_cutoff = PETSC_FALSE;
  R->perm_lower_cutoff = 1.e-100;
  R->perm_upper_cutoff = 1.e+100;
  
  flg = PETSC_FALSE;
  ierr = PetscOptionsGetReal(PETSC_NULL,"-Darcy_perm_lower_cutoff",&perm,&flg);CHKERRQ(ierr);
  if (flg == PETSC_TRUE) { 
    R->apply_perm_cutoff = PETSC_TRUE;
    R->perm_lower_cutoff = perm;
  }
  
  flg = PETSC_FALSE;
  ierr = PetscOptionsGetReal(PETSC_NULL,"-Darcy_perm_lower_cutoff",&perm,&flg);CHKERRQ(ierr);  
  
  if (flg == PETSC_TRUE) { 
    R->apply_perm_cutoff = PETSC_TRUE;
    R->perm_upper_cutoff = perm;
  }
  
  if ( R->apply_perm_cutoff == PETSC_TRUE) { 
  PetscPrintf(PETSC_COMM_WORLD,"Darcy Permeability cut-off, min= %1.6e, max = %1.6e  \n", R->perm_lower_cutoff, R->perm_upper_cutoff);
  }
  // Attribute the global value to all the phase,
  // deviating from global value should be in the model definition
  // basically, plugin in Darcy is easy for model with constant everything
  
  perm = 1e-17; //  permeability for sr = sr0  and eta rock = eta0 
  phi  = 0.05;  // 5% of porosity at the surface
  ierr = PetscOptionsGetReal(PETSC_NULL,"-Darcy_char_permeability",&perm,0);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(PETSC_NULL,"-Darcy_surf_porosity",&phi,0);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"DARCY : Default  surface porosity = %1.2e - char permeability = %1.3e \n", phi,perm);
  for (p=0; p<MAX_PHASE; p++) {
    R->phi0[p] = phi;
    R->perm0[p] = perm;
  }
 
 PetscFunctionReturn(0);
}




#undef __FUNCT__
#define __FUNCT__ "PhysComp_DarcyEquationCreate1"
PetscErrorCode PhysComp_DarcyEquationCreate1(PhysCompDarcyCtx *p)
{
	PhysCompDarcyCtx phys;
	PetscErrorCode ierr;

	PetscFunctionBegin;
	ierr = PetscMalloc(sizeof(struct _p_PhysCompDarcyCtx),&phys);CHKERRQ(ierr);
	ierr = PetscMemzero(phys,sizeof(struct _p_PhysCompDarcyCtx));CHKERRQ(ierr);
	*p = phys;
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PhysComp_DarcyEquationCreate2"
PetscErrorCode PhysComp_DarcyEquationCreate2(DM davq2,PhysCompDarcyCtx *p)
{
	DMDAE dae;
	PhysCompDarcyCtx phys;
	DM dmq1;
	PetscInt ncells;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;

	/* create structure */
	ierr = PhysComp_DarcyEquationCreate1(&phys);CHKERRQ(ierr);
	
	ierr = DMDACreateOverlappingQ1FromQ2(davq2,&dmq1);CHKERRQ(ierr);

	phys->daPf = dmq1;
	ierr = DMGetDMDAE(phys->daPf,&dae);CHKERRQ(ierr);
	phys->mx = dae->mx;
	phys->my = dae->my;
	
	ierr = DMCreateGlobalVector(dmq1,&phys->Pf);CHKERRQ(ierr);
	ierr = DMCreateGlobalVector(dmq1,&phys->Pfold);CHKERRQ(ierr);
	
	ncells = dae->lmx * dae->lmy;
	ierr = QuadratureVolumeDarcyCreate(QRule_GaussLegendre2,ncells,&phys->Q);CHKERRQ(ierr);

	ierr = DMDABCListCreate(phys->daPf,&phys->Pf_bclist);CHKERRQ(ierr);
	

	
	*p = phys;
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PhysComp_DarcyEquationDestroy"
PetscErrorCode PhysComp_DarcyEquationDestroy(PhysCompDarcyCtx *p)
{
	PhysCompDarcyCtx phys;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	if (!p) { PetscFunctionReturn(0); }
	phys = *p;
	
	
	ierr = DMDestroyDMDAE(phys->daPf);CHKERRQ(ierr);
	
	ierr = DMDestroy(&phys->daPf);CHKERRQ(ierr);
	ierr = VecDestroy(&phys->Pf);CHKERRQ(ierr);
	
	
	ierr = QuadratureVolumeDarcyDestroy(&phys->Q);CHKERRQ(ierr);
	ierr = BCListDestroy(&phys->Pf_bclist);CHKERRQ(ierr);
	
	ierr = VecDestroy(&phys->Pfold);CHKERRQ(ierr);
	
	
	ierr = PetscFree(phys);CHKERRQ(ierr);
	
	*p = PETSC_NULL;
	
  PetscFunctionReturn(0);
}



#undef __FUNCT__
#define __FUNCT__ "PhysCompView_DarcyEquation"
PetscErrorCode PhysCompView_DarcyEquation(PhysCompDarcyCtx phys,Vec X,const char fname[])
{
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	ierr = DMDAViewPetscVTK(phys->daPf,X,fname);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatinLoadPhysics_DarcyEquation"
PetscErrorCode pTatinLoadPhysics_DarcyEquation(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	
	
	PetscFunctionBegin;
	
	ierr = PhysComp_DarcyEquationCreate2(ctx->dav,&ctx->phys_darcy);CHKERRQ(ierr);
	
  PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "FormJacobian_Darcy_qp"
PetscErrorCode FormJacobian_Darcy_qp(PetscReal time,Vec X,PetscReal dt,Mat *A,Mat *B,MatStructure *mstr,void *ctx)
{
  PhysCompDarcyCtx data = (PhysCompDarcyCtx)ctx;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
    DM          da,cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
    PetscInt    p,i,j;
	PetscScalar ADe[NODES_PER_EL_Q1_2D*NODES_PER_EL_Q1_2D],el_coords[NSD*NODES_PER_EL_Q1_2D],el_V[NODES_PER_EL_Q1_2D*NSD];
	PetscScalar gp_kappa[27];
	/**/
	PetscInt nel,nen,e,n;
	const PetscInt *elnidx;
	BCList bclist;
	PetscInt nxg,Len_local, *gidx_bclocal;
	PetscInt *gidx,elgidx[NODES_PER_EL_Q1_2D],elgidx_bc[NODES_PER_EL_Q1_2D];
	
    
	CoefficientsDarcyEq   *quadpoints;
	PetscErrorCode ierr;
	
	
  PetscFunctionBegin;
	da     = data->daPf;
	bclist = data->Pf_bclist;
	
	/* trash old entries */
  ierr = MatZeroEntries(*B);CHKERRQ(ierr);
	
	/* quadrature */
	ngp       = data->Q->ngp;
	gp_xi     = data->Q->xi;
	gp_weight = data->Q->weight;
	
  /* setup for coords */
  ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
  ierr = DMDAGetGhostedCoordinates(da,&gcoords);CHKERRQ(ierr);
  ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	
	/* stuff for eqnums */
	ierr = DMDAGetGlobalIndices(da,0,&gidx);CHKERRQ(ierr);
	ierr = BCListGetDofIdx(bclist,0,0,&Len_local,&gidx_bclocal);CHKERRQ(ierr);
	
	ierr = DMDAGetElementsQ1(da,&nel,&nen,&elnidx);CHKERRQ(ierr);
	for (e=0;e<nel;e++) {
		/* get coords for the element */
		ierr = DMDAGetElementCoordinatesQ1_2D(el_coords,(PetscInt*)&elnidx[nen*e],LA_gcoords);CHKERRQ(ierr);
		
		ierr = QuadratureVolumeDarcyGetCell(data->Q,e,&quadpoints);CHKERRQ(ierr);
		
		/* copy the diffusivity */
		for (n=0; n<ngp; n++) {
			gp_kappa[n] = quadpoints[n].diff;
		}
		
		/* initialise element stiffness matrix */
		ierr = PetscMemzero(ADe,sizeof(PetscScalar)*NODES_PER_EL_Q1_2D*NODES_PER_EL_Q1_2D);CHKERRQ(ierr);
		
		/* form element stiffness matrix */
		AElement_qp( ADe,dt,el_coords, gp_kappa, ngp,gp_xi,gp_weight );
		

		/* insert element matrix into global matrix */
		ierr = GetElementEqnIndicesScalarQ1((PetscInt*)&elnidx[nen*e],gidx,elgidx);CHKERRQ(ierr);
		ierr = GetElementEqnIndicesScalarQ1((PetscInt*)&elnidx[nen*e],gidx_bclocal,elgidx_bc);CHKERRQ(ierr);
		
		ierr = MatSetValues(*B,NODES_PER_EL_Q1_2D,elgidx_bc, NODES_PER_EL_Q1_2D,elgidx_bc, ADe, ADD_VALUES );CHKERRQ(ierr);
  }
	/* tidy up */
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	
	
	/* partial assembly */
	ierr = MatAssemblyBegin(*B, MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(*B, MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
	
	/* boundary conditions */
	{
		PetscInt Len_ghost,*gidx_bcghost;
		PetscBool is_dirich;
		PetscInt nproc,*procs,*mapL,**mapIdx;
		ISLocalToGlobalMapping map;
		PetscMPIInt rank;
		
		MPI_Comm_rank( ((PetscObject)da)->comm,&rank);
		ierr = BCListGetDofIdx(bclist,0,0,&Len_ghost,&gidx_bcghost);CHKERRQ(ierr);
		
		ierr = DMGetLocalToGlobalMapping(da,&map);CHKERRQ(ierr);
		ierr = ISLocalToGlobalMappingGetInfo(map,&nproc,&procs,&mapL,&mapIdx);CHKERRQ(ierr);
		
		if (Len_ghost!=mapL[rank]) {
			//			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"map indices are fucked up");
		}
		for (i=0; i<Len_ghost; i++) {
			ierr = BCListIsDirichlet(gidx_bcghost[i],&is_dirich);CHKERRQ(ierr);
			if (is_dirich) {
				ierr = MatSetValueLocal(*B,i,i,1.0,INSERT_VALUES);CHKERRQ(ierr);
			}
		}
	}
	
	/* assemble */
	ierr = MatAssemblyBegin(*B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(*B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    if (*A != *B) {
    ierr = MatAssemblyBegin(*A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(*A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }
	*mstr = SAME_NONZERO_PATTERN;
	
  PetscFunctionReturn(0);
}



#undef __FUNCT__  
#define __FUNCT__ "FormFunction_Darcy_Pf_qp"
PetscErrorCode FormFunction_Darcy_Pf_qp(
						PhysCompDarcyCtx data,
						PetscReal dt,
						DM da,
						PetscScalar *LA_phi,
						PetscScalar *LA_philast,
						PetscScalar *LA_R)
{
  Vec kappa;
  DM                     cda;
  Vec                    gcoords;
  PetscScalar            *LA_gcoords;
  MatStencil             phi_eqn[NODES_PER_EL_Q1_2D];
  PetscInt               sex,sey,mx,my;
  PetscInt               ei,ej;
  PetscScalar            Re[NODES_PER_EL_Q1_2D];
  PetscScalar            el_coords[NODES_PER_EL_Q1_2D*NSD],el_V[NODES_PER_EL_Q1_2D*NSD],el_phi[NODES_PER_EL_Q1_2D],el_philast[NODES_PER_EL_Q1_2D];
	PetscScalar gp_kappa[27];
	PetscScalar gp_Q[27];
	PetscInt nel,nen,e,n;
	const PetscInt *elnidx;
	
  PetscInt    ngp;
  PetscScalar *gp_xi;
  PetscScalar *gp_weight;
  PetscScalar Ni_p[NODES_PER_EL_Q1_2D];
  PetscScalar GNi_p[NSD][NODES_PER_EL_Q1_2D],GNx_p[NSD][NODES_PER_EL_Q1_2D];
	PetscInt phi_el_lidx[NODES_PER_EL_Q1_2D];
  PetscScalar J_p,fac;
  PetscScalar phi_p;
  PetscInt    p,i;
	PetscReal   cg,c = 0.0;
	CoefficientsDarcyEq   *quadpoints;
  PetscErrorCode         ierr;
	
  PetscFunctionBegin;
	
	/* quadrature */
	ngp       = data->Q->ngp;
	gp_xi     = data->Q->xi;
	gp_weight = data->Q->weight;
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(da,&gcoords);CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);

	ierr = DMDAGetElementsQ1(da,&nel,&nen,&elnidx);CHKERRQ(ierr);
	for (e=0;e<nel;e++) {
		
		ierr = GetElementLocalIndicesQ1(phi_el_lidx,(PetscInt*)&elnidx[nen*e]);CHKERRQ(ierr);
		
		/* get coords for the element */
		ierr = DMDAGetElementCoordinatesQ1_2D(el_coords,(PetscInt*)&elnidx[nen*e],LA_gcoords);CHKERRQ(ierr);
		
		ierr = DMDAGetScalarElementFieldQ1_2D(el_phi,(PetscInt*)&elnidx[nen*e],LA_phi);CHKERRQ(ierr);
		ierr = DMDAGetScalarElementFieldQ1_2D(el_philast,(PetscInt*)&elnidx[nen*e],LA_philast);CHKERRQ(ierr);
		
		ierr = QuadratureVolumeDarcyGetCell(data->Q,e,&quadpoints);CHKERRQ(ierr);
		
		/* copy the diffusivity and force */
		for (n=0; n<ngp; n++) {
			gp_kappa[n] = quadpoints[n].diff;
			gp_Q[n]     = quadpoints[n].source;
		}

		/* initialise element stiffness matrix */
		ierr = PetscMemzero(Re,sizeof(PetscScalar)*NODES_PER_EL_Q1_2D);CHKERRQ(ierr);
		
		/* form element stiffness matrix */
		FormMass_FormResidual_qp(Re,dt,el_coords,el_phi,el_philast,gp_kappa,gp_Q,ngp,gp_xi,gp_weight);
		
		ierr = DMDASetValuesLocalStencil_AddValues_Scalar_Q1(LA_R,phi_el_lidx,Re);CHKERRQ(ierr);

	
		/* diagnostics */
		for (p=0; p<ngp; p++) {
			ConstructNi_Q1_2D(&gp_xi[2*p],Ni_p);
			ConstructGNi_Q1_2D(&gp_xi[2*p],GNi_p);
			ConstructGNx_Q1_2D(GNi_p,GNx_p,el_coords,&J_p);
			
			phi_p = 0.0;
			for (i=0; i<NODES_PER_EL_Q1_2D; i++) {
				phi_p = phi_p + Ni_p[i] * el_philast[i];
			}
			
			fac = gp_weight[p]*J_p;
			c = c + phi_p * fac;
		}
		
	}
  ierr = MPI_Allreduce(&c,&cg,1,MPIU_REAL,MPI_MIN,((PetscObject)da)->comm);CHKERRQ(ierr);
	PetscPrintf(PETSC_COMM_WORLD,"\\int \\phi dV = %1.12e \n", cg );
	
  /* tidy up local arrays (input) */
  ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
  PetscFunctionReturn(0);
}

/*
 Computes f - C.phi - M.phiDot
*/
#undef __FUNCT__  
#define __FUNCT__ "FormFunction_Darcy_qp"
PetscErrorCode FormFunction_Darcy_qp(PetscReal time,Vec X,PetscReal dt,Vec Pf,void *ctx)
{
  PhysCompDarcyCtx data  = (PhysCompDarcyCtx)ctx;
  DM             da,cda;
  Vec            philoc, philastloc, Pfphiloc;
  PetscScalar    *LA_philoc, *LA_philastloc, *LA_Pfphiloc;
	
  PetscErrorCode ierr;
	
  PetscFunctionBegin;
	da = data->daPf;
	
	
	ierr = VecZeroEntries(Pf);CHKERRQ(ierr);
	
  ierr = DMGetLocalVector(da,&philoc);CHKERRQ(ierr);
  ierr = DMGetLocalVector(da,&philastloc);CHKERRQ(ierr);
	
  ierr = DMGetLocalVector(da,&Pfphiloc);CHKERRQ(ierr);
	
	/* get local solution and time derivative */
	ierr = VecZeroEntries(philoc);CHKERRQ(ierr);
	ierr = DMGlobalToLocalBegin(da,X,ADD_VALUES,philoc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd  (da,X,ADD_VALUES,philoc);CHKERRQ(ierr);
	
	ierr = VecZeroEntries(philastloc);CHKERRQ(ierr);
	ierr = DMGlobalToLocalBegin(da,data->Pfold,ADD_VALUES,philastloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd  (da,data->Pfold,ADD_VALUES,philastloc);CHKERRQ(ierr);
	
	/* insert boundary conditions into local vectors */
	ierr = BCListInsertLocal(data->Pf_bclist,philoc);CHKERRQ(ierr);
	
	/* what do i do with the phi_dot */
	
	/* init residual */
	ierr = VecZeroEntries(Pfphiloc);CHKERRQ(ierr);
	
	/* get arrays */
	ierr = VecGetArray(philoc,    &LA_philoc);CHKERRQ(ierr);
	ierr = VecGetArray(philastloc,&LA_philastloc);CHKERRQ(ierr);
	ierr = VecGetArray(Pfphiloc,   &LA_Pfphiloc);CHKERRQ(ierr);
	
	/* ============= */
	/* FORM_FUNCTION */
	
	
	ierr = FormFunction_Darcy_Pf_qp(data,dt,da, LA_philoc,LA_philastloc,LA_Pfphiloc);CHKERRQ(ierr);
	
    /* ============= */
	
	ierr = VecRestoreArray(Pfphiloc,   &LA_Pfphiloc);CHKERRQ(ierr);
	ierr = VecRestoreArray(philastloc,&LA_philastloc);CHKERRQ(ierr);
	ierr = VecRestoreArray(philoc,    &LA_philoc);CHKERRQ(ierr);
	
	
	/* do global fem summation */
	ierr = VecZeroEntries(Pf);CHKERRQ(ierr);
	
  ierr = DMLocalToGlobalBegin(da,Pfphiloc,ADD_VALUES,Pf);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd  (da,Pfphiloc,ADD_VALUES,Pf);CHKERRQ(ierr);
	
  ierr = DMRestoreLocalVector(da,&Pfphiloc);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da,&philoc);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da,&philastloc);CHKERRQ(ierr);
	
	/* modify Pf for the boundary conditions, F_k = scale_k(x_k - phi_k) */
	ierr = BCListResidualDirichlet(data->Pf_bclist,X,Pf);CHKERRQ(ierr);
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatinOutputMeshFluidPressurePVTS"
PetscErrorCode pTatinOutputMeshFluidPressurePVTS(pTatinCtx ctx,const char prefix[],const char name[])
{
	PetscErrorCode ierr;
	PetscInt mx,my,cnt;
	PetscInt ei,ej,i,j,esi,esj;
  PetscScalar *LA_cell_data;
	FILE*	vtk_fp = NULL;
	PetscInt M,N,P,swidth;
	PetscMPIInt rank;
	DM da;
	
	
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	vtk_fp = NULL;
	if (rank==0) {
		if ((vtk_fp = fopen ( name, "w")) == NULL)  {
			SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
		}
	}
	
	
	da = ctx->phys_darcy->daPf;
	
	/* VTS HEADER - OPEN */	
	if(vtk_fp) fprintf( vtk_fp, "<?xml version=\"1.0\"?>\n");
	if(vtk_fp) fprintf( vtk_fp, "<VTKFile type=\"PStructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
	
	DMDAGetInfo( da, 0, &M,&N,&P, 0,0,0, 0,&swidth, 0,0,0, 0 );
	if(vtk_fp) fprintf( vtk_fp, "  <PStructuredGrid GhostLevel=\"%d\" WholeExtent=\"%d %d %d %d %d %d\">\n", swidth, 0,M-1, 0,N-1, 0,P-1 ); /* note overlap = 1 for Q1 */
	//	fprintf( vtk_fp, "    <Piece Extent=\"%d %d %d %d %d %d\">\n", esi,esi+2*mx+1-1, esj,esj+2*my+1-1, 0,0);
	
	/* VTS COORD DATA */	
	if(vtk_fp) fprintf( vtk_fp, "    <PPoints>\n");
	if(vtk_fp) fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\"/>\n");
	if(vtk_fp) fprintf( vtk_fp, "    </PPoints>\n");
	
	
	/* VTS CELL DATA */	
	if(vtk_fp) fprintf( vtk_fp, "    <PCellData>\n");
	if(vtk_fp) fprintf( vtk_fp, "    </PCellData>\n");
	
	
	/* VTS NODAL DATA */
	if(vtk_fp) fprintf( vtk_fp, "    <PPointData>\n");
	if(vtk_fp) fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"fluid pressure\" NumberOfComponents=\"1\"/>\n");
	if(vtk_fp) fprintf( vtk_fp, "    </PPointData>\n");
	
	/* write out the parallel information */
	//DAViewVTK_write_PieceExtend(vtk_fp,2,dau,prefix);
	ierr = DAQ1PieceExtendForGhostLevelZero(vtk_fp,2,da,prefix);CHKERRQ(ierr);
	
	
	/* VTS HEADER - CLOSE */	
	if(vtk_fp) fprintf( vtk_fp, "  </PStructuredGrid>\n");
	if(vtk_fp) fprintf( vtk_fp, "</VTKFile>\n");
	
	if(vtk_fp) fclose( vtk_fp );
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatinOutputParaViewMeshFluidPressure"
PetscErrorCode pTatinOutputParaViewMeshFluidPressure(pTatinCtx user,Vec X,const char path[],const char prefix[])
{
	char *vtkfilename,*filename;
	PetscMPIInt rank;
	PetscBool use_dimensional_units = PETSC_FALSE;
	pTatinUnits *units;
	double L_bar,P_bar;
	Vec coords;
	DM daPf;
	PetscErrorCode ierr;
	
	ierr = PetscOptionsGetBool(PETSC_NULL,"-use_dimensional_units",&use_dimensional_units,PETSC_NULL);CHKERRQ(ierr);

	ierr = pTatinGenerateParallelVTKName(prefix,"vts",&vtkfilename);CHKERRQ(ierr);
	if (path) {
		asprintf(&filename,"%s/%s",path,vtkfilename);
	} else {
		asprintf(&filename,"./%s",vtkfilename);
	}

	daPf = user->phys_darcy->daPf;
	
	/* apply scaling */
	if (use_dimensional_units) {
		units = &user->units;
		/* Get length scale */
		L_bar = units->si_length->characteristic_scale;
        P_bar = units->si_stress->characteristic_scale;
		/* scale coordinates */
		ierr = DMDAGetGhostedCoordinates(daPf,&coords);CHKERRQ(ierr);
		//UnitsApplyScalingToArray(units->si_length,(int)N,(double*)LA_fields,(double*)LA_fields);
		ierr = VecScale(coords,L_bar);CHKERRQ(ierr);
		ierr = DMDAGetCoordinates(daPf,&coords);CHKERRQ(ierr);
		ierr = VecScale(coords,L_bar);CHKERRQ(ierr);
		ierr = VecScale(X,P_bar);CHKERRQ(ierr);
		
		
	}

	/* write out file */
	ierr = pTatinOutputQ1Mesh2dNodalFieldVTS(daPf,1,X,filename,"fluid pressure");CHKERRQ(ierr);

	/* apply inverse scaling */
	if (use_dimensional_units) {
		units = &user->units;
		/* Get length scale */
		L_bar = units->si_length->characteristic_scale;
        P_bar = units->si_stress->characteristic_scale;
		/* unscale coordinates */
		ierr = DMDAGetGhostedCoordinates(daPf,&coords);CHKERRQ(ierr);
		//UnitsApplyInverseScalingToArray(units->si_length,(int)N,(double*)LA_fields,(double*)LA_fields);
		ierr = VecScale(coords,1.0/L_bar);CHKERRQ(ierr);
		ierr = DMDAGetCoordinates(daPf,&coords);CHKERRQ(ierr);
		ierr = VecScale(coords,1.0/L_bar);CHKERRQ(ierr);
		ierr = VecScale(X,1.0/P_bar);CHKERRQ(ierr);
	}
	
	free(filename);
	free(vtkfilename);
	
	ierr = pTatinGenerateVTKName(prefix,"pvts",&vtkfilename);CHKERRQ(ierr);
	if (path) {
		asprintf(&filename,"%s/%s",path,vtkfilename);
	} else {
		asprintf(&filename,"./%s",vtkfilename);
	}
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	//	if (rank==0) {
	ierr = pTatinOutputMeshFluidPressurePVTS(user,prefix,filename);CHKERRQ(ierr);
	//	}
	free(filename);
	free(vtkfilename);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatinOutputQuadratureDarcyAveragedFieldsVTS"
PetscErrorCode pTatinOutputQuadratureDarcyAveragedFieldsVTS(PhysCompDarcyCtx ctx,const char filename[])
{
	PetscErrorCode ierr;
	DMDAE dmdae;
	DM da,cda;
	Vec gcoords;
	DMDACoor2d **LA_gcoords;	
	PetscInt mx,my,cnt,d,p,nqp,e;
	PetscInt ei,ej,i,j,esi,esj;
	FILE*	vtk_fp = NULL;
  PetscScalar *LA_cell_data_diff,*LA_cell_data_source;
	PetscInt gsi,gsj,gm,gn;
	CoefficientsDarcyEq *quadpoints;
	
	if ((vtk_fp = fopen ( filename, "w")) == NULL)  {
		SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",filename );
	}
	
	da = ctx->daPf;
	ierr = DMGetDMDAE(da,&dmdae);CHKERRQ(ierr);
	
	ierr = DMDAGetGhostCorners(da,&gsi,&gsj,0,&gm,&gn,0);CHKERRQ(ierr);
	
	//	ierr = DMDAGetCornersElementQ2(da,&esi,&esj,0,&mx,&my,0);CHKERRQ(ierr);
	esi = dmdae->si;
	esj = dmdae->sj;
	mx = dmdae->lmx;
	my = dmdae->lmy;
	
	ierr = PetscMalloc(sizeof(PetscScalar)*mx*my,&LA_cell_data_diff);CHKERRQ(ierr);
	ierr = PetscMalloc(sizeof(PetscScalar)*mx*my,&LA_cell_data_source);CHKERRQ(ierr);
	
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(da,&gcoords);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	/* VTS HEADER - OPEN */	
	fprintf( vtk_fp, "<VTKFile type=\"StructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
	fprintf( vtk_fp, "  <StructuredGrid WholeExtent=\"%d %d %d %d %d %d\">\n", esi,esi+mx+1-1, esj,esj+my+1-1, 0,0);
	fprintf( vtk_fp, "    <Piece Extent=\"%d %d %d %d %d %d\">\n", esi,esi+mx+1-1, esj,esj+my+1-1, 0,0);
	
	/* VTS COORD DATA */	
	fprintf( vtk_fp, "    <Points>\n");
	fprintf( vtk_fp, "      <DataArray Name=\"coords\" type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	for (j=esj; j<esj+my+1; j++) {
		for (i=esi; i<esi+mx+1; i++) {
			fprintf( vtk_fp,"      %1.6e %1.6e %1.6e\n", LA_gcoords[j][i].x, LA_gcoords[j][i].y, 0.0 );
		}
	}
	fprintf( vtk_fp, "      </DataArray>\n");
	fprintf( vtk_fp, "    </Points>\n");
	
	/* VTS CELL DATA */	
	fprintf( vtk_fp, "    <CellData>\n");
	
	/* get quad point information */
	nqp = ctx->Q->ngp;
	
	/* =============================================================================== */
	/* integrated diffusivty */
	ierr = PetscMemzero(LA_cell_data_diff,sizeof(PetscScalar)*mx*my);CHKERRQ(ierr);

    /* integrated source */
	ierr = PetscMemzero(LA_cell_data_source,sizeof(PetscScalar)*mx*my);CHKERRQ(ierr);

	for (e=0; e<mx*my; e++) {
		ierr = QuadratureVolumeDarcyGetCell(ctx->Q,e,&quadpoints);CHKERRQ(ierr);
		
		for (p=0; p<nqp; p++) {
			LA_cell_data_diff[e]   += quadpoints[p].diff;
			LA_cell_data_source[e] += quadpoints[p].source;
		}
		LA_cell_data_diff[e] = LA_cell_data_diff[e] / (double)(nqp);
		LA_cell_data_source[e] = LA_cell_data_source[e] / (double)(nqp);
	}
	
	fprintf( vtk_fp, "      <DataArray Name=\"avg_diff\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	fprintf( vtk_fp,"      ");
	for (ej=0; ej<my; ej++) { 
		for (ei=0; ei<mx; ei++) {
			fprintf( vtk_fp,"%1.6e ", LA_cell_data_diff[ei+ej*mx] );
		}
	}
	fprintf( vtk_fp,"\n");
	fprintf( vtk_fp, "      </DataArray>\n");
	
	
	fprintf( vtk_fp, "      <DataArray Name=\"avg_source\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	fprintf( vtk_fp,"      ");
	for (ej=0; ej<my; ej++) { 
		for (ei=0; ei<mx; ei++) {
			fprintf( vtk_fp,"%1.6e ", LA_cell_data_source[ei+ej*mx] );
		}
	}
	fprintf( vtk_fp,"\n");
	fprintf( vtk_fp, "      </DataArray>\n");
	/* =============================================================================== */
	
	
	fprintf( vtk_fp, "    </CellData>\n");
	
	/* VTS NODAL DATA */
	fprintf( vtk_fp, "    <PointData>\n");
#if 0
	fprintf( vtk_fp, "      <DataArray Name=\"pressure (%s)\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	for (j=esj; j<esj+my+1; j++) {
		for (i=esi; i<esi+mx+1; i++) {
			double press;
			
			cnt = (i-gsi) + (j-gsj)*gm;
			press = (double)LA_fieldsP[2*cnt+0];
			
			UnitsApplyScaling(units->si_stress,press,&press);

			fprintf( vtk_fp,"      %1.6e \n", press);
		}
	}
	fprintf( vtk_fp, "      </DataArray>\n");
#endif
	
	fprintf( vtk_fp, "    </PointData>\n");
	
	/* VTS HEADER - CLOSE */	
	fprintf( vtk_fp, "    </Piece>\n");
	fprintf( vtk_fp, "  </StructuredGrid>\n");
	fprintf( vtk_fp, "</VTKFile>\n");
	
	ierr = DMDAVecRestoreArray(cda,gcoords,&LA_gcoords);CHKERRQ(ierr);
	//free(LA_cell_data_diff);
	//free(LA_cell_data_source);
	fclose( vtk_fp );
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatinOutputQuadratureDarcyAveragedFieldsPVTS"
PetscErrorCode pTatinOutputQuadratureDarcyAveragedFieldsPVTS(PhysCompDarcyCtx ctx,const char prefix[],const char name[])
{
	PetscErrorCode ierr;
	PetscInt mx,my,cnt;
	PetscInt ei,ej,i,j,esi,esj;
  PetscScalar *LA_cell_data;
	FILE*	vtk_fp = NULL;
	PetscInt M,N,P,swidth;
	PetscMPIInt rank;
	DM da;
	
	
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	vtk_fp = NULL;
	if (rank==0) {
		if ((vtk_fp = fopen ( name, "w")) == NULL)  {
			SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
		}
	}
	
	
	da = ctx->daPf;
	
	/* VTS HEADER - OPEN */	
	if(vtk_fp) fprintf( vtk_fp, "<?xml version=\"1.0\"?>\n");
	if(vtk_fp) fprintf( vtk_fp, "<VTKFile type=\"PStructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
	
	DMDAGetInfo( da, 0, &M,&N,&P, 0,0,0, 0,&swidth, 0,0,0, 0 );
	if(vtk_fp) fprintf( vtk_fp, "  <PStructuredGrid GhostLevel=\"%d\" WholeExtent=\"%d %d %d %d %d %d\">\n", 0, 0,M-1, 0,N-1, 0,P-1 ); /* note overlap = 1 for Q1 */
	
	/* VTS COORD DATA */	
	if(vtk_fp) fprintf( vtk_fp, "    <PPoints>\n");
	if(vtk_fp) fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\"/>\n");
	if(vtk_fp) fprintf( vtk_fp, "    </PPoints>\n");
	
	
	/* VTS CELL DATA */	
	if(vtk_fp) fprintf( vtk_fp, "    <PCellData>\n");
	if(vtk_fp) fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"avg_kappa\" NumberOfComponents=\"1\"/>\n");
	if(vtk_fp) fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"avg_H\" NumberOfComponents=\"1\"/>\n");
	if(vtk_fp) fprintf( vtk_fp, "    </PCellData>\n");
	
	
	/* VTS NODAL DATA */
	if(vtk_fp) fprintf( vtk_fp, "    <PPointData>\n");
	if(vtk_fp) fprintf( vtk_fp, "    </PPointData>\n");
	
	/* write out the parallel information */
	ierr = DAQ1PieceExtendForGhostLevelZero(vtk_fp,2,da,prefix);CHKERRQ(ierr);
	
	
	/* VTS HEADER - CLOSE */	
	if(vtk_fp) fprintf( vtk_fp, "  </PStructuredGrid>\n");
	if(vtk_fp) fprintf( vtk_fp, "</VTKFile>\n");
	
	if(vtk_fp) fclose( vtk_fp );
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatinParaViewOutputQuadratureDarcyAveragedFields"
PetscErrorCode pTatinParaViewOutputQuadratureDarcyAveragedFields(pTatinCtx user,const char path[],const char prefix[])
{
	char *vtkfilename,*filename;
	PetscMPIInt rank;
	
	PetscErrorCode ierr;
	
	ierr = pTatinGenerateParallelVTKName(prefix,"vts",&vtkfilename);CHKERRQ(ierr);
	if (path) {
		asprintf(&filename,"%s/%s",path,vtkfilename);
	} else {
		asprintf(&filename,"./%s",vtkfilename);
	}
	
	ierr = pTatinOutputQuadratureDarcyAveragedFieldsVTS(user->phys_darcy,filename);CHKERRQ(ierr);
	free(filename);
	free(vtkfilename);
	
	ierr = pTatinGenerateVTKName(prefix,"pvts",&vtkfilename);CHKERRQ(ierr);
	if (path) {
		asprintf(&filename,"%s/%s",path,vtkfilename);
	} else {
		asprintf(&filename,"./%s",vtkfilename);
	}
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	//	if (rank==0) {
	ierr = pTatinOutputQuadratureDarcyAveragedFieldsPVTS(user->phys_darcy,prefix,filename);CHKERRQ(ierr);
	//	}
	free(filename);
	free(vtkfilename);
	
	PetscFunctionReturn(0);
}




