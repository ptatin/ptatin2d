

#include <stdio.h>
#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"
#include "rheology.h"
#include "element_type_Q2.h"
#include "element_type_Q1.h"


#undef __FUNCT__
#define __FUNCT__ "SurfaceQuadratureStokesComputeStress"
PetscErrorCode SurfaceQuadratureStokesComputeStress(pTatinCtx user,SurfaceQuadratureStokes Q,DM dau,PetscScalar LA_u[],DM dap,PetscScalar LA_p[])
{
  SurfaceRheologyConstants *rheo;
	QPoint2d *qdata;
	double xi_p[2];
	PetscScalar *normal,*tangent,tangent_aligned[2];
	PetscScalar sigma[2][2];
	PetscScalar vel_p[2];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nen_u,nen_p,nel,i,j,k,fe,qp,e;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar elu[2*Q2_NODES_PER_EL_2D];
	PetscScalar elp[P_BASIS_FUNCTIONS];
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	PetscScalar GNIu[2][Q2_NODES_PER_EL_2D];
	PetscScalar NIp[P_BASIS_FUNCTIONS];
	PetscScalar NIu[U_BASIS_FUNCTIONS];
	SurfaceStressType type;
	static int been_here = 0;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
  rheo = &user->surf_rheology_constants;
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA(dau,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(dau,&gcoords);CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	/* stokes */
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
	
	
	for (fe=0; fe<Q->nfaces; fe++) {
		SurfaceQPointCoefficientsStokes *point;

		/* element index */
		e = Q->cell_list[fe];
		/* coordinates*/
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		/* velocity*/
		ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],LA_u);CHKERRQ(ierr);
		/*pressure*/
		ierr = DMDAGetScalarElementField_2D(elp,nen_p,(PetscInt*)&elnidx_p[nen_p*e],LA_p);CHKERRQ(ierr);
		
		
		ierr = SurfaceQuadratureStokesGetCell(Q,fe,&point);CHKERRQ(ierr);
		for (qp=0; qp<Q->ngp; qp++) {

			/* compute stress */
			type = rheo->stress_type[ point[qp].phase ];
			normal  = point[qp].normal;
			tangent = point[qp].tangent;

			tangent_aligned[0] = tangent[0];
			tangent_aligned[1] = tangent[1];
			if (Q->edge_id == QUAD_EDGE_Neta) {
				tangent_aligned[0] = -tangent[0];
			}
			if (Q->edge_id == QUAD_EDGE_Pxi) {
				tangent_aligned[1] = -tangent[1];
			}
			
			sigma[0][0] = 0.0;
			sigma[1][1] = 0.0;
			sigma[0][1] = 0.0;
			sigma[1][0] = sigma[0][1];

			
			switch (type) {
				case STRESS_NONE:
				break;
				
				case STRESS_NULL:
				{
					
					if (been_here==0) {
						PetscPrintf(PETSC_COMM_WORLD,"*** Applying NonlinearUpdate(surface) for STRESS_NULL ***\n");
						been_here = 1;
					}
					sigma[0][0] = 0.0;
					sigma[1][1] = 0.0;
					sigma[0][1] = 0.0;
					sigma[1][0] = sigma[0][1];
					
					/* set value */
					point[qp].traction[0] = sigma[0][0] * normal[0] + sigma[0][1] * normal[1];
					point[qp].traction[1] = sigma[1][0] * normal[0] + sigma[1][1] * normal[1];					
				}
					break;
					
				case STRESS_VOLUMETRIC_VISCOSITY:
				{
					PetscScalar J[2][2], iJ[2][2];
					PetscScalar J_p,ojp;
					PetscScalar exx_qp,eyy_qp,exy_qp,pressure_qp,eta_qp;
					PetscScalar v_dot_t,T[2],stress[2][2],sigma_n,abstau,tau;
					
					if (been_here==0) {
						PetscPrintf(PETSC_COMM_WORLD,"*** WARNING NonlinearUpdate(surface) for STRESS_VOLUMETRIC_VISCOSITY under development ***\n");
						been_here = 1;
					}

					qdata = &Q->gp2[qp];
					xi_p[0] = qdata->xi;
					xi_p[1] = qdata->eta;

					
					/* interpolate pressure, strain rate at quadrature point */
					PTatinConstructNI_Q2_2D(xi_p,NIu);
					PTatinConstructGNI_Q2_2D(xi_p,GNIu);
					
					/* get element velocity/coordinates in component form */
					for (k=0; k<Q2_NODES_PER_EL_2D; k++) {
						ux[k] = elu[2*k  ];
						uy[k] = elu[2*k+1];
						
						xc[k] = elcoords[2*k  ];
						yc[k] = elcoords[2*k+1];
					}
					
					/* coord transformation */
					for (i=0; i<2; i++) {
						for (j=0; j<2; j++) { J[i][j] = 0.0; }
					}
					for (k=0; k<Q2_NODES_PER_EL_2D; k++) {
						J[0][0] += GNIu[0][k] * xc[k] ;
						J[0][1] += GNIu[0][k] * yc[k] ;
						
						J[1][0] += GNIu[1][k] * xc[k] ;
						J[1][1] += GNIu[1][k] * yc[k] ;
					}
					J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
					ojp = 1.0/J_p;
					iJ[0][0] =  J[1][1]*ojp;
					iJ[0][1] = -J[0][1]*ojp;
					iJ[1][0] = -J[1][0]*ojp;
					iJ[1][1] =  J[0][0]*ojp;
					
					/* global derivs at each quadrature point */
					for (k=0; k<Q2_NODES_PER_EL_2D; k++) {
						nx[k] = iJ[0][0]*GNIu[0][k] + iJ[0][1]*GNIu[1][k];
						ny[k] = iJ[1][0]*GNIu[0][k] + iJ[1][1]*GNIu[1][k];
					}
					
					/* strain rate (e= B u) at qp */
					exx_qp = eyy_qp = exy_qp = 0.0;
					for (k=0; k<U_BASIS_FUNCTIONS; k++) {
						exx_qp += nx[k] * ux[k];
						eyy_qp += ny[k] * uy[k];
						exy_qp += ny[k] * ux[k] + nx[k] * uy[k];
					}
					exy_qp = 0.5 * exy_qp;

					/* pressure */
					ConstructNi_pressure(xi_p,elcoords,NIp);
					pressure_qp = 0.0;
					for (k=0; k<P_BASIS_FUNCTIONS; k++) {
						pressure_qp += NIp[k] * elp[k];
					}
					
					/* velocity */
					vel_p[0] = vel_p[1] = 0.0;
					for (k=0; k<Q2_NODES_PER_EL_2D; k++) {
						vel_p[0] += NIu[k] * ux[k];
						vel_p[1] += NIu[k] * uy[k];
					}
					
					/* stess */
					eta_qp = point[qp].effective_eta;
					
					//
					stress[0][0] = 2.0 * eta_qp * exx_qp - pressure_qp;
					stress[1][1] = 2.0 * eta_qp * eyy_qp - pressure_qp;
					stress[0][1] = 2.0 * eta_qp * exy_qp;
					stress[1][0] = stress[0][1];
					
					// compute tangential velocity
					
					v_dot_t = vel_p[0]*tangent_aligned[0] + vel_p[1]*tangent_aligned[1];
            
					T[0] = stress[0][0] * normal[0] + stress[0][1] * normal[1];
					T[1] = stress[1][0] * normal[0] + stress[1][1] * normal[1];
					sigma_n = normal[0] * T[0] + normal[1] * T[1];
                    tau = tangent_aligned[0]* T[0] + tangent_aligned[1] * T[1]; 
#if 1                
{
PetscScalar abstau_pred;

                   	if (v_dot_t > 0.0) {
                            abstau = -tau;
                        } else {
                            abstau = tau;
                        }
                    abstau_pred = tan(15.0*M_PI/180.0) * sigma_n;
                    if ((point[qp].effective_rho > 1.e-30)){// && (fabs(v_dot_t) > 0.001)) { 
                       if (v_dot_t > 0.0) {
                            tau = abstau_pred ;
                       } else {
                          tau = -abstau_pred ;
                       }
                     }
                    point[qp].traction[0] = sigma_n * normal[0] + tau * tangent_aligned[0];
                    point[qp].traction[1] = sigma_n * normal[1] + tau * tangent_aligned[1];
}                 
#endif
    
#if 0
    {
					abstau = -tan(15.0*M_PI/180.0) * sigma_n;// +30.0/2.77777777777777777778e+06;
                    
                    if (v_dot_t > 0.0) {
						tau = -abstau;
					} else {
						tau = abstau;
					}
                     
                    if (abstau < 0.0 ){//|| fabs(v_dot_t) < 0.00001
                        tau = 0.0;
                    }
                    if (fabs(v_dot_t) > 0.001){
					point[qp].traction[0] = sigma_n * normal[0] + tau * tangent_aligned[0];
					point[qp].traction[1] = sigma_n * normal[1] + tau * tangent_aligned[1];
					}
					/*
					if (v_dot_t > 0.0) {
						point[qp].traction[0] = - fabs( point[qp].traction[0] );
						point[qp].traction[1] = - fabs( point[qp].traction[1] );
					} else {
						point[qp].traction[0] = fabs( point[qp].traction[0] );
						point[qp].traction[1] = fabs( point[qp].traction[1] );
					}
					*/
					//
					/* to remove stress bc */
					/*
					 sigma[0][0] = 0.0;
					 sigma[1][1] = 0.0;
					 sigma[0][1] = 0.0;
					 sigma[1][0] = sigma[0][1];
					 */
					/*
					{
						PetscScalar t_dot_stress_dot_n,tau_b;
						
						stress_dot_n[0] = stress[0][0] * (-normal[0]) + stress[0][1] * (-normal[1]);
						stress_dot_n[1] = stress[1][0] * (-normal[0]) + stress[1][1] * (-normal[1]);
						
						t_dot_stress_dot_n = tangent[0] * stress_dot_n[0] + tangent[1] * stress_dot_n[1];
					
						tau_b = 1.0e-2 * v_dot_t;
						
						point[qp].traction[0] = 0.0 * (normal[0])   + tau_b * (normal[1]);
						point[qp].traction[1] = tau_b * (normal[0]) + 0.0 * (normal[1]);

						if (v_dot_t > 0.0) {
							point[qp].traction[0] = - fabs( point[qp].traction[0] );
							point[qp].traction[1] = - fabs( point[qp].traction[1] );
						} else {
							point[qp].traction[0] = fabs( point[qp].traction[0] );
							point[qp].traction[1] = fabs( point[qp].traction[1] );
						}
						//
						//if (Q->edge_id == QUAD_EDGE_Neta) {
						//	printf("  tx,ty    = %+1.4e %+1.4e \n",point[qp].traction[0],point[qp].traction[1]);
						//}
						
					}
					*/
					
					/* set value */
					//point[qp].traction[0] = 0.0;
					//point[qp].traction[1] = 0.0;
					
					/*
					if (Q->edge_id == QUAD_EDGE_Neta) {
						
						printf("[faceid = %d] : xp[] = %1.4e , %1.4e : vel[] = %1.4e , %1.4e : eta = %1.4e : pressure = %1.4e \n",
									 Q->edge_id, point[qp].coord[0],point[qp].coord[1], vel_p[0],vel_p[1],eta_qp,pressure_qp);
						
						printf("  v_dot_t = %+1.4e \n",v_dot_t);
						printf("  n_dot_stress_dot_n = %+1.4e \n",n_dot_stress_dot_n);
						printf("  stress   = %+1.4e %+1.4e \n",stress[0][0],stress[0][1]);
						printf("             %+1.4e %+1.4e \n",stress[1][0],stress[1][1]);
						printf("  stress.n = %+1.4e %+1.4e \n",stress[0][0] * normal[0] + stress[0][1] * normal[1],stress[1][0] * normal[0] + stress[1][1] * normal[1]);
						printf("  sigma    = %+1.4e %+1.4e \n",sigma[0][0],sigma[0][1]);
						printf("             %+1.4e %+1.4e \n",sigma[1][0],sigma[1][1]);
						printf("  sigma.n  = %+1.4e %+1.4e \n",sigma[0][0] * normal[0] + sigma[0][1] * normal[1],sigma[1][0] * normal[0] + sigma[1][1] * normal[1]);
						printf("  tx,ty    = %+1.4e %+1.4e \n",point[qp].traction[0],point[qp].traction[1]);
					}
					*/ 
				}
#endif
}
					break;
#if 0				
				case WRINKLER:
				{
					PetscScalar J[2][2], iJ[2][2];
					PetscScalar J_p,ojp;
					PetscScalar exx_qp,eyy_qp,exy_qp,pressure_qp,eta_qp;
					PetscScalar v_dot_t,T[2],stress[2][2],sigma_n;
					PestcScalar P_ref,rho_u,y_ref;
					P_ref = -1.82e+009/1.e11;
					delta_rho = 300/1.e7;
					y_ref =-6.0;
	
					if (been_here==0) {
						PetscPrintf(PETSC_COMM_WORLD,"*** WARNING NonlinearUpdate(surface) for WRINKLER under development ***\n");
						been_here = 1;
					}
					/* coord of quad surf points  */
								
					qdata = &Q->gp2[qp];
					xi_p[0] = qdata->xi;
					xi_p[1] = qdata->eta;

					
					/* interpolate pressure, strain rate at quadrature point */
					PTatinConstructNI_Q2_2D(xi_p,NIu);
				
					for (k=0; k<U_BASIS_FUNCTIONS; k++) {
						y_qp += NIu[k] * elcoords[2*k+1];
						}
					
                    sigma_n = P_ref+rho_u*g*(y_ref-y_qp);
					point[qp].traction[0] = sigma_n * normal[0] ;
					point[qp].traction[1] = sigma_n * normal[1] ;
					
			}
					break;
#endif				
				case STRESS_DP:
				{
					
					if (been_here==0) {
						PetscPrintf(PETSC_COMM_WORLD,"*** WARNING NonlinearUpdate(surface) for STRESS_DP not implemented ***\n");
						been_here = 1;
					}
					SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"STRESS_DP under development");
				}
					break;
					
			}
			
			/* set value */
			//point[qp].traction[0] = sigma[0][0] * normal[0] + sigma[0][1] * normal[1];
			//point[qp].traction[1] = sigma[1][0] * normal[0] + sigma[1][1] * normal[1];
		}
	}
	
	
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	been_here = 1;
	
  PetscFunctionReturn(0);
}

