/*
 *  stokes_visco_plastic.h
 *  
 *
 *  Created by laetitia le pourhiet on 6/11/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef __pTatin_stokes_visco_plastic_h__
#define __pTatin_stokes_visco_plastic_h__

PetscErrorCode EvaluateRheologyNonlinearitiesMarkers_ViscoPlastic(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[]);
PetscErrorCode EvaluateRheologyNonlinearitiesMarkers_ViscoPlasticStrainWeakening(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[]);
PetscErrorCode EvaluateRheologyNonlinearitiesMarkers_ViscoPlasticY(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[]);
PetscErrorCode TkUpdateRheologyMarkerStokes_ViscoPlastic_plastic_strain(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[]);
PetscErrorCode pTatin_TkUpdateRheologyMarkerStokes_ViscoPlastic(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[]);

PetscErrorCode EvaluateRheologyNonlinearitiesQuadratureStokes_ViscoPlastic(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[]);
PetscErrorCode TkUpdateRheologyQuadratureStokes_ViscoPlastic_plastic_strain(pTatinCtx user,DM dau);
PetscErrorCode pTatin_TkUpdateRheologyQuadratureStokes_ViscoPlastic(pTatinCtx user,DM dau);


#endif
