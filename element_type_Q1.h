
#ifndef __element_type_Q1_h__
#define __element_type_Q1_h__

#define NODES_PER_EL_Q1_2D 4

void ConstructNi_Q1_2D(PetscScalar _xi[],PetscScalar Ni[]);
void ConstructGNi_Q1_2D(PetscScalar _xi[],PetscScalar GNi[][NODES_PER_EL_Q1_2D]);
void ConstructGNx_Q1_2D(PetscScalar GNi[][NODES_PER_EL_Q1_2D],PetscScalar GNx[][NODES_PER_EL_Q1_2D],PetscScalar coords[],PetscScalar *det_J);
void ConstructElementTransformation_Q1_2D(PetscScalar coords[],PetscScalar GNi[][NODES_PER_EL_Q1_2D],PetscScalar _J[][2],PetscScalar _invJ[][2]);

PetscErrorCode DMDACreateOverlappingQ1FromQ2(DM dmq2,DM *dmq1);
PetscErrorCode DMDAProjectCoordinatesQ2toQ1_2d(DM daq2,DM daq1);

PetscErrorCode DMDAGetElementsQ1(DM dm,PetscInt *nel,PetscInt *nen,const PetscInt *e[]);
PetscErrorCode DMDAGetElementCoordinatesQ1_2D(PetscScalar elcoords[],PetscInt elnid[],PetscScalar LA_gcoords[]);
PetscErrorCode DMDAGetScalarElementFieldQ1_2D(PetscScalar elfield[],PetscInt elnid[],PetscScalar LA_gfield[]);
PetscErrorCode DMDAGetVectorElementFieldQ1_2D(PetscScalar elfield[],PetscInt elnid[],PetscScalar LA_gfield[]);

PetscErrorCode GetElementEqnIndicesScalarQ1(PetscInt elnidx[],PetscInt globalindices[],PetscInt eldofidx[]);
PetscErrorCode GetElementEqnIndicesVectorQ1(PetscInt elnidx[],PetscInt globalindices[],PetscInt eldofidx[]);
PetscErrorCode GetElementLocalIndicesQ1(PetscInt el_localIndices[],PetscInt elnid[]);
PetscErrorCode DMDASetValuesLocalStencil_AddValues_Scalar_Q1(PetscScalar *fields_F,PetscInt u_eqn[],PetscScalar Fe_u[]);

#endif

