

#ifndef __daVMS_Q1_h__
#define __daVMS_Q1_h__

#include "pTatin2d.h"
#include "daSUPG_Q1.h"


PetscErrorCode VMSFormJacobian(PetscReal time,Vec X,PetscReal dt,Mat *A,Mat *B,MatStructure *mstr,void *ctx);
PetscErrorCode VMSFormFunction(PetscReal time,Vec X,PetscReal dt,Vec F,void *ctx);


#endif

