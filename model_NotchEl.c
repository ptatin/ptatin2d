/*
 
 Model Description:
 
 
 Input / command line parameters:
 
 
 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"

/* ================= Notch plasticity shearband test model ================= */

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelOutput_NotchEl"
PetscErrorCode pTatin2d_ModelOutput_NotchEl(pTatinCtx ctx,Vec X,const char prefix[])
{
	PetscErrorCode ierr;
	
	/*ierr = pTatin2d_ModelOutput_Default(ctx,X,prefix);CHKERRQ(ierr);*/
	
	if (prefix == PETSC_NULL) {
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,"qpoints");CHKERRQ(ierr);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,"vp");CHKERRQ(ierr);
		ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,"int_fields");CHKERRQ(ierr);
	} else { /* exanple of adding a different prefix to the file names - not sure this is useful */
		char *name;
		
		asprintf(&name,"%s_qpoints",prefix);
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
    
		asprintf(&name,"%s_vp",prefix);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
    
		asprintf(&name,"%s_int_fields",prefix);
		ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_NotchEl"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_NotchEl(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
  PetscReal  opts_Lx,opts_Ly;	
  PetscInt i,j,si,sj,m,n,M,N,ndof;
	DM cda;
	Vec coords;
	DMDACoor2d **LA_coords;	
  /*set the model size */ 
	opts_Lx   = 15.0;
	opts_Ly   = 4.0;
	ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_Lx",&opts_Lx,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_Ly",&opts_Ly,0);CHKERRQ(ierr);
  
	ierr = DMDASetUniformCoordinates(ctx->dav,0.0,opts_Lx, 0.0,opts_Ly, 0.0,0.0);CHKERRQ(ierr);
	
  
  
  ierr = DMDAGetInfo(ctx->dav,0, &M,&N,0, 0,0,0, &ndof,0, 0,0,0, 0);CHKERRQ(ierr);
	ierr = DMDAGetCorners(ctx->dav,&si,&sj,0,&m,&n,0);CHKERRQ(ierr);
	ierr = DMDAGetCoordinateDA(ctx->dav,&cda);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(ctx->dav,&coords);CHKERRQ(ierr);
	if (!coords) { SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Coordinates must be set"); }
	ierr = DMDAVecGetArray(cda,coords,&LA_coords);CHKERRQ(ierr);
  
	/* perturb the bottom */
	
#if 0			
    if (sj==0) {
			j = 0;
			for (i=si; i<si+m; i++) {
				LA_coords[j][i].y = LA_coords[j][i].y+0.05 * opts_Ly/(ctx->my)*sin(3.14*LA_coords[j][i].x/opts_Lx*5) ;
		}
    }
#endif  
#if 0	
	/* perturb the top */
	
		if (sj+n==N) {
			j = sj+n-1;
			for (i=si; i<si+m; i++) {
			LA_coords[j][i].y = LA_coords[j][i].y-0.05 * opts_Ly/(ctx->my)*sin(3.14*LA_coords[j][i].x/opts_Lx*5) ;			
			}
		}
#endif
  
  
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_NotchEl"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_NotchEl(pTatinCtx ctx)
{
	PetscInt e,ncells;
	PetscInt p,ngp;
	GaussPointCoefficientsStokes *gausspoints;
  PetscReal  opts_LayerVis,opts_LayerCo,opts_LayerPhi, opts_LayerTens,opts_LayerGamSoft,opts_LayerCoInf;
  PetscReal  opts_NotchVis,opts_NotchCo,opts_NotchPhi, opts_NotchTens,opts_NotchGamSoft,opts_NotchCoInf;
  PetscReal  opts_NotchMaxX,opts_NotchMinX,opts_NotchMaxY,opts_NotchMinY;
  PetscReal  opts_LayerG,opts_NotchG,d0, opts_LayerGrav,opts_NotchGrav;
  
  PetscInt   opts_Notchtype = 0;
  PetscReal gravity; 
	PetscErrorCode ierr;
	RheologyConstants *rheology;
  
	PetscFunctionBegin;
	
	
  /*First we fill up the model with one phase everywhere */ 
  /*set gravity */ 	
  gravity = 0.0;
  ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_gravity",&gravity,0);CHKERRQ(ierr);  
  
  /*set the Layer parameter */
  rheology  = &ctx->rheology_constants;
	rheology->rheology_type = RHEOLOGY_VISCO_ELASTIC_PLASTIC;
	
  opts_LayerG       = 1.0;
  opts_LayerVis     = 1.0;
	opts_LayerCo      = 1000000.0;
	opts_LayerPhi     = 0.0;
  opts_LayerGamSoft = 0.0; // means no softening
  opts_LayerGrav    = gravity; 
  
  ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_LayerG",&opts_LayerG,0);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_LayerVis",&opts_LayerVis,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_LayerCo",&opts_LayerCo,0);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_LayerPhi",&opts_LayerPhi,0);CHKERRQ(ierr);	  
  ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_LayerGamSoft",&opts_LayerGamSoft,0);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_LayerGrav",&opts_LayerGrav,0);CHKERRQ(ierr);
  
  
  
  rheology->const_shearmod[0]         = opts_LayerG;  
  rheology->const_eta0[0]             = opts_LayerVis;
  rheology->mises_tau_yield[0]        = opts_LayerCo;
  rheology->dp_pressure_dependance[0] = opts_LayerPhi; 
  rheology->gamma_soft[0]             = opts_LayerGamSoft;
  
  
  opts_LayerCoInf   = opts_LayerCo;
  ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_LayerCoInf",&opts_LayerCoInf,0);CHKERRQ(ierr);
  rheology->mises_tau_yield_inf[0]    = opts_LayerCoInf;
   
  opts_LayerTens  = opts_LayerCo/3.;
  ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_LayerTens",&opts_LayerTens,0);CHKERRQ(ierr);
  rheology->tens_cutoff[0] = opts_LayerTens;
  

	ncells = ctx->Q->ncells;
	ngp    = ctx->Q->ngp;
	
  /*set the Layer parameter everywhere */
	for (e=0;e<ncells;e++) {
		ierr = QuadratureStokesGetCell(ctx->Q,e,&gausspoints);CHKERRQ(ierr);
		
		for (p=0; p<ngp; p++) {
      
      gausspoints[p].phase   = 0;
			gausspoints[p].Fu[0]   = 0.0;
 			gausspoints[p].Fu[1]   = opts_LayerGrav;
			gausspoints[p].Fp      = 0.0;
      gausspoints[p].pls     = 0.0;
      d0 = 1.0 + rheology->const_shearmod[0]/rheology->const_eta0[0]*ctx->dt;
      //gausspoints[p].eta_ref = d0; //d0 // DONT STORE STATE!! //
		  gausspoints[p].eta     = 2.0 * rheology->const_shearmod[0]*ctx->dt/d0; //d1
     }
  }
  
  
  
  /*Now we look for the heterogneneity mechprop */
  
  opts_NotchG       = 1.0;
  opts_NotchVis     = 1.0e-6;
	opts_NotchCo      = 1000000000000000000000000000000.0;
	opts_NotchPhi     = 0.0;
  opts_NotchGamSoft = 0.0;
  opts_NotchGrav    = gravity; 
  
  ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_NotchG",&opts_NotchG,0);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_NotchVis",&opts_NotchVis,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_NotchCo",&opts_NotchCo,0);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_NotchPhi",&opts_NotchPhi,0);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_NotchGamSoft",&opts_NotchGamSoft,0);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_NotchGrav",&opts_NotchGrav,0);CHKERRQ(ierr);
  
  rheology->const_shearmod[1]         = opts_LayerG; 
  rheology->const_eta0[1]             = opts_NotchVis;
  rheology->mises_tau_yield[1]        = opts_NotchCo;
  rheology->dp_pressure_dependance[1] = opts_NotchPhi; 
  rheology->gamma_soft[1]             = opts_NotchGamSoft;
  
  opts_NotchCoInf   = opts_NotchCo;
  ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_NotchCoInf",&opts_NotchCoInf,0);CHKERRQ(ierr);
  rheology->mises_tau_yield_inf[1]    = opts_NotchCoInf;
  
  opts_NotchTens  = opts_NotchCo/3.;
  ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_NotchTens",&opts_NotchTens,0);CHKERRQ(ierr);
  rheology->tens_cutoff[1] = opts_NotchTens;
  
  
  
  
  /*Now we look in the command line for the type of heterogeneity */
  
  ierr = PetscOptionsGetInt(PETSC_NULL,"-notch_type",&opts_Notchtype,0);CHKERRQ(ierr);
  
                                                
                                                
  switch(opts_Notchtype){
    case 0 : 
      /*set the notch location */  
      opts_NotchMaxY   = 0.2;
      opts_NotchMinY   = 0.0;
      opts_NotchMinX   = 7.2;
      opts_NotchMaxX   = 7.6;
      ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_NotchMaxY",&opts_NotchMaxY,0);CHKERRQ(ierr);
      ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_NotchMinX",&opts_NotchMinX,0);CHKERRQ(ierr);
      ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_NotchMaxX",&opts_NotchMaxX,0);CHKERRQ(ierr);
      ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_NotchMinY",&opts_NotchMinY,0);CHKERRQ(ierr);
      for (e=0;e<ncells;e++) {
        ierr = QuadratureStokesGetCell(ctx->Q,e,&gausspoints);CHKERRQ(ierr);        
        for (p=0; p<ngp; p++) {
          PetscScalar coord_x = gausspoints[p].coord[0];
          PetscScalar coord_y = gausspoints[p].coord[1];
          if ((coord_y < opts_NotchMaxY) && (coord_y > opts_NotchMinY) && (coord_x > opts_NotchMinX) && (coord_x < opts_NotchMaxX) ) {
            gausspoints[p].phase = 1;
            
            d0 = 1.0 + rheology->const_shearmod[1]/rheology->const_eta0[1]*ctx->dt;
            gausspoints[p].eta_ref = d0; //d0
            gausspoints[p].eta     = 2.0 * rheology->const_shearmod[1]*ctx->dt/d0;
            gausspoints[p].Fu[0] = 0;
            gausspoints[p].Fu[1] = opts_NotchGrav;
          }
        }
      }
      break; 
      
    case 1 :
      
      /* notch in bottom corner*/
      ierr = QuadratureStokesGetCell(ctx->Q,0,&gausspoints);CHKERRQ(ierr);
      for (p=0; p<ngp; p++) {
        gausspoints[p].phase = 1;
        
        d0 = 1.0 + rheology->const_shearmod[1]/rheology->const_eta0[1]*ctx->dt;
        gausspoints[p].eta_ref = d0; //d0
        gausspoints[p].eta     = 2.0 * rheology->const_shearmod[1]*ctx->dt/d0;
        gausspoints[p].Fu[0] = 0;
        gausspoints[p].Fu[1] = opts_NotchGrav;
      }
      break; 
    case 2 : 
      /* notch in the middle of the box, first checks the user has provided an odd number of element*/ 
      
      if (((ctx->mx + 1)%2 ==0 ) & ((ctx->my + 1)%2 ==0)){
        PetscScalar e_center = (ctx->mx + 1)*0.5-1+((ctx->my + 1)*0.5-1) * ctx->mx; 
        ierr = QuadratureStokesGetCell(ctx->Q,e_center,&gausspoints);CHKERRQ(ierr);
        for (p=0; p<ngp; p++) {
          gausspoints[p].phase = 1;
          d0 = 1.0 + rheology->const_shearmod[1]/rheology->const_eta0[1]*ctx->dt;
          gausspoints[p].eta_ref = d0; //d0
          gausspoints[p].eta     = 2.0 * rheology->const_shearmod[1]*ctx->dt/d0;
          gausspoints[p].Fu[0] = 0;
          gausspoints[p].Fu[1] = opts_NotchGrav;
        }
      }else {
        SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"the model must have a odd number of elements to put the notch in its middle");
      }
       
      break;
      case 3 :
      /* notch in two corners*/
      
      ierr = QuadratureStokesGetCell(ctx->Q,0,&gausspoints);CHKERRQ(ierr);
      for (p=0; p<ngp; p++) {
        gausspoints[p].phase = 1;
        d0 = 1.0 + rheology->const_shearmod[1]/rheology->const_eta0[1]*ctx->dt;
        gausspoints[p].eta_ref = d0; //d0
        gausspoints[p].eta     = 2.0 * rheology->const_shearmod[1]*ctx->dt/d0;
        gausspoints[p].Fu[0] = 0;
        gausspoints[p].Fu[1] = opts_NotchGrav;
      }
      ierr = QuadratureStokesGetCell(ctx->Q,ctx->mx*ctx->my-1,&gausspoints);CHKERRQ(ierr);
      for (p=0; p<ngp; p++) {
        gausspoints[p].phase = 1;
        d0 = 1.0 + rheology->const_shearmod[1]/rheology->const_eta0[1]*ctx->dt;
        gausspoints[p].eta_ref = d0; //d0
        gausspoints[p].eta     = 2.0 * rheology->const_shearmod[1]*ctx->dt/d0;
        gausspoints[p].Fu[0] = 0;
        gausspoints[p].Fu[1] = opts_NotchGrav;
      }
      break;
      /*
    case 4 :
      for (e=0;e<ncells;e++) {
        PetscScalar alors; 
        ierr = QuadratureStokesGetCell(ctx->Q,e,&gausspoints);CHKERRQ(ierr);
        alors = (-rand() % 500 )/1000.0+1;
        for (p=0; p<ngp; p++) {
          gausspoints[p].eta_ref     = alors*gausspoints[p].eta;
        }        
      }      
      break;
      */
  } 
  
  
	for (e=0;e<ncells;e++) {
		ierr = QuadratureStokesGetCell(ctx->Q,e,&gausspoints);CHKERRQ(ierr);
    /* Set all props based on the center gauss point */
    for (p=0; p<ngp; p++) {
      gausspoints[p].phase   = gausspoints[4].phase;
      gausspoints[p].Fu[0]   = gausspoints[4].Fu[0];
      gausspoints[p].Fu[1]   = gausspoints[4].Fu[1];
      gausspoints[p].Fp      = gausspoints[4].Fp;
      gausspoints[p].eta     = gausspoints[4].eta;
      gausspoints[p].eta_ref = gausspoints[4].eta_ref;
    }
	}
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_NotchEl"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_NotchEl(pTatinCtx ctx)
{
	BCList         ubclist;
	PetscScalar    bcval,fac;
	DM             dav;
	PetscErrorCode ierr;
	
  PetscReal jmin[3],jmax[3]; 
	PetscFunctionBegin;
	PetscScalar opts_srH, opts_Lx,opts_Ly;
  PetscInt opts_free;
	PetscScalar alpha_m;
	ubclist = ctx->u_bclist;
	dav     = ctx->dav;
  
  
  /*  -notch_srH   : strainrate (default =1.0); */ 
  
  opts_srH = 1.0;
 	ierr = PetscOptionsGetScalar(PETSC_NULL,"-notch_srH",&opts_srH,0);CHKERRQ(ierr);
  
  alpha_m = (double)(ctx->continuation_m)/( (double)(ctx->continuation_M) );
  alpha_m=1.0;  
  
  DMDAGetBoundingBox(dav,jmin,jmax);
  opts_Lx = jmax[0]-jmin[0]; 
  opts_Ly = jmax[1]-jmin[1];
  
  opts_free = 1;
  ierr = PetscOptionsGetInt(PETSC_NULL,"-notch_free",&opts_free,0);CHKERRQ(ierr);
  
  
  
  
  fac = (ctx->dt)/(ctx->dt_adv);
  
  switch(opts_free){
    case 0 : 
      bcval = -opts_srH*2.0*opts_Ly/opts_Lx*alpha_m*fac;  
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = -opts_srH*alpha_m*fac; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
      bcval = opts_srH*alpha_m*fac;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
    
      break;
      
    case 1 : 
      bcval = -opts_srH*alpha_m*fac; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
      bcval = opts_srH*alpha_m*fac;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
    
      break;
      
    case 2 : 
      bcval = -opts_srH*2.0*opts_Ly/opts_Lx*alpha_m*fac;  
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
      bcval = 2*opts_srH*alpha_m*fac;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
    
      bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      break;
    case 3 : {      
      bcval = -opts_srH*opts_Ly/opts_Lx*alpha_m*fac;  
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      
      bcval = opts_srH*opts_Ly/opts_Lx*alpha_m*fac;  
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      
      bcval = -opts_srH*alpha_m*fac; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
      bcval = opts_srH*alpha_m*fac;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
    }
    case 4 :      
      bcval = 0.0;        
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      break;
    
    case 5 :    
      bcval = 0.0;  
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      break;      
    
    case 6 :    
      
      bcval = opts_srH*fac;  
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = 0.0;  
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_MIDPOINT,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_MIDPOINT,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

      break;

     case 7 : 
         
      bcval = -opts_srH*opts_Ly*fac;  
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
      bcval = opts_srH*opts_Lx*fac;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
     
     break;
      
  } 
  
  
	PetscFunctionReturn(0);
}

