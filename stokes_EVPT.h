//
//  stokes_VPT.h
//  
//
//  Created by le pourhiet laetitia on 8/25/17.
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#ifndef _stokes_EVPT_h
#define _stokes_EVPT_h


PetscErrorCode StokesEvaluateRheologyNonLinearitiesMarkers_EVPT(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[]);

PetscErrorCode TkUpdateRheologyMarkerStokes_EVPT(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[]);


#endif
