/*
 
 Model Description:

 
 Input / command line parameters:
 
 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"

/* ================= CrystalJCP ================= */

struct _p_CrystalCtx {
	PetscScalar Lx,Ly;
	PetscInt    ncrystal;
	PetscInt    crystal_shape; // 0 - circle: 1 - ellipse
	PetscScalar radius;
	PetscScalar radius_x;
	PetscScalar radius_y;
	PetscScalar eta_bkg,eta_crystal;
	PetscScalar rho_bkg,rho_crystal;
};


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelOutput_CrystalJCP"
PetscErrorCode pTatin2d_ModelOutput_CrystalJCP(pTatinCtx ctx,Vec X,const char prefix[])
{
	char *pvdfilename;
	PetscErrorCode ierr;
	
	
	PetscFunctionBegin;
	if (prefix == PETSC_NULL) {
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,"vp");CHKERRQ(ierr);
		ierr = SwarmOutputParaView_MPntPStokes(ctx->db,ctx->outputpath,"mpoints_stk");CHKERRQ(ierr);
	} else { /* exanple of adding a different prefix to the file names - not sure this is useful */
		char *name;
		
		///////////////////////////////////////////////////////////////////////////
		// PVD
		asprintf(&pvdfilename,"%s/timeseries_vp.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;
			
			asprintf(&vtkfilename, "%s_vp.pvts",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			free(vtkfilename);
		}
		free(pvdfilename);
		// PVTS
		asprintf(&name,"%s_vp",prefix);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
		
		///////////////////////////////////////////////////////////////////////////
		// PVD
		asprintf(&pvdfilename,"%s/timeseries_mpoints_stk.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;
			
			asprintf(&vtkfilename, "%s_mpoints_stk.pvtu",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			free(vtkfilename);
		}
		free(pvdfilename);
		// PVTU
		asprintf(&name,"%s_mpoints_stk",prefix);
		ierr = SwarmOutputParaView_MPntPStokes(ctx->db,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
		//////////////
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_CrystalJCP"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_CrystalJCP(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	struct _p_CrystalCtx *user;

	PetscFunctionBegin;

	ierr = PetscMalloc(sizeof(struct _p_CrystalCtx),&user);CHKERRQ(ierr);
	ierr = pTatinCtxAttachModelData(ctx,"CrystalJCPCtx",(void*)user);CHKERRQ(ierr);
	
	user->Lx = 1.0;
	user->Ly = 1.0;
	PetscOptionsGetScalar(PETSC_NULL,"-crystaljcp_Lx",&user->Lx,0);
	PetscOptionsGetScalar(PETSC_NULL,"-crystaljcp_Ly",&user->Ly,0);
	

	ierr = DMDASetUniformCoordinates(ctx->dav,0.0,user->Lx, 0.0,user->Ly, 0.0,0.0);CHKERRQ(ierr);

	PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "compute_crystal_geometry_circle"
PetscErrorCode compute_crystal_geometry_circle(PetscInt ncrystals,PetscScalar r,PetscScalar Lx,PetscScalar Ly,PetscScalar **crystal_pos,PetscScalar  **crystal_angle)
{
	PetscErrorCode         ierr;
	PetscScalar *pos,*angle;
	PetscInt p,found,overlap,attempt;
	
	PetscFunctionBegin;

	PetscMalloc(sizeof(PetscScalar)*2*ncrystals,&pos);
	PetscMalloc(sizeof(PetscScalar)*ncrystals,&angle);
	
	srand(0);
	for (p=0; p<ncrystals; p++) {
		PetscScalar a = rand()/( (PetscScalar)RAND_MAX );

		angle[p] = -M_PI + 2.0 * M_PI * a;
	}

	
	srand(0);

START_INCLUSION:
	PetscPrintf(PETSC_COMM_WORLD,"Commencing inclusion generation \n");
	
	found = 0;
	attempt = 0;
	while (found < ncrystals) {
		PetscScalar xp = rand()/( (PetscScalar)RAND_MAX );
		PetscScalar yp = rand()/( (PetscScalar)RAND_MAX );
		PetscScalar dx,dy,range[2],cp[2];

		if (attempt == 50000) { goto START_INCLUSION; }
		
		xp = xp * Lx;
		yp = yp * Ly;
		attempt++;
		
		/*
		dx = rx * sin(angle[found]); 
		range[0] = xp - dx;
		if (range[0] < 0.0) { continue; }
		range[0] = xp + dx;
		if (range[0] > Lx) { continue; }

		dy = ry * cos(angle[found]); 
		range[1] = yp - dy;
		if (range[1] < 0.0) { continue; }
		range[1] = yp + dy;
		if (range[1] > Ly) { continue; }
		*/

		dx = 1.5*r; 
		range[0] = xp - dx;
		if (range[0] < 0.0) { continue; }
		range[0] = xp + dx;
		if (range[0] > Lx) { continue; }
		
		dy = 1.5*r; 
		range[1] = yp - dy;
		if (range[1] < 0.0) { continue; }
		range[1] = yp + dy;
		if (range[1] > Ly) { continue; }

		
		/* check others for overlap */
		cp[0] = xp;
		cp[1] = yp;
		overlap = 0;
		for (p=0; p<found; p++) {
			PetscScalar sep;

			sep = sqrt( (pos[2*p+0]-cp[0])*(pos[2*p+0]-cp[0]) + (pos[2*p+1]-cp[1])*(pos[2*p+1]-cp[1]) );
			if (sep < 2.1*r) {
				overlap = 1;
				break;
			}
		}
		if (overlap == 1) { continue; }
		
		pos[2*found+0] = xp;
		pos[2*found+1] = yp;
		found++;
	}
	
	*crystal_pos   = pos;
	*crystal_angle = angle;
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_CrystalJCP"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_CrystalJCP(pTatinCtx ctx)
{
	PetscErrorCode         ierr;
	PetscInt               e,ncells,n_mp_points;
	PetscInt               p;
	DataBucket             db;
	DataField              PField_std,PField_stokes;
	double                 eta,rho;
	int                    phase;
	PetscBool              crystal;
	PetscInt               cc,ncrystals; 
	PetscScalar            *crystal_pos,*crystal_angle;
	struct _p_CrystalCtx   *user;
	
	
	PetscFunctionBegin;
	
	ctx->rheology_constants.rheology_type = RHEOLOGY_VISCOUS;

	ierr = QuadratureStokesGravityModelCoordinateAlignedSetValues(ctx->Q,0.0,-9.8);CHKERRQ(ierr);
	
	ierr = pTatinCtxGetModelData(ctx,"CrystalJCPCtx",(void**)&user);CHKERRQ(ierr);

	user->ncrystal = 1;
	user->crystal_shape = 0;
	
	PetscOptionsGetInt(PETSC_NULL,"-crystaljcp_n",&user->ncrystal,0);
	PetscOptionsGetInt(PETSC_NULL,"-crystaljcp_shape",&user->crystal_shape,0);
	
	user->radius = 0.05;
	user->radius_x = 0.05;
	user->radius_y = 0.05;
	
	PetscOptionsGetScalar(PETSC_NULL,"-crystaljcp_r",&user->radius,0);
	PetscOptionsGetScalar(PETSC_NULL,"-crystaljcp_rx",&user->radius_x,0);
	PetscOptionsGetScalar(PETSC_NULL,"-crystaljcp_ry",&user->radius_y,0);

	if (user->crystal_shape == 0) { /* circle */
		user->radius_x = user->radius;
		user->radius_y = user->radius;
	}
	
	user->eta_bkg = 1.0e-3;
	user->eta_crystal = 1.0;
	
	PetscOptionsGetScalar(PETSC_NULL,"-crystaljcp_eta0",&user->eta_bkg,0);
	PetscOptionsGetScalar(PETSC_NULL,"-crystaljcp_eta1",&user->eta_crystal,0);

	user->rho_bkg = 0.5;
	user->rho_crystal = 1.0;
	
	PetscOptionsGetScalar(PETSC_NULL,"-crystaljcp_rho0",&user->rho_bkg,0);
	PetscOptionsGetScalar(PETSC_NULL,"-crystaljcp_rho1",&user->rho_crystal,0);
	
	/* compute geometry */
	ncrystals = user->ncrystal;
	ierr = compute_crystal_geometry_circle(ncrystals,user->radius,user->Lx,user->Ly,&crystal_pos,&crystal_angle);CHKERRQ(ierr);

	/* define properties on material points */
	db = ctx->db;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));

	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));
	
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	for (p=0; p<n_mp_points; p++) {
		MPntStd     *material_point;
		MPntPStokes *mpprop_stokes;
		double      *position;
		
		DataFieldAccessPoint(PField_std,p,(void**)&material_point);
		DataFieldAccessPoint(PField_stokes,p,(void**)&mpprop_stokes);

		MPntStdGetField_global_coord(material_point,&position);
		
		eta = user->eta_bkg;
		rho = user->rho_bkg;
		phase = 0;

		for (cc=0; cc<ncrystals; cc++) {
			PetscScalar *cp = &crystal_pos[2*cc];
			PetscScalar sep,r;

			crystal = PETSC_FALSE;
			sep = (position[0]-cp[0])*(position[0]-cp[0]) + (position[1]-cp[1])*(position[1]-cp[1]);
			if (sep < user->radius*user->radius) {
				crystal = PETSC_TRUE;
			}
			
			if (crystal) {
				eta = user->eta_crystal;
				rho = user->rho_crystal;
				phase = 1;
				break;
			}
		}
		
		
		
		/* user the setters provided for you */
		MPntStdSetField_phase_index(material_point,phase);

		MPntPStokesSetField_eta_effective(mpprop_stokes,eta);
		MPntPStokesSetField_density(mpprop_stokes,rho);
	}

	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokes);
	
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_CrystalJCP"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_CrystalJCP(pTatinCtx ctx)
{
	BCList         ubclist;
	PetscScalar    bcval;
	DM             dav;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ubclist = ctx->u_bclist;
	dav     = ctx->dav;

	/* free slip top */
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

	/* free slip left */
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	

	/* free slip right */
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

	/* free slip bottom */
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	
	
	PetscFunctionReturn(0);
}


