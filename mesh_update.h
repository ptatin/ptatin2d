
#ifndef __mesh_update_h__
#define __mesh_update_h__

PetscErrorCode  DMCoarsenHierarchy2_DA(DM da,PetscInt nlevels,DM dac[]);


PetscErrorCode UpdateMeshGeometry_FullLagSurface(DM dav,Vec velocity,PetscReal step);
PetscErrorCode UpdateMeshGeometry_VerticalDisplacement(DM dav,Vec velocity,PetscReal step);
PetscErrorCode UpdateMeshGeometry_FullLagSurfaceJMAX_RemeshInteriorJMIN_JMAX(DM dav,Vec velocity,PetscReal step);
PetscErrorCode UpdateMeshGeometry_FullLagSurfaceJMIN_JMAX_RemeshInteriorJMIN_JMAX(DM dav,Vec velocity,PetscReal step);
PetscErrorCode UpdateMeshGeometry_LatInflowLagSurfaceJMIN_JMAX_RemeshInteriorJMIN_JMAX(DM dav,Vec velocity,PetscReal step);
PetscErrorCode UpdateMeshGeometry_LagSurfaceJMIN_JMAX_RemeshInteriorRefineJMIN_JMAX(DM dav,Vec velocity,PetscReal dt);
PetscErrorCode UpdateMeshGeometry_LatInflowLagSurfaceJMIN_JMAX_RemeshInteriorRefineJMIN_JMAX(DM dav,Vec velocity,PetscReal dt);
PetscErrorCode UpdateMeshGeometry_FullLagSurfaceJMAX_RemeshInteriorRefineJMIN_JMAX(DM dav,Vec velocity,PetscReal step);
PetscErrorCode UpdateMeshGeometry_BilinearizeQ2Elements(DM dau);

PetscErrorCode UpdateMeshGeometry_SurfaceCourantStep(DM da,Vec velocity,PetscReal max_disp_y,PetscReal Courant_surf, PetscReal *step);

PetscErrorCode UpdateMeshGeometry_NormalVPrescribedWithLagrangianFreeSurface(DM dav,Vec velocity,PetscScalar Unormal[],PetscScalar Vnormal_base,PetscReal step);
PetscErrorCode UpdateMeshGeometry_DecoupledHorizontalVerticalMeshMovement(DM dav,Vec velocity,PetscReal step);
PetscErrorCode UpdateMeshGeometry_Interp(DM dav,Vec new_mesh_coords);

#endif

