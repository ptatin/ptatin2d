/*
 
 Model Description:
 
 Input / command line parameters:
 
 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"

/* ================= winkler ================= */

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelOutput_winkler"
PetscErrorCode pTatin2d_ModelOutput_winkler(pTatinCtx ctx,Vec X,const char prefix[])
{
	PetscErrorCode ierr;

	if (prefix == PETSC_NULL) {
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,"qpoints");CHKERRQ(ierr);
		ierr = pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(ctx,ctx->outputpath,"faceqpoints");CHKERRQ(ierr);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,"vp");CHKERRQ(ierr);
		ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,"int_fields");CHKERRQ(ierr);
	} else { /* exanple of adding a different prefix to the file names - not sure this is useful */
		char *name;
		
		asprintf(&name,"%s_qpoints",prefix);
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);

		asprintf(&name,"%s_faceqpoints",prefix);
		ierr = pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
		
		asprintf(&name,"%s_vp",prefix);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
		asprintf(&name,"%s_int_fields",prefix);
		ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
	}
	
	PetscFunctionReturn(0);
}

extern PetscErrorCode deform_mesh_splay(pTatinCtx ctx);

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_winkler"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_winkler(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
	
	ierr = DMDASetUniformCoordinates(ctx->dav,0.0,1.0, 0.0,1.0, 0.0,0.0);CHKERRQ(ierr);
	ierr = deform_mesh_splay(ctx);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_winkler"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_winkler(pTatinCtx ctx)
{
	PetscInt e,ncells;
	PetscInt p,ngp;
	GaussPointCoefficientsStokes *gausspoints;
	PetscReal opts_eta0,opts_eta1,opts_xc;
	PetscInt  opts_nz;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	opts_eta0 = 1.0;
	
	
	ncells = ctx->Q->ncells;
	ngp    = ctx->Q->ngp;
	ctx->rheology_constants.rheology_type = RHEOLOGY_VISCOUS;

	for (e=0;e<ncells;e++) {
		ierr = QuadratureStokesGetCell(ctx->Q,e,&gausspoints);CHKERRQ(ierr);
		
		for (p=0; p<ngp; p++) {
			PetscScalar coord_x = gausspoints[p].coord[0];
			PetscScalar coord_y = gausspoints[p].coord[1];
			/* eta */
			
			gausspoints[p].eta = opts_eta0;
			/* rhs */
			gausspoints[p].Fu[0] = 0.0;
			gausspoints[p].Fu[1] = -9.8;//sin((double)opts_nz*M_PI*coord_y)*cos(1.0*M_PI*coord_x);
		}
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_winkler"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_winkler(pTatinCtx ctx)
{
	BCList         ubclist;
	PetscScalar    bcval,p_base,v_ext;
	DM             dav;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ubclist = ctx->u_bclist;
	dav     = ctx->dav;
	/* extension */
	v_ext = 1.0;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-winkler_v_ext",&v_ext,0);CHKERRQ(ierr);
	bcval = -v_ext; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
	bcval =  0.0;   ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	

	bcval = v_ext; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	bcval = 0.0;   ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	

	/* free surface */

	/* basement pressure */
	p_base = -10.0;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-winkler_p_base",&p_base,0);CHKERRQ(ierr);
	bcval = p_base; 
    ierr = SurfaceQuadratureStokesTraverseSetTraction(ctx->surfQ,DMDATracLoc_JMIN_LOC,SurfaceQuadatureEvaluator_Isotropic,(void*)&bcval);
    CHKERRQ(ierr);

	
	
	PetscFunctionReturn(0);
}


