
static char help[] = "Solves the diffusion equation in 2d on the unit domain using a DA.\n";

#include "stdio.h"
#include "stdlib.h"

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscdm.h"

#include "daSUPG_Q1.h"

#undef __FUNCT__
#define __FUNCT__ "test_da_supg_1"
PetscErrorCode test_da_supg_1(PetscInt mx,PetscInt my)
{
	DM daT;
  PetscErrorCode ierr;

	
  PetscFunctionBegin;
  ierr = DMDACreateSUPG2d(PETSC_COMM_WORLD,mx,my,&daT);CHKERRQ(ierr);

  
	ierr = DMDADestroySUPG(&daT);CHKERRQ(ierr);
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DefineVelocityField_rigid_rotation"
PetscErrorCode DefineVelocityField_rigid_rotation(DM da,Vec V)
{
  DM cda;
  Vec local_coords;
  PetscInt si,nx,sj,ny;
  PetscInt i,j;
  DMDACoor2d **LA_coords;
  PetscScalar ***LA_V;
  PetscScalar omega;
  PetscErrorCode ierr;
	
  PetscFunctionBegin;
  ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
  ierr = DMDAGetCoordinates(da,&local_coords);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(cda,local_coords,&LA_coords);CHKERRQ(ierr);
	
  ierr = DMDAVecGetArrayDOF(cda,V,&LA_V);CHKERRQ(ierr);
	
  ierr = DMDAGetCorners(da,&si,&sj,0,&nx,&ny,0);CHKERRQ(ierr);
  omega = 1.0;
  for (j=sj;j<sj+ny;j++) {
    for( i=si;i<si+nx;i++) {
      PetscScalar xc = LA_coords[j][i].x;
      PetscScalar yc = LA_coords[j][i].y;
      if (sqrt(xc*xc+yc*yc) < 1.0) {
        LA_V[j][i][0] = -yc * omega; //Fx(xc,yc);
        LA_V[j][i][1] = xc * omega; //Fy(xc,yc);
      }
    }
  }
	
  ierr = DMDAVecRestoreArray(cda,local_coords,&LA_coords);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArrayDOF(cda,V,&LA_V);CHKERRQ(ierr);
	
  PetscFunctionReturn(0);
}
#undef __FUNCT__
#define __FUNCT__ "DefineInitialConditionPhi_gaussian"
PetscErrorCode DefineInitialConditionPhi_gaussian(DM da,Vec phi)
{
  DM cda;
  Vec local_coords;
  PetscInt si,nx,sj,ny;
  PetscInt i,j;
  DMDACoor2d **LA_coords;
  PetscScalar **LA_phi;
  PetscErrorCode ierr;
	
  PetscFunctionBegin;
  ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
  ierr = DMDAGetCoordinates(da,&local_coords);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(cda,local_coords,&LA_coords);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da,phi,&LA_phi);CHKERRQ(ierr);
	
  ierr = DMDAGetCorners(da,&si,&sj,0,&nx,&ny,0);CHKERRQ(ierr);
  for (j=sj;j<sj+ny;j++) {
    for( i=si;i<si+nx;i++) {
      PetscScalar xc = LA_coords[j][i].x;
      PetscScalar yc = LA_coords[j][i].y;
			
			LA_phi[j][i] = 100.0*exp( (-1.0/0.03)*( (xc+0.5)*(xc+0.5) + yc*yc) ); //F(xc,yc);
    }
  }
	
  ierr = DMDAVecRestoreArray(cda,local_coords,&LA_coords);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da,phi,&LA_phi);CHKERRQ(ierr);
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DefineInitialConditionPhi_Line_gaussian"
PetscErrorCode DefineInitialConditionPhi_Line_gaussian(DM da,Vec phi)
{
  DM cda;
  Vec local_coords;
  PetscInt si,nx,sj,ny;
  PetscInt i,j;
  DMDACoor2d **LA_coords;
  PetscScalar **LA_phi;
  PetscErrorCode ierr;
	
  PetscFunctionBegin;
  ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
  ierr = DMDAGetCoordinates(da,&local_coords);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(cda,local_coords,&LA_coords);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da,phi,&LA_phi);CHKERRQ(ierr);
	
  ierr = DMDAGetCorners(da,&si,&sj,0,&nx,&ny,0);CHKERRQ(ierr);
  for (j=sj;j<sj+ny;j++) {
    for( i=si;i<si+nx;i++) {
      PetscScalar xc = LA_coords[j][i].x;
      PetscScalar yc = LA_coords[j][i].y;
			if (i==15) {
				if ( (yc>=-0.5) && (yc<=0.0) ) {
					LA_phi[j][i] = 0.5*(cos(4.0*M_PI*yc+M_PI)+1.0);
				}
			}
    }
  }
	
  ierr = DMDAVecRestoreArray(cda,local_coords,&LA_coords);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da,phi,&LA_phi);CHKERRQ(ierr);
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "test_da_supg_2"
PetscErrorCode test_da_supg_2(PetscInt mx,PetscInt my)
{
	DM daT;
  DMDASUPGParameters params;
  Vec V,kappa;
	BCList bclist;
  PetscInt k,nstep,noutput;
	MatStructure mstr;
  PetscErrorCode ierr;
	
	
  PetscFunctionBegin;
  ierr = DMDACreateSUPG2d(PETSC_COMM_WORLD,mx,my,&daT);CHKERRQ(ierr);
  ierr = DASUPGGetParameters(daT,&params);CHKERRQ(ierr);

  
  ierr = PetscObjectSetName((PetscObject)params->phi,"phi");CHKERRQ(ierr);
  ierr = DMDACreateSUPGWorkVectors(daT,0,&kappa);CHKERRQ(ierr);    ierr = PetscObjectSetName((PetscObject)kappa,"kappa");CHKERRQ(ierr);
  ierr = DMDACreateSUPGWorkVectors(daT,&V,0);CHKERRQ(ierr);        ierr = PetscObjectSetName((PetscObject)V,"V");CHKERRQ(ierr);
	
	ierr = DMDABCListCreate(daT,&bclist);CHKERRQ(ierr);
	
  noutput = 1e6;
  ierr = PetscOptionsGetInt(PETSC_NULL,"-noutput",&noutput,PETSC_NULL);CHKERRQ(ierr);
	
	params->phi_bclist = bclist;
	params->daT = daT;
	params->kappa = kappa;
	params->V = V;
	params->noutput = noutput;

	/* setup flow field and initial condition */
  ierr = VecSet(kappa,1.0e-6);CHKERRQ(ierr);
//
  ierr = DMDASetUniformCoordinates(daT, -1.0,1.0, -1.0,1.0, PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
	ierr = DefineInitialConditionPhi_gaussian(daT,params->phi);CHKERRQ(ierr);
	ierr = DefineVelocityField_rigid_rotation(daT,params->V);CHKERRQ(ierr);
//
/*
  ierr = DMDASetUniformCoordinates(daT, -0.5,0.5, -0.5,0.5, PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
	ierr = DefineInitialConditionPhi_Line_gaussian(daT,params->phi);CHKERRQ(ierr);
	ierr = DefineVelocityField_rigid_rotation(daT,params->V);CHKERRQ(ierr);
*/	
	//ierr = VecZeroEntries(params->V);CHKERRQ(ierr);
	
	/* bc */
	{
		PetscReal bcval;
		bcval = 0.0; ierr = DMDABCListTraverse(bclist,daT,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
		bcval = 0.0; ierr = DMDABCListTraverse(bclist,daT,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
		bcval = 0.0; ierr = DMDABCListTraverse(bclist,daT,DMDABCList_JMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
		bcval = 0.0; ierr = DMDABCListTraverse(bclist,daT,DMDABCList_JMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	}
	
	ierr = DASUPGView(daT,params->kappa,params->V,params->phi,0);CHKERRQ(ierr);
	
	
	/* setup and solve */
//	ierr = SUPGFormJacobian( 0.0, params->phi, 1.0e-3, &params->J,&params->J, &mstr, (void*)params );CHKERRQ(ierr);
//	ierr = SUPGFormFunction( 0.0, params->phi, 1.0e-3, params->F, (void*)params); CHKERRQ(ierr);
	
	{
		KSP ksp;
		PC pc;
		PetscInt k;
		PetscReal time, dt;
		Vec delta_T;
		
		ierr = KSPCreate(PETSC_COMM_WORLD,&ksp);CHKERRQ(ierr);
		ierr = KSPSetOperators(ksp,params->J,params->J,SAME_NONZERO_PATTERN);CHKERRQ(ierr);
		ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
		ierr = KSPSetInitialGuessNonzero(ksp,PETSC_TRUE);CHKERRQ(ierr);
		
		ierr = VecDuplicate(params->phi,&delta_T);CHKERRQ(ierr);

		ierr = VecCopy(params->phi,params->phi_last);CHKERRQ(ierr);
		ierr = VecZeroEntries(params->phi);CHKERRQ(ierr);

		ierr = DASUPG2dComputeTimestep(daT,0.5,params->kappa,params->V,&dt);CHKERRQ(ierr);
		printf("dt = %1.4e \n", dt );
		
		//dt   = 2.5*1.2903e-02;
		//printf("dt = %1.4e \n", dt );
		time = dt;
		for (k=1; k<200000; k++) {
			PetscInt nl;
			
			for (nl=0; nl<1; nl++) {
				ierr = SUPGFormJacobian( time, params->phi, dt, &params->J,&params->J, &mstr, (void*)params );CHKERRQ(ierr);
				ierr = SUPGFormFunction( time, params->phi, dt, params->F, (void*)params ); CHKERRQ(ierr);
				
				ierr = KSPSetOperators(ksp,params->J,params->J,SAME_NONZERO_PATTERN);CHKERRQ(ierr);

				ierr = VecScale(params->F,-1.0);CHKERRQ(ierr);
				ierr = KSPSolve(ksp,params->F,delta_T);CHKERRQ(ierr);
				ierr = VecAXPY(params->phi, 1.0, delta_T);CHKERRQ(ierr); /* phi = phi + delta_phi */
		
	//		ierr = KSPSolve(ksp,params->F,params->phi);CHKERRQ(ierr);
			}
			
			ierr = VecCopy(params->phi,params->phi_last);CHKERRQ(ierr);
			
			time = time + dt;
			ierr = DASUPGView(daT,params->kappa,params->V,params->phi,k);CHKERRQ(ierr);

			printf("step[%d]: time = %1.4e \n", k, time );
			{
				PetscReal nrm;
				ierr = VecNorm(params->F,NORM_2,&nrm);CHKERRQ(ierr);
				printf("  norm(F) = %1.4e \n", nrm);
				ierr = VecNorm(params->phi,NORM_2,&nrm);CHKERRQ(ierr);
				printf("  norm(phi) = %1.4e \n", nrm);
				ierr = VecNorm(params->phi_last,NORM_2,&nrm);CHKERRQ(ierr);
				printf("  norm(phiLast) = %1.4e \n", nrm);
			}
			if (time >= 6.4515e+00 ) { break; }
			
		}

		
		ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
	}
	
	
	
	/* destroy */
	ierr = BCListDestroy(&bclist);CHKERRQ(ierr);
  ierr = VecDestroy(&params->kappa);CHKERRQ(ierr);
  ierr = VecDestroy(&params->V);CHKERRQ(ierr);
	ierr = DMDADestroySUPG(&daT);CHKERRQ(ierr);
	
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc,char **args)
{
  PetscErrorCode ierr;
  PetscInt mx,my;
  
  PetscInitialize(&argc,&args,(char *)0,help);
  
  mx = my = 10;
  ierr = PetscOptionsGetInt(PETSC_NULL,"-mx",&mx,PETSC_NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(PETSC_NULL,"-my",&my,PETSC_NULL);CHKERRQ(ierr);
  
//  ierr = test_da_supg_1(mx,my);CHKERRQ(ierr);
  ierr = test_da_supg_2(mx,my);CHKERRQ(ierr);
	
  ierr = PetscFinalize();CHKERRQ(ierr);
  return 0;
}
