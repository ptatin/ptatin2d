/*
 
 Model Description:

 
 Input / command line parameters:
 
 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"

/* ================= CrystalChannel ================= */

struct _p_CrystalChannelPiston {
	PetscScalar vy;
	PetscScalar block_center_x;
	PetscScalar block_center_y;
	PetscScalar block_center_y_init;
	PetscScalar block_W;
	PetscScalar block_H;
};

struct _p_CrystalChannelPiston pistonL;
struct _p_CrystalChannelPiston pistonR;


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelOutput_CrystalChannel"
PetscErrorCode pTatin2d_ModelOutput_CrystalChannel(pTatinCtx ctx,Vec X,const char prefix[])
{
	char *pvdfilename;
	PetscErrorCode ierr;
	
	
	if (prefix == PETSC_NULL) {
		//ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,"qpoints");CHKERRQ(ierr);
		//ierr = pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(ctx,ctx->outputpath,"faceqpoints");CHKERRQ(ierr);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,"vp");CHKERRQ(ierr);
		//ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,"int_fields");CHKERRQ(ierr);
		ierr = SwarmOutputParaView_MPntPStokes(ctx->db,ctx->outputpath,"mpoints_stk");CHKERRQ(ierr);
	} else { /* exanple of adding a different prefix to the file names - not sure this is useful */
		char *name;
		
		///////////////////////////////////////////////////////////////////////////
/*
		// PVD
		asprintf(&pvdfilename,"%s/timeseries_qpoints.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;
			
			asprintf(&vtkfilename, "%s_qpoints.pvtu",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			free(vtkfilename);
		}
		free(pvdfilename);
		// PVTU
		asprintf(&name,"%s_qpoints",prefix);
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
*/
		///////////////////////////////////////////////////////////////////////////		
		// PVD
		/*		
		 if (ctx->step==0) {
		 ierr = ParaviewPVDOpen("faceqpoints_timeseries.pvd");CHKERRQ(ierr);
		 }
		 else {
		 char *vtkfilename;
		 char *filename;
		 
		 asprintf(&vtkfilename, "%s_faceqpoints.pvtu",prefix);
		 ierr = ParaviewPVDAppend("faceqpoints_timeseries.pvd",ctx->time, vtkfilename, ctx->outputpath);CHKERRQ(ierr);
		 free(vtkfilename);
		 }
		 */ 
		// PVTU
/*		
		asprintf(&name,"%s_faceqpoints",prefix);
		ierr = pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
*/		
		///////////////////////////////////////////////////////////////////////////
		// PVD
		asprintf(&pvdfilename,"%s/timeseries_vp.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;
			
			asprintf(&vtkfilename, "%s_vp.pvts",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			free(vtkfilename);
		}
		free(pvdfilename);
		// PVTS
		asprintf(&name,"%s_vp",prefix);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
		
		///////////////////////////////////////////////////////////////////////////
/*
		// PVD
		asprintf(&pvdfilename,"%s/timeseries_int_fields.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;
			
			asprintf(&vtkfilename, "%s_int_fields.pvts",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			free(vtkfilename);
		}
		free(pvdfilename);
		// PVTS
		asprintf(&name,"%s_int_fields",prefix);
		ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
*/		
		///////////////////////////////////////////////////////////////////////////
		// PVD
		asprintf(&pvdfilename,"%s/timeseries_mpoints_stk.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;
			
			asprintf(&vtkfilename, "%s_mpoints_stk.pvtu",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			free(vtkfilename);
		}
		free(pvdfilename);
		// PVTU
		asprintf(&name,"%s_mpoints_stk",prefix);
		ierr = SwarmOutputParaView_MPntPStokes(ctx->db,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
		//////////////
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_CrystalChannel"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_CrystalChannel(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	extern struct _p_CrystalChannelPiston pistonL, pistonR;
	PetscScalar x0,x1,y0,y1;
	PetscFunctionBegin;

	
	x0 = 0.0;
	x1 = 0.2;
	y0 = 0.0;
	y1 = 1.55;
	

	/* 
	 Define size of the block for the driving piston.
	 Best to do that here so that the block dimensions are compatible with the domain size.
	*/
	pistonL.block_center_y_init = 1.55-0.5-0.05;
	pistonL.block_W             = 0.05;
	pistonL.block_H             = 1.0;
	
	pistonL.block_center_x = x0 + 0.5*pistonL.block_W;
	pistonL.block_center_y = pistonL.block_center_y_init;
	///////////
///////////////////////////////////////////////////////////////////
	///////////
	pistonR.block_center_y_init = 1.55-0.5-0.05;
	pistonR.block_W             = 0.05;
	pistonR.block_H             = 1.0;
	
	pistonR.block_center_x = x1 - 0.5*pistonR.block_W;
	pistonR.block_center_y = pistonR.block_center_y_init;

	ierr = DMDASetUniformCoordinates(ctx->dav,x0,x1, y0,y1, 0.0,0.0);CHKERRQ(ierr);

	// no block //
	//x1 = 0.1;
	//ierr = DMDASetUniformCoordinates(ctx->dav,x0,x1, y0,y1, 0.0,0.0);CHKERRQ(ierr);

	
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_CrystalChannel"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_CrystalChannel(pTatinCtx ctx)
{
	PetscInt               e,ncells,n_mp_points;
	PetscInt               p,ngp;
	GaussPointCoefficientsStokes *gausspoints;
	PetscReal              opts_eta0,opts_eta1,opts_rho0,opts_rho1;
	DataBucket             db;
	DataField              PField_std,PField_stokes;
	double                 eta,rho;
	int                    phase_init,phase;
	extern struct _p_CrystalChannelPiston pistonL, pistonR;
	PetscErrorCode         ierr;
	PetscInt               cc,ncrystals=1; 
	PetscScalar            crystal_pos[] = { 
														0.1,  0.44, 
														0.07, 0.4,
														0.13, 0.36
												 };
	
	
	PetscFunctionBegin;
	
	ierr = QuadratureStokesGravityModelCoordinateAlignedSetValues(ctx->Q,0.0,-9.8);CHKERRQ(ierr);
	
	ncrystals = sizeof(crystal_pos)/(sizeof(PetscScalar)*2);
	
	/* crystal */
	opts_eta0 = 1.0;
	opts_rho0 = 2.7;
	/* mush */
	opts_eta1 = 1.0e-6;
	opts_rho1 = 2.4;
	
	ierr = PetscOptionsGetReal(PETSC_NULL,"-crystal_eta",&opts_eta0,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-mush_eta",   &opts_eta1,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-crystal_rho",&opts_rho0,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL, "-mush_rho",  &opts_rho1,0);CHKERRQ(ierr);
	
	ncells = ctx->Q->ncells;
	ngp    = ctx->Q->ngp;
	ctx->rheology_constants.rheology_type = RHEOLOGY_VISCOUS;

	/* define properties on material points */
	db = ctx->db;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));

	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));
	
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	for (p=0; p<n_mp_points; p++) {
		MPntStd     *material_point;
		MPntPStokes *mpprop_stokes;
		double  *position;
		
		DataFieldAccessPoint(PField_std,p,(void**)&material_point);
		DataFieldAccessPoint(PField_stokes,p,(void**)&mpprop_stokes);

		MPntStdGetField_global_coord(material_point,&position);
		MPntStdGetField_phase_index(material_point,&phase_init);
		
		eta = opts_eta1;
		rho = opts_rho1;
		phase = 0;

/*		
		if ( (position[0]>=pistonL.block_center_x-0.5*pistonL.block_W) && (position[0]<=pistonL.block_center_x+0.5*pistonL.block_W) ) {
			if ( (position[1]>=pistonL.block_center_y-0.5*pistonL.block_H) && (position[1]<=pistonL.block_center_y+0.5*pistonL.block_H) ) {
				eta = opts_eta0 * 10.0;
			}
		}
		if ( (position[0]>=pistonR.block_center_x-0.5*pistonR.block_W) && (position[0]<=pistonR.block_center_x+0.5*pistonR.block_W) ) {
			if ( (position[1]>=pistonR.block_center_y-0.5*pistonR.block_H) && (position[1]<=pistonR.block_center_y+0.5*pistonR.block_H) ) {
				eta = opts_eta0 * 10.0;
			}
		}
*/		
		for (cc=0; cc<ncrystals; cc++) {
			PetscScalar *cp = &crystal_pos[2*cc];
			PetscScalar sep,r;
			
			sep = (position[0]-cp[0])*(position[0]-cp[0]) + (position[1]-cp[1])*(position[1]-cp[1]);
			r = 0.01;
//			r = 0.0075; // pips //
			if (sep < r*r) {
				eta = opts_eta0;
				rho = opts_rho0;
			}
			
		}
		
		
		
		/* user the setters provided for you */
		MPntStdSetField_phase_index(material_point,phase);

		MPntPStokesSetField_eta_effective(mpprop_stokes,eta);
		MPntPStokesSetField_density(mpprop_stokes,rho);
	}

	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokes);
	
	
	PetscFunctionReturn(0);
}


PetscBool BCListEvaluator_piston_vx( PetscScalar position[], PetscScalar *value, void *ctx ) 
{
	PetscBool impose_dirichlet = PETSC_TRUE;
	struct _p_CrystalChannelPiston *piston;
	
	piston = (struct _p_CrystalChannelPiston*)ctx;
	
	*value = 0.0;
	if ( (position[0]>=piston->block_center_x-0.5*piston->block_W) && (position[0]<=piston->block_center_x+0.5*piston->block_W) ) {
		if ( (position[1]>=piston->block_center_y-0.5*piston->block_H) && (position[1]<=piston->block_center_y+0.5*piston->block_H) ) {

			impose_dirichlet = PETSC_TRUE;
			*value = 0.0;		

		}
	}

	return impose_dirichlet;
}

PetscBool BCListEvaluator_piston_vy( PetscScalar position[], PetscScalar *value, void *ctx ) 
{
	PetscBool impose_dirichlet = PETSC_TRUE;
	struct _p_CrystalChannelPiston *piston;
	
	piston = (struct _p_CrystalChannelPiston*)ctx;
	
	*value = 0.0;
	if ( (position[0]>=piston->block_center_x-0.5*piston->block_W) && (position[0]<=piston->block_center_x+0.5*piston->block_W) ) {
		if ( (position[1]>=piston->block_center_y-0.5*piston->block_H) && (position[1]<=piston->block_center_y+0.5*piston->block_H) ) {
			
			impose_dirichlet = PETSC_TRUE;
			*value = piston->vy;

//			printf("applying %lf to node %lf %lf \n", *value, position[0],position[1] );
		}
	}
	
	return impose_dirichlet;
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_CrystalChannel"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_CrystalChannel(pTatinCtx ctx)
{
	BCList         ubclist;
	PetscScalar    bcval;
	DM             dav;
	PetscReal      time;
	extern struct _p_CrystalChannelPiston pistonL, pistonR;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ubclist = ctx->u_bclist;
	dav     = ctx->dav;

	/* boundary condition is moving so reset the bc list and rebuild it for the new piston configuration */
	ierr = BCListInitialize(ubclist);CHKERRQ(ierr);
	ierr = BCListInitGlobal(ubclist);CHKERRQ(ierr);
	ierr = BCListGlobalToLocal(ubclist);CHKERRQ(ierr);
		
	/* free slip top */
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

	/* free slip left */
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	

	/* free slip right */
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

#if 0
	/* apply internal driving velocity */
	time = ctx->time;
	
	pistonL.vy = -1.0;
	pistonR.vy = pistonL.vy;
	
	/* update position of piston */
	pistonL.block_center_y = pistonL.block_center_y_init + pistonL.vy * time;
	pistonR.block_center_y = pistonR.block_center_y_init + pistonR.vy * time;
	
	ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_piston_vx,(void*)&pistonL);CHKERRQ(ierr);
	ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_piston_vy,(void*)&pistonL);CHKERRQ(ierr);
	//DMDABCList_INTERIOR_LOC
	ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_piston_vx,(void*)&pistonR);CHKERRQ(ierr);
	ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,1,BCListEvaluator_piston_vy,(void*)&pistonR);CHKERRQ(ierr);
#endif	

	/* free slip bottom */
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	/* apply free slip bottom last to clobber an non-zero vy components */
	
	
	PetscFunctionReturn(0);
}


