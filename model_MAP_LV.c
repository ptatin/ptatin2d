/*
 
 Model Description:
 Example reading geometry from file.
 This example uses "sticky-air". We delibrately define a mesh which is larger in the vertical
 direction than the phase map, just to demonstrate how to deal with the case when you query
 the phase map for information outside of the domain defined by the phase map.
 
 
 Input / command line parameters:
 
 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"

/* ================= MAP_LV: Linear viscous models from phase map ================= */
/*  for the moment only free surface and free slip with initial surface being horizontal, 
 TODO: 1 / take the bc functions from model notch 
       2 / create a surface adaptor 
 */ 

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelOutput_MAP_LV"
PetscErrorCode pTatin2d_ModelOutput_MAP_LV(pTatinCtx ctx,Vec X,const char prefix[])
{
	char *pvdfilename;
	PetscErrorCode ierr;

	
	if (prefix == PETSC_NULL) {
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,"qpoints");CHKERRQ(ierr);
		ierr = pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(ctx,ctx->outputpath,"faceqpoints");CHKERRQ(ierr);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,"vp");CHKERRQ(ierr);
		ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,"int_fields");CHKERRQ(ierr);
		ierr = SwarmOutputParaView_MPntPStokes(ctx->db,ctx->outputpath,"mpoints_stk");CHKERRQ(ierr);
	} else { /* exanple of adding a different prefix to the file names - not sure this is useful */
		char *name;
		
		///////////////////////////////////////////////////////////////////////////
		// PVD
		asprintf(&pvdfilename,"%s/timeseries_qpoints.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;
			
			asprintf(&vtkfilename, "%s_qpoints.pvtu",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			free(vtkfilename);
		}
		free(pvdfilename);
		// PVTU
		asprintf(&name,"%s_qpoints",prefix);
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);

		///////////////////////////////////////////////////////////////////////////		
		// PVD
/*		
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen("faceqpoints_timeseries.pvd");CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;
			
			asprintf(&vtkfilename, "%s_faceqpoints.pvtu",prefix);
			ierr = ParaviewPVDAppend("faceqpoints_timeseries.pvd",ctx->time, vtkfilename, ctx->outputpath);CHKERRQ(ierr);
			free(vtkfilename);
		}
*/ 
		// PVTU
		asprintf(&name,"%s_faceqpoints",prefix);
		ierr = pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);

		///////////////////////////////////////////////////////////////////////////
		// PVD
		asprintf(&pvdfilename,"%s/timeseries_vp.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;
			
			asprintf(&vtkfilename, "%s_vp.pvts",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			free(vtkfilename);
		}
		free(pvdfilename);
		// PVTS
		asprintf(&name,"%s_vp",prefix);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);

		///////////////////////////////////////////////////////////////////////////
		// PVD
		asprintf(&pvdfilename,"%s/timeseries_int_fields.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;
			
			asprintf(&vtkfilename, "%s_int_fields.pvts",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			free(vtkfilename);
		}
		free(pvdfilename);
		// PVTS
		asprintf(&name,"%s_int_fields",prefix);
		ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);

		///////////////////////////////////////////////////////////////////////////
		// PVD
		asprintf(&pvdfilename,"%s/timeseries_mpoints_stk.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;
			
			asprintf(&vtkfilename, "%s_mpoints_stk.pvtu",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			free(vtkfilename);
		}
		free(pvdfilename);
		// PVTU
		asprintf(&name,"%s_mpoints_stk",prefix);
		ierr = SwarmOutputParaView_MPntPStokes(ctx->db,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
		//////////////
		
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_MAP_LV"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_MAP_LV(pTatinCtx ctx)
{
	PhaseMap phasemap;
	PetscInt M,N;
  PetscBool flg;
	PetscErrorCode ierr;
	char *name;
  char map_file[PETSC_MAX_PATH_LEN];
  PetscFunctionBegin;
	
  flg = PETSC_FALSE;
  ierr = PetscOptionsGetString(PETSC_NULL,"-map_file",map_file,PETSC_MAX_PATH_LEN-1,&flg);CHKERRQ(ierr);
  
  if (flg == PETSC_FALSE) {
    sprintf(map_file,"model_geometry");
  }
  
  
  asprintf(&name,"./inputdata/%s.pmap",map_file);
	PhaseMapLoadFromFile(name,&phasemap);
  asprintf(&name,"./inputdata/%s_phase_map.gp",map_file);
	PhaseMapViewGnuplot(name,phasemap);

	ierr = pTatinCtxAttachPhaseMap(ctx,phasemap);CHKERRQ(ierr);
	
	PetscPrintf(PETSC_COMM_WORLD,"ApplyInitialMeshGeometry_MAP_LV: Domain = [ %1.4e , %1.4e ] x [ %1.4e , %1.4e ] \n", phasemap->x0,phasemap->x1, phasemap->y0,phasemap->y1+0.5 );
	ierr = DMDASetUniformCoordinates(ctx->dav,phasemap->x0,phasemap->x1, phasemap->y0,phasemap->y1, 0.0,0.0);CHKERRQ(ierr);
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_MAP_LV"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_MAP_LV(pTatinCtx ctx)
{
	PhaseMap               phasemap;
	PetscInt               e,p,n_mp_points;
	DataBucket             db;
	DataField              PField_std,PField_stokes;
	PetscScalar            eta_phase[7],rho_phase[7];
	PetscScalar            eta,rho,eta_scale,rho_scale;
	int                    phase_init, phase, phase_index, is_valid, i;
	PetscErrorCode         ierr;
	   
	PetscFunctionBegin;
	
	ctx->rheology_constants.rheology_type = RHEOLOGY_VISCOUS;
	
	ierr = pTatinCtxGetPhaseMap(ctx,&phasemap);CHKERRQ(ierr);
	
	eta_scale = 1.0;
  rho_scale = 1.0;
  ierr = PetscOptionsGetReal(PETSC_NULL,"-MAP_LV_eta_scale",&eta_scale,0);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(PETSC_NULL,"-MAP_LV_rho_scale",&rho_scale,0);CHKERRQ(ierr);
  
	
	eta_phase[0] = 1.0e23 ; // [mantle]
  eta_phase[1] = 1.0e23 ; // [mantle]
	eta_phase[2] = 1.0e22 ; // [upper crust]
	eta_phase[3] = 1.0e19 ; // [lower crust - right]
	eta_phase[4] = 1.0e22 ; // [lower crust - left]
	eta_phase[5] = 1.0e18 ; // [air]
  eta_phase[6] = 1.0e18 ; // [air]

  
	rho_phase[0] = 3.2;
	rho_phase[1] = 3.3; 
	rho_phase[2] = 2.7;
	rho_phase[3] = 2.7;
	rho_phase[4] = 2.9;
  rho_phase[5] = 0.0;
  rho_phase[6] = 0.0;
  PetscPrintf(PETSC_COMM_WORLD,"eta_scale : %e  rho_scale  %e \n", eta_scale,rho_scale);
  
  /* TODO:  WRITE THE NUMBER OF PHASE IN THE PMAP AND DO A LOOP WITH ASPRINTF ON THE COMAND LINE OPTION, */ 
  /* TODO:  AUTO GENERATE THE COMMAND LINE OPTION WITH MATLAB */ 
 
  ierr = PetscOptionsGetReal(PETSC_NULL,"-MAP_LV_eta0",&eta_phase[0],0);CHKERRQ(ierr);ierr = PetscOptionsGetReal(PETSC_NULL,"-MAP_LV_rho0",&rho_phase[0],0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-MAP_LV_eta1",&eta_phase[1],0);CHKERRQ(ierr);ierr = PetscOptionsGetReal(PETSC_NULL,"-MAP_LV_rho1",&rho_phase[1],0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-MAP_LV_eta2",&eta_phase[2],0);CHKERRQ(ierr);ierr = PetscOptionsGetReal(PETSC_NULL,"-MAP_LV_rho2",&rho_phase[2],0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-MAP_LV_eta3",&eta_phase[3],0);CHKERRQ(ierr);ierr = PetscOptionsGetReal(PETSC_NULL,"-MAP_LV_rho3",&rho_phase[3],0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-MAP_LV_eta4",&eta_phase[4],0);CHKERRQ(ierr);ierr = PetscOptionsGetReal(PETSC_NULL,"-MAP_LV_rho4",&rho_phase[4],0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-MAP_LV_eta5",&eta_phase[5],0);CHKERRQ(ierr);ierr = PetscOptionsGetReal(PETSC_NULL,"-MAP_LV_rho5",&rho_phase[5],0);CHKERRQ(ierr);

  
  
  for (i=0; i< 7 ;i++) {
    eta_phase[i] = eta_phase[i]/eta_scale;
    rho_phase[i] = rho_phase[i]/rho_scale;
    PetscPrintf(PETSC_COMM_WORLD,"eta_phase[%d] : %e  rho_phase[%d]  %e \n", i,eta_phase[i],i,rho_phase[i]);
  }    
  
  
	/* define properties on material points */
	db = ctx->db;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));
	
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	for (p=0; p<n_mp_points; p++) {
		MPntStd     *material_point;
		MPntPStokes *mpprop_stokes;
		double  *position;
		
		DataFieldAccessPoint(PField_std,p,(void**)&material_point);
		DataFieldAccessPoint(PField_stokes,p,(void**)&mpprop_stokes);
		
		/* Access using the getter function provided for you (recommeneded for beginner user) */
		MPntStdGetField_global_coord(material_point,&position);
		// OR
		/* Access directly into material point struct (not recommeneded for beginner user) */
		/* Direct access only recommended for efficiency */
		/* position = material_point->coor; */
		
		MPntStdGetField_phase_index(material_point,&phase_init);
		
		phase = 6; /* air, default */

		PhaseMapGetPhaseIndex(phasemap,position,&phase_index);
		PhaseMapCheckValidity(phasemap,phase_index,&is_valid);
    //PetscPrintf(PETSC_COMM_WORLD,"Phase index : %d  is_valid %d \n", phase_index,is_valid);

		if (is_valid==1) { /* point located in the phase map */
			phase = phase_index;
		}

		eta = eta_phase[phase];
		rho = rho_phase[phase];
		
		/* user the setters provided for you */
		MPntStdSetField_phase_index(material_point,phase);
		
		MPntPStokesSetField_eta_effective(mpprop_stokes,eta);
		MPntPStokesSetField_density(mpprop_stokes,rho);
	}
	
	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokes);
	
	
	PetscFunctionReturn(0);
}
#if 0 
#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_MAP_LV"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_MAP_LV(pTatinCtx ctx)
{
	BCList         ubclist;
	PetscScalar    bcval;
	DM             dav;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ubclist = ctx->u_bclist;
	dav     = ctx->dav;
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

//	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

	/* free surface */
//	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
//	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	
	//	bcval = 1.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}
#endif

#if 1

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_MAP_LV"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_MAP_LV(pTatinCtx ctx)
{
	BCList         ubclist;
	PetscScalar    bcval;
	DM             dav;
	PetscErrorCode ierr;
	
  PetscReal jmin[3],jmax[3]; 
	PetscFunctionBegin;
	PetscScalar opts_srH, opts_Lx,opts_Ly;
  PetscInt opts_bcs;
	PetscScalar alpha_m;
	ubclist = ctx->u_bclist;
	dav     = ctx->dav;
  
  opts_srH = 0.0;
 	ierr = PetscOptionsGetScalar(PETSC_NULL,"-vx",&opts_srH,0);CHKERRQ(ierr);
  opts_bcs = 0;
  ierr = PetscOptionsGetInt(PETSC_NULL,"-bc_type",&opts_bcs,0);CHKERRQ(ierr);
  
  
  DMDAGetBoundingBox(dav,jmin,jmax);
  opts_Lx = jmax[0]-jmin[0]; 
  opts_Ly = jmax[1]-jmin[1];
  
  switch(opts_bcs){
    // free surface and free slip all side with symetric horizontal velocity
    case 0 : {
      bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
      bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
    }
      break; 
      
    // incompressible symetric bottom fixed 
    case 1 : {
      bcval = -opts_srH*2.0*opts_Ly/opts_Lx;  
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
      bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
    }
      break; 
      
    // incompressible right side  
     case 2 : {
      bcval = -opts_srH*opts_Ly/opts_Lx;  
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
      bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
    } bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      break;
      
    // incompressible 4 sides  
    case 3 : {      
      bcval = -opts_srH*opts_Ly/opts_Lx;  
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      
      bcval = opts_srH*opts_Ly/opts_Lx;  
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      
      bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
      bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
    }
      break;
      
    // free slip everywhere  
    case 4 :    
      bcval = 0.0;  
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      break;      
    // free surface both side with mid point fixed 
    
    case 5 :    
      bcval = opts_srH;  
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = 0.0;  
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_MIDPOINT,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_MIDPOINT,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);      
      break;
      
      // free surface right side  
    case 6 : {
      
      bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
      bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
     bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
     }  
      break;
      
      // free surface left side  
    case 7 : {
      
      bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
      bcval = 0.0;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
     bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
     }
      break;
      
      
      
  } 
	PetscFunctionReturn(0);
}


#endif


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelUpdateMeshGeometry_MAP_LV"
PetscErrorCode pTatin2d_ModelUpdateMeshGeometry_MAP_LV(pTatinCtx ctx,Vec X)
{
	PetscReal step;
	Vec velocity,pressure;
	DM dav,dap;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	step = ctx->dt;
  
	ierr = DMCompositeGetEntries(ctx->pack,&dav,&dap);CHKERRQ(ierr);
	ierr = DMCompositeGetAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
  
  //	ierr = UpdateMeshGeometry_FullLagSurface(dav,velocity,step);CHKERRQ(ierr);
  //  ierr = UpdateMeshGeometry_VerticalDisplacement(dav,velocity,step);CHKERRQ(ierr);
	ierr = UpdateMeshGeometry_FullLagSurfaceJMAX_RemeshInteriorJMIN_JMAX(dav,velocity,step);CHKERRQ(ierr);
	
	ierr = DMCompositeRestoreAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
  
	PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelUpdateMeshGeometry_MAP_LV_local_global"
PetscErrorCode pTatin2d_ModelUpdateMeshGeometry_MAP_LV_local_global(pTatinCtx ctx,Vec X)
{
	Vec velocity,pressure,coordinates,vel_advect_mesh;
	PetscInt M,N;
	PetscReal step;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	step = ctx->dt;

	ierr = DMCompositeGetAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
	
	ierr = DMGetGlobalVector(ctx->dav,&vel_advect_mesh);CHKERRQ(ierr);
	
	ierr = VecCopy(velocity,vel_advect_mesh);CHKERRQ(ierr);
	ierr = VecStrideScale(vel_advect_mesh,0,0.0);CHKERRQ(ierr); /* zero out the x-componenet */
	
	ierr = DMDAGetCoordinates(ctx->dav,&coordinates);CHKERRQ(ierr);
	ierr = VecAXPY(coordinates,step,vel_advect_mesh);CHKERRQ(ierr); /* x = x + dt.vel_advect_mesh */

	ierr = DMRestoreGlobalVector(ctx->dav,&vel_advect_mesh);CHKERRQ(ierr);

	ierr = DMCompositeRestoreAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
	//
	//
	ierr = DMGetLocalVector(ctx->dav,&vel_advect_mesh);CHKERRQ(ierr);
	ierr = DMGlobalToLocalBegin(ctx->dav,velocity,INSERT_VALUES,vel_advect_mesh);CHKERRQ(ierr);
	ierr = DMGlobalToLocalEnd(  ctx->dav,velocity,INSERT_VALUES,vel_advect_mesh);CHKERRQ(ierr);

	ierr = VecStrideScale(vel_advect_mesh,0,0.0);CHKERRQ(ierr); /* zero out the x-componenet */

	ierr = DMDAGetGhostedCoordinates(ctx->dav,&coordinates);CHKERRQ(ierr);
	ierr = VecAXPY(coordinates,step,vel_advect_mesh);CHKERRQ(ierr); /* x = x + dt.vel_advect_mesh */
	
	ierr = DMRestoreLocalVector(ctx->dav,&vel_advect_mesh);CHKERRQ(ierr);
	
	
	PetscFunctionReturn(0);
}



