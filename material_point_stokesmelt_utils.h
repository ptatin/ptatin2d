
#ifndef __MATERIAL_POINT_PSTOKESMELT_UTILS_H__
#define __MATERIAL_POINT_PSTOKESMELT_UTILS_H__

#include "swarm_fields.h"
#include "data_exchanger.h"
#include "MPntStd_def.h"
#include "MPntPStokes_def.h"
#include "MPntPStokesMelt_def.h"


PetscErrorCode SwarmOutputParaView_MPntPStokesMelt(DataBucket db,const char path[],const char prefix[]);
PetscErrorCode SwarmUpdateProperties_MPntPStokesMelt(DataBucket db,pTatinCtx user,Vec X);

#endif