/*
 *  AVD2d_testing.c
 *  
 *
 *  Created by Dave A. May on 11/16/11.
 *  Copyright 2011 Geophysical Fluid Dynamics. All rights reserved.
 *
 *  The algorithm computes an Approximate Voronoi Diagram (AVD) in 3D using a given set of point coordinates.
 *  
 *  The AVD algorithm, is described in:
 *    M. Velic, D.A. May & L. Moresi,
 *    "A Fast Robust Algorithm for Computing Discrete Voronoi Diagrams",
 *    Journal of Mathematical Modelling and Algorithms,
 *    Volume 8, Number 3, 343-355, DOI: 10.1007/s10852-008-9097-6
 *
 *
 *  Notes:
 *    This implementation uses von-Neumann neighbourhoods for boundary chain growth. 
 *    Do not be tempted to implement "diagonal" neighbourhood growth cycles - this will greatly increase the
 *    size of the boundary chain (and thus memory usage will increase and CPU time will decrease).
 *
 *  Compilation:
 *    gcc -O3 -o AVD2d_testing AVD2d_testing.c AVD2d.o -lm
 *
 */


#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"

#include "AVD2d.h"


void testAVD2d(void)
{
	AVD2d A;
	AVDPoint2d points;
	Aint p,npoints;
	Aint i,count,claimed,iterations;
	double t0,t1,tubc,tcc;
	
	
//	AVD2dCreate(32*6,32*6,10,&A);
//	AVD2dCreate(250,250,10,&A);
	AVD2dCreate(10,10,10,&A);
	
	AVD2dSetDomainSize(A,-1.0,1.0,-1.0,1.0);

	/* define coordinates of points */
//	npoints = 60 * 32*32*32;
	npoints = 16;

	AVDPoint2dCreate(npoints,&points);
	for (p=0; p<npoints; p++) {
		points[p].x = 2.0*rand()/(RAND_MAX+1.0) - 1.0;
		points[p].y = 2.0*rand()/(RAND_MAX+1.0) - 1.0;
	}
	
	/* define initial phase */
	for (p=0; p<npoints; p++) {
		if ( points[p].y < 0.2*sin(M_PI*points[p].x - 0.5) ) {
			points[p].phase = 1;
		}
	}

	/* set points */
	AVD2dSetPoints(A,npoints,points);
	
	/* reset cells */
	AVDCell2dReset(A);
	
	AVDGetTime(&t0);
	AVD2dInit(A,npoints,points);
	AVDGetTime(&t1);
  printf("Total time (AVD2dInit) %1.6e \n", t1-t0);
	
	printf("generating approximate voronoi diagram... [%d points, %dx%d avd cells]\n",npoints,A->mx,A->my);
	tubc = tcc = 0.0;
	count = npoints;
	claimed = 1;
	iterations = 0;
	while (claimed != 0){
		claimed = 0 ;
		for (i=0; i<count; i++){
			AVDGetTime(&t0);
			AVD2dClaimCells(A,i);
			claimed += A->chains[i].num_claimed;
			AVDGetTime(&t1);
			tcc = tcc + (t1-t0);
			
			AVDGetTime(&t0);
			AVD2dUpdateChain(A,i);
			AVDGetTime(&t1);
			tubc = tubc + (t1-t0);
		}
		printf("\tAVD it. %.4d - (ClaimCells) %1.4e , (UpdateChain) %1.4e \n", iterations, tcc, tubc );
		iterations++;
	}
  printf("Total time (AVD2dClaimCells) %1.6e \n", tcc);
  printf("Total time (AVD2dUpdateChain) %1.6e \n", tubc);
  printf("Total time (AVD2d) %1.6e \n", tcc+tubc);
	printf("Total AVD its. %d \n", iterations);	
	
	//
	AVD2dReportMemory(A);

	printf("writing vtu,vtr output files...\n");
	AVDGetTime(&t0);
	AVD2dPointsView_VTU(A,"points_rand.vtu");
	AVD2dCellsView_VTR(A,"cells_rand.vtr");
	AVD2dCellsView_appendedVTR(A,"cells_randa.vtr");
	AVD2dCellsView_VTU(A,"cells_rand.vtu");
	AVD2dCellsView_VTS(A,"cells_rand.vts");
	AVDGetTime(&t1);
  printf("Total time (AVD2dOutput) %1.6e \n", t1-t0);
	
	AVDPoint2dDestroy(&A->points);
	AVD2dDestroy(&A);
}

int main(int nargs,char *args[])
{
	int p;
	double t0,t1;

	AVDGetTime(&t0);
	for (p=0; p<1; p++) {
		testAVD2d();
	}
	AVDGetTime(&t1);
  printf("Total time %1.6e \n", t1-t0);
	
	return EXIT_SUCCESS;
}
