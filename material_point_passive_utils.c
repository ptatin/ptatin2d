#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"

#include "pTatin2d.h"
#include "swarm_fields.h"
#include "data_exchanger.h"
#include "MPntStd_def.h"
#include "MPntPPassive_def.h"
#include "material_point_passive_utils.h"


#undef __FUNCT__
#define __FUNCT__ "SwarmUpdateProperties_MPntPPassive"
PetscErrorCode SwarmUpdateProperties_MPntPPassive(DataBucket db,pTatinCtx user,Vec X)
{
	BTruth found;
  DM                dau,dap;
  Vec               Uloc,Ploc;
	Vec               u,p;
  PetscScalar       *LA_Uloc,*LA_Ploc;
	PetscErrorCode    ierr;
	
	PetscFunctionBegin;
	
	
	DataBucketQueryDataFieldByName(db,MPntPPassive_classname,&found);
	if(found==BFALSE) {
		SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot find DataField with name %s \n", MPntPStokesMelt_classname );
	}
	
    ierr = DMCompositeGetEntries(user->pack,&dau,&dap);CHKERRQ(ierr);
    ierr = DMCompositeGetLocalVectors(user->pack,&Uloc,&Ploc);CHKERRQ(ierr);
	ierr = DMCompositeScatter(user->pack,X,Uloc,Ploc);CHKERRQ(ierr);
	ierr = VecGetArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
	ierr = VecGetArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
	
	ierr = pTatin_TkUpdateMarker_Passive(user,dau,LA_Uloc,dap,LA_Ploc);CHKERRQ(ierr);
	
	ierr = VecRestoreArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
	ierr = VecRestoreArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
  ierr = DMCompositeRestoreLocalVectors(user->pack,&Uloc,&Ploc);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}



#undef __FUNCT__
#define __FUNCT__ "pTatin_TkUpdateMarker_Passive"
PetscErrorCode pTatin_TkUpdateMarker_Passive(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[])
{	
	PetscErrorCode ierr;
	int         pidx,n_mp_points;
	DataBucket  db;
	DataField   PField_std, PField_passive;
	PetscScalar GNIu[2][Q2_NODES_PER_EL_2D],NIp[P_BASIS_FUNCTIONS];
	PetscScalar NIu[U_BASIS_FUNCTIONS];
	PetscScalar NIT[NODES_PER_EL_Q1_2D];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen_u,nen_p,nen_T,e,i,j,k;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	const PetscInt *elnidx_T;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	PetscScalar elT[NODES_PER_EL_Q1_2D];
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	
	PhysCompEnergyCtx phys_energy;
	Vec               T_vec,T_local_vec;
	DM                daT;
	PetscScalar       *LA_T;
	pTatinUnits *units;	
	
	PetscFunctionBegin;
	
	units   = &user->units;
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	/////////////////////* get physics */////////////////
	
	/* stokes */
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
	
	db = user->db_passive;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	DataBucketGetDataFieldByName(db,MPntPPassive_classname,&PField_passive);
	DataFieldGetAccess(PField_passive);
	DataFieldVerifyAccess(PField_passive,sizeof(MPntPPassive));
	
	
	/* energy */

		phys_energy = user->phys_energy;
		T_vec       = phys_energy->T;
		daT         = phys_energy->daT;
		
		ierr = DMGetLocalVector(daT,&T_local_vec);CHKERRQ(ierr);		
		ierr = DMGlobalToLocalBegin(daT,T_vec,INSERT_VALUES,T_local_vec);CHKERRQ(ierr);
		ierr = DMGlobalToLocalEnd(  daT,T_vec,INSERT_VALUES,T_local_vec);CHKERRQ(ierr);
		
		ierr = VecGetArray(T_local_vec,&LA_T);CHKERRQ(ierr);
		ierr = DMDAGetElementsQ1(daT,&nel,&nen_T,&elnidx_T);CHKERRQ(ierr);
		

	/* new piece of physics to be hook here following energy exemple*/
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	

	/* marker loop */
	
	for (pidx=0; pidx<n_mp_points; pidx++) {
		MPntStd       *material_point;
		MPntPPassive  *mpprop_passive;
		double        *position, *xip;
		int            e,phase_mp;
		PetscScalar J[2][2], iJ[2][2];
		PetscScalar J_p,ojp;
		PetscScalar  d1,exx_mp,eyy_mp,exy_mp,strainrateinv_mp;
		PetscScalar  pressure_mp;
		PetscScalar T_mp;	
		
		DataFieldAccessPoint(PField_std,    pidx,(void**)&material_point);	
		MPntStdGetField_local_element_index(material_point,&e);
		MPntStdGetField_global_coord(material_point,&position);
		MPntStdGetField_local_coord(material_point,&xip);
		MPntStdGetField_phase_index(material_point,&phase_mp);
		DataFieldAccessPoint(PField_passive,    pidx,(void**)&mpprop_passive);
		/* get nodal properties for element e */
		/* coordinates*/
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		
		
		/*pressure*/
		ierr = DMDAGetScalarElementField_2D(elp,nen_p,(PetscInt*)&elnidx_p[nen_p*e],p);CHKERRQ(ierr);
		
		ConstructNi_pressure(xip,elcoords,NIp);
		
		pressure_mp = 0.0;
		for (k=0; k<P_BASIS_FUNCTIONS; k++) {
			pressure_mp += NIp[k] * elp[k];
		}
		
		/* temperature */
		T_mp = 0; 

			ierr = DMDAGetScalarElementFieldQ1_2D(elT,(PetscInt*)&elnidx_T[nen_T*e],LA_T);CHKERRQ(ierr);
			ConstructNi_Q1_2D(xip,NIT);
			for (k=0; k<NODES_PER_EL_Q1_2D; k++) {
				T_mp = T_mp + NIT[k] * elT[k];
			}
				
		MPntPPassiveSetField_pressure(mpprop_passive,pressure_mp);
		MPntPPassiveSetField_temperature(mpprop_passive,T_mp);
		
	}
	
	
 	DataFieldRestoreAccess(PField_std);
  	DataFieldRestoreAccess(PField_passive);
		
	ierr = VecRestoreArray(T_local_vec,&LA_T);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(daT,&T_local_vec);CHKERRQ(ierr);		

    ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatin_OutputMarker_Passive"
PetscErrorCode pTatin_OutputMarker_Passive(pTatinCtx user)
{
    int         pidx,n_mp_points;
	DataBucket  db;
	DataField   PField_std, PField_passive;
	FILE        *fp = NULL;
	char        *filename;
	PetscMPIInt    rank;
	
	PetscFunctionBegin;
	
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	asprintf(&filename,"%s/step%1.5d_Passive_PT-subdomain%1.5d.dat",user->outputpath,user->step,rank);
	
	/* open file to parse */
	fp = fopen(filename,"w");
	if (fp==NULL) {
		printf("Error(%s): Could not open file: %s \n",__func__, filename );
		exit(EXIT_FAILURE);
	}
	free(filename);
	fprintf(fp,"# Passive Marker information \n");
	fprintf(fp,"# time: %1.4e \n",user->time);
	
	db = user->db_passive;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	DataBucketGetDataFieldByName(db,MPntPPassive_classname,&PField_passive);
	DataFieldGetAccess(PField_passive);
	DataFieldVerifyAccess(PField_passive,sizeof(MPntPPassive));
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	for (pidx=0; pidx<n_mp_points; pidx++) {
		MPntStd       *material_point;
		MPntPPassive  *mpprop_passive;
		double        *pos;
		float         pressure_mp, T_mp;
	    long int      pid;
	    int           phase_mp,wil;
	    
	    DataFieldAccessPoint(PField_std,    pidx,(void**)&material_point);	
		DataFieldAccessPoint(PField_passive,    pidx,(void**)&mpprop_passive);
		
		MPntStdGetField_point_index(material_point,&pid);
		MPntStdGetField_global_coord(material_point,&pos);
		MPntStdGetField_phase_index(material_point,&phase_mp);
		MPntStdGetField_local_element_index(material_point,&wil);
	    MPntPPassiveGetField_pressure(mpprop_passive,&pressure_mp);
		MPntPPassiveGetField_temperature(mpprop_passive,&T_mp);
        
        fprintf(fp,"%1.9ld %1.2d %d  %1.4e %1.4e %1.4e %1.4e\n", pid, wil,phase_mp,pos[0],pos[1],pressure_mp,T_mp);
		
	}
	fprintf(fp,"\n");
	fclose(fp);
	
	DataFieldRestoreAccess(PField_std);
  	DataFieldRestoreAccess(PField_passive);
    PetscFunctionReturn(0);
	
}


















#if 0  		
		/* velocity*/
		ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],u);CHKERRQ(ierr);
		
		PTatinConstructNI_Q2_2D(xip,NIu);
		PTatinConstructGNI_Q2_2D(xip,GNIu);
		
		
		/* get element velocity/coordinates in component form */
		for (i=0; i<Q2_NODES_PER_EL_2D; i++) {
			ux[i] = elu[2*i  ];
			uy[i] = elu[2*i+1];
			
			xc[i] = elcoords[2*i  ];
			yc[i] = elcoords[2*i+1];
		}
		
		/* coord transformation */
		for (i=0; i<2; i++) {
			for (j=0; j<2; j++) { J[i][j] = 0.0; }
		}
		for (k=0; k<Q2_NODES_PER_EL_2D; k++) {
			J[0][0] += GNIu[0][k] * xc[k] ;
			J[0][1] += GNIu[0][k] * yc[k] ;
			
			J[1][0] += GNIu[1][k] * xc[k] ;
			J[1][1] += GNIu[1][k] * yc[k] ;
		}
		J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
		ojp = 1.0/J_p;
		iJ[0][0] =  J[1][1]*ojp;
		iJ[0][1] = -J[0][1]*ojp;
		iJ[1][0] = -J[1][0]*ojp;
		iJ[1][1] =  J[0][0]*ojp;
		
		/* global derivs at each quadrature point */
		for (k=0; k<Q2_NODES_PER_EL_2D; k++) {
			nx[k] = iJ[0][0]*GNIu[0][k] + iJ[0][1]*GNIu[1][k];
			ny[k] = iJ[1][0]*GNIu[0][k] + iJ[1][1]*GNIu[1][k];
		}
		
		
		/* strain rate (e= B u) at gp */
		exx_mp = eyy_mp = exy_mp = 0.0;
		for (k=0; k<U_BASIS_FUNCTIONS; k++) {
			exx_mp += nx[k] * ux[k];
			eyy_mp += ny[k] * uy[k];
			exy_mp += ny[k] * ux[k] + nx[k] * uy[k];
		}
		
		
		exy_mp = 0.5 * exy_mp;
		strainrateinv_mp = sqrt(0.25*(exx_mp-eyy_mp)*(exx_mp-eyy_mp)+exy_mp*exy_mp);
		
#endif	
