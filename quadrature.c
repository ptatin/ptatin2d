

#include "pTatin2d.h"
#include "element_type_Q2.h"
#include "quadrature.h"

#undef __FUNCT__
#define __FUNCT__ "SurfaceQuadratureStokesCreate"
PetscErrorCode SurfaceQuadratureStokesCreate(DM da,QuadElementEdge index,SurfaceQuadratureStokes *Q)
{
	PetscInt si,sj,ni,nj,M,N,lmx,lmy;
	PetscInt nfaces[QUAD_EDGES];
	SurfaceQuadratureStokes quadrature;
	PetscErrorCode ierr;
	
	ierr = DMDAGetInfo(da,0,&M,&N,0, 0,0,0,0, 0,0,0,0,0);CHKERRQ(ierr);
	ierr = DMDAGetCorners(da,&si,&sj,PETSC_NULL,&ni,&nj,PETSC_NULL);CHKERRQ(ierr);
	ierr = DMDAGetLocalSizeElementQ2(da,&lmx,&lmy,PETSC_NULL);CHKERRQ(ierr);
	nfaces[0] = nfaces[1] = nfaces[2] = nfaces[3] = 0;
	if (si+ni==M) { nfaces[QUAD_EDGE_Pxi]  = lmy; }
	if (si==0)    { nfaces[QUAD_EDGE_Nxi]  = lmy; }
	if (sj+nj==N) { nfaces[QUAD_EDGE_Peta] = lmx; }
	if (sj==0)    { nfaces[QUAD_EDGE_Neta] = lmx; }

	/* do the allocation */
	ierr = _SurfaceQuadratureStokesCreate(index,nfaces[index],Q);CHKERRQ(ierr);
	
	/* assign the cell indices */
	ierr = SurfaceQuadratureStokesCellIndexSetUp(*Q,index,da);CHKERRQ(ierr);
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_SurfaceQuadratureStokesCreate"
PetscErrorCode _SurfaceQuadratureStokesCreate(QuadElementEdge index,PetscInt nfaces,SurfaceQuadratureStokes *Q)
{
	SurfaceQuadratureStokes quadrature;
	ConformingElementFamily e;
	double size;
	PetscErrorCode ierr;
	
  PetscFunctionBegin;
	PetscPrintf(PETSC_COMM_WORLD,"SurfaceQuadratureStokesCreate:\n");
	ierr = PetscMalloc( sizeof(struct _p_SurfaceQuadratureStokes), &quadrature);CHKERRQ(ierr);
	
	ElementTypeCreate_Q2(&e,NSD);
	quadrature->e       = e;
	quadrature->edge_id = index;
	e->generate_surface_quadrature_2D(e,index,&quadrature->ngp,quadrature->gp1,quadrature->gp2);	

	quadrature->nfaces = nfaces;
	
	PetscPrintf(PETSC_COMM_WORLD,"\t[SurfaceQuadratureStokes]: attributing %d edge elements \n", nfaces );
	PetscPrintf(PETSC_COMM_WORLD,"\t[SurfaceQPointCoefficientsStokes]: attributing %d surface quadrature points \n", nfaces*quadrature->ngp );
	if (nfaces!=0) {
		size = (double) sizeof(PetscInt)*nfaces;
		ierr = PetscMalloc( sizeof(PetscInt)*nfaces, &quadrature->cell_list);CHKERRQ(ierr);

		size += (double) ( sizeof(SurfaceQPointCoefficientsStokes)*nfaces*quadrature->ngp );
		ierr = PetscMalloc( sizeof(SurfaceQPointCoefficientsStokes)*nfaces*quadrature->ngp, &quadrature->surfproperties);CHKERRQ(ierr);
		ierr = PetscMemzero(quadrature->surfproperties, sizeof(SurfaceQPointCoefficientsStokes)*nfaces*quadrature->ngp);CHKERRQ(ierr);

		PetscPrintf(PETSC_COMM_WORLD,"\t[SurfaceQPointCoefficientsStokes]: using %1.2lf MBytes \n", (double)(size*1.0e-6) );
	} else {
		quadrature->surfproperties = PETSC_NULL;
	}
		
	*Q = quadrature;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SurfaceQuadratureStokesDestroy"
PetscErrorCode SurfaceQuadratureStokesDestroy(SurfaceQuadratureStokes *Q)
{
	SurfaceQuadratureStokes quadrature;
	PetscErrorCode ierr;
	
  PetscFunctionBegin;

	if (!Q) { PetscFunctionReturn(0); }
	
	quadrature = *Q;
	if (quadrature->surfproperties) { ierr = PetscFree(quadrature->surfproperties);CHKERRQ(ierr); }
	ierr = PetscFree(quadrature);CHKERRQ(ierr);
	
	*Q = PETSC_NULL;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SurfaceQuadratureStokesCellIndexSetUp"
PetscErrorCode SurfaceQuadratureStokesCellIndexSetUp(SurfaceQuadratureStokes Q,QuadElementEdge index,DM da)
{
	PetscInt eli,elj;
	PetscInt si,sj,ni,nj,M,N,lmx,lmy;
	PetscInt nfaces[QUAD_EDGES];
	PetscInt cnt,elidx,nface_edge;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;

	ierr = DMDAGetInfo(da,0,&M,&N,0, 0,0,0,0, 0,0,0,0,0);CHKERRQ(ierr);
	ierr = DMDAGetCorners(da,&si,&sj,PETSC_NULL,&ni,&nj,PETSC_NULL);CHKERRQ(ierr);
	ierr = DMDAGetLocalSizeElementQ2(da,&lmx,&lmy,PETSC_NULL);CHKERRQ(ierr);
	nfaces[0] = nfaces[1] = nfaces[2] = nfaces[3] = 0;
	if (si+ni==M) { nfaces[QUAD_EDGE_Pxi]  = lmy; }
	if (si==0)    { nfaces[QUAD_EDGE_Nxi]  = lmy; }
	if (sj+nj==N) { nfaces[QUAD_EDGE_Peta] = lmx; }
	if (sj==0)    { nfaces[QUAD_EDGE_Neta] = lmx; }
	
	nface_edge = nfaces[ index ];
	if (nface_edge==0) { PetscFunctionReturn(0); }
  
	switch (index) {

		case QUAD_EDGE_Pxi:
			cnt = 0;
			eli = lmx - 1;
			for (elj=0; elj<lmy; elj++) {
				elidx = eli + elj * lmx;
				Q->cell_list[ cnt ] = elidx;
				cnt++;
			}
			break;

		case QUAD_EDGE_Nxi:
			cnt = 0;
			eli = 0;
			for (elj=0; elj<lmy; elj++) {
				elidx = eli + elj * lmx;
				Q->cell_list[ cnt ] = elidx;
				cnt++;
			}
			break;

		case QUAD_EDGE_Peta:
			cnt = 0;
			elj = lmy - 1;
			for (eli=0; eli<lmx; eli++) {
				elidx = eli + elj * lmx;
				Q->cell_list[ cnt ] = elidx;
				cnt++;
			}
			break;

		case QUAD_EDGE_Neta:
			cnt = 0;
			elj = 0;
			for (eli=0; eli<lmx; eli++) {
				elidx = eli + elj * lmx;
				Q->cell_list[ cnt ] = elidx;
				cnt++;
			}
			break;
}
	
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SurfaceQuadratureStokesCoordinatesSetUp"
PetscErrorCode SurfaceQuadratureStokesCoordinatesSetUp(SurfaceQuadratureStokes Q,DM da)
{
	PetscErrorCode ierr;
	DM             cda;
	Vec            gcoords;
	PetscScalar    *LA_gcoords;
	PetscInt       nel,nen,fe,e,i,p,gp;
	const PetscInt *elnidx;
	PetscScalar    elcoords[2*Q2_NODES_PER_EL_2D];
	ConformingElementFamily element;
	double         U_Ni[9];

	PetscFunctionBegin;
	/* setup for coords */
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(da,&gcoords);CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(da,&nel,&nen,&elnidx);CHKERRQ(ierr);
	element = Q->e;

	p = 0;
	for (fe=0; fe<Q->nfaces; fe++) {
		e = Q->cell_list[fe];
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx[nen*e],LA_gcoords);CHKERRQ(ierr);
		
		for (gp=0; gp<Q->ngp; gp++) {
			SurfaceQPointCoefficientsStokes *point = &Q->surfproperties[p];
			PetscScalar xp,yp;
			
			if (p >= Q->nfaces * Q->ngp) {
				SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Invalid access to surface quadrature points occurred");
			}
			
			element->basis_NI_2D(&Q->gp2[gp],U_Ni);
			
			xp = 0.0;
			yp = 0.0;
			for (i=0; i<element->n_nodes_2D; i++) {
				xp = xp + U_Ni[i] * elcoords[NSD*i  ];
				yp = yp + U_Ni[i] * elcoords[NSD*i+1];
			}
			point->coord[0] = xp;
			point->coord[1] = yp;
			
			p++;
		}
	}
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SurfaceQuadratureStokesOrientationSetUp"
PetscErrorCode SurfaceQuadratureStokesOrientationSetUp(SurfaceQuadratureStokes Q,DM da)
{
	PetscErrorCode ierr;
	DM             cda;
	Vec            gcoords;
	PetscScalar    *LA_gcoords;
	PetscInt       nel,nen,fe,e,i,gp;
	const PetscInt *elnidx;
	ConformingElementFamily element;
	double         surfJ;
	double         elcoords[2*Q2_NODES_PER_EL_2D];
	
	PetscFunctionBegin;
	/* setup for coords */
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(da,&gcoords);CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(da,&nel,&nen,&elnidx);CHKERRQ(ierr);
	element = Q->e;
	
	for (fe=0; fe<Q->nfaces; fe++) {
		SurfaceQPointCoefficientsStokes *point;
		
		e = Q->cell_list[fe];
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx[nen*e],LA_gcoords);CHKERRQ(ierr);
		
		ierr = SurfaceQuadratureStokesGetCell(Q,fe,&point);CHKERRQ(ierr);
		
		for (gp=0; gp<Q->ngp; gp++) {
			
			element->compute_surface_geometry_2D(	
														element, 
														elcoords,    // should contain 9 points with dimension 2 (x,y) // 
														Q->edge_id,	 // edge index 0,1,2,3 //
														&Q->gp1[gp], // should contain 1 point with dimension 1 (xi)   //
													  point[gp].normal,point[gp].tangent, &surfJ ); // n0[],t0 contains 1 point with dimension 2 (x,y) //
//			printf("fe=%d p=%d %lf : xi,eta %lf %lf : x,y %lf %lf \n",
//						 fe,gp, Q->gp1[gp].xi, Q->gp2[gp].xi,Q->gp2[gp].eta,
//						 point[gp].coord[0],point[gp].coord[1]);
		}
	}
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SurfaceQuadratureStokesGeometrySetUp"
PetscErrorCode SurfaceQuadratureStokesGeometrySetUp(SurfaceQuadratureStokes Q,DM da)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
	ierr = SurfaceQuadratureStokesCoordinatesSetUp(Q,da);CHKERRQ(ierr);
	ierr = SurfaceQuadratureStokesOrientationSetUp(Q,da);CHKERRQ(ierr);
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SurfaceQuadratureStokesGetCell"
PetscErrorCode SurfaceQuadratureStokesGetCell(SurfaceQuadratureStokes Q,PetscInt cidx,SurfaceQPointCoefficientsStokes **points)
{
  PetscFunctionBegin;
	if (cidx>=Q->nfaces) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_SIZ,"cidx > max cells");
	}
	*points = &Q->surfproperties[cidx*Q->ngp];
  PetscFunctionReturn(0);
}


/*
 
 PetscBool *eval(PetscScalar*,PetscScalar*,PetscScalar*,PetscScalar*,void*)
 
 PetscBool my_bc_evaluator( PetscScalar position[], PetscScalar normal[], PetscScalar tangent[], PetscScalar *value, void *ctx ) 
 {
   PetscBool impose_dirichlet = PETSC_FALSE;
 
   if (x<1.0) {
     *value = 10.0;
     impose_dirichlet = PETSC_TRUE;
   }
 
   return impose_dirichlet;
 }
 
*/


PetscBool SurfaceQuadatureEvaluator_constant(PetscScalar position[],PetscScalar normal[], PetscScalar tangent[],PetscScalar *value,void *ctx) 
{
	PetscBool impose_dirichlet = PETSC_TRUE;
	PetscScalar dv = *((PetscScalar*)ctx);
	*value = dv;
	return impose_dirichlet;
}

#undef __FUNCT__
#define __FUNCT__ "SurfaceQuadratureStokesTraverseSetTractionComponent"
PetscErrorCode SurfaceQuadratureStokesTraverseSetTractionComponent(SurfaceQuadratureStokes surfQ[],DMDATractionConstraintLoc loc,PetscInt dof_idx,
									PetscBool (*eval)(PetscScalar*,PetscScalar*,PetscScalar*,PetscScalar*,void*),void *ctx)
{
	PetscErrorCode ierr;
	SurfaceQuadratureStokes Q;
	ConformingElementFamily element;
	PetscBool impose_traction;
	PetscScalar traction_bc;
	PetscInt fe,gp;
	PetscFunctionBegin;


	switch (loc) {
		case DMDATracLoc_IMAX_LOC:
			Q = surfQ[QUAD_EDGE_Pxi];
			break;
		case DMDATracLoc_IMIN_LOC:
			Q = surfQ[QUAD_EDGE_Nxi];
			break;
		case DMDATracLoc_JMAX_LOC:
			Q = surfQ[QUAD_EDGE_Peta];
			break;
		case DMDATracLoc_JMIN_LOC:
			Q = surfQ[QUAD_EDGE_Neta];
			break;
	}
	
	element = Q->e;
	if (dof_idx >= 2) { SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONG,"dof_index must be < 2"); }
	
	for (fe=0; fe<Q->nfaces; fe++) {
		SurfaceQPointCoefficientsStokes *point;

		ierr = SurfaceQuadratureStokesGetCell(Q,fe,&point);CHKERRQ(ierr);
		for (gp=0; gp<Q->ngp; gp++) {

			impose_traction = PETSC_FALSE;
			impose_traction = eval(point[gp].coord,point[gp].normal,point[gp].tangent, &traction_bc,ctx);
			if (impose_traction==PETSC_TRUE) {
				point[gp].traction[ dof_idx ] = traction_bc;
			}
			
		}
	}
  PetscFunctionReturn(0);
}

PetscBool SurfaceQuadatureEvaluator_Isotropic(PetscScalar position[],PetscScalar normal[], PetscScalar tangent[],PetscScalar value[],void *ctx) 
{
	PetscBool impose_dirichlet = PETSC_TRUE;
	PetscScalar dv = *((PetscScalar*)ctx);

	value[0] = dv * normal[0];
	value[1] = dv * normal[1];
	return impose_dirichlet;
}

PetscBool SurfaceQuadatureEvaluator_Wrinkler(PetscScalar position[],PetscScalar normal[], PetscScalar tangent[],PetscScalar value[],void *ctx) 
{
	PetscBool impose_dirichlet = PETSC_TRUE;
	BC_WrinklerData wrinklerdata = (BC_WrinklerData)ctx;
	PetscScalar local_restoring_force;
    local_restoring_force = (position[1]-wrinklerdata->yref)*wrinklerdata->deltarhog+wrinklerdata->Pisos;
	value[0] = local_restoring_force * normal[0];
	value[1] = local_restoring_force * normal[1];
	return impose_dirichlet;
}

#undef __FUNCT__
#define __FUNCT__ "SurfaceQuadratureStokesTraverseSetTraction"
PetscErrorCode SurfaceQuadratureStokesTraverseSetTraction(SurfaceQuadratureStokes surfQ[],DMDATractionConstraintLoc loc,
									PetscBool (*eval)(PetscScalar*,PetscScalar*,PetscScalar*,PetscScalar*,void*),void *ctx)
{
	PetscErrorCode ierr;
	SurfaceQuadratureStokes Q;
	ConformingElementFamily element;
	PetscBool impose_traction;
	PetscScalar traction_bc[2];
	PetscInt fe,gp;
	PetscFunctionBegin;
	
	
	switch (loc) {
		case DMDATracLoc_IMAX_LOC:
			Q = surfQ[QUAD_EDGE_Pxi];
			break;
		case DMDATracLoc_IMIN_LOC:
			Q = surfQ[QUAD_EDGE_Nxi];
			break;
		case DMDATracLoc_JMAX_LOC:
			Q = surfQ[QUAD_EDGE_Peta];
			break;
		case DMDATracLoc_JMIN_LOC:
			Q = surfQ[QUAD_EDGE_Neta];
			break;
	}

	element = Q->e;
	for (fe=0; fe<Q->nfaces; fe++) {
		SurfaceQPointCoefficientsStokes *point;
		
		ierr = SurfaceQuadratureStokesGetCell(Q,fe,&point);CHKERRQ(ierr);
		for (gp=0; gp<Q->ngp; gp++) {
			
			impose_traction = PETSC_FALSE;
			impose_traction = eval(point[gp].coord,point[gp].normal,point[gp].tangent, traction_bc,ctx);
			if (impose_traction==PETSC_TRUE) {
				point[gp].traction[0] = traction_bc[0];
				point[gp].traction[1] = traction_bc[1];
			}
			
		}
	}
  PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatinOutputSurfaceQuadratureStokesPointsVTU"
PetscErrorCode pTatinOutputSurfaceQuadratureStokesPointsVTU(SurfaceQuadratureStokes surfQ,const char name[])
{
	PetscErrorCode ierr;
	PetscInt fe,n,c,ngp,npoints;
	SurfaceQPointCoefficientsStokes *quadpoints;
	FILE*	fp = NULL;
	
	if ((fp = fopen ( name, "w")) == NULL)  {
		SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
	}
	
	ngp = surfQ->ngp;
	
	npoints = surfQ->nfaces * surfQ->ngp;
	
	/* VTU HEADER - OPEN */
	fprintf(fp, "<?xml version=\"1.0\"?>\n");
	fprintf(fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
  fprintf(fp, "  <UnstructuredGrid>\n");
	fprintf(fp, "    <Piece NumberOfPoints=\"%d\" NumberOfCells=\"%d\" >\n",npoints,npoints);
	
	/* POINT COORDS */
	fprintf(fp, "    <Points>\n");
	fprintf(fp, "      <DataArray type=\"Float32\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	for (fe=0;fe<surfQ->nfaces;fe++) {
		ierr = SurfaceQuadratureStokesGetCell(surfQ,fe,&quadpoints);CHKERRQ(ierr);
		for (n=0; n<ngp; n++) {
			fprintf(fp, "      %1.4e %1.4e %1.4e \n", quadpoints[n].coord[0], quadpoints[n].coord[1], 0.0 );
		}
	}
	fprintf(fp, "      </DataArray>\n");
	fprintf(fp, "    </Points>\n");
	
	/* POINT-DATA HEADER - OPEN */
	fprintf(fp, "    <PointData>\n");
	
	/* POINT-DATA FIELDS */
	fprintf(fp, "      <DataArray type=\"Int32\" Name=\"phase\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	for (fe=0;fe<surfQ->nfaces;fe++) {
		ierr = SurfaceQuadratureStokesGetCell(surfQ,fe,&quadpoints);CHKERRQ(ierr);
		fprintf(fp, "      ");
		for (n=0; n<ngp; n++) {
			fprintf(fp, "%d ", quadpoints[n].phase );
		}
		fprintf(fp, "\n");
	}
	fprintf(fp, "      </DataArray>\n");

	/* normals */
	fprintf(fp, "      <DataArray type=\"Float32\" Name=\"normal\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	for (fe=0;fe<surfQ->nfaces;fe++) {
		ierr = SurfaceQuadratureStokesGetCell(surfQ,fe,&quadpoints);CHKERRQ(ierr);
		for (n=0; n<ngp; n++) {
			fprintf(fp, "      %1.4e %1.4e 0.0\n", quadpoints[n].normal[0], quadpoints[n].normal[1] );
		}
	}
	fprintf(fp, "      </DataArray>\n");

	/* tangent */
	fprintf(fp, "      <DataArray type=\"Float32\" Name=\"tangent\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	for (fe=0;fe<surfQ->nfaces;fe++) {
		ierr = SurfaceQuadratureStokesGetCell(surfQ,fe,&quadpoints);CHKERRQ(ierr);
		for (n=0; n<ngp; n++) {
			fprintf(fp, "      %1.4e %1.4e 0.0\n", quadpoints[n].tangent[0], quadpoints[n].tangent[1] );
		}
	}
	fprintf(fp, "      </DataArray>\n");

	/* traction */
	fprintf(fp, "      <DataArray type=\"Float32\" Name=\"traction\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	for (fe=0;fe<surfQ->nfaces;fe++) {
		ierr = SurfaceQuadratureStokesGetCell(surfQ,fe,&quadpoints);CHKERRQ(ierr);
		for (n=0; n<ngp; n++) {
			fprintf(fp, "      %1.4e %1.4e 0.0\n", quadpoints[n].traction[0], quadpoints[n].traction[1] );
		}
	}
	fprintf(fp, "      </DataArray>\n");
  
  /* POINT-DATA HEADER - CLOSE */
	fprintf(fp, "    </PointData>\n");
	
	
	
	/* UNSTRUCTURED GRID DATA */
	fprintf(fp, "    <Cells>\n");
	
	// connectivity //
	fprintf(fp, "      <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");
	fprintf(fp,"      ");		
	for (c=0; c<npoints; c++) {
		fprintf(fp,"%d ", c);
	}
	fprintf(fp,"\n");		
	fprintf(fp, "      </DataArray>\n");
	
	// offsets //
	fprintf(fp, "      <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	fprintf(fp,"      ");		
	for (c=0; c<npoints; c++) {
		fprintf(fp,"%d ", (c+1));		
	}
	fprintf(fp,"\n");		
	fprintf(fp, "      </DataArray>\n");
	
	// types //
	fprintf(fp, "      <DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\">\n");
	fprintf(fp,"      ");		
	for (c=0; c<npoints; c++) {
		fprintf(fp,"%d ", 1);		
	}
	fprintf(fp,"\n");		
	fprintf(fp, "      </DataArray>\n");
	
	fprintf(fp, "    </Cells>\n");
	
	
	/* VTU HEADER - CLOSE */
	fprintf(fp, "    </Piece>\n");
  fprintf(fp, "  </UnstructuredGrid>\n");
	fprintf(fp, "</VTKFile>\n");
	
	fclose( fp );
	PetscFunctionReturn(0);
}
///
#undef __FUNCT__  
#define __FUNCT__ "_pTatinOutputSurfaceQuadratureStokesPointsRawPVTU"
PetscErrorCode _pTatinOutputSurfaceQuadratureStokesPointsRawPVTU(const char prefix[],const char name[])
{
	PetscErrorCode ierr;
	FILE*	fp = NULL;
	PetscMPIInt i,nproc;
	char *sourcename;
	
	if ((fp = fopen ( name, "w")) == NULL)  {
		SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
	}
	
	/* PVTU HEADER - OPEN */
	fprintf(fp, "<?xml version=\"1.0\"?>\n");
	fprintf(fp, "<VTKFile type=\"PUnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
	/* define size of the nodal mesh based on the cell DM */
	fprintf(fp, "  <PUnstructuredGrid GhostLevel=\"0\">\n" ); /* note overlap = 0 */
	
	/* POINT COORDS */
	fprintf(fp, "    <PPoints>\n");
	fprintf(fp, "      <PDataArray type=\"Float32\" Name=\"Points\" NumberOfComponents=\"3\"/>\n");
	fprintf(fp, "    </PPoints>\n");
	
	/* CELL-DATA HEADER - OPEN */
	fprintf(fp, "    <PCellData>\n");
	/* CELL-DATA HEADER - CLOSE */
	fprintf(fp, "    </PCellData>\n");
	
	/* POINT-DATA HEADER - OPEN */
	fprintf(fp, "    <PPointData>\n");
	/* POINT-DATA FIELDS */
	fprintf(fp,"      <PDataArray type=\"Int32\" Name=\"phase\" NumberOfComponents=\"1\"/>\n");
	fprintf(fp,"      <PDataArray type=\"Float32\" Name=\"normal\" NumberOfComponents=\"3\"/>\n");
  fprintf(fp,"      <PDataArray type=\"Float32\" Name=\"tangent\" NumberOfComponents=\"3\"/>\n");
	fprintf(fp,"      <PDataArray type=\"Float32\" Name=\"traction\" NumberOfComponents=\"3\"/>\n");
  
  /* POINT-DATA HEADER - CLOSE */
	fprintf(fp, "    </PPointData>\n");
	
	
	/* PVTU write sources */
	MPI_Comm_size(PETSC_COMM_WORLD,&nproc);
	for (i=0; i<nproc; i++) {
		asprintf( &sourcename, "%s-subdomain%1.5d.vtu", prefix, i );
		fprintf( fp, "    <Piece Source=\"%s\"/>\n",sourcename);
		free(sourcename);
	}
	
	
	/* PVTU HEADER - CLOSE */
  fprintf(fp, "  </PUnstructuredGrid>\n");
	fprintf(fp, "</VTKFile>\n");
	
	fclose( fp );
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "_pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw"
PetscErrorCode _pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(pTatinCtx user,const char path[],const char prefix[])
{
	PetscInt e;
	char *vtkfilename,*filename;
	PetscMPIInt rank;
	PetscErrorCode ierr;
	
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	
	for (e=0; e<QUAD_EDGES; e++) {
		char *appended;
		
		asprintf(&appended,"%s_edge%.2d",prefix,e);
		ierr = pTatinGenerateParallelVTKName(appended,"vtu",&vtkfilename);CHKERRQ(ierr);
		if (path) {
			asprintf(&filename,"%s/%s",path,vtkfilename);
		} else {
			asprintf(&filename,"./%s",vtkfilename);
		}
		
		ierr = pTatinOutputSurfaceQuadratureStokesPointsVTU(user->surfQ[e],filename);CHKERRQ(ierr);
		free(filename);
		free(vtkfilename);
		
		ierr = pTatinGenerateVTKName(appended,"pvtu",&vtkfilename);CHKERRQ(ierr);
		if (path) {
			asprintf(&filename,"%s/%s",path,vtkfilename);
		} else {
			asprintf(&filename,"./%s",vtkfilename);
		}
		if (rank==0) {
			ierr = pTatinOutputSurfaceQuadratureStokesPointsRawPVTU(appended,filename);CHKERRQ(ierr);
		}
		free(filename);
		free(vtkfilename);
		free(appended);
	}
	
	PetscFunctionReturn(0);
}
///
#undef __FUNCT__  
#define __FUNCT__ "pTatinOutputSurfaceQuadratureStokesPointsRawPVTU"
PetscErrorCode pTatinOutputSurfaceQuadratureStokesPointsRawPVTU(const char prefix[],const char name[])
{
	PetscErrorCode ierr;
	FILE*	fp = NULL;
	PetscMPIInt nproc;
	PetscInt i,fe;
	char *sourcename;
	
	if ((fp = fopen ( name, "w")) == NULL)  {
		SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
	}
	
	/* PVTU HEADER - OPEN */
	fprintf(fp, "<?xml version=\"1.0\"?>\n");
	fprintf(fp, "<VTKFile type=\"PUnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
	/* define size of the nodal mesh based on the cell DM */
	fprintf(fp, "  <PUnstructuredGrid GhostLevel=\"0\">\n" ); /* note overlap = 0 */
	
	/* POINT COORDS */
	fprintf(fp, "    <PPoints>\n");
	fprintf(fp, "      <PDataArray type=\"Float32\" Name=\"Points\" NumberOfComponents=\"3\"/>\n");
	fprintf(fp, "    </PPoints>\n");
	
	/* CELL-DATA HEADER - OPEN */
	fprintf(fp, "    <PCellData>\n");
	/* CELL-DATA HEADER - CLOSE */
	fprintf(fp, "    </PCellData>\n");
	
	/* POINT-DATA HEADER - OPEN */
	fprintf(fp, "    <PPointData>\n");
	/* POINT-DATA FIELDS */
	fprintf(fp,"      <PDataArray type=\"Int32\" Name=\"phase\" NumberOfComponents=\"1\"/>\n");
	fprintf(fp,"      <PDataArray type=\"Float32\" Name=\"normal\" NumberOfComponents=\"3\"/>\n");
  fprintf(fp,"      <PDataArray type=\"Float32\" Name=\"tangent\" NumberOfComponents=\"3\"/>\n");
	fprintf(fp,"      <PDataArray type=\"Float32\" Name=\"traction\" NumberOfComponents=\"3\"/>\n");
  
  /* POINT-DATA HEADER - CLOSE */
	fprintf(fp, "    </PPointData>\n");
	
	
	/* PVTU write sources */
	MPI_Comm_size(PETSC_COMM_WORLD,&nproc);
	for (i=0; i<nproc; i++) {
		for (fe=0; fe<QUAD_EDGES; fe++) {
			asprintf( &sourcename, "%s_edge%.2d-subdomain%1.5d.vtu", prefix, fe,i );
			fprintf( fp, "    <Piece Source=\"%s\"/>\n",sourcename);
			free(sourcename);
		}
	}
	
	
	/* PVTU HEADER - CLOSE */
  fprintf(fp, "  </PUnstructuredGrid>\n");
	fprintf(fp, "</VTKFile>\n");
	
	fclose( fp );
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw"
PetscErrorCode pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(pTatinCtx user,const char path[],const char prefix[])
{
	PetscInt e;
	char *vtkfilename,*filename;
	PetscMPIInt rank;
	char *appended;
	PetscErrorCode ierr;
	
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	
	for (e=0; e<QUAD_EDGES; e++) {
		
		asprintf(&appended,"%s_edge%.2d",prefix,e);
		ierr = pTatinGenerateParallelVTKName(appended,"vtu",&vtkfilename);CHKERRQ(ierr);
		if (path) {
			asprintf(&filename,"%s/%s",path,vtkfilename);
		} else {
			asprintf(&filename,"./%s",vtkfilename);
		}
		
		ierr = pTatinOutputSurfaceQuadratureStokesPointsVTU(user->surfQ[e],filename);CHKERRQ(ierr);
		free(filename);
		free(vtkfilename);
		free(appended);
	}
		
	asprintf(&appended,"%s_alledges",prefix);
	ierr = pTatinGenerateVTKName(appended,"pvtu",&vtkfilename);CHKERRQ(ierr);
	if (path) {
		asprintf(&filename,"%s/%s",path,vtkfilename);
	} else {
		asprintf(&filename,"./%s",vtkfilename);
	}
	if (rank==0) { /* not we are a bit tricky about which name we pass in here to define the edge data sets */
		ierr = pTatinOutputSurfaceQuadratureStokesPointsRawPVTU(prefix,filename);CHKERRQ(ierr);
	}
	free(filename);
	free(vtkfilename);
	free(appended);
	
	PetscFunctionReturn(0);
}

/* testing */
/* deform mesh for testing normals */
#undef __FUNCT__  
#define __FUNCT__ "deform_mesh_splay"
PetscErrorCode deform_mesh_splay(pTatinCtx ctx)
{
	DM         da,cda;
	Vec        coordinates,gcoords;
	DMDACoor2d **LA_gcoords;
	PetscInt   i,j,si,sj,ni,nj;
	
	PetscErrorCode ierr;
	PetscFunctionBegin;
	
	da = ctx->dav;
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(da,&gcoords);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,gcoords,&LA_gcoords);CHKERRQ(ierr);
	ierr = DMDAGetCorners(cda,&si,&sj,0,&ni,&nj,0);CHKERRQ(ierr);
	
	for (i=si; i<si+ni; i++) {
		for (j=sj; j<sj+nj; j++) {
			PetscScalar xn,yn,scale;
			xn = LA_gcoords[j][i].x;
			yn = LA_gcoords[j][i].y;
			
			scale = xn-0.5; // -0.5 < scale < 0.5
			LA_gcoords[j][i].x = xn + (yn-0.5)*(yn-0.5)*scale;
			
			scale = yn-0.5; // -0.5 < scale < 0.5
			LA_gcoords[j][i].y = yn + (xn+0.5)*(xn+0.5)*scale;
		}
	}
	
	ierr = DMDAVecRestoreArray(cda,gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetCoordinates(da,&coordinates);CHKERRQ(ierr);
	ierr = DMLocalToGlobalBegin(cda,gcoords,INSERT_VALUES,coordinates);CHKERRQ(ierr);
	ierr = DMLocalToGlobalEnd  (cda,gcoords,INSERT_VALUES,coordinates);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "deform_mesh_buldge"
PetscErrorCode deform_mesh_buldge(pTatinCtx ctx)
{
	DM         da,cda;
	Vec        gcoords,coordinates;
	DMDACoor2d **LA_gcoords;
	PetscInt   i,j,si,sj,ni,nj;
	
	PetscErrorCode ierr;
	PetscFunctionBegin;
	
	da = ctx->dav;
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(da,&gcoords);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,gcoords,&LA_gcoords);CHKERRQ(ierr);
	ierr = DMDAGetCorners(cda,&si,&sj,0,&ni,&nj,0);CHKERRQ(ierr);
	
	for (i=si; i<si+ni; i++) {
		for (j=sj; j<sj+nj; j++) {
			PetscScalar xn,yn,scale;
			xn = LA_gcoords[j][i].x;
			yn = LA_gcoords[j][i].y;
			
			scale = xn-0.5; // -0.5 < scale < 0.5
			LA_gcoords[j][i].x = xn + (yn-0.5)*(yn-0.5)*scale;
			scale = yn-0.5; // -0.5 < scale < 0.5
			LA_gcoords[j][i].y = yn - (xn-0.5)*(xn-0.5)*scale;
		}
	}
	
	ierr = DMDAVecRestoreArray(cda,gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetCoordinates(da,&coordinates);CHKERRQ(ierr);
	ierr = DMLocalToGlobalBegin(cda,gcoords,INSERT_VALUES,coordinates);CHKERRQ(ierr);
	ierr = DMLocalToGlobalEnd  (cda,gcoords,INSERT_VALUES,coordinates);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_TestSurfaceQuadratureStokes"
PetscErrorCode pTatin2d_TestSurfaceQuadratureStokes(pTatinCtx ctx)
{
	Vec X;
	PetscInt e;
	PetscErrorCode ierr;
	PetscFunctionBegin;
	
	ierr = DMDASetUniformCoordinates(ctx->dav,0.0,1.0, 0.0,1.0, 0.0,0.0);CHKERRQ(ierr);

	ierr = deform_mesh_splay(ctx);CHKERRQ(ierr);
	ierr = deform_mesh_buldge(ctx);CHKERRQ(ierr);
	
	ierr = QuadratureStokesCoordinateSetUp(ctx->Q,ctx->dav);CHKERRQ(ierr);
	for (e=0; e<QUAD_EDGES; e++) {
		ierr = SurfaceQuadratureStokesGeometrySetUp(ctx->surfQ[e],ctx->dav);CHKERRQ(ierr);
	}
	
	ierr = DMGetGlobalVector(ctx->pack,&X);CHKERRQ(ierr);
	ierr = VecZeroEntries(X);CHKERRQ(ierr);
	ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,"SurfQTest_vp");CHKERRQ(ierr);
	ierr = pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(ctx,ctx->outputpath,"SurfQTest_faceqpoints");CHKERRQ(ierr);
	ierr = DMRestoreGlobalVector(ctx->pack,&X);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

/* defined in element_type_Q2.c */
extern void evaluate_divF( const int type, double xp[], double *div );
extern void evaluate_F( const int type, double xp[], double *Fx,double *Fy,double *Fz );

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_TestSurfaceQuadratureStokes2"
PetscErrorCode pTatin2d_TestSurfaceQuadratureStokes2(pTatinCtx ctx)
{
	DM da,cda;
	PetscErrorCode ierr;
	Vec            gcoords;
	PetscScalar    *LA_gcoords;
	PetscInt       nel,nen,fe,k,p,e,gp,nfaces;
	const PetscInt *elnidx;
	PetscScalar    surfJ,volJ,elcoords[2*Q2_NODES_PER_EL_2D];
	ConformingElementFamily element;
	double XP[3],ii_F_dot_n,ii_F_dot_nG,F_dot_n,fx_p,fy_p,fz_p,Fn;
	double ii_div_F,ii_div_FG,div_F;
	int func_type = 0;
	double __GNi[9*2], __GNx[9*2];
	double *GNi[2];
	double *GNx[2];
	
	PetscFunctionBegin;
	ierr = DMDASetUniformCoordinates(ctx->dav,0.0,1.0, 0.0,1.0, 0.0,0.0);CHKERRQ(ierr);

	/* choose a deformed mesh */
//	ierr = deform_mesh_splay(ctx);CHKERRQ(ierr);
//	ierr = deform_mesh_buldge(ctx);CHKERRQ(ierr);
	
	ierr = QuadratureStokesCoordinateSetUp(ctx->Q,ctx->dav);CHKERRQ(ierr);
	for (e=0; e<QUAD_EDGES; e++) {
		ierr = SurfaceQuadratureStokesGeometrySetUp(ctx->surfQ[e],ctx->dav);CHKERRQ(ierr);
	}
	
	/* setup for coords */
	da = ctx->dav;
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(da,&gcoords);CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(da,&nel,&nen,&elnidx);CHKERRQ(ierr);

	
	/* VOLUME INTEGRATION */
	GNi[0] = &__GNi[0];
	GNi[1] = &__GNi[9*1];
	
	GNx[0] = &__GNx[0];
	GNx[1] = &__GNx[9*1];
	
	ii_div_F = 0.0;
	element = ctx->surfQ[0]->e; /* take any of these - we are just going to call the BasisFuncGrad() and ComputeGeom functions */
	for (e=0; e<nel; e++) {
		GaussPointCoefficientsStokes *point;

		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx[nen*e],LA_gcoords);CHKERRQ(ierr);
		ierr = QuadratureStokesGetCell(ctx->Q,e,&point);CHKERRQ(ierr);

		for (gp=0; gp<ctx->Q->ngp; gp++) {
			QPoint2d tmp_point;
			
			tmp_point.xi  = ctx->Q->xi[2*gp  ];
			tmp_point.eta = ctx->Q->xi[2*gp+1];
			tmp_point.w   = ctx->Q->weight[gp];
			
			element->basis_GNI_2D(&tmp_point,GNi);
			element->compute_volume_geometry_2D(elcoords,(const double**)GNi,GNx,&volJ);

			XP[0] = point[gp].coord[0];
			XP[1] = point[gp].coord[1];
			XP[2] = 0.0;
			evaluate_divF( func_type, XP, &div_F );
			
			ii_div_F = ii_div_F + tmp_point.w * div_F * volJ;
		}
	}
	ii_div_FG = 0.0;
	MPI_Allreduce(&ii_div_F,&ii_div_FG,1,MPI_DOUBLE,MPI_SUM,PETSC_COMM_WORLD);
	PetscPrintf(PETSC_COMM_WORLD,"int_vol( div(F) ) = %+1.6lf \n", ii_div_FG );
	
	
	/* SURFACE INTEGRATION */
	ii_F_dot_n = 0.0;
	for (k=0; k<QUAD_EDGES; k++) {
		element = ctx->surfQ[k]->e;
		nfaces  = ctx->surfQ[k]->nfaces;
		
		for (fe=0; fe<nfaces; fe++) {
			double normal[2],tangent[2];
			SurfaceQPointCoefficientsStokes *point;

			e = ctx->surfQ[k]->cell_list[fe];
			ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx[nen*e],LA_gcoords);CHKERRQ(ierr);
			
			ierr = SurfaceQuadratureStokesGetCell(ctx->surfQ[k],fe,&point);CHKERRQ(ierr);
			
			for (gp=0; gp<ctx->surfQ[k]->ngp; gp++) {
				element->compute_surface_geometry_2D(	
																						 element, 
																						 elcoords,    // should contain 9 points with dimension 2 (x,y) // 
																						 ctx->surfQ[k]->edge_id,	 // edge index 0,1,2,3 //
																						 &ctx->surfQ[k]->gp1[gp], // should contain 1 point with dimension 1 (xi)   //
																						 normal,tangent, &surfJ ); // n0[],t0 contains 1 point with dimension 2 (x,y) //
				/* compute integral contribution */
				XP[0] = point[gp].coord[0];
				XP[1] = point[gp].coord[1];
				XP[2] = 0.0;
				evaluate_F( func_type, XP, &fx_p, &fy_p, &fz_p );
				F_dot_n = fx_p * normal[0] + fy_p * normal[1];
				ii_F_dot_n = ii_F_dot_n + ctx->surfQ[k]->gp1[gp].w * F_dot_n * surfJ;
			
			}
		}
		
	}
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);

	ii_F_dot_nG = 0.0;
	MPI_Allreduce(&ii_F_dot_n,&ii_F_dot_nG,1,MPI_DOUBLE,MPI_SUM,PETSC_COMM_WORLD);
	PetscPrintf(PETSC_COMM_WORLD,"int_surf( F.n )   = %+1.6lf \n", ii_F_dot_nG );
	
	
	PetscFunctionReturn(0);
}


