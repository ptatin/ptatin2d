
static const char help[] = "Stokes solver using Q2-Pm1 mixed finite elements.\n"
"2D prototype of the (p)ragmatic version of Tatin. (pTatin2d_v0.0)\n\n";

#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"
#include "pTatin2d_models.h"

extern PetscErrorCode DMGetInterpolation_P1(DM dac,DM daf,Mat *A,Vec *X);


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_full_gmg"
PetscErrorCode pTatin2d_full_gmg(int argc,char **argv)
{
	PetscInt        k,nlevels;
	DM							*pack_hierarchy, dau_k, dap_k;
	Mat             *interpolatation_u, *interpolatation_p, *interpolatation;
	Mat             *restriction_u, *restriction_p, *restriction;
	DM              multipys_pack;
	IS              *isg;
	SNES            snes;
	Mat             A[10],B[10],Auu,Aup,Apu,Spp;
	Vec             X,F;
	KSP             ksp;
	PC              pc;
	pTatinCtx       user[10];
	PetscInt        e,tk;
	PetscReal       time;
	PetscErrorCode  ierr;
	
	
  nlevels = 1;
	PetscOptionsGetInt(PETSC_NULL,"-daup_nlevels",&nlevels,0);

	for (k=0; k<nlevels; k++) {
		ierr = pTatin2dCreateContext(&user[k]);CHKERRQ(ierr);
		ierr = pTatin2dParseOptions(user[k]);CHKERRQ(ierr);
		ierr = pTatin2dCreateQ2PmMesh(user[k]);CHKERRQ(ierr);
		if (k!=nlevels-1) {
			ierr = DMDestroy(&user[k]->dav);CHKERRQ(ierr);
			ierr = DMDestroy(&user[k]->dap);CHKERRQ(ierr);
		}
	}	
	multipys_pack = user[nlevels-1]->pack;
	
/*
	ierr = PetscMalloc(sizeof(DM)*nlevels,&dav_hierarchy);CHKERRQ(ierr);
	dav_hierarchy[ nlevels-1 ] = user->dav;
	ierr = PetscObjectReference((PetscObject)user->dav);CHKERRQ(ierr);

	ierr = PetscMalloc(sizeof(DM)*nlevels,&dap_hierarchy);CHKERRQ(ierr);
	dap_hierarchy[ nlevels-1 ] = user->dap;
	ierr = PetscObjectReference((PetscObject)user->dap);CHKERRQ(ierr);
*/
	ierr = PetscMalloc(sizeof(DM)*nlevels,&pack_hierarchy);CHKERRQ(ierr);
	pack_hierarchy[ nlevels-1 ] = multipys_pack;
	ierr = PetscObjectReference((PetscObject)multipys_pack);CHKERRQ(ierr);
	
	/* option 1 - simply coarsen nlevels - 1 times */
/*
	{
		DM *coarsened_list;
		ierr = PetscMalloc(sizeof(DM)*(nlevels-1),&coarsened_list);CHKERRQ(ierr);
		ierr = DMCoarsenHierarchy(user->dav,nlevels-1,coarsened_list);CHKERRQ(ierr);
		for (k=0; k<nlevels-1; k++) {
			dav_hierarchy[ nlevels-2-k ] = coarsened_list[k];
		}
		PetscFree(coarsened_list);

		for (k=0; k<nlevels; k++) {
			PetscPrintf(PETSC_COMM_WORLD,"level[%d] :\n", k );
			ierr = DMView(dav_hierarchy[k],PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
		}
	}
	{
		DM *coarsened_list;
		ierr = PetscMalloc(sizeof(DM)*(nlevels-1),&coarsened_list);CHKERRQ(ierr);
		ierr = DMCoarsenHierarchy(user->dap,nlevels-1,coarsened_list);CHKERRQ(ierr);
		for (k=0; k<nlevels-1; k++) {
			dap_hierarchy[ nlevels-2-k ] = coarsened_list[k];
		}
		
		PetscFree(coarsened_list);
	}
*/
	
	{
		DM *coarsened_list;

		ierr = PetscMalloc(sizeof(DM)*(nlevels-1),&coarsened_list);CHKERRQ(ierr);
		ierr = DMCoarsenHierarchy(multipys_pack,nlevels-1,coarsened_list);CHKERRQ(ierr);
		for (k=0; k<nlevels-1; k++) {
			pack_hierarchy[ nlevels-2-k ] = coarsened_list[k];
		}
		
		for (k=0; k<nlevels-1; k++) {
			ierr = DMCompositeGetEntries(pack_hierarchy[k],  &dau_k,&dap_k);CHKERRQ(ierr);

			user[k]->dav = dau_k;
			user[k]->dap = dap_k;
			ierr = PetscObjectReference((PetscObject)user[k]->dav);CHKERRQ(ierr);
			ierr = PetscObjectReference((PetscObject)user[k]->dap);CHKERRQ(ierr);
//			ierr = DMDASetElementType_Q2(dau_k);CHKERRQ(ierr);
//			ierr = DMDASetElementType_P1(dap_k);CHKERRQ(ierr);
		}
		
		PetscFree(coarsened_list);
	}

	/* prolongation/interpolation */
	ierr = PetscMalloc(sizeof(Mat)*nlevels,&interpolatation_u);CHKERRQ(ierr);
	interpolatation_u[0] = PETSC_NULL;
	for (k=0; k<nlevels-1; k++) {
		DM dau_c,dau_f,dap_c,dap_f;

		ierr = DMCompositeGetEntries(pack_hierarchy[k],  &dau_c,&dap_c);CHKERRQ(ierr);
		ierr = DMCompositeGetEntries(pack_hierarchy[k+1],&dau_f,&dap_f);CHKERRQ(ierr);

		ierr = DMGetInterpolation(dau_c,dau_f,&interpolatation_u[k+1],PETSC_NULL);CHKERRQ(ierr);
	}
	
	ierr = PetscMalloc(sizeof(Mat)*nlevels,&interpolatation_p);CHKERRQ(ierr);
	interpolatation_p[0] = PETSC_NULL;
	for (k=0; k<nlevels-1; k++) {
		DM dau_c,dau_f,dap_c,dap_f;
		
		ierr = DMCompositeGetEntries(pack_hierarchy[k],  &dau_c,&dap_c);CHKERRQ(ierr);
		ierr = DMCompositeGetEntries(pack_hierarchy[k+1],&dau_f,&dap_f);CHKERRQ(ierr);

		ierr = MatCreateProlongationP1(dap_c,dap_f,&interpolatation_p[k+1]);CHKERRQ(ierr);
	}
	
	/* restriction */
	ierr = PetscMalloc(sizeof(Mat)*nlevels,&restriction_u);CHKERRQ(ierr);
	restriction_u[0] = PETSC_NULL;
	for (k=1; k<nlevels; k++) {
		ierr = MatCreateTranspose(interpolatation_u[k],&restriction_u[k]);CHKERRQ(ierr);
	}

	ierr = PetscMalloc(sizeof(Mat)*nlevels,&restriction_p);CHKERRQ(ierr);
	restriction_p[0] = PETSC_NULL;
	for (k=0; k<nlevels-1; k++) {
		DM dau_c,dau_f,dap_c,dap_f;
		
		ierr = DMCompositeGetEntries(pack_hierarchy[k],  &dau_c,&dap_c);CHKERRQ(ierr);
		ierr = DMCompositeGetEntries(pack_hierarchy[k+1],&dau_f,&dap_f);CHKERRQ(ierr);

		ierr = MatCreateRestrictionP1(dap_c,dap_f,&restriction_p[k+1]);CHKERRQ(ierr);
	}
	
	/* nest restriction */
	ierr = PetscMalloc(sizeof(Mat)*nlevels,&interpolatation);CHKERRQ(ierr);
	interpolatation[0] = PETSC_NULL;
	ierr = PetscMalloc(sizeof(Mat)*nlevels,&restriction);CHKERRQ(ierr);
	restriction[0] = PETSC_NULL;

	for (k=0; k<nlevels-1; k++) {
		DM dau_c,dau_f,dap_c,dap_f;
		IS *is_c,*is_f;
		Mat bA[2][2];
		
		ierr = DMCompositeGetEntries(pack_hierarchy[k],  &dau_c,&dap_c);CHKERRQ(ierr);
		ierr = DMCompositeGetEntries(pack_hierarchy[k+1],&dau_f,&dap_f);CHKERRQ(ierr);

		ierr = DMCompositeGetGlobalISs(pack_hierarchy[k],  &is_c);CHKERRQ(ierr);
		ierr = DMCompositeGetGlobalISs(pack_hierarchy[k+1],&is_f);CHKERRQ(ierr);

		bA[0][0] = interpolatation_u[k+1];    bA[0][1] = PETSC_NULL;
		bA[1][0] = PETSC_NULL;                bA[1][1] = interpolatation_p[k+1];
		ierr = MatCreateNest(PETSC_COMM_WORLD,2,is_f,2,is_c,&bA[0][0],&interpolatation[k+1]);CHKERRQ(ierr);
		ierr = MatAssemblyBegin(interpolatation[k+1],MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
		ierr = MatAssemblyEnd(interpolatation[k+1],MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

		bA[0][0] = restriction_u[k+1];    bA[0][1] = PETSC_NULL;
		bA[1][0] = PETSC_NULL;            bA[1][1] = restriction_p[k+1];
		ierr = MatCreateNest(PETSC_COMM_WORLD,2,is_c,2,is_f,&bA[0][0],&restriction[k+1]);CHKERRQ(ierr);
		ierr = MatAssemblyBegin(restriction[k+1],MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
		ierr = MatAssemblyEnd(restriction[k+1],MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

		
		ierr = ISDestroy(&is_c[0]);CHKERRQ(ierr);
		ierr = ISDestroy(&is_c[1]);CHKERRQ(ierr);
		ierr = PetscFree(is_c);CHKERRQ(ierr);
		ierr = ISDestroy(&is_f[0]);CHKERRQ(ierr);
		ierr = ISDestroy(&is_f[1]);CHKERRQ(ierr);
		ierr = PetscFree(is_f);CHKERRQ(ierr);
	}
	
	
	for (k=0; k<nlevels; k++) {
		ierr = pTatin2dCreateBoundaList(user[k]);CHKERRQ(ierr);
		ierr = pTatin2dCreateQuadratureStokes(user[k]);CHKERRQ(ierr);
		ierr = pTatin2dCreateMaterialPoints(user[k]);CHKERRQ(ierr);
		
		/* mesh geometry */
		ierr = pTatin2d_ModelApplyInitialMeshGeometry(user[k]);CHKERRQ(ierr);
		
		/* interpolate point coordinates (needed if mesh was modified) */
		ierr = QuadratureStokesCoordinateSetUp(user[k]->Q,user[k]->dav);CHKERRQ(ierr);
		ierr = MaterialPointCoordinateSetUp(user[k]->db,user[k]->dav);CHKERRQ(ierr);
		
		for (e=0; e<QUAD_EDGES; e++) {
			ierr = SurfaceQuadratureStokesGeometrySetUp(user[k]->surfQ[e],user[k]->dav);CHKERRQ(ierr);
		}
		
		
		/* material geometry */
		ierr = pTatin2d_ModelApplyInitialMaterialGeometry(user[k]);CHKERRQ(ierr);
		
		/* boundary conditions */
		ierr = pTatin2d_ModelApplyBoundaryCondition(user[k]);CHKERRQ(ierr);
	}
	
	ierr = DMCreateGlobalVector(multipys_pack,&X);CHKERRQ(ierr);
  ierr = VecDuplicate(X,&F);CHKERRQ(ierr);
	
	ierr = pTatin2d_ModelOutput(user[nlevels-1],X,"ic_bc");CHKERRQ(ierr);

	for (k=0; k<nlevels; k++) {
		PetscScalar       *LAu,*LAp;
		Vec               Xl,Uloc,Ploc;
		
		
		Auu = Aup = Apu = Spp = PETSC_NULL;

		ierr = DMCompositeStokes_MatCreate(pack_hierarchy[k],MATAIJ,&Auu,MATAIJ,&Aup,MATAIJ,PETSC_NULL,MATAIJ,&Spp);CHKERRQ(ierr);
		ierr = DMCompositeStokes_MatCreate(pack_hierarchy[k],MATAIJ,PETSC_NULL,MATAIJ,PETSC_NULL,MATAIJ,&Apu,MATAIJ,PETSC_NULL);CHKERRQ(ierr);

		ierr = DMCompositeStokesCreateNest(pack_hierarchy[k],Auu,Aup,Apu,PETSC_NULL,&A[k]);CHKERRQ(ierr);
		ierr = DMCompositeStokesCreateNest(pack_hierarchy[k],Auu,Aup,Apu,Spp,&B[k]);CHKERRQ(ierr);

		
		ierr = DMCreateGlobalVector(pack_hierarchy[k],&Xl);CHKERRQ(ierr);
		ierr = DMCompositeGetEntries(pack_hierarchy[k],&dau_k,&dap_k);CHKERRQ(ierr);
		ierr = DMCompositeGetLocalVectors(pack_hierarchy[k],&Uloc,&Ploc);CHKERRQ(ierr);
		ierr = DMCompositeScatter(pack_hierarchy[k],Xl,Uloc,Ploc);CHKERRQ(ierr);
		ierr = VecGetArray(Uloc,&LAu);CHKERRQ(ierr);
		ierr = VecGetArray(Ploc,&LAp);CHKERRQ(ierr);
		ierr = pTatin_EvaluateRheologyNonlinearitiesMarkers(user[k],dau_k,LAu,dap_k,LAp);CHKERRQ(ierr);
		ierr = VecDestroy(&Xl);CHKERRQ(ierr);
		
		ierr = Assemble_Stokes_A11_Q2(user[k],dau_k,LAu,dap_k,LAp,Auu);CHKERRQ(ierr);
		ierr = Assemble_Stokes_A12_Q2(user[k],dau_k,LAu,dap_k,LAp,Aup);CHKERRQ(ierr);
		ierr = Assemble_Stokes_A21_Q2(user[k],dau_k,LAu,dap_k,LAp,Apu);CHKERRQ(ierr);
		
		ierr = Assemble_Stokes_B22_P1(user[k],dau_k,LAu,dap_k,LAp,Spp);CHKERRQ(ierr);
		
		if (Auu) { ierr = MatDestroy(&Auu);CHKERRQ(ierr); }
		if (Aup) { ierr = MatDestroy(&Aup);CHKERRQ(ierr); }
		if (Apu) { ierr = MatDestroy(&Apu);CHKERRQ(ierr); }
		if (Spp) { ierr = MatDestroy(&Spp);CHKERRQ(ierr); }		
	}
	
	/* ====== Generate an initial condition for Newton ====== */
	time = 0.0;
	ierr = pTatin2d_ComputeTimestep(user[nlevels-1],multipys_pack,X);CHKERRQ(ierr);
//	for (tk=1; tk<=user->nsteps; tk++) {
	for (tk=1; tk<2; tk++) {
		time = time + user[nlevels-1]->dt;
		
    user[nlevels-1]->time = time; 
    user[nlevels-1]->step = tk;
		
    PetscPrintf(PETSC_COMM_WORLD,"step = %1.5d, time = %1.4e, dt = %1.4e \n", tk, time, user[nlevels-1]->dt );
    
    
    /* boundary conditions */
		for (k=0; k<nlevels; k++) {
			ierr = pTatin2d_ModelApplyBoundaryCondition(user[k]);CHKERRQ(ierr);
			ierr = pTatin2d_ModelApplyMaterialBoundaryCondition(user[k]);CHKERRQ(ierr);
		}
		
		/* creation */
		ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
		
		ierr = SNESSetFunction(snes,F,FormFunction_Stokes,user[nlevels-1]);CHKERRQ(ierr);  
		if (user[nlevels-1]->rheology_constants.rheology_type == RHEOLOGY_VISCO_ELASTIC_PLASTIC ) {
			ierr = SNESSetFunction(snes,F,FormFunction_Stokes_ViscoElastoPlastic,user[nlevels-1]);CHKERRQ(ierr);  
		}
		
		if (user[nlevels-1]->use_mf_stokes) {
			ierr = SNESSetJacobian(snes,B[nlevels-1],B[nlevels-1],FormJacobian_Stokes,user[nlevels-1]);CHKERRQ(ierr);
		} else {
			ierr = SNESSetJacobian(snes,A[nlevels-1],B[nlevels-1],FormJacobian_Stokes,user[nlevels-1]);CHKERRQ(ierr);
		}
		ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
		
		/* configure for fieldsplit */
		ierr = SNESGetKSP(snes,&ksp);CHKERRQ(ierr);
		//ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
		ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
		
		ierr = PCSetType(pc,PCMG);CHKERRQ(ierr);
		ierr = PCMGSetLevels(pc,nlevels,PETSC_NULL);CHKERRQ(ierr);
		ierr = PCMGSetType(pc,PC_MG_MULTIPLICATIVE);CHKERRQ(ierr);
		ierr = PCMGSetGalerkin(pc,PETSC_FALSE);CHKERRQ(ierr);
		
		for( k=1; k<nlevels; k++ ){
			ierr = PCMGSetInterpolation(pc,k,interpolatation[k]);CHKERRQ(ierr);
			ierr = PCMGSetRestriction(pc,k,restriction[k]);CHKERRQ(ierr);
		}

		
		/* configure up split on each multi-grid level */
		for( k=0; k<nlevels; k++ ){
			KSP      ksp_i;
			
			ierr = PCMGGetSmoother(pc,k,&ksp_i);CHKERRQ(ierr);
			ierr = KSPSetOperators(ksp_i,A[k],B[k],DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);
			//ierr = KSPSetDM(ksp_i,pack_hierarchy[k]);CHKERRQ(ierr);
		}
		//ierr = KSPSetUp(ksp);CHKERRQ(ierr);
		
#if 1
		for( k=0; k<nlevels; k++ ){
			PetscInt nsplits;
			PC       pc_i;
			KSP      *sub_ksp,ksp_coarse,ksp_i;
			
			ierr = PCMGGetSmoother(pc,k,&ksp_i);CHKERRQ(ierr);
			ierr = KSPSetOperators(ksp_i,A[k],B[k],DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);

			ierr = KSPGetPC(ksp_i,&pc_i);CHKERRQ(ierr);
			ierr = DMCompositeGetGlobalISs(pack_hierarchy[k],&isg);CHKERRQ(ierr);
			ierr = PCSetType(pc_i,PCFIELDSPLIT);CHKERRQ(ierr);
			ierr = PCFieldSplitSetIS(pc_i,"u",isg[0]);CHKERRQ(ierr);
			ierr = PCFieldSplitSetIS(pc_i,"p",isg[1]);CHKERRQ(ierr);

			ierr = ISDestroy(&isg[0]);CHKERRQ(ierr);
			ierr = ISDestroy(&isg[1]);CHKERRQ(ierr);
			ierr = PetscFree(isg);CHKERRQ(ierr);
		}
#endif
		
		
		ierr = pTatinSetSNESMonitor(snes,user[nlevels-1]);CHKERRQ(ierr);

		ierr = SNESSolve(snes,PETSC_NULL,X);CHKERRQ(ierr);
		

		
		
		
		
		/* output */
		if (tk%user[nlevels-1]->output_frequency==0) {
			char *prefix;
			asprintf(&prefix,"step%1.5d",tk);
			ierr = pTatin2d_ModelOutput(user[nlevels-1],X,prefix);CHKERRQ(ierr);
			free(prefix);
		}

		/* tidy up */
		ierr = SNESDestroy(&snes);CHKERRQ(ierr);
	}
	
	for (k=1; k<nlevels; k++) {
		ierr = MatDestroy(&interpolatation_u[k]);CHKERRQ(ierr);
		ierr = MatDestroy(&interpolatation_p[k]);CHKERRQ(ierr);
	}
	ierr = PetscFree(interpolatation_u);CHKERRQ(ierr);
	ierr = PetscFree(interpolatation_p);CHKERRQ(ierr);

	for (k=0; k<nlevels; k++) {
		ierr = DMDestroy(&pack_hierarchy[k]);CHKERRQ(ierr);
	}
	ierr = PetscFree(pack_hierarchy);CHKERRQ(ierr);
	
	for (k=0; k<nlevels; k++) {
		if (!user[nlevels-1]->use_mf_stokes) {
			ierr = MatDestroy(&A[k]);CHKERRQ(ierr);
		}
		ierr = MatDestroy(&B[k]);CHKERRQ(ierr);
	}
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
	
	for (k=0; k<nlevels-1; k++) {
		ierr = pTatin2dDestroyContext(&user[k]);
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc,char **argv)
{
	PetscErrorCode ierr;
	
	ierr = PetscInitialize(&argc,&argv,0,help);CHKERRQ(ierr);

	ierr = pTatinWriteOptionsFile(PETSC_NULL);CHKERRQ(ierr);
	
	ierr = pTatin2d_full_gmg(argc,argv);CHKERRQ(ierr);

	ierr = PetscFinalize();CHKERRQ(ierr);
	return 0;
}
