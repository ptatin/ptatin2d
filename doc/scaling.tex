\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}

\newcommand{\ptat}{\tt pTatin}

\title{{\ptat}: Stokes Non-Dimensional Scaling}
\author{DAM}

\begin{document}
\maketitle

\section{Governing Equations}

\subsection{Viscous flow}
Momentum equation for creeping flow:
\begin{equation}
	\Bigl[ \eta D_{ij}(\boldsymbol u) \Bigr]_{,j} - p_{,i} = \rho g_i,
	\label{eq:mom}
\end{equation}
where the strain-rate operator is given by
\begin{equation}
	D_{ij}(\boldsymbol u) := \tfrac{1}{2} \left( u_{i,j} + u_{j,i} \right).
\end{equation}
We assume incompressibilty (simplified mass conservation):
\begin{equation}
	u_{k,k} = 0
	\label{eq:cont}
\end{equation}

\subsection{Heat transport}
The conservation of energy is 
\begin{equation}
	{\rho C_p} \left( \frac{\partial T}{ \partial t} + u_i T_{,i} \right) = \left( k T_{,i} \right)_{,i} + H
	\label{eq:energy}
\end{equation}

\section{Non-Dimensional Scaling}
\subsection{Viscous flow}
We {\it choose} the following dependent non-dimensional variables
{\setlength\arraycolsep{0.2em}
\begin{eqnarray}
	x_i &=& L x_i^*		\label{eq:x_nd}\\
	u_i &=& U u_i^*		\label{eq:u_nd}\\
	\eta &=& \hat{\eta} \eta^*	\label{eq:eta_nd}
\end{eqnarray}
}
Substituting Eqns.~\eqref{eq:x_nd},\eqref{eq:u_nd},\eqref{eq:eta_nd} into Eqns.~\eqref{eq:mom},\eqref{eq:cont} yields
\begin{equation}
	\left( \frac{\hat{\eta} U}{L^2} \right) \frac{1}{2} \Bigl[ \eta^*  \left( u^*_{i,j^*} + u^*_{j,i^*} \right)		 \Bigr]_{,j} 
	- \left( \frac{1}{L} \right) p_{,i^*} = \rho g_i,
\end{equation}
and
\begin{equation}
	u^*_{k,k^*} = 0.
\end{equation}
Here the sub-scripts $i^*$ indicate that the spatial derivative is taken with with respect to the non-dimensional variable $x^*_i$. Re-arranging the scaling parameters in the momentum equations yields
\begin{equation}
	 \frac{1}{2} \Bigl[ \eta^*  \left( u^*_{i,j^*} + u^*_{j,i^*} \right)		 \Bigr]_{,j^*} 
	- \left( \frac{L}{\hat{\eta} U} \right) p_{,i^*} = \left( \frac{L^2}{\hat{\eta} U} \right) \rho g_i,
\end{equation}
The pressure and density are scaled according to
{\setlength\arraycolsep{0.2em}
\begin{eqnarray}
	p &=& \hat{p} p^* \\
	f_i &=& \rho g_i = \hat{f} f^*_i
\end{eqnarray}
}
where $\hat{p}$ is chosen according to
\begin{equation}
	\hat{p} = \frac{\hat{\eta} U}{L}.
\end{equation}
and $\hat{\rho}$ is chosen according to
\begin{equation}
	\hat{f} = \frac{\hat{\eta} U}{L^2}.
\end{equation}
Collectively this choice of scales leads to the following momentum equation
\begin{equation}
	 \frac{1}{2} \Bigl[ \eta^*  \left( u^*_{i,j^*} + u^*_{j,i^*} \right)		 \Bigr]_{,j^*} 
	- p^*_{,i^*} 
	= \hat{f} f^*_i,
\end{equation}
Note that time scale $\hat{t}$
\begin{equation}
	t = \hat{t} t^*
	\label{eq:t_nd}
\end{equation}
is defined via
\begin{equation}
	\hat{t} = \frac{L}{U}.
\end{equation}

Note, one could also obtain a density scale
$$
	\rho = \hat{\rho} \rho^*
$$
via
$$
	\hat{\rho} = \frac{\hat{\eta} U}{L^2} \times \frac{\hat{t}^2}{L} = \frac{\hat{\eta}}{UL},
$$
and a gravity scale
$$
	g_i= \hat{g} g_i^* 
$$
via
$$
	\hat{g} = \frac{\hat{\eta} U}{L^2} \times \frac{1}{\hat{\rho}} = \frac{U^2}{L}.
$$



\subsection{Heat transport (Form A)}
We write the conservation of energy as
\begin{equation}
	\left( \frac{\partial T}{ \partial t} + u_i T_{,i} \right) = \left(  \frac{k}{\rho C_p}  T_{,i} \right)_{,i} + \frac{H}{\rho C_p} 
	\label{eq:energy2}
\end{equation}
Inserting Eqns.~\eqref{eq:t_nd},\eqref{eq:u_nd},\eqref{eq:x_nd} into Eqn.~\eqref{eq:energy2} we obtain
\begin{equation}
	\left( \frac{1}{\hat{t}} \frac{\partial T}{ \partial t^*} 
		+ \frac{U}{L}  u^*_i T_{,i^*} \right) 
	= 
	\frac{1}{L^2} \left(  \frac{k}{\rho C_p}  T_{,i^*} \right)_{,i^*} + \frac{H}{\rho C_p} 
	\label{eq:energy2}
\end{equation}
which simplifies to
\begin{equation}
	\left( \frac{\partial T}{ \partial t^*}  + u^*_i T_{,i^*} \right) 
	= 
	\frac{\hat{t}}{L^2} \left(  \frac{k}{\rho C_p}  T_{,i^*} \right)_{,i^*} + \hat{t} \frac{H}{\rho C_p} 
\end{equation}
and using $\rho = \hat{\rho} \rho^*$ gives
\begin{equation}
	\left( \frac{\partial T}{ \partial t^*}  + u^*_i T_{,i^*} \right) 
	= 
	\frac{\hat{t} UL}{\hat{\eta}} \left(  \frac{k}{\rho^* C_p}  T_{,i^*} \right)_{,i^*} 
	+ \frac{\hat{t} UL}{\hat{\eta}} \frac{H}{\rho^* C_p} 
\end{equation}
or
\begin{equation}
	\left( \frac{\partial T}{ \partial t^*}  + u^*_i T_{,i^*} \right) 
	= 
	\frac{L^2}{\hat{\eta}} \left(  \frac{k}{\rho^* C_p}  T_{,i^*} \right)_{,i^*} 
	+ \frac{L^2}{\hat{\eta}} \frac{H}{\rho^* C_p} 
\end{equation}
Expressing the 

\subsection{Heat transport (Form B)}
\begin{equation}
	\left( \frac{\partial T}{ \partial t} + u_i T_{,i} \right) = \left(  \kappa  T_{,i} \right)_{,i} + \frac{H}{\rho C_p} 
	\label{eq:energy2}
\end{equation}
where 
$$
	\kappa = \frac{k}{\rho C_p}.
$$
and using $\rho = \hat{\rho} \rho^*$ gives
\begin{equation}
	\left( \frac{\partial T}{ \partial t^*}  + u^*_i T_{,i^*} \right) 
	= 
	\frac{1}{UL} \left(  \kappa T_{,i^*} \right)_{,i^*} 
	+ \frac{L^2}{\hat{\eta}} \frac{H}{\rho^* C_p} 
\end{equation}
which becomes
\begin{equation}
	\left( \frac{\partial T}{ \partial t^*}  + u^*_i T_{,i^*} \right) 
	= 
	\left(  \kappa^* T_{,i^*} \right)_{,i^*} 
	+ \frac{H^*}{\rho^* C_p} 
\end{equation}
with
{\setlength\arraycolsep{0.2em}
\begin{eqnarray}
	\kappa &=& \hat{\kappa} \kappa^* 	=	U L  \kappa^*	\\
	H &=& \hat{H} H^*				=	\frac{\hat{\eta} }{L^2} H^*
\end{eqnarray}
}

  
  
\end{document}
