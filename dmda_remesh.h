#ifndef __PTATIN3d_DMDA_REMESH_H__
#define __PTATIN3d_DMDA_REMESH_H__

#include <petsc.h>
#include <petscvec.h>
#include <petscdm.h>

PetscErrorCode DMDARemeshSetUniformCoordinatesInPlane_IJ(DM da,PetscInt K,DMDACoor3d coords[]);
PetscErrorCode DMDAGetCornerCoordinatesInPlane_IJ(DM da,PetscInt K,DMDACoor3d coords[]);
PetscErrorCode DMDARemeshSetUniformCoordinatesBetweenKLayers3d( DM da, PetscInt startK, PetscInt endK );
PetscErrorCode DMDARemeshSetUniformCoordinatesBetweenJLayers2d( DM da, PetscInt startJ, PetscInt endJ );
PetscErrorCode DMDARemeshJMAX_UpdateHeightsFromInterior(DM da);
PetscErrorCode DMDARemeshSetUniformCoordinatesIJ(DM da,PetscInt dof_i,PetscInt index,PetscScalar value);

PetscErrorCode RMTSetCoordinates(DM dm,Vec coords);
PetscErrorCode RMTSetUniformCoordinatesRange_X(DM cdm,Vec coords,PetscInt istart,PetscScalar xstart,PetscInt iend,PetscScalar xend);
PetscErrorCode RMTSetUniformCoordinatesRange_Y(DM cdm,Vec coords,PetscInt jstart,PetscScalar ystart,PetscInt jend,PetscScalar yend);
PetscErrorCode RMTSetUniformCoordinatesRange(DM dm,
																						 PetscInt istart,PetscScalar xstart,PetscInt iend,PetscScalar xend,
																						 PetscInt jstart,PetscScalar ystart,PetscInt jend,PetscScalar yend,PetscBool update_ghost_coords);
PetscErrorCode RMTSetUniformCoordinatesBetweenILayers2d( DM da, PetscInt startI, PetscInt endI );
PetscErrorCode DMDASetRefinedCoordinates(DM dav);
#endif
