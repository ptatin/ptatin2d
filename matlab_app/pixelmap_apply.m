function pixelmap_apply(filename)
load([filename,'_basic.mat']);
flag = false;
fid = fopen([filename,'_phase.mat']);
if (fid > 2)
    fclose(fid);
    flag = questdlg('Do you need to redefine your phasemap?','ptatin2dMatlabApp','Yes','No','No');
else
    flag = 'Yes';
end

if strcmp(flag,'Yes')
    [nphase,LayerPhase,phase2D,X,Y] = RGB2tatin(filename,fname,entry,Length,Origine);
    save([filename,'_phase'],'nphase','LayerPhase','phase2D','X','Y');
end

end
function [nphase,i_litho,X2,X,Y] = RGB2tatin(filename,fname,entry,Length,Origine)
% the file must be  RGB file, desactivate the anti-aliasing while saving
% it works fine with *.tif and *.png
% do not use jpg unless you want a thousands of phases
% the rest you can try
% on Illustrator save the file using artbox and create one that fits the
% model size
if isempty(fname)
    errordlg('User must provide a valid filename for the image')
end
I=imread(fname);
% figure(1);image(I);
% pause(1);
[X,map] = RGB2nphaseLocal(I(1:end-1,1:end-1,:));
nphase = size(map,1);
msgbox(sprintf('Number of regions found %d \n', nphase));
[nx,ny]=size(X);
for i=1:nx
    X2(nx-i+1,:)=X(i,:);
end
clear('X','I');

y=linspace(Origine(2),Length(2),size(X2,1));
x=linspace(Origine(1),Length(1),size(X2,2));
[X,Y] = meshgrid(x,y);
figure(1);
colormap(hsv(nphase))
pcolor(X,Y,double(X2));shading flat; colorbar; title('phase number for the input file');axis equal tight;
                default = cellstr(repmat('0 5',length(entry),1));
                answer  = inputdlg(entry,'pTatin2DMatlabApp',1,default);            
                for i = 1: length(entry)
                    t = str2num(char(answer{i}))
                    i_litho(t+1)=i
                end
    
end

function [X,map] = RGB2nphaseLocal(img)
max_colors = 65536;
RGB=img;
N = floor(max_colors^(1/3)) - 1;

[x,y,z] = meshgrid((0:N)/N);
map = [x(:),y(:),z(:)];


RGB = round(im2doubleLocal(RGB)*N);
X = RGB(:,:,3)*((N+1)^2)+RGB(:,:,1)*(N+1)+RGB(:,:,2)+1;

[X,map] = cmunique(X,map);
end

function d = im2doubleLocal(img)

imgClass = class(img);

switch imgClass
    case {'uint8','uint16'}
        d = double(img) / double(intmax(imgClass));
    case {'single'}
        d = double(img);
    case 'double'
        d = img;
end
end
