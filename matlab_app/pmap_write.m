function pmap_write(X2,filename,Origine,Length)
X2=X2';
[nx,ny]=size(X2);
fid2 = fopen([filename,'.pmap'],'w');
fprintf(fid2,'number vary first in x and than y \n');
fprintf(fid2,'%d\n', nx);
fprintf(fid2,'%d\n' ,ny);
fprintf(fid2,'%1.8e %1.8e %1.8e %1.8e\n',Origine(1),Origine(2),Length(1),Length(2));
fprintf(fid2,'%d ',X2);
fclose(fid2);