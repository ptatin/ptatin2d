function initial_topo_apply(filename)

load([filename,'_phase.mat']);
load([filename,'_basic.mat']);
load([filename,'_temperature.mat']);

rho         = data(find(strcmp('rho',options)),:);
rho2D       = rho(LayerPhase(phase2D+1));
if EnergySolver
    alpha = data(find(strcmp('alpha',options)),:);
    alpha2D     = alpha(LayerPhase(phase2D+1));
    rho2D       = (1-alpha2D.*Anew).*rho2D;
end

[h,Ly,deltapixmax,ref,nbpix]=initial_topo_prepare(Length,Origine,phase2D,rho2D);

[phase2D] = shift(double(phase2D),deltapixmax,ref,nbpix);
phase2D = uint8(phase2D);
tmap_write(h,filename,Origine,Length);
pmap_write(phase2D,filename,Origine,Length);
if EnergySolver
    load([filename,'_temperature.mat'],'Anew');
    [Anew] = shift(Anew,deltapixmax,ref,nbpix);
    save([filename,'_temperature'],'Anew');
    tempmap_write(Anew,filename,Origine,Length);
end


flag = true;
end
function[hs,Ly,deltapixmax,ref,nbpix]=initial_topo_prepare(Length,Origine,phase2D,rho2D)

l= (Length(2)-Origine(2));
L= (Length(1)-Origine(1));

%found the compensation
ref = find(all((double(phase2D(:,1))*ones(1,size(phase2D,2)) == phase2D)'), 1, 'last' );
dy          = l/size(phase2D,1);
P           = sum(rho2D(ref:end,:))*dy;
h           = (P(1)-P)./rho2D(ref,:);
deltapixmax = ceil(max(h)/dy);

Ly = Length(2)+deltapixmax*dy;
x=linspace(0,L,length(h));
[dummy,p]=csaps(x,h);
hs = fnval(csaps(x,h,0.85*p),x)+Length(2);
nbpix = ceil(hs/dy);


end

function [X]=shift(X,deltapixmax,ref,nbpix)

[m,n] = size(X);
X2=[X;ones(deltapixmax,1)*X(end,:)];
for i=1:n
    if nbpix(i)<=0;
        X2(ref:m+nbpix(i),i)=X(-nbpix(i)+ref:m,i);
    else
        
        X2(nbpix(i)+ref:m+nbpix(i),i)=X(ref:end,i);
    end
end

end

function tmap_write(X2,filename,Origine,Length)
X2=X2';
[nx,nz]=size(X2);
fid2 = fopen([filename,'.tmap'],'w');
fprintf(fid2,'number vary first in x and than z \n');
fprintf(fid2,'%d\n', nx);
fprintf(fid2,'%d\n' ,nz);
fprintf(fid2,'%1.8e %1.8e %1.8e %1.8e\n',Origine(1),Origine(3),Length(1),Length(3));
fprintf(fid2,'%d ',X2);
fclose(fid2);
end