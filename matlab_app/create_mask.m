function [MASK,type] = create_mask(Tana,X,Y,phase2D,name)
newmask = 'Yes';

figure(50);clf;pcolor(X,Y,phase2D);shading interp; axis equal tight; caxis([-1,max(phase2D(:))]);colorbar;hold off

MASK = zeros(size(phase2D));

liste = {'following phase' 'graphically on the fig' 'circle' 'from isotherm','box','empty'};
operators = {'Union' 'Intersection' 'Union with complement' 'Intersection with complement' 'Cancel'};


while strcmp(newmask,'Yes')
    
    figure(51); clf;
    subplot(311); pcolor(X,Y,MASK.*phase2D);shading interp; axis equal tight; colorbar; title('current MASK');
    
    type = menu ('What type of submask ?', liste); 

    switch type
        
        case find(strcmp('box',liste))
            M = ones(size(phase2D));
        case find(strcmp('empty',liste))
            M = zeros(size(phase2D));
        case find(strcmp('following phase',liste))
            M = create_mask_from_phase(phase2D);
        case find(strcmp('graphically on the fig',liste))
            M = create_mask_from_figure(X,Y,(phase2D+1.*MASK)-1,50);
        case find(strcmp('circle',liste))
            M = create_circular_mask(X,Y);
        case find(strcmp('from isotherm',liste))
            M = create_mask_from_isotherm(X,Y,Tana,name);
    end
    figure(51);subplot(312); pcolor(X,Y,M.*phase2D);shading interp; axis equal tight; caxis([-1,max(phase2D(:))]);colorbar; title('New subMASK');
    
    happy = 'No';
    while strcmp(happy,'No')
        operator = menu('Which operator of mask',operators);
        switch operator
            case find(strcmp('Union',operators))
                M2 = MASK | M;
            case find(strcmp('Intersection',operators))
                M2 = MASK & M;
            case find(strcmp('Union with complement',operators))
                M2 = MASK | ~M;
            case find(strcmp('Intersection with complement',operators))
                M2 = MASK & ~M;    
            case find(strcmp('Cancel',operators))
                M2 = MASK;
        end
        
        figure(51);subplot(3,1,3,'replace');pcolor(X,Y,M2.*phase2D);shading interp; axis equal tight; colorbar; title('Updated MASK');
        
        happy = questdlg('Do you accept this modification?','ptatin2dMatlabApp','Yes','No','No');
  
    end
    MASK = M2;
    newmask = questdlg('Would you like to continue adding submask to this mask?','ptatin2dMatlabApp','Yes','No','No');
end
type   = menu('Which geotherm do you wanna use with this mask?',name);
end

function [M]=create_mask_from_phase(phase2D)
% phase2D is a 2D array which contains particule IDs
% phasein is a 1D array which particule index which must be included in the mask
options.Resize='on';
options.Window='normal';
phase_in = str2num(char(inputdlg({'Liste of particle IDs'},'Create subMask for particles ',1,{'1 3 5'},options)));
M = 0*phase2D;
for i = 1:length(phase_in)
    M = M+(phase2D == phase_in(i));
end
end

function [M]=create_mask_from_isotherm(X,Y,Tana,name)
% phase2D is a 2D array which contains particule IDs
% phasein is a 1D array which particule index which must be included in the mask
 
type   = menu('which geotherm do you wanna use?\n',name);
T      = Tana(:,:,type);
figure(100);pcolor(X,Y,T); shading interp;colorbar;
prompt = {'min temperature' 'max temperature'};
options.Resize='on';
answer=inputdlg(prompt,'Temperature range',1,{'0','100'},options);
M = (T>=str2num(char(answer(1))) & T<=str2num(char(answer(2))));
close(100);
end

function [M]=create_mask_from_figure(X,Y,phase2D,numfig)
% phase2D is a 2D array which contains particule IDs
% phasein is a 1D array which particule index which must be included in the mask


figure(numfig);clf;pcolor(X,Y,phase2D);shading interp; axis equal tight; caxis([-1,max(phase2D(:))]);colorbar;hold off
j=0;
addpoint='Yes';
while strcmp(addpoint,'Yes')
    j=j+1;
    figure(numfig);
    [xdomain(j),ydomain(j)] = ginput(1);
    figure(numfig);hold on ; plot(xdomain(j),ydomain(j),'+'); hold off;
    if j > 2 
    addpoint = questdlg('Add more points?','ptatin2dMatlabApp','Yes','No','No');
    end
end
TRI = delaunay(xdomain,ydomain);
M   = double(~isnan(tsearch(xdomain,ydomain,TRI,X,Y)));
figure(numfig);clf;close(numfig);
end


function [M]=create_circular_mask(X,Y)
M = X*0;
options.Resize='on';
options.Window='normal';
data=str2num(char(inputdlg({'Radius' 'Xcentre' 'Ycentre'},'Enter Circle Properies',1,{'1e5','1e5','1e5'},options)));
R    = data(1);
x    = data(2);
y    = data(3);
M= ((X-x).^2+(Y-y).^2-R^2) < 0;
end


