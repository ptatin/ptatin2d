function flag = write_option_file(filename)

load([filename,'_basic.mat']);
load([filename,'_phase.mat']);

model  = 'GENE';
prefix = ['-',model,'_'];
fid3 = fopen([filename2,'.opts'],'w');
fprintf(fid3,'#####################################\n');
fprintf(fid3,'#############Model Parameters###############\n');
fprintf(fid3,'#####################################\n');
s1 = [prefix,'rheol'];
fprintf(fid3,'%s %d\n',s1,rheology_type);
fprintf(fid3,'%s %s\n','-ptatin_model',model);
fprintf(fid3,'%s %s\n','-output_path',outputpath);
fprintf(fid3,'%s %d\n','-nsteps',nsteps);
fprintf(fid3,'%s %d\n','-output_frequency',output_frequency);
fprintf(fid3,'%s %s\n','-map_file',filename);
if isostatic_topo
    fprintf(fid3,'%s %s\n','-topo_file',filename);
end
if EnergySolver
    fprintf(fid3,'%s %s\n','-temp_file',filename);
end
fprintf(fid3,'%s %d\n','-mx',nel(1));
fprintf(fid3,'%s %d\n','-my',nel(2));
fprintf(fid3,'%s %d\n','-mz',nel(3));

fprintf(fid3,'%s %d\n','-Lx',Length(1));
fprintf(fid3,'%s %d\n','-Ly',Length(2));
fprintf(fid3,'%s %d\n','-Lz',Length(3));

fprintf(fid3,'%s %d\n','-Ox',Origine(1));
fprintf(fid3,'%s %d\n','-Oy',Origine(2));
fprintf(fid3,'%s %d\n','-Oz',Origine(3));
fprintf(fid3,'######## population control#################\n');

fprintf(fid3,'%s %d\n','-lattice_layout_Nx', lattice_layout_Nx);
fprintf(fid3,'%s %d\n','-lattice_layout_Ny', lattice_layout_Ny);
fprintf(fid3,'%s %d\n','-mp_popctrl_nxp', mp_popctrl_nxp);
fprintf(fid3,'%s %d\n','-mp_popctrl_nyp', mp_popctrl_nyp);
fprintf(fid3,'%s %d\n','-coefficient_projection_type', coefficient_projection_type);
fprintf(fid3,'%s %d\n','-mp_popctrl_np_lower', mp_popctrl_np_lower);

fprintf(fid3,'######## timestep control#################\n');
fprintf(fid3,'%s %e\n','-surface_courant_factor', surface_courant_factor);
fprintf(fid3,'%s %e\n','-dt_max',dt_max);% 1.5e-2
fprintf(fid3,'%s %e\n','-max_surf_vz',max_surf_vz);% 3e-2

fprintf(fid3,'######## scaling parameters#################\n');
fprintf(fid3,'%s %e\n','-stokes_scale_length', Length_scaling);
fprintf(fid3,'%s %e\n','-stokes_scale_viscosity',Vis_scaling);
fprintf(fid3,'%s %e\n','-stokes_scale_velocity',Vel_scaling);
fprintf(fid3,'%s %e\n','-energy_scale_temperature',Temp_scaling);

if (rheology_type > 0 )
    fprintf(fid3,'######## viscous cutoff#################\n');
    s1 = [prefix,'eta_min_cut_off'];
    fprintf(fid3,'%s %e\n',s1,eta_min_cut_off);
    s1 = [prefix,'eta_max_cut_off'];
    fprintf(fid3,'%s %e\n',s1,eta_max_cut_off);
end

fprintf(fid3,'#############Boundary Conditions###############\n');

fprintf(fid3,'#############velocity###############\n');
fprintf(fid3,'%s %d\n','-bc_type',bctype);
fprintf(fid3,'%s %f\n','-vx',vx);
if EnergySolver
    fprintf(fid3,'#############temperature###############\n');
    fprintf(fid3,'%s %s\n','-temp_file',filename);
    if ~isempty(Ttop); fprintf(fid3,'%s %f\n','-Ttop',Ttop);       end;
    if ~isempty(Tbot); fprintf(fid3,'%s %f\n','-Tbot',Tbot);       end;
    if ~isempty(Tright); fprintf(fid3,'%s %f\n','-Tright',Tright); end;
    if ~isempty(Tleft); fprintf(fid3,'%s %f\n','-Tleft',Tleft);    end;
end

fprintf(fid3,'#####################################\n');
fprintf(fid3,'#############Materials###############\n');
fprintf(fid3,'#####################################\n');

s1 = [prefix,'nphase'];
fprintf(fid3,'%s %d\n',s1,nphase);

for i_phase = 0:nphase-1
    fprintf(fid3,'#### param for phase indexed %d behaving as %s #####\n',i_phase,char(entry(LayerPhase(i_phase+1))));
    for i_opt = 1:size(data,1)
        s = [prefix,char(options(i_opt)),'_',num2str(i_phase)];
        fprintf(fid3,['%s %e \n'],s,data(i_opt,LayerPhase(i_phase+1)));
    end
    fprintf(fid3,'#####################################\n');
end

fprintf(fid3,'########################################\n');
fprintf(fid3,'#Numerics/solver happen below this line#\n');
fprintf(fid3,'########################################\n');

switch solver_options
    case 'linear'
        num_file = 'numerics.txt'; 
    case 'nonlinear'
        num_file = 'numerics.txt';
    case 'strongly_nonlinear'
        num_file = 'numerics.txt';
end
    
    fid5 = fopen(num_file,'r');
    S= textscan(fid5,'%s','Delimiter','\n');
    fclose(fid5);
    options=S{1};
    for i=1:length(options)
        fprintf(fid3,'%s \n',options{i});
    end

flag=1
fclose(fid3);


