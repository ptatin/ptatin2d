function Production = Energy_apply(filename)
load([filename,'_phase.mat']);
load([filename,'_basic.mat']);

fid = fopen([filename,'_temperature.mat']);

if fid == -1
    warndlg('your geotherm is not defined')
else
    fclose(fid);
    load([filename,'_temperature.mat'],'Anew','Prodnew');
    figure(500); pcolor(X,Y,Anew); shading interp; colorbar; title('This is your current geotherm')
end

fid = fopen([filename,'_therm_ana.mat'],'r');
if fid > 2
    fclose(fid);
    load ([filename,'_therm_ana'],'name')
    s=sprintf('The following geotherms are already defined for this model:\n');
    l = length(name);
    for i = 1:l
        s=[s,sprintf(' %d %s \n', i, name{i})];
    end
    create_geotherm = questdlg([s,'Do you want to prepare additional geotherm from analytical solutions?'],'ptatin2dMatlabApp','Yes','No','No');
else
    create_geotherm = 'Yes';
    l = 0;
end
if strcmp(create_geotherm,'Yes')
    makegeotherm(filename,l);
end
interpolat_geotherm = questdlg('Do you want to create a geotherm?','ptatin2dMatlabApp','Yes','No','No');
if strcmp(interpolat_geotherm,'Yes');
    [Anew,Prodnew] = geothermscat(filename);
    save([filename,'_temperature'],'Anew','Prodnew');
end

if exist('Anew')
    figure(500); clf; pcolor(X,Y,Anew); shading interp; colorbar; title('This is your current geotherm')
    fname = [filename2,'.tempmap'];
    tempmap_write(Anew,fname,Origine,Length);
    close(500);
    Production=0;
    if sum(Prodnew(:))>0
        fname = [filename2,'.prodmap'];
        tempmap_write(Prodnew,fname,Origine,Length);
        Production = 1;
    end
else
    errordlg('you have not define your geotherm, something went wrong exiting')
    error('you have not define your geotherm, something went wrong exiting');
end
