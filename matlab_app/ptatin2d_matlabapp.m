clear;
clf;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% SET PATH HERE %%%%%%%%%%%%%%%%%%
% text file where particle properties are stored
datafile = 'example/data2.txt';
% Define the filename for the geometry if you have one
fname = 'example/mikeymouseplume-01.png';
% what name of file do you want temporary auxiliary files ?
filename = 'test';
% what name of file do you want for your pTat option file ?
filename2 = 'option';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%DEFINE WHAT KIND OF MODEL YOU WANT TO RUN HERE%%%%%%%
isostatic_topo = true;
EnergySolver   = true;
solver_options ='nonlinear';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%   MODEL DATA   %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% what is the name of the directory in which you wanna store pTat output?
outputpath = 'mickey_plume';
% what is the size of the model?
Origine = [0.0   -4.0 0.0]*1e5;
Length  = [8.0   0.0 0.0]*1e5;
% what kind of rheological model are you using ? 
% VISCOUS => 0 VISCOPLASTIC => 1 VISCOPLASTIC WITH SOFTENING => 2
rheology_type =1;
% what is resolution the model?
nel     = [128 64 0];
% How many timestep do you want to run your model for? 
nsteps = 10;
% What is the output frequency for plotting? 
output_frequency = 1;
% What is your gravity acceleration?
gravity = 10; 
% What is your scaling ? 
Length_scaling = 1e5;
Vis_scaling    = 1e22;
Vel_scaling    = 1e-10;
Temp_scaling   = 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%  BOUNDARY CONDITIONS  %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Velocity bc's type
bctype = 0;
% Velocity bc's value
vx     = 0;

% temperature bc's .... 
Tbot=[]; Ttop=[];Tleft=[]; Tright=[];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%   Advanced numerical parameters   %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% for non linear models it is menditory to set viscosity cut off for the continuation
eta_min_cut_off = 1e-6;
eta_max_cut_off = 1.;
% timestep control
surface_courant_factor = 1.0;
dt_max                 = 1.5e-2;
max_surf_vz            = 3e-2;
% particles parameters
% initial number particle per cell in x and y
lattice_layout_Nx = 4;
lattice_layout_Ny = 4;
% number of particles inserted per cell in x and y at each repopulation
mp_popctrl_nxp = 3;
mp_popctrl_nyp = 3;
%minimum particule in the cell (repopulation criteria)
mp_popctrl_np_lower = 12;
% type of projection of particle value to mesh
% 0 -> P_0  1-> Q_1
coefficient_projection_type = 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% NOTHING TO MODIFY HERE %%%%%%%%%%%%%%%%%%
[entry,options,data]=importlitho(datafile);
save([filename,'_basic']);
pixelmap_apply(filename);
if EnergySolver
Energy_apply(filename)
end
if isostatic_topo
   initial_topo_apply(filename);
end
flag = write_option_file(filename);

