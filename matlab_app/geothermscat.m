function [Anew,Prodnew] = geothermscat(filename)
% load data
load([filename,'_phase.mat']);
load([filename,'_therm_ana.mat']);
sh     = 4; % number of pixels in the border of the domain

phase2D = double(phase2D);

% create the T,X,Y,name variable
BORD=zeros(size(X));
DOMAIN=BORD;

interpolate = 'Yes';
i=0;

interpolate = questdlg('Do you want to interpolate between geotherm?','ptatin2dMatlabApp','Yes','No','No');

if strcmp(interpolate,'No')
    type   = menu('Which geotherm do you wanna use?',name);
    Anew = Tana(:,:,type);
    Prodnew = Prod(:,:,type);
else
    while strcmp(interpolate,'Yes')
        i=i+1;
        [M,type] = create_mask(Tana,X,Y,phase2D,name);
        [BORD(:,:,i), DOMAIN(:,:,i)]=contour_mask(X,Y,M,sh);
        A(:,:,i) = Tana(:,:,type);
        H(:,:,i) = Prod(:,:,type);
        %assign -1 to the part of the phase array which have been assigned.
        phase2D = ((phase2D+1).*~DOMAIN(:,:,i))-1;
        interpolate = questdlg('Would you like to Adding a new geotherm patch to the interpolation?','ptatin2dMatlabApp','Yes','No','No');
    end
    
    Anew = sum(A.*DOMAIN,3);
    Prodnew = sum(H.*DOMAIN,3);
    BORDnew = sum(BORD,3);
    DOMAINnew  = sum(DOMAIN,3);
    
    type=[];
    while isempty(type)
        type = menu('Which geotherm do you wanna use for the bottom?',name);
    end
    Anew(1,:)=Tana(1,:,type);
    BORDnew(1,:)= 1;
    DOMAINnew(1,:)= 1;
    
    type=[];
    while isempty(type)
        type = menu('Which geotherm do you wanna use for the top?',name);
    end
    Anew(end,:)=Tana(end,:,type);
    BORDnew(end,:)= 1;
    DOMAINnew(end,:)= 1;
    
    type=[];
    while isempty(type)
        type = menu('Which geothem do you wanna use for the left side?',name);
    end
    Anew(:,1:2)=Tana(:,1:2,type);
    BORDnew(:,1:2)= 1;
    DOMAINnew(:,1:2)= 1;
    
    type=[];
    while isempty(type)
        type = menu('Which geotherm do you wanna use for the right side?',name);
    end
    Anew(:,end-1:end)=Tana(:,end-1:end,type);
    BORDnew(:,end-1:end)= 1;
    DOMAINnew(:,end-1:end)= 1;
    
    
    indice   = find(BORDnew==1);
    xp       = X(indice);
    yp       = Y(indice);
    ap       = Anew(indice);
    
    Ainterp  = griddata(xp,yp,ap,X,Y);
    Anew     = Anew+~DOMAINnew.*Ainterp;
    
    figure(100);pcolor(X,Y,Anew); shading flat;
    figure(101);pcolor(X,Y,Prodnew); shading flat;
    
    smooth = questdlg('do you want to smooth it a bit more?','ptatin2dMatlabApp','Yes','No','No');
    
    if strcmp(smooth,'Yes')
        dummy = Anew;
        happy ='No'
        while strcmp(happy,'No')
            n = str2num(char(inputdlg({'smoothing factor'},'pTatin2dMatlabApp',1,{'5'})));
            h = ones(n*2);
            Z = filter2(h,Anew,'valid');
            Z = filter2(h,Anew,'valid');
            Z = Z/max(Z(:))*max(Anew(:));
            dummy(n:end-n,n:end-n)=Z;
            figure(100);clf;pcolor(X,Y,dummy); shading flat; axis tight equal; colorbar
            happy = questdlg('Keep this result?','ptatin2dMatlabApp','Yes','No','Stop','No');
        end
        
        if strcmp(happy,'Yes')
            Anew=dummy;
        end
        happy = questdlg('Do you wanna store the geotherm with your analytical solution?','ptatin2dMatlabApp','Yes','No','Yes');
        if strcmp(happy,'Yes')
            index = size(Tana,3)+1;
            Tana(:,:,index) = Anew;
            Prod(:,:,index) = Prodnew;
            name{index} = char(inputdlg({'Name of the geotherm'},'pTatin2dmatlabApp ',1,{'mygeotherm_name'}));
            save([filename,'_therm_ana'],'Tana','Prod','name','X','Y');
        end
        
    end
end
figure(100);pcolor(X,Y,Anew); shading flat;colorbar
figure(100);pcolor(X,Y,Prodnew); shading flat;colorbar
end

function [B,M]=contour_mask(X,Y,M,sh)
%X,Y,A are 2D arrays of the same size
% A is the analytical solution at point X,Y
%B and M are also 2 arrays containing respectively mask for the contour and
%for the domain where A applies
[nl,nc]=size(M);
Zhb=zeros(sh,nc);
Zdg=zeros(nl,sh);
Nb = [Zhb;M(1:end-sh,:)];
Nh = [M(sh+1:end,:);Zhb];
Nd = [Zdg M(:,1:end-sh)];
Ng = [M(:,sh+1:end) Zdg];
B  =  M&~(Nb&Nh&Nd&Ng);
end
