function [entry,options,values]=importlitho(fileToRead1)


% check if the file exists

fid = fopen(fileToRead1); 
if (fid == -1)
    errordlg('your file with properties (''datafile='') doesn''t exists or is not at the right place');
    error('your file with properties (''datafile='') doesn''t exists or is not at the right place');
end

line  = fgetl(fid);
dummy = textscan(line,'%s','MultipleDelimsAsOne',true);
entry = dummy{1};
str = ['%s',repmat('%f',size(entry'))];
C=textscan(fid,str,'MultipleDelimsAsOne',true);
options = C{1};
values=[];
for i=1:length(entry);
    values = [values,C{i+1}];
end
fclose(fid);

