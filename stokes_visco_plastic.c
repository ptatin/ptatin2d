/*
 *  stokes_visco_plastic.c
 *  
 *
 *  Created by laetitia le pourhiet on 6/11/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"
#include "rheology.h"
#include "stokes_visco_plastic.h"

#undef __FUNCT__
#define __FUNCT__ "EvaluateRheologyNonlinearitiesMarkers_ViscoPlastic"
PetscErrorCode EvaluateRheologyNonlinearitiesMarkers_ViscoPlastic(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[])
{	
	PetscErrorCode ierr;
	int         pidx,n_mp_points;
	DataBucket  db;
	DataField   PField_std, PField_stokes;
	PetscScalar GNIu[2][Q2_NODES_PER_EL_2D],NIp[P_BASIS_FUNCTIONS];
	PetscScalar NIu[U_BASIS_FUNCTIONS];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen_u,nen_p,e,i,j,k;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	PetscInt vel_el_lidx[U_BASIS_FUNCTIONS*2];
	PetscInt *gidx;
	PetscLogDouble t0,t1;
	PetscScalar  d1,exx_mp,eyy_mp,exy_mp,eta_mp;
	PetscScalar  sxx_mp,syy_mp,sxy_mp,u_mp,v_mp,trace_mp,pressure_mp;
	PetscScalar min_eta,max_eta,min_eta_g,max_eta_g;
	PetscInt npoints_yielded_g, npoints_yielded_tens_g,npoints_yielded,npoints_yielded_tens;
	PetscScalar Co, phi, A, B, tauyield_mp,strainrateinv_mp,stressinv_mp,contrast; 
	
	
	RheologyConstants *rheology;
	PetscScalar eta_eff_continuation_min,eta_eff_continuation_max,min_eta_cut,max_eta_cut,alpha_m;
	
	PetscFunctionBegin;
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetGlobalIndices(dau,0,&gidx);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
	min_eta = 1.0e100;
	max_eta = 1.0e-100;
	PetscGetTime(&t0);
	/* setup for rheological parameters depending on phases */
	rheology  = &user->rheology_constants;
	
	/* continuation parameter */
	
	min_eta_cut = rheology->eta_lower_cutoff_global;
	max_eta_cut = rheology->eta_upper_cutoff_global;  
	alpha_m = (double)(user->continuation_m+1)/( (double)(user->continuation_M+1) );
    contrast=log10(max_eta_cut/min_eta_cut);
    eta_eff_continuation_max = min_eta_cut*pow(10.0,(alpha_m*contrast));
    eta_eff_continuation_min = min_eta_cut;
    //PetscPrintf(PETSC_COMM_WORLD," continuation viscosity cut-off:max  %f  min %f \n", eta_eff_continuation_max,eta_eff_continuation_min);
    
 	/* marker loop */
	db = user->db;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));
	
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	npoints_yielded = 0;
	npoints_yielded_tens = 0; 

	for (pidx=0; pidx<n_mp_points; pidx++) {
		MPntStd     *material_point;
		MPntPStokes *mpprop_stokes;
		double      *position, *xip;
		int          wil,phase_mp;
        char         is_yielding, yield_type;
		PetscScalar J[2][2], iJ[2][2];
		PetscScalar J_p,ojp;
		
		
		DataFieldAccessPoint(PField_std,   pidx,(void**)&material_point);
		DataFieldAccessPoint(PField_stokes,pidx,(void**)&mpprop_stokes);
		
		MPntStdGetField_local_element_index(material_point,&wil);
		MPntStdGetField_global_coord(material_point,&position);
		MPntStdGetField_local_coord(material_point,&xip);
		MPntStdGetField_phase_index(material_point,&phase_mp);
		
		e = wil;
		
		/* get nodal properties for element e */
		ierr = StokesVelocity_GetElementLocalIndices(vel_el_lidx,(PetscInt*)&elnidx_u[nen_u*e]);CHKERRQ(ierr);
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],u);CHKERRQ(ierr);
		ierr = DMDAGetScalarElementField_2D(elp,nen_p,(PetscInt*)&elnidx_p[nen_p*e],p);CHKERRQ(ierr);
		
		/* get element velocity/coordinates in component form */
		for( i=0; i<Q2_NODES_PER_EL_2D; i++ ) {
			ux[i] = elu[2*i  ];
			uy[i] = elu[2*i+1];
			
			xc[i] = elcoords[2*i  ];
			yc[i] = elcoords[2*i+1];
		}
		
		
		PTatinConstructNI_Q2_2D(xip,NIu);
		PTatinConstructGNI_Q2_2D(xip,GNIu);
		ConstructNi_pressure(xip,elcoords,NIp);
		
		/* coord transformation */
		for( i=0; i<2; i++ ) {
			for( j=0; j<2; j++ ) { J[i][j] = 0.0; }
		}
		for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
			J[0][0] += GNIu[0][k] * xc[k] ;
			J[0][1] += GNIu[0][k] * yc[k] ;
			
			J[1][0] += GNIu[1][k] * xc[k] ;
			J[1][1] += GNIu[1][k] * yc[k] ;
		}
		J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
		ojp = 1.0/J_p;
		iJ[0][0] =  J[1][1]*ojp;
		iJ[0][1] = -J[0][1]*ojp;
		iJ[1][0] = -J[1][0]*ojp;
		iJ[1][1] =  J[0][0]*ojp;
		
		/* global derivs at each quadrature point */
		for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
			nx[k] = iJ[0][0]*GNIu[0][k] + iJ[0][1]*GNIu[1][k];
			ny[k] = iJ[1][0]*GNIu[0][k] + iJ[1][1]*GNIu[1][k];
		}
		
		/* u,v and strain rate (e= B u) at gp */
		u_mp = v_mp = 0.0;
		exx_mp = eyy_mp = exy_mp = 0.0;
		for( k=0; k<U_BASIS_FUNCTIONS; k++ ) {
			u_mp   += NIu[k] * ux[k];
			v_mp   += NIu[k] * uy[k];
			
			exx_mp += nx[k] * ux[k];
			eyy_mp += ny[k] * uy[k];
			exy_mp += ny[k] * ux[k] + nx[k] * uy[k];
		}
		exy_mp = 0.5 * exy_mp;
		strainrateinv_mp = sqrt(0.25*(exx_mp-eyy_mp)*(exx_mp-eyy_mp)+exy_mp*exy_mp);
        
        /* compute current stress at mp to get the pressure*/
		eta_mp = mpprop_stokes->eta;
        d1 = 2.0 * eta_mp;
		
		sxx_mp = d1 * exx_mp;
		syy_mp = d1 * eyy_mp;
		sxy_mp = d1 * exy_mp;
		
		trace_mp = 0.5 * ( sxx_mp + syy_mp );
		sxx_mp = sxx_mp - trace_mp;
		syy_mp = syy_mp - trace_mp;
		
		/* pressure at mp */
		pressure_mp = 0.0;
		for( k=0; k<P_BASIS_FUNCTIONS; k++ ) {
			pressure_mp += NIp[k] * elp[k];
		}
		pressure_mp = pressure_mp + trace_mp;
		
        
        /* get the plastic properties*/
		Co          = rheology->mises_tau_yield[phase_mp]; 
		phi         = rheology->dp_pressure_dependance[phase_mp];

        /* softening bits could be replace by more specific functions in the future 
        /*  
		epl_softmin = rheology->soft_min_strain_cutoff[phase_mp]; 
		epl_softmax = rheology->soft_max_strain_cutoff[phase_mp];
        
        if (eplastic > epl_softmin){
            if (eplastic > epl_softmax) {
                Co  = rheology->soft_Co_inf[phase_mp];
                phi = rheology->soft_phi_inf[phase_mp];
            } else {
                float X  = (eplastic-epl_softmin)/(epl_softmax-epl_softmin);
                Co  = Co  - X * (Co-rheology->soft_Co_inf[phase_mp]);
                phi = phi - X * (phi-rheology->soft_phi_inf[phase_mp]);
            }
        }
        */
        
        /* check the yield criteria   */ 
        /* regular failure in shear  */
		A           = sin(phi);
        B           = cos(phi);	
        tauyield_mp = A * pressure_mp + B *Co;
        yield_type  = 1; 
        
		if ( tauyield_mp < rheology->tens_cutoff[phase_mp]){
            /* failure in tension cutoff */
			tauyield_mp = rheology->tens_cutoff[phase_mp];
			yield_type = 2;
		} else if (tauyield_mp > rheology->Hst_cutoff[phase_mp]){   
            /* failure at High stress cut off à la boris */
			tauyield_mp = rheology->Hst_cutoff[phase_mp];
			yield_type = 3;
		}  
		
		/* viscosity */
        eta_mp = rheology->const_eta0[phase_mp];
        /* viscous stress predictor */ 
		d1 = 2.0 * eta_mp;
		sxx_mp = d1 * exx_mp;
		syy_mp = d1 * eyy_mp;
		sxy_mp = d1 * exy_mp;
		trace_mp = 0.5 * ( sxx_mp + syy_mp );
		sxx_mp = sxx_mp - trace_mp;
		syy_mp = syy_mp - trace_mp;
		
		stressinv_mp = sqrt(0.25*(sxx_mp-syy_mp)*(sxx_mp-syy_mp)+sxy_mp*sxy_mp);
		
		/* apply plastic correction when marker is yielding */ 
		is_yielding = 0; 
        if (stressinv_mp > tauyield_mp){
			eta_mp = 0.5*tauyield_mp/strainrateinv_mp;
			npoints_yielded++;
            is_yielding = yield_type; 
		}
        
        
        /* Continuation cutt off */ 
        if (eta_mp > eta_eff_continuation_max) eta_mp = eta_eff_continuation_max ;
		if (eta_mp < eta_eff_continuation_min) eta_mp = eta_eff_continuation_min ;    
		
		
		/* Global cuttoffs for viscosity*/
		if (eta_mp > rheology->eta_upper_cutoff_global) {
			eta_mp = rheology->eta_upper_cutoff_global;
		}
		if (eta_mp < rheology->eta_lower_cutoff_global) {
			eta_mp = rheology->eta_lower_cutoff_global;
		}
    
		/* update viscosity on marker */
		MPntPStokesSetField_eta_effective(mpprop_stokes,eta_mp);
		
		/* monitor bounds */
		if (eta_mp > max_eta) { max_eta = eta_mp; }
		if (eta_mp < min_eta) { min_eta = eta_mp; }
	}
	
	
	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokes);
	
	PetscGetTime(&t1);
  MPI_Allreduce(&min_eta,&min_eta_g,1, MPI_DOUBLE, MPI_MIN, PETSC_COMM_WORLD);
  MPI_Allreduce(&max_eta,&max_eta_g,1, MPI_DOUBLE, MPI_MAX, PETSC_COMM_WORLD);
  MPI_Allreduce(&npoints_yielded,&npoints_yielded_g,1, MPI_INT, MPI_SUM, PETSC_COMM_WORLD);
	/*
  PetscPrintf(PETSC_COMM_WORLD,"Update rheology (visco-plastic) [mpoint]: npoints_yielded %d; (min,max)_eta %1.2e,%1.2e; log10(max/min) %1.2e; cpu time %1.2e (sec)\n", npoints_yielded_g,  min_eta_g, max_eta_g, log10(max_eta_g/min_eta_g), t1-t0 );
	*/
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvaluateRheologyNonlinearitiesMarkers_ViscoPlasticStrainWeakening"
PetscErrorCode EvaluateRheologyNonlinearitiesMarkers_ViscoPlasticStrainWeakening(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[])
{	
	PetscErrorCode ierr;
	int         pidx,n_mp_points;
	DataBucket  db;
	DataField   PField_std, PField_stokes, PField_stokespl;
	PetscScalar GNIu[2][Q2_NODES_PER_EL_2D],NIp[P_BASIS_FUNCTIONS];
	PetscScalar NIu[U_BASIS_FUNCTIONS];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen_u,nen_p,e,i,j,k;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	PetscInt vel_el_lidx[U_BASIS_FUNCTIONS*2];
	PetscInt *gidx;
	PetscLogDouble t0,t1;
	PetscScalar  d1,exx_mp,eyy_mp,exy_mp,eta_mp;
	PetscScalar  sxx_mp,syy_mp,sxy_mp,u_mp,v_mp,trace_mp,pressure_mp;
	PetscScalar min_eta,max_eta,min_eta_g,max_eta_g;
	PetscInt npoints_yielded_g,npoints_yielded;
	PetscScalar phi, Co, A, B, tauyield_mp,strainrateinv_mp,stressinv_mp;
        
	
	
	
	RheologyConstants *rheology;
	PetscScalar eta_eff_continuation_max,eta_eff_continuation_min,min_eta_cut,max_eta_cut,alpha_m,contrast;
	
	PetscFunctionBegin;
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetGlobalIndices(dau,0,&gidx);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
	min_eta = 1.0e100;
	max_eta = 1.0e-100;
	PetscGetTime(&t0);
	/* setup for rheological parameters depending on phases */
	rheology  = &user->rheology_constants;
	
	/* continuation parameter */
	
	min_eta_cut = rheology->eta_lower_cutoff_global;
	max_eta_cut = rheology->eta_upper_cutoff_global;  
	alpha_m = (double)(user->continuation_m+1)/( (double)(user->continuation_M+1) );
    contrast=log10(max_eta_cut/min_eta_cut);
    eta_eff_continuation_max = min_eta_cut*pow(10.0,(alpha_m*contrast));
    eta_eff_continuation_min = min_eta_cut;
    //PetscPrintf(PETSC_COMM_WORLD," continuation viscosity cut-off:max  %f  min %f \n", eta_eff_continuation_max,eta_eff_continuation_min);
    
 	/* marker loop */
	db = user->db;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));

	DataBucketGetDataFieldByName(db,MPntPStokesPl_classname,&PField_stokespl);
	DataFieldGetAccess(PField_stokespl);
	DataFieldVerifyAccess(PField_stokespl,sizeof(MPntPStokesPl));
	

	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	npoints_yielded = 0; 
	
	for (pidx=0; pidx<n_mp_points; pidx++) {
		MPntStd       *material_point;
		MPntPStokes   *mpprop_stokes;
		MPntPStokesPl *mpprop_stokespl;
		double        *position, *xip;
		int           wil,phase_mp;
		char          is_yielding, yield_type;
		float         eplastic;
                PetscScalar   epl_softmin,epl_softmax;
		PetscScalar   J[2][2], iJ[2][2];
		PetscScalar   J_p,ojp;
		
		
		DataFieldAccessPoint(PField_std,     pidx,(void**)&material_point);
		DataFieldAccessPoint(PField_stokes,  pidx,(void**)&mpprop_stokes);
		DataFieldAccessPoint(PField_stokespl,pidx,(void**)&mpprop_stokespl);
		
		MPntStdGetField_local_element_index(material_point,&wil);
		MPntStdGetField_global_coord(material_point,&position);
		MPntStdGetField_local_coord(material_point,&xip);
		MPntStdGetField_phase_index(material_point,&phase_mp);
		
		MPntPStokesPlGetField_plastic_strain(mpprop_stokespl,&eplastic);
		MPntPStokesPlGetField_yield_indicator(mpprop_stokespl,&is_yielding);
		
		e = wil;
		
		/* get nodal properties for element e */
		ierr = StokesVelocity_GetElementLocalIndices(vel_el_lidx,(PetscInt*)&elnidx_u[nen_u*e]);CHKERRQ(ierr);
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],u);CHKERRQ(ierr);
		ierr = DMDAGetScalarElementField_2D(elp,nen_p,(PetscInt*)&elnidx_p[nen_p*e],p);CHKERRQ(ierr);
		
		/* get element velocity/coordinates in component form */
		for( i=0; i<Q2_NODES_PER_EL_2D; i++ ) {
			ux[i] = elu[2*i  ];
			uy[i] = elu[2*i+1];
			
			xc[i] = elcoords[2*i  ];
			yc[i] = elcoords[2*i+1];
		}
		
		
		PTatinConstructNI_Q2_2D(xip,NIu);
		PTatinConstructGNI_Q2_2D(xip,GNIu);
		ConstructNi_pressure(xip,elcoords,NIp);
		
		/* coord transformation */
		for( i=0; i<2; i++ ) {
			for( j=0; j<2; j++ ) { J[i][j] = 0.0; }
		}
		for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
			J[0][0] += GNIu[0][k] * xc[k] ;
			J[0][1] += GNIu[0][k] * yc[k] ;
			
			J[1][0] += GNIu[1][k] * xc[k] ;
			J[1][1] += GNIu[1][k] * yc[k] ;
		}
		J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
		ojp = 1.0/J_p;
		iJ[0][0] =  J[1][1]*ojp;
		iJ[0][1] = -J[0][1]*ojp;
		iJ[1][0] = -J[1][0]*ojp;
		iJ[1][1] =  J[0][0]*ojp;
		
		/* global derivs at each quadrature point */
		for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
			nx[k] = iJ[0][0]*GNIu[0][k] + iJ[0][1]*GNIu[1][k];
			ny[k] = iJ[1][0]*GNIu[0][k] + iJ[1][1]*GNIu[1][k];
		}
		
		/* u,v and strain rate (e= B u) at gp */
		u_mp = v_mp = 0.0;
		exx_mp = eyy_mp = exy_mp = 0.0;
		for( k=0; k<U_BASIS_FUNCTIONS; k++ ) {
			u_mp   += NIu[k] * ux[k];
			v_mp   += NIu[k] * uy[k];
			
			exx_mp += nx[k] * ux[k];
			eyy_mp += ny[k] * uy[k];
			exy_mp += ny[k] * ux[k] + nx[k] * uy[k];
		}
		exy_mp = 0.5 * exy_mp;
		strainrateinv_mp = sqrt(0.25*(exx_mp-eyy_mp)*(exx_mp-eyy_mp)+exy_mp*exy_mp);

		/* viscous predictor */ 
		eta_mp      = rheology->const_eta0[phase_mp];
		
		//eta_mp  = mpprop_stokes->eta;
		if (eta_mp > eta_eff_continuation_max) eta_mp = eta_eff_continuation_max ;
		if (eta_mp < eta_eff_continuation_min) eta_mp = eta_eff_continuation_min ;
		/* stress */
		d1 = 2.0 * eta_mp;
		sxx_mp = d1 * exx_mp;
		syy_mp = d1 * eyy_mp;
		sxy_mp = d1 * exy_mp;
		
		trace_mp = 0.5 * ( sxx_mp + syy_mp );
		sxx_mp = sxx_mp - trace_mp;
		syy_mp = syy_mp - trace_mp;
		
		stressinv_mp = sqrt(0.25*(sxx_mp-syy_mp)*(sxx_mp-syy_mp)+sxy_mp*sxy_mp);

		/* pressure at gp */
		pressure_mp = 0.0;
		for( k=0; k<P_BASIS_FUNCTIONS; k++ ) {
			pressure_mp += NIp[k] * elp[k];
		}
		pressure_mp = pressure_mp + trace_mp;


                /*compute yield stress env*/
		Co          = rheology->mises_tau_yield[phase_mp]; 
		phi         = rheology->dp_pressure_dependance[phase_mp];
		
                /* softening bits could be replace by more specific functions in the future */  
		epl_softmin = rheology->soft_min_strain_cutoff[phase_mp]; 
		epl_softmax = rheology->soft_max_strain_cutoff[phase_mp];
    		
                if (eplastic > epl_softmin){
                    if (eplastic > epl_softmax) {
                        Co  = rheology->soft_Co_inf[phase_mp];
                        phi = rheology->soft_phi_inf[phase_mp];
                    } else {
                        float X  = (eplastic-epl_softmin)/(epl_softmax-epl_softmin);
                        Co  = Co  - X * (Co-rheology->soft_Co_inf[phase_mp]);
                        phi = phi - X * (phi-rheology->soft_phi_inf[phase_mp]);
                    }
                 }
        
        /* check the yield criteria   */ 
        /* regular failure in shear  */
		A           = sin(phi);
        B           = cos(phi);	
        tauyield_mp = A * pressure_mp + B *Co;
        yield_type  = 1; 

		if ( tauyield_mp < rheology->tens_cutoff[phase_mp]){
                /* failure in tension cutoff */
			tauyield_mp = rheology->tens_cutoff[phase_mp];
			yield_type = 2;
		} else if (tauyield_mp > rheology->Hst_cutoff[phase_mp]){   
                /* failure at High stress cut off à la boris */
			tauyield_mp = rheology->Hst_cutoff[phase_mp];
			yield_type = 3;
		}  
		
        /* viscosity */
        eta_mp = rheology->const_eta0[phase_mp];
        /* viscous stress predictor */ 
		d1 = 2.0 * eta_mp;
		sxx_mp = d1 * exx_mp;
		syy_mp = d1 * eyy_mp;
		sxy_mp = d1 * exy_mp;
		trace_mp = 0.5 * ( sxx_mp + syy_mp );
		sxx_mp = sxx_mp - trace_mp;
		syy_mp = syy_mp - trace_mp;
		
		stressinv_mp = sqrt(0.25*(sxx_mp-syy_mp)*(sxx_mp-syy_mp)+sxy_mp*sxy_mp);
		
		/* apply plastic correction when marker is yielding */ 
		is_yielding = 0; 
        if (stressinv_mp > tauyield_mp){
			eta_mp = 0.5*tauyield_mp/strainrateinv_mp;
			npoints_yielded++;
            is_yielding = yield_type; 
		}
        
        /* Continuation cutt off */ 
        if (eta_mp > eta_eff_continuation_max) eta_mp = eta_eff_continuation_max ;
		if (eta_mp < eta_eff_continuation_min) eta_mp = eta_eff_continuation_min ;    
		
		
		/* Global cuttoffs for viscosity*/
		if (eta_mp > rheology->eta_upper_cutoff_global) {
			eta_mp = rheology->eta_upper_cutoff_global;
		}
		if (eta_mp < rheology->eta_lower_cutoff_global) {
			eta_mp = rheology->eta_lower_cutoff_global;
		}
        
		/* update viscosity on marker */
		MPntPStokesSetField_eta_effective(mpprop_stokes,eta_mp);

		/* set plastic properties */
		MPntPStokesPlSetField_yield_indicator(mpprop_stokespl,is_yielding);

		
		/* monitor bounds */
		if (eta_mp > max_eta) { max_eta = eta_mp; }
		if (eta_mp < min_eta) { min_eta = eta_mp; }
	}
	
	
	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokes);
	DataFieldRestoreAccess(PField_stokespl);
	
	PetscGetTime(&t1);
  MPI_Allreduce(&min_eta,&min_eta_g,1, MPI_DOUBLE, MPI_MIN, PETSC_COMM_WORLD);
  MPI_Allreduce(&max_eta,&max_eta_g,1, MPI_DOUBLE, MPI_MAX, PETSC_COMM_WORLD);
  MPI_Allreduce(&npoints_yielded,&npoints_yielded_g,1, MPI_INT, MPI_SUM, PETSC_COMM_WORLD);
	/*
	 PetscPrintf(PETSC_COMM_WORLD,"Update rheology (visco-plastic) [mpoint]: npoints_yielded %d; (min,max)_eta %1.2e,%1.2e; log10(max/min) %1.2e; cpu time %1.2e (sec)\n",
	 npoints_yielded_g, min_eta_g, max_eta_g, log10(max_eta_g/min_eta_g), t1-t0 );
	 */
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EvaluateRheologyNonlinearitiesMarkers_ViscoPlasticY"
PetscErrorCode EvaluateRheologyNonlinearitiesMarkers_ViscoPlasticY(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[])
{	
	PetscErrorCode ierr;
	int         pidx,n_mp_points;
	DataBucket  db;
	DataField   PField_std, PField_stokes;
	PetscScalar GNIu[2][Q2_NODES_PER_EL_2D],NIp[P_BASIS_FUNCTIONS];
	PetscScalar NIu[U_BASIS_FUNCTIONS];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen_u,nen_p,e,i,j,k;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	PetscInt vel_el_lidx[U_BASIS_FUNCTIONS*2];
	PetscInt *gidx;
	PetscLogDouble t0,t1;
	PetscScalar  d1,exx_mp,eyy_mp,exy_mp,eta_mp,y0;
	PetscScalar  sxx_mp,syy_mp,sxy_mp,u_mp,v_mp,trace_mp,pressure_mp,y_mp;
	PetscScalar min_eta,max_eta,min_eta_g,max_eta_g;
	PetscInt npoints_yielded_g, npoints_yielded_tens_g,npoints_yielded,npoints_yielded_tens;
	PetscScalar phi, A, B, tauyield_mp,strainrateinv_mp,stressinv_mp,contrast; 
	
	
	RheologyConstants *rheology;
	PetscScalar eta_eff_continuation_min,eta_eff_continuation_max,min_eta_cut,max_eta_cut,alpha_m;
	
	PetscFunctionBegin;
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetGlobalIndices(dau,0,&gidx);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
	min_eta = 1.0e100;
	max_eta = 1.0e-100;
	PetscGetTime(&t0);
	/* setup for rheological parameters depending on phases */
	rheology  = &user->rheology_constants;
	
	/* continuation parameter */
	
	min_eta_cut = rheology->eta_lower_cutoff_global;
	max_eta_cut = rheology->eta_upper_cutoff_global;  
	alpha_m = (double)(user->continuation_m+1)/( (double)(user->continuation_M+1) );
    // starting from average visc.... 
	//eta_eff_continuation_max = (max_eta_cut-min_eta_cut)/2. + (max_eta_cut-min_eta_cut)*alpha_m/2.;
    //eta_eff_continuation_min = (max_eta_cut-min_eta_cut)/2. - (max_eta_cut-min_eta_cut)*alpha_m/2.;
    
    contrast=log10(max_eta_cut/min_eta_cut);
    eta_eff_continuation_max = max_eta_cut;
    eta_eff_continuation_min = max_eta_cut*pow(10.0,(-alpha_m*contrast));// - (max_eta_cut-min_eta_cut)*alpha_m;
    
    //eta_eff_continuation_max = min_eta_cut*pow(10.0,(alpha_m*contrast));
    //eta_eff_continuation_min = min_eta_cut;// - (max_eta_cut-min_eta_cut)*alpha_m;
    
    
    //eta_eff_continuation_max = min_eta_cut+(max_eta_cut-min_eta_cut)*alpha_m;
    //eta_eff_continuation_min = min_eta_cut;
    
    PetscPrintf(PETSC_COMM_WORLD," continuation viscosity cut-off:max  %1.6e  min %1.6e \n", eta_eff_continuation_max,eta_eff_continuation_min);
    // original doesn't work 
    //eta_eff_continuation_max = min_eta_cut + (max_eta_cut-min_eta_cut)*alpha_m;
    //eta_eff_continuation_min = min_eta_cut;
	//PetscPrintf(PETSC_COMM_WORLD," continuation viscosity cut-off:  %f  \n", eta_eff_continuation);
	
 	/* marker loop */
	db = user->db;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));
	
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	npoints_yielded = 0;
	npoints_yielded_tens = 0; 
    
	for (pidx=0; pidx<n_mp_points; pidx++) {
		MPntStd     *material_point;
		MPntPStokes *mpprop_stokes;
		double      *position, *xip;
		int          wil,phase_mp;
		PetscScalar J[2][2], iJ[2][2];
		PetscScalar J_p,ojp;
		
		
		DataFieldAccessPoint(PField_std,   pidx,(void**)&material_point);
		DataFieldAccessPoint(PField_stokes,pidx,(void**)&mpprop_stokes);
		
		MPntStdGetField_local_element_index(material_point,&wil);
		MPntStdGetField_global_coord(material_point,&position);
		MPntStdGetField_local_coord(material_point,&xip);
		MPntStdGetField_phase_index(material_point,&phase_mp);
		
		e = wil;
		
		/* get nodal properties for element e */
		ierr = StokesVelocity_GetElementLocalIndices(vel_el_lidx,(PetscInt*)&elnidx_u[nen_u*e]);CHKERRQ(ierr);
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],u);CHKERRQ(ierr);
		ierr = DMDAGetScalarElementField_2D(elp,nen_p,(PetscInt*)&elnidx_p[nen_p*e],p);CHKERRQ(ierr);
		
		/* get element velocity/coordinates in component form */
		for( i=0; i<Q2_NODES_PER_EL_2D; i++ ) {
			ux[i] = elu[2*i  ];
			uy[i] = elu[2*i+1];
			
			xc[i] = elcoords[2*i  ];
			yc[i] = elcoords[2*i+1];
		}
		
		
		PTatinConstructNI_Q2_2D(xip,NIu);
		PTatinConstructGNI_Q2_2D(xip,GNIu);
		ConstructNi_pressure(xip,elcoords,NIp);
		
		/* coord transformation */
		for( i=0; i<2; i++ ) {
			for( j=0; j<2; j++ ) { J[i][j] = 0.0; }
		}
		for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
			J[0][0] += GNIu[0][k] * xc[k] ;
			J[0][1] += GNIu[0][k] * yc[k] ;
			
			J[1][0] += GNIu[1][k] * xc[k] ;
			J[1][1] += GNIu[1][k] * yc[k] ;
		}
		J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
		ojp = 1.0/J_p;
		iJ[0][0] =  J[1][1]*ojp;
		iJ[0][1] = -J[0][1]*ojp;
		iJ[1][0] = -J[1][0]*ojp;
		iJ[1][1] =  J[0][0]*ojp;
		
		/* global derivs at each quadrature point */
		for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
			nx[k] = iJ[0][0]*GNIu[0][k] + iJ[0][1]*GNIu[1][k];
			ny[k] = iJ[1][0]*GNIu[0][k] + iJ[1][1]*GNIu[1][k];
		}
		
		/* u,v and strain rate (e= B u) and z at at gp */
		u_mp = v_mp = 0.0;
		exx_mp = eyy_mp = exy_mp = 0.0;
        y_mp= 0;
		for( k=0; k<U_BASIS_FUNCTIONS; k++ ) {
			u_mp   += NIu[k] * ux[k];
			v_mp   += NIu[k] * uy[k];
			
			exx_mp += nx[k] * ux[k];
			eyy_mp += ny[k] * uy[k];
			exy_mp += ny[k] * ux[k] + nx[k] * uy[k];
            
            y_mp+= NIu[k] * yc[k];
		}
		exy_mp = 0.5 * exy_mp;
		strainrateinv_mp = sqrt(0.25*(exx_mp-eyy_mp)*(exx_mp-eyy_mp)+exy_mp*exy_mp);
		
        /* constituive */
		eta_mp = mpprop_stokes->eta;
        
        d1 = 2.0 * eta_mp;
		
		/* stress */
		sxx_mp = d1 * exx_mp;
		syy_mp = d1 * eyy_mp;
		sxy_mp = d1 * exy_mp;
		
		trace_mp = 0.5 * ( sxx_mp + syy_mp );
		sxx_mp = sxx_mp - trace_mp;
		syy_mp = syy_mp - trace_mp;
		
		/* pressure at gp */
		pressure_mp = 0.0;
		for( k=0; k<P_BASIS_FUNCTIONS; k++ ) {
			pressure_mp += NIp[k] * elp[k];
		}
		pressure_mp = pressure_mp + trace_mp;
		
		
		y0          = 5.5;
        
		phi         = rheology->dp_pressure_dependance[phase_mp];
		A           = sin(phi);
		B           = (rheology->mises_tau_yield[phase_mp])*cos(phi);
		tauyield_mp = - A * (y0-y_mp) * mpprop_stokes->rho + B;
		/* check for failure in tension */
        if ( tauyield_mp < rheology->tens_cutoff[phase_mp]){
			tauyield_mp = rheology->tens_cutoff[phase_mp];
		}   
		/* check for High stress cut off à la boris */
		//if ( tauyield_mp > rheology->Hst_cutoff[phase_mp]){
		//	tauyield_mp = rheology->Hst_cutoff[phase_mp];
		//}   
		
		/* viscous predictor */ 
		eta_mp = rheology->const_eta0[phase_mp];
        
        
		//eta_mp  = mpprop_stokes->eta;
		
		//if (eta_mp > eta_eff_continuation_max) eta_mp = eta_eff_continuation_max ;
		//if (eta_mp < eta_eff_continuation_min) eta_mp = eta_eff_continuation_min ;
        
		/* stress */
		d1 = 2.0 * eta_mp;
		sxx_mp = d1 * exx_mp;
		syy_mp = d1 * eyy_mp;
		sxy_mp = d1 * exy_mp;
		
		trace_mp = 0.5 * ( sxx_mp + syy_mp );
		sxx_mp = sxx_mp - trace_mp;
		syy_mp = syy_mp - trace_mp;
		// predictor
		stressinv_mp = sqrt(0.25*(sxx_mp-syy_mp)*(sxx_mp-syy_mp)+sxy_mp*sxy_mp);
		
		/* plastic correction */ 
		if (stressinv_mp > tauyield_mp){
			/* failure in shear */
			eta_mp = 0.5*tauyield_mp/strainrateinv_mp;
			//gausspoints[n].plsr = (1.0-eta_mp/rheology->const_eta0[phase_mp]);
			npoints_yielded++;
			if (fabs(stressinv_mp-rheology->tens_cutoff[phase_mp])<1.e-12) {
				npoints_yielded_tens++;
			}
		} 
        
        if (eta_mp > eta_eff_continuation_max) eta_mp = eta_eff_continuation_max ;
		if (eta_mp < eta_eff_continuation_min) eta_mp = eta_eff_continuation_min ;    
		
		
		/* apply global cuttoffs for viscosity*/
		if (eta_mp > rheology->eta_upper_cutoff_global) {
			eta_mp = rheology->eta_upper_cutoff_global;
		}
		if (eta_mp < rheology->eta_lower_cutoff_global) {
			eta_mp = rheology->eta_lower_cutoff_global;
		}
        
		/* update viscosity on marker */
		MPntPStokesSetField_eta_effective(mpprop_stokes,eta_mp);
		
		/* monitor bounds */
		if (eta_mp > max_eta) { max_eta = eta_mp; }
		if (eta_mp < min_eta) { min_eta = eta_mp; }
	}
	
	
	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokes);
	
	PetscGetTime(&t1);
    MPI_Allreduce(&min_eta,&min_eta_g,1, MPI_DOUBLE, MPI_MIN, PETSC_COMM_WORLD);
    MPI_Allreduce(&max_eta,&max_eta_g,1, MPI_DOUBLE, MPI_MAX, PETSC_COMM_WORLD);
    MPI_Allreduce(&npoints_yielded,&npoints_yielded_g,1, MPI_INT, MPI_SUM, PETSC_COMM_WORLD);
    MPI_Allreduce(&npoints_yielded_tens,&npoints_yielded_tens_g,1, MPI_INT, MPI_SUM, PETSC_COMM_WORLD);
	/*
     PetscPrintf(PETSC_COMM_WORLD,"Update rheology (visco-plastic) [mpoint]: npoints_yielded %d; npoints_yielded_tens %d; (min,max)_eta %1.2e,%1.2e; log10(max/min) %1.2e; cpu time %1.2e (sec)\n",
     npoints_yielded_g, npoints_yielded_tens_g, min_eta_g, max_eta_g, log10(max_eta_g/min_eta_g), t1-t0 );
     */
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "TkUpdateRheologyMarkerStokes_ViscoPlastic_plastic_strain"
PetscErrorCode TkUpdateRheologyMarkerStokes_ViscoPlastic_plastic_strain(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[])
{	
	PetscErrorCode ierr;
	int         pidx,n_mp_points;
	DataBucket  db;
	DataField   PField_std, PField_stokes, PField_stokespl;
	PetscScalar GNIu[2][Q2_NODES_PER_EL_2D],NIp[P_BASIS_FUNCTIONS];
	PetscScalar NIu[U_BASIS_FUNCTIONS];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen_u,nen_p,e,i,j,k;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	PetscInt vel_el_lidx[U_BASIS_FUNCTIONS*2];
	PetscInt *gidx;
	PetscScalar  d1,exx_mp,eyy_mp,exy_mp,eta_mp,strainrateinv_mp,u_mp,v_mp;
	RheologyConstants *rheology;
	
	PetscFunctionBegin;
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetGlobalIndices(dau,0,&gidx);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
	
	/* setup for rheological parameters depending on phases */
	rheology  = &user->rheology_constants;
	
 	/* marker loop */
	db = user->db;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));

	DataBucketGetDataFieldByName(db,MPntPStokesPl_classname,&PField_stokespl);
	DataFieldGetAccess(PField_stokespl);
	DataFieldVerifyAccess(PField_stokespl,sizeof(MPntPStokesPl));
	

	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	
	for (pidx=0; pidx<n_mp_points; pidx++) {
		MPntStd       *material_point;
		MPntPStokes   *mpprop_stokes;
		MPntPStokesPl *mpprop_stokespl;
		double        *position, *xip;
		int           wil,phase_mp;
		char          is_yielding;
		float         eplastic;
		PetscScalar   J[2][2], iJ[2][2];
		PetscScalar   J_p,ojp;
		
		
		DataFieldAccessPoint(PField_std,     pidx,(void**)&material_point);
		DataFieldAccessPoint(PField_stokes,  pidx,(void**)&mpprop_stokes);
		DataFieldAccessPoint(PField_stokespl,pidx,(void**)&mpprop_stokespl);
		
                MPntPStokesPlGetField_yield_indicator(mpprop_stokespl,&is_yielding);
                /* check the particle is yielding */
                
                if (is_yielding != 0) {

			MPntStdGetField_local_element_index(material_point,&wil);
			MPntStdGetField_global_coord(material_point,&position);
			MPntStdGetField_local_coord(material_point,&xip);
			MPntStdGetField_phase_index(material_point,&phase_mp);
			MPntPStokesPlGetField_plastic_strain(mpprop_stokespl,&eplastic);
		
			e = wil;
			/* get nodal properties for element e */
			ierr = StokesVelocity_GetElementLocalIndices(vel_el_lidx,(PetscInt*)&elnidx_u[nen_u*e]);CHKERRQ(ierr);
			ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
			ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],u);CHKERRQ(ierr);
			ierr = DMDAGetScalarElementField_2D(elp,nen_p,(PetscInt*)&elnidx_p[nen_p*e],p);CHKERRQ(ierr);
			
			/* get element velocity/coordinates in component form */
			for( i=0; i<Q2_NODES_PER_EL_2D; i++ ) {
				ux[i] = elu[2*i  ];
				uy[i] = elu[2*i+1];
			
				xc[i] = elcoords[2*i  ];
				yc[i] = elcoords[2*i+1];
			}
				
			PTatinConstructNI_Q2_2D(xip,NIu);
			PTatinConstructGNI_Q2_2D(xip,GNIu);
			ConstructNi_pressure(xip,elcoords,NIp);
		
			/* coord transformation */
			for( i=0; i<2; i++ ) {
			for( j=0; j<2; j++ ) { J[i][j] = 0.0; }
			}
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				J[0][0] += GNIu[0][k] * xc[k] ;
				J[0][1] += GNIu[0][k] * yc[k] ;
			
				J[1][0] += GNIu[1][k] * xc[k] ;
				J[1][1] += GNIu[1][k] * yc[k] ;
			}
			J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
			ojp = 1.0/J_p;
			iJ[0][0] =  J[1][1]*ojp;
			iJ[0][1] = -J[0][1]*ojp;
			iJ[1][0] = -J[1][0]*ojp;
			iJ[1][1] =  J[0][0]*ojp;
			
			/* global derivs at each quadrature point */
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				nx[k] = iJ[0][0]*GNIu[0][k] + iJ[0][1]*GNIu[1][k];
				ny[k] = iJ[1][0]*GNIu[0][k] + iJ[1][1]*GNIu[1][k];
			}
		
			/* u,v and strain rate (e= B u) at gp */
			u_mp = v_mp = 0.0;
			exx_mp = eyy_mp = exy_mp = 0.0;
			for( k=0; k<U_BASIS_FUNCTIONS; k++ ) {
				u_mp   += NIu[k] * ux[k];
				v_mp   += NIu[k] * uy[k];
				
				exx_mp += nx[k] * ux[k];
				eyy_mp += ny[k] * uy[k];
				exy_mp += ny[k] * ux[k] + nx[k] * uy[k];
			}
			exy_mp = 0.5 * exy_mp;
			strainrateinv_mp = sqrt(0.25*(exx_mp-eyy_mp)*(exx_mp-eyy_mp)+exy_mp*exy_mp);
	
			/* compute plastic strain and set it */
			/* community accepted version */ 	
 	           	eplastic += strainrateinv_mp * user->dt;
#if 0
			/* laeti version */ 
			eplastic += (1.0-eta_mp/rheology->const_eta0[phase_mp])*strainrateinv_mp * user->dt; 		
			/* explanations 
                         sigmaII = eta_mp*strainrateinv	= eta0*strainrateinv_viscous
							= eta0*(strainrateinv - strainrateinvplastic)
                         eta0*strainrateinvplastic      = (eta0 - eta_mp)*strainrateinv
                         strainrateinvplastic           = (1-eta_mp/eta0)*strainrateinv	
			*/

#endif
			MPntPStokesPlSetField_plastic_strain(mpprop_stokespl,eplastic);
		}
	}
	
	
	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokes);
	DataFieldRestoreAccess(PField_stokespl);
	
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "pTatin_TkUpdateRheologyMarkerStokes_ViscoPlastic"
PetscErrorCode pTatin_TkUpdateRheologyMarkerStokes_ViscoPlastic(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[])
{	
  PetscErrorCode ierr;
  
PetscFunctionBegin;
  ierr = TkUpdateRheologyMarkerStokes_ViscoPlastic_plastic_strain(user,dau,u,dap,p);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}



 
#undef __FUNCT__
#define __FUNCT__ "EvaluateRheologyNonlinearitiesQuadratureStokes_ViscoPlastic"
PetscErrorCode EvaluateRheologyNonlinearitiesQuadratureStokes_ViscoPlastic(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[])
{	
	PetscErrorCode ierr;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
	PetscScalar GNIu[MAX_QUAD_PNTS][2][Q2_NODES_PER_EL_2D],NIp[MAX_QUAD_PNTS][P_BASIS_FUNCTIONS];
	PetscScalar NIu[MAX_QUAD_PNTS][U_BASIS_FUNCTIONS];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen_u,nen_p,e,n,k;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D],el_eta[MAX_QUAD_PNTS];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	PetscScalar Fe[Q2_NODES_PER_EL_2D*2],Be[Q2_NODES_PER_EL_2D*2];
	PetscInt vel_el_lidx[U_BASIS_FUNCTIONS*2];
	PetscInt *gidx,elgidx[2*Q2_NODES_PER_EL_2D];
	PetscInt nbcs,i,j;
	const PetscInt *ubcidx;
	Vec diag;
	PetscScalar *LA_diag;
	PetscLogDouble t0,t1;
	BCList u_bclist = user->u_bclist;
	GaussPointCoefficientsStokes *gausspoints;
	PetscScalar  d1,exx_gp,eyy_gp,exy_gp,eta_gp;
	PetscScalar  sxx_gp,syy_gp,sxy_gp,u_gp,v_gp,xpos_gp,ypos_gp,trace_p,pressure_gp;
	PetscScalar min_eta,max_eta;
  PetscScalar strainrateinv_gp,stressinv_gp;
  PetscInt npoints,npoints_yielded,npoints_yielded_tens;
  PetscScalar min_eta_g,max_eta_g; 
  PetscInt npoints_yielded_g, npoints_yielded_tens_g,npoints_g;
	static int beenHere = 1;
  RheologyConstants *rheology;
	PetscScalar alpha_m,eta_eff_continuation, min_eta_cut, max_eta_cut;
  PetscScalar soft; 
  
	PetscFunctionBegin;
	
	/* quadrature */
	/*QuadratureCreateGauss_3pnt_2D(&ngp,gp_xi,gp_weight);*/
  rheology  = &user->rheology_constants;
	ngp       = user->Q->ngp;
	gp_xi     = user->Q->xi;
	gp_weight = user->Q->weight;
	for (k=0; k<ngp; k++) {
		PetscScalar *xip = &gp_xi[2*k];
		
		PTatinConstructNI_Q2_2D(xip,NIu[k]);
		PTatinConstructGNI_Q2_2D(xip,GNIu[k]);
	}
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetGlobalIndices(dau,0,&gidx);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
	
	min_eta = 1.e100;
  max_eta = 1.e-100;
  npoints = 0;
  npoints_yielded = 0;
  npoints_yielded_tens = 0;
	PetscGetTime(&t0);
  
  // continuation parameter //
  
  min_eta_cut = rheology->eta_lower_cutoff_global;
  max_eta_cut = rheology->eta_upper_cutoff_global;  
  alpha_m = (double)(user->continuation_m)/( (double)(user->continuation_M) );
  eta_eff_continuation = min_eta_cut + (max_eta_cut-min_eta_cut)*alpha_m;
  //PetscPrintf(PETSC_COMM_WORLD," continuation viscosity cut-off:  %f  \n", eta_eff_continuation);
  //continuation_Co  = maxCo  - (maxCo-minCo)*alpha_m;
  
  for (e=0;e<nel;e++) {
		
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		ierr = StokesVelocity_GetElementLocalIndices(vel_el_lidx,(PetscInt*)&elnidx_u[nen_u*e]);CHKERRQ(ierr);
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],u);CHKERRQ(ierr);
		ierr = DMDAGetScalarElementField_2D(elp,nen_p,(PetscInt*)&elnidx_p[nen_p*e],p);CHKERRQ(ierr);
		
		/* get element velocity */
		for( i=0; i<Q2_NODES_PER_EL_2D; i++ ) {
			ux[i] = elu[2*i  ];
			uy[i] = elu[2*i+1];
			
			xc[i] = elcoords[2*i  ];
			yc[i] = elcoords[2*i+1];
		}
		/* get element pressure */
		for (n=0; n<ngp; n++) {
			PetscScalar *xip = &gp_xi[2*n];
			
			PTatinConstructNI_Q2_2D(xip,NIu[n]);
			ConstructNi_pressure(xip,elcoords,NIp[n]);
		}
		
		for (n=0; n<ngp; n++) {
			PetscScalar  J[2][2], iJ[2][2];
			PetscScalar J_p,ojp;
      PetscInt phase_gp = gausspoints[n].phase;
			PetscScalar tauyield_gp, phi, A , B;
      
			/* coord transformation */
			for( i=0; i<2; i++ ) {
				for( j=0; j<2; j++ ) { J[i][j] = 0.0; }
			}
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				J[0][0] += GNIu[n][0][k] * xc[k] ;
				J[0][1] += GNIu[n][0][k] * yc[k] ;
				
				J[1][0] += GNIu[n][1][k] * xc[k] ;
				J[1][1] += GNIu[n][1][k] * yc[k] ;
			}
			J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
			ojp = 1.0/J_p;
			iJ[0][0] =  J[1][1]*ojp;
			iJ[0][1] = -J[0][1]*ojp;
			iJ[1][0] = -J[1][0]*ojp;
			iJ[1][1] =  J[0][0]*ojp;
			
			/* global derivs at each quadrature point */
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				nx[k] = iJ[0][0]*GNIu[n][0][k] + iJ[0][1]*GNIu[n][1][k];
				ny[k] = iJ[1][0]*GNIu[n][0][k] + iJ[1][1]*GNIu[n][1][k];
			}
      
			/* x,y and u,v and strain rate (e= B u) at gp */
			xpos_gp = ypos_gp = 0.0;
			u_gp = v_gp = 0.0;
			exx_gp = eyy_gp = exy_gp = 0.0;
			for( i=0; i<U_BASIS_FUNCTIONS; i++ ) {
				xpos_gp   += NIu[n][i] * xc[i];
				ypos_gp   += NIu[n][i] * yc[i];
				
				u_gp   += NIu[n][i] * ux[i];
				v_gp   += NIu[n][i] * uy[i];
				
				exx_gp += nx[i] * ux[i];
				eyy_gp += ny[i] * uy[i];
				exy_gp += ny[i] * ux[i] + nx[i] * uy[i];
			}
			exy_gp = 0.5 * exy_gp;
			strainrateinv_gp = sqrt(0.25*(exx_gp-eyy_gp)*(exx_gp-eyy_gp)+exy_gp*exy_gp);
      
			/* isotropic constituive behaviour requires only one viscosity be defined */
			
      
      /*stress update */ 
      /* we need to check if this is absolutely necessary */ 
      
			eta_gp = gausspoints[n].eta;
			/* stress */
			d1 = 2.0 * eta_gp;
			sxx_gp = d1 * exx_gp;
			syy_gp = d1 * eyy_gp;
			sxy_gp = d1 * exy_gp;
			
			trace_p = 0.5 * ( sxx_gp + syy_gp );
			sxx_gp = sxx_gp - trace_p;
			syy_gp = syy_gp - trace_p;
			
			stressinv_gp = sqrt(0.25*(sxx_gp-syy_gp)*(sxx_gp-syy_gp)+sxy_gp*sxy_gp);
      
			/* pressure at gp */
			pressure_gp = 0.0;
			for( i=0; i<P_BASIS_FUNCTIONS; i++ ) {
				pressure_gp += NIp[n][i] * elp[i];
			}
			pressure_gp  = pressure_gp + trace_p;
      
      /* yield surface */
      soft = 0.0; 
      if (rheology->gamma_soft[phase_gp] > 0){
        PetscReal char_strain = gausspoints[n].pls/rheology->gamma_soft[phase_gp];
        if (char_strain > 1) char_strain = 1;
        soft = char_strain*(rheology->mises_tau_yield_inf[phase_gp]-rheology->mises_tau_yield[phase_gp]);
      }
      phi         = rheology->dp_pressure_dependance[phase_gp];
      A           = sin(phi);
      B           = (rheology->mises_tau_yield[phase_gp]+soft)*cos(phi);
      tauyield_gp = A * pressure_gp + B;
      
      /* check for failure in tension */
      
      if ( tauyield_gp < rheology->tens_cutoff[phase_gp]){
        tauyield_gp = rheology->tens_cutoff[phase_gp];
      }   
      
      if ( tauyield_gp > 1.e4){
        tauyield_gp = 1.e4 ;
      }
      /*  {
       PetscScalar opts_srH,opts_Lx;
       opts_srH=1.0;
       opts_Lx=15.0;
       ierr = PetscOptionsGetScalar(PETSC_NULL,"-notch_srH",&opts_srH,0);CHKERRQ(ierr);
       ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_Lx",&opts_Lx,0);CHKERRQ(ierr);
       if (opts_srH > 0.0) {  
       tauyield_gp = A * (pressure_gp+ 4.0*opts_srH/opts_Lx*eta_gp) + B;     
       }
       } 
       */
      
      
      /* viscous predictor */ 
      eta_gp = rheology->const_eta0[phase_gp];
      gausspoints[n].plsr = 0.0;
      //eta_gp  = gausspoints[n].eta_ref;
      if (eta_gp > eta_eff_continuation) eta_gp = eta_eff_continuation ;
      
      /* stress */
      d1 = 2.0 * eta_gp;
      sxx_gp = d1 * exx_gp;
      syy_gp = d1 * eyy_gp;
      sxy_gp = d1 * exy_gp;
      
      trace_p = 0.5 * ( sxx_gp + syy_gp );
      sxx_gp = sxx_gp - trace_p;
      syy_gp = syy_gp - trace_p;
      
      stressinv_gp = sqrt(0.25*(sxx_gp-syy_gp)*(sxx_gp-syy_gp)+sxy_gp*sxy_gp);
      
      
      /* plastic correction */ 
      
      if (stressinv_gp >= tauyield_gp){
        /* failure in shear */
        eta_gp = 0.5*tauyield_gp/strainrateinv_gp;
        gausspoints[n].plsr = strainrateinv_gp;
        npoints_yielded++;
        if (abs(stressinv_gp-rheology->tens_cutoff[phase_gp])<1.e-12) npoints_yielded_tens++;
      }
      
      /* update the viscosity, fu_x, fu_y, fp  */
      
      
      /* set the viscosity on the quadrature point */  
      gausspoints[n].eta = eta_gp;				
      
      /* monitor bounds */
      //if (gausspoints[n].eta > max_eta) { max_eta = gausspoints[n].eta; }
      //if (gausspoints[n].eta < min_eta) { min_eta = gausspoints[n].eta; }
      
      /*
       if (gausspoints[n].eta > eta_upper_cutoff) {
       gausspoints[n].eta = eta_upper_cutoff;
       }
       if (gausspoints[n].eta < eta_lower_cutoff) {
       gausspoints[n].eta = eta_lower_cutoff;
       }
       */
      
      /* update point counter */
      npoints++;
    }
  }
  
  /* */
  // continuation on the viscosity //
  
  /*	
   min_eta = 1.0e100;
   max_eta = 1.0e-100;
   for (e=0;e<nel;e++) {
   ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
   for (n=0; n<ngp; n++) {
   PetscScalar alpha_m,eta_eff_continuation;
   
   alpha_m = (double)(user->continuation_m)/( (double)(user->continuation_M) );
   
   //eta_eff_continuation = pow( gausspoints[n].eta, alpha_m );
   
   eta_eff_continuation=gausspoints[n].eta;
   gausspoints[n].eta = eta_eff_continuation;				
   
   // monitor bounds 
   if (gausspoints[n].eta > max_eta) { max_eta = gausspoints[n].eta; }
   if (gausspoints[n].eta < min_eta) { min_eta = gausspoints[n].eta; }
   }
   }
   */	
  
  /* Set all props based on the center gauss point */
  /*
   for (e=0;e<nel;e++) {
   ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
   for (n=0; n<ngp; n++) {
   gausspoints[n].eta = gausspoints[4].eta;
   }
   }
   */
  
  
  /* Apply viscosity limiters ? */
  if (rheology->apply_viscosity_cutoff_global) {
    PetscPrintf(PETSC_COMM_WORLD,"Update rheology (visco-plastic): Enforcing global viscosity cut-off, %1.2e, %1.2e\n",
                rheology->eta_upper_cutoff_global,rheology->eta_lower_cutoff_global);
    for (e=0;e<nel;e++) {
      ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
      for (n=0; n<ngp; n++) {
        
        if (gausspoints[n].eta > rheology->eta_upper_cutoff_global) {
          gausspoints[n].eta = rheology->eta_upper_cutoff_global;
        }
        if (gausspoints[n].eta < rheology->eta_lower_cutoff_global) {
          gausspoints[n].eta = rheology->eta_lower_cutoff_global;
        }
        
      }
    }
  }
	// I removed that because it was buggy and never used... it is obsolete with markers and should probably be cleaned
	/* 
	 if (rheology->apply_viscosity_cutoff) {
	 PetscPrintf(PETSC_COMM_WORLD,"Update rheology (visco-plastic): Enforcing per-phase viscosity cut-off\n");
	 for (e=0;e<nel;e++) {
	 ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
	 for (n=0; n<ngp; n++) {
	 PetscInt phase_p;
	 
	 phase_p = gausspoints[n].phase_index;
	 if (gausspoints[n].eta > rheology->eta_upper_cutoff[phase_p]) {
	 gausspoints[n].eta = rheology->eta_upper_cutoff[phase_p];
	 }
	 if (gausspoints[n].eta < rheology->eta_lower_cutoff[phase_p]) {
	 gausspoints[n].eta = rheology->eta_lower_cutoff[phase_p];
	 }
	 
	 }
	 }
	 }
	 */  
  
  for (e=0;e<nel;e++) {
    ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
    for (n=0; n<ngp; n++) {
      PetscInt phase_p;
			
      /* monitor bounds */
      if (gausspoints[n].eta > max_eta) { max_eta = gausspoints[n].eta; }
      if (gausspoints[n].eta < min_eta) { min_eta = gausspoints[n].eta; }
      
      
    }
  }  
  
  PetscGetTime(&t1);
  MPI_Allreduce(&min_eta,&min_eta_g,1, MPI_DOUBLE, MPI_MIN, PETSC_COMM_WORLD);
  MPI_Allreduce(&max_eta,&max_eta_g,1, MPI_DOUBLE, MPI_MAX, PETSC_COMM_WORLD);
  MPI_Allreduce(&npoints_yielded,&npoints_yielded_g,1, MPI_INT, MPI_SUM, PETSC_COMM_WORLD);
  MPI_Allreduce(&npoints_yielded_tens,&npoints_yielded_tens_g,1, MPI_INT, MPI_SUM, PETSC_COMM_WORLD);
  MPI_Allreduce(&npoints,&npoints_g,1, MPI_INT, MPI_SUM, PETSC_COMM_WORLD);	
  PetscPrintf(PETSC_COMM_WORLD,"Update rheology (visco-plastic): npoints %d; npoints_yielded %d; npoints_yielded_tens %d; (min,max)_eta %1.2e,%1.2e; log10(max/min) %1.2e; cpu time %1.2e (sec)\n",
              npoints_g, npoints_yielded_g, npoints_yielded_tens_g, min_eta_g, max_eta_g, log10(max_eta_g/min_eta_g), t1-t0 );
  
  ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
  
  beenHere++;
  
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "TkUpdateRheologyQuadratureStokes_ViscoPlastic_plastic_strain"
PetscErrorCode TkUpdateRheologyQuadratureStokes_ViscoPlastic_plastic_strain(pTatinCtx user,DM dau)
{	
  PetscErrorCode ierr;
  PetscInt e,nel, n, ngp, dum;
  const PetscInt *dum2;
  GaussPointCoefficientsStokes *gausspoints;
  
	PetscFunctionBegin;
  ierr = DMDAGetElements_pTatin(dau, &nel, &dum, &dum2);CHKERRQ(ierr);
  ngp  = user->Q->ngp;
  
  for (e=0;e<nel;e++) {
    ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
    for (n=0; n<ngp; n++) {
      
      gausspoints[n].pls += gausspoints[n].plsr * user->dt ;       
    }
  }
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatin_TkUpdateRheologyQuadratureStokes_ViscoPlastic"
PetscErrorCode pTatin_TkUpdateRheologyQuadratureStokes_ViscoPlastic(pTatinCtx user,DM dau)
{	
  PetscErrorCode ierr;
  
	PetscFunctionBegin;
  ierr = TkUpdateRheologyQuadratureStokes_ViscoPlastic_plastic_strain(user,dau);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
