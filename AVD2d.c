/*
 *  AVD2d.c
 *  
 *
 *  Created by Dave A. May on 11/16/11.
 *  Copyright 2011 Geophysical Fluid Dynamics. All rights reserved.
 *
 *  The algorithm computes an Approximate Voronoi Diagram (AVD) in 2D using a given set of point coordinates.
 *  
 *  The AVD algorithm, is described in:
 *    M. Velic, D.A. May & L. Moresi,
 *    "A Fast Robust Algorithm for Computing Discrete Voronoi Diagrams",
 *    Journal of Mathematical Modelling and Algorithms,
 *    Volume 8, Number 3, 343-355, DOI: 10.1007/s10852-008-9097-6
 *
 *
 *  Notes:
 *    This implementation uses von-Neumann neighbourhoods for boundary chain growth. 
 *    Do not be tempted to implement "diagonal" neighbourhood growth cycles - this will greatly increase the
 *    size of the boundary chain (and thus memory usage will increase and CPU time will decrease).
 *
 *  Compilation:
 *    gcc -O3 -c AVD2d.c
 *
 */


#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"
#include "time.h"
#include "sys/time.h"

#include "AVD2d.h"

void AVDGetTime(double *time)
{
	struct timeval t1;
	double tt;
	
	gettimeofday(&t1,NULL);
	tt = (double)(t1.tv_sec*1000000 + t1.tv_usec);
	
	*time = tt / 1000000.0; /* convert from micro-sec to seconds */
}

void _AVDCell2dCreate(const Aint mx,const Aint my,AVDCell2d *C)
{
	AVDCell2d cells;
	Aint i,j;
	
	cells = malloc( sizeof(struct _p_AVDCell2d)*(mx*my) );
	memset( cells, 0, sizeof(struct _p_AVDCell2d)*(mx*my) );

	for (j=0; j<my; j++) {
		for (i=0; i<mx; i++) {
			Aint index;
			
			index = i + j * mx;
			cells[index].index = index;
			cells[index].i = i;
			cells[index].j = j;

			if ( (i==0) || (i==mx-1) ) { cells[index].p = AVD_CELL_MASK; }
			if ( (j==0) || (j==my-1) ) { cells[index].p = AVD_CELL_MASK; }
		
			//printf("II, i,j=%d %d :  cid = %d : mask = %d \n", i,j, cells[index].index, cells[index].p );
		}
	}
	
	*C = cells;
}

void AVDCell2dDestroy(AVDCell2d *C)
{
	AVDCell2d cells;
	
	if (!C) { return; }
	cells = *C;
	free(cells);
	*C = NULL;
}

void AVDCell2dReset(AVD2d A)
{
	Aint i,j;
	Aint mx,my;
	
	mx = A->mx_mesh;
	my = A->my_mesh;

	for (j=0; j<my; j++) {
		for (i=0; i<mx; i++) {
			Aint ii = i + j * mx;

			A->cells[ii].p = -1;
			A->cells[ii].done = AVD_FALSE;

			if ( (i==0) || (i==mx-1) ) { A->cells[ii].p = AVD_CELL_MASK; }
			if ( (j==0) || (j==my-1) ) { A->cells[ii].p = AVD_CELL_MASK; }
		}
	}
}

void _AVDChainCreate(const Aint npoints, const Aint buffer,AVDChain *CH)
{
	AVDChain chains;
	Aint p;
	
	chains = malloc( sizeof(struct _p_AVDChain)*(npoints) );
	memset( chains, 0, sizeof(struct _p_AVDChain)*(npoints) );
	for (p=0; p<npoints; p++) {
		chains[p].new_claimed_cells_malloced = buffer;
		chains[p].new_boundary_cells_malloced = buffer;
		
		chains[p].new_claimed_cells = malloc (sizeof(Aint)*buffer );
		chains[p].new_boundary_cells = malloc (sizeof(Aint)*buffer );
	}
	
	*CH = chains;
}

void AVDChainDestroy(const Aint npoints,AVDChain *CH)
{
	AVDChain chains;
	Aint p;
	
	if (!CH) { return; }
	chains = *CH;
	for (p=0; p<npoints; p++) {
		if (chains[p].new_claimed_cells) {
			free( chains[p].new_claimed_cells );
			chains[p].new_claimed_cells = NULL;
		}

		if (chains[p].new_boundary_cells) {
			free( chains[p].new_boundary_cells );
			chains[p].new_boundary_cells = NULL;
		}
	}
	free(chains);
	*CH = NULL;
}

void AVDPoint2dCreate(const Aint npoints, AVDPoint2d *P)
{
	AVDPoint2d points;

	points = malloc( sizeof(struct _p_AVDPoint2d)*(npoints) );
	memset( points, 0, sizeof(struct _p_AVDPoint2d)*(npoints) );
	
	*P = points;
}

void AVDPoint2dDestroy(AVDPoint2d *P)
{
	AVDPoint2d points;
	
	if (!P) { return; }
	points = *P;
	free(points);
	*P = NULL;
}


/*
	i = (xp - (x0-dx) )/mx_mesh
 
 
*/
void AVD2dCreate(const Aint mx,const Aint my,const Aint buffer,AVD2d *A)
{
	AVD2d avd2d;
	
	avd2d = malloc( sizeof(struct _p_AVD2d) );
	memset( avd2d, 0, sizeof(struct _p_AVD2d) );

	avd2d->buffer = buffer;
	avd2d->mx = mx;
	avd2d->my = my;

	avd2d->mx_mesh = mx+2;
	avd2d->my_mesh = my+2;

	_AVDCell2dCreate((const Aint)avd2d->mx_mesh,(const Aint)avd2d->my_mesh, &avd2d->cells);
	
	*A = avd2d;
}

void AVD2dDestroy(AVD2d *A)
{
	AVD2d aa;
	if (!A) { return; }
	aa = *A;
	if (aa->chains) {
		AVDChainDestroy(aa->npoints,&aa->chains);
	}
	if (aa->cells) {
		AVDCell2dDestroy(&aa->cells);
	}
	if (aa->points) {
		AVDPoint2dDestroy(&aa->points);
	}
	
	free(aa);
	*A = NULL;
}

void AVD2dSetDomainSize(AVD2d A,const double x0,const double x1,const double y0,const double y1)
{
	A->x0 = x0;
	A->x1 = x1;
	A->y0 = y0;
	A->y1 = y1;
	
	A->dx = (x1-x0)/(double)A->mx;
	A->dy = (y1-y0)/(double)A->my;
}

void AVD2dSetPoints(AVD2d A,const Aint npoints,AVDPoint2d points)
{
	
	if (A->chains) {
//	printf("Deallocating existing chains\n");
		AVDChainDestroy(A->npoints,&A->chains);
	} 
	_AVDChainCreate(npoints,(const Aint)A->buffer,&A->chains);

	A->npoints = npoints;
	A->points = points;
}

static inline double AVD2dDistanceTest(double x0,double y0,double x1,double y1,double x2,double y2)
{
	return (x1+x2-x0-x0)*(x1-x2) + (y1+y2-y0-y0)*(y1-y2);
}

/* Claim cells for particle p_i in the list */
void AVD2dClaimCells(AVD2d A,const Aint p_i)
{
	Aint i,count;
	double x0,y0,x1,y1,x2,y2,dist1;
	Aint *temp;
	AVDChain bchain;
	AVDPoint2d points;
	AVDCell2d cells,cell0;
	Aint index0,cell_num0,cell_num1,cell_num[4];
	Aint buffer;
	double dx,dy;
	
	//printf("AVD2dClaimCells (%d)\n",p_i);
	buffer = A->buffer;
	dx     = A->dx;
	dy     = A->dy;
	bchain = &A->chains[p_i];
	cells  = A->cells;
	points = A->points;
	
	count = 0;
	bchain->num_claimed = 0;
	
	for (i=0; i<bchain->length; i++) {
		cell_num0 = bchain->new_boundary_cells[i]; /* cell number we are trying to claim */

#ifdef __AVD_DEBUG_MODE
		if (cell_num0<0) {
			printf("  AVD2dClaimCells(ERROR): p_i = %ld, [%ld] \n", p_i,cell_num1 );
			printf("  AVD2dClaimCells(ERROR):   point %f %f \n", A->points[p_i].x,A->points[p_i].y);
			exit(0);
		}
		if (cells[cell_num0].p == AVD_CELL_MASK) { printf("YOU SHOULD NEVER HAVE A MASKED CELL IN YOUR LIST\n"); exit(1); }
#endif
		//printf("  Testing i,j = %d,%d \n", cells[cell_num0].i,cells[cell_num0].j );
		
		if (cells[cell_num0].p == AVD_CELL_UNCLAIMED) { /* if cell unclaimed, then claim it */
			/* Realloc, note that we need one space more than the number of points to terminate the list */
			if( count == bchain->new_claimed_cells_malloced-1  ){
				temp = realloc( bchain->new_claimed_cells, (bchain->new_claimed_cells_malloced + buffer)*sizeof(Aint) );
				bchain->new_claimed_cells = temp;
				bchain->new_claimed_cells_malloced += buffer;
				
				temp = realloc( bchain->new_boundary_cells, (bchain->new_boundary_cells_malloced + buffer)*sizeof(Aint) );
				bchain->new_boundary_cells = temp;
				bchain->new_boundary_cells_malloced += buffer; 
			}
			bchain->new_claimed_cells[count] = cell_num0;
			bchain->num_claimed++;
			count++;
			cells[cell_num0].p = p_i; /* mark cell as owned by particle p_i */
		} 
#ifdef AVD_VON_NEUMANN_NEIGHBOURS 
		else if (cells[cell_num0].p != p_i) {
			/* perform distance test between points to determine ownership */
			x2 = points[p_i].x;
			y2 = points[p_i].y;
			
			x1 = points[cells[cell_num0].p].x;
			y1 = points[cells[cell_num0].p].y;
			
			/* cell centroid */
			x0 = cells[cell_num0].i*dx + (A->x0 - dx + 0.5*dx);
			y0 = cells[cell_num0].j*dy + (A->y0 - dy + 0.5*dy);
			
			dist1 = AVD2dDistanceTest(x0,y0,x1,y1,x2,y2);
			if (dist1 > 0.0) {
				bchain->new_claimed_cells[count] = cell_num0;
				bchain->num_claimed++;
				count++;
				cells[cell_num0].p = p_i; /* mark cell as owned by particle p_i */
			}
		}
#endif
		
		bchain->new_claimed_cells[count] = -1; /* mark end of list */
	}
}

void AVD2dUpdateChain(AVD2d A,const Aint p_i)
{
	Aint i,k;
	Aint count;
	Aint index0,cell_num0,cell_num1,cell_num[4];
	AVDChain bchain;
	AVDCell2d cells,cell0;
	Aint mx,my,buffer;
	Aint *temp;
	
	//printf("AVD3dUpdateChain (%d) \n",p_i);
	buffer = A->buffer;
	mx     = A->mx_mesh;
	my     = A->my_mesh;
	bchain = &A->chains[p_i];
	cells  = A->cells;
	
	count = 0;
	bchain->length = 0;
	for( i=0; i<bchain->num_claimed; i++) {
		cell_num0 = bchain->new_claimed_cells[i];
		cell0 = &cells[cell_num0];
		
		if (cell0->p == AVD_CELL_MASK) { continue; }

		cell_num[0] = (cell0->i  ) + (cell0->j-1)*mx ; // S
		cell_num[1] = (cell0->i  ) + (cell0->j+1)*mx ; // N
		cell_num[2] = (cell0->i+1) + (cell0->j  )*mx ; // E
		cell_num[3] = (cell0->i-1) + (cell0->j  )*mx ; // W

		//printf("UC: cell_num0 = %d:, cell0->index = %d , i,j = %d,%d \n", cell_num0, cell0->index, cell0->i,cell0->j );
		//printf("  %d %d , %d %d \n", cell_num[0],cell_num[1],cell_num[2], cell_num[3] );
		
		/* boundary protection */
		for (k=0; k<4; k++) {
			//printf("  k=%d :: CHEKING i,j,k = %d,%d,%d [%d]\n", k,cells[cell_num[k]].i,cells[cell_num[k]].j,cells[cell_num[k]].k,cell_num[k] );
			if (cells[cell_num[k]].p == AVD_CELL_MASK) {
				//printf("    k=%d :: REJECTING i,j,k = %d,%d,%d [%d]\n", k,cells[cell_num[k]].i,cells[cell_num[k]].j,cells[cell_num[k]].k,cell_num[k] );
				cell_num[k] = -2;
			}
		}
		
		for (k=0; k<4; k++) {
			cell_num1 = cell_num[k];
			//printf("  k=%d :: cell_num1 = %d \n", k,cell_num1 );
			/*
			 if cell does not already belong to the particle and hasn't been
			 marked as being done then add it to new boundary array and mark it as done
			*/
			if (cell_num1 != -2) {
				if ( (cells[cell_num1].p != p_i) && (cells[cell_num1].done != AVD_TRUE) ) {
					/* Realloc, note that we need one space more than the number of points to terminate the list */
					if (count == bchain->new_boundary_cells_malloced-1 ) {
						temp = (Aint*)realloc( bchain->new_claimed_cells, (bchain->new_claimed_cells_malloced + buffer)*sizeof(Aint) );
						bchain->new_claimed_cells = temp;
						bchain->new_claimed_cells_malloced += buffer;

						temp = (Aint*)realloc( bchain->new_boundary_cells, (bchain->new_boundary_cells_malloced + buffer)*sizeof(Aint) );
						bchain->new_boundary_cells = temp;
						bchain->new_boundary_cells_malloced += buffer; 
					}
					//printf("  INSERTING i,j = %d,%d [%d] \n", cells[cell_num1].i,cells[cell_num1].j,cell_num1 );
#ifdef __AVD_DEBUG_MODE
					if (cell_num1<0) {
						printf("  AVD3dUpdateChain(ERROR): INSERTING negative cell index \n");
						printf("  AVD3dUpdateChain(ERROR):   k=%ld :: cell0 i,j = %ld,%ld neighbourid [%ld]\n", k,cell0->i, cell0->j, cell_num1 );
						exit(0);
					}
#endif					
					bchain->new_boundary_cells[count] = cell_num1;
					bchain->length++;
					count++;
					cells[cell_num1].done = AVD_TRUE;
				}
			}
		}
		
	}

	/* reset the processed flags */
	for (i=0; i<count; i++){
		cells[ bchain->new_boundary_cells[i] ].done = AVD_FALSE;
	}
}

/* I would like to be able to use this function with different numbers of particles */
void AVD2dInit(AVD2d A,const Aint npoints,AVDPoint2d points)
{
	Aint p,i,j;
	Aint mx,my,index;
	
	//printf("AVD2dInit: \n");
	if (npoints != A->npoints) {
		printf("AVD2dInit: npoints != A->npoints\n");
		exit(0);
	}
	
	mx = A->mx_mesh;
	my = A->my_mesh;
	
	for (p=0; p<npoints; p++){
		/* check if point outside the domain */
		if (points[p].x < A->x0) {  printf("AVD2dInit(ERROR): xp(%1.6f) < x0(%1.6f) \n", points[p].x,A->x0); exit(1); }
		if (points[p].y < A->y0) {  printf("AVD2dInit(ERROR): yp(%1.6f) < y0(%1.6f) \n", points[p].y,A->y0); exit(1); }

		if (points[p].x > A->x1) {  printf("AVD2dInit(ERROR): xp(%1.6f) > x1(%1.6f) \n", points[p].x,A->x1); exit(1); }
		if (points[p].y > A->y1) {  printf("AVD2dInit(ERROR): yp(%1.6f) > y1(%1.6f) \n", points[p].y,A->y1); exit(1); }
		
		
		i = (points[p].x - (A->x0 - A->dx))/A->dx;
		j = (points[p].y - (A->y0 - A->dy))/A->dy;

		/* If a particle is exactly on the border then make sure it is in a valid cell inside the element */
		if (i == mx-1) { i--; }
		if (j == my-1) { j--; }
		//printf("pp[%d]: i,j = %d %d \n", p,i,j );
		
		if (i<=0) { printf("AVD2dInit(ERROR): i(%ld)<=0: (p=%ld) %lf %lf \n", i,p,points[p].x, points[p].y ); exit(1); }
		if (j<=0) { printf("AVD2dInit(ERROR): j(%ld)<=0: (p=%ld) %lf %lf \n", j,p,points[p].x, points[p].y ); exit(1); }

		if (i>=A->mx_mesh-1) { printf("AVD2dInit(ERROR): i>=mx-1: %lf %lf \n", points[p].x, points[p].y ); exit(1); }
		if (j>=A->my_mesh-1) { printf("AVD2dInit(ERROR): j>=my-1: %lf %lf \n", points[p].x, points[p].y ); exit(1); }
		
		
		index = i+j*mx;
		if (A->cells[index].p == AVD_CELL_MASK) {
			printf("AVD2dInit(ERROR): Inserting cells into boundary cells - this is not permitted\n");
			exit(1);
		}
		
		A->cells[i+j*mx].p = p; /* particle index */
		A->chains[p].num_claimed = 1; /* number of claimed cells, currently just the one the point initially resides within */
		A->chains[p].length = 0;
		A->chains[p].total_claimed = 1; /* total of claimed cells */
		A->chains[p].done = AVD_FALSE;
		A->chains[p].index = i+j*mx; /* ith particle is in cell i +j*mx */
		A->chains[p].new_claimed_cells[0] = i+j*mx;  /* ith particle claimed cell it resides within, i.e. cell_index i+j*mx+k*mx*my */
		A->chains[p].new_claimed_cells[1] = -1; /* mark end of claimed_cells list with -1 */

		AVD2dUpdateChain(A,p);
	}
		
}

void AVD2dReportMemory(AVD2d A)
{
	Aint c,ncells,npoints;
	double mem_cells, mem_points, mem_chain;
	
	ncells = A->mx_mesh * A->my_mesh;
	mem_cells = (double)(sizeof(struct _p_AVDCell2d) * ncells );

	npoints = A->npoints;
	mem_points = (double)(sizeof(struct _p_AVDPoint2d) * npoints );

	mem_chain = 0.0;
	for (c=0; c<npoints; c++) {
		mem_chain = mem_chain + sizeof(Aint) * 7.0 + sizeof(char) * 1.0 + sizeof(char*) * 2.0;
		mem_chain = mem_chain + (double)( sizeof(Aint) * A->chains[c].new_boundary_cells_malloced );
		mem_chain = mem_chain + (double)( sizeof(Aint) * A->chains[c].new_claimed_cells_malloced );
	}

	printf("AVD2d Memory usage: \n");
	printf("  points:  %1.4f (MB) \n", mem_points*1.0e-6);
	printf("  cells:   %1.4f (MB) \n", mem_cells*1.0e-6);
	printf("  chains:  %1.4f (MB) \n", mem_chain*1.0e-6);
	printf("  [total]: %1.4f (MB) \n", (mem_chain+mem_cells+mem_points)*1.0e-6);
	
}

void AVD2dPointsView_VTU(AVD2d A,const char name[])
{
	FILE*	fp;
	Aint  c,i,npoints;
	
	if ((fp = fopen ( name, "w")) == NULL)  {
		exit(1);
	}

	npoints = A->npoints;
	
	fprintf(fp, "<?xml version=\"1.0\"?>\n");

#ifdef WORDSIZE_BIGENDIAN
        fprintf(fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
#else
        fprintf(fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
#endif
	fprintf(fp, "  <UnstructuredGrid>\n");
	fprintf(fp, "    <Piece NumberOfPoints=\"%d\" NumberOfCells=\"%d\" >\n",npoints,npoints);
	
	
	fprintf(fp, "    <Points>\n");
	fprintf(fp, "      <DataArray type=\"Float32\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	for( i=0; i<npoints; i++ ) {
		fprintf(fp, "      %1.4e %1.4e %1.4e \n", A->points[i].x,A->points[i].y,0.0 );
	}
	fprintf(fp, "      </DataArray>\n");
	fprintf(fp, "    </Points>\n");
	
	
	fprintf(fp, "    <PointData>\n");
	
	fprintf(fp, "      <DataArray type=\"Int32\" Name=\"pid\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	for( i=0; i<npoints; i++ ) {
		fprintf(fp, "      %d \n", i );
	}
	fprintf(fp, "      </DataArray>\n");

	fprintf(fp, "      <DataArray type=\"Int32\" Name=\"phase\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	for( i=0; i<npoints; i++ ) {
		fprintf(fp, "      %d \n", A->points[i].phase );
	}
	fprintf(fp, "      </DataArray>\n");
	
	fprintf(fp, "    </PointData>\n");
	
	
	
	
	
	/* UNSTRUCTURED GRID DATA */
	fprintf(fp, "    <Cells>\n");
	
	// connectivity //
	fprintf(fp, "      <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");
	fprintf(fp,"      ");		
	for (c=0; c<npoints; c++) {
		fprintf(fp,"%d ", c);
	}
	fprintf(fp,"\n");		
	fprintf(fp, "      </DataArray>\n");
	
	// offsets //
	fprintf(fp, "      <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	fprintf(fp,"      ");		
	for (c=0; c<npoints; c++) {
		fprintf(fp,"%d ", (c+1));
	}
	fprintf(fp,"\n");		
	fprintf(fp, "      </DataArray>\n");
	
	// types //
	fprintf(fp, "      <DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\">\n");
	fprintf(fp,"      ");		
	for (c=0; c<npoints; c++) {
		fprintf(fp,"%d ", 1);		
	}
	fprintf(fp,"\n");		
	fprintf(fp, "      </DataArray>\n");
	
	fprintf(fp, "    </Cells>\n");
	
	
	fprintf(fp, "    </Piece>\n");
  fprintf(fp, "  </UnstructuredGrid>\n");
	fprintf(fp, "</VTKFile>\n");
	
	fclose( fp );
}

void AVD2dCellsView_VTR(AVD2d A,const char name[])
{
	FILE*	fp;
	Aint  c,i,j;
	
	if ((fp = fopen ( name, "w")) == NULL)  {
		exit(1);
	}
	
	fprintf(fp, "<?xml version=\"1.0\"?>\n");
#ifdef WORDSIZE_BIGENDIAN
        fprintf(fp, "<VTKFile type=\"RectilinearGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
#else
        fprintf(fp, "<VTKFile type=\"RectilinearGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
#endif
	fprintf(fp, "  <RectilinearGrid WholeExtent=\"%d %d %d %d %d %d\" >\n", 0,A->mx, 0,A->my, 0,1);
	fprintf(fp, "    <Piece Extent=\"%d %d %d %d %d %d\" >\n", 0,A->mx, 0,A->my, 0,1);
	
	fprintf(fp, "    <Coordinates>\n");
	/* X */
	fprintf(fp, "      <DataArray type=\"Float32\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	for( i=0; i<A->mx+1; i++ ) {
		fprintf(fp, "      %1.4e \n", A->x0 + (i)*A->dx );
	}
	fprintf(fp, "      </DataArray>\n");
	/* Y */
	fprintf(fp, "      <DataArray type=\"Float32\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	for( i=0; i<A->my+1; i++ ) {
		fprintf(fp, "      %1.4e \n", A->y0 + (i)*A->dy );
	}
	fprintf(fp, "      </DataArray>\n");
	/* Z */
	fprintf(fp, "      <DataArray type=\"Float32\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	for( i=0; i<1; i++ ) {
		fprintf(fp, "      %1.4e \n", 0.0 );
	}
	fprintf(fp, "      </DataArray>\n");
	
	fprintf(fp, "    </Coordinates>\n");

	fprintf(fp, "    <CellData>\n");

	// pid
	fprintf(fp, "      <DataArray type=\"Int32\" Name=\"pid\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	for (j=1; j<A->my+1; j++) {
		for (i=1; i<A->mx+1; i++) {
			Aint ii = i + j*A->mx_mesh;
			
			fprintf(fp, "      %d \n", A->cells[ii].p	);			
		}
	}
	fprintf(fp, "      </DataArray>\n");

	// phase
	fprintf(fp, "      <DataArray type=\"Int32\" Name=\"phase\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	for (j=1; j<A->my+1; j++) {
		for (i=1; i<A->mx+1; i++) {
			Aint ii = i + j*A->mx_mesh;
			Aint phase;
			
			phase = A->points[ A->cells[ii].p ].phase;
			fprintf(fp, "      %d \n", phase	);			
		}
	}
	fprintf(fp, "      </DataArray>\n");
	
	fprintf(fp, "    </CellData>\n");
	
	fprintf(fp, "    <PointData>\n");
	fprintf(fp, "    </PointData>\n");
	
	fprintf(fp, "    </Piece>\n");
  fprintf(fp, "  </RectilinearGrid>\n");
	fprintf(fp, "</VTKFile>\n");
	
	fclose( fp );
}

void AVD2dCellsView_appendedVTR(AVD2d A,const char name[])
{
	FILE*	fp;
	Aint  c,i,j,k;
	int offset,L;
	
	if ((fp = fopen ( name, "w")) == NULL)  {
		exit(1);
	}
	
	fprintf(fp, "<?xml version=\"1.0\"?>\n");
#ifdef WORDSIZE_BIGENDIAN
	fprintf(fp, "<VTKFile type=\"RectilinearGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
#else
	fprintf(fp, "<VTKFile type=\"RectilinearGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
#endif
  fprintf(fp, "  <RectilinearGrid WholeExtent=\"%d %d %d %d %d %d\" >\n", 0,A->mx, 0,A->my, 0,1);
	fprintf(fp, "    <Piece Extent=\"%d %d %d %d %d %d\" >\n", 0,A->mx, 0,A->my, 0,1);

	offset = 0;
	
	fprintf(fp, "    <Coordinates>\n");
	/* X */
	fprintf(fp, "      <DataArray type=\"Float32\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%d\"/>\n",offset);
	offset = offset + sizeof(int) + sizeof(float)*(A->mx+1);
	
	fprintf(fp, "      <DataArray type=\"Float32\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%d\"/>\n",offset);
	offset = offset + sizeof(int) + sizeof(float)*(A->my+1);

	fprintf(fp, "      <DataArray type=\"Float32\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%d\"/>\n",offset);
	offset = offset + sizeof(int) + sizeof(float)*(1);

	fprintf(fp, "    </Coordinates>\n");
	
	fprintf(fp, "    <CellData>\n");
	// pid
	fprintf(fp, "      <DataArray type=\"Int32\" Name=\"pid\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%d\"/>\n",offset);
	offset = offset + sizeof(int) + sizeof(int)*(A->mx * A->my);

	// phase
	fprintf(fp, "      <DataArray type=\"Int32\" Name=\"phase\" NumberOfComponents=\"1\" format=\"appended\" offset=\"%d\"/>\n",offset);
	offset = offset + sizeof(int) + sizeof(int)*(A->mx * A->my);
	
	fprintf(fp, "    </CellData>\n");
	
	fprintf(fp, "    <PointData>\n");
	fprintf(fp, "    </PointData>\n");
	
	fprintf(fp, "    </Piece>\n");
  fprintf(fp, "  </RectilinearGrid>\n");


	fprintf(fp,"  <AppendedData encoding=\"raw\">\n");	
	fprintf(fp,"_");	

	// X
	L = sizeof(float)*(A->mx+1);
	fwrite(&L, sizeof(int), 1, fp);
	for( i=0; i<A->mx+1; i++ ) {
		float val = (float)(A->x0 + (i)*A->dx);
		fwrite(&val,sizeof(float),1,fp);
	}

	// Y
	L = sizeof(float)*(A->my+1);
	fwrite(&L, sizeof(int), 1, fp);
	for( i=0; i<A->my+1; i++ ) {
		float val = (float)(A->y0 + (i)*A->dy);
		fwrite(&val,sizeof(float),1,fp);
	}

	// Z
	L = sizeof(float)*(1);
	fwrite(&L, sizeof(int), 1, fp);
	for( i=0; i<1; i++ ) {
		float val = 0.0;
		fwrite(&val,sizeof(float),1,fp);
	}
	
	// pid
	L = sizeof(int)*(A->my*A->mx);
	fwrite(&L, sizeof(int), 1, fp);
	for (j=1; j<A->my+1; j++) {
		for (i=1; i<A->mx+1; i++) {
			Aint ii = i + j*A->mx_mesh;
			int val = A->cells[ii].p;
			fwrite(&val,sizeof(int),1,fp);			
		}
	}

	// phase
	L = sizeof(int)*(A->my*A->mx);
	fwrite(&L, sizeof(int), 1, fp);
	for (j=1; j<A->my+1; j++) {
		for (i=1; i<A->mx+1; i++) {
			Aint ii = i + j*A->mx_mesh;
			Aint phase;
			
			phase = A->points[ A->cells[ii].p ].phase;
			fwrite(&phase,sizeof(int),1,fp);			
		}
	}
	fprintf(fp,"\n  </AppendedData>\n");
	
	
	fprintf(fp, "</VTKFile>\n");
	
	fclose( fp );
}

void AVD2dCellsView_VTS(AVD2d A,const char name[])
{
	FILE*	fp;
	Aint  c,i,j;
	
	if ((fp = fopen ( name, "w")) == NULL)  {
		exit(1);
	}
	
	fprintf(fp, "<?xml version=\"1.0\"?>\n");
#ifdef WORDSIZE_BIGENDIAN
	fprintf(fp, "<VTKFile type=\"StructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
#else
	fprintf(fp, "<VTKFile type=\"StructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
#endif
	fprintf(fp, "  <StructuredGrid WholeExtent=\"%d %d %d %d %d %d\" >\n", 0,A->mx, 0,A->my, 0,0);
	fprintf(fp, "    <Piece Extent=\"%d %d %d %d %d %d\" >\n", 0,A->mx, 0,A->my, 0,0);
	
	fprintf(fp, "    <Points>\n");
	fprintf(fp, "      <DataArray Name=\"coords\" type=\"Float32\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	for( j=0; j<A->my+1; j++ ) {
		for( i=0; i<A->mx+1; i++ ) {
			double xx,yy,zz;
			xx = A->x0 + (i)*A->dx;
			yy = A->y0 + (j)*A->dy;
			zz = 0.0;
			fprintf(fp, "      %1.4e %1.4e %1.4e \n", xx,yy,zz );
		}
	}
	fprintf(fp, "      </DataArray>\n");
	fprintf(fp, "    </Points>\n");
	
	fprintf(fp, "    <CellData>\n");
	
	// pid
	fprintf(fp, "      <DataArray type=\"Int32\" Name=\"pid\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	for (j=1; j<A->my+1; j++) {
		for (i=1; i<A->mx+1; i++) {
			Aint ii = i + j*A->mx_mesh;
			
			fprintf(fp, "      %d \n", A->cells[ii].p	);			
		}
	}
	fprintf(fp, "      </DataArray>\n");
	
	// phase
	fprintf(fp, "      <DataArray type=\"Int32\" Name=\"phase\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	for (j=1; j<A->my+1; j++) {
		for (i=1; i<A->mx+1; i++) {
			Aint ii = i + j*A->mx_mesh;
			Aint phase;
			
			phase = A->points[ A->cells[ii].p ].phase;
			fprintf(fp, "      %d \n", phase	);			
		}
	}
	fprintf(fp, "      </DataArray>\n");
	
	fprintf(fp, "    </CellData>\n");
	
	fprintf(fp, "    <PointData>\n");
	fprintf(fp, "    </PointData>\n");
	
	fprintf(fp, "    </Piece>\n");
  fprintf(fp, "  </StructuredGrid>\n");
	fprintf(fp, "</VTKFile>\n");
	
	fclose( fp );
}

void AVD2dCellsView_VTU(AVD2d A,const char name[])
{
	FILE*	fp;
	Aint  c,i,j;
	Aint ncells,nvertices;
	
	if ((fp = fopen ( name, "w")) == NULL)  {
		exit(1);
	}
	
	ncells    = A->mx * A->my;
	nvertices = (A->mx+1) * (A->my+1);
	
	fprintf(fp, "<?xml version=\"1.0\"?>\n");
#ifdef WORDSIZE_BIGENDIAN
	fprintf(fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
#else
	fprintf(fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
#endif
	fprintf(fp, "  <UnstructuredGrid>\n");
	fprintf(fp, "    <Piece NumberOfPoints=\"%d\" NumberOfCells=\"%d\" >\n",nvertices,ncells);
	
	
	fprintf(fp, "    <Points>\n");
	fprintf(fp, "      <DataArray type=\"Float32\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	for (j=0; j<A->my+1; j++) {
		for (i=0; i<A->mx+1; i++) {
			fprintf(fp, "      %1.4e %1.4e %1.4e \n", A->x0+i*A->dx, A->y0+j*A->dy, 0.0 );
		}
	}
	fprintf(fp, "      </DataArray>\n");
	fprintf(fp, "    </Points>\n");	

	
	fprintf(fp, "    <CellData>\n");
	// pid
	fprintf(fp, "      <DataArray type=\"Int32\" Name=\"pid\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	for (j=1; j<A->my+1; j++) {
		for (i=1; i<A->mx+1; i++) {
			Aint ii = i + j*A->mx_mesh;
			
			fprintf(fp, "      %d \n", A->cells[ii].p	);			
		}
	}
	fprintf(fp, "      </DataArray>\n");
	
	// phase
	fprintf(fp, "      <DataArray type=\"Int32\" Name=\"phase\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	for (j=1; j<A->my+1; j++) {
		for (i=1; i<A->mx+1; i++) {
			Aint ii = i + j*A->mx_mesh;
			Aint phase;
			
			phase = A->points[ A->cells[ii].p ].phase;
			fprintf(fp, "      %d \n", phase	);			
		}
	}
	fprintf(fp, "      </DataArray>\n");
	
	fprintf(fp, "    </CellData>\n");
	
	fprintf(fp, "    <PointData>\n");
	fprintf(fp, "    </PointData>\n");
	
	
	/* <BEGIN> UNSTRUCTURED GRID DATA */
	fprintf(fp, "    <Cells>\n");
	// connectivity //
	fprintf(fp, "      <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");
	for (j=0; j<A->my; j++) {
		for (i=0; i<A->mx; i++) {
			Aint i0,nid[4];

			i0 = i + j * A->mx + j;
			
			nid[0] = i0;
			nid[1] = i0 + A->mx+1;
			nid[2] = i0 + A->mx+1 + 1;
			nid[3] = i0 + 1;

			fprintf(fp,"      %d %d %d %d \n", 
							nid[0],nid[3],nid[2],nid[1] );
		}
	}
	fprintf(fp, "      </DataArray>\n");
	
	// offsets //
	fprintf(fp, "      <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	fprintf(fp,"      ");		
	for (c=0; c<ncells; c++) {
		fprintf(fp,"%d ", 4*(c+1));		
	}
	fprintf(fp,"\n");		
	fprintf(fp, "      </DataArray>\n");
	
	// types //
	fprintf(fp, "      <DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\">\n");
	fprintf(fp,"      ");		
	for (c=0; c<ncells; c++) {
		fprintf(fp,"%d ", 9); /* VTK_QUAD */		
	}
	fprintf(fp,"\n");		
	fprintf(fp, "      </DataArray>\n");
	
	fprintf(fp, "    </Cells>\n");
	/* <END> UNSTRUCTURED GRID DATA */
	
	fprintf(fp, "    </Piece>\n");
  fprintf(fp, "  </UnstructuredGrid>\n");
	fprintf(fp, "</VTKFile>\n");
	
	fclose( fp );
}

