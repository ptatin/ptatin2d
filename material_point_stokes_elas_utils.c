

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"

#include "petscksp.h"
#include "petscdm.h"

#include "pTatin2d.h"
#include "swarm_fields.h"
#include "data_exchanger.h"
#include "MPntStd_def.h"
#include "MPntPStokes_def.h"
#include "MPntPStokesElas_def.h"
#include "material_point_stokes_elas_utils.h"

#undef __FUNCT__
#define __FUNCT__ "SwarmView_MPntPStokes_el_VTKascii"
PetscErrorCode SwarmView_MPntPStokesel__VTKascii(DataBucket db,const char name[])
{
    PetscMPIInt rank;
	FILE *vtk_fp;
	PetscInt k;
	int npoints;
	PetscLogDouble t0,t1;
	DataField PField_std;
	DataField PField_stokes_el;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	ierr = PetscGetTime(&t0);CHKERRQ(ierr);
	
	if ((vtk_fp = fopen ( name, "w")) == NULL)  {
		SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
	}
	
	fprintf( vtk_fp, "<?xml version=\"1.0\"?>\n");
	
#ifdef WORDSIZE_BIGENDIAN
	fprintf( vtk_fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
#else
	fprintf( vtk_fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
#endif
	fprintf( vtk_fp, "\t<UnstructuredGrid>\n" );
	
	DataBucketGetSizes(db,&npoints,PETSC_NULL,PETSC_NULL);
	fprintf( vtk_fp, "\t\t<Piece NumberOfPoints=\"%d\" NumberOfCells=\"%d\">\n",npoints,npoints );
	
	
	fprintf( vtk_fp, "\n");
	fprintf( vtk_fp, "\t\t\t<Cells>\n");
	
	// connectivity //
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");
	fprintf( vtk_fp, "\t\t\t\t");
	for(k=0;k<npoints;k++) {
		fprintf( vtk_fp,"%d ",k);
	}
	fprintf( vtk_fp, "\n");
	fprintf( vtk_fp, "\t\t\t\t</DataArray>\n");	
	
	// offsets //
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	fprintf( vtk_fp, "\t\t\t\t");
	for(k=0;k<npoints;k++) {
		fprintf( vtk_fp,"%d ",k+1);
	}
	fprintf( vtk_fp, "\n");
	fprintf( vtk_fp, "\t\t\t\t</DataArray>\n");
	
	// types //
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\">\n");
	fprintf( vtk_fp, "\t\t\t\t");
	for(k=0;k<npoints;k++) {
		fprintf( vtk_fp,"1 "); // VTK_VERTEX //
	}
	fprintf( vtk_fp, "\n");
	fprintf( vtk_fp, "\t\t\t\t</DataArray>\n");
	
	fprintf( vtk_fp, "\t\t\t</Cells>\n");
	
	fprintf( vtk_fp, "\n");
	fprintf( vtk_fp, "\t\t\t<CellData>\n");
	fprintf( vtk_fp, "\t\t\t</CellData>\n");
	fprintf( vtk_fp, "\n");
	
	
	DataBucketGetDataFieldByName(db, MPntStd_classname ,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess( PField_std,sizeof(MPntStd));

	DataBucketGetDataFieldByName(db, MPntPStokesElas_classname ,&PField_stokes_el);
	DataFieldGetAccess(PField_stokes_el);
	DataFieldVerifyAccess( PField_stokes_el,sizeof(MPntPStokesElas));
	
	
	/* point coordinates */
	fprintf( vtk_fp, "\t\t\t<Points>\n");
	
	/* copy coordinates */
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	for(k=0;k<npoints;k++) {
		MPntStd *marker;
		double *coords;
		
		DataFieldAccessPoint(PField_std,k,(void**)&marker);
		
		
		/* extract coords from your data type */
		//coords = elasticParticle->pos;
		MPntStdGetField_global_coord( marker,&coords );
		
		fprintf( vtk_fp,"\t\t\t\t\t%lf %lf %lf \n",coords[0],coords[1],0.0);
	}
	fprintf( vtk_fp, "\t\t\t\t</DataArray>\n");
	
	fprintf( vtk_fp, "\t\t\t</Points>\n");
	fprintf( vtk_fp, "\n");
	
	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokes_el);
	
	
	
	/* point data BEGIN */
	fprintf( vtk_fp, "\t\t\t<PointData>\n");
	
	/* auto generated shit goes here */
	{
		MPntStd     *marker_std    = PField_std->data; /* should write a function to do this */
		MPntPStokesElas *marker_stokes_el = PField_stokes_el->data; /* should write a function to do this */
		
		MPntStdVTKWriteAsciiAllFields(vtk_fp,(const int)npoints,(const MPntStd*)marker_std );
		MPntPStokesVTKWriteAsciiAllFields(vtk_fp,(const int)npoints,(const MPntPStokesElas*)marker_stokes_el);
	}
	fprintf( vtk_fp, "\t\t\t</PointData>\n");
	fprintf( vtk_fp, "\n");
	/* point data END */
	
	
	fprintf( vtk_fp, "\t\t</Piece>\n");
	fprintf( vtk_fp, "\t</UnstructuredGrid>\n");
	
	fprintf( vtk_fp, "</VTKFile>\n");
	
	
	if( vtk_fp!= NULL ) {
		fclose( vtk_fp );
		vtk_fp = NULL;
	}
	
	ierr = PetscGetTime(&t1);CHKERRQ(ierr);
#ifdef PROFILE_TIMING
	PetscPrintf(PETSC_COMM_WORLD,"VTKWriter(%s): Time %1.4e sec\n",__FUNCT__,t1-t0);
#endif
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SwarmView_MPntPStokes_el_VTKappended_binary"
PetscErrorCode SwarmView_MPntPStokes_el_VTKappended_binary(DataBucket db,const char name[])
{
	PetscMPIInt rank;
	FILE *vtk_fp;
	PetscInt k;
	int npoints;
	PetscLogDouble t0,t1;
	DataField PField_std;
	DataField PField_stokes_el;
	int byte_offset,length;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	ierr = PetscGetTime(&t0);CHKERRQ(ierr);
	
	if ((vtk_fp = fopen ( name, "w")) == NULL)  {
		SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
	}
	
	fprintf( vtk_fp, "<?xml version=\"1.0\"?>\n");
	
#ifdef WORDSIZE_BIGENDIAN
	fprintf( vtk_fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
#else
	fprintf( vtk_fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
#endif
	fprintf( vtk_fp, "\t<UnstructuredGrid>\n" );
	
	DataBucketGetSizes(db,&npoints,PETSC_NULL,PETSC_NULL);
	fprintf( vtk_fp, "\t\t<Piece NumberOfPoints=\"%d\" NumberOfCells=\"%d\">\n",npoints,npoints );
	
	
	fprintf( vtk_fp, "\n");
	fprintf( vtk_fp, "\t\t\t<Cells>\n");
	
	byte_offset = 0;
	
	// connectivity //
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Int32\" Name=\"connectivity\" format=\"appended\" offset=\"%d\" />\n",byte_offset);
  byte_offset = byte_offset + sizeof(int) + npoints * sizeof(int);
	
	// offsets //
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Int32\" Name=\"offsets\" format=\"appended\" offset=\"%d\" />\n",byte_offset);
  byte_offset = byte_offset + sizeof(int) + npoints * sizeof(int);
	
	// types //
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"UInt8\" Name=\"types\" format=\"appended\" offset=\"%d\" />\n",byte_offset);
  byte_offset = byte_offset + sizeof(int) + npoints * sizeof(unsigned char);
	
	fprintf( vtk_fp, "\t\t\t</Cells>\n");
	
	fprintf( vtk_fp, "\n");
	fprintf( vtk_fp, "\t\t\t<CellData>\n");
	fprintf( vtk_fp, "\t\t\t</CellData>\n");
	fprintf( vtk_fp, "\n");

	
	DataBucketGetDataFieldByName(db, MPntStd_classname ,&PField_std);
	DataBucketGetDataFieldByName(db, MPntPStokesElas_classname ,&PField_stokes_el);
	
	
	/* point coordinates */
	fprintf( vtk_fp, "\t\t\t<Points>\n");
	
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\" format=\"appended\" offset=\"%d\" />\n",byte_offset);
  byte_offset = byte_offset + sizeof(int) + npoints * 3 * sizeof(double);
	
	fprintf( vtk_fp, "\t\t\t</Points>\n");
	fprintf( vtk_fp, "\n");

	/* point data BEGIN */
	fprintf( vtk_fp, "\t\t\t<PointData>\n");
	
	/* auto generated shit goes here */
	{
		MPntStd     *marker_std    = PField_std->data; /* should write a function to do this */
		MPntPStokes *marker_stokes_el = PField_stokes_el->data; /* should write a function to do this */
		
		MPntStdVTKWriteBinaryAppendedHeaderAllFields(vtk_fp,&byte_offset,(const int)npoints,(const MPntStd*)marker_std );
		MPntPStokesVTKWriteBinaryAppendedHeaderAllFields(vtk_fp,&byte_offset,(const int)npoints,(const MPntPStokes*)marker_stokes_el);
	}
	fprintf( vtk_fp, "\t\t\t</PointData>\n");
	fprintf( vtk_fp, "\n");
	/* point data END */
	
	
	fprintf( vtk_fp, "\t\t</Piece>\n");
	fprintf( vtk_fp, "\t</UnstructuredGrid>\n");
	
	/* WRITE APPENDED DATA HERE */
	fprintf( vtk_fp,"\t<AppendedData encoding=\"raw\">\n");
	fprintf( vtk_fp,"_");
	
	/* connectivity, offsets, types, coords */
	////////////////////////////////////////////////////////
	/* write connectivity */
	length = sizeof(int)*npoints;
	fwrite( &length,sizeof(int),1,vtk_fp);
	for (k=0; k<npoints; k++) {
		int idx = k;
		fwrite( &idx, sizeof(int),1, vtk_fp );
	}
	////////////////////////////////////////////////////////
	/* write offset */
	length = sizeof(int)*npoints;
	fwrite( &length,sizeof(int),1,vtk_fp);
	for (k=0; k<npoints; k++) {
		int idx = k+1;
		fwrite( &idx, sizeof(int),1, vtk_fp );
	}
	////////////////////////////////////////////////////////
	/* write types */
	length = sizeof(unsigned char)*npoints;
	fwrite( &length,sizeof(int),1,vtk_fp);
	for (k=0; k<npoints; k++) {
		unsigned char idx = 1; /* VTK_VERTEX */
		fwrite( &idx, sizeof(unsigned char),1, vtk_fp );
	}
	////////////////////////////////////////////////////////
	/* write coordinates */
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	length = sizeof(double)*npoints*3;
	fwrite( &length,sizeof(int),1,vtk_fp);
	for (k=0; k<npoints; k++) {
		MPntStd *marker;
		double  *coor;
		double  coords_k[] = {0.0, 0.0, 0.0};
		
		DataFieldAccessPoint(PField_std,k,(void**)&marker);
		MPntStdGetField_global_coord(marker,&coor);
		coords_k[0] = coor[0];
		coords_k[1] = coor[1];
		
		fwrite( coords_k, sizeof(double), 3, vtk_fp );
	}
	DataFieldRestoreAccess(PField_std);
	
	/* auto generated shit for the marker data goes here */
	{
		MPntStd     *markers_std    = PField_std->data;
		MPntPStokes *markers_stokes_el = PField_stokes_el->data;

		MPntStdVTKWriteBinaryAppendedDataAllFields(vtk_fp,npoints,markers_std);
		MPntPStokesVTKWriteBinaryAppendedDataAllFields(vtk_fp,npoints,markers_stokes_el);
	}
	
	fprintf( vtk_fp,"\n\t</AppendedData>\n");
	
	
	fprintf( vtk_fp, "</VTKFile>\n");
	
	
	if( vtk_fp!= NULL ) {
		fclose( vtk_fp );
		vtk_fp = NULL;
	}
	
	ierr = PetscGetTime(&t1);CHKERRQ(ierr);
#ifdef PROFILE_TIMING
	PetscPrintf(PETSC_COMM_WORLD,"VTKWriter(%s): Time %1.4e sec\n",__FUNCT__,t1-t0);
#endif
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "__SwarmView_MPntPStokes_el_PVTU"
PetscErrorCode __SwarmView_MPntPStokes_el_PVTU(const char prefix[],const char name[])
{
	PetscMPIInt nproc,rank;
	FILE *vtk_fp;
	PetscInt i;
	char *sourcename;
	
	PetscFunctionBegin;
	
	if ((vtk_fp = fopen ( name, "w")) == NULL)  {
		SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
	}
	
	/* (VTK) generate pvts header */
	fprintf( vtk_fp, "<?xml version=\"1.0\"?>\n");
	
#ifdef WORDSIZE_BIGENDIAN
	fprintf( vtk_fp, "<VTKFile type=\"PUnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
#else
	fprintf( vtk_fp, "<VTKFile type=\"PUnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
#endif
	
	/* define size of the nodal mesh based on the cell DM */
	fprintf( vtk_fp, "  <PUnstructuredGrid GhostLevel=\"0\">\n" ); /* note overlap = 0 */
	
	
	/* DUMP THE CELL REFERENCES */
	fprintf( vtk_fp, "    <PCellData>\n");
	fprintf( vtk_fp, "    </PCellData>\n");
	
	
	///////////////
	fprintf( vtk_fp, "    <PPoints>\n");
	fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\"/>\n");
	fprintf( vtk_fp, "    </PPoints>\n");
	///////////////
	
	///////////////
  fprintf(vtk_fp, "    <PPointData>\n");
	MPntStdPVTUWriteAllPPointDataFields(vtk_fp);
	MPntPStokesElasPVTUWriteAllPPointDataFields(vtk_fp);
  fprintf(vtk_fp, "    </PPointData>\n");
	///////////////
	
	
	/* write out the parallel information */
	MPI_Comm_size(PETSC_COMM_WORLD,&nproc);
	for (i=0; i<nproc; i++) {
		asprintf( &sourcename, "%s-subdomain%1.5d.vtu", prefix, i );
		fprintf( vtk_fp, "    <Piece Source=\"%s\"/>\n",sourcename);
		free(sourcename);
	}
	
	/* close the file */
	fprintf( vtk_fp, "  </PUnstructuredGrid>\n");
	fprintf( vtk_fp, "</VTKFile>\n");
	
	if(vtk_fp!=NULL){
		fclose( vtk_fp );
		vtk_fp = NULL;
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SwarmOutputParaView_MPntPStokes_el"
PetscErrorCode SwarmOutputParaView_MPntPStokes_el(DataBucket db,const char path[],const char prefix[])
{ 
	char *vtkfilename,*filename;
	PetscMPIInt rank;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ierr = pTatinGenerateParallelVTKName(prefix,"vtu",&vtkfilename);CHKERRQ(ierr);
	if (path) {
		asprintf(&filename,"%s/%s",path,vtkfilename);
	} else {
		asprintf(&filename,"./%s",vtkfilename);
	}

//#ifdef __VTK_ASCII__
//	ierr = SwarmView_MPntPStokes_VTKascii( db,filename );CHKERRQ(ierr);
//#endif
//#ifndef __VTK_ASCII__
	ierr = SwarmView_MPntPStokes_el_VTKappended_binary(db,filename);CHKERRQ(ierr);
//#endif
	free(filename);
	free(vtkfilename);
	
	
	ierr = pTatinGenerateVTKName(prefix,"pvtu",&vtkfilename);CHKERRQ(ierr);
	if (path) {
		asprintf(&filename,"%s/%s",path,vtkfilename);
	} else {
		asprintf(&filename,"./%s",vtkfilename);
	}
	
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	if (rank==0) {
		ierr = __SwarmView_MPntPStokes_el_PVTU( prefix, filename );CHKERRQ(ierr);
	}
	free(filename);
	free(vtkfilename);
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "_SwarmUpdateGaussPropertiesLocalL2ProjectionQ1_MPntPStokes_elas"
PetscErrorCode _SwarmUpdateGaussPropertiesLocalL2ProjectionQ1_MPntPStokes_elas(
																																				DM clone,Vec properties_A1,Vec properties_A2,Vec properties_S1, Vec properties_S2, Vec properties_S3,Vec properties_B,
																																				const int npoints,MPntStd mp_std[],MPntPStokes mp_stokes[],MPntPStokesElas mp_stokes_el[],MaterialCoeffAvergeType average_type, QuadratureStokes Q)
{
	PetscScalar Ni_p[Q2_NODES_PER_EL_2D];
	PetscScalar NiQ1_p[4];
	PetscScalar Ae1[Q2_NODES_PER_EL_2D], Ae2[Q2_NODES_PER_EL_2D], Se1[Q2_NODES_PER_EL_2D],Se2[Q2_NODES_PER_EL_2D], Se3[Q2_NODES_PER_EL_2D],Be[Q2_NODES_PER_EL_2D];
	PetscInt el_lidx[U_BASIS_FUNCTIONS];
	Vec Lproperties_A1, Lproperties_A2, Lproperties_S1, Lproperties_S2, Lproperties_S3,Lproperties_B;
	PetscScalar *LA_properties_A1, *LA_properties_A2,*LA_properties_S1,*LA_properties_S2, *LA_properties_S3, *LA_properties_B;
	PetscLogDouble t0,t1;
	PetscInt p,i;
	PetscInt nel,nen,e;
	const PetscInt *elnidx;
	
	PetscInt ngp;
	PetscScalar *xi_mp;
	PetscScalar NIu[MAX_QUAD_PNTS][U_BASIS_FUNCTIONS];
	GaussPointCoefficientsStokes *gausspoints;
	PetscErrorCode ierr;
	
	
	PetscFunctionBegin;
	
    ierr = DMGetLocalVector(clone,&Lproperties_A1);CHKERRQ(ierr);		ierr = VecZeroEntries(Lproperties_A1);CHKERRQ(ierr);
    ierr = DMGetLocalVector(clone,&Lproperties_A2);CHKERRQ(ierr);		ierr = VecZeroEntries(Lproperties_A2);CHKERRQ(ierr);
	
	ierr = DMGetLocalVector(clone,&Lproperties_S1);CHKERRQ(ierr);		ierr = VecZeroEntries(Lproperties_S1);CHKERRQ(ierr);
	ierr = DMGetLocalVector(clone,&Lproperties_S2);CHKERRQ(ierr);		ierr = VecZeroEntries(Lproperties_S2);CHKERRQ(ierr);
	ierr = DMGetLocalVector(clone,&Lproperties_S3);CHKERRQ(ierr);		ierr = VecZeroEntries(Lproperties_S3);CHKERRQ(ierr);
    
    ierr = DMGetLocalVector(clone,&Lproperties_B);CHKERRQ(ierr);		ierr = VecZeroEntries(Lproperties_B);CHKERRQ(ierr);
	
    ierr = VecGetArray(Lproperties_A1,&LA_properties_A1);CHKERRQ(ierr);
    ierr = VecGetArray(Lproperties_A2,&LA_properties_A2);CHKERRQ(ierr);

    
    ierr = VecGetArray(Lproperties_S1,&LA_properties_S1);CHKERRQ(ierr);
	ierr = VecGetArray(Lproperties_S2,&LA_properties_S2);CHKERRQ(ierr);
	ierr = VecGetArray(Lproperties_S3, &LA_properties_S3);CHKERRQ(ierr);
    
    ierr = VecGetArray(Lproperties_B, &LA_properties_B);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(clone,&nel,&nen,&elnidx);CHKERRQ(ierr);
	
	PetscGetTime(&t0);
	for (p=0; p<npoints; p++) {
		double *xi_p  = &mp_std[p].xi[0];
        double eta_p  = mp_stokes[p].eta;
        double rho_p  = mp_stokes[p].rho;
		double sxx_p  = mp_stokes_el[p].sxx_r;
		double syy_p  = mp_stokes_el[p].syy_r;
        double sxy_p  = mp_stokes_el[p].sxy_r;
        
        
		ierr = PetscMemzero(Ae1,sizeof(PetscScalar)*Q2_NODES_PER_EL_2D);CHKERRQ(ierr);
		ierr = PetscMemzero(Ae2,sizeof(PetscScalar)*Q2_NODES_PER_EL_2D);CHKERRQ(ierr);
        
        ierr = PetscMemzero(Se1, sizeof(PetscScalar)*Q2_NODES_PER_EL_2D);CHKERRQ(ierr);
        ierr = PetscMemzero(Se2, sizeof(PetscScalar)*Q2_NODES_PER_EL_2D);CHKERRQ(ierr);
		ierr = PetscMemzero(Se3, sizeof(PetscScalar)*Q2_NODES_PER_EL_2D);CHKERRQ(ierr);
        
        ierr = PetscMemzero(Be, sizeof(PetscScalar)*Q2_NODES_PER_EL_2D);CHKERRQ(ierr);
		
		PTatinConstructNI_Q1_2D(xi_p,NiQ1_p);
		
		Ni_p[0] = NiQ1_p[0];
		Ni_p[2] = NiQ1_p[1];
		Ni_p[6] = NiQ1_p[2];
		Ni_p[8] = NiQ1_p[3];
		
		Ni_p[1] = Ni_p[7] = 1.0;
		Ni_p[3] = Ni_p[4] = Ni_p[5] = 1.0;
		
        switch (average_type) {
            case AVERAGE_TYPE_ARITHMETIC:
                for (i=0; i<Q2_NODES_PER_EL_2D; i++) {
                    Ae1[i] = Ni_p[i] * eta_p;
                    Ae2[i] = Ni_p[i] * rho_p;
                    Se1[i] = Ni_p[i] * sxx_p;
                    Se2[i] = Ni_p[i] * syy_p;
                    Se3[i]  = Ni_p[i]* sxy_p;
                    Be[i]  = Ni_p[i];
                }
                break;

            case AVERAGE_TYPE_HARMONIC:
                for (i=0; i<Q2_NODES_PER_EL_2D; i++) {
                    Ae1[i] = Ni_p[i] * (1.0/eta_p);
                    Ae2[i] = Ni_p[i] * rho_p;
                    Se1[i] = Ni_p[i] * (1.0/sxx_p);
                    Se2[i] = Ni_p[i] * (1.0/syy_p);
                    Se3[i] = Ni_p[i] * (1.0/sxy_p);
                    Be[i]  = Ni_p[i];
                }
                break;

            default:
                SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Average type must be ARITHMETIC or HARMONIC");
                break;
        }
		
		/* sum into local vectors */
		e = mp_std[p].wil;
		ierr = Q2GetElementLocalIndicesDOF(el_lidx,1,(PetscInt*)&elnidx[nen*e]);CHKERRQ(ierr);
        
        ierr = DMDASetValuesLocalStencil_AddValues_DOF(LA_properties_A1, 1, el_lidx,Ae1);CHKERRQ(ierr);
        ierr = DMDASetValuesLocalStencil_AddValues_DOF(LA_properties_A2, 1, el_lidx,Ae2);CHKERRQ(ierr);

		ierr = DMDASetValuesLocalStencil_AddValues_DOF(LA_properties_S1, 1, el_lidx,Se1);CHKERRQ(ierr);
		ierr = DMDASetValuesLocalStencil_AddValues_DOF(LA_properties_S2, 1, el_lidx,Se2);CHKERRQ(ierr);
        ierr = DMDASetValuesLocalStencil_AddValues_DOF(LA_properties_S3, 1, el_lidx,Se3);CHKERRQ(ierr);
		
        ierr = DMDASetValuesLocalStencil_AddValues_DOF(LA_properties_B,  1, el_lidx,Be);CHKERRQ(ierr);
		
	}
	PetscGetTime(&t1);
//	PetscPrintf(PETSC_COMM_WORLD,"  [ L2 projectionQ1 (summation): %1.4lf ]\n",t1-t0);
	
    ierr = VecRestoreArray(Lproperties_B,&LA_properties_B);CHKERRQ(ierr);
    ierr = VecRestoreArray(Lproperties_A2,&LA_properties_A2);CHKERRQ(ierr);
    ierr = VecRestoreArray(Lproperties_A1,&LA_properties_A1);CHKERRQ(ierr);
    ierr = VecRestoreArray(Lproperties_S2,&LA_properties_S2);CHKERRQ(ierr);
    ierr = VecRestoreArray(Lproperties_S1,&LA_properties_S1);CHKERRQ(ierr);
    ierr = VecRestoreArray(Lproperties_S3,&LA_properties_S3);CHKERRQ(ierr);
	
	/* scatter to quadrature points */
	ierr = DMLocalToGlobalBegin(clone,Lproperties_A1,ADD_VALUES,properties_A1);CHKERRQ(ierr);
	ierr = DMLocalToGlobalEnd(  clone,Lproperties_A1,ADD_VALUES,properties_A1);CHKERRQ(ierr);
	
	ierr = DMLocalToGlobalBegin(clone,Lproperties_A2,ADD_VALUES,properties_A2);CHKERRQ(ierr);
	ierr = DMLocalToGlobalEnd(  clone,Lproperties_A2,ADD_VALUES,properties_A2);CHKERRQ(ierr);
    
    ierr = DMLocalToGlobalBegin(clone,Lproperties_S1,ADD_VALUES,properties_S1);CHKERRQ(ierr);
    ierr = DMLocalToGlobalEnd(  clone,Lproperties_S1,ADD_VALUES,properties_S1);CHKERRQ(ierr);
    
    ierr = DMLocalToGlobalBegin(clone,Lproperties_S2,ADD_VALUES,properties_S2);CHKERRQ(ierr);
    ierr = DMLocalToGlobalEnd(  clone,Lproperties_S2,ADD_VALUES,properties_S2);CHKERRQ(ierr);
	
	ierr = DMLocalToGlobalBegin(clone,Lproperties_S3,ADD_VALUES,properties_S3);CHKERRQ(ierr);
	ierr = DMLocalToGlobalEnd(  clone,Lproperties_S3,ADD_VALUES,properties_S3);CHKERRQ(ierr);
    
    ierr = DMLocalToGlobalBegin(clone,Lproperties_B,ADD_VALUES,properties_B);CHKERRQ(ierr);
    ierr = DMLocalToGlobalEnd(  clone,Lproperties_B,ADD_VALUES,properties_B);CHKERRQ(ierr);
	
	/* scale */
    ierr = VecPointwiseDivide( properties_A1, properties_A1, properties_B );CHKERRQ(ierr);
    ierr = VecPointwiseDivide( properties_A2, properties_A2, properties_B );CHKERRQ(ierr);
	ierr = VecPointwiseDivide( properties_S1, properties_S1, properties_B );CHKERRQ(ierr);
	ierr = VecPointwiseDivide( properties_S2, properties_S2, properties_B );CHKERRQ(ierr);
    ierr = VecPointwiseDivide( properties_S3, properties_S3, properties_B );CHKERRQ(ierr);

	/*
     Two choices are possible for harmonic averaging:
     We have projected 1/eta to the nodes, we can either
     a) take reciporcal of the nodal 1/eta and interpolate the result to the quad points, or
     b) interpolate 1/eta to the quad points AND THEN take the reciporcal.
     Here we use option a)
     */
    switch (average_type) {
        case AVERAGE_TYPE_HARMONIC:
            ierr = VecReciprocal(properties_A1);CHKERRQ(ierr);
            ierr = VecReciprocal(properties_S1);CHKERRQ(ierr);
            ierr = VecReciprocal(properties_S2);CHKERRQ(ierr);
            ierr = VecReciprocal(properties_S3);CHKERRQ(ierr);
            
            break;
    }
    /* ========================================= */
	
	/* scatter result back to local array and do the interpolation onto the quadrature points */
	ngp       = Q->ngp;
	xi_mp     = Q->xi;
	for (p=0; p<ngp; p++) {
		PetscScalar *xip = &xi_mp[2*p];
		
		ierr = PetscMemzero(NIu[p], sizeof(PetscScalar)*Q2_NODES_PER_EL_2D);CHKERRQ(ierr);

		PTatinConstructNI_Q1_2D(xip,NiQ1_p);
		NIu[p][0] = NiQ1_p[0];
		NIu[p][2] = NiQ1_p[1];
		NIu[p][6] = NiQ1_p[2];
		NIu[p][8] = NiQ1_p[3];
	}
	
	
	PetscGetTime(&t0);
    ierr = VecZeroEntries(Lproperties_A1);CHKERRQ(ierr);
    ierr = VecZeroEntries(Lproperties_A2);CHKERRQ(ierr);
	ierr = VecZeroEntries(Lproperties_S1);CHKERRQ(ierr);
	ierr = VecZeroEntries(Lproperties_S2);CHKERRQ(ierr);
    ierr = VecZeroEntries(Lproperties_S3);CHKERRQ(ierr);
	
	ierr = DMGlobalToLocalBegin(clone,properties_A1,INSERT_VALUES,Lproperties_A1);CHKERRQ(ierr);
	ierr = DMGlobalToLocalEnd(  clone,properties_A1,INSERT_VALUES,Lproperties_A1);CHKERRQ(ierr);
	
	ierr = DMGlobalToLocalBegin(clone,properties_A2,INSERT_VALUES,Lproperties_A2);CHKERRQ(ierr);
	ierr = DMGlobalToLocalEnd(  clone,properties_A2,INSERT_VALUES,Lproperties_A2);CHKERRQ(ierr);
    
    ierr = DMGlobalToLocalBegin(clone,properties_S1,INSERT_VALUES,Lproperties_S1);CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(  clone,properties_S1,INSERT_VALUES,Lproperties_S1);CHKERRQ(ierr);
    
    ierr = DMGlobalToLocalBegin(clone,properties_S2,INSERT_VALUES,Lproperties_S2);CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(  clone,properties_S2,INSERT_VALUES,Lproperties_S2);CHKERRQ(ierr);
    
    ierr = DMGlobalToLocalBegin(clone,properties_S3,INSERT_VALUES,Lproperties_S3);CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd(  clone,properties_S3,INSERT_VALUES,Lproperties_S3);CHKERRQ(ierr);
	PetscGetTime(&t1);
//	PetscPrintf(PETSC_COMM_WORLD,"  [ L2 projectionQ1 (scatter): %1.4lf ]\n",t1-t0);
	
	PetscGetTime(&t0);
	ierr = VecGetArray(Lproperties_A1,&LA_properties_A1);CHKERRQ(ierr);
	ierr = VecGetArray(Lproperties_A2,&LA_properties_A2);CHKERRQ(ierr);
    ierr = VecGetArray(Lproperties_S1,&LA_properties_S1);CHKERRQ(ierr);
    ierr = VecGetArray(Lproperties_S2,&LA_properties_S2);CHKERRQ(ierr);
    ierr = VecGetArray(Lproperties_S3,&LA_properties_S3);CHKERRQ(ierr);
	
	/* traverse elements and interpolate */
	for (e=0;e<nel;e++) {
		ierr = QuadratureStokesGetCell(Q,e,&gausspoints);CHKERRQ(ierr);
		
		ierr = Q2GetElementLocalIndicesDOF(el_lidx,1,(PetscInt*)&elnidx[nen*e]);CHKERRQ(ierr);
		
		ierr = DMDAGetScalarElementField_2D(Ae1,nen,(PetscInt*)&elnidx[nen*e],LA_properties_A1);CHKERRQ(ierr);
		ierr = DMDAGetScalarElementField_2D(Ae2,nen,(PetscInt*)&elnidx[nen*e],LA_properties_A2);CHKERRQ(ierr);
        
        ierr = DMDAGetScalarElementField_2D(Se1,nen,(PetscInt*)&elnidx[nen*e],LA_properties_S1);CHKERRQ(ierr);
        ierr = DMDAGetScalarElementField_2D(Se2,nen,(PetscInt*)&elnidx[nen*e],LA_properties_S2);CHKERRQ(ierr);
        ierr = DMDAGetScalarElementField_2D(Se3,nen,(PetscInt*)&elnidx[nen*e],LA_properties_S3);CHKERRQ(ierr);

		/* zero out the junk */
		Ae1[1] = Ae1[7] = 0.0;
		Ae1[3] = Ae1[4] = Ae1[5] = 0.0;

		Ae2[1] = Ae2[7] = 0.0;
		Ae2[3] = Ae2[4] = Ae2[5] = 0.0;

        
        Se1[1] = Se1[7] = 0.0;
        Se1[3] = Se1[4] = Se1[5] = 0.0;
        
        Se2[1] = Se2[7] = 0.0;
        Se2[3] = Se2[4] = Se2[5] = 0.0;
        
        Se3[1] = Se3[7] = 0.0;
        Se3[3] = Se3[4] = Se3[5] = 0.0;
		
		
		for (p=0; p<ngp; p++) {
			gausspoints[p].eta = 0.0;
			gausspoints[p].Fu[0] = 0.0;
			gausspoints[p].Fu[1] = 0.0;
            gausspoints[p].stress_old[0] = 0.0;
            gausspoints[p].stress_old[1] = 0.0;
            gausspoints[p].stress_old[2] = 0.0;
			for (i=0; i<Q2_NODES_PER_EL_2D; i++) {
				gausspoints[p].eta    += NIu[p][i] * Ae1[i];
				gausspoints[p].Fu[0]  += NIu[p][i] * Ae2[i];
                gausspoints[p].stress_old[0]  += NIu[p][i] * Se1[i];
                gausspoints[p].stress_old[1]  += NIu[p][i] * Se2[i];
                gausspoints[p].stress_old[2]  += NIu[p][i] * Se3[i];
                
			}
			
			gausspoints[p].Fu[1] = gausspoints[p].Fu[0];

			gausspoints[p].Fu[0] = gausspoints[p].Fu[0] * gausspoints[p].gravity[0];
			gausspoints[p].Fu[1] = gausspoints[p].Fu[1] * gausspoints[p].gravity[1];
			
			
		}
		
	}
	
  ierr = VecRestoreArray(Lproperties_A2,&LA_properties_A2);CHKERRQ(ierr);
  ierr = VecRestoreArray(Lproperties_A1,&LA_properties_A1);CHKERRQ(ierr);
    
    
    ierr = VecRestoreArray(Lproperties_S3,&LA_properties_S3);CHKERRQ(ierr);
    ierr = VecRestoreArray(Lproperties_S2,&LA_properties_S2);CHKERRQ(ierr);
    ierr = VecRestoreArray(Lproperties_A1,&LA_properties_S1);CHKERRQ(ierr);
	
	PetscGetTime(&t1);
//	PetscPrintf(PETSC_COMM_WORLD,"  [ L2 projectionQ1 (interpolation): %1.4lf ]\n",t1-t0);
	
	ierr = DMRestoreLocalVector(clone,&Lproperties_B);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(clone,&Lproperties_A2);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(clone,&Lproperties_A1);CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(clone,&Lproperties_S1);CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(clone,&Lproperties_S2);CHKERRQ(ierr);
    ierr = DMRestoreLocalVector(clone,&Lproperties_S3);CHKERRQ(ierr);
	
	
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SwarmUpdateGaussPropertiesLocalL2Projection_Q1_MPntPStokes_elas"
PetscErrorCode SwarmUpdateGaussPropertiesLocalL2Projection_Q1_MPntPStokes_elas(const int npoints,MPntStd mp_std[],MPntPStokes mp_stokes[],MPntPStokesElas mp_stokes_el[],MaterialCoeffAvergeType average_type,DM da,QuadratureStokes Q)
{
	PetscInt  dof;
	DM        clone;
	Vec       properties_A1, properties_A2, properties_B;
    Vec       properties_S1, properties_S2, properties_S3;
	PetscBool view;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	/* setup */
	dof = 1;
	ierr = DMDADuplicateLayout(da,dof,2,DMDA_STENCIL_BOX,&clone);CHKERRQ(ierr); /* Q2 - but we'll fake it as a Q1 with cells the same size as the Q2 guys */
	
	ierr = DMGetGlobalVector(clone,&properties_A1);CHKERRQ(ierr);  ierr = PetscObjectSetName( (PetscObject)properties_A1, "LocalL2ProjQ1_nu");CHKERRQ(ierr);
	ierr = DMGetGlobalVector(clone,&properties_A2);CHKERRQ(ierr);  ierr = PetscObjectSetName( (PetscObject)properties_A2, "LocalL2ProjQ1_rho");CHKERRQ(ierr);
    
    ierr = DMGetGlobalVector(clone,&properties_S1);CHKERRQ(ierr);  ierr = PetscObjectSetName( (PetscObject)properties_S1, "LocalL2ProjQ1_sxx");CHKERRQ(ierr);
    ierr = DMGetGlobalVector(clone,&properties_S2);CHKERRQ(ierr);  ierr = PetscObjectSetName( (PetscObject)properties_S2, "LocalL2ProjQ1_syy");CHKERRQ(ierr);
    ierr = DMGetGlobalVector(clone,&properties_S3);CHKERRQ(ierr);  ierr = PetscObjectSetName( (PetscObject)properties_S3, "LocalL2ProjQ1_sxy");CHKERRQ(ierr);
    
	ierr = DMGetGlobalVector(clone,&properties_B);CHKERRQ(ierr);
	
	ierr = VecZeroEntries(properties_A1);CHKERRQ(ierr);
	ierr = VecZeroEntries(properties_A2);CHKERRQ(ierr);
    ierr = VecZeroEntries(properties_S1);CHKERRQ(ierr);
    ierr = VecZeroEntries(properties_S2);CHKERRQ(ierr);
    ierr = VecZeroEntries(properties_S3);CHKERRQ(ierr);
	ierr = VecZeroEntries(properties_B);CHKERRQ(ierr);
	
	/* compute */
	ierr = _SwarmUpdateGaussPropertiesLocalL2ProjectionQ1_MPntPStokes_elas(
                                                                      						clone, properties_A1,properties_A2,
                                                                      properties_S1,properties_S2,properties_S3,properties_B,
                                                                      npoints, mp_std,mp_stokes,mp_stokes_el,average_type, Q );CHKERRQ(ierr);
	
	/* view */
	view = PETSC_FALSE;
	PetscOptionsGetBool(PETSC_NULL,"-view_projected_marker_fields",&view,PETSC_NULL);
	if (view) {
		PetscViewer viewer;
		
		ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, "SwarmUpdateProperties_LocalL2Proj_Stokes.vtk", &viewer);CHKERRQ(ierr);
		ierr = PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
		ierr = DMView(clone, viewer);CHKERRQ(ierr);
		ierr = VecView(properties_A1, viewer);CHKERRQ(ierr);
		ierr = VecView(properties_A2, viewer);CHKERRQ(ierr);
        
        ierr = VecView(properties_S1, viewer);CHKERRQ(ierr);
        ierr = VecView(properties_S2, viewer);CHKERRQ(ierr);
        ierr = VecView(properties_S3, viewer);CHKERRQ(ierr);
		
        ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
	}
	
	
	/* destroy */
	ierr = DMRestoreGlobalVector(clone,&properties_B);CHKERRQ(ierr);
	ierr = DMRestoreGlobalVector(clone,&properties_A2);CHKERRQ(ierr);
	ierr = DMRestoreGlobalVector(clone,&properties_A1);CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(clone,&properties_S3);CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(clone,&properties_S2);CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(clone,&properties_S1);CHKERRQ(ierr);
	
	ierr = DMDestroy(&clone);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}



#undef __FUNCT__
#define __FUNCT__ "SwarmUpdateProperties_MPntPStokes_el"
PetscErrorCode SwarmUpdateProperties_MPntPStokes_el(DataBucket db,pTatinCtx user,Vec X)
{
	BTruth found;
	PetscErrorCode ierr;
    DM                dau,dap;
    Vec               Uloc,Ploc;
    Vec               u,p;
    PetscScalar       *LA_Uloc,*LA_Ploc;
	
	PetscFunctionBegin;
	
	
	DataBucketQueryDataFieldByName(db,MPntPStokesElas_classname,&found);
	if(found==BFALSE) {
		SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot find DataField with name %s \n", MPntPStokesElas_classname );
	}
	
    ierr = DMCompositeGetEntries(user->pack,&dau,&dap);CHKERRQ(ierr);
    ierr = DMCompositeGetLocalVectors(user->pack,&Uloc,&Ploc);CHKERRQ(ierr);
    ierr = DMCompositeScatter(user->pack,X,Uloc,Ploc);CHKERRQ(ierr);
    ierr = VecGetArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
    ierr = VecGetArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
    
    ierr = TkUpdateRheologyMarkerStokes_EVPT(user,dau,LA_Uloc,dap,LA_Ploc);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}
