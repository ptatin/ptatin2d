

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"

#include "pTatin2d.h"
#include "swarm_fields.h"
#include "data_exchanger.h"
#include "MPntStd_def.h"
#include "MPntPStokes_def.h"
#include "MPntPStokesPl_def.h"


#undef __FUNCT__
#define __FUNCT__ "SwarmView_MPntPStokesPl_VTKappended_binary"
PetscErrorCode SwarmView_MPntPStokesPl_VTKappended_binary(DataBucket db,const char name[])
{
	PetscMPIInt rank;
	FILE *vtk_fp;
	PetscInt k;
	int npoints;
	PetscLogDouble t0,t1;
	DataField PField_std;
	DataField PField_stokes;
	DataField PField_stokespl;
	int byte_offset,length;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	ierr = PetscGetTime(&t0);CHKERRQ(ierr);
	
	if ((vtk_fp = fopen ( name, "w")) == NULL)  {
		SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
	}
	
	fprintf( vtk_fp, "<?xml version=\"1.0\"?>\n");
	
#ifdef WORDSIZE_BIGENDIAN
	fprintf( vtk_fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
#else
	fprintf( vtk_fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
#endif
	fprintf( vtk_fp, "\t<UnstructuredGrid>\n" );
	
	DataBucketGetSizes(db,&npoints,PETSC_NULL,PETSC_NULL);
	fprintf( vtk_fp, "\t\t<Piece NumberOfPoints=\"%d\" NumberOfCells=\"%d\">\n",npoints,npoints );
	
	
	fprintf( vtk_fp, "\n");
	fprintf( vtk_fp, "\t\t\t<Cells>\n");
	
	byte_offset = 0;
	
	// connectivity //
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Int32\" Name=\"connectivity\" format=\"appended\" offset=\"%d\" />\n",byte_offset);
  byte_offset = byte_offset + sizeof(int) + npoints * sizeof(int);
	
	// offsets //
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Int32\" Name=\"offsets\" format=\"appended\" offset=\"%d\" />\n",byte_offset);
  byte_offset = byte_offset + sizeof(int) + npoints * sizeof(int);
	
	// types //
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"UInt8\" Name=\"types\" format=\"appended\" offset=\"%d\" />\n",byte_offset);
  byte_offset = byte_offset + sizeof(int) + npoints * sizeof(unsigned char);
	
	fprintf( vtk_fp, "\t\t\t</Cells>\n");
	
	fprintf( vtk_fp, "\n");
	fprintf( vtk_fp, "\t\t\t<CellData>\n");
	fprintf( vtk_fp, "\t\t\t</CellData>\n");
	fprintf( vtk_fp, "\n");
	
	
	DataBucketGetDataFieldByName(db, MPntStd_classname ,&PField_std);
	DataBucketGetDataFieldByName(db, MPntPStokes_classname ,&PField_stokes);
	DataBucketGetDataFieldByName(db, MPntPStokesPl_classname ,&PField_stokespl);
	
	
	/* point coordinates */
	fprintf( vtk_fp, "\t\t\t<Points>\n");
	
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\" format=\"appended\" offset=\"%d\" />\n",byte_offset);
  byte_offset = byte_offset + sizeof(int) + npoints * 3 * sizeof(double);
	
	fprintf( vtk_fp, "\t\t\t</Points>\n");
	fprintf( vtk_fp, "\n");
	
	/* point data BEGIN */
	fprintf( vtk_fp, "\t\t\t<PointData>\n");
	
	/* auto generated shit goes here */
	{
		MPntStd       *marker_std    = PField_std->data; /* should write a function to do this */
		MPntPStokes   *marker_stokes = PField_stokes->data; /* should write a function to do this */
		MPntPStokesPl *marker_stokespl = PField_stokespl->data; /* should write a function to do this */
		
		MPntStdVTKWriteBinaryAppendedHeaderAllFields(vtk_fp,&byte_offset,(const int)npoints,(const MPntStd*)marker_std );
		MPntPStokesVTKWriteBinaryAppendedHeaderAllFields(vtk_fp,&byte_offset,(const int)npoints,(const MPntPStokes*)marker_stokes);
		MPntPStokesPlVTKWriteBinaryAppendedHeaderAllFields(vtk_fp,&byte_offset,(const int)npoints,(const MPntPStokesPl*)marker_stokespl);
	}
	fprintf( vtk_fp, "\t\t\t</PointData>\n");
	fprintf( vtk_fp, "\n");
	/* point data END */
	
	
	fprintf( vtk_fp, "\t\t</Piece>\n");
	fprintf( vtk_fp, "\t</UnstructuredGrid>\n");
	
	/* WRITE APPENDED DATA HERE */
	fprintf( vtk_fp,"\t<AppendedData encoding=\"raw\">\n");
	fprintf( vtk_fp,"_");
	
	/* connectivity, offsets, types, coords */
	////////////////////////////////////////////////////////
	/* write connectivity */
	length = sizeof(int)*npoints;
	fwrite( &length,sizeof(int),1,vtk_fp);
	for (k=0; k<npoints; k++) {
		int idx = k;
		fwrite( &idx, sizeof(int),1, vtk_fp );
	}
	////////////////////////////////////////////////////////
	/* write offset */
	length = sizeof(int)*npoints;
	fwrite( &length,sizeof(int),1,vtk_fp);
	for (k=0; k<npoints; k++) {
		int idx = k+1;
		fwrite( &idx, sizeof(int),1, vtk_fp );
	}
	////////////////////////////////////////////////////////
	/* write types */
	length = sizeof(unsigned char)*npoints;
	fwrite( &length,sizeof(int),1,vtk_fp);
	for (k=0; k<npoints; k++) {
		unsigned char idx = 1; /* VTK_VERTEX */
		fwrite( &idx, sizeof(unsigned char),1, vtk_fp );
	}
	////////////////////////////////////////////////////////
	/* write coordinates */
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	length = sizeof(double)*npoints*3;
	fwrite( &length,sizeof(int),1,vtk_fp);
	for (k=0; k<npoints; k++) {
		MPntStd *marker;
		double  *coor;
		double  coords_k[] = {0.0, 0.0, 0.0};
		
		DataFieldAccessPoint(PField_std,k,(void**)&marker);
		MPntStdGetField_global_coord(marker,&coor);
		coords_k[0] = coor[0];
		coords_k[1] = coor[1];
		
		fwrite( coords_k, sizeof(double), 3, vtk_fp );
	}
	DataFieldRestoreAccess(PField_std);
	
	/* auto generated shit for the marker data goes here */
	{
		MPntStd       *markers_std      = PField_std->data;
		MPntPStokes   *markers_stokes   = PField_stokes->data;
		MPntPStokesPl *markers_stokespl = PField_stokespl->data;
		
		MPntStdVTKWriteBinaryAppendedDataAllFields(vtk_fp,npoints,markers_std);
		MPntPStokesVTKWriteBinaryAppendedDataAllFields(vtk_fp,npoints,markers_stokes);
		MPntPStokesPlVTKWriteBinaryAppendedDataAllFields(vtk_fp,npoints,markers_stokespl);
	}
	
	fprintf( vtk_fp,"\n\t</AppendedData>\n");
	
	
	fprintf( vtk_fp, "</VTKFile>\n");
	
	
	if( vtk_fp!= NULL ) {
		fclose( vtk_fp );
		vtk_fp = NULL;
	}
	
	ierr = PetscGetTime(&t1);CHKERRQ(ierr);
#ifdef PROFILE_TIMING
	PetscPrintf(PETSC_COMM_WORLD,"VTKWriter(%s): Time %1.4e sec\n",__FUNCT__,t1-t0);
#endif
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "__SwarmView_MPntPStokesPl_PVTU"
PetscErrorCode __SwarmView_MPntPStokesPl_PVTU(const char prefix[],const char name[])
{
	PetscMPIInt nproc,rank;
	FILE *vtk_fp;
	PetscInt i;
	char *sourcename;
	
	PetscFunctionBegin;
	
	if ((vtk_fp = fopen ( name, "w")) == NULL)  {
		SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
	}
	
	/* (VTK) generate pvts header */
	fprintf( vtk_fp, "<?xml version=\"1.0\"?>\n");
	
#ifdef WORDSIZE_BIGENDIAN
	fprintf( vtk_fp, "<VTKFile type=\"PUnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
#else
	fprintf( vtk_fp, "<VTKFile type=\"PUnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
#endif
	
	/* define size of the nodal mesh based on the cell DM */
	fprintf( vtk_fp, "  <PUnstructuredGrid GhostLevel=\"0\">\n" ); /* note overlap = 0 */
	
	
	/* DUMP THE CELL REFERENCES */
	fprintf( vtk_fp, "    <PCellData>\n");
	fprintf( vtk_fp, "    </PCellData>\n");
	
	
	fprintf( vtk_fp, "    <PPoints>\n");
	fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\"/>\n");
	fprintf( vtk_fp, "    </PPoints>\n");
	///////////////
	
	///////////////
  fprintf(vtk_fp, "    <PPointData>\n");
	MPntStdPVTUWriteAllPPointDataFields(vtk_fp);
	MPntPStokesPVTUWriteAllPPointDataFields(vtk_fp);
	MPntPStokesPlPVTUWriteAllPPointDataFields(vtk_fp);
  fprintf(vtk_fp, "    </PPointData>\n");
	///////////////
	
	
	/* write out the parallel information */
	MPI_Comm_size(PETSC_COMM_WORLD,&nproc);
	for (i=0; i<nproc; i++) {
		asprintf( &sourcename, "%s-subdomain%1.5d.vtu", prefix, i );
		fprintf( vtk_fp, "    <Piece Source=\"%s\"/>\n",sourcename);
		free(sourcename);
	}
	
	/* close the file */
	fprintf( vtk_fp, "  </PUnstructuredGrid>\n");
	fprintf( vtk_fp, "</VTKFile>\n");
	
	if(vtk_fp!=NULL){
		fclose( vtk_fp );
		vtk_fp = NULL;
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SwarmOutputParaView_MPntPStokesPl"
PetscErrorCode SwarmOutputParaView_MPntPStokesPl(DataBucket db,const char path[],const char prefix[])
{ 
	char *vtkfilename,*filename;
	PetscMPIInt rank;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ierr = pTatinGenerateParallelVTKName(prefix,"vtu",&vtkfilename);CHKERRQ(ierr);
	if (path) {
		asprintf(&filename,"%s/%s",path,vtkfilename);
	} else {
		asprintf(&filename,"./%s",vtkfilename);
	}
	
	//#ifdef __VTK_ASCII__
	//	ierr = SwarmView_MPntPStokesPl_VTKascii( db,filename );CHKERRQ(ierr);
	//#endif
	//#ifndef __VTK_ASCII__
	ierr = SwarmView_MPntPStokesPl_VTKappended_binary(db,filename);CHKERRQ(ierr);
	//#endif
	free(filename);
	free(vtkfilename);
	
	
	ierr = pTatinGenerateVTKName(prefix,"pvtu",&vtkfilename);CHKERRQ(ierr);
	if (path) {
		asprintf(&filename,"%s/%s",path,vtkfilename);
	} else {
		asprintf(&filename,"./%s",vtkfilename);
	}
	
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	if (rank==0) {
		ierr = __SwarmView_MPntPStokesPl_PVTU( prefix, filename );CHKERRQ(ierr);
	}
	free(filename);
	free(vtkfilename);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SwarmUpdatePosition_Communication_Generic"
PetscErrorCode SwarmUpdatePosition_Communication_Generic(DM da,DataBucket db,DataEx de)
{
	DataField            PField_std;
	DataField            *PField_all;
	int                  f,nfields;
	size_t               sizeof_marker_contents;
	PetscInt p,npoints,npoints_global_init,npoints_global_fin;
	void     *recv_data;
	void     *data_p;
	int n,neighborcount, *neighborranks2;
	int recv_length,npoints_accepted;
	PetscMPIInt rank,size;
	MPntStd     *marker_std;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	/* communucate */
	MPI_Comm_size(((PetscObject)da)->comm,&size);
	if (size==1) {
		PetscFunctionReturn(0);
	}
	
	MPI_Comm_rank(((PetscObject)da)->comm,&rank);
	
	neighborcount  = de->n_neighbour_procs;
	neighborranks2 = de->neighbour_procs;
	
	
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataBucketGetDataFields(db,&nfields,&PField_all);
	for (f=1; f<nfields; f++) {
		DataFieldGetAccess(PField_all[f]);
	}
	
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	DataBucketGetSizes(db,&npoints,0,0);
	
	MPI_Allreduce(&npoints,&npoints_global_init,1,MPI_INT,MPI_SUM,de->comm);
	
	/* figure out how many points left processor */
	ierr = DataExInitializeSendCount(de);CHKERRQ(ierr);
	for (p=0; p<npoints; p++) {
		PetscBool onproc;
		MPntStd   *marker;
		
		DataFieldAccessPoint(PField_std,p,(void**)&marker);
		onproc = PETSC_TRUE;
		if (marker->wil==-1) {
			onproc = PETSC_FALSE;
		}
		
		if (onproc==PETSC_FALSE) {
			for (n=0; n<neighborcount; n++) {
				//	printf("  DataEx: rank %d sending %d to %d \n",rank,1,neighborranks2[n] );
				ierr = DataExAddToSendCount( de, neighborranks2[n], 1 );CHKERRQ(ierr);
			}
		}
	}
	ierr = DataExFinalizeSendCount(de);CHKERRQ(ierr);
	
	DataFieldRestoreAccess(PField_std);
	
	/* pack points which left processor */
	marker_std    = PField_std->data; /* should write a function to do this */
	
	sizeof_marker_contents = 0;
	sizeof_marker_contents = sizeof_marker_contents + sizeof(MPntStd);
	for (f=1; f<nfields; f++) {
		sizeof_marker_contents += PField_all[f]->atomic_size;
	}

	ierr = DataExPackInitialize(de,sizeof_marker_contents);CHKERRQ(ierr);
	
	/* allocate a temporary buffer whihc is large enough to store all marker fields from an individual point,p */
	ierr = PetscMalloc(sizeof_marker_contents,&data_p);CHKERRQ(ierr);
	
	for (p=0; p<npoints; p++) {
		PetscBool onproc;
		MPntStd     *marker_p;
		void        *marker_p_prop;
		size_t      atomic_size,offset;
		
		/* access fields from the bucket */
		marker_p     = &marker_std[p];
		
		onproc = PETSC_TRUE;
		if (marker_p->wil==-1) {
			onproc = PETSC_FALSE;
			/* pack together */
			//(void*)((char*)field->data + index*field->atomic_size)
			
			/* copy all fields from the bucket into a temporary buffer */
			PetscMemcpy((void*)data_p,                         marker_p,    sizeof(MPntStd));
			
			offset = sizeof(MPntStd);
			for (f=1; f<nfields; f++) {
				atomic_size = PField_all[f]->atomic_size;
				DataFieldAccessPoint(PField_all[f],p,&marker_p_prop);

				PetscMemcpy((void*)((char*)data_p+offset),marker_p_prop,atomic_size);
				offset = offset + atomic_size;
			}
		}
		
		if (onproc==PETSC_FALSE) {
			for (n=0; n<neighborcount; n++) {
				ierr = DataExPackData( de, neighborranks2[n], 1,(void*)data_p );CHKERRQ(ierr);
			}
		}
	}		
	for (f=1; f<nfields; f++) {
		DataFieldRestoreAccess(PField_all[f]);
	}
	
	
	ierr = DataExPackFinalize(de);CHKERRQ(ierr);
	
	/* remove points which left processor */
	DataBucketGetSizes(db,&npoints,0,0);
	DataFieldGetAccess(PField_std);
	for (p=0; p<npoints; p++) {
		PetscBool onproc;
		MPntStd   *marker_p;
		
		DataFieldAccessPoint(PField_std,p,(void**)&marker_p);
		onproc = PETSC_TRUE;
		if (marker_p->wil==-1) {
			onproc = PETSC_FALSE;
		}
		
		if (onproc==PETSC_FALSE) { 
			/* kill point */
			DataBucketRemovePointAtIndex(db,p);
			DataBucketGetSizes(db,&npoints,0,0); /* you need to update npoints as the list size decreases! */
			p--; /* check replacement point */
		}
	}		
	DataFieldRestoreAccess(PField_std);
	
	// START communicate //
	ierr = DataExBegin(de);CHKERRQ(ierr);
	ierr = DataExEnd(de);CHKERRQ(ierr);
	// END communicate //
	
	// receive, if i own them, add new points to list //
	ierr = DataExGetRecvData( de, &recv_length, (void**)&recv_data );CHKERRQ(ierr);
	{
		int totalsent;
		MPI_Allreduce(&recv_length,&totalsent,1,MPI_INT,MPI_SUM,de->comm);
		//[LOG]PetscPrintf(PETSC_COMM_WORLD,"  DataEx: total points sent = %d \n", totalsent);
	}
	
	
	
	/* update the local coordinates and cell owner for all recieved points */
	{
		DM cda;
		Vec gcoords;
		PetscScalar *LA_gcoords;
		double tolerance;
		int max_its;
		Truth use_nonzero_guess, monitor, log;
		PetscInt lmx,lmy;
		PetscInt nel,nen_u;
		MPntStd *marker_p;
		const PetscInt *elnidx_u;
		
		/* setup for coords */
		ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
		ierr = DMDAGetGhostedCoordinates(da,&gcoords);CHKERRQ(ierr);
		ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
		
		ierr = DMDAGetElements_pTatin(da,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
		
		ierr = DMDAGetLocalSizeElementQ2(da,&lmx,&lmy,PETSC_NULL);CHKERRQ(ierr);
		
		/* point location parameters */
		tolerance         = 1.0e-10;
		max_its           = 10;
		use_nonzero_guess = _FALSE; /* for markers sent across processors, it is necessary to NOT use the last known values! */
		monitor           = _FALSE;
		log               = _FALSE;
		
		/* 
		 NOTE: My algorithm will break down if you try to use an non-zero initial guess
		 if the marker has been sent to another cpu. This could be fixed, however if would
		 require additional logic to be added into the function InverseMappingDomain_2dQ2()
		 
		 It seems more reasonable to simply assume that if the marker was sent to another cpu,
		 the value of wil currently stored on the marker is completely meaningless and thus we
		 should ALWAYS use a zero initial guess (i.e. assume we know nothing)
		 */
		
		for (p=0; p<recv_length; p++) {
			marker_p = (MPntStd*)( (char*)recv_data + p*(sizeof_marker_contents) );
			//printf("revc p=%d : (%lf,%lf) \n", p, marker_p->coor[0], marker_p->coor[1] );
			
			InverseMappingDomain_2dQ2( 		 tolerance, max_its,
																use_nonzero_guess, 
																monitor, log,
																(const double*)LA_gcoords, (const int)lmx,(const int)lmy, (const int*)elnidx_u,
																1, marker_p );
			//printf("++[%d] revc p=%d : wil=%d \n", rank,p, marker_p->wil );
		}
		
		ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	}
	
	/* accepte all points living locally */
	npoints_accepted = 0;
	for (p=0; p<recv_length; p++) {
		PetscBool onproc;
		MPntStd     *marker_p;
		MPntPStokes *marker_stk_p;
		size_t      offset;
		
		offset = 0;
		marker_p     = (MPntStd*)(     (char*)recv_data + p*(sizeof_marker_contents) + offset);
		
		//printf("++[%d] revc p=%d : wil=%d [eta,rho] = (%lf,%lf) \n", rank,p, marker_p->wil, marker_stk_p->eta, marker_stk_p->rho );
		
		onproc = PETSC_TRUE;
		if (marker_p->wil==-1) {
			onproc = PETSC_FALSE;
		}
		
		if (onproc == PETSC_TRUE) {
			int end;
			
			DataBucketAddPoint(db);
			DataBucketGetSizes(db,&end,0,0);
			end = end - 1;

			offset = 0;
			for (f=0; f<nfields; f++) {
				void *data_p = (void*)( (char*)recv_data + p*(sizeof_marker_contents) + offset );
				
				DataFieldInsertPoint(PField_all[f], end, (void*)data_p );

				offset = offset + PField_all[f]->atomic_size;
			}
			
			
			npoints_accepted++;
		}
	}	
	//printf("  DataEx: rank %d accepted %d new points \n",rank,npoints_accepted );
	
	
	DataBucketGetSizes(db,&npoints,0,0);
	MPI_Allreduce(&npoints,&npoints_global_fin,1,MPI_INT,MPI_SUM,de->comm);
	//PetscPrintf(PETSC_COMM_WORLD,"  SwarmUpdatePosition_GENERIC(Communication): num. points global ( init. = %d : final = %d )\n", npoints_global_init,npoints_global_fin);
	ierr = PetscFree(data_p);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SwarmUpdateProperties_MPntPStokesPl"
PetscErrorCode SwarmUpdateProperties_MPntPStokesPl(DataBucket db,pTatinCtx user,Vec X)
{
	BTruth found;
  DM                dau,dap;
  Vec               Uloc,Ploc;
  Vec               u,p;
  PetscScalar       *LA_Uloc,*LA_Ploc;
  PetscErrorCode ierr;
  	
	PetscFunctionBegin;
	
	
	DataBucketQueryDataFieldByName(db,MPntPStokesPl_classname,&found);
	if(found==BFALSE) {
		SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot find DataField with name %s \n", MPntPStokesPl_classname );
	}
	
    ierr = DMCompositeGetEntries(user->pack,&dau,&dap);CHKERRQ(ierr);
    ierr = DMCompositeGetLocalVectors(user->pack,&Uloc,&Ploc);CHKERRQ(ierr);
	ierr = DMCompositeScatter(user->pack,X,Uloc,Ploc);CHKERRQ(ierr);
	ierr = VecGetArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
	ierr = VecGetArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
	switch  (user->rheology_constants.rheology_type) {
	case RHEOLOGY_VISCO_PLASTIC :
		{
	      	if(found==BFALSE) {
				SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot find DataField with name %s \n", MPntPStokesPl_classname );
			}
	       	ierr = pTatin_TkUpdateRheologyMarkerStokes_ViscoPlastic(user,dau,LA_Uloc,dap,LA_Ploc);CHKERRQ(ierr);
	    }
	break;
	case RHEOLOGY_VISCO_PLASTIC_STRAIN_WEAKENING : 
		{
			if(found==BFALSE) {
				SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot find DataField with name %s \n", MPntPStokesPl_classname );
			}
	       	ierr = pTatin_TkUpdateRheologyMarkerStokes_ViscoPlastic(user,dau,LA_Uloc,dap,LA_Ploc);CHKERRQ(ierr);
	    }
	break;
	case RHEOLOGY_VISCO_PLASTIC_T :
	   {
	   		if(found==BFALSE) {
				SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot find DataField with name %s \n", MPntPStokesPl_classname );
			}
	       	ierr = pTatin_TkUpdateRheologyMarkerStokes_ViscoPlastic(user,dau,LA_Uloc,dap,LA_Ploc);CHKERRQ(ierr);
	    }
	break;
	case RHEOLOGY_VISCO_PLASTIC_STRAIN_WEAKENING_T : 
	   {
	   		if(found==BFALSE) {
				SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot find DataField with name %s \n", MPntPStokesPl_classname );
			}
	       	ierr = pTatin_TkUpdateRheologyMarkerStokes_ViscoPlastic(user,dau,LA_Uloc,dap,LA_Ploc);CHKERRQ(ierr);
	    }
	break;
	case RHEOLOGY_VPT_STD : 
		{
			if(found==BFALSE) {
		         SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot find DataField with name %s \n", MPntPStokesPl_classname );
	              }
			ierr = TkUpdateRheologyMarkerStokes_VPT(user,dau,LA_Uloc,dap,LA_Ploc);CHKERRQ(ierr);
		}
	    break;
	    default:
		   break;
	
	}
	ierr = VecRestoreArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
	ierr = VecRestoreArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
  ierr = DMCompositeRestoreLocalVectors(user->pack,&Uloc,&Ploc);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}


