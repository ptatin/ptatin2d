
#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"


/* Quasi newton update on the coordinates */

PetscErrorCode FormFunctionLocal_U_QCoords(pTatinCtx user,DM dau,PetscScalar LA_u[],DM dap,PetscScalar LA_p[],DM dac,PetscScalar LA_c[],PetscScalar LA_Ru[]);
PetscErrorCode FormFunctionLocal_P_QCoords(pTatinCtx user,DM dau,PetscScalar LA_u[],DM dap,PetscScalar LA_p[],DM dac,PetscScalar LA_c[],PetscScalar LA_Rp[]);

PetscErrorCode AssembleStokesA11_QCoords_Q2(pTatinCtx user,DM dau,PetscScalar LA_u[],DM dap,PetscScalar LA_p[],DM dac,PetscScalar LA_c[],Mat Auu);
PetscErrorCode AssembleStokesA12_QCoords_Q2(pTatinCtx user,DM dau,PetscScalar LA_u[],DM dap,PetscScalar LA_p[],DM dac,PetscScalar LA_c[],Mat A);
PetscErrorCode AssembleStokesA21_QCoords_Q2(pTatinCtx user,DM dau,PetscScalar LA_u[],DM dap,PetscScalar LA_p[],DM dac,PetscScalar LA_c[],Mat A);
PetscErrorCode AssembleStokesB22_QCoords_P1(pTatinCtx user,DM dau,PetscScalar LA_u[],DM dap,PetscScalar LA_p[],DM dac,PetscScalar LA_c[],Mat Bpp);

PetscErrorCode FormFunctionLocal_U_tractionBC(pTatinCtx user,DM dau,PetscScalar velocity[],DM dap,PetscScalar pressure[],PetscScalar Ru[]);

/*
 Computes r = Ax - b
 SNES will scale by -1, F = -r = b - Ax
 Thus, in OUR function, dirichlet slots become A_ii(x_i - phi)
 In SNES, these become A_ii(phi-x_i), and the updates on the dirichlet slots will be
 A_ii d_i = -F_i 
 = A_ii(phi-x_i)
 Then the update will be 
 x_i^new = x_i + d_i
 = x_i + inv(A_ii) A_ii(phi-x_i)
 = x_i + phi - x_i
 = phi
 */
#undef __FUNCT__  
#define __FUNCT__ "FormFunction_Stokes_QCoords"
PetscErrorCode FormFunction_Stokes_QCoords(SNES snes,Vec X,Vec F,void *ctx)
{
  pTatinCtx   user = (pTatinCtx)ctx;
  DM                dau,dap,dac;
  PetscErrorCode    ierr;
  Vec               Uloc,Ploc,Cloc,FUloc,FPloc;
	Vec               u,p,c,Fu,Fp;
  PetscScalar       *LA_Uloc,*LA_Ploc,*LA_Cloc;
  PetscScalar       *LA_FUloc,*LA_FPloc;
	
  PetscFunctionBegin;
    
  ierr = DMCompositeGetEntries(user->pack,&dau,&dap);CHKERRQ(ierr);
	
  ierr = DMCompositeGetLocalVectors(user->pack,&Uloc,&Ploc);CHKERRQ(ierr);
  ierr = DMCompositeGetLocalVectors(user->pack,&FUloc,&FPloc);CHKERRQ(ierr);

	/* setup for coords */
	ierr = DMDAGetCoordinateDA(dau,&dac);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(dau,&c);CHKERRQ(ierr);
	ierr = DMGetLocalVector(dac,&Cloc);CHKERRQ(ierr);
	ierr = DMGlobalToLocalBegin(dac,c,INSERT_VALUES,Cloc);CHKERRQ(ierr);
	ierr = DMGlobalToLocalEnd  (dac,c,INSERT_VALUES,Cloc);CHKERRQ(ierr);
		
	/* get the local (ghosted) entries for each physics */
	ierr = DMCompositeScatter(user->pack,X,Uloc,Ploc);CHKERRQ(ierr);
	/* insert boundary conditions into local vectors */
	ierr = BCListInsertLocal(user->u_bclist,Uloc);CHKERRQ(ierr);
	
	/* update coords, x = x + dt.v */
	ierr = VecAXPY(Cloc,1.0*user->dt,Uloc);CHKERRQ(ierr);
	ierr = VecGetArray(Cloc,&LA_Cloc);CHKERRQ(ierr);
	
	ierr = VecGetArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
	ierr = VecGetArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
	
	/* compute Ax - b */
	ierr = VecZeroEntries(FUloc);CHKERRQ(ierr);
	ierr = VecZeroEntries(FPloc);CHKERRQ(ierr);
	ierr = VecGetArray(FUloc,&LA_FUloc);CHKERRQ(ierr);
	ierr = VecGetArray(FPloc,&LA_FPloc);CHKERRQ(ierr);
	
	/* ======================================== */
	/*         UPDATE NON-LINEARITIES           */
	/* evaluate rheology and rhs using X        */
	
	/* map marker eta to quadrature points */
	
	/* map marker force to quadrature points */
	
	/* ======================================== */
	
	ierr = pTatin_EvaluateRheologyNonlinearities(user,dau,LA_Uloc,dap,LA_Ploc);CHKERRQ(ierr);
//	ierr = pTatin_EvaluateRheologyNonlinearities_QCoords(user,dau,LA_Uloc,dap,LA_Ploc,dac,LA_Cloc);CHKERRQ(ierr);
	
	ierr = FormFunctionLocal_U_QCoords(user,dau,LA_Uloc,dap,LA_Ploc,dac,LA_Cloc,LA_FUloc);CHKERRQ(ierr);
	ierr = FormFunctionLocal_U_tractionBC(user,dau,LA_Uloc,dap,LA_Ploc,LA_FUloc);CHKERRQ(ierr);
	
	ierr = FormFunctionLocal_P_QCoords(user,dau,LA_Uloc,dap,LA_Ploc,dac,LA_Cloc,LA_FPloc);CHKERRQ(ierr);
	
	ierr = VecRestoreArray(FPloc,&LA_FPloc);CHKERRQ(ierr);
	ierr = VecRestoreArray(FUloc,&LA_FUloc);CHKERRQ(ierr);
	ierr = VecRestoreArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
	ierr = VecRestoreArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
	ierr = VecRestoreArray(Cloc,&LA_Cloc);CHKERRQ(ierr);
	
	/* do global fem summation */
	ierr = VecZeroEntries(F);CHKERRQ(ierr);
	ierr = DMCompositeGather(user->pack,F,ADD_VALUES,FUloc,FPloc);CHKERRQ(ierr);
	
  ierr = DMCompositeRestoreLocalVectors(user->pack,&FUloc,&FPloc);CHKERRQ(ierr);
  ierr = DMCompositeRestoreLocalVectors(user->pack,&Uloc,&Ploc);CHKERRQ(ierr);
	
	/* modify F for the boundary conditions, F_k = scale_k(x_k - phi_k) */
	ierr = DMCompositeGetAccess(user->pack,F,&Fu,&Fp);CHKERRQ(ierr);
	ierr = DMCompositeGetAccess(user->pack,X,&u,&p);CHKERRQ(ierr);
	
	ierr = BCListResidualDirichlet(user->u_bclist,u,Fu);CHKERRQ(ierr);
	
	ierr = DMCompositeRestoreAccess(user->pack,X,&u,&p);CHKERRQ(ierr);
	ierr = DMCompositeRestoreAccess(user->pack,F,&Fu,&Fp);CHKERRQ(ierr);
		
	ierr = DMRestoreLocalVector(dac,&Cloc);CHKERRQ(ierr);
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormFunctionLocal_U_QCoords"
PetscErrorCode FormFunctionLocal_U_QCoords(pTatinCtx user,DM dau,PetscScalar LA_u[],DM dap,PetscScalar LA_p[],DM dac,PetscScalar LA_c[],PetscScalar LA_Ru[])
{	
	PetscErrorCode ierr;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
	PetscScalar GNIu[MAX_QUAD_PNTS][2][Q2_NODES_PER_EL_2D],NIp[MAX_QUAD_PNTS][P_BASIS_FUNCTIONS];
	PetscScalar NIu[MAX_QUAD_PNTS][U_BASIS_FUNCTIONS];
	PetscInt nel,nen_u,nen_p,e,n,k;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D],el_eta[MAX_QUAD_PNTS];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	PetscScalar Fe[Q2_NODES_PER_EL_2D*2],Be[Q2_NODES_PER_EL_2D*2];
	PetscInt vel_el_lidx[U_BASIS_FUNCTIONS*2];
	PetscInt *gidx,elgidx[2*Q2_NODES_PER_EL_2D];
	PetscInt nbcs,i,j;
	const PetscInt *ubcidx;
	Vec diag;
	PetscScalar *LA_diag;
	PetscLogDouble t0,t1;
	BCList u_bclist = user->u_bclist;
	GaussPointCoefficientsStokes *gausspoints;
	PetscFunctionBegin;
	
	/* quadrature */
	/*QuadratureCreateGauss_3pnt_2D(&ngp,gp_xi,gp_weight);*/
	ngp       = user->Q->ngp;
	gp_xi     = user->Q->xi;
	gp_weight = user->Q->weight;
	for (k=0; k<ngp; k++) {
		PetscScalar *xip = &gp_xi[2*k];
		
		PTatinConstructNI_Q2_2D(xip,NIu[k]);
		PTatinConstructGNI_Q2_2D(xip,GNIu[k]);
	}
	
	ierr = DMDAGetGlobalIndices(dau,0,&gidx);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
	PetscGetTime(&t0);
	for (e=0;e<nel;e++) {
		PetscScalar int_P, int_divu;
		
		ierr = StokesVelocity_GetElementLocalIndices(vel_el_lidx,(PetscInt*)&elnidx_u[nen_u*e]);CHKERRQ(ierr);
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_c);CHKERRQ(ierr);
		ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],LA_u);CHKERRQ(ierr);
		ierr = DMDAGetScalarElementField_2D(elp,nen_p,(PetscInt*)&elnidx_p[nen_p*e],LA_p);CHKERRQ(ierr);
		
		for( i=0; i<Q2_NODES_PER_EL_2D; i++ ) {
			ux[i] = elu[2*i  ];
			uy[i] = elu[2*i+1];
			
			xc[i] = elcoords[2*i  ];
			yc[i] = elcoords[2*i+1];
		}
		
		for (n=0; n<ngp; n++) {
			PetscScalar *xip = &gp_xi[2*n];
			ConstructNi_pressure(xip,elcoords,NIp[n]);
		}
		
		/* insert element matrix into global matrix */
		/* initialise element stiffness matrix */
		PetscMemzero( Fe, sizeof(PetscScalar)* Q2_NODES_PER_EL_2D*2 );
		PetscMemzero( Be, sizeof(PetscScalar)* Q2_NODES_PER_EL_2D*2 );
		
		/* evaluate the viscosity */
		for (n=0; n<ngp; n++) {
			el_eta[n] = gausspoints[n].eta;
		}
		
		int_P = int_divu = 0.0;
		for (n=0; n<ngp; n++) {
			PetscScalar  J[2][2], iJ[2][2];
			PetscScalar  exx,eyy,exy;
			PetscScalar  sxx,syy,sxy,pressure_gp;
			PetscScalar fac,d1,d2,J_p,ojp;
			
			/* coord transformation */
			for( i=0; i<2; i++ ) {
				for( j=0; j<2; j++ ) { J[i][j] = 0.0; }
			}
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				J[0][0] += GNIu[n][0][k] * xc[k] ;
				J[0][1] += GNIu[n][0][k] * yc[k] ;
				
				J[1][0] += GNIu[n][1][k] * xc[k] ;
				J[1][1] += GNIu[n][1][k] * yc[k] ;
			}
			J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
			ojp = 1.0/J_p;
			iJ[0][0] =  J[1][1]*ojp;
			iJ[0][1] = -J[0][1]*ojp;
			iJ[1][0] = -J[1][0]*ojp;
			iJ[1][1] =  J[0][0]*ojp;
			
			/* global derivs */
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				nx[k] = iJ[0][0]*GNIu[n][0][k] + iJ[0][1]*GNIu[n][1][k];
				ny[k] = iJ[1][0]*GNIu[n][0][k] + iJ[1][1]*GNIu[n][1][k];
			}
			
			fac = gp_weight[n] * J_p;
			
			/* pressure */
			pressure_gp = 0.0;
			for( i=0; i<P_BASIS_FUNCTIONS; i++ ) {
				pressure_gp += NIp[n][i] * elp[i];
			}
			pressure_gp = pressure_gp * fac;
			
			/* strain rate, B u */
			exx = eyy = exy = 0.0;
			for( i=0; i<Q2_NODES_PER_EL_2D; i++ ) {
				exx += nx[i] * ux[i];
				eyy += ny[i] * uy[i];
				exy += ny[i] * ux[i] + nx[i] * uy[i];
			}
			int_P    += fac * pressure_gp;
			int_divu += fac * (exx + eyy);
			
			/* constituive */
			d1 = 2.0 * el_eta[n] ;
			d2 =       el_eta[n] ;
			
			/* stress */
            sxx = d1 * exx;//+gausspoints[n].stress_old[0];
            syy = d1 * eyy;//+gausspoints[n].stress_old[1];
            sxy = d2 * exy;//+gausspoints[n].stress_old[2];
            //ierr=PetscPrintf(PETSC_COMM_WORLD,"sxx_old= %e sxx_n= %e eta = %e \n",gausspoints[n].stress_old[0],sxx,d1);CHKERRQ(ierr);
            sxx = sxx*fac;
            syy = syy*fac;
            sxy = sxy*fac;
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) { 
				Fe[2*k]   += nx[k]*(sxx-pressure_gp)   + ny[k]*sxy; 
				Fe[2*k+1] += ny[k]*(syy-pressure_gp)   + nx[k]*sxy; 
			}
			
			/* compute any body force terms here */
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) { 
				Be[2*k  ] = Be[2*k  ] + fac * NIu[n][k] * gausspoints[n].Fu[0];
				Be[2*k+1] = Be[2*k+1] + fac * NIu[n][k] * gausspoints[n].Fu[1];
			}
		}
		
		/* combine body force with A.x */
		for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) { 
			Fe[2*k  ] = Fe[2*k  ] - Be[2*k  ];
			Fe[2*k+1] = Fe[2*k+1] - Be[2*k+1];
		}
		
		ierr = DMDASetValuesLocalStencil_AddValues_Stokes_Velocity(LA_Ru,vel_el_lidx,Fe);CHKERRQ(ierr);
	}
	
	PetscGetTime(&t1);
	//PetscPrintf(PETSC_COMM_WORLD,"Assemble Ru, = %1.4e (sec)\n",t1-t0);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormFunctionLocal_P_QCoords"
PetscErrorCode FormFunctionLocal_P_QCoords(pTatinCtx user,DM dau,PetscScalar LA_u[],DM dap,PetscScalar LA_p[],DM dac,PetscScalar LA_c[],PetscScalar LA_Rp[])
{	
	PetscErrorCode ierr;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
	PetscScalar GNI[MAX_QUAD_PNTS][2][Q2_NODES_PER_EL_2D],NIp[MAX_QUAD_PNTS][P_BASIS_FUNCTIONS];
	PetscInt nel,nen_u,nen_p,e,n,k;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	PetscScalar Fe[P_BASIS_FUNCTIONS];
	PetscScalar Be[P_BASIS_FUNCTIONS];
	PetscInt p_el_lidx[P_BASIS_FUNCTIONS];
	PetscInt *gidx,elgidx[2*Q2_NODES_PER_EL_2D];
	PetscInt *gidx_p,elgidx_p[P_BASIS_FUNCTIONS];
	PetscInt nbcs,i,j;
	const PetscInt *ubcidx;
	Vec diag;
	PetscScalar *LA_diag;
	PetscLogDouble t0,t1;
	BCList u_bclist = user->u_bclist;
	GaussPointCoefficientsStokes *gausspoints;
	
	PetscFunctionBegin;
	/* quadrature */
	ngp       = user->Q->ngp;
	gp_xi     = user->Q->xi;
	gp_weight = user->Q->weight;
	for (k=0; k<ngp; k++) {
		PetscScalar *xip = &gp_xi[2*k];
		PTatinConstructGNI_Q2_2D(xip,GNI[k]);
	}
	
	ierr = DMDAGetGlobalIndices(dau,0,&gidx);CHKERRQ(ierr);
	ierr = DMDAGetGlobalIndices(dap,0,&gidx_p);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
	PetscGetTime(&t0);
	for (e=0;e<nel;e++) {
		ierr = StokesPressure_GetElementLocalIndices(p_el_lidx,(PetscInt*)&elnidx_p[nen_p*e]);CHKERRQ(ierr);
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_c);CHKERRQ(ierr);
		ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],LA_u);CHKERRQ(ierr);
		ierr = DMDAGetScalarElementField_2D(elp,nen_p,(PetscInt*)&elnidx_p[nen_p*e],LA_p);CHKERRQ(ierr);
		
		for( i=0; i<Q2_NODES_PER_EL_2D; i++ ) {
			ux[i] = elu[2*i  ];
			uy[i] = elu[2*i+1];
			
			xc[i] = elcoords[2*i  ];
			yc[i] = elcoords[2*i+1];
		}
		
		for (n=0; n<ngp; n++) {
			PetscScalar *xip = &gp_xi[2*n];
			ConstructNi_pressure(xip,elcoords,NIp[n]);
		}
		
		/* insert element matrix into global matrix */
		//		ierr = GetElementEqnIndicesQ2((PetscInt*)&elnidx_u[nen_u*e],gidx,elgidx);CHKERRQ(ierr);
		//		ierr = GetElementEqnIndicesPressure(nen_p,(PetscInt*)&elnidx_p[nen_p*e],gidx_p,elgidx_p);CHKERRQ(ierr);
		
		/* initialise element stiffness matrix */
		PetscMemzero( Fe, sizeof(PetscScalar)* P_BASIS_FUNCTIONS );
		PetscMemzero( Be, sizeof(PetscScalar)* P_BASIS_FUNCTIONS );
		
		for (n=0; n<ngp; n++) {
			PetscScalar  J[2][2], iJ[2][2];
			PetscScalar  div_u_gp;
			PetscScalar fac,J_p,ojp;
			
			/* coord transformation */
			for( i=0; i<2; i++ ) {
				for( j=0; j<2; j++ ) { J[i][j] = 0.0; }
			}
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				J[0][0] += GNI[n][0][k] * xc[k] ;
				J[0][1] += GNI[n][0][k] * yc[k] ;
				
				J[1][0] += GNI[n][1][k] * xc[k] ;
				J[1][1] += GNI[n][1][k] * yc[k] ;
			}
			J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
			ojp = 1.0/J_p;
			iJ[0][0] =  J[1][1]*ojp;
			iJ[0][1] = -J[0][1]*ojp;
			iJ[1][0] = -J[1][0]*ojp;
			iJ[1][1] =  J[0][0]*ojp;
			
			/* global derivs */
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				nx[k] = iJ[0][0]*GNI[n][0][k] + iJ[0][1]*GNI[n][1][k];
				ny[k] = iJ[1][0]*GNI[n][0][k] + iJ[1][1]*GNI[n][1][k];
			}
			fac = gp_weight[n] * J_p;
			
			/* div(u) */
			div_u_gp = 0.0;
			for( i=0; i<U_BASIS_FUNCTIONS; i++ ) {
				div_u_gp += ( nx[i] * ux[i] + ny[i] * uy[i] );
			}
			div_u_gp = -div_u_gp * fac; /* note the -ve sign here */
			
			for( k=0; k<P_BASIS_FUNCTIONS; k++ ) { 
				Fe[k] += NIp[n][k] * div_u_gp; 
			}
			
			/* compute any body force terms here */
			for( k=0; k<P_BASIS_FUNCTIONS; k++ ) { 
				Be[k] = Be[k] + fac * NIp[n][k] * gausspoints[n].Fp;
			}
			
		}
		
		/* combine body force with A.x */
		for( k=0; k<P_BASIS_FUNCTIONS; k++ ) { 
			Fe[k] = Fe[k] - Be[k];
		}
		
		ierr = DMDASetValuesLocalStencil_AddValues_Stokes_Pressure(LA_Rp,p_el_lidx,Fe);CHKERRQ(ierr);
	}
	PetscGetTime(&t1);
	//	PetscPrintf(PETSC_COMM_WORLD,"Assemble Rp, = %1.4e (sec)\n",t1-t0);
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "FormJacobian_Stokes_QCoords"
PetscErrorCode FormJacobian_Stokes_QCoords(SNES snes,Vec X,Mat *A,Mat *B,MatStructure *mstr,void *ctx)
{
  pTatinCtx   user = (pTatinCtx)ctx;
  DM                dau,dap,dac;
  PetscScalar       *LA_u,*LA_p,*LA_c;
	IS                *is;
  Vec               c,Uloc,Ploc,Cloc;
	PetscBool         is_mffd = PETSC_FALSE;
  PetscErrorCode    ierr;
	
  PetscFunctionBegin;
  ierr = DMCompositeGetEntries(user->pack,&dau,&dap);CHKERRQ(ierr);
  ierr = DMCompositeGetLocalVectors(user->pack,&Uloc,&Ploc);CHKERRQ(ierr);
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA(dau,&dac);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(dau,&c);CHKERRQ(ierr);
	ierr = DMGetLocalVector(dac,&Cloc);CHKERRQ(ierr);
	ierr = DMGlobalToLocalBegin(dac,c,INSERT_VALUES,Cloc);CHKERRQ(ierr);
	ierr = DMGlobalToLocalEnd  (dac,c,INSERT_VALUES,Cloc);CHKERRQ(ierr);
	
	ierr = DMCompositeScatter(user->pack,X,Uloc,Ploc);CHKERRQ(ierr);

	/* update coords, x = x + dt.v */
	ierr = VecAXPY(Cloc,1.0*user->dt,Uloc);CHKERRQ(ierr);

	ierr = VecGetArray(Cloc,&LA_c);CHKERRQ(ierr);	
	ierr = VecGetArray(Uloc,&LA_u);CHKERRQ(ierr);
	ierr = VecGetArray(Ploc,&LA_p);CHKERRQ(ierr);
	ierr = DMCompositeGetGlobalISs(user->pack,&is);CHKERRQ(ierr);
	
	/* Jacobian */
	ierr = pTatin_EvaluateRheologyNonlinearities(user,dau,LA_u,dap,LA_p);CHKERRQ(ierr);
//	ierr = pTatin_EvaluateRheologyNonlinearities_QCoords(user,dau,LA_u,dap,LA_p,dac,LA_c);CHKERRQ(ierr);
	
	ierr = PetscTypeCompare((PetscObject)(*A),MATMFFD,&is_mffd);CHKERRQ(ierr);
	if (!is_mffd) {
		Mat Auu,Aup,Apu;
		
		ierr = MatGetSubMatrix(*A,is[0],is[0],MAT_INITIAL_MATRIX,&Auu);CHKERRQ(ierr);
		ierr = MatGetSubMatrix(*A,is[0],is[1],MAT_INITIAL_MATRIX,&Aup);CHKERRQ(ierr);
		ierr = MatGetSubMatrix(*A,is[1],is[0],MAT_INITIAL_MATRIX,&Apu);CHKERRQ(ierr);
		
		ierr = AssembleStokesA11_QCoords_Q2(user,dau,LA_u,dap,LA_p,dac,LA_c,Auu);CHKERRQ(ierr);
		ierr = AssembleStokesA12_QCoords_Q2(user,dau,LA_u,dap,LA_p,dac,LA_c,Aup);CHKERRQ(ierr);
		ierr = AssembleStokesA21_QCoords_Q2(user,dau,LA_u,dap,LA_p,dac,LA_c,Apu);CHKERRQ(ierr);
		
		ierr = MatDestroy(&Auu);CHKERRQ(ierr);
		ierr = MatDestroy(&Aup);CHKERRQ(ierr);
		ierr = MatDestroy(&Apu);CHKERRQ(ierr);
	}
	
	ierr = MatAssemblyBegin(*A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd  (*A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	
	/* preconditioner for Jacobian */
	{
		Mat Buu,Bup,Bpu,Bpp;
		PetscBool upper,lower,full;
		
		upper = lower = full = PETSC_FALSE;
		ierr = PetscOptionsGetBool(PETSC_NULL,"-dmcomposite_pc_upper",&upper,PETSC_NULL);CHKERRQ(ierr);
		ierr = PetscOptionsGetBool(PETSC_NULL,"-dmcomposite_pc_lower",&lower,PETSC_NULL);CHKERRQ(ierr);
		ierr = PetscOptionsGetBool(PETSC_NULL,"-dmcomposite_pc_full",&full,PETSC_NULL);CHKERRQ(ierr);
		if (full){ upper = PETSC_TRUE; lower = PETSC_TRUE; }
		
		ierr = MatGetSubMatrix(*B,is[0],is[0],MAT_INITIAL_MATRIX,&Buu);CHKERRQ(ierr);
		ierr = MatGetSubMatrix(*B,is[0],is[1],MAT_INITIAL_MATRIX,&Bup);CHKERRQ(ierr);
		ierr = MatGetSubMatrix(*B,is[1],is[0],MAT_INITIAL_MATRIX,&Bpu);CHKERRQ(ierr);
		ierr = MatGetSubMatrix(*B,is[1],is[1],MAT_INITIAL_MATRIX,&Bpp);CHKERRQ(ierr);
		
		if (upper) { ierr = AssembleStokesA12_QCoords_Q2(user,dau,LA_u,dap,LA_p,dac,LA_c,Bup);CHKERRQ(ierr); }
		if (lower) { ierr = AssembleStokesA21_QCoords_Q2(user,dau,LA_u,dap,LA_p,dac,LA_c,Bpu);CHKERRQ(ierr); }
		
		ierr = AssembleStokesA11_QCoords_Q2(user,dau,LA_u,dap,LA_p,dac,LA_c,Buu);CHKERRQ(ierr);
		ierr = AssembleStokesB22_QCoords_P1(user,dau,LA_u,dap,LA_p,dac,LA_c,Bpp);CHKERRQ(ierr);
		
		ierr = MatDestroy(&Buu);CHKERRQ(ierr);
		ierr = MatDestroy(&Bup);CHKERRQ(ierr);
		ierr = MatDestroy(&Bpu);CHKERRQ(ierr);
		ierr = MatDestroy(&Bpp);CHKERRQ(ierr);		
  }
  ierr = MatAssemblyBegin(*B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd  (*B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	
	*mstr = DIFFERENT_NONZERO_PATTERN;
	
	/* clean up */
	ierr = ISDestroy(&is[0]);CHKERRQ(ierr);
	ierr = ISDestroy(&is[1]);CHKERRQ(ierr);
	ierr = PetscFree(is);CHKERRQ(ierr);
	
	ierr = VecRestoreArray(Cloc,&LA_c);CHKERRQ(ierr);
	ierr = VecRestoreArray(Uloc,&LA_u);CHKERRQ(ierr);
	ierr = VecRestoreArray(Ploc,&LA_p);CHKERRQ(ierr);
	
	ierr = DMRestoreLocalVector(dac,&Cloc);CHKERRQ(ierr);
  ierr = DMCompositeRestoreLocalVectors(user->pack,&Uloc,&Ploc);CHKERRQ(ierr);
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "AssembleStokesA11_QCoords_Q2"
PetscErrorCode AssembleStokesA11_QCoords_Q2(pTatinCtx user,DM dau,PetscScalar LA_u[],DM dap,PetscScalar LA_p[],DM dac,PetscScalar LA_c[],Mat Auu)
{	
	PetscErrorCode ierr;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
	PetscInt nel,nen,e,n;
	const PetscInt *elnidx;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D],el_eta[MAX_QUAD_PNTS];
	PetscScalar Ae[Q2_NODES_PER_EL_2D*2 * Q2_NODES_PER_EL_2D*2];
	PetscInt *gidx,elgidx[2*Q2_NODES_PER_EL_2D],elgidx_bc[2*Q2_NODES_PER_EL_2D];
	PetscInt nbcs,i;
	const PetscInt *ubcidx;
	Vec diag;
	PetscScalar *LA_diag;
	PetscLogDouble t0,t1;
	BCList u_bclist = user->u_bclist;
	PetscInt Len_local, *gidx_bclocal;
	GaussPointCoefficientsStokes *gausspoints;
	
	PetscFunctionBegin;
	
	/* zero entries */
	ierr = MatZeroEntries(Auu);CHKERRQ(ierr);
	
	/* quadrature */
	ngp       = user->Q->ngp;
	gp_xi     = user->Q->xi;
	gp_weight = user->Q->weight;
	
	ierr = DMDAGetGlobalIndices(dau,0,&gidx);CHKERRQ(ierr);
	ierr = BCListGetDofIdx(u_bclist,0,0,&Len_local,&gidx_bclocal);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen,&elnidx);CHKERRQ(ierr);
	PetscGetTime(&t0);
	for (e=0;e<nel;e++) {
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx[nen*e],LA_c);CHKERRQ(ierr);
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		
		/* initialise element stiffness matrix */
		PetscMemzero( Ae, sizeof(PetscScalar)* Q2_NODES_PER_EL_2D*2 * Q2_NODES_PER_EL_2D*2 );
		
		/* evaluate the viscosity */
		for (n=0; n<ngp; n++) {
			el_eta[n] = gausspoints[n].eta;
		}
		
		/* form element stiffness matrix */
		AElement_Stokes_A11_Q2_2D( Ae, elcoords, el_eta, ngp,gp_xi,gp_weight );

		/* insert element matrix into global matrix */
		ierr = GetElementEqnIndicesQ2((PetscInt*)&elnidx[nen*e],gidx,elgidx);CHKERRQ(ierr);
		ierr = GetElementEqnIndicesQ2((PetscInt*)&elnidx[nen*e],gidx_bclocal,elgidx_bc);CHKERRQ(ierr);
		
		ierr = MatSetValues( Auu, Q2_NODES_PER_EL_2D*2,elgidx_bc, Q2_NODES_PER_EL_2D*2,elgidx_bc, Ae, ADD_VALUES );CHKERRQ(ierr);
	}
	ierr = MatAssemblyBegin(Auu, MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(Auu, MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
	PetscGetTime(&t1);
	//	PetscPrintf(PETSC_COMM_WORLD,"Assemble A11, Q2 = %1.4e (sec)\n",t1-t0);
	
	/* works, but this requires communication */
	{
		PetscInt Len_ghost,*gidx_bcghost;
		PetscBool is_dirich;
		PetscInt nproc,*procs,*mapL,**mapIdx;
		ISLocalToGlobalMapping map;
		PetscMPIInt rank;
		
		MPI_Comm_rank( ((PetscObject)dau)->comm,&rank);
		ierr = BCListGetDofIdx(u_bclist,0,0,&Len_ghost,&gidx_bcghost);CHKERRQ(ierr);
		
		ierr = DMGetLocalToGlobalMapping(dau,&map);CHKERRQ(ierr);
		ierr = ISLocalToGlobalMappingGetInfo(map,&nproc,&procs,&mapL,&mapIdx);CHKERRQ(ierr);
		
		if (Len_ghost!=mapL[rank]) {
			//			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"map indices are fucked up");
		}
		for (i=0; i<Len_ghost; i++) {
			ierr = BCListIsDirichlet(gidx_bcghost[i],&is_dirich);CHKERRQ(ierr);
			if (is_dirich) {
				ierr = MatSetValueLocal(Auu,i,i,1.0,INSERT_VALUES);CHKERRQ(ierr);
			}
		}
	}
	
	ierr = MatAssemblyBegin(Auu, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(Auu, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "AssembleStokesA12_QCoords_Q2"
PetscErrorCode AssembleStokesA12_QCoords_Q2(pTatinCtx user,DM dau,PetscScalar LA_u[],DM dap,PetscScalar LA_p[],DM dac,PetscScalar LA_c[],Mat A)
{	
	PetscErrorCode ierr;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
	PetscInt e,n;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar Ae[Q2_NODES_PER_EL_2D*2 * P_BASIS_FUNCTIONS];
	PetscLogDouble t0,t1;
	/* velocity */
	PetscInt u_nel,u_nen; /* num vel elements, nodes per ele */
	BCList u_bclist = user->u_bclist;
	const PetscInt *u_elnidx; /* element -> node indices */
	PetscInt *u_gidx, u_elgidx[2*Q2_NODES_PER_EL_2D]; /* global indices and element counterpart (ghosted) */
	PetscInt u_Len_local, *u_gidx_bclocal, u_elgidx_bc[2*Q2_NODES_PER_EL_2D]; /* global indices and element counterpart with bc's masked out (ghosted) */
	/* pressure */
	PetscInt p_nel,p_nen;
	BCList p_bclist = user->p_bclist; /* this will be NULL */
	const PetscInt *p_elnidx;
	PetscInt *p_gidx, p_elgidx[P_BASIS_FUNCTIONS];
	
	PetscFunctionBegin;
	
	/* zero entries */
	ierr = MatZeroEntries(A);CHKERRQ(ierr);
	
	/* quadrature */
	//	QuadratureCreateGauss_3pnt_2D(&ngp,gp_xi,gp_weight);
	ngp       = user->Q->ngp;
	gp_xi     = user->Q->xi;
	gp_weight = user->Q->weight;
	
	/* setup eqnums */
	ierr = DMDAGetGlobalIndices(dau,0,&u_gidx);CHKERRQ(ierr);
	ierr = BCListGetDofIdx(u_bclist,0,0,&u_Len_local,&u_gidx_bclocal);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dau,&u_nel,&u_nen,&u_elnidx);CHKERRQ(ierr);
	
	ierr = DMDAGetGlobalIndices(dap,0,&p_gidx);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&p_nel,&p_nen,&p_elnidx);CHKERRQ(ierr);
	
	PetscGetTime(&t0);
	for (e=0;e<u_nel;e++) {
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&u_elnidx[u_nen*e],LA_c);CHKERRQ(ierr);
		
		/* initialise element stiffness matrix */
		PetscMemzero( Ae, sizeof(PetscScalar)* Q2_NODES_PER_EL_2D*2 * P_BASIS_FUNCTIONS );
		
		/* form element stiffness matrix */
		AElement_Stokes_A12_Q2_2D( Ae, elcoords, ngp,gp_xi,gp_weight );

		/* insert element matrix into global matrix */
		ierr = GetElementEqnIndicesQ2((PetscInt*)&u_elnidx[u_nen*e],u_gidx_bclocal,u_elgidx_bc);CHKERRQ(ierr);
		ierr = GetElementEqnIndicesPressure(P_BASIS_FUNCTIONS,(PetscInt*)&p_elnidx[p_nen*e],p_gidx,p_elgidx);CHKERRQ(ierr);
		ierr = MatSetValues( A, Q2_NODES_PER_EL_2D*2,u_elgidx_bc, P_BASIS_FUNCTIONS,p_elgidx, Ae, ADD_VALUES );CHKERRQ(ierr);
	}
	ierr = MatAssemblyBegin(A, MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(A, MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
	PetscGetTime(&t1);
	//	PetscPrintf(PETSC_COMM_WORLD,"Assemble A12, Q2 = %1.4e (sec)\n",t1-t0);
	
	/* no bc modifications */
	ierr = MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "AssembleStokesA21_QCoords_Q2"
PetscErrorCode AssembleStokesA21_QCoords_Q2(pTatinCtx user,DM dau,PetscScalar LA_u[],DM dap,PetscScalar LA_p[],DM dac,PetscScalar LA_c[],Mat A)
{	
	PetscErrorCode ierr;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
	PetscInt e,n;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar Ae[Q2_NODES_PER_EL_2D*2 * P_BASIS_FUNCTIONS];
	PetscLogDouble t0,t1;
	/* velocity */
	PetscInt u_nel,u_nen; /* num vel elements, nodes per ele */
	BCList u_bclist = user->u_bclist;
	const PetscInt *u_elnidx; /* element -> node indices */
	PetscInt *u_gidx, u_elgidx[2*Q2_NODES_PER_EL_2D]; /* global indices and element counterpart (ghosted) */
	PetscInt u_Len_local, *u_gidx_bclocal, u_elgidx_bc[2*Q2_NODES_PER_EL_2D]; /* global indices and element counterpart with bc's masked out (ghosted) */
	/* pressure */
	PetscInt p_nel,p_nen;
	BCList p_bclist = user->p_bclist; /* this will be NULL */
	const PetscInt *p_elnidx;
	PetscInt *p_gidx, p_elgidx[P_BASIS_FUNCTIONS];
	
	PetscFunctionBegin;
	
	/* zero entries */
	ierr = MatZeroEntries(A);CHKERRQ(ierr);
	
	/* quadrature */
	//	QuadratureCreateGauss_3pnt_2D(&ngp,gp_xi,gp_weight);
	ngp       = user->Q->ngp;
	gp_xi     = user->Q->xi;
	gp_weight = user->Q->weight;
	
	/* setup eqnums */
	ierr = DMDAGetGlobalIndices(dau,0,&u_gidx);CHKERRQ(ierr);
	ierr = BCListGetDofIdx(u_bclist,0,0,&u_Len_local,&u_gidx_bclocal);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dau,&u_nel,&u_nen,&u_elnidx);CHKERRQ(ierr);
	
	ierr = DMDAGetGlobalIndices(dap,0,&p_gidx);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&p_nel,&p_nen,&p_elnidx);CHKERRQ(ierr);
	
	PetscGetTime(&t0);
	for (e=0;e<u_nel;e++) {
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&u_elnidx[u_nen*e],LA_c);CHKERRQ(ierr);
		
		/* initialise element stiffness matrix */
		PetscMemzero( Ae, sizeof(PetscScalar)* Q2_NODES_PER_EL_2D*2 * P_BASIS_FUNCTIONS );
		
		/* form element stiffness matrix */
		AElement_Stokes_A21_Q2_2D( Ae, elcoords, ngp,gp_xi,gp_weight );
		
		/* insert element matrix into global matrix */
		ierr = GetElementEqnIndicesQ2((PetscInt*)&u_elnidx[u_nen*e],u_gidx_bclocal,u_elgidx_bc);CHKERRQ(ierr);
		ierr = GetElementEqnIndicesPressure(P_BASIS_FUNCTIONS,(PetscInt*)&p_elnidx[p_nen*e],p_gidx,p_elgidx);CHKERRQ(ierr);
		ierr = MatSetValues( A, P_BASIS_FUNCTIONS,p_elgidx, Q2_NODES_PER_EL_2D*2,u_elgidx_bc, Ae, ADD_VALUES );CHKERRQ(ierr);
	}
	ierr = MatAssemblyBegin(A, MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(A, MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
	PetscGetTime(&t1);
	//	PetscPrintf(PETSC_COMM_WORLD,"Assemble A21, Q2 = %1.4e (sec)\n",t1-t0);
	
	/* no bc modifications */
	ierr = MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "AssembleStokesB22_QCoords_P1"
PetscErrorCode AssembleStokesB22_QCoords_P1(pTatinCtx user,DM dau,PetscScalar LA_u[],DM dap,PetscScalar LA_p[],DM dac,PetscScalar LA_c[],Mat Bpp)
{	
	PetscErrorCode ierr;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
	PetscInt nel_u,nel_p,nen_u,nen_p,e,n;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D],el_eta[MAX_QUAD_PNTS];
	PetscScalar Ae[P_BASIS_FUNCTIONS * P_BASIS_FUNCTIONS];
	PetscInt *gidx_u,*gidx_p,elgidx_p[P_BASIS_FUNCTIONS];
	PetscInt nbcs,i;
	const PetscInt *ubcidx;
	Vec diag;
	PetscScalar *LA_diag;
	PetscLogDouble t0,t1;
	GaussPointCoefficientsStokes *gausspoints;
	PetscFunctionBegin;
	
	/* zero entries */
	ierr = MatZeroEntries(Bpp);CHKERRQ(ierr);
	
	/* quadrature */
	ngp       = user->Q->ngp;
	gp_xi     = user->Q->xi;
	gp_weight = user->Q->weight;
	
	ierr = DMDAGetGlobalIndices(dau,0,&gidx_u);CHKERRQ(ierr);
	ierr = DMDAGetGlobalIndices(dap,0,&gidx_p);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel_u,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel_p,&nen_p,&elnidx_p);CHKERRQ(ierr);
	if (nel_u	!= nel_p) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Velocity DA is not compatible with pressure DA\n");
	}
	
	PetscGetTime(&t0);
	for (e=0;e<nel_u;e++) {
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_c);CHKERRQ(ierr);
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		
		/* initialise element stiffness matrix */
		PetscMemzero( Ae, sizeof(PetscScalar)* P_BASIS_FUNCTIONS * P_BASIS_FUNCTIONS );
		
		/* evaluate the viscosity */
		for (n=0; n<ngp; n++) {
			el_eta[n] = gausspoints[n].eta;
		}
		
		/* form element stiffness matrix */
		AElement_Stokes_B22_P1_2D( Ae, elcoords, el_eta, ngp,gp_xi,gp_weight );
		
		/* insert element matrix into global matrix */
		ierr = GetElementEqnIndicesPressure(nen_p,(PetscInt*)&elnidx_p[nen_p*e],gidx_p,elgidx_p);CHKERRQ(ierr);
		
		ierr = MatSetValues( Bpp, P_BASIS_FUNCTIONS,elgidx_p, P_BASIS_FUNCTIONS,elgidx_p, Ae, ADD_VALUES );CHKERRQ(ierr);
	}
	ierr = MatAssemblyBegin(Bpp, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(Bpp, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	PetscGetTime(&t1);
	//	PetscPrintf(PETSC_COMM_WORLD,"Assemble B22, P1 = %1.4e (sec)\n",t1-t0);
	
	PetscFunctionReturn(0);
}

