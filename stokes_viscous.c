/*
 *  stokes_viscous.c
 *  
 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"
#include "rheology.h"


#undef __FUNCT__
#define __FUNCT__ "StokesComputeViscosity"
PetscErrorCode StokesComputeViscosity( 
																			PetscScalar el_coords[], 
																			PetscScalar el_vel[], PetscScalar el_pressure[], 
																			PetscScalar xi[], PetscScalar NIu[], PetscScalar *GNXu[], PetscScalar NIp[],
																			void *data, /* place holder for user data (additional marker properties...) */
																			PetscScalar *eta )
{
	PetscScalar vx_p,vy_p,pressure_p,xpos[2];
	PetscScalar du_dx, du_dy, dv_dx, dv_dy;
	PetscScalar e1,e2, sri2, D[2][2], dilitation_e;
	PetscInt ii,jj,ki,kj;
	PetscScalar eta_0, eta_eff;
	
	
	PetscFunctionBegin;
	/* compute position */
	/* compute velocity */
	xpos[0] = xpos[1] = 0.0;
	vx_p = vy_p = 0.0;
	for (ii=0; ii<U_BASIS_FUNCTIONS; ii++) {
		PetscScalar XX = el_coords[2*ii  ];
		PetscScalar YY = el_coords[2*ii+1];
		PetscScalar vx = el_vel[2*ii  ];
		PetscScalar vy = el_vel[2*ii+1];
		
		xpos[0] += NIu[ii] * XX;
		xpos[1] += NIu[ii] * YY;
		
		vx_p += NIu[ii] * vx;
		vy_p += NIu[ii] * vy;
	}
	
	/* compute pressure */
	pressure_p = 0.0;
	for (jj=0; jj<P_BASIS_FUNCTIONS; jj++) {
		pressure_p += NIp[jj] * el_pressure[jj];
	}
	
	/* compute du/dx, du/dy, dv/dx, dvdy */
	du_dx = 0.0;
	du_dy = 0.0;
	dv_dx = 0.0;
	dv_dy = 0.0;
	for( ii=0; ii<U_BASIS_FUNCTIONS; ii++ ) {
		PetscScalar vx = el_vel[2*ii  ];
		PetscScalar vy = el_vel[2*ii+1];
		
		du_dx = du_dx + GNXu[0][ii] * vx ;
		du_dy = du_dy + GNXu[1][ii] * vx ;
		dv_dx = dv_dx + GNXu[0][ii] * vy ;
		dv_dy = dv_dy + GNXu[1][ii] * vy ;
	}
	
	/* compute strain rate */
	D[0][0] = 0.5*( du_dx + du_dx );
	D[0][1] = 0.5*( du_dy + dv_dx );
	D[1][0] = 0.5*( dv_dx + du_dy );
	D[1][1] = 0.5*( dv_dy + dv_dy );
	
	dilitation_e = 0.5 * ( D[0][0] + D[1][1] );
	
	/* Enforce D[][] to be deviatoric ? */
	D[0][0] = D[0][0] - dilitation_e;
	D[1][1] = D[1][1] - dilitation_e;
	
	//pressure_p = pressure_p + dilitation_e;
	
	/* compute strain rate invariants */
	e1 = e2 = 0.0;
	for( ki=0; ki<2; ki++ ) {
		e1 = e1 + D[ki][ki];
		for( kj=0; kj<2; kj++ ) {
			e2 = e2 + D[ki][kj]*D[ki][kj];
		}
	}
	sri2 = 0.5 * sqrt( e2 );
	
	
	/* evaluate the viscosity */
	eta_0 = 1.0e3;
	eta_eff = eta_0;
	
	/* set result in pointer */
	*eta = eta_eff;
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "UpdateRheologyQuadratureStokes_EXAMPLE"
PetscErrorCode UpdateRheologyQuadratureStokes_EXAMPLE(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[])
{	
	PetscErrorCode ierr;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
	PetscScalar GNIu[MAX_QUAD_PNTS][2][Q2_NODES_PER_EL_2D],NIp[MAX_QUAD_PNTS][P_BASIS_FUNCTIONS];
	PetscScalar NIu[MAX_QUAD_PNTS][U_BASIS_FUNCTIONS];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen_u,nen_p,e,n,k;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D],el_eta[MAX_QUAD_PNTS];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	PetscScalar Fe[Q2_NODES_PER_EL_2D*2],Be[Q2_NODES_PER_EL_2D*2];
	PetscInt vel_el_lidx[U_BASIS_FUNCTIONS*2];
	PetscInt *gidx,elgidx[2*Q2_NODES_PER_EL_2D];
	PetscInt nbcs,i,j;
	const PetscInt *ubcidx;
	Vec diag;
	PetscScalar *LA_diag;
	PetscLogDouble t0,t1;
	BCList u_bclist = user->u_bclist;
	GaussPointCoefficientsStokes *gausspoints;
	PetscScalar  d1,exx_gp,eyy_gp,exy_gp,eta_gp;
	PetscScalar  sxx_gp,syy_gp,sxy_gp,u_gp,v_gp,trace_p,pressure_gp;
	static int beenHere = 1;

	PetscFunctionBegin;
	/* quadrature */
	/*QuadratureCreateGauss_3pnt_2D(&ngp,gp_xi,gp_weight);*/
	ngp       = user->Q->ngp;
	gp_xi     = user->Q->xi;
	gp_weight = user->Q->weight;
	for (k=0; k<ngp; k++) {
		PetscScalar *xip = &gp_xi[2*k];
		
		PTatinConstructNI_Q2_2D(xip,NIu[k]);
		PTatinConstructGNI_Q2_2D(xip,GNIu[k]);
	}
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetGlobalIndices(dau,0,&gidx);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
	PetscGetTime(&t0);
	for (e=0;e<nel;e++) {
		
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		ierr = StokesVelocity_GetElementLocalIndices(vel_el_lidx,(PetscInt*)&elnidx_u[nen_u*e]);CHKERRQ(ierr);
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],u);CHKERRQ(ierr);
		ierr = DMDAGetScalarElementField_2D(elp,nen_p,(PetscInt*)&elnidx_p[nen_p*e],p);CHKERRQ(ierr);
		
		/* get element velocity */
		for( i=0; i<Q2_NODES_PER_EL_2D; i++ ) {
			ux[i] = elu[2*i  ];
			uy[i] = elu[2*i+1];
			
			xc[i] = elcoords[2*i  ];
			yc[i] = elcoords[2*i+1];
		}
		/* get element pressure */
		for (n=0; n<ngp; n++) {
			PetscScalar *xip = &gp_xi[2*n];
			
			PTatinConstructNI_Q2_2D(xip,NIu[n]);
			ConstructNi_pressure(xip,elcoords,NIp[n]);
		}
		
		for (n=0; n<ngp; n++) {
			PetscScalar  J[2][2], iJ[2][2];
			PetscScalar J_p,ojp;
			
			/* coord transformation */
			for( i=0; i<2; i++ ) {
				for( j=0; j<2; j++ ) { J[i][j] = 0.0; }
			}
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				J[0][0] += GNIu[n][0][k] * xc[k] ;
				J[0][1] += GNIu[n][0][k] * yc[k] ;
				
				J[1][0] += GNIu[n][1][k] * xc[k] ;
				J[1][1] += GNIu[n][1][k] * yc[k] ;
			}
			J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
			ojp = 1.0/J_p;
			iJ[0][0] =  J[1][1]*ojp;
			iJ[0][1] = -J[0][1]*ojp;
			iJ[1][0] = -J[1][0]*ojp;
			iJ[1][1] =  J[0][0]*ojp;
			
			/* global derivs at each quadrature point */
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				nx[k] = iJ[0][0]*GNIu[n][0][k] + iJ[0][1]*GNIu[n][1][k];
				ny[k] = iJ[1][0]*GNIu[n][0][k] + iJ[1][1]*GNIu[n][1][k];
			}
			
			/* u,v and strain rate (e= B u) at gp */
			u_gp = v_gp = 0.0;
			exx_gp = eyy_gp = exy_gp = 0.0;
			for( i=0; i<U_BASIS_FUNCTIONS; i++ ) {
				u_gp   += NIu[n][i] * ux[i];
				v_gp   += NIu[n][i] * uy[i];
				
				exx_gp += nx[i] * ux[i];
				eyy_gp += ny[i] * uy[i];
				exy_gp += ny[i] * ux[i] + nx[i] * uy[i];
			}
			exy_gp = 0.5 * exy_gp;
			
			/* constituive */
			eta_gp = 1.0;
			d1 = 2.0 * eta_gp;
			
			/* stress */
			sxx_gp = d1 * exx_gp;
			syy_gp = d1 * eyy_gp;
			sxy_gp = d1 * exy_gp;
			
			trace_p = 0.5 * ( sxx_gp + syy_gp );
			sxx_gp = sxx_gp - trace_p;
			syy_gp = syy_gp - trace_p;
			
			/* pressure at gp */
			pressure_gp = 0.0;
			for( i=0; i<P_BASIS_FUNCTIONS; i++ ) {
				pressure_gp += NIp[n][i] * elp[i];
			}
			//pressure_gp = pressure_gp + trace_p;
			
			
			/* update the viscosity, fu_x, fu_y, fp  */
			
			
			
#if 0
			{ // power law
				PetscScalar exponent,eta_0,e_II,a,b,eta_eff;
				PetscScalar alpha_m,eta_max,eta_eff_continuation,eta_norm;
				
				
				exponent = 7.5;
				eta_0    = 1.0;
				
				alpha_m = (double)(user->continuation_m)/( (double)(user->continuation_M) );
				// continuation on the exponent //
				exponent = pow( exponent, alpha_m );
				
				e_II = 0.5 * sqrt( exx_gp*exx_gp + eyy_gp*eyy_gp + 2.0*exy_gp*exy_gp );
				a = pow( 2.0*eta_0, 1.0/exponent );
				if (fabs(e_II) < 1.0e-32) {
					b = 1.0;
				} else {
					b = pow( e_II, (1.0-exponent)/exponent );
				}
				eta_eff = 0.5 * a * b;
				
				eta_norm = eta_eff;
				
				// continuation on the viscosity //				
				//				eta_eff_continuation = pow( eta_eff, alpha_m );
				//				eta_max = 1.0;
				//	  		eta_norm = eta_eff_continuation / eta_max;
				
				//				printf("eta_eff = %1.4e, alpha_(%d) = %1.4e, eta_eff_continuation = %1.4e, eta_norm = %1.4e \n", 
				//							 eta_eff,user->continuation_m,alpha_m, eta_eff_continuation,eta_norm );
				
				gausspoints[n].eta   = eta_norm;
			}
#endif			
			
#if 0
			{ // bingham
				PetscScalar epsilon = 1.0e-5;
				PetscScalar tau_s   = 0.0;
				PetscScalar eta_0   = 1.0;
				PetscScalar eta_eff,abs_Du;
				
				abs_Du = exx_gp*exx_gp + eyy_gp*eyy_gp + 2.0*exy_gp*exy_gp;
				abs_Du = sqrt( 0.5 * abs_Du );
				
				eta_eff = 2.0 * eta_0 + tau_s / sqrt( epsilon*epsilon + abs_Du*abs_Du );
				
				gausspoints[n].eta = eta_eff;
			}
#endif
			
#if 0
			{ // made up shit
				PetscScalar eta_eff = 1.0 + sqrt(u_gp*u_gp) + sqrt(v_gp*v_gp) + fabs(pressure_gp);
				
				gausspoints[n].eta = eta_eff;
			}
#endif
			
			
			{ 				// continuation on the viscosity //				
				PetscScalar alpha_m,eta_eff_continuation;
				
				alpha_m = (double)(user->continuation_m)/( (double)(user->continuation_M) );
				
				eta_eff_continuation = pow( gausspoints[n].eta_ref, alpha_m );
				
				gausspoints[n].eta = eta_eff_continuation;				
			}
			
			
			//			if (gausspoints[n].eta >1.0e4) {
			//				gausspoints[n].eta = 1.0e4;
			//			}
			
			//			gausspoints[n].eta   = 1.0;
			//			gausspoints[n].Fu[0] = 0.0;
			//			gausspoints[n].Fu[1] = 0.0;
			//			gausspoints[n].Fp    = 0.0;
			
		}
	}
	
	PetscGetTime(&t1);
	
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	beenHere++;
	
	PetscFunctionReturn(0);
}

/*

 This viscosity evaluator simply takes the value intialised on the gauss point.
 As an example, I show you how to fetch, u,v,p from the grid and how to compute
 the strain-rate, and stress. We don't do anything with the computed values - it's
 just to demonstrate how to obtain access to the fields.
*/
#undef __FUNCT__
#define __FUNCT__ "EvaluateRheologyNonlinearitiesQuadratureStokes_Viscous"
PetscErrorCode EvaluateRheologyNonlinearitiesQuadratureStokes_Viscous(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[])
{	
	PetscErrorCode ierr;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
	PetscScalar GNIu[MAX_QUAD_PNTS][2][Q2_NODES_PER_EL_2D],NIp[MAX_QUAD_PNTS][P_BASIS_FUNCTIONS];
	PetscScalar NIu[MAX_QUAD_PNTS][U_BASIS_FUNCTIONS];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen_u,nen_p,e,n,k;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D],el_eta[MAX_QUAD_PNTS];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	PetscScalar Fe[Q2_NODES_PER_EL_2D*2],Be[Q2_NODES_PER_EL_2D*2];
	PetscInt vel_el_lidx[U_BASIS_FUNCTIONS*2];
	PetscInt *gidx,elgidx[2*Q2_NODES_PER_EL_2D];
	PetscInt nbcs,i,j;
	const PetscInt *ubcidx;
	Vec diag;
	PetscScalar *LA_diag;
	PetscLogDouble t0,t1;
	BCList u_bclist = user->u_bclist;
	GaussPointCoefficientsStokes *gausspoints;
	PetscScalar  d1,exx_gp,eyy_gp,exy_gp,eta_gp;
	PetscScalar  sxx_gp,syy_gp,sxy_gp,u_gp,v_gp,trace_p,pressure_gp;
	PetscScalar min_eta,max_eta,min_eta_g,max_eta_g;

	PetscFunctionBegin;
	/* quadrature */
	/*QuadratureCreateGauss_3pnt_2D(&ngp,gp_xi,gp_weight);*/
	ngp       = user->Q->ngp;
	gp_xi     = user->Q->xi;
	gp_weight = user->Q->weight;
	for (k=0; k<ngp; k++) {
		PetscScalar *xip = &gp_xi[2*k];
		
		PTatinConstructNI_Q2_2D(xip,NIu[k]);
		PTatinConstructGNI_Q2_2D(xip,GNIu[k]);
	}
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetGlobalIndices(dau,0,&gidx);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
	min_eta = 1.0e100;
	max_eta = 1.0e-100;
	PetscGetTime(&t0);
	for (e=0;e<nel;e++) {
		
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		ierr = StokesVelocity_GetElementLocalIndices(vel_el_lidx,(PetscInt*)&elnidx_u[nen_u*e]);CHKERRQ(ierr);
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],u);CHKERRQ(ierr);
		ierr = DMDAGetScalarElementField_2D(elp,nen_p,(PetscInt*)&elnidx_p[nen_p*e],p);CHKERRQ(ierr);
		
		/* get element velocity */
		for( i=0; i<Q2_NODES_PER_EL_2D; i++ ) {
			ux[i] = elu[2*i  ];
			uy[i] = elu[2*i+1];
			
			xc[i] = elcoords[2*i  ];
			yc[i] = elcoords[2*i+1];
		}
		/* get element pressure */
		for (n=0; n<ngp; n++) {
			PetscScalar *xip = &gp_xi[2*n];
			
			PTatinConstructNI_Q2_2D(xip,NIu[n]);
			ConstructNi_pressure(xip,elcoords,NIp[n]);
		}
		
		for (n=0; n<ngp; n++) {
			PetscScalar  J[2][2], iJ[2][2];
			PetscScalar J_p,ojp;
			
			/* coord transformation */
			for( i=0; i<2; i++ ) {
				for( j=0; j<2; j++ ) { J[i][j] = 0.0; }
			}
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				J[0][0] += GNIu[n][0][k] * xc[k] ;
				J[0][1] += GNIu[n][0][k] * yc[k] ;
				
				J[1][0] += GNIu[n][1][k] * xc[k] ;
				J[1][1] += GNIu[n][1][k] * yc[k] ;
			}
			J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
			ojp = 1.0/J_p;
			iJ[0][0] =  J[1][1]*ojp;
			iJ[0][1] = -J[0][1]*ojp;
			iJ[1][0] = -J[1][0]*ojp;
			iJ[1][1] =  J[0][0]*ojp;
			
			/* global derivs at each quadrature point */
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				nx[k] = iJ[0][0]*GNIu[n][0][k] + iJ[0][1]*GNIu[n][1][k];
				ny[k] = iJ[1][0]*GNIu[n][0][k] + iJ[1][1]*GNIu[n][1][k];
			}
			
			/* u,v and strain rate (e= B u) at gp */
			u_gp = v_gp = 0.0;
			exx_gp = eyy_gp = exy_gp = 0.0;
			for( i=0; i<U_BASIS_FUNCTIONS; i++ ) {
				u_gp   += NIu[n][i] * ux[i];
				v_gp   += NIu[n][i] * uy[i];
				
				exx_gp += nx[i] * ux[i];
				eyy_gp += ny[i] * uy[i];
				exy_gp += ny[i] * ux[i] + nx[i] * uy[i];
			}
			exy_gp = 0.5 * exy_gp;
			
			/* constituive */
			eta_gp = gausspoints[n].eta;
			d1 = 2.0 * eta_gp;
			
			/* stress */
			sxx_gp = d1 * exx_gp;
			syy_gp = d1 * eyy_gp;
			sxy_gp = d1 * exy_gp;
			
			trace_p = 0.5 * ( sxx_gp + syy_gp );
			sxx_gp = sxx_gp - trace_p;
			syy_gp = syy_gp - trace_p;
			
			/* pressure at gp */
			pressure_gp = 0.0;
			for( i=0; i<P_BASIS_FUNCTIONS; i++ ) {
				pressure_gp += NIp[n][i] * elp[i];
			}
			pressure_gp = pressure_gp + trace_p;
			
			/* monitor bounds */
			if (gausspoints[n].eta > max_eta) { max_eta = gausspoints[n].eta; }
			if (gausspoints[n].eta < min_eta) { min_eta = gausspoints[n].eta; }
		}
	}
	
	PetscGetTime(&t1);
  MPI_Allreduce(&min_eta,&min_eta_g,1, MPI_DOUBLE, MPI_MIN, PETSC_COMM_WORLD);
  MPI_Allreduce(&max_eta,&max_eta_g,1, MPI_DOUBLE, MPI_MAX, PETSC_COMM_WORLD);

  PetscPrintf(PETSC_COMM_WORLD,"Update rheology (viscous): (min,max)_eta %1.2e,%1.2e; log10(max/min) %1.2e; cpu time %1.2e (sec)\n",
              min_eta_g, max_eta_g, log10(max_eta_g/min_eta_g), t1-t0 );
	//	PetscPrintf(PETSC_COMM_WORLD,"Update rheology: time = %1.4e (sec)\n",t1-t0);
	
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "EvaluateRheologyNonlinearitiesMarkers_Viscous"
PetscErrorCode EvaluateRheologyNonlinearitiesMarkers_Viscous(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[])
{	
	PetscErrorCode ierr;
	int         pidx,n_mp_points;
	DataBucket  db;
	DataField   PField_std, PField_stokes;
	PetscScalar GNIu[2][Q2_NODES_PER_EL_2D],NIp[P_BASIS_FUNCTIONS];
	PetscScalar NIu[U_BASIS_FUNCTIONS];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen_u,nen_p,e,i,j,k;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	PetscInt vel_el_lidx[U_BASIS_FUNCTIONS*2];
	PetscInt *gidx;
	PetscLogDouble t0,t1;
	PetscScalar  d1,exx_mp,eyy_mp,exy_mp,eta_mp;
	PetscScalar  sxx_mp,syy_mp,sxy_mp,u_mp,v_mp,trace_mp,pressure_mp;
	PetscScalar min_eta,max_eta,min_eta_g,max_eta_g;
	
	PetscFunctionBegin;

	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetGlobalIndices(dau,0,&gidx);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
	min_eta = 1.0e100;
	max_eta = 1.0e-100;
	PetscGetTime(&t0);
	
	
	/* marker loop */
	db = user->db;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));
	
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	for (pidx=0; pidx<n_mp_points; pidx++) {
		MPntStd     *material_point;
		MPntPStokes *mpprop_stokes;
		double      *position, *xip;
		int         wil;
		PetscScalar  J[2][2], iJ[2][2];
		PetscScalar J_p,ojp;
		
		DataFieldAccessPoint(PField_std,   pidx,(void**)&material_point);
		DataFieldAccessPoint(PField_stokes,pidx,(void**)&mpprop_stokes);
		
		MPntStdGetField_local_element_index(material_point,&wil);
		MPntStdGetField_global_coord(material_point,&position);
		MPntStdGetField_local_coord(material_point,&xip);
//		MPntStdGetField_phase_index(material_point,&phase_init);

		e = wil;
		
		/* get nodal properties for element e */
		ierr = StokesVelocity_GetElementLocalIndices(vel_el_lidx,(PetscInt*)&elnidx_u[nen_u*e]);CHKERRQ(ierr);
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],u);CHKERRQ(ierr);
		ierr = DMDAGetScalarElementField_2D(elp,nen_p,(PetscInt*)&elnidx_p[nen_p*e],p);CHKERRQ(ierr);
		
		/* get element velocity/coordinates in component form */
		for( i=0; i<Q2_NODES_PER_EL_2D; i++ ) {
			ux[i] = elu[2*i  ];
			uy[i] = elu[2*i+1];
			
			xc[i] = elcoords[2*i  ];
			yc[i] = elcoords[2*i+1];
		}
		
		
		PTatinConstructNI_Q2_2D(xip,NIu);
		PTatinConstructGNI_Q2_2D(xip,GNIu);
		ConstructNi_pressure(xip,elcoords,NIp);
		
		/* coord transformation */
		for( i=0; i<2; i++ ) {
			for( j=0; j<2; j++ ) { J[i][j] = 0.0; }
		}
		for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
			J[0][0] += GNIu[0][k] * xc[k] ;
			J[0][1] += GNIu[0][k] * yc[k] ;
			
			J[1][0] += GNIu[1][k] * xc[k] ;
			J[1][1] += GNIu[1][k] * yc[k] ;
		}
		J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
		ojp = 1.0/J_p;
		iJ[0][0] =  J[1][1]*ojp;
		iJ[0][1] = -J[0][1]*ojp;
		iJ[1][0] = -J[1][0]*ojp;
		iJ[1][1] =  J[0][0]*ojp;
		
		/* global derivs at each quadrature point */
		for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
			nx[k] = iJ[0][0]*GNIu[0][k] + iJ[0][1]*GNIu[1][k];
			ny[k] = iJ[1][0]*GNIu[0][k] + iJ[1][1]*GNIu[1][k];
		}
		
		/* u,v and strain rate (e= B u) at gp */
		u_mp = v_mp = 0.0;
		exx_mp = eyy_mp = exy_mp = 0.0;
		for( k=0; k<U_BASIS_FUNCTIONS; k++ ) {
			u_mp   += NIu[k] * ux[k];
			v_mp   += NIu[k] * uy[k];
			
			exx_mp += nx[k] * ux[k];
			eyy_mp += ny[k] * uy[k];
			exy_mp += ny[k] * ux[k] + nx[k] * uy[k];
		}
		exy_mp = 0.5 * exy_mp;
		
		/* constituive */
		eta_mp = mpprop_stokes->eta;
		d1 = 2.0 * eta_mp;
		
		/* stress */
		sxx_mp = d1 * exx_mp;
		syy_mp = d1 * eyy_mp;
		sxy_mp = d1 * exy_mp;
		
		trace_mp = 0.5 * ( sxx_mp + syy_mp );
		sxx_mp = sxx_mp - trace_mp;
		syy_mp = syy_mp - trace_mp;
		
		/* pressure at gp */
		pressure_mp = 0.0;
		for( k=0; k<P_BASIS_FUNCTIONS; k++ ) {
			pressure_mp += NIp[k] * elp[k];
		}
		pressure_mp = pressure_mp + trace_mp;
		
			
		/* update viscosity on marker */
		MPntPStokesSetField_eta_effective(mpprop_stokes,eta_mp);
		
		/* monitor bounds */
		if (eta_mp > max_eta) { max_eta = eta_mp; }
		if (eta_mp < min_eta) { min_eta = eta_mp; }
	}
	
	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokes);
		
	PetscGetTime(&t1);
  MPI_Allreduce(&min_eta,&min_eta_g,1, MPI_DOUBLE, MPI_MIN, PETSC_COMM_WORLD);
  MPI_Allreduce(&max_eta,&max_eta_g,1, MPI_DOUBLE, MPI_MAX, PETSC_COMM_WORLD);
	
  PetscPrintf(PETSC_COMM_WORLD,"Update rheology (viscous) [mpoint]: (min,max)_eta %1.2e,%1.2e; log10(max/min) %1.2e; cpu time %1.2e (sec)\n",
              min_eta_g, max_eta_g, log10(max_eta_g/min_eta_g), t1-t0 );
	
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}





#undef __FUNCT__
#define __FUNCT__ "pTatin_TkUpdateRheologyQuadratureStokes_Viscous"
PetscErrorCode pTatin_TkUpdateRheologyQuadratureStokes_Viscous(void)
{	
  PetscErrorCode ierr;
	PetscFunctionBegin;
  PetscFunctionReturn(0);
}