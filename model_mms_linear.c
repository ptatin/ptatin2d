/*
 
 Model Description:
 
 Input / command line parameters:
 
 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"
#include "ex43-solCx.h"

typedef struct _p_MMSBucket MMSBucket;
struct _p_MMSBucket {
	PetscBool remove_int_pressure;
	PetscBool has_gradients;
	void (*fp_EvaluateStokes2dMS_RUVP)(double*,double*);
	void (*fp_EvaluateStokes2dMS_eta)(double*,double*);
	void (*fp_EvaluateStokes2dMS_UVP)(double*,double*);
	void (*fp_EvaluateStokes2dMS_GradUVP)(double*,double*);
	void (*fp_EvaluateStokes2dMS_IntP)(double*);
	void (*fp_DefineStokes2DMS_ModelDomain)(pTatinCtx ctx);
};

/* ================= mms ================= */
// template
void EvaluateStokes2dMS_RUVP_X(double pos[],double Ru[])
{
	double x,y;
	
	x = pos[0];
	y = pos[1];
	Ru[0] = 0.0;
	Ru[1] = 0.0;
	Ru[2] = 0.0;
}
void EvaluateStokes2dMS_eta_X(double pos[],double *eta)
{
	double x,y;	

	x = pos[0];
	y = pos[1];
	*eta = 0.0;
}
void EvaluateStokes2dMS_UVP_X(double pos[],double X[])
{
	double x,y;

	x = pos[0];
	y = pos[1];
	X[0] = 0.0;
	X[1] = 0.0;
	X[2] = 0.0;
}
void EvaluateStokes2dMS_GradUVP_X(double pos[],double L[])
{
	double x,y;
	
	x = pos[0];
	y = pos[1];
	L[0] = 0.0;
	L[1] = 0.0;
	L[2] = 0.0;
	L[3] = 0.0;
	L[4] = 0.0;
	L[5] = 0.0;
}

void EvaluateStokes2dMS_IntP_X(double *intP)
{
	*intP = 0.0;
}

void Stokes2DMS_ModelDomain_X(pTatinCtx ctx)
{
}


/*
 stokes_uvp_ms1a.py
*/
void EvaluateStokes2dMS_RUVP_1(double pos[],double Ruvp[])
{
	double x,y;
	double alpha = 11.0;
	
	x = pos[0];
	y = pos[1];
	Ruvp[0] = -2*x - pow(M_PI,2)*(0.00028 + 2.0e-5*exp(11.0*x)*sin(y))*cos(M_PI*y)*sin(M_PI*x) + 0.00022*M_PI*cos(M_PI*x)*cos(M_PI*y)*exp(11.0*x)*sin(y);    /* Ru_x  - [stokes_expEta_1a] */
	Ruvp[1] = -2*y + pow(M_PI,2)*(0.00028 + 2.0e-5*exp(11.0*x)*sin(y))*cos(M_PI*x)*sin(M_PI*y) - 2.0e-5*M_PI*cos(y)*cos(M_PI*x)*cos(M_PI*y)*exp(11.0*x);    /* Ru_p  - [stokes_expEta_1a] */
	Ruvp[2] = 0;    /* Rp  - [stokes_expEta_1a] */
	
	Ruvp[0] = 1.0 * Ruvp[0];
	Ruvp[1] = 1.0 * Ruvp[1];
}
void EvaluateStokes2dMS_eta_1(double pos[],double *eta)
{
	double x,y;
	double alpha = 11.0;
	
	x = pos[0];
	y = pos[1];
	*eta = 0.00014 + 1.0e-5*exp(11.0*x)*sin(y);    /* viscosity - [stokes_expEta_1a] */
}
void EvaluateStokes2dMS_UVP_1(double pos[],double X[])
{
	double x,y;

	x = pos[0];
	y = pos[1];
	X[0] = cos(M_PI*y)*sin(M_PI*x);    /* velocity_x - [stokes_expEta_1a] */
	X[1] = -cos(M_PI*x)*sin(M_PI*y);    /* velocity_y - [stokes_expEta_1a] */
	X[2] = pow(x,2) + pow(y,2);    /* pressure - [stokes_expEta_1a] */
}
void EvaluateStokes2dMS_GradUVP_1(double pos[],double L[])
{
	double x,y;
	
	x = pos[0];
	y = pos[1];
	L[0] = M_PI*cos(M_PI*x)*cos(M_PI*y);    /* L_xx = du/dx - [stokes_expEta_1a] */
	L[1] = -M_PI*sin(M_PI*x)*sin(M_PI*y);    /* L_xy = du/dy - [stokes_expEta_1a] */
	L[2] = M_PI*sin(M_PI*x)*sin(M_PI*y);    /* L_yx = dv/dx - [stokes_expEta_1a] */
	L[3] = -M_PI*cos(M_PI*x)*cos(M_PI*y);    /* L_yy = dv/dy - [stokes_expEta_1a] */
	L[4] = 2*x;    /* dp/dx - [stokes_expEta_1a] */
	L[5] = 2*y;    /* p/dy - [stokes_expEta_1a] */
}

void EvaluateStokes2dMS_IntP_1(double *intP)
{
	*intP = 0.666666666666667;    /* \int P dV - [stokes_expEta_1a] */
}

void Stokes2DMS_ModelDomain_1(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	ierr = DMDASetUniformCoordinates(ctx->dav,0.0,1.0, 0.0,1.0, 0.0,0.0);CHKERRQ(ierr);
}

/* model 2 - stokes_uvp_ms2_sinker.py */
void EvaluateStokes2dMS_RUVP_2(double pos[],double Ruvp[])
{
	double x,y;
	double eta_min = -7.56e-03 + 1.0e-6;
	double eta_max = 10.0;
	double f_ETA_m = 16.0; //160;
	double f_VEL_m = 3.0; // 30.0;
	double f_P_m   = 30.0; //3000.0;
	
	x = pos[0];
	y = pos[1];
	{
		Ruvp[0] = (2.0*eta_min + 2.0*eta_max*(0.5 + 0.5*atan(f_ETA_m*(0.5 + x))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 + y))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 - x))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 - y))/atan(1.5*f_ETA_m)))*(-0.013*(0.5*pow(f_VEL_m,2)*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + x),2))*(1 + pow(f_VEL_m,2)*pow((0.5 - x),2))*pow(atan(1.5*f_VEL_m),2)) + 0.5*pow(f_VEL_m,3)*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))*(1.0 + 2*x)/(pow((1 + pow(f_VEL_m,2)*pow((0.5 + x),2)),2)*atan(1.5*f_VEL_m)) + 0.5*pow(f_VEL_m,3)*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))*(1.0 - 2*x)/(pow((1 + pow(f_VEL_m,2)*pow((0.5 - x),2)),2)*atan(1.5*f_VEL_m)))*cos(-0.5*M_PI + 0.5*M_PI*y)*sin(M_PI*x) + 0.013*pow(M_PI,2)*(1.0 - (0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m)))*cos(-0.5*M_PI + 0.5*M_PI*y)*sin(M_PI*x) - 0.026*M_PI*(0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - x),2))*atan(1.5*f_VEL_m)) - 0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + x),2))*atan(1.5*f_VEL_m)))*cos(M_PI*x)*cos(-0.5*M_PI + 0.5*M_PI*y)) + (2.0*eta_min + 2.0*eta_max*(0.5 + 0.5*atan(f_ETA_m*(0.5 + x))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 + y))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 - x))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 - y))/atan(1.5*f_ETA_m)))*(-0.01*(0.25*pow(f_VEL_m,2)*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + x),2))*(1 + pow(f_VEL_m,2)*pow((0.5 - y),2))*pow(atan(1.5*f_VEL_m),2)) + 0.25*pow(f_VEL_m,2)*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + y),2))*(1 + pow(f_VEL_m,2)*pow((0.5 - x),2))*pow(atan(1.5*f_VEL_m),2)) - 0.25*pow(f_VEL_m,2)*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + x),2))*(1 + pow(f_VEL_m,2)*pow((0.5 + y),2))*pow(atan(1.5*f_VEL_m),2)) - 0.25*pow(f_VEL_m,2)*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - x),2))*(1 + pow(f_VEL_m,2)*pow((0.5 - y),2))*pow(atan(1.5*f_VEL_m),2)))*sin(0.5*M_PI*(1 + y))*sin(0.5*M_PI - M_PI*x) - 0.0065*(0.5*pow(f_VEL_m,2)*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + y),2))*(1 + pow(f_VEL_m,2)*pow((0.5 - y),2))*pow(atan(1.5*f_VEL_m),2)) + 0.5*pow(f_VEL_m,3)*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))*(1.0 + 2*y)/(pow((1 + pow(f_VEL_m,2)*pow((0.5 + y),2)),2)*atan(1.5*f_VEL_m)) + 0.5*pow(f_VEL_m,3)*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(1.0 - 2*y)/(pow((1 + pow(f_VEL_m,2)*pow((0.5 - y),2)),2)*atan(1.5*f_VEL_m)))*cos(-0.5*M_PI + 0.5*M_PI*y)*sin(M_PI*x) + 0.005*pow(M_PI,2)*(1.0 - (0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m)))*cos(0.5*M_PI*(1 + y))*cos(0.5*M_PI - M_PI*x) + 0.01*M_PI*(0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - y),2))*atan(1.5*f_VEL_m)) - 0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + y),2))*atan(1.5*f_VEL_m)))*cos(0.5*M_PI - M_PI*x)*sin(0.5*M_PI*(1 + y)) + 0.001625*pow(M_PI,2)*(1.0 - (0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m)))*cos(-0.5*M_PI + 0.5*M_PI*y)*sin(M_PI*x) + 0.0065*M_PI*(0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - y),2))*atan(1.5*f_VEL_m)) - 0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + y),2))*atan(1.5*f_VEL_m)))*sin(M_PI*x)*sin(-0.5*M_PI + 0.5*M_PI*y) - 0.005*M_PI*(0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - x),2))*atan(1.5*f_VEL_m)) - 0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + x),2))*atan(1.5*f_VEL_m)))*cos(0.5*M_PI*(1 + y))*sin(0.5*M_PI - M_PI*x) + 0.001875*pow(f_VEL_m,2)*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + x),2))*(1 + pow(f_VEL_m,2)*pow((0.5 - y),2))*pow(atan(1.5*f_VEL_m),2)) + 0.001875*pow(f_VEL_m,2)*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + y),2))*(1 + pow(f_VEL_m,2)*pow((0.5 - x),2))*pow(atan(1.5*f_VEL_m),2)) - 0.001875*pow(f_VEL_m,2)*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + x),2))*(1 + pow(f_VEL_m,2)*pow((0.5 + y),2))*pow(atan(1.5*f_VEL_m),2)) - 0.001875*pow(f_VEL_m,2)*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - x),2))*(1 + pow(f_VEL_m,2)*pow((0.5 - y),2))*pow(atan(1.5*f_VEL_m),2))) + (-0.013*(0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - x),2))*atan(1.5*f_VEL_m)) - 0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + x),2))*atan(1.5*f_VEL_m)))*cos(-0.5*M_PI + 0.5*M_PI*y)*sin(M_PI*x) - 0.013*M_PI*(1.0 - (0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m)))*cos(M_PI*x)*cos(-0.5*M_PI + 0.5*M_PI*y))*(eta_max*f_ETA_m*(0.5 + 0.5*atan(f_ETA_m*(0.5 + y))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 - x))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 - y))/atan(1.5*f_ETA_m))/((1 + pow(f_ETA_m,2)*pow((0.5 + x),2))*atan(1.5*f_ETA_m)) - 1.0*eta_max*f_ETA_m*(0.5 + 0.5*atan(f_ETA_m*(0.5 + x))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 + y))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 - y))/atan(1.5*f_ETA_m))/((1 + pow(f_ETA_m,2)*pow((0.5 - x),2))*atan(1.5*f_ETA_m))) + (eta_max*f_ETA_m*(0.5 + 0.5*atan(f_ETA_m*(0.5 + x))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 - x))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 - y))/atan(1.5*f_ETA_m))/((1 + pow(f_ETA_m,2)*pow((0.5 + y),2))*atan(1.5*f_ETA_m)) - 1.0*eta_max*f_ETA_m*(0.5 + 0.5*atan(f_ETA_m*(0.5 + x))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 + y))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 - x))/atan(1.5*f_ETA_m))/((1 + pow(f_ETA_m,2)*pow((0.5 - y),2))*atan(1.5*f_ETA_m)))*(-0.01*(0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - x),2))*atan(1.5*f_VEL_m)) - 0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + x),2))*atan(1.5*f_VEL_m)))*sin(0.5*M_PI*(1 + y))*sin(0.5*M_PI - M_PI*x) - 0.0065*(0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - y),2))*atan(1.5*f_VEL_m)) - 0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + y),2))*atan(1.5*f_VEL_m)))*cos(-0.5*M_PI + 0.5*M_PI*y)*sin(M_PI*x) + 0.01*M_PI*(1.0 - (0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m)))*cos(0.5*M_PI - M_PI*x)*sin(0.5*M_PI*(1 + y)) + 0.00325*M_PI*(1.0 - (0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m)))*sin(M_PI*x)*sin(-0.5*M_PI + 0.5*M_PI*y) + 0.00375*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - x),2))*atan(1.5*f_VEL_m)) - 0.00375*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + x),2))*atan(1.5*f_VEL_m))) - (-0.3*sin(0.5*M_PI*y) - 0.12*cos(M_PI*x)*sin(0.5*M_PI*y))*(0.5*f_P_m*(0.5 + 0.5*atan(f_P_m*(0.5 + x))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 + y))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 - y))/atan(1.5*f_P_m))/((1 + pow(f_P_m,2)*pow((0.5 - x),2))*atan(1.5*f_P_m)) - 0.5*f_P_m*(0.5 + 0.5*atan(f_P_m*(0.5 + y))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 - x))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 - y))/atan(1.5*f_P_m))/((1 + pow(f_P_m,2)*pow((0.5 + x),2))*atan(1.5*f_P_m))) - 0.12*M_PI*(1.0 - (0.5 + 0.5*atan(f_P_m*(0.5 + x))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 + y))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 - x))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 - y))/atan(1.5*f_P_m)))*sin(M_PI*x)*sin(0.5*M_PI*y);    /* Ru_x  - [stokes_sinker_1a] */
		Ruvp[1] = (2.0*eta_min + 2.0*eta_max*(0.5 + 0.5*atan(f_ETA_m*(0.5 + x))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 + y))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 - x))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 - y))/atan(1.5*f_ETA_m)))*(-0.02*(0.5*pow(f_VEL_m,2)*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + y),2))*(1 + pow(f_VEL_m,2)*pow((0.5 - y),2))*pow(atan(1.5*f_VEL_m),2)) + 0.5*pow(f_VEL_m,3)*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))*(1.0 + 2*y)/(pow((1 + pow(f_VEL_m,2)*pow((0.5 + y),2)),2)*atan(1.5*f_VEL_m)) + 0.5*pow(f_VEL_m,3)*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(1.0 - 2*y)/(pow((1 + pow(f_VEL_m,2)*pow((0.5 - y),2)),2)*atan(1.5*f_VEL_m)))*sin(0.5*M_PI*(1 + y))*sin(0.5*M_PI - M_PI*x) + 0.005*pow(M_PI,2)*(1.0 - (0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m)))*sin(0.5*M_PI*(1 + y))*sin(0.5*M_PI - M_PI*x) - 0.02*M_PI*(0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - y),2))*atan(1.5*f_VEL_m)) - 0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + y),2))*atan(1.5*f_VEL_m)))*cos(0.5*M_PI*(1 + y))*sin(0.5*M_PI - M_PI*x) + 0.0075*pow(f_VEL_m,2)*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + y),2))*(1 + pow(f_VEL_m,2)*pow((0.5 - y),2))*pow(atan(1.5*f_VEL_m),2)) + 0.0075*pow(f_VEL_m,3)*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))*(1.0 + 2*y)/(pow((1 + pow(f_VEL_m,2)*pow((0.5 + y),2)),2)*atan(1.5*f_VEL_m)) + 0.0075*pow(f_VEL_m,3)*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(1.0 - 2*y)/(pow((1 + pow(f_VEL_m,2)*pow((0.5 - y),2)),2)*atan(1.5*f_VEL_m))) + (2.0*eta_min + 2.0*eta_max*(0.5 + 0.5*atan(f_ETA_m*(0.5 + x))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 + y))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 - x))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 - y))/atan(1.5*f_ETA_m)))*(-0.01*(0.5*pow(f_VEL_m,2)*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + x),2))*(1 + pow(f_VEL_m,2)*pow((0.5 - x),2))*pow(atan(1.5*f_VEL_m),2)) + 0.5*pow(f_VEL_m,3)*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))*(1.0 + 2*x)/(pow((1 + pow(f_VEL_m,2)*pow((0.5 + x),2)),2)*atan(1.5*f_VEL_m)) + 0.5*pow(f_VEL_m,3)*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))*(1.0 - 2*x)/(pow((1 + pow(f_VEL_m,2)*pow((0.5 - x),2)),2)*atan(1.5*f_VEL_m)))*sin(0.5*M_PI*(1 + y))*sin(0.5*M_PI - M_PI*x) - 0.0065*(0.25*pow(f_VEL_m,2)*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + x),2))*(1 + pow(f_VEL_m,2)*pow((0.5 - y),2))*pow(atan(1.5*f_VEL_m),2)) + 0.25*pow(f_VEL_m,2)*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + y),2))*(1 + pow(f_VEL_m,2)*pow((0.5 - x),2))*pow(atan(1.5*f_VEL_m),2)) - 0.25*pow(f_VEL_m,2)*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + x),2))*(1 + pow(f_VEL_m,2)*pow((0.5 + y),2))*pow(atan(1.5*f_VEL_m),2)) - 0.25*pow(f_VEL_m,2)*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - x),2))*(1 + pow(f_VEL_m,2)*pow((0.5 - y),2))*pow(atan(1.5*f_VEL_m),2)))*cos(-0.5*M_PI + 0.5*M_PI*y)*sin(M_PI*x) + 0.01*pow(M_PI,2)*(1.0 - (0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m)))*sin(0.5*M_PI*(1 + y))*sin(0.5*M_PI - M_PI*x) + 0.02*M_PI*(0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - x),2))*atan(1.5*f_VEL_m)) - 0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + x),2))*atan(1.5*f_VEL_m)))*cos(0.5*M_PI - M_PI*x)*sin(0.5*M_PI*(1 + y)) + 0.00325*M_PI*(0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - x),2))*atan(1.5*f_VEL_m)) - 0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + x),2))*atan(1.5*f_VEL_m)))*sin(M_PI*x)*sin(-0.5*M_PI + 0.5*M_PI*y) + 0.00325*pow(M_PI,2)*(1.0 - (0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m)))*cos(M_PI*x)*sin(-0.5*M_PI + 0.5*M_PI*y) - 0.0065*M_PI*(0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - y),2))*atan(1.5*f_VEL_m)) - 0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + y),2))*atan(1.5*f_VEL_m)))*cos(M_PI*x)*cos(-0.5*M_PI + 0.5*M_PI*y) + 0.00375*pow(f_VEL_m,2)*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + x),2))*(1 + pow(f_VEL_m,2)*pow((0.5 - x),2))*pow(atan(1.5*f_VEL_m),2)) + 0.00375*pow(f_VEL_m,3)*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))*(1.0 + 2*x)/(pow((1 + pow(f_VEL_m,2)*pow((0.5 + x),2)),2)*atan(1.5*f_VEL_m)) + 0.00375*pow(f_VEL_m,3)*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))*(1.0 - 2*x)/(pow((1 + pow(f_VEL_m,2)*pow((0.5 - x),2)),2)*atan(1.5*f_VEL_m))) + (eta_max*f_ETA_m*(0.5 + 0.5*atan(f_ETA_m*(0.5 + y))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 - x))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 - y))/atan(1.5*f_ETA_m))/((1 + pow(f_ETA_m,2)*pow((0.5 + x),2))*atan(1.5*f_ETA_m)) - 1.0*eta_max*f_ETA_m*(0.5 + 0.5*atan(f_ETA_m*(0.5 + x))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 + y))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 - y))/atan(1.5*f_ETA_m))/((1 + pow(f_ETA_m,2)*pow((0.5 - x),2))*atan(1.5*f_ETA_m)))*(-0.01*(0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - x),2))*atan(1.5*f_VEL_m)) - 0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + x),2))*atan(1.5*f_VEL_m)))*sin(0.5*M_PI*(1 + y))*sin(0.5*M_PI - M_PI*x) - 0.0065*(0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - y),2))*atan(1.5*f_VEL_m)) - 0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + y),2))*atan(1.5*f_VEL_m)))*cos(-0.5*M_PI + 0.5*M_PI*y)*sin(M_PI*x) + 0.01*M_PI*(1.0 - (0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m)))*cos(0.5*M_PI - M_PI*x)*sin(0.5*M_PI*(1 + y)) + 0.00325*M_PI*(1.0 - (0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m)))*sin(M_PI*x)*sin(-0.5*M_PI + 0.5*M_PI*y) + 0.00375*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - x),2))*atan(1.5*f_VEL_m)) - 0.00375*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + x),2))*atan(1.5*f_VEL_m))) + (eta_max*f_ETA_m*(0.5 + 0.5*atan(f_ETA_m*(0.5 + x))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 - x))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 - y))/atan(1.5*f_ETA_m))/((1 + pow(f_ETA_m,2)*pow((0.5 + y),2))*atan(1.5*f_ETA_m)) - 1.0*eta_max*f_ETA_m*(0.5 + 0.5*atan(f_ETA_m*(0.5 + x))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 + y))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 - x))/atan(1.5*f_ETA_m))/((1 + pow(f_ETA_m,2)*pow((0.5 - y),2))*atan(1.5*f_ETA_m)))*(-0.02*(0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - y),2))*atan(1.5*f_VEL_m)) - 0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + y),2))*atan(1.5*f_VEL_m)))*sin(0.5*M_PI*(1 + y))*sin(0.5*M_PI - M_PI*x) - 0.01*M_PI*(1.0 - (0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m)))*cos(0.5*M_PI*(1 + y))*sin(0.5*M_PI - M_PI*x) + 0.0075*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - y),2))*atan(1.5*f_VEL_m)) - 0.0075*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + y),2))*atan(1.5*f_VEL_m))) - (1.0 - (0.5 + 0.5*atan(f_P_m*(0.5 + x))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 + y))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 - x))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 - y))/atan(1.5*f_P_m)))*(-0.15*M_PI*cos(0.5*M_PI*y) - 0.06*M_PI*cos(M_PI*x)*cos(0.5*M_PI*y)) - (-0.3*sin(0.5*M_PI*y) - 0.12*cos(M_PI*x)*sin(0.5*M_PI*y))*(0.5*f_P_m*(0.5 + 0.5*atan(f_P_m*(0.5 + x))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 + y))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 - x))/atan(1.5*f_P_m))/((1 + pow(f_P_m,2)*pow((0.5 - y),2))*atan(1.5*f_P_m)) - 0.5*f_P_m*(0.5 + 0.5*atan(f_P_m*(0.5 + x))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 - x))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 - y))/atan(1.5*f_P_m))/((1 + pow(f_P_m,2)*pow((0.5 + y),2))*atan(1.5*f_P_m)));    /* Ru_p  - [stokes_sinker_1a] */
		Ruvp[2] = -0.02*(0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - y),2))*atan(1.5*f_VEL_m)) - 0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + y),2))*atan(1.5*f_VEL_m)))*sin(0.5*M_PI*(1 + y))*sin(0.5*M_PI - M_PI*x) - 0.013*(0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - x),2))*atan(1.5*f_VEL_m)) - 0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + x),2))*atan(1.5*f_VEL_m)))*cos(-0.5*M_PI + 0.5*M_PI*y)*sin(M_PI*x) - 0.01*M_PI*(1.0 - (0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m)))*cos(0.5*M_PI*(1 + y))*sin(0.5*M_PI - M_PI*x) - 0.013*M_PI*(1.0 - (0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m)))*cos(M_PI*x)*cos(-0.5*M_PI + 0.5*M_PI*y) + 0.0075*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - y),2))*atan(1.5*f_VEL_m)) - 0.0075*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + y),2))*atan(1.5*f_VEL_m));    /* Rp  - [stokes_sinker_1a] */
	}
}
void EvaluateStokes2dMS_eta_2(double pos[],double *eta)
{
	double x,y;	
	double eta_min = -7.56e-03 + 1.0e-6;
	double eta_max = 10.0;
	double f_ETA_m = 16.0; //160;
	double f_VEL_m = 3.0; // 30.0;
	double f_P_m   = 30.0; //3000.0;
	
	x = pos[0];
	y = pos[1];
	*eta = eta_min + eta_max*(0.5 + 0.5*atan(f_ETA_m*(0.5 + x))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 + y))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 - x))/atan(1.5*f_ETA_m))*(0.5 + 0.5*atan(f_ETA_m*(0.5 - y))/atan(1.5*f_ETA_m));    /* viscosity - [stokes_sinker_1a] */

}
void EvaluateStokes2dMS_UVP_2(double pos[],double X[])
{
	double x,y;
	double eta_min = -7.56e-03 + 1.0e-6;
	double eta_max = 10.0;
	double f_ETA_m = 16.0; //160;
	double f_VEL_m = 3.0; // 30.0;
	double f_P_m   = 30.0; //3000.0;
	
	x = pos[0];
	y = pos[1];
	{
		X[0] = -0.013*(1.0 - (0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m)))*cos(-0.5*M_PI + 0.5*M_PI*y)*sin(M_PI*x);    /* velocity_x - [stokes_sinker_1a] */
		X[1] = -0.02*(1.0 - (0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m)))*sin(0.5*M_PI*(1 + y))*sin(0.5*M_PI - M_PI*x) - 0.015*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m));    /* velocity_y - [stokes_sinker_1a] */
		X[2] = (1.0 - (0.5 + 0.5*atan(f_P_m*(0.5 + x))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 + y))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 - x))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 - y))/atan(1.5*f_P_m)))*(-0.3*sin(0.5*M_PI*y) - 0.12*cos(M_PI*x)*sin(0.5*M_PI*y));    /* pressure - [stokes_sinker_1a] */
	}
}
void EvaluateStokes2dMS_GradUVP_2(double pos[],double L[])
{
	double x,y;
	double eta_min = -7.56e-03 + 1.0e-6;
	double eta_max = 10.0;
	double f_ETA_m = 16.0; //160;
	double f_VEL_m = 3.0; // 30.0;
	double f_P_m   = 30.0; //3000.0;
	
	x = pos[0];
	y = pos[1];
	{
		L[0] = -0.013*(0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - x),2))*atan(1.5*f_VEL_m)) - 0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + x),2))*atan(1.5*f_VEL_m)))*cos(-0.5*M_PI + 0.5*M_PI*y)*sin(M_PI*x) - 0.013*M_PI*(1.0 - (0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m)))*cos(M_PI*x)*cos(-0.5*M_PI + 0.5*M_PI*y);    /* L_xx = du/dx - [stokes_sinker_1a] */
		L[1] = -0.013*(0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - y),2))*atan(1.5*f_VEL_m)) - 0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + y),2))*atan(1.5*f_VEL_m)))*cos(-0.5*M_PI + 0.5*M_PI*y)*sin(M_PI*x) + 0.0065*M_PI*(1.0 - (0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m)))*sin(M_PI*x)*sin(-0.5*M_PI + 0.5*M_PI*y);    /* L_xy = du/dy - [stokes_sinker_1a] */
		L[2] = -0.02*(0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - x),2))*atan(1.5*f_VEL_m)) - 0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + x),2))*atan(1.5*f_VEL_m)))*sin(0.5*M_PI*(1 + y))*sin(0.5*M_PI - M_PI*x) + 0.02*M_PI*(1.0 - (0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m)))*cos(0.5*M_PI - M_PI*x)*sin(0.5*M_PI*(1 + y)) + 0.0075*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - x),2))*atan(1.5*f_VEL_m)) - 0.0075*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + x),2))*atan(1.5*f_VEL_m));    /* L_yx = dv/dx - [stokes_sinker_1a] */
		L[3] = -0.02*(0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - y),2))*atan(1.5*f_VEL_m)) - 0.5*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + y),2))*atan(1.5*f_VEL_m)))*sin(0.5*M_PI*(1 + y))*sin(0.5*M_PI - M_PI*x) - 0.01*M_PI*(1.0 - (0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m)))*cos(0.5*M_PI*(1 + y))*sin(0.5*M_PI - M_PI*x) + 0.0075*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 + y))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 - y),2))*atan(1.5*f_VEL_m)) - 0.0075*f_VEL_m*(0.5 + 0.5*atan(f_VEL_m*(0.5 + x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - x))/atan(1.5*f_VEL_m))*(0.5 + 0.5*atan(f_VEL_m*(0.5 - y))/atan(1.5*f_VEL_m))/((1 + pow(f_VEL_m,2)*pow((0.5 + y),2))*atan(1.5*f_VEL_m));    /* L_yy = dv/dy - [stokes_sinker_1a] */
		L[4] = (-0.3*sin(0.5*M_PI*y) - 0.12*cos(M_PI*x)*sin(0.5*M_PI*y))*(0.5*f_P_m*(0.5 + 0.5*atan(f_P_m*(0.5 + x))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 + y))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 - y))/atan(1.5*f_P_m))/((1 + pow(f_P_m,2)*pow((0.5 - x),2))*atan(1.5*f_P_m)) - 0.5*f_P_m*(0.5 + 0.5*atan(f_P_m*(0.5 + y))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 - x))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 - y))/atan(1.5*f_P_m))/((1 + pow(f_P_m,2)*pow((0.5 + x),2))*atan(1.5*f_P_m))) + 0.12*M_PI*(1.0 - (0.5 + 0.5*atan(f_P_m*(0.5 + x))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 + y))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 - x))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 - y))/atan(1.5*f_P_m)))*sin(M_PI*x)*sin(0.5*M_PI*y);    /* dp/dx - [stokes_sinker_1a] */
		L[5] = (1.0 - (0.5 + 0.5*atan(f_P_m*(0.5 + x))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 + y))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 - x))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 - y))/atan(1.5*f_P_m)))*(-0.15*M_PI*cos(0.5*M_PI*y) - 0.06*M_PI*cos(M_PI*x)*cos(0.5*M_PI*y)) + (-0.3*sin(0.5*M_PI*y) - 0.12*cos(M_PI*x)*sin(0.5*M_PI*y))*(0.5*f_P_m*(0.5 + 0.5*atan(f_P_m*(0.5 + x))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 + y))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 - x))/atan(1.5*f_P_m))/((1 + pow(f_P_m,2)*pow((0.5 - y),2))*atan(1.5*f_P_m)) - 0.5*f_P_m*(0.5 + 0.5*atan(f_P_m*(0.5 + x))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 - x))/atan(1.5*f_P_m))*(0.5 + 0.5*atan(f_P_m*(0.5 - y))/atan(1.5*f_P_m))/((1 + pow(f_P_m,2)*pow((0.5 + y),2))*atan(1.5*f_P_m)));    /* p/dy - [stokes_sinker_1a] */
	}
}

void EvaluateStokes2dMS_IntP_2(double *intP)
{
	double eta_min = -7.56e-03 + 1.0e-6;
	double eta_max = 10.0;
	double f_ETA_m = 16.0; //160;
	double f_VEL_m = 3.0; // 30.0;
	double f_P_m   = 30.0; //3000.0;

	*intP = 0.0;
}

void Stokes2DMS_ModelDomain_2(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	ierr = DMDASetUniformCoordinates(ctx->dav,-1.0,1.0, -1.0,1.0, PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
}

/* model 3 */
void EvaluateStokes2dMS_RUVP_3(double pos[],double Ru[])
{
	double pressure,vel[2],total_stress[3],strain_rate[3];
	double etaA=1.0;
	double etaB=1.0e2;
	double xc=0.5;
	int n = 3;
	double x,y;
	
	x = pos[0];
	y = pos[1];
	
	Ru[0] = 0.0;
	Ru[1] = -sin((double)n*M_PI*y)*cos(1.0*M_PI*x); // solCx
	Ru[2] = 0.0;
}
void EvaluateStokes2dMS_eta_3(double pos[],double *eta)
{
	double pressure,vel[2],total_stress[3],strain_rate[3];
	double etaA=1.0;
	double etaB=1.0e2;
	double xc=0.5;
	int n = 3;
	double x,y;
	
	x = pos[0];
	y = pos[1];

	*eta = etaA; // solCx
	if (x > xc) {
		*eta = etaB;
	}
}
void EvaluateStokes2dMS_UVP_3(double pos[],double X[])
{
	double pressure,vel[2],total_stress[3],strain_rate[3];
	double etaA=1.0;
	double etaB=1.0e2;
	double xc=0.5;
	int n = 3;
	
	evaluate_solCx(pos,etaA,etaB,xc,n,vel,&pressure,total_stress,strain_rate);
	X[0] = vel[0];
	X[1] = vel[1];
	X[2] = pressure;
}
void EvaluateStokes2dMS_GradUVP_3(double pos[],double L[])
{
}

void EvaluateStokes2dMS_IntP_3(double *intP)
{
}

void Stokes2DMS_ModelDomain_3(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	ierr = DMDASetUniformCoordinates(ctx->dav,0.0,1.0, 0.0,1.0, PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
}




PetscErrorCode pTatinModelMMSLoadSolution(MMSBucket *mms)
{
	PetscErrorCode ierr;
	PetscInt model_id;
	PetscFunctionBegin;

	/* choose a model */
	model_id = 1;
	ierr = PetscOptionsGetInt(PETSC_NULL,"-ptatin_mms_linear_model_id",&model_id,0);CHKERRQ(ierr);
	
	switch (model_id) {
		case 1:
			PetscPrintf(PETSC_COMM_WORLD,"  [pTatinModel]: MMS => Loading \"solution 1\" \n");
			mms->remove_int_pressure             = PETSC_TRUE;
			mms->has_gradients                   = PETSC_TRUE;
			mms->fp_EvaluateStokes2dMS_RUVP      = &EvaluateStokes2dMS_RUVP_1;
			mms->fp_EvaluateStokes2dMS_eta       = &EvaluateStokes2dMS_eta_1;
			mms->fp_EvaluateStokes2dMS_UVP       = &EvaluateStokes2dMS_UVP_1;
			mms->fp_EvaluateStokes2dMS_GradUVP   = &EvaluateStokes2dMS_GradUVP_1;
			mms->fp_EvaluateStokes2dMS_IntP      = &EvaluateStokes2dMS_IntP_1;
			mms->fp_DefineStokes2DMS_ModelDomain = &Stokes2DMS_ModelDomain_1;
			break;

		case 2:
			PetscPrintf(PETSC_COMM_WORLD,"  [pTatinModel]: MMS => Loading \"solution 2 (sinker)\" \n");
			mms->remove_int_pressure             = PETSC_TRUE;
			mms->has_gradients                   = PETSC_TRUE;
			mms->fp_EvaluateStokes2dMS_RUVP      = &EvaluateStokes2dMS_RUVP_2;
			mms->fp_EvaluateStokes2dMS_eta       = &EvaluateStokes2dMS_eta_2;
			mms->fp_EvaluateStokes2dMS_UVP       = &EvaluateStokes2dMS_UVP_2;
			mms->fp_EvaluateStokes2dMS_GradUVP   = &EvaluateStokes2dMS_GradUVP_2;
			mms->fp_EvaluateStokes2dMS_IntP      = &EvaluateStokes2dMS_IntP_2;
			mms->fp_DefineStokes2DMS_ModelDomain = &Stokes2DMS_ModelDomain_2;
			break;

		case 3:
			PetscPrintf(PETSC_COMM_WORLD,"  [pTatinModel]: MMS => Loading \"solution 2 (solCx)\" \n");
			mms->remove_int_pressure             = PETSC_FALSE;
			mms->has_gradients                   = PETSC_FALSE;
			mms->fp_EvaluateStokes2dMS_RUVP      = &EvaluateStokes2dMS_RUVP_3;
			mms->fp_EvaluateStokes2dMS_eta       = &EvaluateStokes2dMS_eta_3;
			mms->fp_EvaluateStokes2dMS_UVP       = &EvaluateStokes2dMS_UVP_3;
			mms->fp_EvaluateStokes2dMS_GradUVP   = &EvaluateStokes2dMS_GradUVP_3;
			mms->fp_EvaluateStokes2dMS_IntP      = &EvaluateStokes2dMS_IntP_3;
			mms->fp_DefineStokes2DMS_ModelDomain = &Stokes2DMS_ModelDomain_3;
			break;
			
		default:
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"  [pTatinModel]: MMS does not permit a default model");
			break;
	}
	
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ComputeElementFields"
PetscErrorCode ComputeElementFields( 
																			PetscScalar el_coords[], 
																			PetscScalar el_vel[], PetscScalar el_pressure[], 
																			PetscScalar xi[], PetscScalar NIu[], PetscScalar *GNXu[], PetscScalar NIp[],
																			PetscScalar pos[], /* position */
																			PetscScalar X[], /* u,v,p */
																			PetscScalar Lij[]) /* L_ij(u,v) */
{
	PetscScalar vx_p,vy_p,pressure_p,xpos[2];
	PetscScalar du_dx, du_dy, dv_dx, dv_dy;
	PetscInt ii,jj;
	
	
	PetscFunctionBegin;
	/* compute position */
	/* compute velocity */
	xpos[0] = xpos[1] = 0.0;
	vx_p = vy_p = 0.0;
	for (ii=0; ii<U_BASIS_FUNCTIONS; ii++) {
		PetscScalar XX = el_coords[2*ii  ];
		PetscScalar YY = el_coords[2*ii+1];
		PetscScalar vx = el_vel[2*ii  ];
		PetscScalar vy = el_vel[2*ii+1];
		
		xpos[0] += NIu[ii] * XX;
		xpos[1] += NIu[ii] * YY;
		
		vx_p += NIu[ii] * vx;
		vy_p += NIu[ii] * vy;
	}
	
	/* compute pressure */
	pressure_p = 0.0;
	for (jj=0; jj<P_BASIS_FUNCTIONS; jj++) {
		pressure_p += NIp[jj] * el_pressure[jj];
	}
	
	/* compute du/dx, du/dy, dv/dx, dvdy */
	du_dx = 0.0;
	du_dy = 0.0;
	dv_dx = 0.0;
	dv_dy = 0.0;
	for( ii=0; ii<U_BASIS_FUNCTIONS; ii++ ) {
		PetscScalar vx = el_vel[2*ii  ];
		PetscScalar vy = el_vel[2*ii+1];
		
		du_dx = du_dx + GNXu[0][ii] * vx ;
		du_dy = du_dy + GNXu[1][ii] * vx ;
		dv_dx = dv_dx + GNXu[0][ii] * vy ;
		dv_dy = dv_dy + GNXu[1][ii] * vy ;
	}

	pos[0] = xpos[0];
	pos[1] = xpos[1];
	
	X[0] = vx_p;
	X[1] = vy_p;
	X[2] = pressure_p;
	
	Lij[0] = du_dx;
	Lij[1] = du_dy;
	Lij[2] = dv_dx;
	Lij[3] = dv_dy;
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatinModelMMSComputeErrors"
PetscErrorCode pTatinModelMMSComputeErrors(MMSBucket *mms,pTatinCtx ctx,PetscScalar velocity[],PetscScalar pressure[])
{
	PetscErrorCode ierr;

	DM dau,dap,cda;
	Vec            gcoords;
	PetscScalar    *LA_gcoords;
	
	PetscInt  k,kk,n_integrals;
	PetscReal int_p_local,int_p,int_p_mms,int_p_mms_local,mmsIntP;
	PetscReal contibution_integrals[100];
	PetscReal integral_local[100];
	PetscReal integral[100];
	PetscScalar pos_p[2],X_p[3],Lij_p[4];

	PetscScalar    NIu[Q2_NODES_PER_EL_2D], NIp[P_BASIS_FUNCTIONS];
	double __GNIu[2*Q2_NODES_PER_EL_2D], __GNXu[2*Q2_NODES_PER_EL_2D];
	double *GNIu[2];
	double *GNXu[2];
	PetscScalar    volJ,elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar    el_vel[2*Q2_NODES_PER_EL_2D];
	PetscScalar    el_vel_mms[2*Q2_NODES_PER_EL_2D];
	PetscScalar    el_p[P_BASIS_FUNCTIONS];
	ConformingElementFamily element;

	PetscInt gp,e,nel,nen_u,nen_p;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	
	PetscFunctionBegin;
	
	GNIu[0] = &__GNIu[0];
	GNIu[1] = &__GNIu[Q2_NODES_PER_EL_2D*1];
	
	GNXu[0] = &__GNXu[0];
	GNXu[1] = &__GNXu[Q2_NODES_PER_EL_2D*1];
	
	
	/* setup for coords */
	dau = ctx->dav;
	dap = ctx->dap;
	ierr = DMDAGetCoordinateDA(dau,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(dau,&gcoords);CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
	
	
	n_integrals = 6; /* Eu_L1, Ev_L1, Ep_L1, uv_L2, p_L2, uv_H1 */
	PetscMemzero(integral_local,sizeof(PetscReal)*n_integrals);
	PetscMemzero(integral,sizeof(PetscReal)*n_integrals);
										
	element = ctx->surfQ[0]->e; /* take any of these - we are just going to call the BasisFuncGrad() and ComputeGeom functions */

	/* integrate the pressure over the domain */
	mmsIntP   = 0.0;
	int_p     = 0.0;
	int_p_mms = 0.0;
	if (mms->remove_int_pressure) {
		mms->fp_EvaluateStokes2dMS_IntP(&mmsIntP);
		
		int_p_local     = 0.0;
		int_p_mms_local = 0.0;
		for (e=0; e<nel; e++) {
			GaussPointCoefficientsStokes *point;
			
			ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
			ierr = DMDAGetScalarElementField_2D(el_p,nen_p,(PetscInt*)&elnidx_p[nen_p*e],pressure);CHKERRQ(ierr);
			ierr = QuadratureStokesGetCell(ctx->Q,e,&point);CHKERRQ(ierr);

			
			for (gp=0; gp<ctx->Q->ngp; gp++) {
				double mmsUVP[3];
				QPoint2d tmp_point;
				PetscScalar *xi_p;
				
				xi_p = &ctx->Q->xi[2*gp];
				
				tmp_point.xi  = xi_p[0];
				tmp_point.eta = xi_p[1];
				tmp_point.w   = ctx->Q->weight[gp];
				
				element->basis_NI_2D(&tmp_point,NIu);
				element->basis_GNI_2D(&tmp_point,GNIu);
				element->compute_volume_geometry_2D(elcoords,(const double**)GNIu,GNXu,&volJ);
				
				ConstructNi_pressure(xi_p,elcoords,NIp);
				
				
				/* evaluate u,v,p */
				/* evaluate L_ij(u) */
				ierr = ComputeElementFields( elcoords,el_vel,el_p, 
																		xi_p,NIu,GNXu,NIp,
																		pos_p,X_p,Lij_p );CHKERRQ(ierr);
				
				int_p_local = int_p_local + tmp_point.w * X_p[2] * volJ;
				
				mms->fp_EvaluateStokes2dMS_UVP(pos_p,mmsUVP);
				
				int_p_mms_local = int_p_mms_local + tmp_point.w * mmsUVP[2] * volJ;
			}
		}
		MPI_Allreduce(&int_p_local,    &int_p,    1,MPIU_SCALAR,MPI_SUM,PETSC_COMM_WORLD);
		MPI_Allreduce(&int_p_mms_local,&int_p_mms,1,MPIU_SCALAR,MPI_SUM,PETSC_COMM_WORLD);
		PetscPrintf(PETSC_COMM_WORLD,"    Compute pressure average of solution    %1.8e \n", int_p);
		PetscPrintf(PETSC_COMM_WORLD,"    Compute pressure average of MS solution %1.8e \n", int_p_mms);
		
	}
	
	
	for (e=0; e<nel; e++) {
		GaussPointCoefficientsStokes *point;
		
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		ierr = DMDAGetVectorElementFieldQ2_2D(el_vel,(PetscInt*)&elnidx_u[nen_u*e],velocity);CHKERRQ(ierr);
		ierr = DMDAGetScalarElementField_2D(el_p,nen_p,(PetscInt*)&elnidx_p[nen_p*e],pressure);CHKERRQ(ierr);

		ierr = QuadratureStokesGetCell(ctx->Q,e,&point);CHKERRQ(ierr);

		/* evaluate analytic solution at the nodal points */
		for (k=0; k<Q2_NODES_PER_EL_2D; k++) {
			double pos_k[2];
			double mmsUVP[3];
			
			pos_k[0] = elcoords[2*k  ];
			pos_k[1] = elcoords[2*k+1];
			mms->fp_EvaluateStokes2dMS_UVP(pos_k,mmsUVP);

			el_vel_mms[2*k  ] = mmsUVP[0];
			el_vel_mms[2*k+1] = mmsUVP[1];
		}
		
		for (gp=0; gp<ctx->Q->ngp; gp++) {
			double mmsUVP[3],mmsGradUVP[4+2];
			double u_err,v_err,p_err,Lerr[4],GPerr[2];
			QPoint2d tmp_point;
			PetscScalar *xi_p;
			
			xi_p = &ctx->Q->xi[2*gp];
			
			tmp_point.xi  = xi_p[0];
			tmp_point.eta = xi_p[1];
			tmp_point.w   = ctx->Q->weight[gp];
			
			element->basis_NI_2D(&tmp_point,NIu);
			element->basis_GNI_2D(&tmp_point,GNIu);
			element->compute_volume_geometry_2D(elcoords,(const double**)GNIu,GNXu,&volJ);
			
			ConstructNi_pressure(xi_p,elcoords,NIp);

			
			/* evaluate u,v,p */
			/* evaluate L_ij(u) */
			ierr = ComputeElementFields( elcoords,el_vel,el_p, 
																	 xi_p,NIu,GNXu,NIp,
																 	 pos_p,X_p,Lij_p );CHKERRQ(ierr);

			/* remove the compute int_p and add the one from the MS */
			if (mms->remove_int_pressure) {
			//	X_p[2] = X_p[2] - int_p + mmsIntP;
				X_p[2] = X_p[2] - int_p + int_p_mms;
			}
			
			/* evaluate u,v,p,Lij(u) from manufactured solution */
			mms->fp_EvaluateStokes2dMS_UVP(pos_p,mmsUVP);
			mms->fp_EvaluateStokes2dMS_GradUVP(pos_p,mmsGradUVP);
			/* if gradients are not present, compute approximate ones using mmsUVP */
			if (mms->has_gradients==PETSC_FALSE) {
				mmsGradUVP[0] = 0.0;
				mmsGradUVP[1] = 0.0;
				mmsGradUVP[2] = 0.0;
				mmsGradUVP[3] = 0.0;
				
				for (k=0; k<Q2_NODES_PER_EL_2D; k++) {
					mmsGradUVP[0] += GNXu[0][k] * el_vel_mms[2*k  ];
					mmsGradUVP[1] += GNXu[1][k] * el_vel_mms[2*k  ];
					mmsGradUVP[2] += GNXu[0][k] * el_vel_mms[2*k+1];
					mmsGradUVP[3] += GNXu[1][k] * el_vel_mms[2*k+1];
				}
			}
			
			u_err = X_p[0] - mmsUVP[0];
			v_err = X_p[1] - mmsUVP[1];
			p_err = X_p[2] - mmsUVP[2];
			
			Lerr[0] = Lij_p[0] - mmsGradUVP[0];
			Lerr[1] = Lij_p[1] - mmsGradUVP[1];
			Lerr[2] = Lij_p[2] - mmsGradUVP[2];
			Lerr[3] = Lij_p[3] - mmsGradUVP[3];
			
			GPerr[0] = 0.0 - mmsGradUVP[4]; // dp/dx
			GPerr[1] = 0.0 - mmsGradUVP[5]; // dp/dy
			
			// u,v,p L1
			contibution_integrals[0] = fabs( u_err ) * tmp_point.w * volJ;
			contibution_integrals[1] = fabs( v_err ) * tmp_point.w * volJ;
			contibution_integrals[2] = fabs( p_err ) * tmp_point.w * volJ;

			// uv,p L2
			contibution_integrals[3] = ( u_err * u_err + v_err * v_err ) * tmp_point.w * volJ;
			contibution_integrals[4] = ( p_err * p_err                 ) * tmp_point.w * volJ;
			
			// u_H1 = du/dx^2 + du/dy^2 + dv/dx^2 + dv/dy^2
			contibution_integrals[5] = ( Lerr[0]*Lerr[0] + Lerr[1]*Lerr[1] + Lerr[2]*Lerr[2] + Lerr[3]*Lerr[3]) * tmp_point.w * volJ;
			
			for (kk=0; kk<n_integrals; kk++ ) {
				integral_local[kk] = integral_local[kk] + contibution_integrals[kk];
			}
			
		}
	}

	MPI_Allreduce(integral_local,integral,n_integrals,MPIU_SCALAR,MPI_SUM,PETSC_COMM_WORLD);
	integral[3] = PetscSqrtScalar(integral[3]);//uv_L2
	integral[4] = PetscSqrtScalar(integral[4]);//p_L2
	integral[5] = PetscSqrtScalar(integral[5]);//uv_H1
	
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);

	PetscPrintf(PETSC_COMM_WORLD,"  [pTatinModel]: MMS Errors \n");
	
	PetscPrintf(PETSC_COMM_WORLD,"    u_L1    = %1.8e \n", integral[0]);
	PetscPrintf(PETSC_COMM_WORLD,"    v_L1    = %1.8e \n", integral[1]);
	PetscPrintf(PETSC_COMM_WORLD,"    p_L1    = %1.8e \n", integral[2]);
	
	PetscPrintf(PETSC_COMM_WORLD,"    uv_L2   = %1.8e \n", integral[3]);
	PetscPrintf(PETSC_COMM_WORLD,"    p_L2    = %1.8e \n", integral[4]);

	if (mms->has_gradients) {
		PetscPrintf(PETSC_COMM_WORLD,"    uv_H1   = %1.8e \n", integral[5]);
	} else {
		PetscPrintf(PETSC_COMM_WORLD,"    uv_H1   = %1.8e [estimated using analytic UV]\n", integral[5]);
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelOutput_mms_linear"
PetscErrorCode pTatin2d_ModelOutput_mms_linear(pTatinCtx ctx,Vec X,const char prefix[])
{
  DM             dau,dap;
  Vec            Uloc,Ploc;
  PetscScalar    *LA_Uloc,*LA_Ploc;
	MMSBucket       mms;
	PetscErrorCode ierr;

	
	if (ctx->step==1) {
		ierr = DMCompositeGetEntries(ctx->pack,&dau,&dap);CHKERRQ(ierr);
		ierr = DMCompositeGetLocalVectors(ctx->pack,&Uloc,&Ploc);CHKERRQ(ierr);
		
		ierr = DMCompositeScatter(ctx->pack,X,Uloc,Ploc);CHKERRQ(ierr);
		ierr = VecGetArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
		ierr = VecGetArray(Ploc,&LA_Ploc);CHKERRQ(ierr);

		/* do error calc */
		ierr = pTatinModelMMSLoadSolution(&mms);CHKERRQ(ierr);
		ierr = pTatinModelMMSComputeErrors(&mms,ctx,LA_Uloc,LA_Ploc);

		ierr = VecRestoreArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
		ierr = VecRestoreArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
		ierr = DMCompositeRestoreLocalVectors(ctx->pack,&Uloc,&Ploc);CHKERRQ(ierr);
	}
	
	
	
	if (prefix == PETSC_NULL) {
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,"qpoints");CHKERRQ(ierr);
		ierr = pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(ctx,ctx->outputpath,"faceqpoints");CHKERRQ(ierr);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,"vp");CHKERRQ(ierr);
		ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,"int_fields");CHKERRQ(ierr);
	} else { /* exanple of adding a different prefix to the file names - not sure this is useful */
		char *name;
		
		asprintf(&name,"%s_qpoints",prefix);
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);

		asprintf(&name,"%s_faceqpoints",prefix);
		ierr = pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
		
		asprintf(&name,"%s_vp",prefix);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
		asprintf(&name,"%s_int_fields",prefix);
		ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
	}
	
	PetscFunctionReturn(0);
}
				
#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_mms_linear"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_mms_linear(pTatinCtx ctx)
{
	MMSBucket mms;
	PetscErrorCode ierr;
	PetscFunctionBegin;
	
	ierr = pTatinModelMMSLoadSolution(&mms);CHKERRQ(ierr);
	
	mms.fp_DefineStokes2DMS_ModelDomain(ctx);
	
	PetscFunctionReturn(0);
}

/*
 FIELD_IDX = {0,1,2,3}
 ==>> eta,Fu[0],Fu[1],Fp
 */
#undef __FUNCT__
#define __FUNCT__ "CellPropertyIntegration"
PetscErrorCode CellPropertyIntegration(PetscScalar celldata[],PetscInt FIELD_IDX,pTatinCtx user,DM dau,DM dap)
{	
	PetscErrorCode ierr;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
	PetscScalar GNIu[MAX_QUAD_PNTS][2][Q2_NODES_PER_EL_2D];
	PetscScalar NIu[MAX_QUAD_PNTS][U_BASIS_FUNCTIONS];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen_u,nen_p,e,n,k;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscInt vel_el_lidx[U_BASIS_FUNCTIONS*2];
	PetscInt *gidx;
	PetscInt i,j;
	GaussPointCoefficientsStokes *gausspoints,integral;
	PetscScalar volume;
	PetscFunctionBegin;
	
	/* quadrature */
	/*QuadratureCreateGauss_3pnt_2D(&ngp,gp_xi,gp_weight);*/
	ngp       = user->Q->ngp;
	gp_xi     = user->Q->xi;
	gp_weight = user->Q->weight;
	for (k=0; k<ngp; k++) {
		PetscScalar *xip = &gp_xi[2*k];
		
		PTatinConstructNI_Q2_2D(xip,NIu[k]);
		PTatinConstructGNI_Q2_2D(xip,GNIu[k]);
	}
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetGlobalIndices(dau,0,&gidx);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
	
	ierr = PetscMemzero(celldata,sizeof(PetscScalar)*nel);CHKERRQ(ierr);
	
	for (e=0;e<nel;e++) {
		
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		ierr = StokesVelocity_GetElementLocalIndices(vel_el_lidx,(PetscInt*)&elnidx_u[nen_u*e]);CHKERRQ(ierr);
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		
		/* get element velocity */
		for( i=0; i<Q2_NODES_PER_EL_2D; i++ ) {
			xc[i] = elcoords[2*i  ];
			yc[i] = elcoords[2*i+1];
		}
		
		integral.eta   = 0.0;
		integral.Fu[0] = 0.0;
		integral.Fu[1] = 0.0;
		integral.Fp    = 0.0;
		volume         = 0.0;
		
		for (n=0; n<ngp; n++) {
			PetscScalar  J[2][2];
			PetscScalar J_p,fac;
			
			/* coord transformation */
			for( i=0; i<2; i++ ) {
				for( j=0; j<2; j++ ) { J[i][j] = 0.0; }
			}
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				J[0][0] += GNIu[n][0][k] * xc[k] ;
				J[0][1] += GNIu[n][0][k] * yc[k] ;
				
				J[1][0] += GNIu[n][1][k] * xc[k] ;
				J[1][1] += GNIu[n][1][k] * yc[k] ;
			}
			J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
			
			
			/* viscosity @ gp */
			fac = J_p * gp_weight[n];
			volume = volume + 1.0 * fac;
			
			integral.eta   += gausspoints[n].eta * fac;
			integral.Fu[0] += gausspoints[n].Fu[0] * fac;
			integral.Fu[1] += gausspoints[n].Fu[1] * fac;
			integral.Fp    += gausspoints[n].Fp * fac;
		}
		
		switch (FIELD_IDX) {
			case 0:
				celldata[e] = integral.eta/volume;
				break;
			case 1:
				celldata[e] = integral.Fu[0]/volume;
				break;
			case 2:
				celldata[e] = integral.Fu[1]/volume;
				break;
			case 3:
				celldata[e] = integral.Fp/volume;
				break;
		}
		
	}
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_mms_linear"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_mms_linear(pTatinCtx ctx)
{
	MMSBucket mms;
	PetscInt e,ncells;
	PetscInt p,ngp;
	GaussPointCoefficientsStokes *gausspoints;
	double Ruvp[3]; /* u,v,p */
	PetscBool use_cell_center_values;
	PetscBool use_int_cell_values;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;

	ierr = QuadratureStokesGravityModelCoordinateAlignedSetValues(ctx->Q,1.0,1.0);CHKERRQ(ierr);


	ncells = ctx->Q->ncells;
	ngp    = ctx->Q->ngp;
	ctx->rheology_constants.rheology_type = RHEOLOGY_VISCOUS;

	/* choose a model */
	ierr = pTatinModelMMSLoadSolution(&mms);CHKERRQ(ierr);
	
	for (e=0;e<ncells;e++) {
		ierr = QuadratureStokesGetCell(ctx->Q,e,&gausspoints);CHKERRQ(ierr);
		
		for (p=0; p<ngp; p++) {
			double pos[2];

			pos[0] = gausspoints[p].coord[0];
			pos[1] = gausspoints[p].coord[1];

			/* eta */
			mms.fp_EvaluateStokes2dMS_eta(pos,&gausspoints[p].eta);

			/* rhs */
			mms.fp_EvaluateStokes2dMS_RUVP(pos,Ruvp);
			gausspoints[p].Fu[0] = -Ruvp[0];
			gausspoints[p].Fu[1] = -Ruvp[1];
			gausspoints[p].Fp    = -Ruvp[2];
		}
	}
	
	use_cell_center_values = PETSC_FALSE;
	PetscOptionsGetBool(PETSC_NULL,"-ptatin_mms_linear_P0",&use_cell_center_values,0);
	if (use_cell_center_values) {
		PetscPrintf(PETSC_COMM_WORLD,"  [pTatinModel]: MMS using P0 cell values\n");
		for (e=0;e<ncells;e++) {
			ierr = QuadratureStokesGetCell(ctx->Q,e,&gausspoints);CHKERRQ(ierr);
			// Set all props based on the center gauss point //
			for (p=0; p<ngp; p++) {
				gausspoints[p].Fu[0] = gausspoints[4].Fu[0];
				gausspoints[p].Fu[1] = gausspoints[4].Fu[1];
				gausspoints[p].Fp    = gausspoints[4].Fp;
				gausspoints[p].eta   = gausspoints[4].eta;
			}
		}
	}
	
	use_int_cell_values = PETSC_FALSE;
	PetscOptionsGetBool(PETSC_NULL,"-ptatin_mms_linear_int_P0",&use_int_cell_values,0);
	if (use_int_cell_values) {
		PetscScalar *LA_cell_data;
		
		PetscPrintf(PETSC_COMM_WORLD,"  [pTatinModel]: MMS using integrated cell values\n");

		ierr = PetscMalloc(sizeof(PetscScalar)*ncells,&LA_cell_data);CHKERRQ(ierr);


		// eta
		ierr = PetscMemzero(LA_cell_data,sizeof(PetscScalar)*ncells);CHKERRQ(ierr);
		ierr = CellPropertyIntegration(LA_cell_data,0,ctx,ctx->dav,ctx->dap);
		for (e=0;e<ncells;e++) {
			ierr = QuadratureStokesGetCell(ctx->Q,e,&gausspoints);CHKERRQ(ierr);
			for (p=0; p<ngp; p++) {
				gausspoints[p].eta   = LA_cell_data[e];
			}
		}
		
		// Fux
		ierr = PetscMemzero(LA_cell_data,sizeof(PetscScalar)*ncells);CHKERRQ(ierr);
		ierr = CellPropertyIntegration(LA_cell_data,1,ctx,ctx->dav,ctx->dap);
		for (e=0;e<ncells;e++) {
			ierr = QuadratureStokesGetCell(ctx->Q,e,&gausspoints);CHKERRQ(ierr);
			for (p=0; p<ngp; p++) {
				gausspoints[p].Fu[0] = LA_cell_data[e];
			}
		}
		
		// Fuy
		ierr = PetscMemzero(LA_cell_data,sizeof(PetscScalar)*ncells);CHKERRQ(ierr);
		ierr = CellPropertyIntegration(LA_cell_data,2,ctx,ctx->dav,ctx->dap);
		for (e=0;e<ncells;e++) {
			ierr = QuadratureStokesGetCell(ctx->Q,e,&gausspoints);CHKERRQ(ierr);
			for (p=0; p<ngp; p++) {
				gausspoints[p].Fu[1] = LA_cell_data[e];
			}
		}
		
		// Fp
		ierr = PetscMemzero(LA_cell_data,sizeof(PetscScalar)*ncells);CHKERRQ(ierr);
		ierr = CellPropertyIntegration(LA_cell_data,3,ctx,ctx->dav,ctx->dap);
		for (e=0;e<ncells;e++) {
			ierr = QuadratureStokesGetCell(ctx->Q,e,&gausspoints);CHKERRQ(ierr);
			for (p=0; p<ngp; p++) {
				gausspoints[p].Fp    = LA_cell_data[e];
			}
		}
		
		ierr = PetscFree(LA_cell_data);CHKERRQ(ierr);
	}
	//
	
	PetscFunctionReturn(0);
}

PetscBool BCListEvaluator_mms_u( PetscScalar position[], PetscScalar *value, void *ctx ) 
{
	MMSBucket *mms;
	PetscBool impose_dirichlet = PETSC_TRUE;
	double UVP[3];
	
	mms = (MMSBucket*)ctx;
	mms->fp_EvaluateStokes2dMS_UVP(position,UVP);
	*value = UVP[0];
	
	return impose_dirichlet;
}
PetscBool BCListEvaluator_mms_v( PetscScalar position[], PetscScalar *value, void *ctx ) 
{
	MMSBucket *mms;
	PetscBool impose_dirichlet = PETSC_TRUE;
	double UVP[3];
	
	mms = (MMSBucket*)ctx;
	mms->fp_EvaluateStokes2dMS_UVP(position,UVP);
	*value = UVP[1];
	
	return impose_dirichlet;
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_mms_linear"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_mms_linear(pTatinCtx ctx)
{
	MMSBucket      mms;
	BCList         ubclist;
	DM             dav;
	PetscScalar    bcval;
	PetscInt       model_id;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ubclist = ctx->u_bclist;
	dav     = ctx->dav;

	/* choose a model */
	ierr = pTatinModelMMSLoadSolution(&mms);CHKERRQ(ierr);


	model_id = 1;
	ierr = PetscOptionsGetInt(PETSC_NULL,"-ptatin_mms_linear_model_id",&model_id,0);CHKERRQ(ierr);
	if (model_id==3) {
		PetscPrintf(PETSC_COMM_WORLD,"  [pTatinModel]: MMS => \"(solCx)\" using true free slip boundary conditions\n");
		bcval = 0.0;
		bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
		bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
		bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
		bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	} else {
		PetscPrintf(PETSC_COMM_WORLD,"  [pTatinModel]: MMS => Using pure dirichlet boundaries from manufactured solution \n");

		/* evaluate mms on all sides, all dofs */
		ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_mms_u,(void*)&mms);CHKERRQ(ierr);	
		ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_mms_u,(void*)&mms);CHKERRQ(ierr);	
		ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,0,BCListEvaluator_mms_u,(void*)&mms);CHKERRQ(ierr);	
		ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,0,BCListEvaluator_mms_u,(void*)&mms);CHKERRQ(ierr);	
		
		ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_mms_v,(void*)&mms);CHKERRQ(ierr);	
		ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,1,BCListEvaluator_mms_v,(void*)&mms);CHKERRQ(ierr);	
		ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_mms_v,(void*)&mms);CHKERRQ(ierr);	
		ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_mms_v,(void*)&mms);CHKERRQ(ierr);	
	}
	 
	PetscFunctionReturn(0);
}


