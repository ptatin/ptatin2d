/*
 
 Model Description:

 These files define the setup for the analytic solution known as solCx.
 Domain is [1,1]^2 with free slip boundaries.
 There is a step function in the viscosity in the x direction.
 Flow is driven by a sinusoidal denstity field.
 
 Input / command line parameters:
 -solcx_eta0 : viscosity on left side of the jump (default = 1.0)
 -solcx_eta1 : viscosity on right side of the jump (default = 1.0)
 -solcx_xc : x position of the jump (default = 0.5)
 -solcx_nz : wavenumber in y of the forcing function (default = 1)
 
 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"

/* ================= SolCx ================= */

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelOutput_SolCx"
PetscErrorCode pTatin2d_ModelOutput_SolCx(pTatinCtx ctx,Vec X,const char prefix[])
{
	char *pvdfilename;
	PetscErrorCode ierr;

//	ierr = pTatin2d_ModelOutput_Default(ctx,X,prefix);CHKERRQ(ierr);
	
	if (prefix == PETSC_NULL) {
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,"qpoints");CHKERRQ(ierr);
		ierr = pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(ctx,ctx->outputpath,"faceqpoints");CHKERRQ(ierr);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,"vp");CHKERRQ(ierr);
		ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,"int_fields");CHKERRQ(ierr);

	//	ierr = SwarmOutputParaView_MPntStd(ctx->db,ctx->outputpath,"mpoints");CHKERRQ(ierr);
		ierr = SwarmOutputParaView_MPntPStokes(ctx->db,ctx->outputpath,"mpoints_stk");CHKERRQ(ierr);
	} else { /* exanple of adding a different prefix to the file names - not sure this is useful */
		char *name;
		
		asprintf(&name,"%s_qpoints",prefix);
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);

		asprintf(&name,"%s_faceqpoints",prefix);
		ierr = pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
		
		asprintf(&name,"%s_vp",prefix);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
		asprintf(&name,"%s_int_fields",prefix);
		ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);

	//	asprintf(&name,"%s_mpoints",prefix);
	//	ierr = SwarmOutputParaView_MPntStd(ctx->db,ctx->outputpath,name);CHKERRQ(ierr);
	//	free(name);

		//////////////
		asprintf(&pvdfilename,"%s/timeseries_mpoints_stk.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;

			asprintf(&vtkfilename, "%s_mpoints_stk.pvtu",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			free(vtkfilename);
		}
		/////////

		asprintf(&name,"%s_mpoints_stk",prefix);
		ierr = SwarmOutputParaView_MPntPStokes(ctx->db,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_SolCx"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_SolCx(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
	
	ierr = DMDASetUniformCoordinates(ctx->dav,0.0,1.0, 0.0,1.0, 0.0,0.0);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_SolCx"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_SolCx(pTatinCtx ctx)
{
	PetscInt               e,ncells,n_mp_points;
	PetscInt               p,ngp;
	GaussPointCoefficientsStokes *gausspoints;
	PetscReal              opts_eta0,opts_eta1,opts_xc;
	PetscInt               opts_nz;
	DataBucket             db;
	DataField              PField_std,PField_stokes;
	double                 eta,rho;
	int                    phase_init,phase;
	PetscErrorCode         ierr;
	
	PetscFunctionBegin;
	
	opts_eta0 = 1.0;
	opts_eta1 = 1.0;
	opts_xc   = 0.5;
	opts_nz   = 1;
	
	ierr = PetscOptionsGetReal(PETSC_NULL,"-solcx_eta0",&opts_eta0,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-solcx_eta1",&opts_eta1,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-solcx_xc",&opts_xc,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetInt(PETSC_NULL,"-solcx_nz",&opts_nz,0);CHKERRQ(ierr);
	
	ncells = ctx->Q->ncells;
	ngp    = ctx->Q->ngp;
	ctx->rheology_constants.rheology_type = RHEOLOGY_VISCOUS;

	/* define properties on quadrature points */
	/* to be removed shortly */
#if 0
	for (e=0;e<ncells;e++) {
		ierr = QuadratureStokesGetCell(ctx->Q,e,&gausspoints);CHKERRQ(ierr);
		
		for (p=0; p<ngp; p++) {
			PetscScalar coord_x = gausspoints[p].coord[0];
			PetscScalar coord_y = gausspoints[p].coord[1];
			/* eta */
			
			gausspoints[p].eta = opts_eta0;
			if (coord_x > opts_xc) {
				gausspoints[p].eta = opts_eta1;
			}
			/* rhs */
			gausspoints[p].Fu[0] = 0.0;
			gausspoints[p].Fu[1] = sin((double)opts_nz*M_PI*coord_y)*cos(1.0*M_PI*coord_x);

			gausspoints[p].eta_ref = gausspoints[p].eta;
		}
	}
#endif
	
	/* define properties on material points */
	db = ctx->db;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));

	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));
	
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	ierr = QuadratureStokesGravityModelCoordinateAlignedSetValues(ctx->Q,0.0,1.0);CHKERRQ(ierr);
	
	for (p=0; p<n_mp_points; p++) {
		MPntStd     *material_point;
		MPntPStokes *mpprop_stokes;
		double  *position;
		
		DataFieldAccessPoint(PField_std,p,(void**)&material_point);
		DataFieldAccessPoint(PField_stokes,p,(void**)&mpprop_stokes);

		
		/* Access using the getter function provided for you (recommeneded for beginner user) */
		//MPntStdGetField_global_coord(material_point,&position);
		// OR
		/* Access directly into material point struct (not recommeneded for beginner user) */
		/* Direct access only recommended for efficiency */
		position = material_point->coor;

		MPntStdGetField_phase_index(material_point,&phase_init);
		

		
		rho = sin((double)opts_nz*M_PI*position[1])*cos(1.0*M_PI*position[0]);
		eta = opts_eta0;
		phase = 0;
		if (position[0] > opts_xc) {
			phase = 1;
			eta = opts_eta1;
		}
		/* for visualisation */
		if ( (position[0]>0.22) && (position[0]<0.28) ) {
			phase = 2;
		}
		/* for visualisation */
		if ( (position[1]>0.49) && (position[1]<0.51) ) {
			phase = 3;
		}
		
		
		/* user the setters provided for you */
		MPntStdSetField_phase_index(material_point,phase);

		MPntPStokesSetField_eta_effective(mpprop_stokes,eta);
		MPntPStokesSetField_density(mpprop_stokes,rho);
	}

	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokes);
	
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_SolCx"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_SolCx(pTatinCtx ctx)
{
	BCList         ubclist;
	PetscScalar    bcval;
	DM             dav;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ubclist = ctx->u_bclist;
	dav     = ctx->dav;
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	
	//	bcval = 1.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}


