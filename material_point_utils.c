
#include "pTatin2d.h"
#include "swarm_fields.h"
#include "MPntStd_def.h"
#include "MPntPStokes_def.h"
#include "MPntPStokesPl_def.h"
#include "MPntPThermal_def.h"
#include "MPntPStokesMelt_def.h"
#include "MPntPChrono_def.h"
#include "MPntPPassive_def.h"
#include "MPntPDarcy_def.h"



/* MPntStd */
#undef __FUNCT__  
#define __FUNCT__ "pTatin_MPntStd_ScaleProperties"
PetscErrorCode pTatin_MPntStd_ScaleProperties(DataBucket db,pTatinUnits *units)
{
	PetscInt          p,n_mp_points;
	DataField         PField_std;
	MPntStd           *mp_std;
	PetscErrorCode    ierr;
	
	PetscFunctionBegin;
	
	/* define properties on material points */
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);

	mp_std     = PField_std->data; /* should write a function to do this */
	for (p=0; p<n_mp_points; p++) {
		UnitsApplyScaling(units->si_length,mp_std[p].coor[0],&mp_std[p].coor[0]);
		UnitsApplyScaling(units->si_length,mp_std[p].coor[1],&mp_std[p].coor[1]);
	}
	
	DataFieldRestoreAccess(PField_std);

	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin_MPntStd_UnScaleProperties"
PetscErrorCode pTatin_MPntStd_UnScaleProperties(DataBucket db,pTatinUnits *units)
{
	PetscInt          p,n_mp_points;
	DataField         PField_std;
	MPntStd           *mp_std;
	PetscErrorCode    ierr;
	
	PetscFunctionBegin;
	
	/* define properties on material points */
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	mp_std     = PField_std->data; /* should write a function to do this */
	for (p=0; p<n_mp_points; p++) {
		UnitsApplyInverseScaling(units->si_length,mp_std[p].coor[0],&mp_std[p].coor[0]);
		UnitsApplyInverseScaling(units->si_length,mp_std[p].coor[1],&mp_std[p].coor[1]);
	}
	
	DataFieldRestoreAccess(PField_std);
	
	PetscFunctionReturn(0);
}

/* MPntPStokes */
#undef __FUNCT__  
#define __FUNCT__ "pTatin_MPntPStokes_ScaleProperties"
PetscErrorCode pTatin_MPntPStokes_ScaleProperties(DataBucket db,pTatinUnits *units)
{
	PetscInt          p,n_mp_points;
	DataField         PField_stokes;
	MPntPStokes       *mp_stokes;
	PetscErrorCode    ierr;
	
	PetscFunctionBegin;
	
	/* define properties on material points */
	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	mp_stokes     = PField_stokes->data; /* should write a function to do this */
	for (p=0; p<n_mp_points; p++) {
		UnitsApplyScaling(units->si_viscosity,mp_stokes[p].eta,&mp_stokes[p].eta);
         UnitsApplyScaling(units->si_force_per_volume,mp_stokes[p].rho,&mp_stokes[p].rho);
	}
	
	DataFieldRestoreAccess(PField_stokes);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin_MPntPStokes_UnScaleProperties"
PetscErrorCode pTatin_MPntPStokes_UnScaleProperties(DataBucket db,pTatinUnits *units)
{
	PetscInt          p,n_mp_points;
	DataField         PField_stokes;
	MPntPStokes       *mp_stokes;
	PetscErrorCode    ierr;
	
	PetscFunctionBegin;
	
	/* define properties on material points */
	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	mp_stokes     = PField_stokes->data; /* should write a function to do this */
	for (p=0; p<n_mp_points; p++) {
		UnitsApplyInverseScaling(units->si_viscosity,mp_stokes[p].eta,&mp_stokes[p].eta);
        UnitsApplyInverseScaling(units->si_force_per_volume,mp_stokes[p].rho,&mp_stokes[p].rho);
	}
	
	DataFieldRestoreAccess(PField_stokes);
	
	PetscFunctionReturn(0);
}

/* MPntPStokesElas */
#undef __FUNCT__
#define __FUNCT__ "pTatin_MPntPStokesElas_ScaleProperties"
PetscErrorCode pTatin_MPntPStokesElas_ScaleProperties(DataBucket db,pTatinUnits *units)
{
    PetscInt          p,n_mp_points;
    DataField         PField_stokeselas;
    MPntPStokesElas     *mp_stokeselas;
    PetscErrorCode    ierr;
    
    PetscFunctionBegin;
    
    /* define properties on material points */
    DataBucketGetDataFieldByName(db,MPntPStokesElas_classname,&PField_stokeselas);
    DataFieldGetAccess(PField_stokeselas);
    DataFieldVerifyAccess(PField_stokeselas,sizeof(MPntPStokesElas));
    
    DataBucketGetSizes(db,&n_mp_points,0,0);
    
    mp_stokeselas     = PField_stokeselas->data; /* should write a function to do this */
    for (p=0; p<n_mp_points; p++) {
        UnitsApplyScaling(units->si_stress,mp_stokeselas[p].sxx_r,&mp_stokeselas[p].sxx_r);
        UnitsApplyScaling(units->si_stress,mp_stokeselas[p].sxx_r,&mp_stokeselas[p].syy_r);
        UnitsApplyScaling(units->si_stress,mp_stokeselas[p].sxx_r,&mp_stokeselas[p].sxy_r);
        UnitsApplyScaling(units->si_stress,mp_stokeselas[p].sxx_r,&mp_stokeselas[p].sxx_old);
        UnitsApplyScaling(units->si_stress,mp_stokeselas[p].sxx_r,&mp_stokeselas[p].syy_old);
        UnitsApplyScaling(units->si_stress,mp_stokeselas[p].sxx_r,&mp_stokeselas[p].sxy_old);
        
    }
    
    DataFieldRestoreAccess(PField_stokeselas);
    
    PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatin_MPntPStokesElas_UnScaleProperties"
PetscErrorCode pTatin_MPntPStokesElas_UnScaleProperties(DataBucket db,pTatinUnits *units)
{
    PetscInt          p,n_mp_points;
    DataField         PField_stokeselas;
    MPntPStokesElas     *mp_stokeselas;
    PetscErrorCode    ierr;
    
    PetscFunctionBegin;
    
    /* define properties on material points */
    DataBucketGetDataFieldByName(db,MPntPStokesElas_classname,&PField_stokeselas);
    DataFieldGetAccess(PField_stokeselas);
    DataFieldVerifyAccess(PField_stokeselas,sizeof(MPntPStokesElas));
    
    DataBucketGetSizes(db,&n_mp_points,0,0);
    
    mp_stokeselas     = PField_stokeselas->data; /* should write a function to do this */
    for (p=0; p<n_mp_points; p++) {
        UnitsApplyInverseScaling(units->si_stress,mp_stokeselas[p].sxx_r,&mp_stokeselas[p].sxx_r);
        UnitsApplyInverseScaling(units->si_stress,mp_stokeselas[p].sxx_r,&mp_stokeselas[p].syy_r);
        UnitsApplyInverseScaling(units->si_stress,mp_stokeselas[p].sxx_r,&mp_stokeselas[p].sxy_r);
        UnitsApplyInverseScaling(units->si_stress,mp_stokeselas[p].sxx_r,&mp_stokeselas[p].sxx_old);
        UnitsApplyInverseScaling(units->si_stress,mp_stokeselas[p].sxx_r,&mp_stokeselas[p].syy_old);
        UnitsApplyInverseScaling(units->si_stress,mp_stokeselas[p].sxx_r,&mp_stokeselas[p].sxy_old);
    }
    
    DataFieldRestoreAccess(PField_stokeselas);
    
    PetscFunctionReturn(0);
}







/* MPntPStokesPl */
#undef __FUNCT__  
#define __FUNCT__ "pTatin_MPntPStokesPl_ScaleProperties"
PetscErrorCode pTatin_MPntPStokesPl_ScaleProperties(DataBucket db,pTatinUnits *units)
{
	PetscInt          p,n_mp_points;
	DataField         PField_stokespl;
	MPntPStokesPl     *mp_stokespl;
	PetscErrorCode    ierr;
	
	PetscFunctionBegin;
	
	/* define properties on material points */
	DataBucketGetDataFieldByName(db,MPntPStokesPl_classname,&PField_stokespl);
	DataFieldGetAccess(PField_stokespl);
	DataFieldVerifyAccess(PField_stokespl,sizeof(MPntPStokesPl));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	mp_stokespl     = PField_stokespl->data; /* should write a function to do this */
	for (p=0; p<n_mp_points; p++) {
		// strain doesn't need to be scaled 
		//UnitsApplyScaling(units->XXX,mp_stokespl[p]->e_plastic,&mp_stokespl[p]->e_plastic);
	}
	
	DataFieldRestoreAccess(PField_stokespl);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin_MPntPStokesPl_UnScaleProperties"
PetscErrorCode pTatin_MPntPStokesPl_UnScaleProperties(DataBucket db,pTatinUnits *units)
{
	PetscInt          p,n_mp_points;
	DataField         PField_stokespl;
	MPntPStokesPl     *mp_stokespl;
	PetscErrorCode    ierr;
	
	PetscFunctionBegin;
	
	/* define properties on material points */
	DataBucketGetDataFieldByName(db,MPntPStokesPl_classname,&PField_stokespl);
	DataFieldGetAccess(PField_stokespl);
	DataFieldVerifyAccess(PField_stokespl,sizeof(MPntPStokesPl));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	mp_stokespl     = PField_stokespl->data; /* should write a function to do this */
	for (p=0; p<n_mp_points; p++) {
		// strain doesn't need to be scaled 
		//UnitsApplyInverseScaling(units->XXX,mp_stokespl[p]->e_plastic,&mp_stokespl[p]->e_plastic);
	}
	
	DataFieldRestoreAccess(PField_stokespl);
	
	PetscFunctionReturn(0);
}

/* MPntPThermal */
#undef __FUNCT__  
#define __FUNCT__ "pTatin_MPntPThermal_ScaleProperties"
PetscErrorCode pTatin_MPntPThermal_ScaleProperties(DataBucket db,pTatinUnits *units)
{
	PetscInt          p,n_mp_points;
	DataField         PField_thermal;
	MPntPThermal      *mp_thermal;
	PetscErrorCode    ierr;
	
	PetscFunctionBegin;
	
	
	/* define properties on material points */
	DataBucketGetDataFieldByName(db,MPntPThermal_classname,&PField_thermal);
	DataFieldGetAccess(PField_thermal);
	DataFieldVerifyAccess(PField_thermal,sizeof(MPntPThermal));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	mp_thermal     = PField_thermal->data; /* should write a function to do this */

	for (p=0; p<n_mp_points; p++) {
		
		UnitsApplyScaling(units->si_diffusivity,mp_thermal[p].kappa,&mp_thermal[p].kappa);
		UnitsApplyScaling(units->si_heatsource,mp_thermal[p].H,&mp_thermal[p].H);
        UnitsApplyScaling(units->si_heatsource,mp_thermal[p].H0,&mp_thermal[p].H0);
	}
	
	DataFieldRestoreAccess(PField_thermal);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin_MPntPThermal_UnScaleProperties"
PetscErrorCode pTatin_MPntPThermal_UnScaleProperties(DataBucket db,pTatinUnits *units)
{
	PetscInt          p,n_mp_points;
	DataField         PField_thermal;
	MPntPThermal      *mp_thermal;
	PetscErrorCode    ierr;
	
	PetscFunctionBegin;
	
	/* define properties on material points */
	DataBucketGetDataFieldByName(db,MPntPThermal_classname,&PField_thermal);
	DataFieldGetAccess(PField_thermal);
	DataFieldVerifyAccess(PField_thermal,sizeof(MPntPThermal));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	mp_thermal     = PField_thermal->data; /* should write a function to do this */
	
	for (p=0; p<n_mp_points; p++) {
		
		UnitsApplyInverseScaling(units->si_diffusivity,mp_thermal[p].kappa,&mp_thermal[p].kappa);
		UnitsApplyInverseScaling(units->si_heatsource,mp_thermal[p].H,&mp_thermal[p].H);
        UnitsApplyInverseScaling(units->si_heatsource,mp_thermal[p].H0,&mp_thermal[p].H0);
	}
	
	DataFieldRestoreAccess(PField_thermal);
	
	PetscFunctionReturn(0);
}

/* MPntPThermal */

#undef __FUNCT__  
#define __FUNCT__ "pTatin_MPntPChrono_ScaleProperties"
PetscErrorCode pTatin_MPntPChrono_ScaleProperties(DataBucket db,pTatinUnits *units)
{
	PetscInt          p,n_mp_points;
	DataField         PField_chrono;
	MPntPChrono      *mp_chrono;
	PetscErrorCode    ierr;
	double            age;
	PetscFunctionBegin;
	
	/* define properties on material points */
	DataBucketGetDataFieldByName(db,MPntPChrono_classname,&PField_chrono);
	DataFieldGetAccess(PField_chrono);
	DataFieldVerifyAccess(PField_chrono,sizeof(MPntPChrono));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	mp_chrono     = PField_chrono->data; /* should write a function to do this */

	for (p=0; p<n_mp_points; p++) {
	
	    age = (double)mp_chrono[p].age120;
		UnitsApplyScaling(units->si_time,age,&age);
		mp_chrono[p].age120=(float)age;
	
	    age = (double)mp_chrono[p].age350;
		UnitsApplyScaling(units->si_time,age,&age);
		mp_chrono[p].age350=(float)age;
		
		age = (double)mp_chrono[p].age800;
		UnitsApplyScaling(units->si_time,age,&age);
		mp_chrono[p].age800=(float)age;
	}
	
	DataFieldRestoreAccess(PField_chrono);
	
	PetscFunctionReturn(0);
}

/* MPntPThermal */
#undef __FUNCT__  
#define __FUNCT__ "pTatin_MPntPChrono_UnScaleProperties"
PetscErrorCode pTatin_MPntPChrono_UnScaleProperties(DataBucket db,pTatinUnits *units)
{
	PetscInt          p,n_mp_points;
	DataField         PField_chrono;
	MPntPChrono      *mp_chrono;
	PetscErrorCode    ierr;
	double            age; 
	PetscFunctionBegin;
	
	/* define properties on material points */
	DataBucketGetDataFieldByName(db,MPntPChrono_classname,&PField_chrono);
	DataFieldGetAccess(PField_chrono);
	DataFieldVerifyAccess(PField_chrono,sizeof(MPntPChrono));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	mp_chrono     = PField_chrono->data; /* should write a function to do this */

	for (p=0; p<n_mp_points; p++) {
		
		age = (double)mp_chrono[p].age120;
		UnitsApplyInverseScaling(units->si_time,age,&age);
		mp_chrono[p].age120=(float)age;
	
	    age = (double)mp_chrono[p].age350;
		UnitsApplyInverseScaling(units->si_time,age,&age);
		mp_chrono[p].age350=(float)age;
		
		age = (double)mp_chrono[p].age800;
		UnitsApplyInverseScaling(units->si_time,age,&age);
		mp_chrono[p].age800=(float)age;
		
	}
	
	DataFieldRestoreAccess(PField_chrono);
	
	PetscFunctionReturn(0);
}

#if 0
{
#undef __FUNCT__  
#define __FUNCT__ "pTatin_MPntPDarcy_ScaleProperties"
PetscErrorCode pTatin_MPntPDarcy_ScaleProperties(DataBucket db,pTatinUnits *units)
{
	PetscInt          p,n_mp_points;
	DataField         PField_darcy;
	MPntPChrono      *mp_darcy;
	PetscErrorCode    ierr;
	double            age;
	PetscFunctionBegin;
	
	/* define properties on material points */
	DataBucketGetDataFieldByName(db,MPntPDarcy_classname,&PField_darcy);
	DataFieldGetAccess(PField_darcy);
	DataFieldVerifyAccess(PField_darcy,sizeof(MPntPDarcy));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	mp_darcy     = PField_darcy->data; /* should write a function to do this */

	for (p=0; p<n_mp_points; p++) {
	
	    age = (double)mp_chrono[p].age120;
		UnitsApplyScaling(units->si_time,age,&age);
		mp_chrono[p].age120=(float)age;
	
	    age = (double)mp_chrono[p].age350;
		UnitsApplyScaling(units->si_time,age,&age);
		mp_chrono[p].age350=(float)age;
		
		age = (double)mp_chrono[p].age800;
		UnitsApplyScaling(units->si_time,age,&age);
		mp_chrono[p].age800=(float)age;
	}
	
	DataFieldRestoreAccess(PField_darcy);
	
	PetscFunctionReturn(0);
}

/* MPntPThermal */
#undef __FUNCT__  
#define __FUNCT__ "pTatin_MPntPDarcy_UnScaleProperties"
PetscErrorCode pTatin_MPntPDarcy_UnScaleProperties(DataBucket db,pTatinUnits *units)
{
	PetscInt          p,n_mp_points;
	DataField         PField_darcy;
	MPntPChrono      *mp_darcy;
	PetscErrorCode    ierr;
	double            age; 
	PetscFunctionBegin;
	
	/* define properties on material points */
	DataBucketGetDataFieldByName(db,MPntPDarcy_classname,&PField_chrono);
	DataFieldGetAccess(PField_darcy);
	DataFieldVerifyAccess(PField_darcy,sizeof(MPntPDarcy));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	mp_darcy     = PField_darcy->data; /* should write a function to do this */

	for (p=0; p<n_mp_points; p++) {
		
		age = (double)mp_chrono[p].age120;
		UnitsApplyInverseScaling(units->si_time,age,&age);
		mp_chrono[p].age120=(float)age;
	
	    age = (double)mp_chrono[p].age350;
		UnitsApplyInverseScaling(units->si_time,age,&age);
		mp_chrono[p].age350=(float)age;
		
		age = (double)mp_chrono[p].age800;
		UnitsApplyInverseScaling(units->si_time,age,&age);
		mp_chrono[p].age800=(float)age;
		
	}
	
	DataFieldRestoreAccess(PField_darcy);
	
	PetscFunctionReturn(0);
}
}
#endif



#undef __FUNCT__ 
#define __FUNCT__ "pTatin_MPntPPassive_ScaleProperties"
PetscErrorCode pTatin_MPntPPassive_ScaleProperties(DataBucket db,pTatinUnits *units)
{
	PetscInt          p,n_mp_points;
	DataField         PField_passive;
	MPntPPassive      *mp_passive;
	PetscErrorCode    ierr;
	double            P,T; 
	
	PetscFunctionBegin;
	
	/* define properties on material points */
	DataBucketGetDataFieldByName(db,MPntPPassive_classname,&PField_passive);
	DataFieldGetAccess(PField_passive);
	DataFieldVerifyAccess(PField_passive,sizeof(MPntPPassive));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	mp_passive     = PField_passive->data; /* should write a function to do this */

	for (p=0; p<n_mp_points; p++) {
		
	    P = (double)mp_passive[p].P;
		T = (double)mp_passive[p].T;
		UnitsApplyScaling(units->si_stress,P,&P);
		UnitsApplyScaling(units->si_temperature,T,&T);
        mp_passive[p].P = (float)P;
        mp_passive[p].T = (float)T;
	}
	
	DataFieldRestoreAccess(PField_passive);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__ 
#define __FUNCT__ "pTatin_MPntPPassive_UnScaleProperties"
PetscErrorCode pTatin_MPntPPassive_UnScaleProperties(DataBucket db,pTatinUnits *units)
{
	PetscInt          p,n_mp_points;
	DataField         PField_passive;
	MPntPPassive      *mp_passive;
	double            P,T; 
	PetscErrorCode    ierr;
	
	PetscFunctionBegin;
	
	/* define properties on material points */
	DataBucketGetDataFieldByName(db,MPntPPassive_classname,&PField_passive);
	DataFieldGetAccess(PField_passive);
	DataFieldVerifyAccess(PField_passive,sizeof(MPntPPassive));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	mp_passive     = PField_passive->data; /* should write a function to do this */

	for (p=0; p<n_mp_points; p++) {
		P = (double)mp_passive[p].P;
		T = (double)mp_passive[p].T;
		UnitsApplyInverseScaling(units->si_stress,P,&P);
		UnitsApplyInverseScaling(units->si_temperature,T,&T);
        mp_passive[p].P = (float)P;
        mp_passive[p].T = (float)T;
		
	}
	
	DataFieldRestoreAccess(PField_passive);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_pTatin_MaterialPointScaleProperties"
PetscErrorCode _pTatin_MaterialPointScaleProperties(DataBucket db,pTatinUnits *units,PetscBool unscale)
{
	BTruth            has_field;
	PetscErrorCode    ierr;
	
	PetscFunctionBegin;

	/* std */
	DataBucketQueryDataFieldByName(db,MPntStd_classname,&has_field);
	if( has_field) {
		if (unscale==PETSC_FALSE) {
			ierr = pTatin_MPntStd_ScaleProperties(db,units);CHKERRQ(ierr);
		} else {
			ierr = pTatin_MPntStd_UnScaleProperties(db,units);CHKERRQ(ierr);
		}
	}

	/* stokes */
	DataBucketQueryDataFieldByName(db,MPntPStokes_classname,&has_field);
	if( has_field) {
		if (unscale==PETSC_FALSE) {
			ierr = pTatin_MPntPStokes_ScaleProperties(db,units);CHKERRQ(ierr);
		} else {
			ierr = pTatin_MPntPStokes_UnScaleProperties(db,units);CHKERRQ(ierr);
		}
	}

	/* stokes_pl */
	DataBucketQueryDataFieldByName(db,MPntPStokesPl_classname,&has_field);
	if( has_field) {
		if (unscale==PETSC_FALSE) {
			ierr = pTatin_MPntPStokesPl_ScaleProperties(db,units);CHKERRQ(ierr);
		} else {
			ierr = pTatin_MPntPStokesPl_UnScaleProperties(db,units);CHKERRQ(ierr);
		}
	}
	
    /* stokes_pl */
    DataBucketQueryDataFieldByName(db,MPntPStokesElas_classname,&has_field);
    if( has_field) {
        if (unscale==PETSC_FALSE) {
            ierr = pTatin_MPntPStokesElas_ScaleProperties(db,units);CHKERRQ(ierr);
        } else {
            ierr = pTatin_MPntPStokesElas_UnScaleProperties(db,units);CHKERRQ(ierr);
        }
    }
    
    
    

	/* thermal */
	DataBucketQueryDataFieldByName(db,MPntPThermal_classname,&has_field);
	if( has_field) {
		if (unscale==PETSC_FALSE) {
			ierr = pTatin_MPntPThermal_ScaleProperties(db,units);CHKERRQ(ierr);
		} else {
			ierr = pTatin_MPntPThermal_UnScaleProperties(db,units);CHKERRQ(ierr);
		}
	}
	
	/* thermochro */
	DataBucketQueryDataFieldByName(db,MPntPChrono_classname,&has_field);
	if( has_field) {
		if (unscale==PETSC_FALSE) {
			ierr = pTatin_MPntPChrono_ScaleProperties(db,units);CHKERRQ(ierr);
		} else {
			ierr = pTatin_MPntPChrono_UnScaleProperties(db,units);CHKERRQ(ierr);
		}
	}
#if 0
	/* darcy */
	DataBucketQueryDataFieldByName(db,MPntPDarcy_classname,&has_field);
	if( has_field) {
		if (unscale==PETSC_FALSE) {
			ierr = pTatin_MPntPDarcy_ScaleProperties(db,units);CHKERRQ(ierr);
		} else {
			ierr = pTatin_MPntPDarcy_UnScaleProperties(db,units);CHKERRQ(ierr);
		}
	}
#endif
	/* PT-PATHS */
	DataBucketQueryDataFieldByName(db,MPntPPassive_classname,&has_field);
	if( has_field) {
		if (unscale==PETSC_FALSE) {
			ierr = pTatin_MPntPPassive_ScaleProperties(db,units);CHKERRQ(ierr);
		} else {
			ierr = pTatin_MPntPPassive_UnScaleProperties(db,units);CHKERRQ(ierr);
		}
	}

	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatin_MaterialPointScaleProperties"
PetscErrorCode pTatin_MaterialPointScaleProperties(DataBucket db,pTatinUnits *units)
{
	PetscErrorCode    ierr;
	PetscFunctionBegin;
	ierr = _pTatin_MaterialPointScaleProperties(db,units,PETSC_FALSE);CHKERRQ(ierr);
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatin_MaterialPointUnScaleProperties"
PetscErrorCode pTatin_MaterialPointUnScaleProperties(DataBucket db,pTatinUnits *units)
{
	PetscErrorCode    ierr;
	PetscFunctionBegin;
	ierr = _pTatin_MaterialPointScaleProperties(db,units,PETSC_TRUE);CHKERRQ(ierr);
	PetscFunctionReturn(0);
}


