/*
 
 Model Description:
 Notch model  
 Input / command line parameters:
 
 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"
#include "rheology.h"

/*	
 length_bar    = 10.0 * 1.0e3; 
 viscosity_bar = 1.0e25;
 velocity_bar  = 2.0e-11;
 */
PetscErrorCode pTatin2d_ModelSetMaterialPropertyFromOptions_NotchGP(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelSetMarkerIndex_NotchGP(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelSetGaussPointIndex_NotchGP(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelSetInitialStokesVariableOnMarker_NotchGP(pTatinCtx ctx);
PetscErrorCode pTatin2d_ModelSetInitialStokesVariableOnGaussPoint_NotchGP(pTatinCtx ctx);

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_NotchGP"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_NotchGP(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	
	PetscReal  opts_Lx,opts_Ly,gravity_acceleration;	
	PetscInt i,j,si,sj,m,n,M,N,ndof;
	PetscBool found; 
	DM cda;
	Vec coords;
	DMDACoor2d **LA_coords;
	pTatinUnits *units;	
	
	PetscFunctionBegin;
	/**/
	units   = &ctx->units;  
	UnitsApplyInverseScaling(units->si_time,ctx->dt_min,&ctx->dt_min);
	UnitsApplyInverseScaling(units->si_time,ctx->dt,&ctx->dt);
	UnitsApplyInverseScaling(units->si_time,ctx->dt_adv,&ctx->dt_adv);
	UnitsApplyInverseScaling(units->si_time,ctx->dt_max,&ctx->dt_max);
	UnitsApplyInverseScaling(units->si_time,ctx->time_max,&ctx->time_max);
	UnitsApplyInverseScaling(units->si_length,ctx->max_disp_y,&ctx->max_disp_y);
	
	
	/*set the model size */ 
	opts_Lx   = 60.0 * 1.0e3;
	opts_Ly   = 20.0 * 1.0e3;
	
	ierr = PetscOptionsGetReal("NotchGP_","-Lx",&opts_Lx,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal("NotchGP_","-Ly",&opts_Ly,0);CHKERRQ(ierr);  
	UnitsApplyInverseScaling(units->si_length,opts_Lx,&opts_Lx);
	UnitsApplyInverseScaling(units->si_length,opts_Ly,&opts_Ly);    
	
	ierr = DMDASetUniformCoordinates(ctx->dav,0.0,opts_Lx, 0.0,opts_Ly, 0.0,0.0);CHKERRQ(ierr);
	
	gravity_acceleration = 9.81; 
	ierr  = PetscOptionsGetReal(PETSC_NULL,"-grav",&gravity_acceleration,&found);CHKERRQ(ierr);
	ierr = QuadratureStokesGravityModelCoordinateAlignedSetValues(ctx->Q,0.0, -gravity_acceleration);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}
#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_NotchGP"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_NotchGP(pTatinCtx ctx)
{
	BCList         ubclist;
	PetscScalar    bcval;
	DM             dav;
	PetscErrorCode ierr;
	PetscReal jmin[3],jmax[3],Vbar; 
	PetscScalar opts_srH, opts_Lx,opts_Ly;
	PetscInt opts_bcs;
	PetscScalar alpha_m;
	pTatinUnits *units;	
	
	PetscFunctionBegin;
	
	ubclist = ctx->u_bclist;
	dav     = ctx->dav;
	units   = &ctx->units;  
	opts_srH = -2.0 * 1.0e-11;
 	ierr = PetscOptionsGetScalar(PETSC_NULL,"-vx",&opts_srH,0);CHKERRQ(ierr);
	UnitsApplyInverseScaling(units->si_velocity,opts_srH,&opts_srH);
	opts_bcs = 0;
	ierr = PetscOptionsGetInt(PETSC_NULL,"-bc_type",&opts_bcs,0);CHKERRQ(ierr);
	
	
	DMDAGetBoundingBox(dav,jmin,jmax);
	opts_Lx = jmax[0]-jmin[0]; 
	opts_Ly = jmax[1]-jmin[1];
	PetscPrintf(PETSC_COMM_WORLD,"domain x := [ %1.4e , %1.4e ] \n", jmin[0], jmax[0] );
	PetscPrintf(PETSC_COMM_WORLD,"domain y := [ %1.4e , %1.4e ] \n", jmin[1], jmax[1] );
	
	switch(opts_bcs){
			// free surface and free slip all side with symetric horizontal velocity
		case 0 : 
		{
			bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
			break; 
		}
			
			// incompressible symetric bottom fixed 
		case 1 : 
		{
			bcval = -opts_srH*2.0*opts_Ly/opts_Lx;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
			break; 
		}
			
			// incompressible right side  
		case 2 : 
		{
			bcval = -opts_srH*opts_Ly/opts_Lx;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
			break;
		}
			
			// incompressible 4 sides  
		case 3 : {      
			bcval = -opts_srH*opts_Ly/opts_Lx;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
			bcval = opts_srH*opts_Ly/opts_Lx;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
			bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval =  opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
			break;
		}
			
			// free slip everywhere  
		case 4 : 
		{
			bcval = 0.0;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			break;      
		}
			
			// free surface both side with mid point fixed 
		case 5 :  
		{
			bcval = opts_srH;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_MIDPOINT,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_MIDPOINT,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);      
			
			break;
		}
			
			// free surface right side  
		case 6 : 
		{
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			//bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
			break;
		}  
			
			// free surface left side  
		case 7 : 
		{
			bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
			break;
		}
			
		default: 
		{
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Boundary condition specified is unknown");
			break;
		}
	} 
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelUpdateMeshGeometry_NotchGP"
PetscErrorCode pTatin2d_ModelUpdateMeshGeometry_NotchGP(pTatinCtx ctx,Vec X)
{
	PetscReal step;
	Vec velocity,pressure;
	DM dav,dap;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	step = ctx->dt;
	
	ierr = DMCompositeGetEntries(ctx->pack,&dav,&dap);CHKERRQ(ierr);
	ierr = DMCompositeGetAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
	ierr = UpdateMeshGeometry_FullLagSurfaceJMAX_RemeshInteriorJMIN_JMAX(dav,velocity,step);CHKERRQ(ierr);
	ierr = DMCompositeRestoreAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_NotchGP"
PetscErrorCode     pTatin2d_ModelApplyInitialMaterialGeometry_NotchGP(pTatinCtx ctx)
{
	PetscErrorCode         ierr;
	PetscBool use_gausspoint,found;
	
	PetscFunctionBegin;
	use_gausspoint = PETSC_FALSE; 
	
	ierr = PetscOptionsGetBool(PETSC_NULL,"-update_nonlinearities_qp",&use_gausspoint,&found);CHKERRQ(ierr);
	
	ierr = pTatin2d_ModelSetMaterialPropertyFromOptions_NotchGP(ctx); CHKERRQ(ierr);
	
	if (use_gausspoint) {
		ierr = pTatin2d_ModelSetGaussPointIndex_NotchGP(ctx); CHKERRQ(ierr);
		ierr = pTatin2d_ModelSetInitialStokesVariableOnGaussPoint_NotchGP(ctx); CHKERRQ(ierr);
	} else {
		
		ierr = pTatin2d_ModelSetMarkerIndex_NotchGP(ctx); CHKERRQ(ierr);
		ierr = pTatin2d_ModelSetInitialStokesVariableOnMarker_NotchGP(ctx); CHKERRQ(ierr);
	}
	
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__ 
#define __FUNCT__ "pTatin2d_ModelSetMaterialPropertyFromOptions_NotchGP"
PetscErrorCode pTatin2d_ModelSetMaterialPropertyFromOptions_NotchGP(pTatinCtx ctx)

{
	RheologyConstants      *rheology;
	PetscInt               nphase, i,rheol_type;
	PetscErrorCode         ierr;
	char   				   *option_name;
	PetscBool              found,use_softening,use_mises,use_damage; 
	
	PetscFunctionBegin;
	
	ctx->rheology_constants.rheology_type = RHEOLOGY_VPT_STD;
	rheology   = &ctx->rheology_constants;
	nphase     = 2;
	
	rheology->eta_upper_cutoff_global = 1.0e30;
	rheology->eta_lower_cutoff_global = 1.0e19;
	ierr       = PetscOptionsGetReal("NotchGP_","-eta_max_cut_off",&rheology->eta_upper_cutoff_global,0);CHKERRQ(ierr);
	ierr       = PetscOptionsGetReal("NotchGP_","-eta_min_cut_off",&rheology->eta_lower_cutoff_global,0);CHKERRQ(ierr);
	
	
	rheology->density_type[0]    = DENSITY_CONSTANT;
	rheology->const_rho0[0]      = 2700.0; 
	rheology->viscous_type[0]    = VISCOUS_CONSTANT;
	rheology->const_eta0[0]      = 1.0e25;
	rheology->plastic_type[0]    = PLASTIC_DP; 
	rheology->mises_tau_yield[0] = 2.0e8;
	rheology->dp_pressure_dependance[0] = 30.0; // CHECK ANGLE DEG RAD OR COEFF
	
	use_mises = PETSC_FALSE;
	ierr  = PetscOptionsGetBool("NotchGP_","-use_mises",&use_mises,&found);CHKERRQ(ierr);
	if (use_mises) {
		rheology->plastic_type[0]    = PLASTIC_M; 
		rheology->mises_tau_yield[0] = 2.0e8;
	}
    
    use_damage = PETSC_FALSE;
	ierr  = PetscOptionsGetBool("NotchGP_","-use_damage",&use_damage,&found);CHKERRQ(ierr);
	if (use_damage) {
		rheology->plastic_type[0]    = PLASTIC_DAMAGE; 
        rheology->dam_eta_h_0[0]     = 1.e24;
        rheology->dam_eta_h_inf[0]   = 1.e22;
        rheology->dam_eps_max[0]     = 0.05;
        rheology->dam_eps_min[0]     = 0.0;
        rheology->dam_healing_rate[0]= 0.0;
        rheology->dam_factor[0]      = 4;
        ierr = PetscOptionsGetScalar("NotchGP_","-eta_h_0",&(rheology->dam_eta_h_0[0]),&found);CHKERRQ(ierr);
        ierr = PetscOptionsGetScalar("NotchGP_","-eta_h_inf",&(rheology->dam_eta_h_inf[0]),&found);CHKERRQ(ierr);
        ierr = PetscOptionsGetScalar("NotchGP_","-dam_eps_max",&(rheology->dam_eps_max[0]),&found);CHKERRQ(ierr);
        ierr = PetscOptionsGetScalar("NotchGP_","-dam_eps_min",&(rheology->dam_eps_min[0]),&found);CHKERRQ(ierr);
        ierr = PetscOptionsGetInt("NotchGP_","-dam_factor",&(rheology->dam_factor[0]),&found);CHKERRQ(ierr);
        
        
	}
	

	ierr = PetscOptionsGetReal("NotchGP_","-Co",&(rheology->mises_tau_yield[0]),&found);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal("NotchGP_","-Phi",&(rheology->dp_pressure_dependance[0]),&found);CHKERRQ(ierr);
	rheology->tens_cutoff[0]     = rheology->mises_tau_yield[0]/10;
	rheology->Hst_cutoff[0]      = 5.0e10;
	ierr = PetscOptionsGetReal("NotchGP_","-Tens",&(rheology->tens_cutoff[0]),&found);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal("NotchGP_","-Hs",&(rheology->Hst_cutoff[0]),&found);CHKERRQ(ierr);
	rheology->softening_type[0]  = SOFTENING_NONE; 
	
	use_softening = PETSC_FALSE;
	ierr = PetscOptionsGetBool("NotchGP_","-use_softening",&use_softening,&found);CHKERRQ(ierr);
	if (use_softening){
		rheology->softening_type[0]         = SOFTENING_LINEAR; 
		rheology->soft_Co_inf[0]            = 2.0e5;
		rheology->soft_phi_inf[0]           = 30.0;
		rheology->soft_min_strain_cutoff[0] = 0.0;
		rheology->soft_max_strain_cutoff[0] = 0.001;
		ierr = PetscOptionsGetReal("NotchGP_","-Co_inf",&(rheology->soft_Co_inf[0]),&found);CHKERRQ(ierr);
		ierr = PetscOptionsGetReal("NotchGP_","-Phi_inf",&(rheology->soft_phi_inf[0]),&found);CHKERRQ(ierr);
		ierr = PetscOptionsGetReal("NotchGP_","-eps_min",&(rheology->soft_min_strain_cutoff[0]),&found);CHKERRQ(ierr);
		ierr = PetscOptionsGetReal("NotchGP_","-eps_max",&(rheology->soft_max_strain_cutoff[0]),&found);CHKERRQ(ierr);
	}
	rheology->density_type[1]   = DENSITY_CONSTANT;
	rheology->const_rho0[1]     = 2700.0; 
	rheology->viscous_type[1]   = VISCOUS_CONSTANT;
	rheology->const_eta0[1]     = 1.0e22; 
	rheology->plastic_type[1]   = PLASTIC_NONE; 
	rheology->softening_type[1] = SOFTENING_NONE;     
	
	for (i=0; i<nphase; i++) {
		
		asprintf(&option_name, "-rho_%d",i);
		ierr = PetscOptionsGetReal("NotchGP_",option_name,&(rheology->const_rho0[i]),&found);CHKERRQ(ierr);
		free(option_name);
		
		asprintf(&option_name, "-eta_%d",i);
		ierr = PetscOptionsGetReal("NotchGP_",option_name,&(rheology->const_eta0[i]),&found);CHKERRQ(ierr);
		free(option_name);
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetMarkerIndex_NotchGP"
PetscErrorCode pTatin2d_ModelSetMarkerIndex_NotchGP(pTatinCtx ctx)
{
	PetscInt               p,n_mp_points;
	DataBucket             db;
	DataField              PField_std;
	PetscReal              opts_NotchMaxX,opts_NotchMinX,opts_NotchMaxY,opts_NotchMinY,opts_Lx;
	int                    phase;
	pTatinUnits *units;
	PetscErrorCode         ierr;
	
	PetscFunctionBegin;
	units   = &ctx->units;
    ierr = PetscOptionsGetReal("NotchGP_","-Lx",&opts_Lx,0);CHKERRQ(ierr);
	
	opts_NotchMinY   = 0.0e3;	
	opts_NotchMaxY   = 1.0e3;
	opts_NotchMinX   = opts_Lx/2.0 - 1.0 * 1000.0;
	opts_NotchMaxX   = opts_Lx/2.0 + 1.0 * 1000.0;
	ierr = PetscOptionsGetReal("NotchGP_","-NotchMinY",&opts_NotchMinY,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal("NotchGP_","-NotchMaxY",&opts_NotchMaxY,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal("NotchGP_","-NotchMinX",&opts_NotchMinX,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal("NotchGP_","-NotchMaxX",&opts_NotchMaxX,0);CHKERRQ(ierr);
	UnitsApplyInverseScaling(units->si_length,opts_NotchMinY,&opts_NotchMinY);
	UnitsApplyInverseScaling(units->si_length,opts_NotchMaxY,&opts_NotchMaxY);
	UnitsApplyInverseScaling(units->si_length,opts_NotchMinX,&opts_NotchMinX);
	UnitsApplyInverseScaling(units->si_length,opts_NotchMaxX,&opts_NotchMaxX);
	
	
	/* define phase on material points */
	db = ctx->db;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	for (p=0; p<n_mp_points; p++) {
		MPntStd       *material_point;
		double  *position;
		
		DataFieldAccessPoint(PField_std,p,(void**)&material_point);
		/* Access using the getter function  (recommeneded for beginner user) */
		MPntStdGetField_global_coord(material_point,&position);
		
		/* Layer default */
		phase = 0;
		
		if ((position[1] < opts_NotchMaxY) && (position[0] >= opts_NotchMinX) && (position[0] <= opts_NotchMaxX) ) {
			/* point located in the notch */
			phase = 1;
		}	
		
		if ((position[1] < opts_NotchMinY)) {
			/* point located in the notch */
			phase = 1;
		}	
		/* asign phases and initializes viscosity to ref viscosity*/
		MPntStdSetField_phase_index(material_point,phase);
		
	}
	
	DataFieldRestoreAccess(PField_std);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetGaussPointIndex_NotchGP"
PetscErrorCode pTatin2d_ModelSetGaussPointIndex_NotchGP(pTatinCtx ctx)
{
	PetscInt e,ncells;
	PetscInt p,ngp;
	GaussPointCoefficientsStokes *gausspoints;
	PetscReal              opts_NotchMaxX,opts_NotchMinX,opts_NotchMaxY,opts_NotchMinY,opts_Lx;
	PetscErrorCode ierr;
	pTatinUnits *units;	
	
	PetscFunctionBegin;
	units   = &ctx->units;  
	ncells = ctx->Q->ncells;
	ngp    = ctx->Q->ngp;
	
    ierr = PetscOptionsGetReal("NotchGP_","-Lx",&opts_Lx,0);CHKERRQ(ierr);
	
	opts_NotchMinY   = 0.0e3;	
	opts_NotchMaxY   = 1.0e3;
	opts_NotchMinX   = opts_Lx/2.0 - 1.0 * 1000.0;
	opts_NotchMaxX   = opts_Lx/2.0 + 1.0 * 1000.0;
    
	ierr = PetscOptionsGetReal("NotchGP_","-NotchMinY",&opts_NotchMinY,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal("NotchGP_","-NotchMaxY",&opts_NotchMaxY,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal("NotchGP_","-NotchMinX",&opts_NotchMinX,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal("NotchGP_","-NotchMaxX",&opts_NotchMaxX,0);CHKERRQ(ierr);
	UnitsApplyInverseScaling(units->si_length,opts_NotchMinY,&opts_NotchMinY);
	UnitsApplyInverseScaling(units->si_length,opts_NotchMaxY,&opts_NotchMaxY);
	UnitsApplyInverseScaling(units->si_length,opts_NotchMinX,&opts_NotchMinX);
	UnitsApplyInverseScaling(units->si_length,opts_NotchMaxX,&opts_NotchMaxX);
	
	for (e=0; e<ncells; e++) {
		ierr = QuadratureStokesGetCell(ctx->Q,e,&gausspoints);CHKERRQ(ierr);        
		for (p=0; p<ngp; p++) {
			PetscScalar coord_x = gausspoints[p].coord[0];
			PetscScalar coord_y = gausspoints[p].coord[1];
			
			gausspoints[p].phase = 0; 
			if ((coord_y < opts_NotchMaxY) && (coord_x > opts_NotchMinX) && (coord_x < opts_NotchMaxX) ) {
				gausspoints[p].phase = 1;
			}
			if (coord_y < opts_NotchMinY) {
				/* point located in the notch */
				gausspoints[p].phase = 1;
			}	
			
		}
	}
	PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetInitialStokesVariableOnMarker_NotchGP"
PetscErrorCode pTatin2d_ModelSetInitialStokesVariableOnMarker_NotchGP(pTatinCtx ctx)
/* define properties on material points */
{
	PetscBool              random_noise, found; 
	PetscInt               e,p,n_mp_points;
	DataBucket             db;
	DataField              PField_std,PField_stokes,PField_stokespl;
	int                    phase_index, i;
	PetscErrorCode         ierr;
	RheologyConstants      *rheology;
	double                 eta_ND,rho_ND; 
	pTatinUnits            *units;	
	
	PetscFunctionBegin;
	units   = &ctx->units;
	
	rheology   = &ctx->rheology_constants;
	db = ctx->db;
    random_noise = PETSC_FALSE;
	ierr = PetscOptionsGetBool("Notch_","-use_random_noise",&random_noise,&found);CHKERRQ(ierr);
	
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));
	
	DataBucketGetDataFieldByName(db,MPntPStokesPl_classname,&PField_stokespl);
	DataFieldGetAccess(PField_stokespl);
	DataFieldVerifyAccess(PField_stokespl,sizeof(MPntPStokesPl));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	for (p=0; p<n_mp_points; p++) {
		MPntStd       *material_point;
		MPntPStokes   *mpprop_stokes;
		MPntPStokesPl *mpprop_stokespl;
		
		DataFieldAccessPoint(PField_std,p,(void**)&material_point);
		DataFieldAccessPoint(PField_stokes,p,(void**)&mpprop_stokes);
		
		MPntStdGetField_phase_index(material_point,&phase_index);
		UnitsApplyInverseScaling(units->si_viscosity,rheology->const_eta0[phase_index],&eta_ND);
		UnitsApplyInverseScaling(units->si_force_per_volume,rheology->const_rho0[phase_index],&rho_ND);
		MPntPStokesSetField_eta_effective(mpprop_stokes,eta_ND);
		MPntPStokesSetField_density(mpprop_stokes,rho_ND);	
		
		DataFieldAccessPoint(PField_stokespl,p,(void**)&mpprop_stokespl);
		MPntPStokesPlSetField_plastic_strain(mpprop_stokespl,0.0);
		MPntPStokesPlSetField_yield_indicator(mpprop_stokespl,0.0);
		
		if ((phase_index == 0) && random_noise) {
			PetscScalar pls; 
			pls = 0.03 * rand() / (RAND_MAX + 1.0);
			MPntPStokesPlSetField_plastic_strain(mpprop_stokespl,pls);
		}
	}
	
	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokes);
	DataFieldRestoreAccess(PField_stokespl);
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatin2d_ModelSetInitialStokesVariableOnGaussPoint_NotchGP"
PetscErrorCode pTatin2d_ModelSetInitialStokesVariableOnGaussPoint_NotchGP(pTatinCtx ctx)
/* define properties on gauss points */
{
	
	int                    phase_index, i;
	RheologyConstants      *rheology;
	PetscBool              random_noise, found; 
	double                 eta_ND,rho_ND; 
	PetscInt e,ncells;
	PetscInt p,ngp;
	GaussPointCoefficientsStokes *gausspoints;
	pTatinUnits *units;	
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	rheology   = &ctx->rheology_constants;
	ncells = ctx->Q->ncells;
	ngp    = ctx->Q->ngp;
    random_noise = PETSC_FALSE;
	ierr = PetscOptionsGetBool("Notch_","-use_random_noise",&random_noise,&found);CHKERRQ(ierr);
	units   = &ctx->units;  
	for (e=0;e<ncells;e++) {
		ierr = QuadratureStokesGetCell(ctx->Q,e,&gausspoints);CHKERRQ(ierr);        
		for (p=0; p<ngp; p++) {
			phase_index= gausspoints[p].phase; 
			UnitsApplyInverseScaling(units->si_viscosity,rheology->const_eta0[phase_index],&eta_ND);
			UnitsApplyInverseScaling(units->si_force_per_volume,rheology->const_rho0[phase_index],&rho_ND);
			gausspoints[p].Fu[0]   = gausspoints[p].gravity[0]*rho_ND;
			gausspoints[p].Fu[1]   = gausspoints[p].gravity[1]*rho_ND;
			gausspoints[p].pls     = 0.0;
			gausspoints[p].eta     = eta_ND;
			if ((phase_index == 0) && random_noise) {
				gausspoints[p].pls = 0.5 * rand() / (RAND_MAX + 1.0);
			}
			
		}
	}
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelOutput_NotchGP"
PetscErrorCode pTatin2d_ModelOutput_NotchGP(pTatinCtx ctx,Vec X,const char prefix[])
{
	static int     beenhere = 0;
	PetscBool      use_gausspoint,found;
	char           *vtkfilename,*pvdfilename,*name;
	PetscErrorCode ierr;
	
	use_gausspoint = PETSC_FALSE;	
	ierr = PetscOptionsGetBool(PETSC_NULL,"-update_nonlinearities_qp",&use_gausspoint,&found);CHKERRQ(ierr);
	

	if (use_gausspoint) {
        asprintf(&name,"qpoints_%1.5d",ctx->step);
        asprintf(&vtkfilename,"qpoints_%1.5d.pvtu",ctx->step);
		asprintf(&pvdfilename,"%s/timeseries_qpoints.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
        }			
        ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
        ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
        free(vtkfilename);
		free(pvdfilename);
        
		asprintf(&name,"vp_%1.5d",ctx->step);
        asprintf(&vtkfilename,"vp_%1.5d.pvts",ctx->step);
		asprintf(&pvdfilename,"%s/timeseries_vp.pvd",ctx->outputpath);        
        if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
        }			
        ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
        ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
        free(vtkfilename);
		free(pvdfilename); 

	} else {

        asprintf(&name,"qpoints_%1.5d",ctx->step);
        asprintf(&vtkfilename,"qpoints_%1.5d.pvtu",ctx->step);
		asprintf(&pvdfilename,"%s/timeseries_qpoints.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
        }			
        ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
        ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
        free(vtkfilename);
		free(pvdfilename);
        
		asprintf(&name,"vp_%1.5d",ctx->step);
        asprintf(&vtkfilename,"vp_%1.5d.pvts",ctx->step);
		asprintf(&pvdfilename,"%s/timeseries_vp.pvd",ctx->outputpath);        
        if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
        }			
        ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
        ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
        free(vtkfilename);
		free(pvdfilename); 
        
        asprintf(&name,"mpoints_stk_%1.5d",ctx->step);
        asprintf(&vtkfilename,"mpoints_stk_%1.5d.pvts",ctx->step);
		asprintf(&pvdfilename,"%s/timeseries_mpoints_stk.pvd",ctx->outputpath);        
        if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
        }			
        ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
        ierr = SwarmOutputParaView_MPntPStokes(ctx->db,ctx->outputpath,"mpoints_stk");CHKERRQ(ierr);
        free(vtkfilename);
		free(pvdfilename); 
        
  	}
	
	beenhere = 1;
	PetscFunctionReturn(0);
}





