
#ifndef __ptatin_DMDAE_h__
#define __ptatin_DMDAE_h__

#include "petsc.h"
#include "petscdm.h"

/* little data bucket for all things needed to define elements */
typedef struct _p_DMDAE *DMDAE;

struct _p_DMDAE {
	PetscInt ne,lne;
	PetscInt mx,my,mz;
	PetscInt lmx,lmy,lmz;
	PetscInt *lsip,*lsjp,*lskp;
	PetscInt *lmxp,*lmyp,*lmzp;
	PetscInt si,sj,sk;
	PetscInt npe; /* nodes per element */
	PetscInt nps; /* nodes per side */
	PetscInt overlap;
	PetscInt sgi,sgj,sgk;
};

PetscErrorCode DMDAECreate(DMDAE *dae);
PetscErrorCode DMDAEDestroy(DMDAE *dae);
PetscErrorCode DMAttachDMDAE(DM dm);
PetscErrorCode DMGetDMDAE(DM dm,DMDAE *dae);
PetscErrorCode DMDestroyDMDAE(DM dm);

#endif

