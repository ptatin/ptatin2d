/*
 
 Model Description:

 These functions define the setup for an isoviscous pure shear problem with free surface in a gravity field
 the solution for pressure is  P = rho g y - 2 mu exx
 Domain is [0,1,-1, 0] with free surface at the jmax, free slip at the jmin and imin face and dirichlet at imax
 
  Input / command line parameters:
 -pureShearFree_eta   : viscosity (default =1.0);
 -pureShearFree_srH   : strainrate (default =1.0); 
 -pureShearFree_body  : rho*g (default =-1.0); 
 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"

/* ================= PureShearFree ================= */

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelOutput_PureShearFree"
PetscErrorCode pTatin2d_ModelOutput_PureShearFree(pTatinCtx ctx,Vec X,const char prefix[])
{
	PetscErrorCode ierr;

	ierr = pTatin2d_ModelOutput_Default(ctx,X,prefix);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_PureShearFree"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_PureShearFree(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
	
	ierr = DMDASetUniformCoordinates(ctx->dav,0.0,1.0, -1.0 ,0.0, 0.0,0.0);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_PureShearFree"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_PureShearFree(pTatinCtx ctx)
{
	PetscInt e,ncells;
	PetscInt p,ngp;
	GaussPointCoefficientsStokes *gausspoints;
	PetscReal opts_eta,opts_body;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	opts_eta  = 1.0;
	opts_body = 1.0;

	
	ierr = PetscOptionsGetReal(PETSC_NULL,"-PureShearFree_eta",&opts_eta,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-PureShearFree_body",&opts_body,0);CHKERRQ(ierr);

	
	ncells = ctx->Q->ncells;
	ngp    = ctx->Q->ngp;
	ctx->rheology_constants.rheology_type = RHEOLOGY_VISCOUS;

	for (e=0;e<ncells;e++) {
		ierr = QuadratureStokesGetCell(ctx->Q,e,&gausspoints);CHKERRQ(ierr);
		
		for (p=0; p<ngp; p++) {
			/*viscosity*/
			gausspoints[p].eta     = opts_eta;
			gausspoints[p].eta_ref = opts_eta;
			/* rhs */
			gausspoints[p].Fu[0] = 0.0;
			gausspoints[p].Fu[1] = opts_body;
		}
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_PureShearFree"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_PureShearFree(pTatinCtx ctx)
{
	BCList         ubclist;
	PetscScalar    bcval;
	DM             dav;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	PetscScalar opts_srH;
	
	ubclist = ctx->u_bclist;
	dav     = ctx->dav;
/*  -pureShearFree_srH   : strainrate (default =1.0); */ 

 	ierr = PetscOptionsGetScalar(PETSC_NULL,"-PureShearFree_srH",&opts_srH,0);CHKERRQ(ierr);

	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
	bcval = opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

	PetscFunctionReturn(0);
}


