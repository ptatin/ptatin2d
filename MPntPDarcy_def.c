
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "MPntPDarcy_def.h"


const char MPntPDarcy_classname[] = "MPntPDarcy";

const int MPntPDarcy_nmembers = 4;

const size_t MPntPDarcy_member_sizes[] = {
  1 * sizeof(double),
  1 * sizeof(double),
  1 * sizeof(double),
  1 * sizeof(double)
} ;

const char *MPntPDarcy_member_names[] = {
  "coefficient_diffusion",
  "porosity",
  "rock_fluid",
  "source"
} ;


/* ===================================== */
/* Getters for MPntPDarcy */
/* ===================================== */
void MPntPDarcyGetField_coefficient_diffusion(MPntPDarcy *point,double *data) 
{
  *data = point->diff;
}

void MPntPDarcyGetField_porosity(MPntPDarcy *point,double *data) 
{
  *data = point->pore;
}

void MPntPDarcyGetField_rock_fluid(MPntPDarcy *point,double *data) 
{
  *data = point->Xrock;
}

void MPntPDarcyGetField_source(MPntPDarcy *point,double *data) 
{
  *data = point->source;
}


/* ===================================== */
/* Setters for MPntPDarcy */
/* ===================================== */
void MPntPDarcySetField_coefficient_diffusion(MPntPDarcy *point,double data) 
{
  point->diff = data;
}

void MPntPDarcySetField_porosity(MPntPDarcy *point,double data) 
{
  point->pore = data;
}

void MPntPDarcySetField_rock_fluid(MPntPDarcy *point,double data) 
{
  point->Xrock = data;
}

void MPntPDarcySetField_source(MPntPDarcy *point,double data) 
{
  point->source = data;
}


/* ===================================== */
/* C-viewer for MPntPDarcy */
/* ===================================== */
void MPntPDarcyView(MPntPDarcy *point)
{
  {
    double data;
    MPntPDarcyGetField_coefficient_diffusion(point,&data);
    printf("field: coefficient_diffusion = %1.6e; [size %zu; type double; variable_name diff]\n",data, MPntPDarcy_member_sizes[0] );
  }
  {
    double data;
    MPntPDarcyGetField_porosity(point,&data);
    printf("field: porosity = %1.6e; [size %zu; type double; variable_name pore]\n",data, MPntPDarcy_member_sizes[1] );
  }
  {
    double data;
    MPntPDarcyGetField_rock_fluid(point,&data);
    printf("field: rock_fluid = %1.6e; [size %zu; type double; variable_name Xrock]\n",data, MPntPDarcy_member_sizes[2] );
  }
  {
    double data;
    MPntPDarcyGetField_source(point,&data);
    printf("field: source = %1.6e; [size %zu; type double; variable_name source]\n",data, MPntPDarcy_member_sizes[3] );
  }
}


/* ===================================== */
/* VTK viewer for MPntPDarcy */
/* ===================================== */
void MPntPDarcyVTKWriteAsciiAllFields(FILE *vtk_fp,const int N,const MPntPDarcy points[]) 
{
  int p;
  fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Float64\" Name=\"diff\" format=\"ascii\">\n");
  for(p=0;p<N;p++) {
    fprintf( vtk_fp,"\t\t\t\t\t%lf\n",(double)points[p].diff);
  }
  fprintf( vtk_fp, "\t\t\t\t</DataArray>\n");
  fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Float64\" Name=\"pore\" format=\"ascii\">\n");
  for(p=0;p<N;p++) {
    fprintf( vtk_fp,"\t\t\t\t\t%lf\n",(double)points[p].pore);
  }
  fprintf( vtk_fp, "\t\t\t\t</DataArray>\n");
  fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Float64\" Name=\"Xrock\" format=\"ascii\">\n");
  for(p=0;p<N;p++) {
    fprintf( vtk_fp,"\t\t\t\t\t%lf\n",(double)points[p].Xrock);
  }
  fprintf( vtk_fp, "\t\t\t\t</DataArray>\n");
  fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Float64\" Name=\"source\" format=\"ascii\">\n");
  for(p=0;p<N;p++) {
    fprintf( vtk_fp,"\t\t\t\t\t%lf\n",(double)points[p].source);
  }
  fprintf( vtk_fp, "\t\t\t\t</DataArray>\n");
}


/* ===================================== */
/* PVTU viewer for MPntPDarcy */
/* ===================================== */
void MPntPDarcyPVTUWriteAllPPointDataFields(FILE *vtk_fp) 
{
  fprintf(vtk_fp, "\t\t\t<PDataArray type=\"Float64\" Name=\"diff\" NumberOfComponents=\"1\"/>\n");
  fprintf(vtk_fp, "\t\t\t<PDataArray type=\"Float64\" Name=\"pore\" NumberOfComponents=\"1\"/>\n");
  fprintf(vtk_fp, "\t\t\t<PDataArray type=\"Float64\" Name=\"Xrock\" NumberOfComponents=\"1\"/>\n");
  fprintf(vtk_fp, "\t\t\t<PDataArray type=\"Float64\" Name=\"source\" NumberOfComponents=\"1\"/>\n");
}


/* ===================================== */
/* VTK binary (appended header) viewer for MPntPDarcy */
/* ===================================== */
void MPntPDarcyVTKWriteBinaryAppendedHeaderAllFields(FILE *vtk_fp,int *offset,const int N,const MPntPDarcy points[]) 
{
  fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Float64\" Name=\"diff\" format=\"appended\"  offset=\"%d\" />\n",*offset);
  *offset = *offset + sizeof(int) + N * sizeof(double);

  fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Float64\" Name=\"pore\" format=\"appended\"  offset=\"%d\" />\n",*offset);
  *offset = *offset + sizeof(int) + N * sizeof(double);

  fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Float64\" Name=\"Xrock\" format=\"appended\"  offset=\"%d\" />\n",*offset);
  *offset = *offset + sizeof(int) + N * sizeof(double);

  fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Float64\" Name=\"source\" format=\"appended\"  offset=\"%d\" />\n",*offset);
  *offset = *offset + sizeof(int) + N * sizeof(double);

}


/* ================================================== */
/* VTK binary (appended data) viewer for MPntPDarcy */
/* ==================================================== */
void MPntPDarcyVTKWriteBinaryAppendedDataAllFields(FILE *vtk_fp,const int N,const MPntPDarcy points[]) 
{
  int p,length;
  size_t atomic_size;

  atomic_size = sizeof(double);
  length = (int)( atomic_size * ((size_t)N) );
  fwrite( &length,sizeof(int),1,vtk_fp);
  for(p=0;p<N;p++) {
    fwrite( &points[p].diff,atomic_size,1,vtk_fp);
  }

  atomic_size = sizeof(double);
  length = (int)( atomic_size * ((size_t)N) );
  fwrite( &length,sizeof(int),1,vtk_fp);
  for(p=0;p<N;p++) {
    fwrite( &points[p].pore,atomic_size,1,vtk_fp);
  }

  atomic_size = sizeof(double);
  length = (int)( atomic_size * ((size_t)N) );
  fwrite( &length,sizeof(int),1,vtk_fp);
  for(p=0;p<N;p++) {
    fwrite( &points[p].Xrock,atomic_size,1,vtk_fp);
  }

  atomic_size = sizeof(double);
  length = (int)( atomic_size * ((size_t)N) );
  fwrite( &length,sizeof(int),1,vtk_fp);
  for(p=0;p<N;p++) {
    fwrite( &points[p].source,atomic_size,1,vtk_fp);
  }

}

