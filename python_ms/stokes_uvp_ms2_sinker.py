## ==============================================
#
#    Manufactured solution generator for 2d
#    isotropic, variable viscosity Stokes flow.
#
## ==============================================


from sympy import *
from sympy import var, Plot
#from sympy.numerics import *


MS_name = 'stokes_sinker_1a'

fp = open(MS_name+'.gen.c','w')


# define variables we will differentiate with respect to
x = Symbol('x')
y = Symbol('y')

eta_min = Symbol('eta_min')
eta_max = Symbol('eta_max')

# controls the sharpness of the viscous block, vel field and pressure, defaults 160,30,3000
f_ETA_m = Symbol('f_ETA_m') 
f_VEL_m = Symbol('f_VEL_m') 
f_P_m = Symbol('f_P_m') 


#//////////////////////////////////////////////////////////////////////////
# define coefficients
# << viscosity >> #
#f_ETA_m = 160.0
sinker_filter_ETA_Lx =  0.5*atan( f_ETA_m*(x+0.5))/atan(f_ETA_m*(1.0+0.5))+0.5
sinker_filter_ETA_Rx = -0.5*atan( f_ETA_m*(x-0.5))/atan(f_ETA_m*(1.0+0.5))+0.5
sinker_filter_ETA_Ly =  0.5*atan( f_ETA_m*(y+0.5))/atan(f_ETA_m*(1.0+0.5))+0.5
sinker_filter_ETA_Ry = -0.5*atan( f_ETA_m*(y-0.5))/atan(f_ETA_m*(1.0+0.5))+0.5
sinker_filter_ETA    = sinker_filter_ETA_Lx * sinker_filter_ETA_Rx * sinker_filter_ETA_Ly * sinker_filter_ETA_Ry

eta = 1.0*eta_min + sinker_filter_ETA * eta_max

# define solutions
# << velocity >> #
# u component #
u_iso = 0.013*sin(pi*x)*-cos(0.5*pi*y-0.5*pi)

#f_VEL_m = 30.0
sinker_filter_VEL_Lx =  0.5*atan( f_VEL_m*(x+0.5))/atan(f_VEL_m*(1.0+0.5))+0.5
sinker_filter_VEL_Rx = -0.5*atan( f_VEL_m*(x-0.5))/atan(f_VEL_m*(1.0+0.5))+0.5
sinker_filter_VEL_Ly =  0.5*atan( f_VEL_m*(y+0.5))/atan(f_VEL_m*(1.0+0.5))+0.5
sinker_filter_VEL_Ry = -0.5*atan( f_VEL_m*(y-0.5))/atan(f_VEL_m*(1.0+0.5))+0.5
sinker_filter_VEL    = sinker_filter_VEL_Lx * sinker_filter_VEL_Rx * sinker_filter_VEL_Ly * sinker_filter_VEL_Ry

u = u_iso * (1.0 - sinker_filter_VEL)


# v component #
v_iso  = 0.02*sin(1.0*pi*x-0.5*pi)*sin(0.5*pi*(y+1))
v      = v_iso * (1.0 - sinker_filter_VEL) - 0.015 * sinker_filter_VEL


# << pressure >> #
#f_P_m = 3000.0
sinker_filter_P_Lx =  0.5*atan( f_P_m*(x+0.5))/atan(f_P_m*(1.0+0.5))+0.5
sinker_filter_P_Rx = -0.5*atan( f_P_m*(x-0.5))/atan(f_P_m*(1.0+0.5))+0.5
sinker_filter_P_Ly =  0.5*atan( f_P_m*(y+0.5))/atan(f_P_m*(1.0+0.5))+0.5
sinker_filter_P_Ry = -0.5*atan( f_P_m*(y-0.5))/atan(f_P_m*(1.0+0.5))+0.5
sinker_filter_P    = sinker_filter_P_Lx * sinker_filter_P_Rx * sinker_filter_P_Ly * sinker_filter_P_Ry


p_iso = -0.3*( sin(0.5*pi*y) + 0.4*sin(0.5*pi*y)*cos(pi*x) )
p     = p_iso * (1.0 - sinker_filter_P)
#//////////////////////////////////////////////////////////////////////////

# look what the constant is for the pressure #
#intp_x = integrate(p,(x,-1.0,1.0))
#intp = integrate(intp_x,(y,-1.0,1.0))


# These do not need to be edited
Lxx = diff(u,x)
Lxy = diff(u,y)
Lyx = diff(v,x)
Lyy = diff(v,y)

exx = diff(u,x)
exy = 0.5 * ( diff(u,y) + diff(v,x) )
eyx = exy
eyy = diff(v,y)

txx = 2.0 * eta * exx
txy = 2.0 * eta * exy
tyx = txy
tyy = 2.0 * eta * eyy

fx = diff(txx,x) + diff(txy,y) - diff(p,x)
fy = diff(tyx,x) + diff(tyy,y) - diff(p,y)
divV = diff(u,x) + diff(v,y)

fp.write(  "/* =========================================================== */\n" )
fp.write(  "/*   Manufactured solution */\n" )
fp.write(  "/*     PDE: Stokes, 2d variable viscosity */\n" )
fp.write(  "/*     Name: " + MS_name + " */\n" )
fp.write(  "/*     Coefficients and solution (u,v,p) */\n" )
fp.write(  "/* =========================================================== */\n" )

fp.write( "*eta = " )
fp.write( ccode(eta) + ";    /* viscosity - [" + MS_name +"] */\n" )
fp.write( "\n" )

fp.write( "X[0] = " )
fp.write( ccode(u) + ";    /* velocity_x - [" + MS_name +"] */\n" )

fp.write( "X[1] = " )
fp.write( ccode(v) + ";    /* velocity_y - [" + MS_name +"] */\n" )

fp.write( "X[2] = " )
fp.write( ccode(p) + ";    /* pressure - [" + MS_name +"] */\n" )
fp.write( "\n" )


#fp.write( "*intP = " )
#fp.write( ccode(intp) + ";    /* \int P dV - [" + MS_name +"] */\n" )
#fp.write( "\n" )

fp.write( "L[0] = " )
fp.write( ccode(Lxx) + ";    /* L_xx = du/dx - [" + MS_name +"] */\n" )

fp.write( "L[1] = " )
fp.write( ccode(Lxy) + ";    /* L_xy = du/dy - [" + MS_name +"] */\n" )

fp.write( "L[2] = " )
fp.write( ccode(Lyx) + ";    /* L_yx = dv/dx - [" + MS_name +"] */\n" )

fp.write( "L[3] = " )
fp.write( ccode(Lyy) + ";    /* L_yy = dv/dy - [" + MS_name +"] */\n" )

fp.write( "L[4] = " )
fp.write( ccode(diff(p,x)) + ";    /* dp/dx - [" + MS_name +"] */\n" )

fp.write( "L[5] = " )
fp.write( ccode(diff(p,y)) + ";    /* p/dy - [" + MS_name +"] */\n" )
fp.write( "\n" )


fp.write(  "/* =========================================================== */\n" )
fp.write(  "/*   Manufactured solution */\n" )
fp.write(  "/*     PDE: Stokes, 2d variable viscosity */\n" )
fp.write(  "/*     Name: " + MS_name + " */\n" )
fp.write(  "/*     Residuals for (u,v) momentum and continuity */\n" )
fp.write(  "/* =========================================================== */\n" )
fp.write( "Ruvp[0] = " )
fp.write( ccode(fx) + ";    /* Ru_x  - [" + MS_name +"] */\n" )

fp.write( "Ruvp[1] = " )
fp.write( ccode(fy) + ";    /* Ru_p  - [" + MS_name +"] */\n" )

fp.write( "Ruvp[2] = " )
fp.write( ccode(divV) + ";    /* Rp  - [" + MS_name +"] */\n" )
fp.write( "\n" )


print "*** gnuplot print-able ***"

print "eta ="
print eta,"\n"

print "u ="
print u,"\n"

print "v ="
print v,"\n"

print "p ="
print p,"\n"

print "rx ="
print fx,"\n"

print "ry ="
print fy,"\n"

print "div(V) ="
print divV,"\n"


fp.close()

