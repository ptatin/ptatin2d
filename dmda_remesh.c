
#include <petsc.h>
#include <petscvec.h>
#include <petscdm.h>

#include "dmda_compare.h"
#include "dmda_update_coords.h"
#include "dmda_redundant.h"
#include "dmda_remesh.h"

#undef __FUNCT__
#define __FUNCT__ "DMDAGetCornerCoordinatesInPlane_IJ"
PetscErrorCode DMDAGetCornerCoordinatesInPlane_IJ(DM da,PetscInt K,DMDACoor3d coords[])
{
	PetscInt ii,jj,si,sj,sk;
	PetscInt MX,MY,MZ,nx,ny,nz;
	PetscReal corners[4][3];
	Vec coordinates;
	MPI_Comm comm;
	DM cda;
	DMDACoor3d ***LA_coords;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	ierr = DMDAGetInfo(da,0,&MX,&MY,&MZ, 0,0,0, 0,0,0,0,0,0);CHKERRQ(ierr);
	ierr = DMDAGetCorners(da, &si,&sj,&sk, &nx,&ny,&nz);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(da,&coordinates);CHKERRQ(ierr);
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,coordinates,&LA_coords);CHKERRQ(ierr);
	
	comm = ((PetscObject)da)->comm;
	if ( (K>=sk) && (K<sk+nz) ) {
		if ( (si==0) && (sj==0) ) {
			ii = 0;
			jj = 0;
			corners[0][0] = LA_coords[K][jj][ii].x;
			corners[0][1] = LA_coords[K][jj][ii].y;
			corners[0][2] = LA_coords[K][jj][ii].z;
			ierr = MPI_Bcast(corners[0],3,MPIU_REAL,0,comm);CHKERRQ(ierr);
		}}
	
	if ( (K>=sk) && (K<sk+nz) ) {
		if ( (si==0) && (sj==MY-1) ) {
			ii = 0;
			jj = MY-1;
			corners[1][0] = LA_coords[K][jj][ii].x;
			corners[1][1] = LA_coords[K][jj][ii].y;
			corners[1][2] = LA_coords[K][jj][ii].z;
			ierr = MPI_Bcast(corners[1],3,MPIU_REAL,0,comm);CHKERRQ(ierr);
		}}
	
	if ( (K>=sk) && (K<sk+nz) ) {
		if ( (si==MX-1) && (sj==MY-1) ) {
			ii = MX-1;
			jj = MY-1;
			corners[2][0] = LA_coords[K][jj][ii].x;
			corners[2][1] = LA_coords[K][jj][ii].y;
			corners[2][2] = LA_coords[K][jj][ii].z;
			ierr = MPI_Bcast(corners[2],3,MPIU_REAL,0,comm);CHKERRQ(ierr);
		}}
	
	if ( (K>=sk) && (K<sk+nz) ) {
		if ( (si==MX-1) && (sj==0) ) {
			ii = MX-1;
			jj = 0;
			corners[3][0] = LA_coords[K][jj][ii].x;
			corners[3][1] = LA_coords[K][jj][ii].y;
			corners[3][2] = LA_coords[K][jj][ii].z;
			ierr = MPI_Bcast(corners[3],3,MPIU_REAL,0,comm);CHKERRQ(ierr);
		}}
	
	ierr = DMDAVecRestoreArray(cda,coordinates,&LA_coords);CHKERRQ(ierr);
	
	coords[0].x = corners[0][0];    coords[0].y = corners[0][1];    	coords[0].z = corners[0][2];
	coords[1].x = corners[1][0];    coords[1].y = corners[1][1];    	coords[1].z = corners[1][2];
	coords[2].x = corners[2][0];    coords[2].y = corners[2][1];    	coords[2].z = corners[2][2];
	coords[3].x = corners[3][0];    coords[3].y = corners[3][1];    	coords[3].z = corners[3][2];
	
	PetscFunctionReturn(0);
}

/*
 Remesh all nodes in plane K, in a regular fashion.
 coords[] defines the vertices of the bilinear plane which defines the surface we remesh over.
 The order should be
 (i,j+1)[1]  (i+1,j+1)[2]
 (i,j)  [0]  (i+1,j)  [3]
*/
#undef __FUNCT__
#define __FUNCT__ "DMDARemeshSetUniformCoordinatesInPlane_IJ"
PetscErrorCode DMDARemeshSetUniformCoordinatesInPlane_IJ(DM da,PetscInt K,DMDACoor3d coords[])
{
	PetscErrorCode ierr;
	PetscInt si,sj,sk,nx,ny,nz,i,j;
	PetscInt MX,MY;
	PetscInt n;
	PetscReal dxi,deta,Ni[4];
	DM cda;
	Vec coord;
	DMDACoor3d ***_coord;

	PetscFunctionBegin;
	ierr = DMDAGetInfo(da,0,&MX,&MY,0, 0,0,0, 0,0,0,0,0,0);CHKERRQ(ierr);
	ierr = DMDAGetCorners( da, &si,&sj,&sk, &nx,&ny,&nz );CHKERRQ(ierr);
	
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(da,&coord);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,coord,&_coord);CHKERRQ(ierr);
	
	if ( (K>=sk) && (K<sk+nz) ) {
		dxi = 2.0/(PetscReal)(MX-1);
		deta = 2.0/(PetscReal)(MY-1);
		for( j=sj; j<sj+ny; j++ ) {
			for( i=si; i<si+nx; i++ ) {
				PetscReal xi,eta, xn,yn,zn;
				
				xi  = -1.0 + i*dxi;
				eta = -1.0 + j*deta;
				
				Ni[0] = 0.25 * (1.0-xi) * (1.0-eta);
				Ni[1] = 0.25 * (1.0-xi) * (1.0+eta);
				Ni[2] = 0.25 * (1.0+xi) * (1.0+eta);
				Ni[3] = 0.25 * (1.0+xi) * (1.0-eta);
				
				xn = yn = zn = 0.0;
				for (n=0; n<4; n++) {
					xn = xn + Ni[n] * coords[n].x;
					yn = yn + Ni[n] * coords[n].y;
					zn = zn + Ni[n] * coords[n].z;
				}
				_coord[K][j][i].x = xn;
				_coord[K][j][i].y = yn;
				_coord[K][j][i].z = zn;
			}
		}
	}
	/* tidy up */
	ierr = DMDAVecRestoreArray(cda,coord,&_coord);CHKERRQ(ierr);

	ierr = DMDAUpdateGhostedCoordinates(da);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "DMDARemeshSetUniformCoordinatesBetweenKLayers3d_MPI"
PetscErrorCode DMDARemeshSetUniformCoordinatesBetweenKLayers3d_MPI( DM da, PetscInt startK, PetscInt endK )
{
	PetscInt si,sj,sk,nx,ny,nz,i,j,k;
	PetscInt s_si,s_sj,s_sk,s_nx,s_ny,s_nz;
	DM surface1_da,surface2_da;
	DM surface1_cda,surface2_cda;
	Vec surface1_coords,surface2_coords;
	PetscScalar *surface1_nodes,*surface2_nodes;
	DM cda;
	Vec coords;
	DMDACoor3d ***nodes;
	DMDACoor3d DX;
	PetscInt start,end, DL, RANGE;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	/* build top and bottom surface da on my processor */
	ierr = DMDAGetCorners( da, &si,&sj,&sk, &nx,&ny,&nz );CHKERRQ(ierr);
	ierr = DMDACreate3dRedundant( da, si,si+nx, sj,sj+ny, startK,  startK+1, 1, &surface1_da );CHKERRQ(ierr);
	ierr = DMDACreate3dRedundant( da, si,si+nx, sj,sj+ny, endK-1,  endK,     1, &surface2_da );CHKERRQ(ierr);
	
	/*
	{
		PetscViewer vv;
		Vec x;
		ierr = PetscViewerASCIIOpen(((PetscObject)(surface1_da))->comm, "test_dmda_remesh_s1.vtk", &vv);CHKERRQ(ierr);
		ierr = PetscViewerSetFormat(vv, PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
		ierr = DMCreateGlobalVector(surface1_da,&x);CHKERRQ(ierr);
		ierr = PetscObjectSetName( (PetscObject)x, "phi" );CHKERRQ(ierr);
		ierr = DMView(surface1_da, vv);CHKERRQ(ierr);
		ierr = VecView(x, vv);CHKERRQ(ierr);
		ierr = PetscViewerDestroy(vv);CHKERRQ(ierr);
		ierr  = VecDestroy(x);CHKERRQ(ierr);
	}
	{
		PetscViewer vv;
		Vec x;
		ierr = PetscViewerASCIIOpen(((PetscObject)(surface2_da))->comm, "test_dmda_remesh_s2.vtk", &vv);CHKERRQ(ierr);
		ierr = PetscViewerSetFormat(vv, PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
		ierr = DMCreateGlobalVector(surface2_da,&x);CHKERRQ(ierr);
		ierr = PetscObjectSetName( (PetscObject)x, "phi" );CHKERRQ(ierr);
		ierr = DMView(surface2_da, vv);CHKERRQ(ierr);
		ierr = VecView(x, vv);CHKERRQ(ierr);
		ierr = PetscViewerDestroy(vv);CHKERRQ(ierr);
		ierr  = VecDestroy(x);CHKERRQ(ierr);
	}
	*/
	

//	ierr = DMDAGetCoordinateDA( surface1_da, &surface1_cda);CHKERRQ(ierr); /* don't access coordinate da's on these guys */
	ierr = DMDAGetCoordinates( surface1_da,&surface1_coords );CHKERRQ(ierr);
//	ierr = DMDAVecGetArray(surface1_cda,surface1_coords,&surface1_nodes);CHKERRQ(ierr);
	ierr = VecGetArray(surface1_coords,&surface1_nodes);CHKERRQ(ierr);
	
//	ierr = DMDAGetCoordinateDA( surface2_da, &surface2_cda);CHKERRQ(ierr); /* don't access coordinate da's on these guys! */
	ierr = DMDAGetCoordinates( surface2_da,&surface2_coords );CHKERRQ(ierr);
//	ierr = DMDAVecGetArray(surface2_cda,surface2_coords,&surface2_nodes);CHKERRQ(ierr);
	ierr = VecGetArray(surface2_coords,&surface2_nodes);CHKERRQ(ierr);

	
	ierr = DMDAGetCoordinateDA( da, &cda);CHKERRQ(ierr);

	ierr = DMDAGetCoordinates( da,&coords );CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,coords,&nodes);CHKERRQ(ierr);

	ierr = DMDAGetCorners(da,&si,&sj,&sk,&nx,&ny,&nz);CHKERRQ(ierr);
//	ierr = DMDAGetCorners(surface2_cda,&s_si,&s_sj,&s_sk,&s_nx,&s_ny,&s_nz);CHKERRQ(ierr);
	ierr = DMDAGetCorners(surface2_da,&s_si,&s_sj,&s_sk,&s_nx,&s_ny,&s_nz);CHKERRQ(ierr);

	/* starts won't match as the the surface meshes are sequential */
/*
	if( si != s_si ) {  SETERRQ( PETSC_COMM_SELF, PETSC_ERR_USER,"si on da must match surface da (s1)" );  }
        if( sj != s_sj ) {  SETERRQ( PETSC_COMM_SELF, PETSC_ERR_USER,"sj on da must match surface da (s1)" );  }
*/
        if( nx != s_nx ) {  SETERRQ( PETSC_COMM_SELF, PETSC_ERR_USER,"nx on da must match surface da (s1)" );  }
        if( ny != s_ny ) {  SETERRQ( PETSC_COMM_SELF, PETSC_ERR_USER,"ny on da must match surface da (s1)" );  }

        if( s_sk != 0  ) {  SETERRQ( PETSC_COMM_SELF, PETSC_ERR_USER,"s_sk on surface da should be 0 (s1)" );  }
        if( s_nz != 1  ) {  SETERRQ(PETSC_COMM_SELF, PETSC_ERR_USER,"s_nz on surface da should be 1 (s1)" );  }



#if 0	
	/* Some portion of grid may overlap sub-domains */
	/* Figure out the range of k indices I need to traverse */
	start = 0;
	end   = 0;
	if( (endK+1) >= (sk+nz) ) {
		printf("sk=%d,nz=%d\n",sk,nz);
		end = PetscMin( sk+nz, endK+1 );
	}
	if( (startK) > (sk) ) {
		start = PetscMax( sk, startK );
	}
	DL = end - start;
	if( DL < 0 ) {
		SETERRQ( ((PetscObject)da)->comm, PETSC_ERR_USER, "DL cannot be negative" );
	}
	
	/* total range of k indices to span */
	RANGE = endK - startK;
	
	printf("DL = %d \n", DL );
	
	if( DL!=0 ) {
		
		for( j=sj; j<sj+ny; j++ ) {
			for( i=si; i<si+nx; i++ ) {
				
				DX.x = (  surface2_nodes[0][j][i].x  -  surface1_nodes[0][j][i].x  )/( (PetscScalar)(RANGE) );
				DX.y = (  surface2_nodes[0][j][i].y  -  surface1_nodes[0][j][i].y  )/( (PetscScalar)(RANGE) );
				DX.z = (  surface2_nodes[0][j][i].z  -  surface1_nodes[0][j][i].z  )/( (PetscScalar)(RANGE) );
				
				for( k=start; k<end; k++ ) {
					ierr = DMDA_CheckNodeIndex3d(da,PETSC_FALSE,i,j,k); CHKERRQ(ierr);
					
					nodes[k][j][i].x = surface1_nodes[0+nx*j+i].x + ((PetscScalar)k) * DX.x;
					nodes[k][j][i].y = surface1_nodes[0+nx*j+i].y + ((PetscScalar)k) * DX.y;
					nodes[k][j][i].z = surface1_nodes[0+nx*j+i].z + ((PetscScalar)k) * DX.z;
				}
			}
		}
		
	}
#endif

	RANGE = endK - startK;
	for( j=0; j<ny; j++ ) {
		for( i=0; i<nx; i++ ) {
			
			DX.x = (  surface2_nodes[3*(0+nx*j+i)  ]  -  surface1_nodes[3*(0+nx*j+i)  ]  )/( (PetscScalar)(RANGE-1) );
			DX.y = (  surface2_nodes[3*(0+nx*j+i)+1]  -  surface1_nodes[3*(0+nx*j+i)+1]  )/( (PetscScalar)(RANGE-1) );
			DX.z = (  surface2_nodes[3*(0+nx*j+i)+2]  -  surface1_nodes[3*(0+nx*j+i)+2]  )/( (PetscScalar)(RANGE-1) );

			for( k=sk; k<sk+nz; k++ ) {
				PetscInt ii = i + si;
				PetscInt jj = j + sj;
				ierr = DMDA_CheckNodeIndex3d(da,PETSC_FALSE,ii,jj,k); CHKERRQ(ierr);
				if ( (k>=startK) && (k<endK) ){
					nodes[k][jj][ii].x = surface1_nodes[3*(0+nx*j+i)  ] + ((PetscScalar)k) * DX.x;
					nodes[k][jj][ii].y = surface1_nodes[3*(0+nx*j+i)+1] + ((PetscScalar)k) * DX.y;
					nodes[k][jj][ii].z = surface1_nodes[3*(0+nx*j+i)+2] + ((PetscScalar)k) * DX.z;
				}
			}
		}
	}
	
	/* tidy up */
	ierr = DMDAVecRestoreArray(cda,coords,&nodes);CHKERRQ(ierr);

	ierr = VecRestoreArray(surface1_coords,&surface1_nodes);CHKERRQ(ierr);
	ierr = VecRestoreArray(surface2_coords,&surface2_nodes);CHKERRQ(ierr);

	ierr = DMDestroy(&surface1_da);CHKERRQ(ierr);
	ierr = DMDestroy(&surface2_da);CHKERRQ(ierr);
	
	/* update */
	ierr = DMDAUpdateGhostedCoordinates(da);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDARemeshSetUniformCoordinatesBetweenKLayers3d"
PetscErrorCode DMDARemeshSetUniformCoordinatesBetweenKLayers3d( DM da, PetscInt startK, PetscInt endK )
{
	MPI_Comm comm;
	PetscMPIInt size;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	PetscObjectGetComm( (PetscObject)da, &comm );
	MPI_Comm_size( comm, &size );
	if(size==1) {
		ierr = DMDARemeshSetUniformCoordinatesBetweenKLayers3d_MPI( da, startK, endK );CHKERRQ(ierr);
	}
	else {
		ierr = DMDARemeshSetUniformCoordinatesBetweenKLayers3d_MPI( da, startK, endK );CHKERRQ(ierr);
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDARemeshSetUniformCoordinatesBetweenJLayers2d_MPI"
PetscErrorCode DMDARemeshSetUniformCoordinatesBetweenJLayers2d_MPI( DM da, PetscInt startJ, PetscInt endJ )
{
	PetscInt si,sj,sk,nx,ny,nz,i,j,k;
	PetscInt s_si,s_sj,s_sk,s_nx,s_ny,s_nz;
	DM surface1_da,surface2_da;
	DM surface1_cda,surface2_cda;
	Vec surface1_coords,surface2_coords;
	PetscScalar *surface1_nodes,*surface2_nodes;
	DM cda;
	Vec coords;
	DMDACoor2d **nodes;
	DMDACoor2d DX;
	PetscInt start,end, DL, RANGE;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	/* build top and bottom surface da on my processor */
	ierr = DMDAGetCorners( da, &si,&sj,&sk, &nx,&ny,&nz );CHKERRQ(ierr);
	ierr = DMDACreate2dRedundant( da, si,si+nx, startJ,  startJ+1, 1, &surface1_da );CHKERRQ(ierr);
	ierr = DMDACreate2dRedundant( da, si,si+nx, endJ-1,  endJ,     1, &surface2_da );CHKERRQ(ierr);
	
	//	ierr = DMDAGetCoordinateDA( surface1_da, &surface1_cda);CHKERRQ(ierr); /* don't access coordinate da's on these guys */
	ierr = DMDAGetCoordinates( surface1_da,&surface1_coords );CHKERRQ(ierr);
	//	ierr = DMDAVecGetArray(surface1_cda,surface1_coords,&surface1_nodes);CHKERRQ(ierr);
	ierr = VecGetArray(surface1_coords,&surface1_nodes);CHKERRQ(ierr);
	
	//	ierr = DMDAGetCoordinateDA( surface2_da, &surface2_cda);CHKERRQ(ierr); /* don't access coordinate da's on these guys! */
	ierr = DMDAGetCoordinates( surface2_da,&surface2_coords );CHKERRQ(ierr);
	//	ierr = DMDAVecGetArray(surface2_cda,surface2_coords,&surface2_nodes);CHKERRQ(ierr);
	ierr = VecGetArray(surface2_coords,&surface2_nodes);CHKERRQ(ierr);
	
	
	ierr = DMDAGetCoordinateDA( da, &cda);CHKERRQ(ierr);
	
	ierr = DMDAGetCoordinates( da,&coords );CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,coords,&nodes);CHKERRQ(ierr);
	
	ierr = DMDAGetCorners(da,&si,&sj,&sk,&nx,&ny,&nz);CHKERRQ(ierr);
	//	ierr = DMDAGetCorners(surface2_cda,&s_si,&s_sj,&s_sk,&s_nx,&s_ny,&s_nz);CHKERRQ(ierr);
	ierr = DMDAGetCorners(surface2_da,&s_si,&s_sj,&s_sk,&s_nx,&s_ny,&s_nz);CHKERRQ(ierr);
	
	/* starts won't match as the the surface meshes are sequential */
	/*
	 if( si != s_si ) {  SETERRQ( PETSC_COMM_SELF, PETSC_ERR_USER,"si on da must match surface da (s1)" );  }
	 if( sj != s_sj ) {  SETERRQ( PETSC_COMM_SELF, PETSC_ERR_USER,"sj on da must match surface da (s1)" );  }
	 */
	if( nx != s_nx ) {  SETERRQ( PETSC_COMM_SELF, PETSC_ERR_USER,"nx on da must match surface da (s1)" );  }
	//if( ny != s_ny ) {  SETERRQ( PETSC_COMM_SELF, PETSC_ERR_USER,"ny on da must match surface da (s1)" );  }
	
	if( s_sj != 0  ) {  SETERRQ( PETSC_COMM_SELF, PETSC_ERR_USER,"s_sj on surface da should be 0 (s1)" );  }
	if( s_ny != 1  ) {  SETERRQ(PETSC_COMM_SELF, PETSC_ERR_USER,"s_ny on surface da should be 1 (s1)" );  }
	
	RANGE = endJ - startJ;
	for( i=0; i<nx; i++ ) {
		
		DX.x = (  surface2_nodes[2*(i)  ]  -  surface1_nodes[2*(i)  ]  )/( (PetscScalar)(RANGE-1) );
		DX.y = (  surface2_nodes[2*(i)+1]  -  surface1_nodes[2*(i)+1]  )/( (PetscScalar)(RANGE-1) );
		
		for( j=sj; j<sj+ny; j++ ) {
			PetscInt ii = i + si;
			ierr = DMDA_CheckNodeIndex2d(da,PETSC_FALSE,ii,j); CHKERRQ(ierr);
			if ( (j>=startJ) && (j<endJ) ){
				nodes[j][ii].x = surface1_nodes[2*(i)  ] + ((PetscScalar)(j-startJ)) * DX.x;
				nodes[j][ii].y = surface1_nodes[2*(i)+1] + ((PetscScalar)(j-startJ)) * DX.y;
			}
		}
	}
	
	/* tidy up */
	ierr = DMDAVecRestoreArray(cda,coords,&nodes);CHKERRQ(ierr);
	
	ierr = VecRestoreArray(surface1_coords,&surface1_nodes);CHKERRQ(ierr);
	ierr = VecRestoreArray(surface2_coords,&surface2_nodes);CHKERRQ(ierr);
	
	ierr = DMDestroy(&surface1_da);CHKERRQ(ierr);
	ierr = DMDestroy(&surface2_da);CHKERRQ(ierr);
	
	/* update */
	ierr = DMDAUpdateGhostedCoordinates(da);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDARemeshSetUniformCoordinatesBetweenJLayers2d"
PetscErrorCode DMDARemeshSetUniformCoordinatesBetweenJLayers2d( DM da, PetscInt startJ, PetscInt endJ )
{
	MPI_Comm comm;
	PetscMPIInt size;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	PetscObjectGetComm( (PetscObject)da, &comm );
	MPI_Comm_size( comm, &size );
	if(size==1) {
		ierr = DMDARemeshSetUniformCoordinatesBetweenJLayers2d_MPI( da, startJ, endJ );CHKERRQ(ierr);
	}
	else {
		ierr = DMDARemeshSetUniformCoordinatesBetweenJLayers2d_MPI( da, startJ, endJ );CHKERRQ(ierr);
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDARemeshJMAX_UpdateHeightsFromInterior"
PetscErrorCode DMDARemeshJMAX_UpdateHeightsFromInterior(DM da)
{
	PetscErrorCode ierr;
	PetscInt M,N,P,si,sj,sk,nx,ny,nz,i,j,k;
	DM cda;
	Vec coords,gcoords;
	DMDACoor2d **LA_coords,**LA_gcoords;
	
	PetscFunctionBegin;
	
	ierr = DMDAGetInfo(da,0,&M,&N,&P,0,0,0, 0,0,0,0,0,0);CHKERRQ(ierr);
	ierr = DMDAGetCorners(da,&si,&sj,&sk,&nx,&ny,&nz);CHKERRQ(ierr);
	
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(da,&coords);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,coords,&LA_coords);CHKERRQ(ierr);
	
	ierr = DMDAGetGhostedCoordinates(da,&gcoords);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	if (sj+ny == N) { /* we contain the surface mesh */
		
		/* check IMIN */
		if (si == 0) {
			i = 0;
			LA_coords[N-1][i].y = LA_gcoords[N-1][i+1].y;
		}
		
		/* check IMAX */
		if (si + nx == M) {
			i = M-1;
			LA_coords[N-1][i].y = LA_gcoords[N-1][i-1].y;
		}
		
	}
	
	ierr = DMDAVecRestoreArray(cda,gcoords,&LA_gcoords);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(cda,coords,&LA_coords);CHKERRQ(ierr);
	
	/* update */
	ierr = DMDAUpdateGhostedCoordinates(da);CHKERRQ(ierr);
		
	PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "DMDARemeshSetUniformCoordinatesIJ"
PetscErrorCode DMDARemeshSetUniformCoordinatesIJ(DM da,PetscInt dof_i,PetscInt index,PetscScalar value)
{
	PetscErrorCode ierr;
	PetscInt si,sj,nx,ny,i,j;
	PetscInt NX,NY;
	DM cda;
	Vec coord;
	DMDACoor2d **_coord;
	
	PetscFunctionBegin;
	ierr = DMDAGetInfo(da,0,&NX,&NY,0, 0,0,0, 0,0,0,0,0,0);CHKERRQ(ierr);
	ierr = DMDAGetCorners( da, &si,&sj,0, &nx,&ny,0 );CHKERRQ(ierr);
	
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(da,&coord);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,coord,&_coord);CHKERRQ(ierr);
	
	switch (dof_i) {
		case 0:
			i = index;
			
			if (index < 0) {
				SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"i < 0");
			}
			if (index >= NX) {
				SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"i >= NX");
			}
			
			if ( (index >= si) && (index < si+nx) ) {
				for(j=sj; j<sj+ny; j++) {
					_coord[j][i].x = value;
				}
			}
			
			break;

		case 1:
			j = index;
			
			if (index < 0) {
				SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"i < 0");
			}
			if (index >= NY) {
				SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"i >= NY");
			}
			
			if ( (index >= sj) && (index < sj+ny) ) {
				for(i=si; i<si+nx; i++) {
					_coord[j][i].y = value;
				}
			}
			break;
	}
	
	/* tidy up */
	ierr = DMDAVecRestoreArray(cda,coord,&_coord);CHKERRQ(ierr);
	
	ierr = DMDAUpdateGhostedCoordinates(da);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

/* ========================================================================================== */
/*

 Alternative remeshing routines
 Simple pattern is followed.
 All functions take in a DM (coordinate da)
 and vectors contain one (or both) the coordinates to modify (global,local)
 and a flag to indicate whether ghost coords should be updated at the end
  
 */

#undef __FUNCT__
#define __FUNCT__ "RMTSetCoordinates"
PetscErrorCode RMTSetCoordinates(DM dm,Vec coords)
{
	PetscErrorCode ierr;
	Vec c;
	PetscFunctionBegin;
	ierr = DMDAGetCoordinates(dm,&c);CHKERRQ(ierr);
	ierr = VecCopy(coords,c);CHKERRQ(ierr);
	ierr = DMDAUpdateGhostedCoordinates(dm);CHKERRQ(ierr);	
	PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "RMTSetUniformCoordinatesRange_X_Bottom"
PetscErrorCode RMTSetUniformCoordinatesRange_X_Bottom(DM cdm,Vec coords)
{
	PetscErrorCode ierr;
	PetscInt nx_range,i,j,k,si,sj,nx,ny;
	PetscScalar dx,xstart,xend;
	DMDACoor2d	**LA_coords;
	
	PetscFunctionBegin;
	nx_range = 10;
	
	
	ierr = DMDAGetCorners(cdm,&si,&sj,0,&nx,&ny,0);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cdm,coords,&LA_coords);CHKERRQ(ierr);

     if (nx-nx_range >= si) { 
         for (j=0; j < 2; j++) {
         xstart = LA_coords[j][nx-nx_range].x;
         xend   = LA_coords[2][nx-1].x;
         dx = (xend - xstart) / (PetscScalar)(nx_range-1);
                k=0;
              for (i=nx-nx_range+1;i<nx; i++) { 
                k = k + 1;
         	  LA_coords[j][i].x = xstart + k * dx;
         	  }
         }
      }
	
	ierr = DMDAVecRestoreArray(cdm,coords,&LA_coords);CHKERRQ(ierr);
	
//	if (update_ghost_coords) {
//		ierr = DMDAUpdateGhostedCoordinates(da);CHKERRQ(ierr);
//	}
	PetscFunctionReturn(0);
}



#undef __FUNCT__
#define __FUNCT__ "RMTSetUniformCoordinatesRange_X"
PetscErrorCode RMTSetUniformCoordinatesRange_X(DM cdm,Vec coords,PetscInt istart,PetscScalar xstart,PetscInt iend,PetscScalar xend)
{
	PetscErrorCode ierr;
	PetscInt nx_range,i,j,si,sj,nx,ny;
	PetscScalar dx;
	DMDACoor2d	**LA_coords;
	
	PetscFunctionBegin;
	nx_range = iend-istart;
	dx = (xend - xstart) / ( (PetscScalar)(nx_range-1) );
	
	ierr = DMDAGetCorners(cdm,&si,&sj,0,&nx,&ny,0);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cdm,coords,&LA_coords);CHKERRQ(ierr);
    
     
    
	
		for (i=0; i<nx_range; i++) {
			
			if (i+istart < si) { continue; }
			if (i+istart >= si+nx) { continue; }
           
    
            for (j=sj; j<sj+ny; j++) {
			
			LA_coords[j][i+istart].x = xstart + i * dx;
		}
	}
	
	ierr = DMDAVecRestoreArray(cdm,coords,&LA_coords);CHKERRQ(ierr);
	
//	if (update_ghost_coords) {
//		ierr = DMDAUpdateGhostedCoordinates(da);CHKERRQ(ierr);
//	}
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "RMTSetUniformCoordinatesRange_Y"
PetscErrorCode RMTSetUniformCoordinatesRange_Y(DM cdm,Vec coords,PetscInt jstart,PetscScalar ystart,PetscInt jend,PetscScalar yend)
{
	PetscErrorCode ierr;
	PetscInt ny_range,i,j,si,sj,nx,ny;
	PetscScalar dy;
	DMDACoor2d	**LA_coords;
	
	PetscFunctionBegin;
	ny_range = jend-jstart;
	dy = (yend - ystart) / ( (PetscScalar)(ny_range-1) );
	
	ierr = DMDAGetCorners(cdm,&si,&sj,0,&nx,&ny,0);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cdm,coords,&LA_coords);CHKERRQ(ierr);
	//printf("RMTSetUniformCoordinatesRange_Y \n");
	for (j=0; j<ny_range; j++) {
		//printf("ystart + j * dy = %1.4e [j=%d]\n",ystart + j * dy,j+jstart);

		if (j+jstart < sj) { continue; }
		if (j+jstart >= sj+ny) { continue; }
		
		for (i=si; i<si+nx; i++) {
			LA_coords[j+jstart][i].y = ystart + j * dy;
		}
	}
	
	ierr = DMDAVecRestoreArray(cdm,coords,&LA_coords);CHKERRQ(ierr);
	
	//	if (update_ghost_coords) {
	//		ierr = DMDAUpdateGhostedCoordinates(dm);CHKERRQ(ierr);
	//	}
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "RMTSetUniformCoordinatesRange"
PetscErrorCode RMTSetUniformCoordinatesRange(
																						 DM dm,
																						 PetscInt istart,PetscScalar xstart,PetscInt iend,PetscScalar xend,
																						 PetscInt jstart,PetscScalar ystart,PetscInt jend,PetscScalar yend,PetscBool update_ghost_coords)
{
	PetscErrorCode ierr;
	Vec coordinates;
	DM cdm;
	
	PetscFunctionBegin;

	ierr = DMDAGetCoordinateDA(dm,&cdm);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(dm,&coordinates);CHKERRQ(ierr);
	
	if (istart != PETSC_IGNORE) {
		ierr = RMTSetUniformCoordinatesRange_X(cdm,coordinates,istart,xstart,iend,xend);CHKERRQ(ierr);
	}
	if (jstart != PETSC_IGNORE) {
	ierr = RMTSetUniformCoordinatesRange_Y(cdm,coordinates,jstart,ystart,jend,yend);CHKERRQ(ierr);
	}
	if (update_ghost_coords) {
		ierr = DMDAUpdateGhostedCoordinates(dm);CHKERRQ(ierr);
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "RMTSetUniformCoordinatesBetweenILayers2d"
PetscErrorCode RMTSetUniformCoordinatesBetweenILayers2d( DM da, PetscInt startI, PetscInt endI )
{
	PetscInt si,sj,sk,nx,ny,nz,i,j,k;
	PetscInt s_si,s_sj,s_sk,s_nx,s_ny,s_nz;
	DM surface1_da,surface2_da;
	DM surface1_cda,surface2_cda;
	Vec surface1_coords,surface2_coords;
	PetscScalar *surface1_nodes,*surface2_nodes;
	DM cda;
	Vec coords;
	DMDACoor2d **nodes;
	DMDACoor2d DX;
	PetscInt start,end, DL, RANGE;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	/* build top and bottom surface da on my processor */
	ierr = DMDAGetCorners( da, &si,&sj,&sk, &nx,&ny,&nz );CHKERRQ(ierr);
	ierr = DMDACreate2dRedundant( da, startI,startI+1,sj,sj+ny, 1, &surface1_da );CHKERRQ(ierr);
	ierr = DMDACreate2dRedundant( da, endI-1,endI,    sj,sj+ny, 1, &surface2_da );CHKERRQ(ierr);
	
	//	ierr = DMDAGetCoordinateDA( surface1_da, &surface1_cda);CHKERRQ(ierr); /* don't access coordinate da's on these guys */
	ierr = DMDAGetCoordinates( surface1_da,&surface1_coords );CHKERRQ(ierr);
	//	ierr = DMDAVecGetArray(surface1_cda,surface1_coords,&surface1_nodes);CHKERRQ(ierr);
	ierr = VecGetArray(surface1_coords,&surface1_nodes);CHKERRQ(ierr);
	
	//	ierr = DMDAGetCoordinateDA( surface2_da, &surface2_cda);CHKERRQ(ierr); /* don't access coordinate da's on these guys! */
	ierr = DMDAGetCoordinates( surface2_da,&surface2_coords );CHKERRQ(ierr);
	//	ierr = DMDAVecGetArray(surface2_cda,surface2_coords,&surface2_nodes);CHKERRQ(ierr);
	ierr = VecGetArray(surface2_coords,&surface2_nodes);CHKERRQ(ierr);
	
	
	ierr = DMDAGetCoordinateDA( da, &cda);CHKERRQ(ierr);
	
	ierr = DMDAGetCoordinates( da,&coords );CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,coords,&nodes);CHKERRQ(ierr);
	
	ierr = DMDAGetCorners(da,&si,&sj,&sk,&nx,&ny,&nz);CHKERRQ(ierr);
	//	ierr = DMDAGetCorners(surface2_cda,&s_si,&s_sj,&s_sk,&s_nx,&s_ny,&s_nz);CHKERRQ(ierr);
	ierr = DMDAGetCorners(surface2_da,&s_si,&s_sj,&s_sk,&s_nx,&s_ny,&s_nz);CHKERRQ(ierr);
	
	/* starts won't match as the the surface meshes are sequential */
	if( ny != s_ny ) {  SETERRQ( PETSC_COMM_SELF, PETSC_ERR_USER,"ny on da must match surface da (s1)" );  }

	if( s_si != 0  ) {  SETERRQ( PETSC_COMM_SELF, PETSC_ERR_USER,"s_si on surface da should be 0 (s1)" );  }
	if( s_nx != 1  ) {  SETERRQ(PETSC_COMM_SELF, PETSC_ERR_USER,"s_nx on surface da should be 1 (s1)" );  }
	
	RANGE = endI - startI;
	for( j=0; j<ny; j++ ) {
		
		DX.x = (  surface2_nodes[2*(j)  ]  -  surface1_nodes[2*(j)  ]  )/( (PetscScalar)(RANGE-1) );
		DX.y = (  surface2_nodes[2*(j)+1]  -  surface1_nodes[2*(j)+1]  )/( (PetscScalar)(RANGE-1) );
		
		for( i=si; i<si+nx; i++ ) {
			PetscInt jj = j + sj;
			ierr = DMDA_CheckNodeIndex2d(da,PETSC_FALSE,i,jj); CHKERRQ(ierr);
			if ( (i>=startI) && (i<endI) ){
				nodes[jj][i].x = surface1_nodes[2*(j)  ] + ((PetscScalar)(i-startI)) * DX.x;
				nodes[jj][i].y = surface1_nodes[2*(j)+1] + ((PetscScalar)(i-startI)) * DX.y;
			}
		}
	}
	
	/* tidy up */
	ierr = DMDAVecRestoreArray(cda,coords,&nodes);CHKERRQ(ierr);
	
	ierr = VecRestoreArray(surface1_coords,&surface1_nodes);CHKERRQ(ierr);
	ierr = VecRestoreArray(surface2_coords,&surface2_nodes);CHKERRQ(ierr);
	
	ierr = DMDestroy(&surface1_da);CHKERRQ(ierr);
	ierr = DMDestroy(&surface2_da);CHKERRQ(ierr);
	
	/* update */
	ierr = DMDAUpdateGhostedCoordinates(da);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDASetRefinedCoordinates"
PetscErrorCode DMDASetRefinedCoordinates(DM dav)
{
    PetscScalar Ox[21],Oy[21],jmin[2],jmax[2];
    PetscInt Nx[20],Ny[20],ndomainx,ndomainy,nlimx,nlimy,NX,NY;
    PetscInt idom,jdom,test,istart,jstart,iend,jend;
    Vec coordinates;
    DM cdm;
    PetscErrorCode ierr;
    PetscFunctionBegin;
    // default create a regular grid for exemple usage see model_paul.c///
    ndomainx = 1;
    ndomainy = 1;
    PetscOptionsGetInt(PETSC_NULL,"-refine_n_domain_x",&ndomainx,0);
    PetscOptionsGetInt(PETSC_NULL,"-refine_n_domain_y",&ndomainy,0);
    nlimx=ndomainx+1;
    nlimy=ndomainy+1;
    ierr = DMDAGetBoundingBox(dav,jmin,jmax);CHKERRQ(ierr);
    Ox[0]=jmin[0]; Ox[1]=jmax[0];
    Oy[0]=jmin[1]; Oy[1]=jmax[1];
    PetscOptionsGetRealArray(PETSC_NULL,"-refine_x_lim",Ox,&nlimx,PETSC_NULL);
    PetscOptionsGetRealArray(PETSC_NULL,"-refine_y_lim",Oy,&nlimy,PETSC_NULL);
    ierr = DMDAGetInfo(dav,0, &NX,&NY,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
    Nx[0] = NX;
    Ny[0] = NY;
    PetscOptionsGetIntArray(PETSC_NULL,"-refine_x_dom_el",Nx,&ndomainx,PETSC_NULL);
    PetscOptionsGetIntArray(PETSC_NULL,"-refine_y_dom_el",Ny,&ndomainy,PETSC_NULL);
    
    // two next lines enforce that the domain keeps its current length in x during remeshing
    Ox[0]=jmin[0];
    Ox[ndomainx]=jmax[0];
    
    
    ierr = DMDAGetCoordinateDA(dav,&cdm);CHKERRQ(ierr);
    ierr = DMDAGetCoordinates(dav,&coordinates);CHKERRQ(ierr);
    
    
    
    istart = 0;
    for (idom=0; idom<ndomainx; idom++) {
        iend=istart+Nx[idom]*2+1;
        ierr = RMTSetUniformCoordinatesRange_X(cdm,coordinates,istart,Ox[idom],iend,Ox[idom+1]);CHKERRQ(ierr);
        istart=iend-1;
    }
    
    jstart = 0;
    for (jdom=0; jdom<ndomainy; jdom++) {
        jend = jstart+Ny[jdom]*2+1;
        ierr = RMTSetUniformCoordinatesRange_Y(cdm,coordinates,jstart,Oy[jdom],jend,Oy[jdom+1]);CHKERRQ(ierr);
        jstart= jend-1;
    }
    
   
    
    ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
    
    
    PetscFunctionReturn(0);
}








