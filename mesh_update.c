#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <petsc.h>
#include <petscvec.h>
#include <petscdm.h>
#include <private/daimpl.h>


#include "dmda_compare.h"
#include "dmda_update_coords.h"
#include "dmda_redundant.h"
#include "dmda_remesh.h"

#include "pTatin2d.h"
#include "element_type_Q1.h"
#include "mesh_update.h"

//#define REMESH_DBG


#undef __FUNCT__  
#define __FUNCT__ "DMCoarsenHierarchy2_DA"
PetscErrorCode  DMCoarsenHierarchy2_DA(DM da,PetscInt nlevels,DM dac[])
{
  PetscErrorCode ierr;
  PetscInt       i,n,*refx,*refy,*refz;
	
  PetscFunctionBegin;
  PetscValidHeaderSpecific(da,DM_CLASSID,1);
  if (nlevels < 0) SETERRQ(((PetscObject)da)->comm,PETSC_ERR_ARG_OUTOFRANGE,"nlevels cannot be negative");
  if (nlevels == 0) PetscFunctionReturn(0);
  PetscValidPointer(dac,3);
	
  /* Get refinement factors, defaults taken from the coarse DMDA */
  ierr = PetscMalloc3(nlevels,PetscInt,&refx,nlevels,PetscInt,&refy,nlevels,PetscInt,&refz);CHKERRQ(ierr);
  for (i=0; i<nlevels; i++) {
    ierr = DMDAGetRefinementFactor(da,&refx[i],&refy[i],&refz[i]);CHKERRQ(ierr);
  }
  n = nlevels;
  ierr = PetscOptionsGetIntArray(((PetscObject)da)->prefix,"-da_refine_hierarchy_x",refx,&n,PETSC_NULL);CHKERRQ(ierr);
  n = nlevels;
  ierr = PetscOptionsGetIntArray(((PetscObject)da)->prefix,"-da_refine_hierarchy_y",refy,&n,PETSC_NULL);CHKERRQ(ierr);
  n = nlevels;
  ierr = PetscOptionsGetIntArray(((PetscObject)da)->prefix,"-da_refine_hierarchy_z",refz,&n,PETSC_NULL);CHKERRQ(ierr);
	
	
	ierr = DMDASetRefinementFactor(da,refx[nlevels-1],refy[nlevels-1],refz[nlevels-1]);CHKERRQ(ierr);
  ierr = DMCoarsen(da,((PetscObject)da)->comm,&dac[0]);CHKERRQ(ierr);
  for (i=1; i<nlevels; i++) {
    ierr = DMDASetRefinementFactor(dac[i-1],refx[nlevels-1-i],refy[nlevels-1-i],refz[nlevels-1-i]);CHKERRQ(ierr);
    ierr = DMCoarsen(dac[i-1],((PetscObject)da)->comm,&dac[i]);CHKERRQ(ierr);
  }
  ierr = PetscFree3(refx,refy,refz);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "UpdateMeshGeometry_FullLagSurface"
PetscErrorCode UpdateMeshGeometry_FullLagSurface(DM dav,Vec velocity,PetscReal step)
{
	Vec            coordinates;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ierr = DMDAGetCoordinates(dav,&coordinates);CHKERRQ(ierr);
	ierr = VecAXPY(coordinates,step,velocity);CHKERRQ(ierr); /* x = x + dt.vel_advect_mesh */
	
	ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "UpdateMeshGeometry_FullLagSurface_fricG2008"
PetscErrorCode UpdateMeshGeometry_FullLagSurface_fricG2008(DM dav,Vec velocity,PetscReal step)
{
	Vec            coordinates,vel_advect_mesh;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ierr = DMDAGetCoordinates(dav,&coordinates);CHKERRQ(ierr);
    ierr = VecAXPY(coordinates,step,velocity);CHKERRQ(ierr); /* x = x + dt.vel_advect_mesh */
	ierr = RMTSetUniformCoordinatesRange_X_Bottom(dav,coordinates);
	ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "UpdateMeshGeometry_VerticalDisplacement"
PetscErrorCode UpdateMeshGeometry_VerticalDisplacement(DM dav,Vec velocity,PetscReal step)
{
	Vec coordinates,vel_advect_mesh;
	PetscInt M,N;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ierr = DMGetGlobalVector(dav,&vel_advect_mesh);CHKERRQ(ierr);
	
	ierr = VecCopy(velocity,vel_advect_mesh);CHKERRQ(ierr);
	ierr = VecStrideScale(vel_advect_mesh,0,0.0);CHKERRQ(ierr); /* zero out the x-componenet */
	
	ierr = DMDAGetCoordinates(dav,&coordinates);CHKERRQ(ierr);
	ierr = VecAXPY(coordinates,step,vel_advect_mesh);CHKERRQ(ierr); /* x = x + dt.vel_advect_mesh */
	
	ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
	
	ierr = DMRestoreGlobalVector(dav,&vel_advect_mesh);CHKERRQ(ierr);
	
	/* remesh nodes using upper and lower surfaces */
	ierr = DMDAGetInfo(dav,0,&M,&N,0,0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);
	ierr = DMDARemeshSetUniformCoordinatesBetweenJLayers2d(dav,0,N);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

/* for each xi in T0, interpolate the values from T1 */
#undef __FUNCT__  
#define __FUNCT__ "SurfaceInterpolation2D"
PetscErrorCode SurfaceInterpolation2D(DM Tsurf_0,PetscScalar ***LA_coor0,DM Tsurf_1,PetscScalar ***LA_coor1)
{
	PetscInt nx_0,ny_0,si_0,sj_0;
	PetscInt nx_1,ny_1,si_1,sj_1;
	PetscInt i,ii,j;
	PetscScalar x_0,y_0;
	PetscScalar x_1[2],y_1[2];
	PetscScalar dx,xi_1;
	PetscScalar Ni[2];
	PetscErrorCode ierr;
	
	PetscFunctionBegin;

	ierr = DMDAGetCorners( Tsurf_0, &si_0,&sj_0,0, &nx_0,&ny_0,0 );CHKERRQ(ierr);
	ierr = DMDAGetCorners( Tsurf_1, &si_1,&sj_1,0, &nx_1,&ny_1,0 );CHKERRQ(ierr);
	
	j = 0;
	for (i=si_0; i<si_0+nx_0; i++) {
		PetscBool found = PETSC_FALSE;

		x_0 = LA_coor0[j][i][0];
		
		/* find which cell in T1 contains */
		for (ii=sj_1; ii<sj_1+nx_1-1; ii++) {
			x_1[0] = LA_coor1[j][ii][0];
			y_1[0] = LA_coor1[j][ii][1];

			x_1[1] = LA_coor1[j][ii+1][0];
			y_1[1] = LA_coor1[j][ii+1][1];
			
			//printf("  %1.9e <=? %1.9e <? %1.9e \n", x_1[0], x_0, x_1[1] );
			if ( (x_0>=x_1[0]) && (x_0<=x_1[1]) ) {
				found = PETSC_TRUE;
				break;
			}
			if ( (fabs(x_0-x_1[0]) < 1.0e-9) ) {
				found = PETSC_TRUE;
				break;
			}
			if ( (fabs(x_0-x_1[1]) < 1.0e-9) ) {
				found = PETSC_TRUE;
				break;
			}
			
		}
		if (found==PETSC_FALSE) {
			PetscPrintf(PETSC_COMM_SELF,"ERROR: Could not locate surface node");
			PetscPrintf(PETSC_COMM_SELF,"  x_0 = %1.9e \n", x_0 );
			
			SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"  Could not locate surface node");
		}
		/* interpolate values */
		/* (xp - x0)/dx = (xip+1)/2 */
		dx = (x_1[1] - x_1[0]);
		xi_1 = 2.0*(x_0 - x_1[0])/dx - 1.0;
	
		Ni[0] = 0.5 * ( 1.0 - xi_1 );
		Ni[1] = 0.5 * ( 1.0 + xi_1 );
		
		y_0 = Ni[0] * y_1[0] + Ni[1] * y_1[1];

		LA_coor0[j][i][1] = y_0;
	}
	
	
	PetscFunctionReturn(0);
}

/* for each xi in T0, interpolate the values from T1 */
#undef __FUNCT__  
#define __FUNCT__ "SurfaceInterpolation_withExtrapolationAtEndPoints2D"
PetscErrorCode SurfaceInterpolation_withExtrapolationAtEndPoints2D(DM Tsurf_0,PetscScalar ***LA_coor0,DM Tsurf_1,PetscScalar ***LA_coor1)
{
	PetscInt nx_0,ny_0,si_0,sj_0;
	PetscInt nx_1,ny_1,si_1,sj_1;
	PetscInt i,ii,j;
	PetscScalar x_0,y_0;
	PetscScalar x_1[2],y_1[2];
	PetscScalar dx,xi_1;
	PetscScalar Ni[2];
	PetscReal   jmin_T0[3],jmax_T0[3];
	PetscReal   jmin_T1[3],jmax_T1[3];
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	
	ierr = DMDAGetBoundingBox(Tsurf_0,jmin_T0,jmax_T0);CHKERRQ(ierr);
	ierr = DMDAGetBoundingBox(Tsurf_1,jmin_T1,jmax_T1);CHKERRQ(ierr);
	
	ierr = DMDAGetCorners( Tsurf_0, &si_0,&sj_0,0, &nx_0,&ny_0,0 );CHKERRQ(ierr);
	ierr = DMDAGetCorners( Tsurf_1, &si_1,&sj_1,0, &nx_1,&ny_1,0 );CHKERRQ(ierr);
	
	j = 0;
	for (i=si_0; i<si_0+nx_0; i++) {
		PetscBool found = PETSC_FALSE;
		
		x_0 = LA_coor0[j][i][0];
		
		if (x_0 < jmin_T1[0]) {
			PetscScalar y_star;
			
			y_star = LA_coor1[j][i][1];
			LA_coor0[j][i][1] = y_star;
			continue;
		}

		if (x_0 > jmax_T1[0]) {
			PetscScalar y_star;
			
			y_star = LA_coor1[j][i][1];
			LA_coor0[j][i][1] = y_star;
			continue;
		}
		
		/* find which cell in T1 contains */
		for (ii=sj_1; ii<sj_1+nx_1-1; ii++) {
			x_1[0] = LA_coor1[j][ii][0];
			y_1[0] = LA_coor1[j][ii][1];
			
			x_1[1] = LA_coor1[j][ii+1][0];
			y_1[1] = LA_coor1[j][ii+1][1];
			
			//printf("  %1.9e <=? %1.9e <? %1.9e \n", x_1[0], x_0, x_1[1] );
			if ( (x_0>=x_1[0]) && (x_0<=x_1[1]) ) {
				found = PETSC_TRUE;
				break;
			}
			if ( (fabs(x_0-x_1[0]) < 1.0e-9) ) {
				found = PETSC_TRUE;
				break;
			}
			if ( (fabs(x_0-x_1[1]) < 1.0e-9) ) {
				found = PETSC_TRUE;
				break;
			}
			
		}
		if (found==PETSC_FALSE) {
			PetscPrintf(PETSC_COMM_SELF,"ERROR: Could not locate surface node");
			PetscPrintf(PETSC_COMM_SELF,"  x_0 = %1.9e \n", x_0 );
			
			SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"  Could not locate surface node");
		}
		/* interpolate values */
		/* (xp - x0)/dx = (xip+1)/2 */
		dx = (x_1[1] - x_1[0]);
		xi_1 = 2.0*(x_0 - x_1[0])/dx - 1.0;
		
		Ni[0] = 0.5 * ( 1.0 - xi_1 );
		Ni[1] = 0.5 * ( 1.0 + xi_1 );
		
		y_0 = Ni[0] * y_1[0] + Ni[1] * y_1[1];
		
		LA_coor0[j][i][1] = y_0;
	}
	
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "DMDAGetLocalSurfaceBoundingBox"
/*@
 DMDAGetLocalBoundingBox - Returns the local bounding box for the DMDA.
 
 Not Collective
 
 Input Parameter:
 .  da - the distributed array
 
 Output Parameters:
 +  lmin - local minimum coordinates (length dim, optional)
 -  lmax - local maximim coordinates (length dim, optional)
 
 Level: beginner
 
 .keywords: distributed array, get, coordinates
 
 .seealso: DMDAGetCoordinateDA(), DMDAGetCoordinates(), DMDAGetBoundingBox()
 @*/
PetscErrorCode  DMDAGetLocalSurfaceBoundingBox(DM da,PetscInt direction,PetscReal lmin[],PetscReal lmax[])
{
  PetscErrorCode    ierr;
  Vec               coords  = PETSC_NULL;
  PetscInt          dim;
  const PetscScalar *local_coords;
  PetscReal         min[3]={PETSC_MAX_REAL,PETSC_MAX_REAL,PETSC_MAX_REAL},max[3]={PETSC_MIN_REAL,PETSC_MIN_REAL,PETSC_MIN_REAL};
  PetscInt          I,i,j,k,m,n,p,d,M,N,P,si,sj,sk;
	
  PetscFunctionBegin;
  PetscValidHeaderSpecific(da,DM_CLASSID,1);
  ierr = DMDAGetCoordinates(da,&coords);CHKERRQ(ierr);
  ierr = VecGetArrayRead(coords,&local_coords);CHKERRQ(ierr);

	ierr = DMDAGetInfo(da,&dim,&M,&N,&P,0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);
	ierr = DMDAGetCorners(da,&si,&sj,&sk,&m,&n,&p);CHKERRQ(ierr);
	switch (dim) {
		case 1:
				for (i=0; i<m; i++) {
					I = i;
					
					if ( (i+si) != M-1 ) { continue; }
					
					for (d=0; d<dim; d++) {
						min[d] = PetscMin(min[d],PetscRealPart(local_coords[I*dim+d]));CHKERRQ(ierr);
						max[d] = PetscMax(min[d],PetscRealPart(local_coords[I*dim+d]));CHKERRQ(ierr);
					}
				}
			break;

		case 2:
			for (j=0; j<n; j++) {
				for (i=0; i<m; i++) {
					I = i + j*m;
					
					if (direction==0) {
						if ( (i+si) != M-1 ) { continue; }
					} else {
						if ( (j+sj) != N-1 ) { continue; }
					}
					
					for (d=0; d<dim; d++) {
						min[d] = PetscMin(min[d],PetscRealPart(local_coords[I*dim+d]));CHKERRQ(ierr);
						max[d] = PetscMax(min[d],PetscRealPart(local_coords[I*dim+d]));CHKERRQ(ierr);
					}
				}
			}
			break;
			
		case 3:
			for (k=0; k<p; k++) {
				for (j=0; j<n; j++) {
					for (i=0; i<m; i++) {
						I = i + j*m + k*m*n;
						
						if (direction==0) {
							if ( (i+si) != M-1 ) { continue; }
						} else if (direction==1) {
							if ( (j+sj) != N-1 ) { continue; }
						} else {
							if ( (k+sk) != P-1 ) { continue; }
						}
						
						for (d=0; d<dim; d++) {
							min[d] = PetscMin(min[d],PetscRealPart(local_coords[I*dim+d]));CHKERRQ(ierr);
							max[d] = PetscMax(min[d],PetscRealPart(local_coords[I*dim+d]));CHKERRQ(ierr);
						}
					}
				}
			}
			break;
	}
	
  ierr = VecRestoreArrayRead(coords,&local_coords);CHKERRQ(ierr);
  if (lmin) {ierr = PetscMemcpy(lmin,min,dim*sizeof(PetscReal));CHKERRQ(ierr);}
  if (lmax) {ierr = PetscMemcpy(lmax,max,dim*sizeof(PetscReal));CHKERRQ(ierr);}
  PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "DMDAGetSurfaceBoundingBox"
/*@
 DMDAGetBoundingBox - Returns the global bounding box for the DMDA.
 
 Collective on DMDA
 
 Input Parameter:
 .  da - the distributed array
 
 Output Parameters:
 +  gmin - global minimum coordinates (length dim, optional)
 -  gmax - global maximim coordinates (length dim, optional)
 
 Level: beginner
 
 .keywords: distributed array, get, coordinates
 
 .seealso: DMDAGetCoordinateDA(), DMDAGetCoordinates(), DMDAGetLocalBoundingBox()
 @*/
PetscErrorCode  DMDAGetSurfaceBoundingBox(DM da,PetscInt direction,PetscReal gmin[],PetscReal gmax[])
{
  PetscErrorCode ierr;
  PetscMPIInt    count;
  PetscReal      lmin[3],lmax[3];
	PetscInt       dim;
	
  PetscFunctionBegin;
  PetscValidHeaderSpecific(da,DM_CLASSID,1);
	ierr = DMDAGetInfo(da,&dim,0,0,0,0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);
  count = PetscMPIIntCast(dim);
  ierr = DMDAGetLocalSurfaceBoundingBox(da,direction,lmin,lmax);CHKERRQ(ierr);
  if (gmin) {ierr = MPI_Allreduce(lmin,gmin,count,MPIU_REAL,MPIU_MIN,((PetscObject)da)->comm);CHKERRQ(ierr);}
  if (gmax) {ierr = MPI_Allreduce(lmax,gmax,count,MPIU_REAL,MPIU_MAX,((PetscObject)da)->comm);CHKERRQ(ierr);}
  PetscFunctionReturn(0);
}

/* 
 Copy all surface nodes
 Zero all velocity dofs expect those on the surface
 Advect all nodes
 Extract advected surface + overlap
 Interpolate
 Push interpolated values into locally owned coordinate vector
 Scatter into ghosted vector
*/

#undef __FUNCT__  
#define __FUNCT__ "UpdateMeshGeometry_FullLagSurfaceJMAX_RemeshInteriorJMIN_JMAX"
PetscErrorCode UpdateMeshGeometry_FullLagSurfaceJMAX_RemeshInteriorJMIN_JMAX(DM dav,Vec velocity,PetscReal step)
{
	Vec coordinates,vel_advect_mesh;
	PetscInt i,j,nx,ny,NX,NY,si,sj;
	PetscScalar ***LA_velocity;
	DM T_surface_0, T_surface_1;
	PetscInt start_i, end_i;
	PetscInt nx_0,ny_0,si_0,sj_0;
	Vec T_surf_0_coordinates, T_surf_1_coordinates;
	PetscScalar ***LA_T_surf_0_coor, ***LA_T_surf_1_coor;
	DM cda_T_surf_0,cda_T_surf_1;
	PetscScalar ***LA_coor;
	PetscInt si_g,ei_g;
	DM cda_dav;
	PetscMPIInt rank;
	PetscReal jmin[3],jmax[3];
	PetscReal dx_0;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	MPI_Comm_rank(((PetscObject)dav)->comm,&rank);

	/* Copy all surface nodes */
	ierr = DMDAGetCorners( dav, &si,&sj,0, &nx,&ny,0 );CHKERRQ(ierr);
	ierr = DMDAGetInfo(dav,0, &NX,&NY,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
#ifdef REMESH_DBG	
	PetscPrintf(PETSC_COMM_SELF,"tau_0: fetching [%D <= i < %D], [j = %D] \n", si,si+nx, NY-1 );
#endif	
	ierr = DMDACreate2dRedundant( dav, si,si+nx, NY-1,  NY, 1, &T_surface_0 );CHKERRQ(ierr);

#ifdef REMESH_DBG
	{
		ierr = DMDAGetBoundingBox(T_surface_0,jmin,jmax);CHKERRQ(ierr);
		printf("T_0] domain x := [ %1.4e , %1.9e ] \n", jmin[0], jmax[0] );
		printf("T_0] domain y := [ %1.4e , %1.9e ] \n", jmin[1], jmax[1] );
	}
#endif	
	
	/* Zero all velocity dofs expect those on the surface */
	ierr = DMGetGlobalVector(dav,&vel_advect_mesh);CHKERRQ(ierr);
	ierr = VecCopy(velocity,vel_advect_mesh);CHKERRQ(ierr);
	
	ierr = DMDAGetCorners(dav,&si,&sj,0,&nx,&ny,0);CHKERRQ(ierr);
	ierr = DMDAGetInfo(dav,0, &NX,&NY,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(dav,vel_advect_mesh,&LA_velocity);CHKERRQ(ierr);
	for (j=sj; j<sj+ny; j++) {
		if (j!=(NY-1)) {
			for (i=si; i<si+nx; i++) {
				LA_velocity[j][i][0] = 0.0; /* vx */
				LA_velocity[j][i][1] = 0.0; /* vy */
			}
		}
	}
	ierr = DMDAVecRestoreArrayDOF(dav,vel_advect_mesh,&LA_velocity);CHKERRQ(ierr);
	
	
	/* Advect all nodes */
	ierr = DMDAGetCoordinates(dav,&coordinates);CHKERRQ(ierr);
	ierr = VecAXPY(coordinates,step,vel_advect_mesh);CHKERRQ(ierr); /* x = x + dt.vel_advect_mesh */
	ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
	
	ierr = DMRestoreGlobalVector(dav,&vel_advect_mesh);CHKERRQ(ierr);

	/* Extract advected surface + overlap */
	ierr = DMDAGetCorners( dav, &si,&sj,0, &nx,&ny,0 );CHKERRQ(ierr);
	ierr = DMDAGetInfo(dav,0, &NX,&NY,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
	start_i = PetscMax(0,si-2);
	end_i   = PetscMin(NX,si+nx+2);
#ifdef REMESH_DBG	
	PetscPrintf(PETSC_COMM_SELF,"tau_1: fetching [%D <= i < %D], [j = %D] \n", start_i,end_i, NY-1 );
#endif
	ierr = DMDACreate2dRedundant( dav, start_i,end_i, NY-1,NY, 1, &T_surface_1 );CHKERRQ(ierr);

	/* re-size and uniformly space in nodes in x direction on surface, T0 */
//	ierr = DMDAGetBoundingBox(T_surface_1,jmin,jmax);CHKERRQ(ierr);
//	ierr = DMDAGetBoundingBox(dav,jmin,jmax);CHKERRQ(ierr);
	ierr = DMDAGetSurfaceBoundingBox(dav,1,jmin,jmax);CHKERRQ(ierr);

	dx_0 = (jmax[0]-jmin[0])/(PetscReal)(NX-1);
#ifdef REMESH_DBG	
	printf("dx_0 = %1.4e : jmin[0] = %1.4e : NX = %d \n", dx_0,jmin[0],NX );
#endif
	
	/* (init y to be zero) */
	ierr = DMDAGetCoordinateDA(T_surface_0,&cda_T_surf_0);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(T_surface_0,&T_surf_0_coordinates);CHKERRQ(ierr);
	ierr = DMDAGetCorners( T_surface_0, &si_0,&sj_0,0, &nx_0,&ny_0,0 );CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
#ifdef REMESH_DBG	
	PetscPrintf(PETSC_COMM_SELF,"T_surface_0: si-ej [%D <= i < %D], [j = %D] \n", si_0,si_0+nx_0, sj_0 );
#endif
	for (j=sj_0; j<sj_0+ny_0; j++) { /* this loop is redundant as it's a surface object */
		for (i=si_0; i<si_0+nx_0; i++) {
			LA_T_surf_0_coor[j][i][0] = jmin[0] + si * dx_0 + i * dx_0;
			//LA_T_surf_0_coor[j][i][0] = jmin[0] + i * dx_0;
#ifdef REMESH_DBG	
			printf("i_red=%d: i_g=%d: x = %1.4e \n", i, si+i, LA_T_surf_0_coor[j][i][0] );
#endif
		}
	}
	ierr = DMDAVecRestoreArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
	//ierr = VecView(T_surf_0_coordinates,PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr);
	
#ifdef REMESH_DBG	
	{
		PetscReal jmin[3],jmax[3]; 
		
		DMDAGetBoundingBox(T_surface_1,jmin,jmax);
		printf("T_1] domain x := [ %1.4e , %1.9e ] \n", jmin[0], jmax[0] );
		printf("T_1] domain y := [ %1.4e , %1.9e ] \n", jmin[1], jmax[1] );
	}
#endif

	/*  Interpolate from T_surface_1(y) => T_surface_0(y) */
	/* (init y to be zero) */
	ierr = DMDAGetCoordinateDA(T_surface_0,&cda_T_surf_0);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(T_surface_0,&T_surf_0_coordinates);CHKERRQ(ierr);
	ierr = DMDAGetCorners( T_surface_0, &si_0,&sj_0,0, &nx_0,&ny_0,0 );CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
	for (j=sj_0; j<sj_0+ny_0; j++) { /* this loop is redundant as it's a surface object */
		for (i=si_0; i<si_0+nx_0; i++) {
			/* LA_T_surf_0_coor[j][i][0] = 0.0; */ /* x */
			LA_T_surf_0_coor[j][i][1] = 0.0; /* y - height */
		}
	}
	ierr = DMDAVecRestoreArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);

	
	ierr = DMDAGetCoordinateDA(T_surface_0,&cda_T_surf_0);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(T_surface_0,&T_surf_0_coordinates);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
	
	ierr = DMDAGetCoordinateDA(T_surface_1,&cda_T_surf_1);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(T_surface_1,&T_surf_1_coordinates);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cda_T_surf_1,T_surf_1_coordinates,&LA_T_surf_1_coor);CHKERRQ(ierr);
	
	/* do the interp */
	ierr = SurfaceInterpolation2D(T_surface_0,LA_T_surf_0_coor, T_surface_1,LA_T_surf_1_coor);CHKERRQ(ierr);

	ierr = DMDAVecRestoreArrayDOF(cda_T_surf_1,T_surf_1_coordinates,&LA_T_surf_1_coor);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);	

#ifdef REMESH_DBG
	{
		PetscReal jmin[3],jmax[3]; 
		
		ierr = DMDAGetBoundingBox(T_surface_1,jmin,jmax);CHKERRQ(ierr);
		printf("a T_1] domain x := [ %1.4e , %1.4e ] \n", jmin[0], jmax[0] );
		printf("a T_1] domain y := [ %1.4e , %1.4e ] \n", jmin[1], jmax[1] );

		
		ierr = DMDAGetBoundingBox(T_surface_0,jmin,jmax);CHKERRQ(ierr);
		printf("b T_0] domain x := [ %1.4e , %1.4e ] \n", jmin[0], jmax[0] );
		printf("b T_0] domain y := [ %1.4e , %1.4e ] \n", jmin[1], jmax[1] );
	}
#endif
	
	/* ---- hack some shit just to fake the interpolation for the time being ---- */
#if 0
	ierr = DMDAVecGetArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
	for (j=sj_0; j<sj_0+ny_0; j++) { /* this loop is redundant as it's a surface object */
		for (i=si_0; i<si_0+nx_0; i++) {
			PetscScalar xn;
			
			xn = LA_T_surf_0_coor[j][i][0];
			/* LA_T_surf_0_coor[j][i][0] = 0.0; */ /* x */
			LA_T_surf_0_coor[j][i][1] = 4.5 + 0.2*sin(xn*M_PI/1.4); /* y - height */
		}
	}
	ierr = DMDAVecRestoreArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
#endif	
	/* ---- end hack ---- */
	
	
	/* force box size */
	{
		PetscReal jmin_T0[3],jmax_T0[3]; 
		PetscReal jmin_V0[3],jmax_V0[3]; 
		
		/* fetch x size from the surface mesh */
		//ierr = DMDAGetBoundingBox(T_surface_1,jmin_T0,jmax_T0);CHKERRQ(ierr);
		ierr = DMDAGetSurfaceBoundingBox(dav,1,jmin_T0,jmax_T0);CHKERRQ(ierr);
		/* fetch any y size from the volume mesh */
		ierr = DMDAGetBoundingBox(dav,jmin_V0,jmax_V0);CHKERRQ(ierr);

		ierr = DMDASetUniformCoordinates(dav,jmin_T0[0],jmax_T0[0],jmin_V0[1],jmax_V0[1],PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
	}
	
	/* Push interpolated values into locally owned coordinate vector */
	ierr = DMDAGetCorners( T_surface_0, &si_0,&sj_0,0, &nx_0,&ny_0,0 );CHKERRQ(ierr);
	si_g = si_0;
	ei_g = si_0 + nx_0;
	//printf("[%d] si_g = %d : ei_g = %d \n", rank, si_g,ei_g);
	ierr = DMDAGetCoordinateDA(dav,&cda_dav);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(dav,&coordinates);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cda_dav,coordinates,&LA_coor);CHKERRQ(ierr);
	
	ierr = DMDAVecGetArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
	if ( (sj+ny) == (NY) ) {
		for (i=si_g; i<ei_g; i++) {
#ifdef REMESH_DBG 
			printf("[%d] mapping LA_Tsurf0[%d] ==> LA_coor[%d] \n", rank, i, si + (i-si_g) ); 
#endif
			LA_coor[NY-1][si + (i-si_g)][0] = LA_T_surf_0_coor[0][i][0]; /* x */
			LA_coor[NY-1][si + (i-si_g)][1] = LA_T_surf_0_coor[0][i][1]; /* y - height */
		}
	}	
	ierr = DMDAVecRestoreArrayDOF(cda_dav,coordinates,&LA_coor);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
	
	
	/* Scatter into ghosted vector */
	ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);

	
	/* remesh nodes using upper and lower surfaces */
	ierr = DMDARemeshSetUniformCoordinatesBetweenJLayers2d(dav,0,NY);CHKERRQ(ierr);
	
	ierr = DMDestroy(&T_surface_1);CHKERRQ(ierr);
	ierr = DMDestroy(&T_surface_0);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "UpdateMeshGeometry_FullLagSurfaceJMIN_JMAX_RemeshInteriorJMIN_JMAX"
PetscErrorCode UpdateMeshGeometry_FullLagSurfaceJMIN_JMAX_RemeshInteriorJMIN_JMAX(DM dav,Vec velocity,PetscReal step)
{
	Vec coordinates,vel_advect_mesh;
	PetscInt i,j,nx,ny,NX,NY,si,sj;
	PetscScalar ***LA_velocity;
	PetscScalar ***LA_coor;
	PetscInt si_g,ei_g;
	DM cda_dav;
	PetscMPIInt rank;
	PetscReal jmin[3],jmax[3];
	PetscReal dx_0;
	
	DM T_surface_0, T_surface_1; 
	DM B_surface_0, B_surface_1;
	Vec T_surf_0_coordinates, T_surf_1_coordinates;
	Vec B_surf_0_coordinates, B_surf_1_coordinates;
	PetscScalar ***LA_T_surf_0_coor, ***LA_T_surf_1_coor;
	PetscScalar ***LA_B_surf_0_coor, ***LA_B_surf_1_coor;
	DM cda_T_surf_0,cda_T_surf_1;
	DM cda_B_surf_0,cda_B_surf_1;
	PetscInt start_i, end_i;
	PetscInt nx_0,ny_0,si_0,sj_0;
	
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	MPI_Comm_rank(((PetscObject)dav)->comm,&rank);

	/* Copy all surface nodes */
	ierr = DMDAGetCorners( dav, &si,&sj,0, &nx,&ny,0 );CHKERRQ(ierr);
	ierr = DMDAGetInfo(dav,0, &NX,&NY,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
#ifdef REMESH_DBG	
	PetscPrintf(PETSC_COMM_SELF,"tau_0: fetching [%D <= i < %D], [j = %D] \n", si,si+nx, NY-1 );
#endif	
	ierr = DMDACreate2dRedundant( dav, si,si+nx, NY-1,  NY, 1, &T_surface_0 );CHKERRQ(ierr);
	ierr = DMDACreate2dRedundant( dav, si,si+nx, 0   , 1  , 1, &B_surface_0 );CHKERRQ(ierr);
#ifdef REMESH_DBG
	{
		ierr = DMDAGetBoundingBox(T_surface_0,jmin,jmax);CHKERRQ(ierr);
		printf("T_0] domain x := [ %1.4e , %1.9e ] \n", jmin[0], jmax[0] );
		printf("T_0] domain y := [ %1.4e , %1.9e ] \n", jmin[1], jmax[1] );
	}
#endif	
	
	/* Zero all velocity dofs expect those on the surface and bottom */
	ierr = DMGetGlobalVector(dav,&vel_advect_mesh);CHKERRQ(ierr);
	ierr = VecCopy(velocity,vel_advect_mesh);CHKERRQ(ierr);
	
	ierr = DMDAGetCorners(dav,&si,&sj,0,&nx,&ny,0);CHKERRQ(ierr);
	ierr = DMDAGetInfo(dav,0, &NX,&NY,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(dav,vel_advect_mesh,&LA_velocity);CHKERRQ(ierr);
	for (j=sj; j<sj+ny; j++) {
		if ((j!=(NY-1)) & (j!=(0))) {
			for (i=si; i<si+nx; i++) {
				LA_velocity[j][i][0] = 0.0; /* vx */
				LA_velocity[j][i][1] = 0.0; /* vy */
			}
		}
	}
	ierr = DMDAVecRestoreArrayDOF(dav,vel_advect_mesh,&LA_velocity);CHKERRQ(ierr);
	
	
	/* Advect all nodes */
	ierr = DMDAGetCoordinates(dav,&coordinates);CHKERRQ(ierr);
	ierr = VecAXPY(coordinates,step,vel_advect_mesh);CHKERRQ(ierr); /* x = x + dt.vel_advect_mesh */
	ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
	
	ierr = DMRestoreGlobalVector(dav,&vel_advect_mesh);CHKERRQ(ierr);

	/* Extract advected surface + overlap */
	ierr = DMDAGetCorners( dav, &si,&sj,0, &nx,&ny,0 );CHKERRQ(ierr);
	ierr = DMDAGetInfo(dav,0, &NX,&NY,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
	start_i = PetscMax(0,si-2);
	end_i   = PetscMin(NX,si+nx+2);
#ifdef REMESH_DBG	
	PetscPrintf(PETSC_COMM_SELF,"tau_1: fetching [%D <= i < %D], [j = %D] \n", start_i,end_i, NY-1 );
#endif
//Set Uniform spacing in x (same at top and bottom)
    ierr = DMDAGetSurfaceBoundingBox(dav,1,jmin,jmax);CHKERRQ(ierr);
	dx_0 = (jmax[0]-jmin[0])/(PetscReal)(NX-1);

	// create a copy of the coordinates of top surface
	ierr = DMDACreate2dRedundant( dav, start_i,end_i, NY-1,NY, 1, &T_surface_1 );CHKERRQ(ierr);
	
#ifdef REMESH_DBG	
	printf("dx_0 = %1.4e : jmin[0] = %1.4e : NX = %d \n", dx_0,jmin[0],NX );
#endif
	
	/* (init y top to be zero because we interpolate y on the newly set uniform spacing in x) */
	ierr = DMDAGetCoordinateDA(T_surface_0,&cda_T_surf_0);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(T_surface_0,&T_surf_0_coordinates);CHKERRQ(ierr);
	ierr = DMDAGetCorners( T_surface_0, &si_0,&sj_0,0, &nx_0,&ny_0,0 );CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
#ifdef REMESH_DBG	
	PetscPrintf(PETSC_COMM_SELF,"T_surface_0: si-ej [%D <= i < %D], [j = %D] \n", si_0,si_0+nx_0, sj_0 );
#endif

	for (j=sj_0; j<sj_0+ny_0; j++) { /* this loop is redundant as it's a surface object */
		for (i=si_0; i<si_0+nx_0; i++) {
			LA_T_surf_0_coor[j][i][0] = jmin[0] + si * dx_0 + i * dx_0;
#ifdef REMESH_DBG	
			printf("i_red=%d: i_g=%d: x = %1.4e \n", i, si+i, LA_T_surf_0_coor[j][i][0] );
#endif
		}
	}
    ierr = DMDAVecRestoreArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);

	/*  Interpolate from T_surface_1(y) => T_surface_0(y) */
	/* (init y to be zero) */
	ierr = DMDAGetCoordinateDA(T_surface_0,&cda_T_surf_0);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(T_surface_0,&T_surf_0_coordinates);CHKERRQ(ierr);
	ierr = DMDAGetCorners( T_surface_0, &si_0,&sj_0,0, &nx_0,&ny_0,0 );CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
	for (j=sj_0; j<sj_0+ny_0; j++) { /* this loop is redundant as it's a surface object */
		for (i=si_0; i<si_0+nx_0; i++) {
			LA_T_surf_0_coor[j][i][1] = 0.0; /* y - height */
		}
	}
	ierr = DMDAVecRestoreArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);

	
	ierr = DMDAGetCoordinateDA(T_surface_0,&cda_T_surf_0);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(T_surface_0,&T_surf_0_coordinates);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
	
	ierr = DMDAGetCoordinateDA(T_surface_1,&cda_T_surf_1);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(T_surface_1,&T_surf_1_coordinates);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cda_T_surf_1,T_surf_1_coordinates,&LA_T_surf_1_coor);CHKERRQ(ierr);
	
	/* do the interp */
	ierr = SurfaceInterpolation2D(T_surface_0,LA_T_surf_0_coor, T_surface_1,LA_T_surf_1_coor);CHKERRQ(ierr);

	ierr = DMDAVecRestoreArrayDOF(cda_T_surf_1,T_surf_1_coordinates,&LA_T_surf_1_coor);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);	

#ifdef REMESH_DBG
	{
		PetscReal jmin[3],jmax[3]; 
		
		ierr = DMDAGetBoundingBox(T_surface_1,jmin,jmax);CHKERRQ(ierr);
		printf("a T_1] domain x := [ %1.4e , %1.4e ] \n", jmin[0], jmax[0] );
		printf("a T_1] domain y := [ %1.4e , %1.4e ] \n", jmin[1], jmax[1] );

		
		ierr = DMDAGetBoundingBox(T_surface_0,jmin,jmax);CHKERRQ(ierr);
		printf("b T_0] domain x := [ %1.4e , %1.4e ] \n", jmin[0], jmax[0] );
		printf("b T_0] domain y := [ %1.4e , %1.4e ] \n", jmin[1], jmax[1] );
	}
#endif

//BOTTOM=================================================
    // create a copy of the coordinates of bottom surface
	ierr = DMDACreate2dRedundant( dav, start_i,end_i, 0   , 1, 1, &B_surface_1 );CHKERRQ(ierr);

	ierr = DMDAGetCoordinateDA(B_surface_0,&cda_B_surf_0);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(B_surface_0,&B_surf_0_coordinates);CHKERRQ(ierr);
	ierr = DMDAGetCorners( B_surface_0, &si_0,&sj_0,0, &nx_0,&ny_0,0 );CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cda_B_surf_0,B_surf_0_coordinates,&LA_B_surf_0_coor);CHKERRQ(ierr);
#ifdef REMESH_DBG	
	PetscPrintf(PETSC_COMM_SELF,"B_surface_0: si-ej [%D <= i < %D], [j = %D] \n", si_0,si_0+nx_0, sj_0 );
#endif
     // set uniform coordinate in x and  y coordinate to zero in the copy 
	for (j=sj_0; j<sj_0+ny_0; j++) { /* this loop is redundant as it's a surface object */
		for (i=si_0; i<si_0+nx_0; i++) {
			LA_B_surf_0_coor[j][i][0] = jmin[0] + si * dx_0 + i * dx_0;
			LA_B_surf_0_coor[j][i][1] = 0.0; /* y - height */
#ifdef REMESH_DBG	
			printf("i_red=%d: i_g=%d: x = %1.4e \n", i, si+i, LA_B_surf_0_coor[j][i][0] );
#endif
		}
	}
	ierr = DMDAVecRestoreArrayDOF(cda_B_surf_0,B_surf_0_coordinates,&LA_B_surf_0_coor);CHKERRQ(ierr);
#ifdef REMESH_DBG	
	{
		PetscReal jmin[3],jmax[3]; 
		
		DMDAGetBoundingBox(B_surface_1,jmin,jmax);
		printf("T_1] domain x := [ %1.4e , %1.9e ] \n", jmin[0], jmax[0] );
		printf("T_1] domain y := [ %1.4e , %1.9e ] \n", jmin[1], jmax[1] );
	}
#endif

	ierr = DMDAGetCoordinateDA(B_surface_0,&cda_B_surf_0);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(B_surface_0,&B_surf_0_coordinates);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cda_B_surf_0,B_surf_0_coordinates,&LA_B_surf_0_coor);CHKERRQ(ierr);
	
	ierr = DMDAGetCoordinateDA(B_surface_1,&cda_B_surf_1);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(B_surface_1,&B_surf_1_coordinates);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cda_B_surf_1,B_surf_1_coordinates,&LA_B_surf_1_coor);CHKERRQ(ierr);
	
	/* do the interp */
	ierr = SurfaceInterpolation2D(B_surface_0,LA_B_surf_0_coor, B_surface_1,LA_B_surf_1_coor);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArrayDOF(cda_B_surf_1,B_surf_1_coordinates,&LA_B_surf_1_coor);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArrayDOF(cda_B_surf_0,B_surf_0_coordinates,&LA_B_surf_0_coor);CHKERRQ(ierr);	

#ifdef REMESH_DBG
	{
		PetscReal jmin[3],jmax[3]; 
		
		ierr = DMDAGetBoundingBox(T_surface_1,jmin,jmax);CHKERRQ(ierr);
		printf("a T_1] domain x := [ %1.4e , %1.4e ] \n", jmin[0], jmax[0] );
		printf("a T_1] domain y := [ %1.4e , %1.4e ] \n", jmin[1], jmax[1] );

		
		ierr = DMDAGetBoundingBox(T_surface_0,jmin,jmax);CHKERRQ(ierr);
		printf("b T_0] domain x := [ %1.4e , %1.4e ] \n", jmin[0], jmax[0] );
		printf("b T_0] domain y := [ %1.4e , %1.4e ] \n", jmin[1], jmax[1] );
	}
#endif
	
//======================================	
	/* force box size */
	{
		PetscReal jmin_T0[3],jmax_T0[3]; 
		PetscReal jmin_V0[3],jmax_V0[3]; 
		
		/* fetch x size from the surface mesh */
		//ierr = DMDAGetBoundingBox(T_surface_1,jmin_T0,jmax_T0);CHKERRQ(ierr);
		ierr = DMDAGetSurfaceBoundingBox(dav,1,jmin_T0,jmax_T0);CHKERRQ(ierr);
		/* fetch any y size from the volume mesh */ 
		/// LLP : I dont understand that part of the code ??? I guess you just dont care aslong as x is set everywhere? 
		ierr = DMDAGetBoundingBox(dav,jmin_V0,jmax_V0);CHKERRQ(ierr);
		ierr = DMDASetUniformCoordinates(dav,jmin_T0[0],jmax_T0[0],jmin_V0[1],jmax_V0[1],PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
	}
	
	/* Push interpolated values into locally owned coordinate vector */
	ierr = DMDAGetCorners( T_surface_0, &si_0,&sj_0,0, &nx_0,&ny_0,0 );CHKERRQ(ierr);
	si_g = si_0;
	ei_g = si_0 + nx_0;
	//printf("[%d] si_g = %d : ei_g = %d \n", rank, si_g,ei_g);
	ierr = DMDAGetCoordinateDA(dav,&cda_dav);CHKERRQ(ierr);
	
	ierr = DMDAGetCoordinates(dav,&coordinates);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cda_dav,coordinates,&LA_coor);CHKERRQ(ierr);
	// set top surface coordinates
	ierr = DMDAVecGetArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
	if ( (sj+ny) == (NY) ) {
		for (i=si_g; i<ei_g; i++) {
#ifdef REMESH_DBG 
			printf("[%d] mapping LA_Tsurf0[%d] ==> LA_coor[%d] \n", rank, i, si + (i-si_g) ); 
#endif
			LA_coor[NY-1][si + (i-si_g)][0] = LA_T_surf_0_coor[0][i][0]; /* x */
			LA_coor[NY-1][si + (i-si_g)][1] = LA_T_surf_0_coor[0][i][1]; /* y - height */
		}
	}	
	ierr = DMDAVecRestoreArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
	// set bottom surface coordinates
	ierr = DMDAVecGetArrayDOF(cda_B_surf_0,B_surf_0_coordinates,&LA_B_surf_0_coor);CHKERRQ(ierr);
		if ( (sj) == (0) ) {
		for (i=si_g; i<ei_g; i++) {
#ifdef REMESH_DBG 
			printf("[%d] mapping LA_Bsurf0[%d] ==> LA_coor[%d] \n", rank, i, si + (i-si_g) ); 
#endif
			LA_coor[0][si + (i-si_g)][0] = LA_B_surf_0_coor[0][i][0]; /* x */
			LA_coor[0][si + (i-si_g)][1] = LA_B_surf_0_coor[0][i][1]; /* y - height */
		}
	}	
	ierr = DMDAVecRestoreArrayDOF(cda_B_surf_0,B_surf_0_coordinates,&LA_B_surf_0_coor);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArrayDOF(cda_dav,coordinates,&LA_coor);CHKERRQ(ierr);

	/* Scatter into ghosted vector */
	ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);

	
	/* remesh nodes using upper and lower surfaces */
	ierr = DMDARemeshSetUniformCoordinatesBetweenJLayers2d(dav,0,NY);CHKERRQ(ierr);
    /* Destroy local surfaces */	
	ierr = DMDestroy(&T_surface_1);CHKERRQ(ierr);
	ierr = DMDestroy(&T_surface_0);CHKERRQ(ierr);
	ierr = DMDestroy(&B_surface_1);CHKERRQ(ierr);
	ierr = DMDestroy(&B_surface_0);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "UpdateMeshGeometry_LatInflowLagSurfaceJMIN_JMAX_RemeshInteriorJMIN_JMAX"
PetscErrorCode UpdateMeshGeometry_LatInflowLagSurfaceJMIN_JMAX_RemeshInteriorJMIN_JMAX(DM dav,Vec velocity,PetscReal step)
{
	Vec coordinates,vel_advect_mesh;
	PetscInt i,j,nx,ny,NX,NY,si,sj;
	PetscScalar ***LA_velocity;
	PetscScalar ***LA_coor;
	PetscInt si_g,ei_g;
	DM cda_dav;
	PetscMPIInt rank;
	PetscReal jmin[3],jmax[3];
	PetscReal dx_0;
	
	DM T_surface_0, T_surface_1; 
	DM B_surface_0, B_surface_1;
	Vec T_surf_0_coordinates, T_surf_1_coordinates;
	Vec B_surf_0_coordinates, B_surf_1_coordinates;
	PetscScalar ***LA_T_surf_0_coor, ***LA_T_surf_1_coor;
	PetscScalar ***LA_B_surf_0_coor, ***LA_B_surf_1_coor;
	DM cda_T_surf_0,cda_T_surf_1;
	DM cda_B_surf_0,cda_B_surf_1;
	PetscInt start_i, end_i;
	PetscInt nx_0,ny_0,si_0,sj_0;
	
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	MPI_Comm_rank(((PetscObject)dav)->comm,&rank);

	/* Copy all surface nodes */
	ierr = DMDAGetCorners( dav, &si,&sj,0, &nx,&ny,0 );CHKERRQ(ierr);
	ierr = DMDAGetInfo(dav,0, &NX,&NY,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
#ifdef REMESH_DBG	
	PetscPrintf(PETSC_COMM_SELF,"tau_0: fetching [%D <= i < %D], [j = %D] \n", si,si+nx, NY-1 );
#endif	
	ierr = DMDACreate2dRedundant( dav, si,si+nx, NY-1,  NY, 1, &T_surface_0 );CHKERRQ(ierr);
	ierr = DMDACreate2dRedundant( dav, si,si+nx, 0   , 1  , 1, &B_surface_0 );CHKERRQ(ierr);
#ifdef REMESH_DBG
	{
		ierr = DMDAGetBoundingBox(T_surface_0,jmin,jmax);CHKERRQ(ierr);
		printf("T_0] domain x := [ %1.4e , %1.9e ] \n", jmin[0], jmax[0] );
		printf("T_0] domain y := [ %1.4e , %1.9e ] \n", jmin[1], jmax[1] );
	}
#endif	
	
	/* Zero all velocity dofs expect those on the surface and bottom */
	ierr = DMGetGlobalVector(dav,&vel_advect_mesh);CHKERRQ(ierr);
	ierr = VecCopy(velocity,vel_advect_mesh);CHKERRQ(ierr);
	
	ierr = DMDAGetCorners(dav,&si,&sj,0,&nx,&ny,0);CHKERRQ(ierr);
	ierr = DMDAGetInfo(dav,0, &NX,&NY,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(dav,vel_advect_mesh,&LA_velocity);CHKERRQ(ierr);
	for (j=sj; j<sj+ny; j++) {
		if ((j!=(NY-1)) & (j!=(0))) {
			for (i=si; i<si+nx; i++) {
				LA_velocity[j][i][0] = 0.0; /* vx */
				LA_velocity[j][i][1] = 0.0; /* vy */
			}
		}else{
		/*do not advect the corner of the box horizontally*/
			for (i=si; i<si+nx; i++) {
		    	if ((i==(NX-1)) | (i==(0))) {
				LA_velocity[j][i][0] = 0.0; /* vx */
				//LA_velocity[j][i][1] = 0.0; /* vy */
			    }
			}
		}
	}
	ierr = DMDAVecRestoreArrayDOF(dav,vel_advect_mesh,&LA_velocity);CHKERRQ(ierr);
	
	
	/* Advect all nodes */
	ierr = DMDAGetCoordinates(dav,&coordinates);CHKERRQ(ierr);
	ierr = VecAXPY(coordinates,step,vel_advect_mesh);CHKERRQ(ierr); /* x = x + dt.vel_advect_mesh */
	ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
	
	ierr = DMRestoreGlobalVector(dav,&vel_advect_mesh);CHKERRQ(ierr);

	/* Extract advected surface + overlap */
	ierr = DMDAGetCorners( dav, &si,&sj,0, &nx,&ny,0 );CHKERRQ(ierr);
	ierr = DMDAGetInfo(dav,0, &NX,&NY,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
	start_i = PetscMax(0,si-2);
	end_i   = PetscMin(NX,si+nx+2);
#ifdef REMESH_DBG	
	PetscPrintf(PETSC_COMM_SELF,"tau_1: fetching [%D <= i < %D], [j = %D] \n", start_i,end_i, NY-1 );
#endif
//Set Uniform spacing in x (same at top and bottom)
    ierr = DMDAGetSurfaceBoundingBox(dav,1,jmin,jmax);CHKERRQ(ierr);
	dx_0 = (jmax[0]-jmin[0])/(PetscReal)(NX-1);

	// create a copy of the coordinates of top surface
	ierr = DMDACreate2dRedundant( dav, start_i,end_i, NY-1,NY, 1, &T_surface_1 );CHKERRQ(ierr);
	
#ifdef REMESH_DBG	
	printf("dx_0 = %1.4e : jmin[0] = %1.4e : NX = %d \n", dx_0,jmin[0],NX );
#endif
	
	/* (init y top to be zero because we interpolate y on the newly set uniform spacing in x) */
	ierr = DMDAGetCoordinateDA(T_surface_0,&cda_T_surf_0);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(T_surface_0,&T_surf_0_coordinates);CHKERRQ(ierr);
	ierr = DMDAGetCorners( T_surface_0, &si_0,&sj_0,0, &nx_0,&ny_0,0 );CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
#ifdef REMESH_DBG	
	PetscPrintf(PETSC_COMM_SELF,"T_surface_0: si-ej [%D <= i < %D], [j = %D] \n", si_0,si_0+nx_0, sj_0 );
#endif

	for (j=sj_0; j<sj_0+ny_0; j++) { /* this loop is redundant as it's a surface object */
		for (i=si_0; i<si_0+nx_0; i++) {
			LA_T_surf_0_coor[j][i][0] = jmin[0] + si * dx_0 + i * dx_0;
#ifdef REMESH_DBG	
			printf("i_red=%d: i_g=%d: x = %1.4e \n", i, si+i, LA_T_surf_0_coor[j][i][0] );
#endif
		}
	}
    ierr = DMDAVecRestoreArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);

	/*  Interpolate from T_surface_1(y) => T_surface_0(y) */
	/* (init y to be zero) */
	ierr = DMDAGetCoordinateDA(T_surface_0,&cda_T_surf_0);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(T_surface_0,&T_surf_0_coordinates);CHKERRQ(ierr);
	ierr = DMDAGetCorners( T_surface_0, &si_0,&sj_0,0, &nx_0,&ny_0,0 );CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
	for (j=sj_0; j<sj_0+ny_0; j++) { /* this loop is redundant as it's a surface object */
		for (i=si_0; i<si_0+nx_0; i++) {
			LA_T_surf_0_coor[j][i][1] = 0.0; /* y - height */
		}
	}
	ierr = DMDAVecRestoreArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);

	
	ierr = DMDAGetCoordinateDA(T_surface_0,&cda_T_surf_0);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(T_surface_0,&T_surf_0_coordinates);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
	
	ierr = DMDAGetCoordinateDA(T_surface_1,&cda_T_surf_1);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(T_surface_1,&T_surf_1_coordinates);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cda_T_surf_1,T_surf_1_coordinates,&LA_T_surf_1_coor);CHKERRQ(ierr);
	
	/* do the interp */
	ierr = SurfaceInterpolation2D(T_surface_0,LA_T_surf_0_coor, T_surface_1,LA_T_surf_1_coor);CHKERRQ(ierr);

	ierr = DMDAVecRestoreArrayDOF(cda_T_surf_1,T_surf_1_coordinates,&LA_T_surf_1_coor);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);	

#ifdef REMESH_DBG
	{
		PetscReal jmin[3],jmax[3]; 
		
		ierr = DMDAGetBoundingBox(T_surface_1,jmin,jmax);CHKERRQ(ierr);
		printf("a T_1] domain x := [ %1.4e , %1.4e ] \n", jmin[0], jmax[0] );
		printf("a T_1] domain y := [ %1.4e , %1.4e ] \n", jmin[1], jmax[1] );

		
		ierr = DMDAGetBoundingBox(T_surface_0,jmin,jmax);CHKERRQ(ierr);
		printf("b T_0] domain x := [ %1.4e , %1.4e ] \n", jmin[0], jmax[0] );
		printf("b T_0] domain y := [ %1.4e , %1.4e ] \n", jmin[1], jmax[1] );
	}
#endif

//BOTTOM=================================================
    // create a copy of the coordinates of bottom surface
	ierr = DMDACreate2dRedundant( dav, start_i,end_i, 0   , 1, 1, &B_surface_1 );CHKERRQ(ierr);

	ierr = DMDAGetCoordinateDA(B_surface_0,&cda_B_surf_0);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(B_surface_0,&B_surf_0_coordinates);CHKERRQ(ierr);
	ierr = DMDAGetCorners( B_surface_0, &si_0,&sj_0,0, &nx_0,&ny_0,0 );CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cda_B_surf_0,B_surf_0_coordinates,&LA_B_surf_0_coor);CHKERRQ(ierr);
#ifdef REMESH_DBG	
	PetscPrintf(PETSC_COMM_SELF,"B_surface_0: si-ej [%D <= i < %D], [j = %D] \n", si_0,si_0+nx_0, sj_0 );
#endif
     // set uniform coordinate in x and  y coordinate to zero in the copy 
	for (j=sj_0; j<sj_0+ny_0; j++) { /* this loop is redundant as it's a surface object */
		for (i=si_0; i<si_0+nx_0; i++) {
			LA_B_surf_0_coor[j][i][0] = jmin[0] + si * dx_0 + i * dx_0;
			LA_B_surf_0_coor[j][i][1] = 0.0; /* y - height */
#ifdef REMESH_DBG	
			printf("i_red=%d: i_g=%d: x = %1.4e \n", i, si+i, LA_B_surf_0_coor[j][i][0] );
#endif
		}
	}
	ierr = DMDAVecRestoreArrayDOF(cda_B_surf_0,B_surf_0_coordinates,&LA_B_surf_0_coor);CHKERRQ(ierr);
#ifdef REMESH_DBG	
	{
		PetscReal jmin[3],jmax[3]; 
		
		DMDAGetBoundingBox(B_surface_1,jmin,jmax);
		printf("T_1] domain x := [ %1.4e , %1.9e ] \n", jmin[0], jmax[0] );
		printf("T_1] domain y := [ %1.4e , %1.9e ] \n", jmin[1], jmax[1] );
	}
#endif

	ierr = DMDAGetCoordinateDA(B_surface_0,&cda_B_surf_0);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(B_surface_0,&B_surf_0_coordinates);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cda_B_surf_0,B_surf_0_coordinates,&LA_B_surf_0_coor);CHKERRQ(ierr);
	
	ierr = DMDAGetCoordinateDA(B_surface_1,&cda_B_surf_1);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(B_surface_1,&B_surf_1_coordinates);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cda_B_surf_1,B_surf_1_coordinates,&LA_B_surf_1_coor);CHKERRQ(ierr);
	
	/* do the interp */
	ierr = SurfaceInterpolation2D(B_surface_0,LA_B_surf_0_coor, B_surface_1,LA_B_surf_1_coor);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArrayDOF(cda_B_surf_1,B_surf_1_coordinates,&LA_B_surf_1_coor);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArrayDOF(cda_B_surf_0,B_surf_0_coordinates,&LA_B_surf_0_coor);CHKERRQ(ierr);	

#ifdef REMESH_DBG
	{
		PetscReal jmin[3],jmax[3]; 
		
		ierr = DMDAGetBoundingBox(T_surface_1,jmin,jmax);CHKERRQ(ierr);
		printf("a T_1] domain x := [ %1.4e , %1.4e ] \n", jmin[0], jmax[0] );
		printf("a T_1] domain y := [ %1.4e , %1.4e ] \n", jmin[1], jmax[1] );

		
		ierr = DMDAGetBoundingBox(T_surface_0,jmin,jmax);CHKERRQ(ierr);
		printf("b T_0] domain x := [ %1.4e , %1.4e ] \n", jmin[0], jmax[0] );
		printf("b T_0] domain y := [ %1.4e , %1.4e ] \n", jmin[1], jmax[1] );
	}
#endif
	
//======================================	
	/* force box size */
	{
		PetscReal jmin_T0[3],jmax_T0[3]; 
		PetscReal jmin_V0[3],jmax_V0[3]; 
		
		/* fetch x size from the surface mesh */
		//ierr = DMDAGetBoundingBox(T_surface_1,jmin_T0,jmax_T0);CHKERRQ(ierr);
		ierr = DMDAGetSurfaceBoundingBox(dav,1,jmin_T0,jmax_T0);CHKERRQ(ierr);
		/* fetch any y size from the volume mesh */ 
		/// LLP : I dont understand that part of the code ??? I guess you just dont care aslong as x is set everywhere? 
		ierr = DMDAGetBoundingBox(dav,jmin_V0,jmax_V0);CHKERRQ(ierr);
		ierr = DMDASetUniformCoordinates(dav,jmin_T0[0],jmax_T0[0],jmin_V0[1],jmax_V0[1],PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
	}
	
	/* Push interpolated values into locally owned coordinate vector */
	ierr = DMDAGetCorners( T_surface_0, &si_0,&sj_0,0, &nx_0,&ny_0,0 );CHKERRQ(ierr);
	si_g = si_0;
	ei_g = si_0 + nx_0;
	//printf("[%d] si_g = %d : ei_g = %d \n", rank, si_g,ei_g);
	ierr = DMDAGetCoordinateDA(dav,&cda_dav);CHKERRQ(ierr);
	
	ierr = DMDAGetCoordinates(dav,&coordinates);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cda_dav,coordinates,&LA_coor);CHKERRQ(ierr);
	// set top surface coordinates
	ierr = DMDAVecGetArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
	if ( (sj+ny) == (NY) ) {
		for (i=si_g; i<ei_g; i++) {
#ifdef REMESH_DBG 
			printf("[%d] mapping LA_Tsurf0[%d] ==> LA_coor[%d] \n", rank, i, si + (i-si_g) ); 
#endif
			LA_coor[NY-1][si + (i-si_g)][0] = LA_T_surf_0_coor[0][i][0]; /* x */
			LA_coor[NY-1][si + (i-si_g)][1] = LA_T_surf_0_coor[0][i][1]; /* y - height */
		}
	}	
	ierr = DMDAVecRestoreArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
	// set bottom surface coordinates
	ierr = DMDAVecGetArrayDOF(cda_B_surf_0,B_surf_0_coordinates,&LA_B_surf_0_coor);CHKERRQ(ierr);
		if ( (sj) == (0) ) {
		for (i=si_g; i<ei_g; i++) {
#ifdef REMESH_DBG 
			printf("[%d] mapping LA_Bsurf0[%d] ==> LA_coor[%d] \n", rank, i, si + (i-si_g) ); 
#endif
			LA_coor[0][si + (i-si_g)][0] = LA_B_surf_0_coor[0][i][0]; /* x */
			LA_coor[0][si + (i-si_g)][1] = LA_B_surf_0_coor[0][i][1]; /* y - height */
		}
	}	
	ierr = DMDAVecRestoreArrayDOF(cda_B_surf_0,B_surf_0_coordinates,&LA_B_surf_0_coor);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArrayDOF(cda_dav,coordinates,&LA_coor);CHKERRQ(ierr);

	/* Scatter into ghosted vector */
	ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);

	
	/* remesh nodes using upper and lower surfaces */
	ierr = DMDARemeshSetUniformCoordinatesBetweenJLayers2d(dav,0,NY);CHKERRQ(ierr);
    /* Destroy local surfaces */	
	ierr = DMDestroy(&T_surface_1);CHKERRQ(ierr);
	ierr = DMDestroy(&T_surface_0);CHKERRQ(ierr);
	ierr = DMDestroy(&B_surface_1);CHKERRQ(ierr);
	ierr = DMDestroy(&B_surface_0);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}



#undef __FUNCT__
#define __FUNCT__ "UpdateMeshGeometry_LatInflowLagSurface_JMIN_JMAX_RemeshInteriorRefineJMIN_JMAX"
PetscErrorCode     UpdateMeshGeometry_LatInflowLagSurfaceJMIN_JMAX_RemeshInteriorRefineJMIN_JMAX(DM dav,Vec velocity,PetscReal dt)
{
    Vec coordinates,vel_advect_mesh;
    PetscInt i,j,nx,ny,NX,NY,si,sj;
    PetscScalar ***LA_velocity;
    PetscScalar ***LA_coor;
    PetscInt si_g,ei_g;
    DM cda_dav;
    PetscMPIInt rank;
    PetscReal jmin[3],jmax[3];
    PetscReal dx_0;
    
    DM T_surface_0, T_surface_1;
    DM B_surface_0, B_surface_1;
    Vec T_surf_0_coordinates, T_surf_1_coordinates;
    Vec B_surf_0_coordinates, B_surf_1_coordinates;
    PetscScalar ***LA_T_surf_0_coor, ***LA_T_surf_1_coor;
    PetscScalar ***LA_B_surf_0_coor, ***LA_B_surf_1_coor;
    DM cda_T_surf_0,cda_T_surf_1;
    DM cda_B_surf_0,cda_B_surf_1;
    PetscInt start_i, end_i;
    PetscInt nx_0,ny_0,si_0,sj_0;
    PetscInt Ny[21],ndomainy,jend,jstart,jdom;
    
    PetscErrorCode ierr;
    
    PetscFunctionBegin;
    
    MPI_Comm_rank(((PetscObject)dav)->comm,&rank);
    
    
    
    ierr = DMGetGlobalVector(dav,&vel_advect_mesh);CHKERRQ(ierr);
    ierr = VecCopy(velocity,vel_advect_mesh);CHKERRQ(ierr);
    
    ierr = DMDAGetCorners(dav,&si,&sj,0,&nx,&ny,0);CHKERRQ(ierr);
    ierr = DMDAGetInfo(dav,0, &NX,&NY,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
    ierr = DMDAVecGetArrayDOF(dav,vel_advect_mesh,&LA_velocity);CHKERRQ(ierr);
    for (j=sj; j<sj+ny; j++) {
        if ((j!=(NY-1)) & (j!=(0))) {
            for (i=si; i<si+nx; i++) {
                LA_velocity[j][i][0] = 0.0; /* vx */
                LA_velocity[j][i][1] = 0.0; /* vy */
            }
        }else{
            /*do not advect the corner of the box horizontally*/
            for (i=si; i<si+nx; i++) {
                if ((i==(NX-1)) | (i==(0))) {
                //if (i==si | (i==(si+nx-1))) {
                    LA_velocity[j][i][0] = 0.0; /* vx */
                    //LA_velocity[j][i][1] = 0.0; /* vy */
                }
            }
        }
    }
    ierr = DMDAVecRestoreArrayDOF(dav,vel_advect_mesh,&LA_velocity);CHKERRQ(ierr);
    
    /* First do langrangian update with modified velocity */
    ierr = DMDAGetCoordinates(dav,&coordinates);CHKERRQ(ierr);
    ierr = VecAXPY(coordinates,dt,vel_advect_mesh);CHKERRQ(ierr); /* x = x + dt.vel_advect_mesh */
    ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(dav,&vel_advect_mesh);CHKERRQ(ierr);

    
    
    /* Extract advected surface + overlap */
    // create redundant copies of the coordinates of bottom and top surface
    ierr = DMDAGetCorners( dav, &si,&sj,0, &nx,&ny,0 );CHKERRQ(ierr);
    ierr = DMDAGetInfo(dav,0, &NX,&NY,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
    start_i = PetscMax(0,si-10);
    end_i   = PetscMin(NX,si+nx+10);
    /*STORE COPY OF DEFORMED UPPER BOTTOM SURFACE called 1 to be consistant with explanation of SurfaceInterpolation2D  function in this file*/
    ierr = DMDACreate2dRedundant( dav, start_i ,end_i , NY-1,  NY, 1, &T_surface_1 );CHKERRQ(ierr);
    ierr = DMDAGetCoordinateDA(T_surface_1,&cda_T_surf_1);CHKERRQ(ierr);
    ierr = DMDAGetCoordinates(T_surface_1,&T_surf_1_coordinates);CHKERRQ(ierr);
    ierr = DMDAVecGetArrayDOF(cda_T_surf_1,T_surf_1_coordinates,&LA_T_surf_1_coor);CHKERRQ(ierr);
#ifdef REMESH_DBG
    ierr = DMDAGetBoundingBox(T_surface_1,jmin,jmax);CHKERRQ(ierr);
    printf("T_1] domain x := [ %1.4e , %1.9e ] \n", jmin[0], jmax[0] );
    printf("T_1] domain y := [ %1.4e , %1.9e ] \n", jmin[1], jmax[1] );
#endif
    
    ierr = DMDACreate2dRedundant( dav, start_i ,end_i , 0   , 1  , 1, &B_surface_1 );CHKERRQ(ierr);
    ierr = DMDAGetCoordinateDA(B_surface_1,&cda_B_surf_1);CHKERRQ(ierr);
    ierr = DMDAGetCoordinates(B_surface_1,&B_surf_1_coordinates);CHKERRQ(ierr);
    ierr = DMDAVecGetArrayDOF(cda_B_surf_1,B_surf_1_coordinates,&LA_B_surf_1_coor);CHKERRQ(ierr);
#ifdef REMESH_DBG
    ierr = DMDAGetBoundingBox(B_surface_1,jmin,jmax);CHKERRQ(ierr);
    printf("B_1] domain x := [ %1.4e , %1.9e ] \n", jmin[0], jmax[0] );
    printf("B_1] domain y := [ %1.4e , %1.9e ] \n", jmin[1], jmax[1] );
#endif
    
    /* Create a refined mesh with same boundary in x as the deformed mesh but y is like the original mesh */
    ierr = DMDASetRefinedCoordinates(dav); CHKERRQ(ierr);
    
    /* create redundant copies of the coordinates of bottom and top surface in the new mesh called 0 to be consistant with explanation of SurfaceInterpolation2D in this file*/
    ierr = DMDACreate2dRedundant( dav, si,si+nx, NY-1,NY, 1, &T_surface_0 );CHKERRQ(ierr);
    ierr = DMDAGetCoordinateDA(T_surface_0,&cda_T_surf_0);CHKERRQ(ierr);
    ierr = DMDAGetCoordinates(T_surface_0,&T_surf_0_coordinates);CHKERRQ(ierr);
    ierr = DMDAVecGetArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
#ifdef REMESH_DBG
    ierr = DMDAGetBoundingBox(T_surface_0,jmin,jmax);CHKERRQ(ierr);
    printf("T_0] domain x := [ %1.4e , %1.9e ] \n", jmin[0], jmax[0] );
    printf("T_0] domain y := [ %1.4e , %1.9e ] \n", jmin[1], jmax[1] );
#endif
    ierr = DMDACreate2dRedundant( dav, si,si+nx, 0   , 1, 1, &B_surface_0 );CHKERRQ(ierr);
    ierr = DMDAGetCoordinateDA(B_surface_0,&cda_B_surf_0);CHKERRQ(ierr);
    ierr = DMDAGetCoordinates(B_surface_0,&B_surf_0_coordinates);CHKERRQ(ierr);
    ierr = DMDAVecGetArrayDOF(cda_B_surf_0,B_surf_0_coordinates,&LA_B_surf_0_coor);CHKERRQ(ierr);
#ifdef REMESH_DBG
    ierr = DMDAGetBoundingBox(B_surface_0,jmin,jmax);CHKERRQ(ierr);
    printf("B_0] domain x := [ %1.4e , %1.9e ] \n", jmin[0], jmax[0] );
    printf("B_0] domain y := [ %1.4e , %1.9e ] \n", jmin[1], jmax[1] );
#endif
    
    /* do the interp for the surface*/
     ierr = SurfaceInterpolation2D(T_surface_0,LA_T_surf_0_coor, T_surface_1,LA_T_surf_1_coor);CHKERRQ(ierr);
    //ierr = SurfaceInterpolation2D(T_surface_1,LA_T_surf_1_coor, T_surface_0,LA_T_surf_0_coor);CHKERRQ(ierr);
    /* restore and destroy the copy that is not used */
    ierr = DMDAVecRestoreArrayDOF(cda_T_surf_1,T_surf_1_coordinates,&LA_T_surf_1_coor);CHKERRQ(ierr);
    ierr = DMDestroy(&T_surface_1);CHKERRQ(ierr);
    
    /* do the interp for the bottom */
    ierr = SurfaceInterpolation2D(B_surface_0,LA_B_surf_0_coor, B_surface_1,LA_B_surf_1_coor);CHKERRQ(ierr);
    //ierr = SurfaceInterpolation2D(B_surface_1,LA_B_surf_1_coor, B_surface_0,LA_B_surf_0_coor);CHKERRQ(ierr);
    /* restore and destroy the copy that is not used anymore */
    ierr = DMDAVecRestoreArrayDOF(cda_B_surf_1,B_surf_1_coordinates,&LA_B_surf_1_coor);CHKERRQ(ierr);
    ierr = DMDestroy(&B_surface_1);CHKERRQ(ierr);
    
    
#ifdef REMESH_DBG
    /* now 0 should have a non zero topography */
    {
        PetscReal jmin[3],jmax[3];
        
        ierr = DMDAGetBoundingBox(T_surface_0,jmin,jmax);CHKERRQ(ierr);
        printf("a T_0] domain x := [ %1.4e , %1.4e ] \n", jmin[0], jmax[0] );
        printf("a T_0] domain y := [ %1.4e , %1.4e ] \n", jmin[1], jmax[1] );
        
        
        ierr = DMDAGetBoundingBox(B_surface_0,jmin,jmax);CHKERRQ(ierr);
        printf("b B_0] domain x := [ %1.4e , %1.4e ] \n", jmin[0], jmax[0] );
        printf("b B_0] domain y := [ %1.4e , %1.4e ] \n", jmin[1], jmax[1] );
    }
#endif
    
    
    /*get the corner of the local copy of the surface */
    ierr = DMDAGetCorners( T_surface_0, &si_0,&sj_0,0, &nx_0,&ny_0,0 );CHKERRQ(ierr);
    si_g = si_0;
    ei_g = si_0 + nx_0;
    /* access local coordinate array of the new mesh*/
    ierr = DMDAGetCoordinateDA(dav,&cda_dav);CHKERRQ(ierr);
    ierr = DMDAGetCoordinates(dav,&coordinates);CHKERRQ(ierr);
    ierr = DMDAVecGetArrayDOF(cda_dav,coordinates,&LA_coor);CHKERRQ(ierr);
    /*push the interpolated value in the new mesh */
    if ( (sj+ny) == (NY) ) {
        for (i=si_g; i<ei_g; i++) {
            // LA_coor[NY-1][si + (i-si_g)][0] = LA_T_surf_0_coor[0][i][0]; /* x */
            LA_coor[NY-1][si + (i-si_g)][1] = LA_T_surf_0_coor[0][i][1]; /* y - height */
        }
    }
    /* restore and destroy the redundant copy of the surface */
    ierr = DMDAVecRestoreArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
    ierr = DMDestroy(&T_surface_0);CHKERRQ(ierr);
    
    
    if ( (sj) == (0) ) {
        for (i=si_g; i<ei_g; i++) {
            // LA_coor[0][si + (i-si_g)][0] = LA_B_surf_0_coor[0][i][0]; /* x */
            LA_coor[0][si + (i-si_g)][1] = LA_B_surf_0_coor[0][i][1]; /* y - height */
        }
    }
    /* restore and destroy the redundant copy of the bottom */
    ierr = DMDAVecRestoreArrayDOF(cda_B_surf_0,B_surf_0_coordinates,&LA_B_surf_0_coor);CHKERRQ(ierr);
    ierr = DMDestroy(&B_surface_0);CHKERRQ(ierr);
    
    /* restore the local copy of global coord array */
    ierr = DMDAVecRestoreArrayDOF(cda_dav,coordinates,&LA_coor);CHKERRQ(ierr);
    /* Scatter into ghosted vector */
    ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
    
    
    /* remesh nodes using upper and lower surfaces as well as the original depth for vertical refinement !! */
    ndomainy = 1;
    PetscOptionsGetInt(PETSC_NULL,"-refine_n_domain_y",&ndomainy,0);
    Ny[0] = NY;
    PetscOptionsGetIntArray(PETSC_NULL,"-refine_y_dom_el",Ny,&ndomainy,PETSC_NULL);
    jstart = 0;
    for (jdom=0; jdom<ndomainy; jdom++) {
        jend = jstart+Ny[jdom]*2+1;
        ierr = DMDARemeshSetUniformCoordinatesBetweenJLayers2d(dav,jstart,jend);CHKERRQ(ierr);
        jstart= jend-1;
    }
    
    PetscFunctionReturn(0);
}





#undef __FUNCT__
#define __FUNCT__ "UpdateMeshGeometry_LagSurfaceJMIN_JMAX_RemeshInteriorRefineJMIN_JMAX"
PetscErrorCode UpdateMeshGeometry_LagSurfaceJMIN_JMAX_RemeshInteriorRefineJMIN_JMAX(DM dav,Vec velocity,PetscReal dt)
{
    Vec coordinates,vel_advect_mesh;
    PetscInt i,j,nx,ny,NX,NY,si,sj;
    PetscScalar ***LA_velocity;
    PetscScalar ***LA_coor;
    PetscInt si_g,ei_g;
    DM cda_dav;
    PetscMPIInt rank;
    PetscReal jmin[3],jmax[3];
    PetscReal dx_0;
    
    DM T_surface_0, T_surface_1;
    DM B_surface_0, B_surface_1;
    Vec T_surf_0_coordinates, T_surf_1_coordinates;
    Vec B_surf_0_coordinates, B_surf_1_coordinates;
    PetscScalar ***LA_T_surf_0_coor, ***LA_T_surf_1_coor;
    PetscScalar ***LA_B_surf_0_coor, ***LA_B_surf_1_coor;
    DM cda_T_surf_0,cda_T_surf_1;
    DM cda_B_surf_0,cda_B_surf_1;
    PetscInt start_i, end_i;
    PetscInt nx_0,ny_0,si_0,sj_0;
    PetscInt Ny[21],ndomainy,jend,jstart,jdom;
    
    PetscErrorCode ierr;
    
    PetscFunctionBegin;
    
    MPI_Comm_rank(((PetscObject)dav)->comm,&rank);
    
    
    
    
    
    /* First do a fully langrangian update */
    ierr = DMDAGetCoordinates(dav,&coordinates);CHKERRQ(ierr);
    ierr = VecAXPY(coordinates,dt,velocity);CHKERRQ(ierr); /* x = x + dt.vel_advect_mesh */
    ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
    /* Extract advected surface + overlap */
    // create redundant copies of the coordinates of bottom and top surface
    ierr = DMDAGetCorners( dav, &si,&sj,0, &nx,&ny,0 );CHKERRQ(ierr);
    ierr = DMDAGetInfo(dav,0, &NX,&NY,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
    start_i = PetscMax(0,si-10);
    end_i   = PetscMin(NX,si+nx+10);
    /*STORE COPY OF DEFORMED UPPER BOTTOM SURFACE called 1 to be consistant with explanation of SurfaceInterpolation2D  function in this file*/
    ierr = DMDACreate2dRedundant( dav, start_i,end_i, NY-1,  NY, 1, &T_surface_1 );CHKERRQ(ierr);
    ierr = DMDAGetCoordinateDA(T_surface_1,&cda_T_surf_1);CHKERRQ(ierr);
    ierr = DMDAGetCoordinates(T_surface_1,&T_surf_1_coordinates);CHKERRQ(ierr);
    ierr = DMDAVecGetArrayDOF(cda_T_surf_1,T_surf_1_coordinates,&LA_T_surf_1_coor);CHKERRQ(ierr);
    
    
#ifdef REMESH_DBG
    ierr = DMDAGetBoundingBox(T_surface_1,jmin,jmax);CHKERRQ(ierr);
    printf("T_1] domain x := [ %1.4e , %1.9e ] \n", jmin[0], jmax[0] );
    printf("T_1] domain y := [ %1.4e , %1.9e ] \n", jmin[1], jmax[1] );
#endif
    
    ierr = DMDACreate2dRedundant( dav, start_i,end_i, 0   , 1  , 1, &B_surface_1 );CHKERRQ(ierr);
    ierr = DMDAGetCoordinateDA(B_surface_1,&cda_B_surf_1);CHKERRQ(ierr);
    ierr = DMDAGetCoordinates(B_surface_1,&B_surf_1_coordinates);CHKERRQ(ierr);
    ierr = DMDAVecGetArrayDOF(cda_B_surf_1,B_surf_1_coordinates,&LA_B_surf_1_coor);CHKERRQ(ierr);
    
    
#ifdef REMESH_DBG
    ierr = DMDAGetBoundingBox(B_surface_1,jmin,jmax);CHKERRQ(ierr);
    printf("B_1] domain x := [ %1.4e , %1.9e ] \n", jmin[0], jmax[0] );
    printf("B_1] domain y := [ %1.4e , %1.9e ] \n", jmin[1], jmax[1] );
#endif
    
    /* Create a refined mesh with same boundary in x as the deformed mesh but y is like the original mesh */
    ierr = DMDASetRefinedCoordinates(dav); CHKERRQ(ierr);
    
    

    /* create redundant copies of the coordinates of bottom and top surface in the new mesh called 1 to be consistant with explanation of SurfaceInterpolation2D in this file*/
    ierr = DMDACreate2dRedundant( dav, si,si+nx, NY-1,NY, 1, &T_surface_0 );CHKERRQ(ierr);
    ierr = DMDAGetCoordinateDA(T_surface_0,&cda_T_surf_0);CHKERRQ(ierr);
    ierr = DMDAGetCoordinates(T_surface_0,&T_surf_0_coordinates);CHKERRQ(ierr);
    ierr = DMDAVecGetArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);

#ifdef REMESH_DBG
    ierr = DMDAGetBoundingBox(T_surface_0,jmin,jmax);CHKERRQ(ierr);
    printf("T_0] domain x := [ %1.4e , %1.9e ] \n", jmin[0], jmax[0] );
    printf("T_0] domain y := [ %1.4e , %1.9e ] \n", jmin[1], jmax[1] );
#endif
    ierr = DMDACreate2dRedundant( dav, si,si+nx, 0   , 1, 1, &B_surface_0 );CHKERRQ(ierr);
    ierr = DMDAGetCoordinateDA(B_surface_0,&cda_B_surf_0);CHKERRQ(ierr);
    ierr = DMDAGetCoordinates(B_surface_0,&B_surf_0_coordinates);CHKERRQ(ierr);
    ierr = DMDAVecGetArrayDOF(cda_B_surf_0,B_surf_0_coordinates,&LA_B_surf_0_coor);CHKERRQ(ierr);

#ifdef REMESH_DBG
    ierr = DMDAGetBoundingBox(B_surface_0,jmin,jmax);CHKERRQ(ierr);
    printf("B_0] domain x := [ %1.4e , %1.9e ] \n", jmin[0], jmax[0] );
    printf("B_0] domain y := [ %1.4e , %1.9e ] \n", jmin[1], jmax[1] );
#endif
    
    /* do the interp for the surface*/
    ierr = SurfaceInterpolation2D(T_surface_0,LA_T_surf_0_coor, T_surface_1,LA_T_surf_1_coor);CHKERRQ(ierr);
    /* restore and destroy the copy that is not used */
    ierr = DMDAVecRestoreArrayDOF(cda_T_surf_1,T_surf_1_coordinates,&LA_T_surf_1_coor);CHKERRQ(ierr);
    ierr = DMDestroy(&T_surface_1);CHKERRQ(ierr);
    
    /* do the interp for the bottom */
    ierr = SurfaceInterpolation2D(B_surface_0,LA_B_surf_0_coor, B_surface_1,LA_B_surf_1_coor);CHKERRQ(ierr);
    /* restore and destroy the copy that is not used anymore */
    ierr = DMDAVecRestoreArrayDOF(cda_B_surf_1,B_surf_1_coordinates,&LA_B_surf_1_coor);CHKERRQ(ierr);
    ierr = DMDestroy(&B_surface_1);CHKERRQ(ierr);
    
    
#ifdef REMESH_DBG
    /* now 0 should have a non zero topography */
    {
        PetscReal jmin[3],jmax[3];
        
        ierr = DMDAGetBoundingBox(T_surface_0,jmin,jmax);CHKERRQ(ierr);
        printf("a T_0] domain x := [ %1.4e , %1.4e ] \n", jmin[0], jmax[0] );
        printf("a T_0] domain y := [ %1.4e , %1.4e ] \n", jmin[1], jmax[1] );
        
        
        ierr = DMDAGetBoundingBox(B_surface_0,jmin,jmax);CHKERRQ(ierr);
        printf("b B_0] domain x := [ %1.4e , %1.4e ] \n", jmin[0], jmax[0] );
        printf("b B_0] domain y := [ %1.4e , %1.4e ] \n", jmin[1], jmax[1] );
    }
#endif
    
    
    /*get the corner of the local copy of the surface */
    ierr = DMDAGetCorners( T_surface_0, &si_0,&sj_0,0, &nx_0,&ny_0,0 );CHKERRQ(ierr);
    si_g = si_0;
    ei_g = si_0 + nx_0;
    /* access local coordinate array of the new mesh*/
    ierr = DMDAGetCoordinateDA(dav,&cda_dav);CHKERRQ(ierr);
    ierr = DMDAGetCoordinates(dav,&coordinates);CHKERRQ(ierr);
    ierr = DMDAVecGetArrayDOF(cda_dav,coordinates,&LA_coor);CHKERRQ(ierr);
    /*push the interpolated value in the new mesh */
    if ( (sj+ny) == (NY) ) {
        for (i=si_g; i<ei_g; i++) {
            // LA_coor[NY-1][si + (i-si_g)][0] = LA_T_surf_0_coor[0][i][0]; /* x */
            LA_coor[NY-1][si + (i-si_g)][1] = LA_T_surf_0_coor[0][i][1]; /* y - height */
        }
    }
    /* restore and destroy the redundant copy of the surface */
    ierr = DMDAVecRestoreArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
    ierr = DMDestroy(&T_surface_0);CHKERRQ(ierr);
    
    
    if ( (sj) == (0) ) {
        for (i=si_g; i<ei_g; i++) {
            // LA_coor[0][si + (i-si_g)][0] = LA_B_surf_0_coor[0][i][0]; /* x */
            LA_coor[0][si + (i-si_g)][1] = LA_B_surf_0_coor[0][i][1]; /* y - height */
        }
    }
    /* restore and destroy the redundant copy of the bottom */
    ierr = DMDAVecRestoreArrayDOF(cda_B_surf_0,B_surf_0_coordinates,&LA_B_surf_0_coor);CHKERRQ(ierr);
    ierr = DMDestroy(&B_surface_0);CHKERRQ(ierr);
    
    /* restore the local copy of global coord array */
    ierr = DMDAVecRestoreArrayDOF(cda_dav,coordinates,&LA_coor);CHKERRQ(ierr);
    /* Scatter into ghosted vector */
    ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
    
    

    
    PetscFunctionReturn(0);
}



#undef __FUNCT__
#define __FUNCT__ "UpdateMeshGeometry_FullLagSurfaceJMAX_RemeshInteriorRefineJMIN_JMAX"
PetscErrorCode UpdateMeshGeometry_FullLagSurfaceJMAX_RemeshInteriorRefineJMIN_JMAX(DM dav,Vec velocity,PetscReal step)
{
    Vec coordinates,vel_advect_mesh;
    PetscInt i,j,nx,ny,NX,NY,si,sj;
    PetscScalar ***LA_velocity;
    PetscScalar ***LA_coor;
    PetscInt si_g,ei_g;
    DM cda_dav;
    PetscMPIInt rank;
    PetscReal jmin[3],jmax[3];
    PetscReal dx_0;
    
    DM T_surface_0, T_surface_1;
    DM B_surface_0, B_surface_1;
    Vec T_surf_0_coordinates, T_surf_1_coordinates;
    Vec B_surf_0_coordinates, B_surf_1_coordinates;
    PetscScalar ***LA_T_surf_0_coor, ***LA_T_surf_1_coor;
    PetscScalar ***LA_B_surf_0_coor, ***LA_B_surf_1_coor;
    DM cda_T_surf_0,cda_T_surf_1;
    DM cda_B_surf_0,cda_B_surf_1;
    PetscInt start_i, end_i;
    PetscInt nx_0,ny_0,si_0,sj_0;
    PetscInt Ny[21],ndomainy,jend,jstart,jdom;
    
    PetscErrorCode ierr;
    
    PetscFunctionBegin;
    
    MPI_Comm_rank(((PetscObject)dav)->comm,&rank);
    
    
    
    /* Zero vertical velocity dofs at the bottom */
    ierr = DMGetGlobalVector(dav,&vel_advect_mesh);CHKERRQ(ierr);
    ierr = VecCopy(velocity,vel_advect_mesh);CHKERRQ(ierr);
    
    ierr = DMDAGetCorners(dav,&si,&sj,0,&nx,&ny,0);CHKERRQ(ierr);
    ierr = DMDAGetInfo(dav,0, &NX,&NY,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
    ierr = DMDAVecGetArrayDOF(dav,vel_advect_mesh,&LA_velocity);CHKERRQ(ierr);
    for (j=sj; j<sj+ny; j++) {
        if (j==0) {
            for (i=si; i<si+nx; i++) {
                //LA_velocity[j][i][0] = 0.0; /* vx */
                LA_velocity[j][i][1] = 0.0; /* vy */
            }
        }
    }
    ierr = DMDAVecRestoreArrayDOF(dav,vel_advect_mesh,&LA_velocity);CHKERRQ(ierr);
    

    
    /* First do a fully langrangian update */
    ierr = DMDAGetCoordinates(dav,&coordinates);CHKERRQ(ierr);
    ierr = VecAXPY(coordinates,step,vel_advect_mesh);CHKERRQ(ierr); /* x = x + dt.vel_advect_mesh */
    ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(dav,&vel_advect_mesh);CHKERRQ(ierr);
    
    /* Extract advected surface + overlap */
    // create redundant copies of the coordinates of bottom and top surface
    ierr = DMDAGetCorners( dav, &si,&sj,0, &nx,&ny,0 );CHKERRQ(ierr);
    ierr = DMDAGetInfo(dav,0, &NX,&NY,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
    start_i = PetscMax(0,si-10);
    end_i   = PetscMin(NX,si+nx+10);
    /*STORE COPY OF DEFORMED UPPER BOTTOM SURFACE called 1 to be consistant with explanation of SurfaceInterpolation2D  function in this file*/
    ierr = DMDACreate2dRedundant( dav, start_i,end_i, NY-1,  NY, 1, &T_surface_1 );CHKERRQ(ierr);
    ierr = DMDAGetCoordinateDA(T_surface_1,&cda_T_surf_1);CHKERRQ(ierr);
    ierr = DMDAGetCoordinates(T_surface_1,&T_surf_1_coordinates);CHKERRQ(ierr);
    ierr = DMDAVecGetArrayDOF(cda_T_surf_1,T_surf_1_coordinates,&LA_T_surf_1_coor);CHKERRQ(ierr);
#ifdef REMESH_DBG
    ierr = DMDAGetBoundingBox(T_surface_1,jmin,jmax);CHKERRQ(ierr);
    printf("T_1] domain x := [ %1.4e , %1.9e ] \n", jmin[0], jmax[0] );
    printf("T_1] domain y := [ %1.4e , %1.9e ] \n", jmin[1], jmax[1] );
#endif
    
    ierr = DMDACreate2dRedundant( dav, start_i,end_i, 0   , 1  , 1, &B_surface_1 );CHKERRQ(ierr);
    ierr = DMDAGetCoordinateDA(B_surface_1,&cda_B_surf_1);CHKERRQ(ierr);
    ierr = DMDAGetCoordinates(B_surface_1,&B_surf_1_coordinates);CHKERRQ(ierr);
    ierr = DMDAVecGetArrayDOF(cda_B_surf_1,B_surf_1_coordinates,&LA_B_surf_1_coor);CHKERRQ(ierr);
#ifdef REMESH_DBG
    ierr = DMDAGetBoundingBox(B_surface_1,jmin,jmax);CHKERRQ(ierr);
    printf("B_1] domain x := [ %1.4e , %1.9e ] \n", jmin[0], jmax[0] );
    printf("B_1] domain y := [ %1.4e , %1.9e ] \n", jmin[1], jmax[1] );
#endif
    
    /* Create a refined mesh with same boundary in x as the deformed mesh but y is like the original mesh */
    ierr = DMDASetRefinedCoordinates(dav); CHKERRQ(ierr);
    
    /* create redundant copies of the coordinates of bottom and top surface in the new mesh called 1 to be consistant with explanation of SurfaceInterpolation2D in this file*/
    ierr = DMDACreate2dRedundant( dav, si,si+nx, NY-1,NY, 1, &T_surface_0 );CHKERRQ(ierr);
    ierr = DMDAGetCoordinateDA(T_surface_0,&cda_T_surf_0);CHKERRQ(ierr);
    ierr = DMDAGetCoordinates(T_surface_0,&T_surf_0_coordinates);CHKERRQ(ierr);
    ierr = DMDAVecGetArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
#ifdef REMESH_DBG
    ierr = DMDAGetBoundingBox(T_surface_0,jmin,jmax);CHKERRQ(ierr);
    printf("T_0] domain x := [ %1.4e , %1.9e ] \n", jmin[0], jmax[0] );
    printf("T_0] domain y := [ %1.4e , %1.9e ] \n", jmin[1], jmax[1] );
#endif
    ierr = DMDACreate2dRedundant( dav, si,si+nx, 0   , 1, 1, &B_surface_0 );CHKERRQ(ierr);
    ierr = DMDAGetCoordinateDA(B_surface_0,&cda_B_surf_0);CHKERRQ(ierr);
    ierr = DMDAGetCoordinates(B_surface_0,&B_surf_0_coordinates);CHKERRQ(ierr);
    ierr = DMDAVecGetArrayDOF(cda_B_surf_0,B_surf_0_coordinates,&LA_B_surf_0_coor);CHKERRQ(ierr);
#ifdef REMESH_DBG
    ierr = DMDAGetBoundingBox(B_surface_0,jmin,jmax);CHKERRQ(ierr);
    printf("B_0] domain x := [ %1.4e , %1.9e ] \n", jmin[0], jmax[0] );
    printf("B_0] domain y := [ %1.4e , %1.9e ] \n", jmin[1], jmax[1] );
#endif
    
    /* do the interp for the surface*/
    ierr = SurfaceInterpolation2D(T_surface_0,LA_T_surf_0_coor, T_surface_1,LA_T_surf_1_coor);CHKERRQ(ierr);
    /* restore and destroy the copy that is not used */
    ierr = DMDAVecRestoreArrayDOF(cda_T_surf_1,T_surf_1_coordinates,&LA_T_surf_1_coor);CHKERRQ(ierr);
    ierr = DMDestroy(&T_surface_1);CHKERRQ(ierr);
    
    /* do the interp for the bottom */
    ierr = SurfaceInterpolation2D(B_surface_0,LA_B_surf_0_coor, B_surface_1,LA_B_surf_1_coor);CHKERRQ(ierr);
    /* restore and destroy the copy that is not used anymore */
    ierr = DMDAVecRestoreArrayDOF(cda_B_surf_1,B_surf_1_coordinates,&LA_B_surf_1_coor);CHKERRQ(ierr);
    ierr = DMDestroy(&B_surface_1);CHKERRQ(ierr);
    
    
#ifdef REMESH_DBG
    /* now 0 should have a non zero topography */
    {
        PetscReal jmin[3],jmax[3];
        
        ierr = DMDAGetBoundingBox(T_surface_0,jmin,jmax);CHKERRQ(ierr);
        printf("a T_0] domain x := [ %1.4e , %1.4e ] \n", jmin[0], jmax[0] );
        printf("a T_0] domain y := [ %1.4e , %1.4e ] \n", jmin[1], jmax[1] );
        
        
        ierr = DMDAGetBoundingBox(B_surface_0,jmin,jmax);CHKERRQ(ierr);
        printf("b B_0] domain x := [ %1.4e , %1.4e ] \n", jmin[0], jmax[0] );
        printf("b B_0] domain y := [ %1.4e , %1.4e ] \n", jmin[1], jmax[1] );
    }
#endif
    
    
    /*get the corner of the local copy of the surface */
    ierr = DMDAGetCorners( T_surface_0, &si_0,&sj_0,0, &nx_0,&ny_0,0 );CHKERRQ(ierr);
    si_g = si_0;
    ei_g = si_0 + nx_0;
    /* access local coordinate array of the new mesh*/
    ierr = DMDAGetCoordinateDA(dav,&cda_dav);CHKERRQ(ierr);
    ierr = DMDAGetCoordinates(dav,&coordinates);CHKERRQ(ierr);
    ierr = DMDAVecGetArrayDOF(cda_dav,coordinates,&LA_coor);CHKERRQ(ierr);
    /*push the interpolated value in the new mesh */
    if ( (sj+ny) == (NY) ) {
        for (i=si_g; i<ei_g; i++) {
            // LA_coor[NY-1][si + (i-si_g)][0] = LA_T_surf_0_coor[0][i][0]; /* x */
            LA_coor[NY-1][si + (i-si_g)][1] = LA_T_surf_0_coor[0][i][1]; /* y - height */
        }
    }
    /* restore and destroy the redundant copy of the surface */
    ierr = DMDAVecRestoreArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
    ierr = DMDestroy(&T_surface_0);CHKERRQ(ierr);
    
    
    if ( (sj) == (0) ) {
        for (i=si_g; i<ei_g; i++) {
            // LA_coor[0][si + (i-si_g)][0] = LA_B_surf_0_coor[0][i][0]; /* x */
            LA_coor[0][si + (i-si_g)][1] = LA_B_surf_0_coor[0][i][1]; /* y - height */
        }
    }
    
    /* restore and destroy the redundant copy of the bottom */
    ierr = DMDAVecRestoreArrayDOF(cda_B_surf_0,B_surf_0_coordinates,&LA_B_surf_0_coor);CHKERRQ(ierr);
    ierr = DMDestroy(&B_surface_0);CHKERRQ(ierr);
    
    /* restore the local copy of global coord array */
    ierr = DMDAVecRestoreArrayDOF(cda_dav,coordinates,&LA_coor);CHKERRQ(ierr);
    /* Scatter into ghosted vector */
    ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
    
    /* remesh nodes using upper and lower surfaces as well as the original depth for vertical refinement !! */
    ndomainy = 1;
    PetscOptionsGetInt(PETSC_NULL,"-refine_n_domain_y",&ndomainy,0);
    Ny[0] = NY;
    PetscOptionsGetIntArray(PETSC_NULL,"-refine_y_dom_el",Ny,&ndomainy,PETSC_NULL);
    jstart = 0;
    for (jdom=0; jdom<ndomainy; jdom++) {
        jend = jstart+Ny[jdom]*2+1;
        ierr = DMDARemeshSetUniformCoordinatesBetweenJLayers2d(dav,jstart,jend);CHKERRQ(ierr);
        jstart= jend-1;
    }

    
    
    PetscFunctionReturn(0);
}






#undef __FUNCT__  
#define __FUNCT__ "DMDAComputeAverageSideValues_2D"
PetscErrorCode DMDAComputeAverageSideValues_2D(DM da,PetscScalar north[2],PetscScalar east[2],PetscScalar south[2],PetscScalar west[2])
{
	PetscErrorCode ierr;
	DM cda;
	Vec coords;
	PetscScalar ***LA_coords;
	PetscInt d,i,j,I,J,si,sj,nx,ny,NX,NY;
	PetscScalar avg_north[2],avg_south[2],avg_east[2],avg_west[2];
	
	PetscFunctionBegin;
	
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(da,&coords);CHKERRQ(ierr);
	
	ierr = DMDAGetInfo(da,0, &NX,&NY,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
	ierr = DMDAGetCorners(cda,&si,&sj,0,&nx,&ny,0);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cda,coords,&LA_coords);CHKERRQ(ierr);

	avg_north[0] = 0.0;
	avg_north[1] = 0.0;	
	J = NY-1;
	if (sj+ny-1 == J) {
		for (i=si; i<si+nx; i++) {
			avg_north[0] += LA_coords[J][i][0];
			avg_north[1] += LA_coords[J][i][1];
		}
	}
	
	avg_south[0] = 0.0;
	avg_south[1] = 0.0;	
	J = 0;
	if (sj == J) {
		for (i=si; i<si+nx; i++) {
			avg_south[0] += LA_coords[J][i][0];
			avg_south[1] += LA_coords[J][i][1];
		}
	}

	avg_east[0] = 0.0;
	avg_east[1] = 0.0;	
	I = NX-1;
	if (si+nx-1 == I) {
		for (j=sj; j<sj+ny; j++) {
			avg_east[0] += LA_coords[j][I][0];
			avg_east[1] += LA_coords[j][I][1];
		}
	}
	
	avg_west[0] = 0.0;
	avg_west[1] = 0.0;	
	I = 0;
	if (si == I) {
		for (j=sj; j<sj+ny; j++) {
			avg_west[0] += LA_coords[j][I][0];
			avg_west[1] += LA_coords[j][I][1];
		}
	}
	
	ierr = DMDAVecRestoreArrayDOF(cda,coords,&LA_coords);CHKERRQ(ierr);

	ierr = MPI_Allreduce(avg_north,north,2,MPIU_SCALAR,MPI_SUM,((PetscObject)da)->comm);CHKERRQ(ierr);
	ierr = MPI_Allreduce(avg_south,south,2,MPIU_SCALAR,MPI_SUM,((PetscObject)da)->comm);CHKERRQ(ierr);
	ierr = MPI_Allreduce(avg_east,east,2,MPIU_SCALAR,MPI_SUM,((PetscObject)da)->comm);CHKERRQ(ierr);
	ierr = MPI_Allreduce(avg_west,west,2,MPIU_SCALAR,MPI_SUM,((PetscObject)da)->comm);CHKERRQ(ierr);
	
	for (d=0; d<2; d++) {
		north[d] = north[d] / ((PetscScalar)NX);
		south[d] = south[d] / ((PetscScalar)NX);
		east[d]  = east[d] / ((PetscScalar)NY);
		west[d]  = west[d] / ((PetscScalar)NY);
	}
	
	PetscFunctionReturn(0);
}

/* 
 
 Free surface - JMAX is moved with the fluid velocity.
 Left wall moved with Unormal[0]
 Right wall moved with Unormal[1]
 Bottom wall moved with Vnorma_base
 
 Copy all surface nodes
 Zero all velocity dofs expect those on the surface
 Advect all nodes
 Extract advected surface + overlap
 Interpolate
 Push interpolated values into locally owned coordinate vector
 Scatter into ghosted vector
 */

#undef __FUNCT__  
#define __FUNCT__ "UpdateMeshGeometry_NormalVPrescribedWithLagrangianFreeSurface"
PetscErrorCode UpdateMeshGeometry_NormalVPrescribedWithLagrangianFreeSurface(DM dav,Vec velocity,PetscScalar Unormal[],PetscScalar Vnormal_base,PetscReal step)
{
	Vec coordinates,vel_advect_mesh;
	PetscInt i,j,nx,ny,NX,NY,si,sj;
	PetscScalar ***LA_velocity;
	DM T_surface_0, T_surface_1;
	PetscInt start_i, end_i;
	PetscInt nx_0,ny_0,si_0,sj_0;
	Vec T_surf_0_coordinates, T_surf_1_coordinates;
	PetscScalar ***LA_T_surf_0_coor, ***LA_T_surf_1_coor;
	DM cda_T_surf_0,cda_T_surf_1;
	PetscScalar ***LA_coor;
	PetscInt si_g,ei_g;
	DM cda_dav;
	PetscMPIInt rank;
	PetscReal jmin[3],jmax[3];
	PetscReal dx_0;
	PetscScalar north[2],east[2],south[2],west[2];
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	MPI_Comm_rank(((PetscObject)dav)->comm,&rank);
	
	/* compute avgeage left/right/base coordinates */
	ierr = DMDAComputeAverageSideValues_2D(dav,north,east,south,west);CHKERRQ(ierr);
#ifdef REMESH_DBG
	PetscPrintf(PETSC_COMM_WORLD,"SIDES : (N) avg y %1.6e , (E) avg x %1.6e , (S) avg y %1.6e , (W) avg x %1.6e \n",north[1],east[0],south[1],west[0]);
#endif	
	
	/* advect average box coords */
	west[0]  = west[0]  + step * Unormal[0];
	east[0]  = east[0]  + step * Unormal[1];
	south[1] = south[1] + step * Vnormal_base;

#ifdef REMESH_DBG
	PetscPrintf(PETSC_COMM_WORLD,"Unormal(W) = %1.6e , Unormal(E) = %1.6e , Vnormal(S) = %1.6e\n",Unormal[0],Unormal[1],Vnormal_base);
	PetscPrintf(PETSC_COMM_WORLD,"SIDES COORD(t+delta_t) : (W) x %1.6e , (E) x %1.6e , (S) y %1.6e \n",west[0],east[0],south[1]);
#endif	
	
	
	/* Copy all surface nodes */
	ierr = DMDAGetCorners( dav, &si,&sj,0, &nx,&ny,0 );CHKERRQ(ierr);
	ierr = DMDAGetInfo(dav,0, &NX,&NY,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
#ifdef REMESH_DBG	
	PetscPrintf(PETSC_COMM_SELF,"tau_0: fetching [%D <= i < %D], [j = %D] \n", si,si+nx, NY-1 );
#endif	
	ierr = DMDACreate2dRedundant( dav, si,si+nx, NY-1,  NY, 1, &T_surface_0 );CHKERRQ(ierr);
	
#ifdef REMESH_DBG
	{
		//ierr = DMDAGetBoundingBox(T_surface_0,jmin,jmax);CHKERRQ(ierr);
		ierr = DMDAGetSurfaceBoundingBox(dav,1,jmin,jmax);CHKERRQ(ierr);
		PetscPrintf(PETSC_COMM_WORLD,"Original free surface geometry:\n");
		PetscPrintf(PETSC_COMM_WORLD,"T_0] domain x := [ %1.4e , %1.4e ] \n", jmin[0], jmax[0] );
		PetscPrintf(PETSC_COMM_WORLD,"T_0] domain y := [ %1.4e , %1.4e ] \n", jmin[1], jmax[1] );
	}
#endif	
	
	/* Zero all velocity dofs expect those on the surface */
	ierr = DMGetGlobalVector(dav,&vel_advect_mesh);CHKERRQ(ierr);
	ierr = VecCopy(velocity,vel_advect_mesh);CHKERRQ(ierr);
	
	ierr = DMDAGetCorners(dav,&si,&sj,0,&nx,&ny,0);CHKERRQ(ierr);
	ierr = DMDAGetInfo(dav,0, &NX,&NY,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(dav,vel_advect_mesh,&LA_velocity);CHKERRQ(ierr);
	for (j=sj; j<sj+ny; j++) {
		if (j!=(NY-1)) {
			for (i=si; i<si+nx; i++) {
				LA_velocity[j][i][0] = 0.0; /* vx */
				LA_velocity[j][i][1] = 0.0; /* vy */
			}
		}
	}
	ierr = DMDAVecRestoreArrayDOF(dav,vel_advect_mesh,&LA_velocity);CHKERRQ(ierr);
	
	
	/* Advect all nodes */
	ierr = DMDAGetCoordinates(dav,&coordinates);CHKERRQ(ierr);
	ierr = VecAXPY(coordinates,step,vel_advect_mesh);CHKERRQ(ierr); /* x = x + dt.vel_advect_mesh */
	ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
	
	ierr = DMRestoreGlobalVector(dav,&vel_advect_mesh);CHKERRQ(ierr);
	
	/* Extract advected surface + overlap of 2 nodes */
	ierr = DMDAGetCorners( dav, &si,&sj,0, &nx,&ny,0 );CHKERRQ(ierr);
	ierr = DMDAGetInfo(dav,0, &NX,&NY,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
	start_i = PetscMax(0,si-2);
	end_i   = PetscMin(NX,si+nx+2);
#ifdef REMESH_DBG	
	PetscPrintf(PETSC_COMM_SELF,"tau_1: fetching [%D <= i < %D], [j = %D] \n", start_i,end_i, NY-1 );
#endif
	ierr = DMDACreate2dRedundant( dav, start_i,end_i, NY-1,NY, 1, &T_surface_1 );CHKERRQ(ierr);
	
#ifdef REMESH_DBG
	{
		//ierr = DMDAGetBoundingBox(T_surface_0,jmin,jmax);CHKERRQ(ierr);
		ierr = DMDAGetSurfaceBoundingBox(dav,1,jmin,jmax);CHKERRQ(ierr);

		PetscPrintf(PETSC_COMM_WORLD,"Deformed free surface (fluid velocity):\n");
		PetscPrintf(PETSC_COMM_WORLD,"T_1] domain x := [ %1.4e , %1.4e ] \n", jmin[0], jmax[0] );
		PetscPrintf(PETSC_COMM_WORLD,"T_1] domain y := [ %1.4e , %1.4e ] \n", jmin[1], jmax[1] );
	}
#endif	
	
	
	/* re-size and uniformly space in nodes in x direction on surface using the kinematic box constraints, T0 and set y = 0 */
	jmin[0] = west[0];
	jmax[0] = east[0];
	jmin[1] = jmax[1] = 0.0;
	dx_0 = (jmax[0]-jmin[0])/(PetscReal)(NX-1);
	
#ifdef REMESH_DBG	
	PetscPrintf(PETSC_COMM_WORLD,"Deformed box span (presecribed velocity):\n");
	PetscPrintf(PETSC_COMM_WORLD,"x := [ %1.4e , %1.4e ] dx_0 = %1.4e : NX = %d \n", jmin[0],jmax[0],dx_0,NX );
#endif
	
	ierr = DMDAGetCoordinateDA(T_surface_0,&cda_T_surf_0);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(T_surface_0,&T_surf_0_coordinates);CHKERRQ(ierr);
	ierr = DMDAGetCorners( T_surface_0, &si_0,&sj_0,0, &nx_0,&ny_0,0 );CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
#ifdef REMESH_DBG	
	PetscPrintf(PETSC_COMM_SELF,"T_surface_0: si-ej [%D <= i < %D], [j = %D] \n", si_0,si_0+nx_0, sj_0 );
#endif
	for (j=sj_0; j<sj_0+ny_0; j++) { /* this loop is redundant as it's a surface object */
		for (i=si_0; i<si_0+nx_0; i++) {
			LA_T_surf_0_coor[j][i][0] = jmin[0] + si*dx_0 + i*dx_0; /* T_surface is COMM_SELF, thus si_0 = 0 */
			//LA_T_surf_0_coor[j][i][0] = jmin[0] + i * dx_0;

			LA_T_surf_0_coor[j][i][1] = 0.0; /* y - height */
#ifdef REMESH_DBG	
			//printf("i_red=%d: i_g=%d: x = %1.4e \n", i, si_0+i, LA_T_surf_0_coor[j][i][0] );
#endif
		}
	}
	ierr = DMDAVecRestoreArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);

	/* inflow/shortening with fixed domain size will always produce this condition */
#if 0
	/* check domains overlap and user didn't move the boundaries in the opposite direction to the flow field */
	ierr = DMDAGetBoundingBox(T_surface_1,jmin,jmax);CHKERRQ(ierr);
	if (west[0] < jmin[0]) {
		PetscPrintf(PETSC_COMM_WORLD,"ALE box has been prescribed to move in inconsistent manner to free surface!\n");
		PetscPrintf(PETSC_COMM_WORLD," west[0] = %1.4e < jmin[0] = %1.4e \n", west[0],jmin[0]);
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"West boundary x-coor < West free surface x-coor : Cannot interpolate\n");
	}
	if (east[0] > jmax[0]) {
		PetscPrintf(PETSC_COMM_WORLD,"ALE box has been prescribed to move in inconsistent manner to free surface!\n");
		PetscPrintf(PETSC_COMM_WORLD," east[0] = %1.4e > jmax[0] = %1.4e \n", east[0],jmax[0]);
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"East boundary x-coor < East free surface x-coor : Cannot interpolate\n");
	}
#endif	
	
	/*  Interpolate from T_surface_1(y) => T_surface_0(y) */
	ierr = DMDAGetCoordinateDA(T_surface_0,&cda_T_surf_0);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(T_surface_0,&T_surf_0_coordinates);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
	
	ierr = DMDAGetCoordinateDA(T_surface_1,&cda_T_surf_1);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(T_surface_1,&T_surf_1_coordinates);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cda_T_surf_1,T_surf_1_coordinates,&LA_T_surf_1_coor);CHKERRQ(ierr);
	
	/* do the interp */
	ierr = SurfaceInterpolation_withExtrapolationAtEndPoints2D(T_surface_0,LA_T_surf_0_coor, T_surface_1,LA_T_surf_1_coor);CHKERRQ(ierr);
	
	ierr = DMDAVecRestoreArrayDOF(cda_T_surf_1,T_surf_1_coordinates,&LA_T_surf_1_coor);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);	
	

#ifdef REMESH_DBG 
	{
		ierr = DMDAGetBoundingBox(T_surface_1,jmin,jmax);CHKERRQ(ierr);		
		printf("interp) T_1] domain x := [ %1.4e , %1.4e ] x [ %1.4e , %1.4e ] \n", jmin[0], jmax[0], jmin[1], jmax[1] );
		ierr = DMDAGetBoundingBox(T_surface_0,jmin,jmax);CHKERRQ(ierr);		
		printf("interp) T_0] domain x := [ %1.4e , %1.4e ] x [ %1.4e , %1.4e ] \n", jmin[0], jmax[0], jmin[1], jmax[1] );
	}
#endif	
	
	
	/* force box size from the advect sides */
	/* fetch any y size from the volume mesh */
	ierr = DMDAGetBoundingBox(dav,jmin,jmax);CHKERRQ(ierr);
	ierr = DMDASetUniformCoordinates(dav,west[0],east[0],south[1],jmax[1],PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
	
	/* Push interpolated values into locally owned coordinate vector */
	ierr = DMDAGetCorners( T_surface_0, &si_0,&sj_0,0, &nx_0,&ny_0,0 );CHKERRQ(ierr);
	si_g = si_0;
	ei_g = si_0 + nx_0;
	//printf("[%d] si_g = %d : ei_g = %d \n", rank, si_g,ei_g);
	ierr = DMDAGetCoordinateDA(dav,&cda_dav);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(dav,&coordinates);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cda_dav,coordinates,&LA_coor);CHKERRQ(ierr);
	
	ierr = DMDAVecGetArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
	if ( (sj+ny) == (NY) ) {
		for (i=si_g; i<ei_g; i++) {
#ifdef REMESH_DBG 
			//printf("[%d] mapping LA_Tsurf0[%d] ==> LA_coor[%d] \n", rank, i, si + (i-si_g) ); 
#endif
			LA_coor[NY-1][si + (i-si_g)][0] = LA_T_surf_0_coor[0][i][0]; /* x */
			LA_coor[NY-1][si + (i-si_g)][1] = LA_T_surf_0_coor[0][i][1]; /* y - height */
		}
	}	
	ierr = DMDAVecRestoreArrayDOF(cda_dav,coordinates,&LA_coor);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
	
	
	/* Scatter into ghosted vector */
	ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
	
	
	/* remesh nodes using upper and lower surfaces */
	ierr = DMDARemeshSetUniformCoordinatesBetweenJLayers2d(dav,0,NY);CHKERRQ(ierr);
	
#ifdef REMESH_DBG 
	{
		ierr = DMDAGetBoundingBox(dav,jmin,jmax);CHKERRQ(ierr);		
		PetscPrintf(PETSC_COMM_WORLD,"final vol mesh) domain x := [ %1.4e , %1.4e ] x [ %1.4e , %1.4e ] \n", jmin[0], jmax[0], jmin[1], jmax[1] );
	}
#endif	
	
	
	ierr = DMDestroy(&T_surface_1);CHKERRQ(ierr);
	ierr = DMDestroy(&T_surface_0);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "DMDASetValuesLocalStencil_InsertValues_Stokes_Velocity"
PetscErrorCode DMDASetValuesLocalStencil_InsertValues_Stokes_Velocity(PetscScalar *fields_F,PetscInt u_eqn[],PetscScalar Fe_u[])
{
  PetscInt n,idx;
	
  PetscFunctionBegin;
  for (n = 0; n<U_BASIS_FUNCTIONS; n++) {
		idx = u_eqn[2*n  ];
    fields_F[idx] = Fe_u[NSD*n  ];
		
		idx = u_eqn[2*n+1];
    fields_F[idx] = Fe_u[NSD*n+1];
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "UpdateMeshGeometry_BilinearizeQ2Elements"
PetscErrorCode UpdateMeshGeometry_BilinearizeQ2Elements(DM dau)
{
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen,e,n,k,ii,jj;
	const PetscInt *elnidx;
	PetscScalar elcoordsQ2[2*Q2_NODES_PER_EL_2D];
	PetscScalar elcoordsQ1[2*NODES_PER_EL_Q1_2D],Ni[NODES_PER_EL_Q1_2D];
	PetscInt cnt;
	PetscScalar xi_nodal_coordsQ2[2*Q2_NODES_PER_EL_2D],x_new[2];
	PetscInt vel_el_lidx[U_BASIS_FUNCTIONS*2];
	PetscErrorCode ierr;
	
	PetscFunctionBegin;

	/* define some xi coords to interpolate to - these correspond to the nodal location of the q2 basis functions */
	/* this loop should be ordered the same way as the natural nodal ordering of the Q2 nodes in element space */
	cnt = 0;
	for (jj=0; jj<3; jj++) {
		for (ii=0; ii<3; ii++) {
			xi_nodal_coordsQ2[2*cnt+0] = -1.0 + (double)ii;
			xi_nodal_coordsQ2[2*cnt+1] = -1.0 + (double)jj;
			cnt++;
		}
	}
	
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA(dau,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(dau,&gcoords);CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen,&elnidx);CHKERRQ(ierr);
	for (e=0;e<nel;e++) {
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoordsQ2,(PetscInt*)&elnidx[nen*e],LA_gcoords);CHKERRQ(ierr);
		ierr = StokesVelocity_GetElementLocalIndices(vel_el_lidx,(PetscInt*)&elnidx[nen*e]);CHKERRQ(ierr);
		
		for (k=0; k<2; k++) {
			elcoordsQ1[2*0+k] = elcoordsQ2[2*0+k];
			elcoordsQ1[2*1+k] = elcoordsQ2[2*2+k];
			elcoordsQ1[2*2+k] = elcoordsQ2[2*6+k];
			elcoordsQ1[2*3+k] = elcoordsQ2[2*8+k];
		}
		
		/* for each interior point */
		for (k=0; k<cnt; k++) {
			ConstructNi_Q1_2D(&xi_nodal_coordsQ2[2*k],Ni);
			
			/* inpterpolate */
			x_new[0] = 0.0;
			x_new[1] = 0.0;
			for (n=0; n<NODES_PER_EL_Q1_2D; n++) {
				x_new[0] += elcoordsQ1[2*n+0] * Ni[n];
				x_new[1] += elcoordsQ1[2*n+1] * Ni[n];
			}
			
			elcoordsQ2[2*k+0] = x_new[0];
			elcoordsQ2[2*k+1] = x_new[1];
		}
		
		/* push modification */
		ierr = DMDASetValuesLocalStencil_InsertValues_Stokes_Velocity(LA_gcoords, vel_el_lidx,elcoordsQ2);CHKERRQ(ierr);
		
	}
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	/* why is this updating from local vector? */
	ierr = DMDASetCoordinatesFromLocalVector(dau,gcoords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}
	
#undef __FUNCT__
#define __FUNCT__ "UpdateMeshGeometry_SurfaceCourantStep"
PetscErrorCode UpdateMeshGeometry_SurfaceCourantStep(DM da,Vec velocity,PetscReal max_disp_y,PetscReal Courant_surf, PetscReal *step)
{
	Vec             Lvelocity, gcoords;
	PetscScalar     ***LA_velocity;
	DMDACoor2d      **LA_coords;
	PetscInt        i,j,si,sj,nx,ny,NX,NY;
	DM              cda;
	double          dt_min_local, dt_min;
    double          dt_min_local2, dt_min2;
    PetscReal       dt_step2;
	PetscReal       dt_step,surface_dy,surface_vy;
	MPI_Comm        comm;
	PetscErrorCode  ierr;
	
	PetscFunctionBegin;

    
  /* setup for coords */
  ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
  ierr = DMDAGetGhostedCoordinates(da,&gcoords);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(cda,gcoords,&LA_coords);CHKERRQ(ierr);
	
	/* setup velocity */
	ierr = DMGetLocalVector(da,&Lvelocity);CHKERRQ(ierr);
	ierr = DMGlobalToLocalBegin(da,velocity,INSERT_VALUES,Lvelocity);CHKERRQ(ierr);
	ierr = DMGlobalToLocalEnd(  da,velocity,INSERT_VALUES,Lvelocity);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(da,Lvelocity,&LA_velocity);CHKERRQ(ierr);
	
	dt_min_local = 1.0e32;
    dt_min_local2 = 1.0e32;
	
    ierr = DMDAGetInfo(da,0, &NX,&NY,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
	ierr = DMDAGetGhostCorners(da,&si,&sj,0 , &nx,&ny,0);CHKERRQ(ierr);
	for( j=sj; j<sj+ny; j++ ) {
		for( i=si; i<si+nx; i++ ) {
			if ((j!=NY-1)&&(j!=0) ){ continue; }

			//surface_dy = LA_coords[j][i].y - LA_coords[j-1][i].y;
			surface_vy = PetscAbs( LA_velocity[j][i][1] );
			
			//dt_step  = surface_dy / ( surface_vy + 1.0e-32 );
            dt_step2 = max_disp_y / ( surface_vy + 1.0e-32 );
            
			//if (dt_step < dt_min_local) { dt_min_local = dt_step; }
            if (dt_step2 < dt_min_local) { dt_min_local = dt_step2; }
		}
	}
	
  ierr = DMDAVecRestoreArray(cda,gcoords,&LA_coords);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArrayDOF(da,Lvelocity,&LA_velocity);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(da,&Lvelocity);CHKERRQ(ierr);
	
	ierr = PetscObjectGetComm((PetscObject)da,&comm);CHKERRQ(ierr);
	ierr = MPI_Allreduce(&dt_min_local,&dt_min,1,MPI_DOUBLE,MPI_MIN,comm);CHKERRQ(ierr);
    //ierr = MPI_Allreduce(&dt_min_local2,&dt_min2,1,MPI_DOUBLE,MPI_MIN,comm);CHKERRQ(ierr);
	
	PetscPrintf(PETSC_COMM_WORLD,"UpdateMeshGeometry_SurfaceCourantStep: dt_surf = %1.4e * Cr (Cr = %1.2e) \n", dt_min, Courant_surf );
        dt_min = dt_min * Courant_surf;
    
    PetscPrintf(PETSC_COMM_WORLD,"UpdateMeshGeometry_SurfaceCourantStep: dt_surf = %1.4e \n", dt_min2);
	
    //if (dt_min2 < dt_min) {dt_min = dt_min2;}  
	*step = dt_min;
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "UpdateMeshGeometry_DecoupledHorizontalVerticalMeshMovement"
PetscErrorCode UpdateMeshGeometry_DecoupledHorizontalVerticalMeshMovement(DM dav,Vec velocity,PetscReal step)
{
	PetscInt       M,N,P;
	Vec            velocity_ale;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ierr = DMGetGlobalVector(dav,&velocity_ale);CHKERRQ(ierr);
	ierr = VecCopy(velocity,velocity_ale);CHKERRQ(ierr);
	
	/* kill y componenet */
	ierr = VecStrideSet(velocity_ale,1,0.0);CHKERRQ(ierr); /* zero y component */
	/* take a full lagrangian step in x  */
	ierr = UpdateMeshGeometry_FullLagSurface(dav,velocity_ale,step);CHKERRQ(ierr);
	
	
	/* resample bottom nodes so they are equi-distance in x-z */
	{
		PetscReal dx,dz;
		PetscReal gmin[3],gmax[3];
		PetscInt i,j,k,si,sj,sk,nx,ny,nz;
		Vec coordinates;
		DM cda;
		DMDACoor2d **LA_coords;
		
		
		ierr = DMDAGetInfo(dav,0,&M,&N,&P,0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);
		ierr = DMDAGetBoundingBox(dav,gmin,gmax);CHKERRQ(ierr);
		dx = (gmax[0]-gmin[0])/( (PetscReal)((M-1)) );
		
		ierr = DMDAGetCorners(dav,&si,&sj,&sk,&nx,&ny,&nz);CHKERRQ(ierr);
		
		ierr = DMDAGetCoordinates(dav,&coordinates);CHKERRQ(ierr);
		ierr = DMDAGetCoordinateDA(dav,&cda);CHKERRQ(ierr);
		ierr = DMDAVecGetArray(cda,coordinates,&LA_coords);CHKERRQ(ierr);
		
		if (sj == 0) { /* bottom layer nodes */
			j = 0;
			for (i=si; i<si+nx; i++) {
				LA_coords[j][i].x = gmin[0] + i * dx;
			}
		}
		
		if (sj + ny == N) { /* top layer nodes */
			j = N - 1;
			for (i=si; i<si+nx; i++) {
				LA_coords[j][i].x = gmin[0] + i * dx;
			}
		}
		
		
		ierr = DMDAVecRestoreArray(cda,coordinates,&LA_coords);CHKERRQ(ierr);
		
		ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
	}
	
	
	ierr = VecCopy(velocity,velocity_ale);CHKERRQ(ierr);
	ierr = VecStrideSet(velocity_ale,0,0.0);CHKERRQ(ierr); /* zero x component */
	
	
	/* take a full lagrangian step in y  */
	ierr = UpdateMeshGeometry_FullLagSurface(dav,velocity_ale,step);CHKERRQ(ierr);
	
	/* move exterior based on interior */
	ierr = DMDARemeshJMAX_UpdateHeightsFromInterior(dav);CHKERRQ(ierr);
	
	/* clean up spacing inside */
	ierr = DMDAGetInfo(dav,0,&M,&N,&P,0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);
	ierr = DMDARemeshSetUniformCoordinatesBetweenJLayers2d(dav,0,N);CHKERRQ(ierr);
	
	ierr = DMRestoreGlobalVector(dav,&velocity_ale);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "UpdateMeshGeometry_Interp"
PetscErrorCode UpdateMeshGeometry_Interp(DM dav,Vec new_mesh_coords)
{
	PetscErrorCode ierr;
	PetscInt si,sj,nx,ny,NX,NY;
	DM T_surface_0,T_surface_1;
	PetscInt si_0,sj_0,nx_0,ny_0;
	DM cda_dav;
	PetscScalar ***LA_coor;
	DM cda_T_surf_0,cda_T_surf_1;
	Vec coordinates,T_surf_0_coordinates,T_surf_1_coordinates;
	PetscScalar ***LA_T_surf_0_coor,***LA_T_surf_1_coor;
	PetscInt i,j;
	PetscInt si_g,ei_g;
	PetscFunctionBegin;
	

	/* Copy all surface nodes */
	ierr = DMDAGetCorners( dav, &si,&sj,0, &nx,&ny,0 );CHKERRQ(ierr);
	ierr = DMDAGetInfo(dav,0, &NX,&NY,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
	ierr = DMDACreate2dRedundant( dav, si,si+nx, NY-1,  NY, 1, &T_surface_1 );CHKERRQ(ierr);

	/* Set new mesh configuration and extract surface (we only need the x spacing */
	ierr = RMTSetCoordinates(dav,new_mesh_coords);CHKERRQ(ierr);
	ierr = DMDACreate2dRedundant( dav, si,si+nx, NY-1,  NY, 1, &T_surface_0);CHKERRQ(ierr);
	
	/*  Interpolate from T_surface_1(y) => T_surface_0(y) */
	{
		ierr = DMDAGetCoordinateDA(T_surface_0,&cda_T_surf_0);CHKERRQ(ierr);
		ierr = DMDAGetCoordinates(T_surface_0,&T_surf_0_coordinates);CHKERRQ(ierr);
		ierr = DMDAVecGetArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
		
		ierr = DMDAGetCoordinateDA(T_surface_1,&cda_T_surf_1);CHKERRQ(ierr);
		ierr = DMDAGetCoordinates(T_surface_1,&T_surf_1_coordinates);CHKERRQ(ierr);
		ierr = DMDAVecGetArrayDOF(cda_T_surf_1,T_surf_1_coordinates,&LA_T_surf_1_coor);CHKERRQ(ierr);
		
		/* do the interp */
		ierr = SurfaceInterpolation_withExtrapolationAtEndPoints2D(T_surface_0,LA_T_surf_0_coor, T_surface_1,LA_T_surf_1_coor);CHKERRQ(ierr);
		
		ierr = DMDAVecRestoreArrayDOF(cda_T_surf_1,T_surf_1_coordinates,&LA_T_surf_1_coor);CHKERRQ(ierr);
		ierr = DMDAVecRestoreArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);	
	}
	
	/* Push interpolated values into locally owned coordinate vector */
	ierr = DMDAGetCorners( T_surface_0, &si_0,&sj_0,0, &nx_0,&ny_0,0 );CHKERRQ(ierr);
	si_g = si_0;
	ei_g = si_0 + nx_0;
	//printf("[%d] si_g = %d : ei_g = %d \n", rank, si_g,ei_g);
	ierr = DMDAGetCoordinateDA(dav,&cda_dav);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(dav,&coordinates);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cda_dav,coordinates,&LA_coor);CHKERRQ(ierr);
	
	ierr = DMDAVecGetArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
	if ( (sj+ny) == (NY) ) {
		for (i=si_g; i<ei_g; i++) {
			LA_coor[NY-1][si + (i-si_g)][0] = LA_T_surf_0_coor[0][i][0]; /* x */
			LA_coor[NY-1][si + (i-si_g)][1] = LA_T_surf_0_coor[0][i][1]; /* y - height */
		}
	}	
	ierr = DMDAVecRestoreArrayDOF(cda_dav,coordinates,&LA_coor);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArrayDOF(cda_T_surf_0,T_surf_0_coordinates,&LA_T_surf_0_coor);CHKERRQ(ierr);
	
	
	/* Scatter into ghosted vector */
	ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
	
	/* remesh nodes using upper and lower surfaces */
	//ierr = DMDARemeshSetUniformCoordinatesBetweenJLayers2d(dav,0,NY);CHKERRQ(ierr);
	
	ierr = DMDestroy(&T_surface_1);CHKERRQ(ierr);
	ierr = DMDestroy(&T_surface_0);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "InterpolateFieldBetweenDMs_SEQ"
PetscErrorCode InterpolateFieldBetweenDMs_SEQ(DM dm_field,Vec coords_omega_k,Vec field_k,Vec field)
{
	PetscErrorCode ierr;
  DM dm_omega;
  Vec omega_coordinates;
  PetscScalar *LA_omega_coordinates,*LA_field_k,*LA_field;
  PetscScalar *LA_omega_k_coordinates;
  PetscInt i,j,k,npoints;
  PetscInt Ni,Nj,ndof,d;
  MPntStd *mp_std;
  PetscBool nearest_neighbour_search_required;
  PetscInt missing_points;
  const PetscInt *elnidx_u;
  PetscInt e,nel,nen_u;
  PetscInt *gidx;
  PetscInt lmx,lmy;
	PetscScalar Ni_p[Q2_NODES_PER_EL_2D],vel_p[NSD];
	PetscScalar el_velocity[Q2_NODES_PER_EL_2D*NSD];
  
  
  ierr = VecZeroEntries(field);CHKERRQ(ierr);
  
  /* Pull out coordinates from dm_omega and copy into a swarm */
  ierr = DMDAGetInfo(dm_field,0,&Ni,&Nj,0, 0,0,0, &ndof,0, 0,0,0, 0);CHKERRQ(ierr);
  
  if (ndof != 2) {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Only valid if dm_field->dof = 2");
  }
  
  npoints = Ni * Nj;
  PetscMalloc(sizeof(MPntStd)*npoints,&mp_std);
  
  ierr = DMDAGetCoordinateDA(dm_field,&dm_omega);CHKERRQ(ierr);
  ierr = DMDAGetCoordinates(dm_field,&omega_coordinates);CHKERRQ(ierr);
  ierr = VecGetArray(omega_coordinates,&LA_omega_coordinates);CHKERRQ(ierr);
  for (i=0; i<npoints; i++) {
    mp_std[i].coor[0] = LA_omega_coordinates[2*i+0];
    mp_std[i].coor[1] = LA_omega_coordinates[2*i+1];
  }
  ierr = VecRestoreArray(omega_coordinates,&LA_omega_coordinates);CHKERRQ(ierr);
  

  ierr = DMDAGetGlobalIndices(dm_field,0,&gidx);CHKERRQ(ierr);
  ierr = DMDAGetElements_pTatin(dm_field,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
  ierr = DMDAGetLocalSizeElementQ2(dm_field,&lmx,&lmy,PETSC_NULL);CHKERRQ(ierr);
  
  /* perform point location */
  {
    double tolerance;
    int max_its;
    Truth use_nonzero_guess, monitor, log;
    PetscScalar *LA_gcoords;
    
    /* setup for coords */
    ierr = VecGetArray(coords_omega_k,&LA_gcoords);CHKERRQ(ierr);
    
    /* point location parameters */
    tolerance         = 1.0e-10;
    max_its           = 10;
    use_nonzero_guess = _FALSE;
    monitor           = _FALSE;
    log               = _FALSE;
    
    InverseMappingDomain_2dQ2(tolerance, max_its,
                              use_nonzero_guess,
                              monitor, log,
                              (const double*)LA_gcoords, (const int)lmx,(const int)lmy, (const int*)elnidx_u,
                              npoints, mp_std );
    
    ierr = VecRestoreArray(coords_omega_k,&LA_gcoords);CHKERRQ(ierr);
  }
  

  ierr = VecGetArray(field,&LA_field);CHKERRQ(ierr);
  ierr = VecGetArray(field_k,&LA_field_k);CHKERRQ(ierr);
  
  /* do interpolation for points found inside the domain */
  nearest_neighbour_search_required = PETSC_FALSE;
  missing_points = 0;
  for (i=0; i<npoints; i++) {
    PetscScalar values[10];
    PetscInt idx[10];
    
    
    if (mp_std[i].wil == -1) {
      missing_points++;
      nearest_neighbour_search_required = PETSC_TRUE;
      continue;
    }
    
		e = mp_std[i].wil;
		if (e < 0) { SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Point[%d] has wil_e < 0", e ); }
		
		ierr = DMDAGetVectorElementFieldQ2_2D(el_velocity,(PetscInt*)&elnidx_u[nen_u*e],LA_field_k);CHKERRQ(ierr);
		
		PTatinConstructNI_Q2_2D(mp_std[i].xi,Ni_p);
    
		values[0] = values[1] = 0.0;
		for (k=0; k<Q2_NODES_PER_EL_2D; k++) {
			values[0] += Ni_p[k] * el_velocity[NSD*k+0];
			values[1] += Ni_p[k] * el_velocity[NSD*k+1];
		}
    
    for (d=0; d<ndof; d++) {
      idx[d] = ndof * i + d;
      LA_field[idx[d]] = values[d];
    }
    //ierr = VecSetValues(field,ndof,idx,values,INSERT_VALUES);CHKERRQ(ierr);
  }

  /* do nearest neighbour interpolation for points outside the domain */
  if (nearest_neighbour_search_required) {
    PetscReal sep2min,sep2;
    PetscInt jmin;
    PetscScalar values[10];
    PetscInt idx[10];
    
    PetscPrintf(PETSC_COMM_WORLD,"  InterpolateFieldBetweenDMs_SEQ: %D points required nearest neighbour interpolation\n",missing_points);
    ierr = VecGetArray(coords_omega_k,&LA_omega_k_coordinates);CHKERRQ(ierr);
    
    for (i=0; i<npoints; i++) {
      if (mp_std[i].wil != -1) continue;
      
      jmin = -1;
      sep2min = 1.0e10;
      for (j=0; j<npoints; j++) {
        
        sep2  = (mp_std[i].coor[0] - LA_omega_k_coordinates[2*j])*(mp_std[i].coor[0] - LA_omega_k_coordinates[2*j]);
        sep2 += (mp_std[i].coor[1] - LA_omega_k_coordinates[2*j+1])*(mp_std[i].coor[1] - LA_omega_k_coordinates[2*j+1]);
        if (sep2 < sep2min) {
          sep2min  = sep2;
          jmin = j;
        }
      }

      /* force nearest values */
      for (d=0; d<ndof; d++) {
        values[d] = LA_field_k[ndof*jmin + d];
      }

      for (d=0; d<ndof; d++) {
        idx[d] = ndof * i + d;
        LA_field[idx[d]] = values[d];
      }
      //ierr = VecSetValues(field,ndof,idx,values,INSERT_VALUES);CHKERRQ(ierr);
    }
    ierr = VecRestoreArray(coords_omega_k,&LA_omega_k_coordinates);CHKERRQ(ierr);
  }
  //ierr = VecAssemblyBegin(field);CHKERRQ(ierr);
  //ierr = VecAssemblyEnd(field);CHKERRQ(ierr);
  
  ierr = VecRestoreArray(field_k,&LA_field_k);CHKERRQ(ierr);
  PetscFree(mp_std);
  
  /*
  {
		PetscViewer viewer;
		
		ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, "interp_v.vtk",&viewer);CHKERRQ(ierr);
		ierr = PetscViewerSetFormat(viewer,PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
		ierr = DMView(dm_field,viewer);CHKERRQ(ierr);
		ierr = VecView(field_k,viewer);CHKERRQ(ierr);
		ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  }
  */
	PetscFunctionReturn(0);
}

/*
 Interpolate from field defined on omega_k (mesh) to omega (mesh)
 
*/
#undef __FUNCT__
#define __FUNCT__ "InterpolateFieldBetweenDMs"
PetscErrorCode InterpolateFieldBetweenDMs(DM dm_field,Vec coords_omega_k,Vec field_k,Vec field)
{
	PetscErrorCode ierr;
  MPI_Comm comm;
  PetscMPIInt size;
  
  ierr = PetscObjectGetComm((PetscObject)dm_field,&comm);CHKERRQ(ierr);
  ierr = MPI_Comm_size(comm,&size);CHKERRQ(ierr);
  if (size == 1) {
    ierr = InterpolateFieldBetweenDMs_SEQ(dm_field,coords_omega_k,field_k,field);CHKERRQ(ierr);
  } else {
    SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Only valid in sequential");
  }
  
	PetscFunctionReturn(0);
}


