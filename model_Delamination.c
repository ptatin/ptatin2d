/*
 
 Model Description:
 
 
 Input / command line parameters:
 -sqrdick_eta{0,1,2,3,4} : ... what is it ... (default = XXX)
 -sqrdick_rho{0,1,2,3,4}
 -sqrdick_{Lx,Ly}
 -sqrdick_Layer{Y1,Y2}
 -sqrdick_DickMin{X,Y}
 -sqrdick_DickMaxX
 
 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"

/* ================= Delamination, aka "SqrDick ================= */

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelOutput_Delamination"
PetscErrorCode pTatin2d_ModelOutput_Delamination(pTatinCtx ctx,Vec X,const char prefix[])
{
	PetscErrorCode ierr;
	
	ierr = pTatin2d_ModelOutput_Default(ctx,X,prefix);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_Delamination"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_Delamination(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
	
	ierr = DMDASetUniformCoordinates(ctx->dav,0.0,10.0, 0.0,4.0, 0.0,0.0);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_Delamination"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_Delamination(pTatinCtx ctx)
{
	PetscInt e,ncells;
	PetscInt p,ngp;
	GaussPointCoefficientsStokes *gausspoints;
	PetscReal opts_eta0,opts_eta1,opts_eta2,opts_eta3,opts_eta4;
	PetscReal opts_rho0,opts_rho1,opts_rho2,opts_rho3,opts_rho4;
	PetscReal  opts_Lx,opts_Ly;	
  PetscReal  opts_LayerY1,opts_LayerY2, opts_DickMinY,opts_DickMinX,opts_DickMaxX;
  PetscReal  opts_misses0,opts_misses1, opts_misses2, opts_misses3,opts_misses4,opts_misses5;
  PetscReal gravity; 
	PetscErrorCode ierr;
	RheologyConstants *rheology;
  
	PetscFunctionBegin;
	gravity = -1.0e-1;
	opts_eta0 = 1000.0;	 opts_rho0 = 2.7;
	opts_eta1 = 0.01;    opts_rho1 = 2.9;
	opts_eta2 = 1.0;     opts_rho2 = 2.9;
	opts_eta3 = 100.0;   opts_rho3 = 3.3;
	opts_eta4 = 100.0;   opts_rho4 = 3.4;
	
	opts_misses0 = 5.e-3; 
  opts_misses1 = 5.e-3;
  opts_misses2 = 5.e-3;
  opts_misses3 = 5.e-3;
  opts_misses4 = 5.e-3;
  opts_misses5 = 5.e-3;
  
	ierr = PetscOptionsGetReal(PETSC_NULL,"-sqrdick_eta0",&opts_eta0,0);CHKERRQ(ierr);ierr = PetscOptionsGetReal(PETSC_NULL,"-sqrdick_rho0",&opts_rho0,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-sqrdick_eta1",&opts_eta1,0);CHKERRQ(ierr);ierr = PetscOptionsGetReal(PETSC_NULL,"-sqrdick_rho1",&opts_rho1,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-sqrdick_eta2",&opts_eta2,0);CHKERRQ(ierr);ierr = PetscOptionsGetReal(PETSC_NULL,"-sqrdick_rho2",&opts_rho2,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-sqrdick_eta3",&opts_eta3,0);CHKERRQ(ierr);ierr = PetscOptionsGetReal(PETSC_NULL,"-sqrdick_rho3",&opts_rho3,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-sqrdick_eta4",&opts_eta4,0);CHKERRQ(ierr);ierr = PetscOptionsGetReal(PETSC_NULL,"-sqrdick_rho4",&opts_rho4,0);CHKERRQ(ierr);
	
  rheology  = &ctx->rheology_constants;
	rheology->rheology_type = RHEOLOGY_VISCOUS;

  
	rheology->const_eta0[0] = opts_eta0;
  rheology->const_eta0[1] = opts_eta1;
  rheology->const_eta0[2] = opts_eta2;
  rheology->const_eta0[3] = opts_eta3;
  rheology->const_eta0[4] = opts_eta4;
  rheology->const_eta0[5] = 1.0;
  
  rheology->mises_tau_yield[0] = opts_misses0;
  rheology->mises_tau_yield[1] = opts_misses1;
  rheology->mises_tau_yield[2] = opts_misses2;
  rheology->mises_tau_yield[3] = opts_misses3;
  rheology->mises_tau_yield[4] = opts_misses4;
  rheology->mises_tau_yield[5] = opts_misses5;
  
  
	opts_Lx   = 10.0;
	opts_Ly   = 4.0;
	ierr = PetscOptionsGetReal(PETSC_NULL,"-sqrdick_Lx",&opts_Lx,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-sqrdick_Ly",&opts_Ly,0);CHKERRQ(ierr);
	
  opts_LayerY1    = 4.0-0.2;
	opts_LayerY2    = 4.0-0.4;
	opts_DickMinY   = 4.0-0.9;
  opts_DickMinX   = 4.5;
  opts_DickMaxX   = 5.5;
	ierr = PetscOptionsGetReal(PETSC_NULL,"-sqrdick_LayerY1",&opts_LayerY1,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-sqrdick_LayerY2",&opts_LayerY2,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-sqrdick_DickMinY",&opts_DickMinY,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-sqrdick_DickMinX",&opts_DickMinX,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-sqrdick_DickMaxX",&opts_DickMaxX,0);CHKERRQ(ierr);
	
	ncells = ctx->Q->ncells;
	ngp    = ctx->Q->ngp;
	
	for (e=0;e<ncells;e++) {
		ierr = QuadratureStokesGetCell(ctx->Q,e,&gausspoints);CHKERRQ(ierr);
		
		for (p=0; p<ngp; p++) {
			PetscScalar coord_x = gausspoints[p].coord[0];
			PetscScalar coord_y = gausspoints[p].coord[1];
			if (coord_y > opts_LayerY1){
        gausspoints[p].phase = 0;
				gausspoints[p].Fu[1] = opts_rho0*gravity;
			} else if( coord_y > opts_LayerY2){
				if (coord_x < opts_DickMinX){
				  gausspoints[p].phase = 1;
					gausspoints[p].Fu[1] = opts_rho1*gravity;
				}else{
          gausspoints[p].phase = 2;
					gausspoints[p].Fu[1] = opts_rho2*gravity;
				}			   
			} else if (coord_y > opts_DickMinY){
				if ((coord_x > opts_DickMinX) && (coord_x < opts_DickMaxX) ) {
          gausspoints[p].phase = 4;
					gausspoints[p].Fu[1] = opts_rho4*gravity;					
				} else {
          gausspoints[p].phase = 3;
					gausspoints[p].Fu[1] = opts_rho3*gravity;
				}  
			} else {
        gausspoints[p].phase = 5;
				gausspoints[p].Fu[1] = opts_rho3*gravity;
			}
			
			//gausspoints[p].eta_ref = gausspoints[p].eta;
 			gausspoints[p].Fu[0]   = 0.0;
			gausspoints[p].Fp      = 0.0;
		}
    
    for (p=0; p<ngp; p++) {
      gausspoints[p].phase = gausspoints[4].phase;
      gausspoints[p].Fu[0] = gausspoints[4].Fu[0];
      gausspoints[p].Fu[1] = gausspoints[4].Fu[1];
      gausspoints[p].Fp    = gausspoints[4].Fp;
    }
      
    
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_Delamination"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_Delamination(pTatinCtx ctx)
{
	BCList         ubclist;
	PetscScalar    bcval;
	DM             dav;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ubclist = ctx->u_bclist;
	dav     = ctx->dav;
	bcval = 0.0  ; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
	bcval = 3.0e-3*0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	bcval = 0.0  ; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

