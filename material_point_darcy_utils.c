
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"

#include "petscksp.h"
#include "petscdm.h"

#include "pTatin2d.h"
#include "swarm_fields.h"
#include "data_exchanger.h"
#include "MPntStd_def.h"
#include "MPntPDarcy_def.h"
#include "phys_darcy_equation.h"
#include "element_type_Q1.h"
#include "dmdae.h"
#include "material_point_darcy_utils.h"


#undef __FUNCT__
#define __FUNCT__ "SwarmView_MPntPDarcy_VTKappended_binary"
PetscErrorCode SwarmView_MPntPDarcy_VTKappended_binary(DataBucket db,const char name[])
{
	PetscMPIInt rank;
	FILE *vtk_fp;
	PetscInt k;
	int npoints;
	PetscLogDouble t0,t1;
	DataField PField_std;
	DataField PField_darcy;
	int byte_offset,length;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	ierr = PetscGetTime(&t0);CHKERRQ(ierr);
	
	if ((vtk_fp = fopen ( name, "w")) == NULL)  {
		SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
	}
	
	fprintf( vtk_fp, "<?xml version=\"1.0\"?>\n");
	
#ifdef WORDSIZE_BIGENDIAN
	fprintf( vtk_fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
#else
	fprintf( vtk_fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
#endif
	fprintf( vtk_fp, "\t<UnstructuredGrid>\n" );
	
	DataBucketGetSizes(db,&npoints,PETSC_NULL,PETSC_NULL);
	fprintf( vtk_fp, "\t\t<Piece NumberOfPoints=\"%d\" NumberOfCells=\"%d\">\n",npoints,npoints );
	
	
	fprintf( vtk_fp, "\n");
	fprintf( vtk_fp, "\t\t\t<Cells>\n");
	
	byte_offset = 0;
	
	// connectivity //
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Int32\" Name=\"connectivity\" format=\"appended\" offset=\"%d\" />\n",byte_offset);
  byte_offset = byte_offset + sizeof(int) + npoints * sizeof(int);
	
	// offsets //
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Int32\" Name=\"offsets\" format=\"appended\" offset=\"%d\" />\n",byte_offset);
  byte_offset = byte_offset + sizeof(int) + npoints * sizeof(int);
	
	// types //
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"UInt8\" Name=\"types\" format=\"appended\" offset=\"%d\" />\n",byte_offset);
  byte_offset = byte_offset + sizeof(int) + npoints * sizeof(unsigned char);
	
	fprintf( vtk_fp, "\t\t\t</Cells>\n");
	
	fprintf( vtk_fp, "\n");
	fprintf( vtk_fp, "\t\t\t<CellData>\n");
	fprintf( vtk_fp, "\t\t\t</CellData>\n");
	fprintf( vtk_fp, "\n");
	
	
	DataBucketGetDataFieldByName(db, MPntStd_classname      ,&PField_std);
	DataBucketGetDataFieldByName(db, MPntPDarcy_classname ,&PField_darcy);
	
	
	/* point coordinates */
	fprintf( vtk_fp, "\t\t\t<Points>\n");
	
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\" format=\"appended\" offset=\"%d\" />\n",byte_offset);
  byte_offset = byte_offset + sizeof(int) + npoints * 3 * sizeof(double);
	
	fprintf( vtk_fp, "\t\t\t</Points>\n");
	fprintf( vtk_fp, "\n");
	
	/* point data BEGIN */
	fprintf( vtk_fp, "\t\t\t<PointData>\n");
	
	/* auto generated shit goes here */
	{
		MPntStd      *marker_std     = PField_std->data; /* should write a function to do this */
		MPntPDarcy   *marker_darcy   = PField_darcy->data; /* should write a function to do this */
		
		MPntStdVTKWriteBinaryAppendedHeaderAllFields(vtk_fp,&byte_offset,(const int)npoints,(const MPntStd*)marker_std );
		MPntPDarcyVTKWriteBinaryAppendedHeaderAllFields(vtk_fp,&byte_offset,(const int)npoints,(const MPntPDarcy*)marker_darcy);
	}
	fprintf( vtk_fp, "\t\t\t</PointData>\n");
	fprintf( vtk_fp, "\n");
	/* point data END */
	
	
	fprintf( vtk_fp, "\t\t</Piece>\n");
	fprintf( vtk_fp, "\t</UnstructuredGrid>\n");
	
	/* WRITE APPENDED DATA HERE */
	fprintf( vtk_fp,"\t<AppendedData encoding=\"raw\">\n");
	fprintf( vtk_fp,"_");
	
	/* connectivity, offsets, types, coords */
	////////////////////////////////////////////////////////
	/* write connectivity */
	length = sizeof(int)*npoints;
	fwrite( &length,sizeof(int),1,vtk_fp);
	for (k=0; k<npoints; k++) {
		int idx = k;
		fwrite( &idx, sizeof(int),1, vtk_fp );
	}
	////////////////////////////////////////////////////////
	/* write offset */
	length = sizeof(int)*npoints;
	fwrite( &length,sizeof(int),1,vtk_fp);
	for (k=0; k<npoints; k++) {
		int idx = k+1;
		fwrite( &idx, sizeof(int),1, vtk_fp );
	}
	////////////////////////////////////////////////////////
	/* write types */
	length = sizeof(unsigned char)*npoints;
	fwrite( &length,sizeof(int),1,vtk_fp);
	for (k=0; k<npoints; k++) {
		unsigned char idx = 1; /* VTK_VERTEX */
		fwrite( &idx, sizeof(unsigned char),1, vtk_fp );
	}
	////////////////////////////////////////////////////////
	/* write coordinates */
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	length = sizeof(double)*npoints*3;
	fwrite( &length,sizeof(int),1,vtk_fp);
	for (k=0; k<npoints; k++) {
		MPntStd *marker;
		double  *coor;
		double  coords_k[] = {0.0, 0.0, 0.0};
		
		DataFieldAccessPoint(PField_std,k,(void**)&marker);
		MPntStdGetField_global_coord(marker,&coor);
		coords_k[0] = coor[0];
		coords_k[1] = coor[1];
		
		fwrite( coords_k, sizeof(double), 3, vtk_fp );
	}
	DataFieldRestoreAccess(PField_std);
	
	/* auto generated shit for the marker data goes here */
	{
		MPntStd      *markers_std     = PField_std->data;
		MPntPDarcy *markers_darcy = PField_darcy->data;
		
		MPntStdVTKWriteBinaryAppendedDataAllFields(vtk_fp,npoints,markers_std);
		MPntPDarcyVTKWriteBinaryAppendedDataAllFields(vtk_fp,npoints,markers_darcy);
	}
	
	fprintf( vtk_fp,"\n\t</AppendedData>\n");
	
	
	fprintf( vtk_fp, "</VTKFile>\n");
	
	
	if( vtk_fp!= NULL ) {
		fclose( vtk_fp );
		vtk_fp = NULL;
	}
	
	ierr = PetscGetTime(&t1);CHKERRQ(ierr);
#ifdef PROFILE_TIMING
	PetscPrintf(PETSC_COMM_WORLD,"VTKWriter(%s): Time %1.4e sec\n",__FUNCT__,t1-t0);
#endif
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "__SwarmView_MPntPDarcy_PVTU"
PetscErrorCode __SwarmView_MPntPDarcy_PVTU(const char prefix[],const char name[])
{
	PetscMPIInt nproc,rank;
	FILE *vtk_fp;
	PetscInt i;
	char *sourcename;
	
	PetscFunctionBegin;
	
	if ((vtk_fp = fopen ( name, "w")) == NULL)  {
		SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
	}
	
	/* (VTK) generate pvts header */
	fprintf( vtk_fp, "<?xml version=\"1.0\"?>\n");
	
#ifdef WORDSIZE_BIGENDIAN
	fprintf( vtk_fp, "<VTKFile type=\"PUnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
#else
	fprintf( vtk_fp, "<VTKFile type=\"PUnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
#endif
	
	/* define size of the nodal mesh based on the cell DM */
	fprintf( vtk_fp, "  <PUnstructuredGrid GhostLevel=\"0\">\n" ); /* note overlap = 0 */
	
	
	/* DUMP THE CELL REFERENCES */
	fprintf( vtk_fp, "    <PCellData>\n");
	fprintf( vtk_fp, "    </PCellData>\n");
	
	
	///////////////
	fprintf( vtk_fp, "    <PPoints>\n");
	fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\"/>\n");
	fprintf( vtk_fp, "    </PPoints>\n");
	///////////////
	
	///////////////
  fprintf(vtk_fp, "    <PPointData>\n");
	MPntStdPVTUWriteAllPPointDataFields(vtk_fp);
	MPntPDarcyPVTUWriteAllPPointDataFields(vtk_fp);
  fprintf(vtk_fp, "    </PPointData>\n");
	///////////////
	
	
	/* write out the parallel information */
	MPI_Comm_size(PETSC_COMM_WORLD,&nproc);
	for (i=0; i<nproc; i++) {
		asprintf( &sourcename, "%s-subdomain%1.5d.vtu", prefix, i );
		fprintf( vtk_fp, "    <Piece Source=\"%s\"/>\n",sourcename);
		free(sourcename);
	}
	
	/* close the file */
	fprintf( vtk_fp, "  </PUnstructuredGrid>\n");
	fprintf( vtk_fp, "</VTKFile>\n");
	
	if(vtk_fp!=NULL){
		fclose( vtk_fp );
		vtk_fp = NULL;
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SwarmOutputParaView_MPntPDarcy"
PetscErrorCode SwarmOutputParaView_MPntPDarcy(DataBucket db,const char path[],const char prefix[])
{ 
	char           *vtkfilename,*filename;
	PetscMPIInt    rank;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ierr = pTatinGenerateParallelVTKName(prefix,"vtu",&vtkfilename);CHKERRQ(ierr);
	if (path) {
		asprintf(&filename,"%s/%s",path,vtkfilename);
	} else {
		asprintf(&filename,"./%s",vtkfilename);
	}
	
	//#ifdef __VTK_ASCII__
	//	ierr = SwarmView_MPntPDarcy_VTKascii( db,filename );CHKERRQ(ierr);
	//#endif
	//#ifndef __VTK_ASCII__
	ierr = SwarmView_MPntPDarcy_VTKappended_binary(db,filename);CHKERRQ(ierr);
	//#endif
	free(filename);
	free(vtkfilename);
	
	
	ierr = pTatinGenerateVTKName(prefix,"pvtu",&vtkfilename);CHKERRQ(ierr);
	if (path) {
		asprintf(&filename,"%s/%s",path,vtkfilename);
	} else {
		asprintf(&filename,"./%s",vtkfilename);
	}
	
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	if (rank==0) {
		ierr = __SwarmView_MPntPDarcy_PVTU( prefix, filename );CHKERRQ(ierr);
	}
	free(filename);
	free(vtkfilename);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SwarmUpdateGaussPropertiesLocalL2Projection_P0_MPntPDarcy"
PetscErrorCode SwarmUpdateGaussPropertiesLocalL2Projection_P0_MPntPDarcy(const int npoints,MPntStd mp_std[],MPntPDarcy mp_darcy[],DM da,QuadratureVolumeDarcy Q)
{
	PetscInt p,i,ngp;
	PetscInt nel,nen,e;
	const PetscInt *elnidx;
	int *el_pcount;
	double *el_avg_kappa, *el_avg_H;
	CoefficientsDarcyEq *quadraturepoints;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ierr = DMDAGetElementsQ1(da,&nel,&nen,&elnidx);CHKERRQ(ierr);
	
	ierr = PetscMalloc( sizeof(int)*nel, &el_pcount );CHKERRQ(ierr);
	ierr = PetscMalloc( sizeof(double)*nel, &el_avg_kappa );CHKERRQ(ierr);
	ierr = PetscMalloc( sizeof(double)*nel, &el_avg_H );CHKERRQ(ierr);
	
	/* init */
	for (e=0; e<nel; e++) {
		el_pcount[e]    = 0;
		el_avg_kappa[e] = 0.0;
		el_avg_H[e]     = 0.0;
	}
	
	for (p=0; p<npoints; p++) {
		double kappa_p  = mp_darcy[p].diff;
		double H_p      = mp_darcy[p].source;
		
		e = mp_std[p].wil;
		if (e >= nel) {
			SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"marker->wil >= num local elements");
		}
		
		el_avg_kappa[e] = el_avg_kappa[e] + kappa_p;
		el_avg_H[e]     = el_avg_H[e]     + H_p;
		
		el_pcount[e]++;
	}
	for (e=0; e<nel; e++) {
		if (el_pcount[e]==0) {
			SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Cell doesn't contain any material points");
		}
		
		el_avg_kappa[e] = el_avg_kappa[e] / (double)(el_pcount[e]);
		el_avg_H[e]     = el_avg_H[e]     / (double)(el_pcount[e]);
	}
	
	
	/* traverse elements and interpolate */
	ngp = Q->ngp;
	for (e=0;e<nel;e++) {
		ierr = QuadratureVolumeDarcyGetCell(Q,e,&quadraturepoints);CHKERRQ(ierr);
		
		for (p=0; p<ngp; p++) {
			quadraturepoints[p].diff = el_avg_kappa[e];
			quadraturepoints[p].source = el_avg_H[e];
		}
	}
	
	ierr = PetscFree(el_pcount);CHKERRQ(ierr);
	ierr = PetscFree(el_avg_kappa);CHKERRQ(ierr);
	ierr = PetscFree(el_avg_H);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}




#undef __FUNCT__
#define __FUNCT__ "SwarmUpdateProperties_MPntPDarcy"
PetscErrorCode SwarmUpdateProperties_MPntPDarcy(DataBucket db,pTatinCtx user,Vec X)
{
  BTruth found;
  DM                dau,dap;
  Vec               Uloc,Ploc;
  Vec               u,p;
  PetscScalar       *LA_Uloc,*LA_Ploc;
  PetscErrorCode ierr;
  	
	PetscFunctionBegin;
		
    ierr = DMCompositeGetEntries(user->pack,&dau,&dap);CHKERRQ(ierr);
    ierr = DMCompositeGetLocalVectors(user->pack,&Uloc,&Ploc);CHKERRQ(ierr);
	ierr = DMCompositeScatter(user->pack,X,Uloc,Ploc);CHKERRQ(ierr);
	ierr = VecGetArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
	ierr = VecGetArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
	
  ierr = TkUpdatePropertyMarkerDarcy(user,dau,LA_Uloc,dap,LA_Ploc);CHKERRQ(ierr);
  ierr = VecRestoreArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
  ierr = VecRestoreArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
  ierr = DMCompositeRestoreLocalVectors(user->pack,&Uloc,&Ploc);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}




#undef __FUNCT__
#define __FUNCT__ "TkUpdatePropertyMarkerDarcy"
PetscErrorCode TkUpdatePropertyMarkerDarcy(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[])
{	
	PetscErrorCode ierr;
	int         pidx,n_mp_points;	
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	DataBucket  db;
	pTatinUnits *units;	
	PetscScalar gravity_acceleration;
	
	
// STOKES
	DataField   PField_std, PField_stokes;
    RheologyConstants *rheol;
	PetscInt nel,nen_u,nen_p;
	PetscInt i,j,k,e;
	PetscScalar GNIu[2][Q2_NODES_PER_EL_2D],NIp[P_BASIS_FUNCTIONS];
	PetscScalar NIu[U_BASIS_FUNCTIONS];
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	
// ENERGY
	PhysCompEnergyCtx phys_energy;
	Vec               T_vec,T_local_vec;
	DM                daT;
	PetscScalar       *LA_T;
    DataField         PField_thermal;
    PetscScalar       NIT[NODES_PER_EL_Q1_2D];
    const PetscInt    *elnidx_T;
    PetscInt          nen_T;
	PetscScalar       elT[NODES_PER_EL_Q1_2D];
	PetscBool         active_thermal_energy = PETSC_FALSE;    
	
// DARCY
	PhysCompDarcyCtx  phys_darcy;
	Vec               Pf_vec,Pf_local_vec;
	Vec               Pfold_vec,Pfold_local_vec;
	DM                daPf;
	PetscScalar       *LA_Pf,*LA_Pfold;
	DarcyConstants    *Dconst;
	DataField         PField_darcy;
	PetscScalar       NIPf[NODES_PER_EL_Q1_2D];
	const PetscInt    *elnidx_Pf;
	PetscInt          nen_Pf;
	PetscScalar       elPf[NODES_PER_EL_Q1_2D],elPfold[NODES_PER_EL_Q1_2D];
	
// LOGGING 
	PetscLogDouble t0,t1;
	PetscScalar min_source_g,max_source_g,min_diff_g,max_diff_g;
    PetscScalar	min_diff = 1.0e100;
	PetscScalar max_diff = 1.0e-100;
	PetscScalar min_source = 1.0e100;
	PetscScalar max_source = 1.0e-100;
	
	PetscContainer container;
	
	PetscFunctionBegin;
	
	units   = &user->units;	
	db = user->db;
	DataBucketGetSizes(db,&n_mp_points,0,0);
	gravity_acceleration = 0.0;
	ierr  = PetscOptionsGetReal(PETSC_NULL,"-grav",&gravity_acceleration,0);CHKERRQ(ierr);
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	/////////////////////* get physics */////////////////
	
// STOKES
    rheol  = &user->rheology_constants;
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
	

	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));
	
// DARCY
	{
	    Dconst      = &user->darcy_constants;
		phys_darcy  = user->phys_darcy;
		Pfold_vec   = phys_darcy->Pfold;
		daPf        = phys_darcy->daPf;
		
		ierr = DMGetLocalVector(daPf,&Pfold_local_vec);CHKERRQ(ierr);		
		ierr = DMGlobalToLocalBegin(daPf,Pfold_vec,INSERT_VALUES,Pfold_local_vec);CHKERRQ(ierr);
		ierr = DMGlobalToLocalEnd(  daPf,Pfold_vec,INSERT_VALUES,Pfold_local_vec);CHKERRQ(ierr);
		ierr = VecGetArray(Pfold_local_vec,&LA_Pfold);CHKERRQ(ierr);
		
		
		ierr = DMDAGetElementsQ1(daPf,&nel,&nen_Pf,&elnidx_Pf);CHKERRQ(ierr);
		
	    DataBucketGetDataFieldByName(db,MPntPDarcy_classname,&PField_darcy);    
	    DataFieldGetAccess(PField_darcy);
	    DataFieldVerifyAccess(PField_darcy,sizeof(MPntPDarcy));
	    
	}
	
// ENERGY	
	active_thermal_energy = PETSC_FALSE;
	ierr = pTatinPhysCompActivated(user,PhysComp_Energy,&active_thermal_energy);CHKERRQ(ierr);
	if (active_thermal_energy) {
		phys_energy = user->phys_energy;
		T_vec       = phys_energy->T;
		daT         = phys_energy->daT;
		
		ierr = DMGetLocalVector(daT,&T_local_vec);CHKERRQ(ierr);		
		ierr = DMGlobalToLocalBegin(daT,T_vec,INSERT_VALUES,T_local_vec);CHKERRQ(ierr);
		ierr = DMGlobalToLocalEnd(  daT,T_vec,INSERT_VALUES,T_local_vec);CHKERRQ(ierr);
		
		ierr = VecGetArray(T_local_vec,&LA_T);CHKERRQ(ierr);
		ierr = DMDAGetElementsQ1(daT,&nel,&nen_T,&elnidx_T);CHKERRQ(ierr);
		
	}
	
	
/*	if (active_thermo_dynamics) {
		phys_energy = user->phys_energy;
		T_vec       = phys_energy->T;
		daT         = phys_energy->daT;
		
		ierr = DMGetLocalVector(daT,&T_local_vec);CHKERRQ(ierr);		
		ierr = DMGlobalToLocalBegin(daT,T_vec,INSERT_VALUES,T_local_vec);CHKERRQ(ierr);
		ierr = DMGlobalToLocalEnd(  daT,T_vec,INSERT_VALUES,T_local_vec);CHKERRQ(ierr);
		
		ierr = VecGetArray(T_local_vec,&LA_T);CHKERRQ(ierr);
		ierr = DMDAGetElementsQ1(daT,&nel,&nen_T,&elnidx_T);CHKERRQ(ierr);
		DataBucketGetDataFieldByName(db,MPntPThermal_classname,&PField_thermal);
		DataFieldGetAccess(PField_thermal);
		DataFieldVerifyAccess(PField_thermal,sizeof(MPntPThermal));
	}
*/	
	
	PetscGetTime(&t0);
	
	
	
	
	/*
	 In this file, all properties marked with mp are scaled, other properties are in SI units.
	 */
// MARKER LOOP 
	
	for (pidx=0; pidx<n_mp_points; pidx++) {
		MPntStd       *material_point;
		MPntPStokes   *mpprop_stokes;
		MPntPDarcy    *mpprop_darcy;        
		MPntPThermal  *mpprop_thermal;
		double        *position, *xip;
		int           wil,phase_mp;
		char name[PETSC_MAX_PATH_LEN];
		PetscScalar J[2][2], iJ[2][2];
		PetscScalar J_p,ojp;
		PetscScalar  d1,exx_mp,eyy_mp,exy_mp,eta_mp,strainrateinv_mp;
		PetscScalar  sxx_mp,syy_mp,sxy_mp,u_mp,v_mp,trace_mp,pressure_mp;
		PetscScalar T_mp,P_mp,source,source_mp,diff_mp,diff,phi,fluid,eta_f_mp,P_hydro_mp,G_hydro_mp;
		PetscScalar phi_old,fluid_old,Pfold_mp,Pfold,P_hydro;
		PetscScalar T_eq, P_eq; 
		PetscReal sr, eta, pressure; 
				
		
		DataFieldAccessPoint(PField_std,    pidx,(void**)&material_point);
		MPntStdGetField_local_element_index(material_point,&wil);
		MPntStdGetField_global_coord(material_point,&position);
		MPntStdGetField_local_coord(material_point,&xip);
		MPntStdGetField_phase_index(material_point,&phase_mp);
		e = wil;
		
		
		DataFieldAccessPoint(PField_stokes, pidx,(void**)&mpprop_stokes);
				
		DataFieldAccessPoint(PField_darcy, pidx,(void**)&mpprop_darcy);
        

		/* get nodal properties for element e */
		/* coordinates*/
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		/* velocity*/
		ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],u);CHKERRQ(ierr);
		
		PTatinConstructNI_Q2_2D(xip,NIu);
		PTatinConstructGNI_Q2_2D(xip,GNIu);
		
		/* get element velocity/coordinates in component form */
		for (i=0; i<Q2_NODES_PER_EL_2D; i++) {
			ux[i] = elu[2*i  ];
			uy[i] = elu[2*i+1];
			
			xc[i] = elcoords[2*i  ];
			yc[i] = elcoords[2*i+1];
		}
		
		/* coord transformation */
		for (i=0; i<2; i++) {
			for (j=0; j<2; j++) { J[i][j] = 0.0; }
		}
		for (k=0; k<Q2_NODES_PER_EL_2D; k++) {
			J[0][0] += GNIu[0][k] * xc[k] ;
			J[0][1] += GNIu[0][k] * yc[k] ;
			
			J[1][0] += GNIu[1][k] * xc[k] ;
			J[1][1] += GNIu[1][k] * yc[k] ;
		}
		J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
		ojp = 1.0/J_p;
		iJ[0][0] =  J[1][1]*ojp;
		iJ[0][1] = -J[0][1]*ojp;
		iJ[1][0] = -J[1][0]*ojp;
		iJ[1][1] =  J[0][0]*ojp;
		
		/* global derivs at each quadrature point */
		for (k=0; k<Q2_NODES_PER_EL_2D; k++) {
			nx[k] = iJ[0][0]*GNIu[0][k] + iJ[0][1]*GNIu[1][k];
			ny[k] = iJ[1][0]*GNIu[0][k] + iJ[1][1]*GNIu[1][k];
		}
		
		/* strain rate (e= B u) at gp */
		exx_mp = eyy_mp = exy_mp = 0.0;
		for (k=0; k<U_BASIS_FUNCTIONS; k++) {
			exx_mp += nx[k] * ux[k];
			eyy_mp += ny[k] * uy[k];
			exy_mp += ny[k] * ux[k] + nx[k] * uy[k];
		}
		exy_mp = 0.5 * exy_mp;
		strainrateinv_mp = sqrt(0.25*(exx_mp-eyy_mp)*(exx_mp-eyy_mp)+exy_mp*exy_mp);
		UnitsApplyScaling(units->si_strainrate,strainrateinv_mp,&sr);
		if (sr < 1.0e-32) {
			sr = 1.0e-32;
		}
	
		/*pressure*/
		ierr = DMDAGetScalarElementField_2D(elp,nen_p,(PetscInt*)&elnidx_p[nen_p*e],p);CHKERRQ(ierr);
		ConstructNi_pressure(xip,elcoords,NIp);
		pressure_mp = 0.0;
		for (k=0; k<P_BASIS_FUNCTIONS; k++) {
			pressure_mp += NIp[k] * elp[k];
		}
		UnitsApplyScaling(units->si_stress,pressure_mp,&pressure);
			
		/* temperature */
		T_mp = 0; 
		if (active_thermal_energy) {
			ierr = DMDAGetScalarElementFieldQ1_2D(elT,(PetscInt*)&elnidx_T[nen_T*e],LA_T);CHKERRQ(ierr);
			ConstructNi_Q1_2D(xip,NIT);
			for (k=0; k<NODES_PER_EL_Q1_2D; k++) {
				T_mp = T_mp + NIT[k] * elT[k];
			}	
		}

		/* fluid pressure */
		Pfold_mp = 0.0;  
		ierr = DMDAGetScalarElementFieldQ1_2D(elPf,(PetscInt*)&elnidx_Pf[nen_Pf*e],LA_Pfold);CHKERRQ(ierr);
		ConstructNi_Q1_2D(xip,NIPf);
		for (k=0; k<NODES_PER_EL_Q1_2D; k++) {
			Pfold_mp = Pfold_mp + NIPf[k] * elPf[k];
		}
	    UnitsApplyScaling(units->si_stress,Pfold_mp,&Pfold);
	    UnitsApplyInverseScaling(units->si_force_per_volume,gravity_acceleration*Dconst->rho_H20,&G_hydro_mp);
	
		P_hydro_mp   =  -position[1]*G_hydro_mp;
		UnitsApplyScaling(units->si_stress,P_hydro_mp,&P_hydro);
		
		if (P_hydro < 0.0) P_hydro = 0.0;
		
		/* phase change */ 
		T_eq = T_mp; 
		P_eq = pressure_mp;
		
		/* viscosity */;
		switch (rheol->viscous_type[phase_mp]) {
				
			case VISCOUS_CONSTANT: {
				
				eta  = rheol->const_eta0[phase_mp];
			}
				break;
				
			case VISCOUS_FRANKK: {
				
				eta  = rheol->const_eta0[phase_mp]*exp(-rheol->temp_theta[phase_mp]*T_mp);
				
			}
				break;
				
			case VISCOUS_ARRHENIUS: {
				PetscScalar R       = 8.31440;
				PetscReal nexp      = rheol->arrh_nexp[phase_mp];
				PetscReal entalpy   = rheol->arrh_entalpy[phase_mp];
				PetscReal preexpA   = rheol->arrh_preexpA[phase_mp];
				PetscReal Vmol      = rheol->arrh_Vmol[phase_mp];
				PetscReal Tref      = rheol->arrh_Tref[phase_mp];
				PetscReal Ascale    = rheol->arrh_Ascale[phase_mp];
				PetscReal T_arrh    = T_mp + Tref ;
				
				entalpy = entalpy + pressure*Vmol;
				eta  = Ascale*0.25*pow(sr,1.0/nexp - 1.0)*pow(0.75*preexpA,-1.0/nexp)*exp(entalpy/(nexp*R*T_arrh));
				
			}
				break;
				
			case VISCOUS_POWERLAW:{
					PetscReal nexp      = rheol->arrh_nexp[phase_mp];
					PetscReal preexpA   = rheol->arrh_preexpA[phase_mp];			        
			        
					if (sr < 1.0e-32) {
						sr = 1.0e-32;
					}
			        
			        eta = preexpA*pow(sr,1.0/(nexp)-1.0); 
			        }
			    break;
			    
			 case VISCOUS_ARRHENIUS_MIXING:{		 
				PetscScalar R       = 8.31440;
				double position[2],preexpA,entalpy,nexp;
				char name[PETSC_MAX_PATH_LEN];
				TempMap map = PETSC_NULL;
				PetscReal Tref      = rheol->arrh_Tref[phase_mp];
				PetscReal Ascale    = rheol->arrh_Ascale[phase_mp];
				PetscReal T_arrh    = T_mp + Tref ;
		
				PetscReal Vmol      = rheol->arrh_Vmol[phase_mp];
                
				position[0] = T_eq;
				position[1] = P_eq; /// might be better to store a converged P for convergence
				
				sprintf(&name,"Amap_%d",phase_mp);
				ierr = pTatinCtxGetMap(user,&map,name);CHKERRQ(ierr);
				RheolMapGetDouble(map,position,&preexpA);
				
				sprintf(&name,"Qmap_%d",phase_mp);
				ierr = pTatinCtxGetMap(user,&map,name);CHKERRQ(ierr);
				RheolMapGetDouble(map,position,&entalpy);
				
				sprintf(&name,"nexpmap_%d",phase_mp);
				ierr = pTatinCtxGetMap(user,&map,name);CHKERRQ(ierr);
				RheolMapGetDouble(map,position,&nexp);
						
			
				entalpy = entalpy + pressure*Vmol;
				eta  = Ascale*0.25*pow(sr,1.0/nexp - 1.0)*pow(0.75*preexpA,-1.0/nexp)*exp(entalpy/(nexp*R*T_arrh));
			}
				break;   
	    
	
	
		}
     if (eta > rheol->eta_upper_cutoff_global) {
	       eta = rheol->eta_upper_cutoff_global;
	 }else if(eta < rheol->eta_lower_cutoff_global){
	       eta = rheol->eta_lower_cutoff_global;
	 }		
	// MPntPStokesGetField_eta_effective(mpprop_stokes,eta_mp);
	// UnitsApplyScaling(units->si_viscosity,eta_mp,&eta);
	/// carefull here ... eta_mp is not eta_arrhenius.... might be better to take the later one in the upper crust ! 
	// best would be to store eta, sr and P on the mpoint much less trouble
				    sprintf(&name,"fluidmap_%d",phase_mp);
                    ierr = PetscObjectQuery((PetscObject)user->model_data,name,(PetscObject*)&container);CHKERRQ(ierr);
					if (container) {
					TempMap map = PETSC_NULL;
					ierr = pTatinCtxGetMap(user,&map,name);
					position[0] = T_eq;
					position[1] = P_eq;
					TempMapGetDouble(map,position,&fluid);
				    } else {
				    fluid = 0.0;
				    }
				
				
				phi  = Dconst->phi0[phase_mp]*eta/Dconst->eta0*(1.0-Dconst->beta_rock*pressure);
				if (user->step == 0){
				phi_old = phi; 
				fluid_old = fluid; 
				}
				else{
				MPntPDarcyGetField_porosity(mpprop_darcy,&phi_old);
				MPntPDarcyGetField_rock_fluid(mpprop_darcy,&fluid_old);
				}
				source = -(phi-phi_old)+(fluid-fluid_old);
				/*
				if (Pfold + source / Dconst->beta_H20 < -P_hydro) {
				// fluid pressure might become negative if we do nothing
				// decreasing porosity  = pore collapse  (we could also decrease dt but it sucks) 
				source = -(Pfold+P_hydro) *  Dconst->beta_H20;
				phi    = -source + (fluid - fluid_old) + phi_old;
				} else if (Pfold + source / Dconst->beta_H20 > pressure) {
              	// fluid pressure might might be higher than rocks pressure if we do nothing
				// increasing porosity   =  hydrofracturing	
				source = -(Pfold-pressure) * Dconst->beta_H20;
				phi    = -source + (fluid - fluid_old) + phi_old;
				}
				*/
				
				 
				
				
				diff         = Dconst->perm0[phase_mp]*pow(phi/Dconst->phi0[phase_mp],2)*sr/Dconst->sr0;
				source_mp    = source/user->dt; // dt is scaled to model units already 
				
				UnitsApplyInverseScaling(units->si_viscosity,Dconst->vis_H20,&eta_f_mp);
				UnitsApplyInverseScaling(units->si_permeability,diff,&diff_mp);
				diff_mp  = diff_mp/eta_f_mp;
				
				{
				PetscScalar faultsink_mp;
				//UnitsApplyInverseScaling(units->si_strainrate,(sr-Dconst->sr0*10.0),&faultsink_mp);
				UnitsApplyInverseScaling(units->si_strainrate,(sr-Dconst->sr0),&faultsink_mp);
				if (faultsink_mp < 0.0) faultsink_mp = 0.0;
				source_mp = source_mp-faultsink_mp*1.0e-1;
				}
				/* community accepted version */ 	
				MPntPDarcySetField_source(mpprop_darcy,source_mp);//in volperunitvol/sec
				MPntPDarcySetField_coefficient_diffusion(mpprop_darcy,diff_mp);	
				MPntPDarcySetField_porosity(mpprop_darcy,phi);//in volperunitvol
				MPntPDarcySetField_rock_fluid(mpprop_darcy,fluid); // in volperunitvol
	
		
	    /*Apply cut off*/
	    
		/* monitor bounds */
		if (diff_mp > max_diff) { max_diff = diff_mp; }
		if (diff_mp < min_diff) { min_diff = diff_mp; }
		/* monitor bounds */
		if (source > max_source) { max_source = source; }
		if (source < min_source) { min_source = source; }
		
	}
	
	PetscGetTime(&t1);
    
#if 0
	ierr = MPI_Allreduce(&min_eta,&min_eta_g,1, MPI_DOUBLE, MPI_MIN, PETSC_COMM_WORLD);CHKERRQ(ierr);
	ierr = MPI_Allreduce(&max_eta,&max_eta_g,1, MPI_DOUBLE, MPI_MAX, PETSC_COMM_WORLD);CHKERRQ(ierr);
	
	ierr = MPI_Allreduce(&min_rho,&min_rho_g,1, MPI_DOUBLE, MPI_MIN, PETSC_COMM_WORLD);CHKERRQ(ierr);
	ierr = MPI_Allreduce(&max_rho,&max_rho_g,1, MPI_DOUBLE, MPI_MAX, PETSC_COMM_WORLD);CHKERRQ(ierr);
    
	PetscPrintf(PETSC_COMM_WORLD,"Update stokes non linearities (VPT) ND [mpoint]: (min,max)_eta %1.2e,%1.2e; log10(max/min) %1.2e; (min,max)_rho %1.2e,%1.2e; cpu time %1.2e (sec)\n",  min_eta_g, max_eta_g, log10(max_eta_g/min_eta_g),min_rho_g, max_rho_g, t1-t0 );
#endif
    
	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokes);
	DataFieldRestoreAccess(PField_darcy);
	ierr = VecRestoreArray(Pfold_local_vec,&LA_Pfold);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(daPf,&Pfold_local_vec);CHKERRQ(ierr);
	if (active_thermal_energy) {
		
		ierr = VecRestoreArray(T_local_vec,&LA_T);CHKERRQ(ierr);
		ierr = DMRestoreLocalVector(daT,&T_local_vec);CHKERRQ(ierr);		
	}
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}
		




#undef __FUNCT__
#define __FUNCT__ "_SwarmUpdateGaussPropertiesLocalL2ProjectionQ1_MPntPDarcy"
PetscErrorCode _SwarmUpdateGaussPropertiesLocalL2ProjectionQ1_MPntPDarcy(
					DM clone,Vec properties_A1,Vec properties_A2,Vec properties_B,
					const int npoints,MPntStd mp_std[],MPntPDarcy mp_darcy[],QuadratureVolumeDarcy Q) 
{
	PetscScalar Ni_p[NODES_PER_EL_Q1_2D];
	PetscScalar Ae1[NODES_PER_EL_Q1_2D], Ae2[NODES_PER_EL_Q1_2D], Be[NODES_PER_EL_Q1_2D];
	PetscInt el_lidx[NODES_PER_EL_Q1_2D];
	Vec Lproperties_A1, Lproperties_A2, Lproperties_B;
	PetscScalar *LA_properties_A1, *LA_properties_A2, *LA_properties_B;
	PetscLogDouble t0,t1;
	PetscInt p,i;
	PetscInt nel,nen,e;
	const PetscInt *elnidx;
	
	PetscInt ngp;
	PetscScalar *xi_mp;
	PetscScalar NIu[MAX_QUAD_PNTS][U_BASIS_FUNCTIONS];
	CoefficientsDarcyEq *quadraturepoints;
	PetscErrorCode ierr;
	
	
	PetscFunctionBegin;
	
	
	ierr = DMGetLocalVector(clone,&Lproperties_A1);CHKERRQ(ierr);		ierr = VecZeroEntries(Lproperties_A1);CHKERRQ(ierr);
	ierr = DMGetLocalVector(clone,&Lproperties_A2);CHKERRQ(ierr);		ierr = VecZeroEntries(Lproperties_A2);CHKERRQ(ierr);
	ierr = DMGetLocalVector(clone,&Lproperties_B);CHKERRQ(ierr);		ierr = VecZeroEntries(Lproperties_B);CHKERRQ(ierr);
	
	ierr = VecGetArray(Lproperties_A1,&LA_properties_A1);CHKERRQ(ierr);
	ierr = VecGetArray(Lproperties_A2,&LA_properties_A2);CHKERRQ(ierr);
	ierr = VecGetArray(Lproperties_B, &LA_properties_B);CHKERRQ(ierr);
	
	ierr = DMDAGetElementsQ1(clone,&nel,&nen,&elnidx);CHKERRQ(ierr);
	
	PetscGetTime(&t0);
	for (p=0; p<npoints; p++) {
		double *xi_p  = &mp_std[p].xi[0];
		double kappa_p = mp_darcy[p].diff;
		double H_p     = mp_darcy[p].source+mp_darcy[p].pore;
		
		ierr = PetscMemzero(Ae1,sizeof(PetscScalar)*NODES_PER_EL_Q1_2D);CHKERRQ(ierr);
		ierr = PetscMemzero(Ae2,sizeof(PetscScalar)*NODES_PER_EL_Q1_2D);CHKERRQ(ierr);
		ierr = PetscMemzero(Be, sizeof(PetscScalar)*NODES_PER_EL_Q1_2D);CHKERRQ(ierr);
		
		ConstructNi_Q1_2D(xi_p,Ni_p);
		
		for (i=0; i<NODES_PER_EL_Q1_2D; i++) {
			Ae1[i] = Ni_p[i] * kappa_p;
			Ae2[i] = Ni_p[i] * H_p;
			Be[i]  = Ni_p[i];
		}
	
		
		/* sum into local vectors */
		e = mp_std[p].wil;
		ierr = GetElementLocalIndicesQ1(el_lidx,(PetscInt*)&elnidx[nen*e]);CHKERRQ(ierr);
		
		ierr = DMDASetValuesLocalStencil_AddValues_Scalar_Q1(LA_properties_A1, el_lidx,Ae1);CHKERRQ(ierr);
		ierr = DMDASetValuesLocalStencil_AddValues_Scalar_Q1(LA_properties_A2, el_lidx,Ae2);CHKERRQ(ierr);
		ierr = DMDASetValuesLocalStencil_AddValues_Scalar_Q1(LA_properties_B,  el_lidx,Be);CHKERRQ(ierr);
		
	}
	PetscGetTime(&t1);
	//	PetscPrintf(PETSC_COMM_WORLD,"  [ L2 projectionQ1 (summation): %1.4lf ]\n",t1-t0);
	
  ierr = VecRestoreArray(Lproperties_B,&LA_properties_B);CHKERRQ(ierr);
  ierr = VecRestoreArray(Lproperties_A2,&LA_properties_A2);CHKERRQ(ierr);
  ierr = VecRestoreArray(Lproperties_A1,&LA_properties_A1);CHKERRQ(ierr);
	
	
	/* scatter to quadrature points */
	ierr = DMLocalToGlobalBegin(clone,Lproperties_A1,ADD_VALUES,properties_A1);CHKERRQ(ierr);
	ierr = DMLocalToGlobalEnd(  clone,Lproperties_A1,ADD_VALUES,properties_A1);CHKERRQ(ierr);
	
	ierr = DMLocalToGlobalBegin(clone,Lproperties_A2,ADD_VALUES,properties_A2);CHKERRQ(ierr);
	ierr = DMLocalToGlobalEnd(  clone,Lproperties_A2,ADD_VALUES,properties_A2);CHKERRQ(ierr);
	
	ierr = DMLocalToGlobalBegin(clone,Lproperties_B,ADD_VALUES,properties_B);CHKERRQ(ierr);
	ierr = DMLocalToGlobalEnd(  clone,Lproperties_B,ADD_VALUES,properties_B);CHKERRQ(ierr);
	
	/* scale */
	ierr = VecPointwiseDivide( properties_A1, properties_A1, properties_B );CHKERRQ(ierr);
	ierr = VecPointwiseDivide( properties_A2, properties_A2, properties_B );CHKERRQ(ierr);
	/* ========================================= */
	
	/* scatter result back to local array and do the interpolation onto the quadrature points */
	ngp       = Q->ngp;
	xi_mp     = Q->xi;
	for (p=0; p<ngp; p++) {
		PetscScalar *xip = &xi_mp[2*p];
		
//		ierr = PetscMemzero(NIu[p], sizeof(PetscScalar)*NODES_PER_EL_Q1_2D);CHKERRQ(ierr);
		ConstructNi_Q1_2D(xip,NIu[p]);
	}
	
	
	PetscGetTime(&t0);
	ierr = VecZeroEntries(Lproperties_A1);CHKERRQ(ierr);
	ierr = VecZeroEntries(Lproperties_A2);CHKERRQ(ierr);
	
	ierr = DMGlobalToLocalBegin(clone,properties_A1,INSERT_VALUES,Lproperties_A1);CHKERRQ(ierr);
	ierr = DMGlobalToLocalEnd(  clone,properties_A1,INSERT_VALUES,Lproperties_A1);CHKERRQ(ierr);
	
	ierr = DMGlobalToLocalBegin(clone,properties_A2,INSERT_VALUES,Lproperties_A2);CHKERRQ(ierr);
	ierr = DMGlobalToLocalEnd(  clone,properties_A2,INSERT_VALUES,Lproperties_A2);CHKERRQ(ierr);
	PetscGetTime(&t1);
	//	PetscPrintf(PETSC_COMM_WORLD,"  [ L2 projectionQ1 (scatter): %1.4lf ]\n",t1-t0);
	
	PetscGetTime(&t0);
	ierr = VecGetArray(Lproperties_A1,&LA_properties_A1);CHKERRQ(ierr);
	ierr = VecGetArray(Lproperties_A2,&LA_properties_A2);CHKERRQ(ierr);
	
	/* traverse elements and interpolate */
	for (e=0;e<nel;e++) {
		ierr = QuadratureVolumeDarcyGetCell(Q,e,&quadraturepoints);CHKERRQ(ierr);
		
		ierr = GetElementLocalIndicesQ1(el_lidx,(PetscInt*)&elnidx[nen*e]);CHKERRQ(ierr);
		
		ierr = DMDAGetScalarElementFieldQ1_2D(Ae1,(PetscInt*)&elnidx[nen*e],LA_properties_A1);CHKERRQ(ierr);
		ierr = DMDAGetScalarElementFieldQ1_2D(Ae2,(PetscInt*)&elnidx[nen*e],LA_properties_A2);CHKERRQ(ierr);
		
		/* zero out the junk */
		
		for (p=0; p<ngp; p++) {
			quadraturepoints[p].diff = 0.0;
			quadraturepoints[p].source = 0.0;
			for (i=0; i<NODES_PER_EL_Q1_2D; i++) {
				quadraturepoints[p].diff   += NIu[p][i] * Ae1[i];
				quadraturepoints[p].source += NIu[p][i] * Ae2[i];
			}
		}
	}
	
  ierr = VecRestoreArray(Lproperties_A2,&LA_properties_A2);CHKERRQ(ierr);
  ierr = VecRestoreArray(Lproperties_A1,&LA_properties_A1);CHKERRQ(ierr);
	
	PetscGetTime(&t1);
	//	PetscPrintf(PETSC_COMM_WORLD,"  [ L2 projectionQ1 (interpolation): %1.4lf ]\n",t1-t0);
	
	ierr = DMRestoreLocalVector(clone,&Lproperties_B);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(clone,&Lproperties_A2);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(clone,&Lproperties_A1);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SwarmUpdateQuadraturePropertiesLocalL2Projection_Q1_MPntPDarcy"
PetscErrorCode SwarmUpdateQuadraturePropertiesLocalL2Projection_Q1_MPntPDarcy(const int npoints,MPntStd mp_std[],MPntPDarcy mp_darcy[],DM da,QuadratureVolumeDarcy Q)
{
	PetscInt  dof;
	DM        clone;
	Vec       properties_A1, properties_A2, properties_B;
	PetscBool view;
	DMDAE     dmdae;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	/* check we have a valid Q1 DMDA passed in here */
	ierr = DMGetDMDAE(da,&dmdae);CHKERRQ(ierr);
	
	/* setup */
	clone  = da;
	ierr = DMGetGlobalVector(clone,&properties_A1);CHKERRQ(ierr);  ierr = PetscObjectSetName( (PetscObject)properties_A1, "LocalL2ProjQ1_kappa");CHKERRQ(ierr);
	ierr = DMGetGlobalVector(clone,&properties_A2);CHKERRQ(ierr);  ierr = PetscObjectSetName( (PetscObject)properties_A2, "LocalL2ProjQ1_H");CHKERRQ(ierr);
	ierr = DMGetGlobalVector(clone,&properties_B);CHKERRQ(ierr);
	
	ierr = VecZeroEntries(properties_A1);CHKERRQ(ierr);
	ierr = VecZeroEntries(properties_A2);CHKERRQ(ierr);
	ierr = VecZeroEntries(properties_B);CHKERRQ(ierr);
	
	/* compute */
	ierr = _SwarmUpdateGaussPropertiesLocalL2ProjectionQ1_MPntPDarcy(
																																		da, properties_A1,properties_A2,properties_B, 																																	 
																																		npoints, mp_std,mp_darcy, Q );CHKERRQ(ierr);
	
	/* view */
	view = PETSC_FALSE;
	PetscOptionsGetBool(PETSC_NULL,"-view_projected_marker_fields_Darcy",&view,PETSC_NULL);
	if (view) {
		PetscViewer viewer;
		
		ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, "SwarmUpdateProperties_LocalL2Proj_PDarcy.vtk", &viewer);CHKERRQ(ierr);
		ierr = PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
		ierr = DMView(clone, viewer);CHKERRQ(ierr);
		ierr = VecView(properties_A1, viewer);CHKERRQ(ierr);
		ierr = VecView(properties_A2, viewer);CHKERRQ(ierr);
		ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
	}
	
	/* destroy */
	ierr = DMRestoreGlobalVector(clone,&properties_B);CHKERRQ(ierr);
	ierr = DMRestoreGlobalVector(clone,&properties_A2);CHKERRQ(ierr);
	ierr = DMRestoreGlobalVector(clone,&properties_A1);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}



