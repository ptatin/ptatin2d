

#include "stdio.h"
#include "stdlib.h"

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscdm.h"

#include "daVMS_Q1.h"
#include "pTatin2d.h"

/*
 
 grad[w].grad[v] = (dw/dx,dw/dy).(dv/dx,dv/dy) = DNx[i].dphi_dx + DNy[i].dphi_dy
 a.grad[w] = (a[0].DNx[i] + a[1].[DNy[i]))
 a.grad[v] = (a[0].dphi_dx + a[1].dphi_dy)
 
 */

extern PetscReal THETA; /* 1 = fully implicit */

extern PetscErrorCode DMDACreateSUPGWorkVectors(DM dm,Vec *V,Vec *phi);
extern PetscErrorCode _DMDACreateSUPG(DM da,DMDASUPGParameters *supg);
extern PetscErrorCode DMDACreateSUPG2d(MPI_Comm comm,PetscInt Mx,PetscInt My,DM *da);
extern PetscErrorCode DMDACreateSUPG2dFromDM(DM dacoarse,PetscInt Refx,PetscInt Refy,DM *da);
extern PetscErrorCode DMDADestroySUPG(DM *da);
extern PetscErrorCode DASUPGGetParameters(DM da,DMDASUPGParameters *p);
extern PetscErrorCode DASUPGView(DM da,Vec kappa,Vec V,Vec phi,PetscInt step);
extern void ConstructGaussQuadrature_2x2_2d(PetscInt *ngp,PetscScalar gp_xi[][2],PetscScalar gp_weight[]);
extern PetscErrorCode DMDAGetLocalElementSize_Q1(DM da,PetscInt *mxl,PetscInt *myl,PetscInt *mzl);
extern PetscErrorCode DMDAGetElementCorners_Q1(DM da,
																						PetscInt *sx,PetscInt *sy,PetscInt *sz,
																				PetscInt *mx,PetscInt *my,PetscInt *mz);
extern PetscErrorCode GetElementCoords_Q1(DMDACoor2d **_coords,PetscInt ei,PetscInt ej,PetscScalar el_coords[]);
extern PetscErrorCode DMDAGetElementValues_Q1_scalar(PetscScalar **data,PetscInt i,PetscInt j,PetscScalar el_data[]);
extern PetscErrorCode DMDAGetElementValues_Q1_vector(PetscScalar ***data,PetscInt i,PetscInt j,PetscScalar el_data[]);
extern PetscErrorCode GetElementEqnIndicesQ1_scalar(PetscInt ei,PetscInt ej,PetscInt nxg, PetscInt globalindices[],PetscInt eldofidx[]);
extern void ConstructQ12D_Ni(PetscScalar _xi[],PetscScalar Ni[]);
extern void ConstructQ12D_GNi(PetscScalar _xi[],PetscScalar GNi[][NODES_PER_EL_Q1]);
extern void ConstructQ12D_GNx(PetscScalar GNi[][NODES_PER_EL_Q1],PetscScalar GNx[][NODES_PER_EL_Q1],PetscScalar coords[],PetscScalar *det_J);
extern void ConstructQ12D_ElementTransformation(PetscScalar coords[],PetscScalar GNi[][NODES_PER_EL_Q1],PetscScalar _J[][2],PetscScalar _invJ[][2]);
extern PetscErrorCode DMDASetValuesLocalStencil_Q1_ADD_VALUES(PetscScalar **data,MatStencil eqn[],PetscScalar Re[]);
extern PetscErrorCode DMDAGetElementEqnums_Q1_scalar(PetscInt i,PetscInt j,MatStencil s_p[]);


/* VMS business */

void ConstructVMS2D_b1_gradient_b1(PetscScalar _xi[],PetscScalar *b1,PetscScalar grad_b1[])
{
  PetscScalar xi  = _xi[0];
  PetscScalar eta = _xi[1];
	
  *b1      = (1.0-xi*xi)*(1.0-eta*eta);

  grad_b1[0] = (-2.0*xi)*(1.0-eta*eta);
  grad_b1[1] = (1.0-xi*xi)*(-2.0*eta);
}

void eval_tri(PetscScalar _xb[],PetscScalar triangle[][2],PetscInt *inside)
{
	PetscScalar sum;

	sum = 0.0;
	
	
	*inside = 0;
	if ( (sum>=0.0) && (sum<=1.0) ) {
		*inside = 1;
	}
}

void _ConstructVMS2D_b2_gradient_b2(PetscScalar _xi[],PetscScalar _xb[],PetscScalar *b2,PetscScalar grad_b2[])
{
  PetscScalar xi  = _xi[0];
  PetscScalar eta = _xi[1];
  PetscScalar Xb = _xb[0];
  PetscScalar Yb = _xb[1];
	PetscScalar triangle[4][3][2];
	PetscInt i,triangle_index;
	
	triangle[0][0][0] =  Xb;		triangle[0][0][1] =  Yb;
	triangle[0][1][0] =  1.0;		triangle[0][1][1] = -1.0;
	triangle[0][2][0] = -1.0;		triangle[0][2][2] = -1.0;

	triangle[1][0][0] = Xb;			triangle[1][0][1] =  Yb;
	triangle[1][1][0] = 1.0;		triangle[1][1][1] =  1.0;
	triangle[1][2][0] = 1.0;		triangle[1][2][2] = -1.0;
	
	triangle[2][0][0] =  Xb;		triangle[2][0][1] =  Yb;
	triangle[2][1][0] = -1.0;		triangle[2][1][1] =  1.0;
	triangle[2][2][0] =  1.0;		triangle[2][2][2] =  1.0;
	
	triangle[3][0][0] =  Xb;		triangle[3][0][1] =  Yb;
	triangle[3][1][0] = -1.0;		triangle[3][1][1] = -1.0;
	triangle[3][2][0] = -1.0;		triangle[3][2][2] =  1.0;
	
	for (i=0; i<4; i++) {
		PetscInt inside;
		
		eval_tri(_xi,triangle[i],&inside);
		if (inside==1) {
			triangle_index = i;
			break;
		}
	}
	
	switch (triangle_index) {
		case 0:
			*b2 = xi*(1.0+eta) / (Xb*(1.0+Yb));
			grad_b2[0] = (1.0+eta) / (Xb*(1.0+Yb));
			grad_b2[1] = xi / (Xb*(1.0+Yb));
			break;
		case 1:
			*b2 = eta*(1.0-xi) / (Yb*(1.0-Xb));
			grad_b2[0] = -eta / (Yb*(1.0-Xb));
			grad_b2[1] = (1.0-xi) / (Yb*(1.0-Xb));
			break;
		case 2:
			*b2 = xi*(1.0-eta) / (Xb*(1.0-Yb));
			grad_b2[0] = (1.0-eta) / (Xb*(1.0-Yb));
			grad_b2[1] = -xi / (Xb*(1.0-Yb));
			break;
		case 3:
			*b2 = eta*(1.0+xi) / (Yb*(1.0+Xb));
			grad_b2[0] = eta / (Yb*(1.0+Xb));
			grad_b2[1] = (1.0+xi) / (Yb*(1.0+Xb));
			break;

		default:
			printf("ERROR: Could not locate point in triangle \n");
			break;
	}
	
	
}


/* compute most upwinded node */
void ConstructVMS2D_b2_gradient_b2(PetscScalar _xi[],PetscScalar el_V[],PetscScalar *b2,PetscScalar grad_b2[])
{
  PetscScalar xi  = _xi[0];
  PetscScalar eta = _xi[1];
	PetscScalar Vc[2],Vc_mag;
	PetscScalar sep,sep_max;
	PetscInt i,node_index;
	PetscScalar quad[4][2];
	
	Vc[0] = Vc[1] = 0.0;
	for (i=0; i<NODES_PER_EL_Q1; i++) {
		Vc[0] += el_V[2*i+0];
		Vc[1] += el_V[2*i+1];
	}
	Vc[0] = 0.25 * Vc[0];
	Vc[1] = 0.25 * Vc[1];
	
	Vc_mag = sqrt( Vc[0]*Vc[0] + Vc[1]*Vc[1] );
	//printf("Vc = %1.4e \n", Vc_mag);
	
	if (Vc_mag>0.0) {
		Vc[0] = Vc[0]/Vc_mag;
		Vc[1] = Vc[1]/Vc_mag;
	}
	
	//Xc[0] = 0.0 + Vc[0] * dt(=1.0)
	
	/* compute dist between end point of normalized displacement and all nodes */
	
	quad[0][0] = -1.0;		quad[0][1] = -1.0;
	quad[1][0] = -1.0;		quad[1][1] =  1.0;
	quad[2][0] =  1.0;		quad[2][1] =  1.0;
	quad[3][0] =  1.0;		quad[3][1] = -1.0;
	
	sep_max = -1.0e32;
	node_index = -1;
	for (i=0; i<NODES_PER_EL_Q1; i++) {
		sep = (Vc[0]-quad[i][0])*(Vc[0]-quad[i][0]) + (Vc[1]-quad[i][1])*(Vc[1]-quad[i][1]);
		//printf("i %d: sep - %1.4e \n", i, sep );
		if (sep > sep_max) {
			sep_max = sep;
			node_index = i;
		}
	}
	
	switch (node_index) {
		case 0:
			*b2 = 0.25*(1.0-xi)*(1.0-eta);
			grad_b2[0] = -0.25*(1.0-eta);
			grad_b2[1] = -0.25*(1.0-xi);
			break;

		case 1:
			*b2 = 0.25*(1.0-xi)*(1.0+eta);
			grad_b2[0] = -0.25*(1.0+eta);
			grad_b2[1] =  0.25*(1.0-xi);
			break;

		case 2:
			*b2 = 0.25*(1.0+xi)*(1.0+eta);
			grad_b2[0] =  0.25*(1.0+eta);
			grad_b2[1] =  0.25*(1.0+xi);
			break;

		case 3:
			*b2 = 0.25*(1.0+xi)*(1.0-eta);
			grad_b2[0] =  0.25*(1.0-eta);
			grad_b2[1] = -0.25*(1.0+xi);
			break;

		default:
			printf("ERROR: Could not identify upwinded node \n");
			exit(0);
			break;
	}
/*
	if (Vc_mag<1.0e-32) {
		*b2 = 0.0;
		grad_b2[0] = 0.0;
		grad_b2[1] = 0.0;
	}
*/	
}



/*
 
tau = b1 [ \int b2 dV ] / [ \int b2 * a \dot grad b1 dV + \int grad b2 \kappa grad b1 dV ]
 
*/
void VMS2d_Compute_tau_const( PetscScalar el_coords[],PetscScalar el_kappa[],PetscScalar el_V[],PetscScalar *tau )
{
  PetscInt    ngp;
  PetscScalar gp_xi[GAUSS_POINTS_2x2][2];
  PetscScalar gp_weight[GAUSS_POINTS_2x2];
  PetscInt    p,i,j;
  PetscScalar Ni_p[NODES_PER_EL_Q1];
  PetscScalar GNi_p[NSD][NODES_PER_EL_Q1],GNx_p[NSD][NODES_PER_EL_Q1];
  PetscScalar J_p;
	PetscScalar b1_p,grad_b1_p[2];
	PetscScalar b2_p,grad_b2_p[2];
  PetscScalar kappa_p,vx_p,vy_p;
	PetscScalar b1_sum,b2_sum,inv_sum,inv_sum_adv,inv_sum_diff;
	
		
  /* define quadrature rule */
  ConstructGaussQuadrature_2x2_2d(&ngp,gp_xi,gp_weight);
	
	b1_sum  = 0.0;
	b2_sum  = 0.0;
	inv_sum_adv = inv_sum_diff = 0.0;
	
  /* evaluate integral */
  for (p = 0; p < ngp; p++) {
    ConstructQ12D_Ni(gp_xi[p],Ni_p);
    ConstructQ12D_GNi(gp_xi[p],GNi_p);
    ConstructQ12D_GNx(GNi_p,GNx_p,el_coords,&J_p);
		
    kappa_p = 0.0;
    vx_p    = vy_p = 0.0;
    for (j=0; j<NODES_PER_EL_Q1; j++) {
      kappa_p += Ni_p[j] * el_kappa[j];
      vx_p    += Ni_p[j] * el_V[2*j+0];      /* compute vx on the particle */
      vy_p    += Ni_p[j] * el_V[2*j+1];      /* compute vy on the particle */
    }
		vx_p = fabs(vx_p);
		vy_p = fabs(vy_p);
		
		ConstructVMS2D_b1_gradient_b1(gp_xi[p],&b1_p,grad_b1_p);
		ConstructVMS2D_b2_gradient_b2(gp_xi[p],el_V,&b2_p,grad_b2_p);
		
		b1_sum = b1_sum + gp_weight[p] * b1_p * J_p;
		b2_sum = b2_sum + gp_weight[p] * b2_p * J_p;

		inv_sum_adv = inv_sum_adv + gp_weight[p] * ( 
										  b2_p * ( vx_p * grad_b1_p[0] + vy_p * grad_b1_p[1]  )  
																				) * J_p;

		inv_sum_diff = inv_sum_diff + gp_weight[p] * ( 
										+ grad_b1_p[0] * kappa_p * grad_b1_p[0]
										+ grad_b1_p[1] * kappa_p * grad_b1_p[1]
																				) * J_p;
		
	}
	inv_sum = fabs(inv_sum_adv) + fabs(inv_sum_diff);
/*	
	{
		double h = 1.0/30.0;
		double kappa = 1.0e-6;
		double vxa = 0.25*( el_V[2*0+0]+el_V[2*1+0]+el_V[2*2+0]+el_V[2*3+0] );
		double vya = 0.25*( el_V[2*0+1]+el_V[2*1+1]+el_V[2*2+1]+el_V[2*3+1] );
		double vxm = sqrt(vxa*vxa + vya*vya);
		double diff,adv;
		diff = kappa/(h*h);
		adv = vxm/h;
		printf("diff = %1.4e ; adv = %1.4e \n", diff,adv );
	}
*/	
//	ConstructVMS2D_b1_gradient_b1(XI,&b1_p,grad_b1);
	if (fabs(inv_sum)>1.0e-32) {
		//*tau = b1_sum * inv_sum * 1.0e3;
		*tau = 1.0e-4 * b1_sum / inv_sum;
		//*tau = b1_sum / inv_sum * 1.0;
		//*tau = b2_sum / inv_sum;
	} else {
		*tau = 0.0;
	}

//	if (inv_sum_adv < 1.0e-32) {
//		*tau = 0.0;
//	}
	
//	*tau = 0.0;
//	
//
//	printf("tau_adv = %1.4e : tau_diff = %1.4e \n", inv_sum_adv,inv_sum_diff );
//	printf("tau = %1.4e : b1 = %1.4e : b2 = %1.4e : tau_adv = %1.4e : tau_diff = %1.4e\n", *tau,b1_sum,b2_sum,inv_sum_adv,inv_sum_diff );
//	
}




void AElement_VMS2d( PetscScalar Re[],PetscReal dt,
														PetscScalar el_coords[],PetscScalar el_kappa[],PetscScalar el_V[])
{
  PetscInt    ngp;
  PetscScalar gp_xi[GAUSS_POINTS_2x2][2];
  PetscScalar gp_weight[GAUSS_POINTS_2x2];
  PetscInt    p,i,j;
  PetscScalar Ni_p[NODES_PER_EL_Q1];
  PetscScalar GNi_p[NSD][NODES_PER_EL_Q1],GNx_p[NSD][NODES_PER_EL_Q1];
  PetscScalar J_p,fac;
  PetscScalar kappa_p,v_p[2];
	PetscScalar tau_constant,tau;
	PetscScalar b1_p,grad_b1_p[2];
	
  /* define quadrature rule */
  ConstructGaussQuadrature_2x2_2d(&ngp,gp_xi,gp_weight);
	
	
	VMS2d_Compute_tau_const( el_coords,el_kappa,el_V,&tau_constant );
	
	
  /* evaluate integral */
  for (p = 0; p < ngp; p++) {
    ConstructQ12D_Ni(gp_xi[p],Ni_p);
    ConstructQ12D_GNi(gp_xi[p],GNi_p);
    ConstructQ12D_GNx(GNi_p,GNx_p,el_coords,&J_p);
		
    fac = gp_weight[p]*J_p;
		
    kappa_p      = 0.0;
    v_p[0]       = v_p[1]       = 0.0;
    for (j=0; j<NODES_PER_EL_Q1; j++) {
      kappa_p    += Ni_p[j] * el_kappa[j];
      v_p[0]     += Ni_p[j] * el_V[NSD*j+0];      /* compute vx on the particle */
      v_p[1]     += Ni_p[j] * el_V[NSD*j+1];      /* compute vy on the particle */
    }
		
		ConstructVMS2D_b1_gradient_b1(gp_xi[p],&b1_p,grad_b1_p);
		tau = ( b1_p * tau_constant );
		//printf("tau = %1.4e \n", tau );
		
		for (i=0; i<NODES_PER_EL_Q1; i++) {
			for (j=0; j<NODES_PER_EL_Q1; j++) {
				Re[j+i*NODES_PER_EL_Q1] += fac * ( 
									+ THETA * dt * Ni_p[i] * ( v_p[0] * GNx_p[0][j] + v_p[1] * GNx_p[1][j] ) /* ( w, \vec a \dot grad[v] ) */
									+ THETA * dt * kappa_p * ( GNx_p[0][i] * GNx_p[0][j] + GNx_p[1][i] * GNx_p[1][j] ) /* ( grad[w], \kappa grad[v] ) */
								  + THETA * dt * ( (v_p[0]*GNx_p[0][i] + v_p[1]*GNx_p[1][i]) * tau * (v_p[0]*GNx_p[0][j] + v_p[1]*GNx_p[1][j])  )
									+ Ni_p[i] * Ni_p[j]
				);
				
			}
		}
  }
}


#undef __FUNCT__  
#define __FUNCT__ "VMSFormJacobian"
PetscErrorCode VMSFormJacobian(PetscReal time,Vec X,PetscReal dt,Mat *A,Mat *B,MatStructure *mstr,void *ctx)
{
  DMDASUPGParameters data = (DMDASUPGParameters)ctx;
  DM             da,cda;
	Vec coords;
  DMDACoor2d             **LA_coords;
	/* quadrature */
  PetscInt    ngp;
  PetscScalar gp_xi[GAUSS_POINTS_2x2][2];
  PetscScalar gp_weight[GAUSS_POINTS_2x2];
  PetscScalar J_p,fac;
	/* loops */
  PetscInt    p,i,j;
	/* basis functions */
  PetscScalar Ni_p[NODES_PER_EL_Q1];
  PetscScalar GNi_p[NSD][NODES_PER_EL_Q1],GNx_p[NSD][NODES_PER_EL_Q1];
	PetscScalar ADe[NODES_PER_EL_Q1*NODES_PER_EL_Q1],el_coords[NSD*NODES_PER_EL_Q1],el_kappa[NODES_PER_EL_Q1],el_V[NODES_PER_EL_Q1*NSD];
	/**/
  PetscInt               sex,sey,mx,my;
  PetscInt               ei,ej;
	BCList bclist;
	PetscInt nxg,Len_local, *gidx_bclocal;
	PetscInt *gidx,elgidx[NODES_PER_EL_Q1],elgidx_bc[NODES_PER_EL_Q1];
	Vec                    kappa,V;
  Vec                    local_kappa,local_V;
  PetscScalar            **LA_kappa, ***LA_V;
	PetscErrorCode ierr;
	
	
  PetscFunctionBegin;
	da     = data->daT;
	kappa  = data->kappa;
	V      = data->V;
	bclist = data->phi_bclist;
	
	/* trash old entries */
  ierr = MatZeroEntries(*B);CHKERRQ(ierr);
	
  /* define quadrature rule */
  ConstructGaussQuadrature_2x2_2d(&ngp,gp_xi,gp_weight);
	
	
  /* setup for coords */
  ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
  ierr = DMDAGetGhostedCoordinates(da,&coords);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(cda,coords,&LA_coords);CHKERRQ(ierr);
	
  /* get acces to the vector kappa */
  ierr = DMGetLocalVector(da,&local_kappa);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(da,kappa,INSERT_VALUES,local_kappa);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(  da,kappa,INSERT_VALUES,local_kappa);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da,local_kappa,&LA_kappa);CHKERRQ(ierr);
	
  /* get acces to the vector V */
  ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
  ierr = DMGetLocalVector(cda,&local_V);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(cda,V,INSERT_VALUES,local_V);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(  cda,V,INSERT_VALUES,local_V);CHKERRQ(ierr);
  ierr = DMDAVecGetArrayDOF(cda,local_V,&LA_V);CHKERRQ(ierr);
	
	
	/* stuff for eqnums */
	ierr = DMDAGetGlobalIndices(da,0,&gidx);CHKERRQ(ierr);
	ierr = BCListGetDofIdx(bclist,0,0,&Len_local,&gidx_bclocal);CHKERRQ(ierr);
	
  ierr = DMDAGetGhostCorners(da,0,0,0,&nxg,0,0);CHKERRQ(ierr);
  ierr = DMDAGetElementCorners_Q1(da,&sex,&sey,0,&mx,&my,0);CHKERRQ(ierr);
  for (ej = sey; ej < sey+my; ej++) {
    for (ei = sex; ei < sex+mx; ei++) {
      /* get coords for the element */
      GetElementCoords_Q1(LA_coords,ei,ej,el_coords);
      /* get coefficients for the element */
      DMDAGetElementValues_Q1_scalar(LA_kappa,ei,ej,el_kappa);
      DMDAGetElementValues_Q1_vector(LA_V,ei,ej,el_V);
			
      /* initialise element stiffness matrix */
      ierr = PetscMemzero(ADe,sizeof(PetscScalar)*NODES_PER_EL_Q1*NODES_PER_EL_Q1);CHKERRQ(ierr);
			
      /* form element stiffness matrix */
			AElement_VMS2d( ADe,dt,
											 el_coords, el_kappa, el_V);
			
      /* insert element matrix into global matrix */
			ierr = GetElementEqnIndicesQ1_scalar(ei,ej,nxg, gidx,elgidx);CHKERRQ(ierr);
			ierr = GetElementEqnIndicesQ1_scalar(ei,ej,nxg, gidx_bclocal,elgidx_bc);CHKERRQ(ierr);
			
			ierr = MatSetValues(*B,NODES_PER_EL_Q1,elgidx_bc, NODES_PER_EL_Q1,elgidx_bc, ADe, ADD_VALUES );CHKERRQ(ierr);
		}
  }
	/* tidy up */
	ierr = DMDAVecRestoreArray(cda,coords,&LA_coords);CHKERRQ(ierr);
	
  ierr = DMDAVecRestoreArrayDOF(da, local_V,     &LA_V       );CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da, local_kappa, &LA_kappa   );CHKERRQ(ierr);
	
  ierr = DMRestoreLocalVector(cda,&local_V       );CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da, &local_kappa   );CHKERRQ(ierr);
	
	/* partial assembly */
	ierr = MatAssemblyBegin(*B, MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(*B, MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
	
	
	/* boundary conditions */
	{
		PetscInt Len_ghost,*gidx_bcghost;
		PetscBool is_dirich;
		PetscInt nproc,*procs,*mapL,**mapIdx;
		ISLocalToGlobalMapping map;
		PetscMPIInt rank;
		
		MPI_Comm_rank( ((PetscObject)da)->comm,&rank);
		ierr = BCListGetDofIdx(bclist,0,0,&Len_ghost,&gidx_bcghost);CHKERRQ(ierr);
		
		ierr = DMGetLocalToGlobalMapping(da,&map);CHKERRQ(ierr);
		ierr = ISLocalToGlobalMappingGetInfo(map,&nproc,&procs,&mapL,&mapIdx);CHKERRQ(ierr);
		
		if (Len_ghost!=mapL[rank]) {
			//			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"map indices are fucked up");
		}
		for (i=0; i<Len_ghost; i++) {
			ierr = BCListIsDirichlet(gidx_bcghost[i],&is_dirich);CHKERRQ(ierr);
			if (is_dirich) {
				ierr = MatSetValueLocal(*B,i,i,1.0,INSERT_VALUES);CHKERRQ(ierr);
			}
		}
	}
	
	/* assemble */
	ierr = MatAssemblyBegin(*B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(*B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  if (*A != *B) {
    ierr = MatAssemblyBegin(*A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(*A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }
	*mstr = SAME_NONZERO_PATTERN;
	
  PetscFunctionReturn(0);
}

void VMS2d_FormMass_FormResidual_2(
																					 PetscScalar Re[],
																		       PetscReal dt,
																					 PetscScalar el_coords[],PetscScalar el_kappa[],PetscScalar el_V[],
																					 PetscScalar el_phi[],PetscScalar el_phi_last[])
{
  PetscInt    ngp;
  PetscScalar gp_xi[GAUSS_POINTS_2x2][2];
  PetscScalar gp_weight[GAUSS_POINTS_2x2];
  PetscInt    p,i,j;
  PetscScalar Ni_p[NODES_PER_EL_Q1];
  PetscScalar GNi_p[NSD][NODES_PER_EL_Q1],GNx_p[NSD][NODES_PER_EL_Q1];
  PetscScalar J_p,fac;
  PetscScalar phi_p,phi_last_p,f_p,v_p[2],kappa_p, gradphi_p[2],gradphiold_p[2];
  PetscScalar JJ[2][2],invJJ[2][2];
	PetscScalar tau_constant,tau;
	PetscScalar b1_p,grad_b1_p[2];
	
  /* define quadrature rule */
  ConstructGaussQuadrature_2x2_2d(&ngp,gp_xi,gp_weight);
	

	VMS2d_Compute_tau_const( el_coords,el_kappa,el_V,&tau_constant );
  
	/* evaluate integral */
  for (p = 0; p < ngp; p++) {
    ConstructQ12D_Ni(gp_xi[p],Ni_p);
    ConstructQ12D_GNi(gp_xi[p],GNi_p);
    ConstructQ12D_GNx(GNi_p,GNx_p,el_coords,&J_p);
		ConstructQ12D_ElementTransformation(el_coords,GNi_p,JJ,invJJ);
		
		
    fac = gp_weight[p]*J_p;
		
		f_p          = 0.0;
    phi_p        = 0.0;
    phi_last_p   = 0.0;
    kappa_p      = 0.0;
    gradphi_p[0] = gradphi_p[1] = 0.0;
    gradphiold_p[0] = gradphiold_p[1] = 0.0;
    v_p[0]       = v_p[1]       = 0.0;
    for (j=0; j<NODES_PER_EL_Q1; j++) {
      phi_p        += Ni_p[j] * el_phi[j];      /* compute phi on the particle */
      phi_last_p   += Ni_p[j] * el_phi_last[j];  /* compute phi_dot on the particle */
      gradphiold_p[0] += GNx_p[0][j] * el_phi_last[j];
      gradphiold_p[1] += GNx_p[1][j] * el_phi_last[j];

      gradphi_p[0] += GNx_p[0][j] * el_phi[j];
      gradphi_p[1] += GNx_p[1][j] * el_phi[j];
      kappa_p      += Ni_p[j] * el_kappa[j];
			
      v_p[0]     += Ni_p[j] * el_V[NSD*j+0];      /* compute vx on the particle */
      v_p[1]     += Ni_p[j] * el_V[NSD*j+1];      /* compute vy on the particle */
    }

		ConstructVMS2D_b1_gradient_b1(gp_xi[p],&b1_p,grad_b1_p);
		tau = ( b1_p * tau_constant );
//		printf("  ** tau = %1.4e \n", tau );
				
    for (i = 0; i < NODES_PER_EL_Q1; i++) {
      Re[ i ] += fac * (
												// -f - M T^k
												- dt * Ni_p[i] * f_p 
												- dt * tau * ( v_p[0]*GNx_p[0][i] + v_p[1]*GNx_p[1][i] ) * f_p 
												// (C+L) T^k+1
												+ (THETA)*dt * Ni_p[i] * ( v_p[0] * gradphi_p[0] + v_p[1] * gradphi_p[1] ) 
												+ (THETA)*dt * kappa_p * ( GNx_p[0][i] * gradphi_p[0] + GNx_p[1][i] * gradphi_p[1] )
												+ (THETA)*dt * ( (v_p[0]*GNx_p[0][i] + v_p[1]*GNx_p[1][i]) * tau * (v_p[0]*gradphi_p[0] + v_p[1]*gradphi_p[1])  )
												
												+ (1.0-THETA)*dt * Ni_p[i] * ( v_p[0] * gradphiold_p[0] + v_p[1] * gradphiold_p[1] ) 
												+ (1.0-THETA)*dt * kappa_p * ( GNx_p[0][i] * gradphiold_p[0] + GNx_p[1][i] * gradphiold_p[1] )
												+ (1.0-THETA)*dt * ( (v_p[0]*GNx_p[0][i] + v_p[1]*GNx_p[1][i]) * tau * (v_p[0]*gradphiold_p[0] + v_p[1]*gradphiold_p[1])  )
												// M T^k+1
                        + Ni_p[i] * phi_p 
                        - Ni_p[i] * phi_last_p 
															
												);
    }
  }
	
}

#undef __FUNCT__  
#define __FUNCT__ "FormFunction_VMS_T"
PetscErrorCode FormFunction_VMS_T(
																					DMDASUPGParameters data,PetscReal dt,
																					DM da,PetscScalar ***LA_V,PetscScalar **LA_kappa,
																					PetscScalar **LA_phi,PetscScalar **LA_philast,PetscScalar **LA_R)
{
	Vec kappa,V;
  DM                     cda;
  Vec                    coords;
  DMDACoor2d             **LA_coords;
  MatStencil             phi_eqn[NODES_PER_EL_Q1];
  PetscInt               sex,sey,mx,my;
  PetscInt               ei,ej;
  PetscScalar            Re[NODES_PER_EL_Q1];
  PetscScalar            el_coords[NODES_PER_EL_Q1*NSD],el_kappa[NODES_PER_EL_Q1],el_V[NODES_PER_EL_Q1*NSD],el_phi[NODES_PER_EL_Q1],el_philast[NODES_PER_EL_Q1];
	
  PetscInt    ngp;
  PetscScalar gp_xi[GAUSS_POINTS_2x2][2];
  PetscScalar gp_weight[GAUSS_POINTS_2x2];
  PetscScalar Ni_p[NODES_PER_EL_Q1];
  PetscScalar GNi_p[NSD][NODES_PER_EL_Q1],GNx_p[NSD][NODES_PER_EL_Q1];
  PetscScalar J_p,fac;
  PetscScalar phi_p;
  PetscInt    p,i;
	PetscReal   cg,c = 0.0;
  PetscErrorCode         ierr;
	
  PetscFunctionBegin;
	
  ConstructGaussQuadrature_2x2_2d(&ngp,gp_xi,gp_weight);
	
  /* setup for coords */
  ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
  ierr = DMDAGetGhostedCoordinates(da,&coords);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(cda,coords,&LA_coords);CHKERRQ(ierr);
	
  ierr = DMDAGetElementCorners_Q1(da,&sex,&sey,0,&mx,&my,0);CHKERRQ(ierr);
  for (ej = sey; ej < sey+my; ej++) {
    for (ei = sex; ei < sex+mx; ei++) {
      /* get coords for the element */
      GetElementCoords_Q1(LA_coords,ei,ej,el_coords);
      /* get coefficients for the element */
      DMDAGetElementValues_Q1_scalar(LA_kappa,ei,ej,el_kappa);
      DMDAGetElementValues_Q1_vector(LA_V,ei,ej,el_V);
      /* get phi */
      DMDAGetElementValues_Q1_scalar(LA_phi,ei,ej,el_phi);
      DMDAGetElementValues_Q1_scalar(LA_philast,ei,ej,el_philast);
			
      /* initialise element stiffness matrix */
      ierr = PetscMemzero(Re,sizeof(PetscScalar)*NODES_PER_EL_Q1);CHKERRQ(ierr);
			
      /* form element stiffness matrix */
      VMS2d_FormMass_FormResidual_2(Re,dt,el_coords,el_kappa,el_V,el_phi,el_philast);
			
			for (p=0; p<ngp; p++) {
				ConstructQ12D_Ni(gp_xi[p],Ni_p);
				ConstructQ12D_GNi(gp_xi[p],GNi_p);
				ConstructQ12D_GNx(GNi_p,GNx_p,el_coords,&J_p);
				
				phi_p = 0.0;
				for (i=0; i<NODES_PER_EL_Q1; i++) {
					phi_p = phi_p + Ni_p[i] * el_phi[i];
				}
				
				fac = gp_weight[p]*J_p;
				c = c + phi_p * fac;
				
			}
			
      /* insert element matrix into global matrix */
      ierr = DMDAGetElementEqnums_Q1_scalar(ei,ej,phi_eqn);CHKERRQ(ierr);
			
      ierr = DMDASetValuesLocalStencil_Q1_ADD_VALUES(LA_R,phi_eqn,Re);CHKERRQ(ierr);
    }
  }
  MPI_Allreduce(&c,&cg,1,MPIU_REAL,MPI_MIN,((PetscObject)da)->comm);
	PetscPrintf(PETSC_COMM_WORLD,"\\int \\phi dV = %1.12e \n", cg );
	
  /* tidy up local arrays (input) */
  ierr = DMDAVecRestoreArray(cda,coords,      &LA_coords  );CHKERRQ(ierr);
	
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "VMSFormFunction"
PetscErrorCode VMSFormFunction(PetscReal time,Vec X,PetscReal dt,Vec F,void *ctx)
{
  DMDASUPGParameters data  = (DMDASUPGParameters)ctx;
  DM             da,cda;
	Vec philoc, philastloc, Fphiloc;
	Vec Vloc,kappaloc;
	PetscScalar **LA_philoc, **LA_philastloc, **LA_Fphiloc;
	PetscScalar ***LA_V,**LA_kappa;
  PetscErrorCode ierr;
	
  PetscFunctionBegin;
	da = data->daT;
	
	
	ierr = VecZeroEntries(F);CHKERRQ(ierr);
	
  ierr = DMGetLocalVector(da,&philoc);CHKERRQ(ierr);
  ierr = DMGetLocalVector(da,&philastloc);CHKERRQ(ierr);
	
  ierr = DMGetLocalVector(da,&Fphiloc);CHKERRQ(ierr);
	
	/* get local solution and time derivative */
	ierr = VecZeroEntries(philoc);CHKERRQ(ierr);
	ierr = DMGlobalToLocalBegin(da,X,ADD_VALUES,philoc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd  (da,X,ADD_VALUES,philoc);CHKERRQ(ierr);
	
	ierr = VecZeroEntries(philastloc);CHKERRQ(ierr);
	ierr = DMGlobalToLocalBegin(da,data->phi_last,ADD_VALUES,philastloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd  (da,data->phi_last,ADD_VALUES,philastloc);CHKERRQ(ierr);
	
	/* insert boundary conditions into local vectors */
	ierr = BCListInsertLocal(data->phi_bclist,philoc);CHKERRQ(ierr);
	
	/* what do i do with the phi_dot */
	
	/* init residual */
	ierr = VecZeroEntries(Fphiloc);CHKERRQ(ierr);
	
	/* get arrays */
	ierr = DMDAVecGetArray(da,philoc,    &LA_philoc);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(da,philastloc,&LA_philastloc);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(da,Fphiloc,   &LA_Fphiloc);CHKERRQ(ierr);
	
	/* ============= */
	/* FORM_FUNCTION */
  /* get acces to the vector kappa */
  ierr = DMGetLocalVector(da,&kappaloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(da,data->kappa,INSERT_VALUES,kappaloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(  da,data->kappa,INSERT_VALUES,kappaloc);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da,kappaloc,&LA_kappa);CHKERRQ(ierr);
	
  /* get acces to the vector V */
  ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
  ierr = DMGetLocalVector(cda,&Vloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(cda,data->V,INSERT_VALUES,Vloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(  cda,data->V,INSERT_VALUES,Vloc);CHKERRQ(ierr);
  ierr = DMDAVecGetArrayDOF(cda,Vloc,&LA_V);CHKERRQ(ierr);
	
	ierr = FormFunction_VMS_T(data,dt,da,LA_V,LA_kappa, LA_philoc,LA_philastloc,LA_Fphiloc);CHKERRQ(ierr);
	
  ierr = DMDAVecRestoreArrayDOF(cda,Vloc,&LA_V);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da,kappaloc,&LA_kappa);CHKERRQ(ierr);
	
  ierr = DMRestoreLocalVector(cda,&Vloc);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da,&kappaloc);CHKERRQ(ierr);
	/* ============= */
	
	ierr = DMDAVecRestoreArray(da,Fphiloc,   &LA_Fphiloc);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(da,philastloc,&LA_philastloc);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(da,philoc,    &LA_philoc);CHKERRQ(ierr);
	
	
	/* do global fem summation */
	ierr = VecZeroEntries(F);CHKERRQ(ierr);
	
  ierr = DMLocalToGlobalBegin(da,Fphiloc,ADD_VALUES,F);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd  (da,Fphiloc,ADD_VALUES,F);CHKERRQ(ierr);
	
  ierr = DMRestoreLocalVector(da,&Fphiloc);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da,&philoc);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da,&philastloc);CHKERRQ(ierr);
	
	/* modify F for the boundary conditions, F_k = scale_k(x_k - phi_k) */
	ierr = BCListResidualDirichlet(data->phi_bclist,X,F);CHKERRQ(ierr);
	
  PetscFunctionReturn(0);
}

