#ifndef __MATERIAL_POINT_PTHERMAL_UTILS_H__
#define __MATERIAL_POINT_PTHERMAL_UTILS_H__

#include "swarm_fields.h"
#include "data_exchanger.h"
#include "MPntStd_def.h"
#include "MPntPThermal_def.h"
#include "phys_energy_equation.h"

PetscErrorCode SwarmOutputParaView_MPntPThermal(DataBucket db,const char path[],const char prefix[]);
PetscErrorCode SwarmUpdateGaussPropertiesLocalL2Projection_P0_MPntPThermal(const int npoints,MPntStd mp_std[],MPntPThermal mp_thermal[],DM da,QuadratureVolumeEnergy Q);
PetscErrorCode SwarmUpdateQuadraturePropertiesLocalL2Projection_Q1_MPntPThermal(const int npoints,MPntStd mp_std[],MPntPThermal mp_thermal[],DM da,QuadratureVolumeEnergy Q);

#endif
