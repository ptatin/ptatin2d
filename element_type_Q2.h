
#ifndef __pTatin_element_type_Q2_h__
#define __pTatin_element_type_Q2_h__


typedef struct {
	double xi,eta,zeta;
	double w;
} QPoint3d;
typedef struct {
	double xi,eta;
	double w;
} QPoint2d;
typedef struct {
	double xi;
	double w;
} QPoint1d;

typedef struct {
	double x,y,z;
} XPoint3d;
typedef struct {
	double x,y;
} XPoint2d;
typedef struct {
	double x;
} XPoint1d;


typedef struct _p_ConformingElementFamily* ConformingElementFamily;

typedef enum { LINE_EDGES=1, QUAD_EDGES=4, HEX_EDGES=6 } HexElementFaceCount;

typedef enum { HEX_FACE_Pxi=0, HEX_FACE_Nxi, HEX_FACE_Peta, HEX_FACE_Neta, HEX_FACE_Pzeta, HEX_FACE_Nzeta } HexElementFace;
typedef enum { QUAD_EDGE_Pxi=0, QUAD_EDGE_Nxi, QUAD_EDGE_Peta, QUAD_EDGE_Neta } QuadElementEdge;
typedef enum { LINE_POINT_Pxi=0, LINE_POINT_Nxi } LineElementPoint;

struct _p_ConformingElementFamily {
	char *name;
	int nsd;
	
	int n_nodes_0D;
	int n_nodes_1D;
	int n_nodes_2D;
	int n_nodes_3D;
	
	void (*basis_NI_1D)(const QPoint1d*,double*); // xi[],Ni[]
	void (*basis_NI_2D)(const QPoint2d*,double*);
	void (*basis_NI_3D)(const QPoint3d*,double*);
	
	void (*basis_GNI_1D)(const QPoint1d*,double**); // xi[],GNi[]
	void (*basis_GNI_2D)(const QPoint2d*,double**);
	void (*basis_GNI_3D)(const QPoint3d*,double**);
	
	// 1D only
	int n_points;
	int **point_node_list;
	void (*generate_all_point_ids)(int**);
	void (*extract_point_field)(ConformingElementFamily,int,int,double*,double*); // Element,face,dofs,e_data[],edge_e_data[]


	
	// 2D only
	int n_edges;
	int **edge_node_list;
	void (*generate_all_edge_ids)(int**);
	void (*extract_edge_field)(ConformingElementFamily,QuadElementEdge,int,double*,double*); // Element,face,dofs,e_data[],edge_e_data[]
	
	// 3D only
	int n_faces;
	int **face_node_list;
	void (*generate_all_face_ids)(int**);
	void (*extract_surface_field)(ConformingElementFamily,HexElementFace,int,double*,double*); // Element,face,dofs,e_data[],surface_e_data[]
	
	void (*generate_surface_quadrature_2D)(ConformingElementFamily,QuadElementEdge,int*,QPoint1d*,QPoint2d*); // face_id,ngp_s,w[],xi_st[],xi_xyz[]
	void (*generate_volume_quadrature_2D)(int*,QPoint2d*); // ngp,w[],xi_xyz[]
	void (*compute_surface_geometry_2D)(ConformingElementFamily,const double*,QuadElementEdge,const QPoint1d*,double*,double*,double*); // coords[],edge_idx,,xi[],normal[],J
	void (*compute_volume_geometry_2D)(const double*,const double**,double**,double*); // coords[],GNi[][],GNx[][],J
	
	void (*generate_surface_quadrature_3D)(ConformingElementFamily,HexElementFace,int*,QPoint2d*,QPoint3d*);
	void (*generate_volume_quadrature_3D)(int*,QPoint3d*);
	void (*compute_surface_geometry_3D)(ConformingElementFamily,const double*,HexElementFace,const QPoint2d*,double*,double*,double*);
	void (*compute_volume_geometry_3D)(const double*,const double**,double**,double*);
	
	/* 
	 3D  2D  1D
	 volume   Y		X		X
	 face     Y		Y		X
	 edge     Y		Y		Y
	 point    Y		Y		Y
	 */
	
};


void extract_element_surface_field_Q2_3D( int face_id, int fid_node_list[][9], int dofs, double e_data[], double surf_e_data[] );



void ConstructNi_Q2_1D( const QPoint1d *q, double Ni[] );
void ConstructGNi_Q2_1D( const QPoint1d *q, double **GNi );

void ElementTypeCreate_Q2(ConformingElementFamily *_e,const int dim);
void ElementHelper_matrix_inverse_3x3(double A[3][3],double B[3][3]);

#endif
