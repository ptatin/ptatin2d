/*
  
 
 Input / command line parameters:
 -Ttop
 -Tbot 
 -mx
 -my
 -vx -3.000000e-10
 
-ptatin_model reza
-output_frequency 1
-output_path reza
-remesh_type 2
-store_ages
-stokes_scale_length 1.000000e+04
-stokes_scale_velocity 1.000000e-10
-stokes_scale_viscosity 1.000000e+27
-dt_max 3e11
-nsteps 500
-time_max 2.000000e+15
-use_refine_mesh 0
 

 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"

#define WRITE_VP
#define WRITE_TFIELD
#define WRITE_MPOINTS
//#define WRITE_QPOINTS
#define WRITE_TCELL
//#define WRITE_INT_FIELDS
//#define WRITE_FACEQPOINTS

 
#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_reza"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_reza(pTatinCtx ctx)
{
    
    PetscErrorCode ierr;
    PetscBool refine;
    PetscScalar OOx[21],OOy[21];
    PetscInt Nx[20], Ny[20],test,ndomainx,ndomainy;
    PetscInt nlimx,nlimy, i,j;
    PhysCompEnergyCtx phys_energy;
    DM daT;
    pTatinUnits *units;
    PetscReal left_cont_litho_length, right_cont_litho_length;
    PetscReal oceanic_litho_length;
    PetscReal Ox,Oy,Lx,Ly,gravity_acceleration;
   
    PetscFunctionBegin;
	units = &ctx->units;
	
	/* set gravity */
	gravity_acceleration = 9.81;
	ierr = QuadratureStokesGravityModelCoordinateAlignedSetValues(ctx->Q,0.0, -gravity_acceleration);CHKERRQ(ierr);
	/* set model size*/
	
	left_cont_litho_length=300e3;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-left_cont_litho_length",&left_cont_litho_length,PETSC_NULL);CHKERRQ(ierr);
	
	right_cont_litho_length=600e3;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-right_cont_litho_length",&right_cont_litho_length,PETSC_NULL);CHKERRQ(ierr);
	
	oceanic_litho_length=800e3;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-oceanic_litho_length",&oceanic_litho_length,PETSC_NULL);CHKERRQ(ierr);
	
	Ox=0;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-Ox",&Ox,PETSC_NULL);CHKERRQ(ierr);

	Ly=0;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-Ly",&Ly,PETSC_NULL);CHKERRQ(ierr);

	Lx=left_cont_litho_length+oceanic_litho_length+right_cont_litho_length;    // total length of model:km
	//ierr = PetscOptionsGetScalar(PETSC_NULL,"-Lx",&Lx,PETSC_NULL);CHKERRQ(ierr);

	Oy=-600e3;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-Oy",&Oy,PETSC_NULL);CHKERRQ(ierr);
	
	PetscPrintf(PETSC_COMM_WORLD,"ApplyInitialMeshGeometry: options Domain_SI = [ %1.4e , %1.4e ] x [ %1.4e , %1.4e ] \n", Ox, Lx,Oy,Ly);	
	// scaling the values  
	UnitsApplyInverseScaling(units->si_length,Ox,&Ox);
	UnitsApplyInverseScaling(units->si_length,Oy,&Oy);
	UnitsApplyInverseScaling(units->si_length,Lx,&Lx);
	UnitsApplyInverseScaling(units->si_length,Ly,&Ly);

	PetscPrintf(PETSC_COMM_WORLD,"ApplyInitialMeshGeometry : options Domain_ND = [ %1.4e , %1.4e ] x [ %1.4e , %1.4e ] \n", Ox, Lx,Oy,Ly );

	 /* create 2D mesh*/
	ierr = DMDASetUniformCoordinates(ctx->dav,Ox,Lx,Oy,Ly, 0.0,0.0);CHKERRQ(ierr);
  
	/* refine the initial mesh*/
	/* PetscOptionsSetValue("-refine_n_domain_x","5");
	PetscOptionsSetValue("-refine_n_domain_y","3");
	PetscOptionsSetValue("-refine_x_dom_el","10,3,250,3,10");
	PetscOptionsSetValue("-refine_y_dom_el","9,30,20");
	PetscOptionsSetValue("-refine_x_lim","0.0,3.0,3.5,12.5,13.0,16.0");
	PetscOptionsSetValue("-refine_y_lim","-3.0,-1.2,-0.2,0.0");
	*/
	refine = PETSC_FALSE;
	PetscOptionsGetBool(PETSC_NULL,"-use_refine_mesh",&refine,0);
    
	if (refine) {
	    PetscOptionsGetInt(PETSC_NULL,"-refine_n_domain_x",&ndomainx,0);
	    PetscOptionsGetInt(PETSC_NULL,"-refine_n_domain_y",&ndomainy,0);
	    nlimx=ndomainx+1;
	    nlimy=ndomainy+1;
	    PetscOptionsGetRealArray(PETSC_NULL,"-refine_x_lim",OOx,&nlimx,PETSC_NULL);
	    PetscOptionsGetRealArray(PETSC_NULL,"-refine_y_lim",OOy,&nlimy,PETSC_NULL);
    
	    PetscOptionsGetIntArray(PETSC_NULL,"-refine_x_dom_el",Nx,&ndomainx,PETSC_NULL);
	    PetscOptionsGetIntArray(PETSC_NULL,"-refine_y_dom_el",Ny,&ndomainy,PETSC_NULL);
    
	    test=0;
	    for (i=0;i<ndomainx;i++) { test = test+ Nx[i];}
	    if (test != ctx->mx) {SETERRQ2(PETSC_COMM_WORLD,PETSC_ERR_SUP,"mx = %d is not equal to the number of element = %d in the     refined mesh", ctx->mx, test);}
	    test=0;
	    for (j=0;j<ndomainy;j++) { test += Ny[j];}
	    if (test != ctx->my) {SETERRQ2(PETSC_COMM_WORLD,PETSC_ERR_SUP,"my = %d is not equal to the number of element = %d in the refined mesh", ctx->my, test);}
	    if ((nlimx > 20) |( nlimy >20)) {SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"maximum refinement domain is set to 20 in the current version of the code");}
	    ierr = DMDASetRefinedCoordinates(ctx->dav); CHKERRQ(ierr);
	 }
	  /* create 2D mesh for temperature aligned on the veocity mesh*/
	phys_energy = ctx->phys_energy;
	daT         = phys_energy->daT;
	ierr = DMDAProjectCoordinatesQ2toQ1_2d(ctx->dav,daT);CHKERRQ(ierr);
    
    PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatin2d_ModelSetMaterialProperty_reza"
PetscErrorCode pTatin2d_ModelSetMaterialProperty_reza(pTatinCtx ctx)

{
    RheologyConstants      *rheology;
    PetscInt               nphase, i;
    PetscErrorCode         ierr;
    char   		           *option_name;
    PetscBool              found,weak_iran_lower_crust;
    PetscScalar            vmol,oceanic_litho_strength;
    PetscFunctionBegin;
	ctx->rheology_constants.rheology_type = 6;// use stockes vpt standard
	nphase = 12;    /* or marker index  */
   
	rheology   = &ctx->rheology_constants;
	rheology->eta_upper_cutoff_global = 1.e25;
	rheology->eta_lower_cutoff_global = 1.e19;
	ierr       = PetscOptionsGetReal(PETSC_NULL,"-eta_max_cut_off",&rheology->eta_upper_cutoff_global,0);CHKERRQ(ierr);
	ierr       = PetscOptionsGetReal(PETSC_NULL,"-eta_min_cut_off",&rheology->eta_lower_cutoff_global,0);CHKERRQ(ierr);
    vmol=8.0e-6;
    ierr       = PetscOptionsGetReal(PETSC_NULL,"-vmol",&vmol,0);CHKERRQ(ierr);
    oceanic_litho_strength = 6.e8;
    ierr       = PetscOptionsGetReal(PETSC_NULL,"-oceanic_litho_strength",&oceanic_litho_strength,0);CHKERRQ(ierr);
    weak_iran_lower_crust = PETSC_FALSE; 
    ierr       = PetscOptionsGetBool(PETSC_NULL,"-weak_iran_lower_crust",&weak_iran_lower_crust,0);CHKERRQ(ierr);

	PetscPrintf(PETSC_COMM_WORLD,"ApplyViscouscuttoff: eta min =  %1.2e , eta_max =  %1.2e \n", rheology->eta_lower_cutoff_global, rheology->eta_upper_cutoff_global);
    
    ierr       = PetscOptionsGetBool(PETSC_NULL,"-store_melt",&rheology->store_melt,0);CHKERRQ(ierr);
    
	for (i=0;i<nphase; i++){
	    rheology->temp_kappa[i]      = 1e-6;
	    rheology->temp_prod[i]       = 0.;
	    rheology->density_type[i]    = 1;
	    rheology->temp_alpha[i]      = 3e-5;
	    rheology->temp_beta[i]       = 1e-11;
	    rheology->viscous_type[i]    = 2;
	    rheology->arrh_Tref[i]       = 273;
	    rheology->plastic_type[i]    = 2;
	    rheology->melt_type[i]       = 2;
	    rheology->tens_cutoff[i]     = 1.e6;
	    rheology->softening_type[i]  = 1;
	    rheology->Hst_cutoff[i]      = 4.e8;
	    rheology->soft_min_strain_cutoff[i] = 0.5 ;
	    rheology->soft_max_strain_cutoff[i] = 1.5;
        rheology->melt_eta[i] = 1e18;
        
	}
	/*6 strong arabia upper crust  Rybacki and Dresen 2000 for Wet anorthite (LC) */
	rheology->const_rho0[6]      = 2800.;
	rheology->arrh_nexp[6]       = 3;
	rheology->arrh_entalpy[6]    = 3.56e5;
	rheology->arrh_preexpA[6]    = 4.0e2;
	rheology->arrh_Ascale[6]     = 1e6;
	rheology->arrh_Vmol[6]       = vmol;
	rheology->mises_tau_yield[6] = 2.e7;
	rheology->dp_pressure_dependance[6]=30.;
	rheology->softening_type[6]=1;
	rheology->soft_Co_inf[6] = 2.e7;
	rheology->soft_phi_inf[6] = 5.;
    rheology->melt_rho[6] = 2000;
    rheology->meltP_type[6]=1;
    
	/*4 arabic lower crust maryland diabiase strong Mackwell 1998 */
	rheology->const_rho0[4]      = 2900.;
	rheology->arrh_nexp[4]       = 4.7;
	rheology->arrh_entalpy[4]    = 485.e5;
	rheology->arrh_preexpA[4]    = 8;
	rheology->arrh_Ascale[4]     = 1e6;
	rheology->arrh_Vmol[4]       = vmol;
	rheology->mises_tau_yield[4] = 2.e7;
	rheology->dp_pressure_dependance[2]=30.;
	rheology->softening_type[4]=1;
	rheology->soft_Co_inf[4] = 2.e7;
	rheology->soft_phi_inf[4] = 5.;
    rheology->melt_rho[4] = 2000;
    rheology->meltP_type[4]=0;
    
	/*8 oceanic crust maryland diabiase strong Mackwell 1998 */
	rheology->const_rho0[8]      = 2900.;
	rheology->arrh_nexp[8]       = 4.7;
	rheology->arrh_entalpy[8]    = 485.e5;
	rheology->arrh_preexpA[8]    = 8;
	rheology->arrh_Ascale[8]     = 1e6;
	rheology->arrh_Vmol[8]       = vmol;
	rheology->mises_tau_yield[8] = 2.e7;
	rheology->dp_pressure_dependance[8]=30.;
	rheology->softening_type[8]=1;
	rheology->soft_Co_inf[8] = 2.e7;
	rheology->soft_phi_inf[8] = 5.;
    rheology->melt_rho[8] = 2000;
    rheology->meltP_type[8]=0;
    
	/* 9 oceanic crust  Dry olivine, H&K */
	rheology->const_rho0[9]   = 2900.;
	rheology->arrh_nexp[9]    = 3;
	rheology->arrh_entalpy[9] = 510.e3;
	rheology->arrh_preexpA[9] = 7.0e3;
	rheology->arrh_Ascale[9]  = 1.e6; /* si A[MPA-n] Ascale=1.e6 else if A[Pa-n] then A=1. */
	rheology->arrh_Vmol[9]    = vmol;
	rheology->mises_tau_yield[9] = 2.e7; /* Cohesion initial */
	rheology->dp_pressure_dependance[9]=30.; /* Phi initial */
	rheology->tens_cutoff[9] =1.e7;
	rheology->Hst_cutoff[9] = 4.e8;
	rheology->softening_type[9]=1;
	rheology->soft_Co_inf[9] = 2.e7;
	rheology->soft_phi_inf[9] = 5.;
    rheology->melt_rho[9] = 2000;
    rheology->meltP_type[9]=0;
    
	/* 1 arabian lithos mantle Dry olivine, H&K */
	rheology->const_rho0[1]   = 3290.;
	rheology->arrh_nexp[1]    = 3;
	rheology->arrh_entalpy[1] = 510.e3;
	rheology->arrh_preexpA[1] = 7.0e3;
	rheology->arrh_Ascale[1]  = 1.e6; /* si A[MPA-n] Ascale=1.e6 else if A[Pa-n] then A=1. */
	rheology->arrh_Vmol[1]    = vmol;
	rheology->mises_tau_yield[1] = 2.e7; /* Cohesion initial */
	rheology->dp_pressure_dependance[1]=30.; /* Phi initial */
	rheology->tens_cutoff[1] =1.e7;
	rheology->Hst_cutoff[1] = 4.e8;
	rheology->softening_type[1]=1;
	rheology->soft_Co_inf[1] = 2.e7;
	rheology->soft_phi_inf[1] = 5.;
    rheology->melt_rho[1] = 2000;
    rheology->meltP_type[1]=0;
    
	/* 0 deep mantle  Dry olivine, H&K */
	rheology->const_rho0[0]   = 3320.;
	rheology->arrh_nexp[0]    = 3;
	rheology->arrh_entalpy[0] = 510.e3;
	rheology->arrh_preexpA[0] = 7.0e3;
	rheology->arrh_Ascale[0]  = 1.e6; /* si A[MPA-n] Ascale=1.e6 else if A[Pa-n] then A=1. */
	rheology->arrh_Vmol[0]    = vmol;
	rheology->mises_tau_yield[0] = 2.e7; /* Cohesion initial */
	rheology->dp_pressure_dependance[0]=30.; /* Phi initial */
	rheology->tens_cutoff[0] =1.e7;
	rheology->Hst_cutoff[0] = 4.e8;
	rheology->softening_type[0]=1;
	rheology->soft_Co_inf[0] = 2.e7;
	rheology->soft_phi_inf[0] = 5.;
    rheology->melt_rho[0] = 2000;
    rheology->meltP_type[0]=0;
    // rheology->temp_kappa[0] = 1e-5;
    
	/* 3 oceanic litho mantle*/
	rheology->const_rho0[3]   = 3320.;
	rheology->arrh_nexp[3]    = 3;
	rheology->arrh_entalpy[3] = 510.e3;
	rheology->arrh_preexpA[3] = 7.0e3;
	rheology->arrh_Ascale[3]  = 1.e6; /* si A[MPA-n] Ascale=1.e6 else if A[Pa-n] then A=1. */
	rheology->arrh_Vmol[3]    = vmol;
	rheology->mises_tau_yield[3] = 2.e7; /* Cohesion initial */
	rheology->dp_pressure_dependance[3]=30.; /* Phi initial */
	rheology->tens_cutoff[3] =1.e7;
	rheology->Hst_cutoff[3] = oceanic_litho_strength;
	rheology->softening_type[3]=1;
	rheology->soft_Co_inf[3] = 2.e7;
	rheology->soft_phi_inf[3] = 15.;
    rheology->melt_rho[3] = 2000;
    rheology->meltP_type[3]=0;
    
	/* 2 iranian litho mantle  Dry olivine, H&K */
	rheology->const_rho0[2]   = 3310.;
	rheology->arrh_nexp[2]    = 3;
	rheology->arrh_entalpy[2] = 510.e3;
	rheology->arrh_preexpA[2] = 7.0e3;
	rheology->arrh_Ascale[2]  = 1.e6; /* si A[MPA-n] Ascale=1.e6 else if A[Pa-n] then A=1. */
	rheology->arrh_Vmol[2]    = vmol;
	rheology->mises_tau_yield[2] = 2.e7; /* Cohesion initial */
	rheology->dp_pressure_dependance[2]=30.; /* Phi initial */
	rheology->tens_cutoff[2] =1.e7;
	rheology->Hst_cutoff[2] = 4.e8;
	rheology->softening_type[2]=1;
	rheology->soft_Co_inf[2] = 2.e7;
	rheology->soft_phi_inf[2] = 5.;
    rheology->melt_rho[2] = 2000;
    rheology->meltP_type[2]=0;
    
	/* 7 upper crust iran  Gleason and Tullis 1995 for Wet Quartz  */
	rheology->const_rho0[7]      = 2800.;
	rheology->arrh_nexp[7]       = 4;
	rheology->arrh_entalpy[7]    = 2.23e5;
	rheology->arrh_preexpA[7]    = 1.1e-4;
	rheology->arrh_Ascale[7]     = 1e6;
	rheology->arrh_Vmol[7]       = vmol;
	rheology->mises_tau_yield[7] = 2.e7;
	rheology->dp_pressure_dependance[5]=30.;
	rheology->softening_type[7]=1;
	rheology->soft_Co_inf[7] = 2.e7;
	rheology->soft_phi_inf[7] = 5.;
    rheology->melt_rho[7] = 2000;
    rheology->meltP_type[7]=1;
    
	/* 5 iranian lower crust  Rybacki and Dresen 2000 for Wet anorthite  */
        rheology->const_rho0[5]   = 2800.;
        rheology->arrh_nexp[5]    = 3.;
        rheology->arrh_entalpy[5] = 3.56e5;
        rheology->arrh_preexpA[5] = 4.0e2;
        rheology->arrh_Ascale[5]  = 1.e6; /* si A[MPA-n] Ascale=1.e6 else if A[Pa-n] then A=1. */
        rheology->arrh_Vmol[5]    = vmol;
        rheology->mises_tau_yield[5] = 2.e7;
        rheology->dp_pressure_dependance[5]=30.;
        rheology->soft_Co_inf[5] = 2.e7;
        rheology->soft_phi_inf[5] = 5.;
        rheology->melt_rho[5] = 2000;
        rheology->meltP_type[5]=1;
    
    if (weak_iran_lower_crust){
        rheology->arrh_nexp[5]       = 4;
        rheology->arrh_entalpy[5]    = 2.23e5;
        rheology->arrh_preexpA[5]    = 1.1e-4;
    }
    
        /* 10 sediments   */
        rheology->const_rho0[10]   = 2200.;
        rheology->arrh_nexp[10]    = 4.;
        rheology->arrh_entalpy[10] = 2.23e5;
        rheology->arrh_preexpA[10] = 1.1e-4;
        rheology->arrh_Ascale[10]  = 1.e6; /* si A[MPA-n] Ascale=1.e6 else if A[Pa-n] then A=1. */
        rheology->arrh_Vmol[10]    = vmol;
        rheology->arrh_Tref[10]    = 273; /* celcius 273 Kelvin 0 */
        rheology->mises_tau_yield[10] = 2.e7;
        rheology->dp_pressure_dependance[10]=5.;
        rheology->soft_Co_inf[10] = 2.e7;
        rheology->soft_phi_inf[10] = 5.;
        rheology->temp_prod[10]              = 1e-12;
        rheology->melt_rho[10] = 2000;
        rheology->meltP_type[10]=1;
        /* 11 weak phase  */
    rheology->const_rho0[11]   = 3310.;
    rheology->arrh_nexp[11]    = 3;
    rheology->arrh_entalpy[11] = 510.e3;
    rheology->arrh_preexpA[11] = 7.0e3;
    rheology->arrh_Ascale[11]  = 1.e6; /* si A[MPA-n] Ascale=1.e6 else if A[Pa-n] then A=1. */
    rheology->arrh_Vmol[11]    = vmol;
    rheology->mises_tau_yield[11] = 1.e7; /* Cohesion initial */
    rheology->dp_pressure_dependance[11]=5.; /* Phi initial */
    rheology->tens_cutoff[11] =1.e7;
    rheology->Hst_cutoff[11] = 2.e7;
    rheology->softening_type[11]=1;
    rheology->soft_Co_inf[11] = 2.e7;
    rheology->soft_phi_inf[11] = 5.;
    rheology->melt_rho[11] = 2000;
    rheology->meltP_type[11]=0;
	
    PetscFunctionReturn(0);
}
 
#undef __FUNCT__
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_reza"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_reza(pTatinCtx ctx)
{
    PetscErrorCode         ierr;
    pTatinUnits *units;
    PetscFunctionBegin;
	units   = &ctx->units;
	UnitsApplyInverseScaling(units->si_time,ctx->dt_min,&ctx->dt_min);
	UnitsApplyInverseScaling(units->si_time,ctx->dt,&ctx->dt);
	UnitsApplyInverseScaling(units->si_time,ctx->dt_adv,&ctx->dt_adv);
	UnitsApplyInverseScaling(units->si_time,ctx->dt_max,&ctx->dt_max);
	UnitsApplyInverseScaling(units->si_time,ctx->time_max,&ctx->time_max);
	UnitsApplyInverseScaling(units->si_length,ctx->max_disp_y,&ctx->max_disp_y);
	ierr = pTatin2d_ModelSetMaterialProperty_reza(ctx); CHKERRQ(ierr);
	ierr = pTatin2d_ModelSetMarker_reza(ctx); CHKERRQ(ierr);
	ierr= pTatin2d_ModelSetInitialTemperatureOnNodes_reza(ctx); CHKERRQ(ierr);
	ierr= pTatin2d_ModelSetSPMParameters_GENE(ctx); CHKERRQ(ierr);
    
    PetscFunctionReturn(0);
}
 
#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetMarker_reza"
PetscErrorCode pTatin2d_ModelSetMarker_reza(pTatinCtx ctx)
{

    PetscInt               p,n_mp_points;
    DataBucket             db;
    DataField              PField_std, PField_stokes,PField_stokespl, PField_thermal, PField_chrono;
    int                    phase;
    PetscScalar            eps_init,eta_ND,rho_ND,kappa_ND,prod_ND;
    DM da; 
    PetscErrorCode ierr;
    RheologyConstants      *rheology;
    pTatinUnits *units;
    PetscReal   thick,alfa ;
    PetscReal  hs, hr;
    PetscReal Ox,Oy,Lx,Ly;
    PetscReal x_middle_ocean;
    PetscReal s_litho_l1, s_litho_l2, sf_tl1,sf_tl2;
    PetscReal s_litho_r1, s_litho_r2, sf_tr1,sf_tr2;
    PetscReal s_cont_lower_l1,s_cont_lower_l2;
    PetscReal s_cont_lower_r1,s_cont_lower_r2;
    PetscReal s_cont_upper_l1, s_cont_upper_l2;
    PetscReal s_cont_upper_r1,s_cont_upper_r2;
    PetscReal x1,x2,y2,pi;
    PetscReal   jmin[3],jmax[3];
    PetscReal left_cont_lower_crust_depth,right_cont_lower_crust_depth;
    PetscReal left_cont_upper_crust_depth, right_cont_upper_crust_depth;
    PetscReal left_cont_litho_depth,right_cont_litho_depth;
    PetscReal oceanic_lower_crust_depth,oceanic_upper_crust_depth;
    PetscReal left_cont_litho_length, right_cont_litho_length;
    PetscReal oceanic_litho_length,oceanic_litho_age;
    PetscReal max_cont_litho_age; 
    PetscReal sf_left_cont_litho, shift_left_cont_litho;
    PetscReal sf_right_cont_litho, shift_right_cont_litho;
    PetscReal sf_left_lower_crust, shift_left_lower_crust;
    PetscReal sf_right_lower_crust, shift_right_lower_crust;
    PetscReal sf_left_upper_crust, shift_left_upper_crust;
    PetscReal sf_right_upper_crust, shift_right_upper_crust;
    PetscReal oceanic_litho_depth;
    
    PetscFunctionBegin;
    
	pi=4*atan(1.);
	rheology   = &ctx->rheology_constants;
	units=&ctx->units;
	da=ctx->dav;
	//DMDAGetBoundingBox(da,jmin,jmax);
	//Lx = jmax[0];
	//Ox = jmin[0];
	//Ly = jmax[1];
	//Oy = jmin[1];
	alfa=45.;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-alfa",&alfa,PETSC_NULL);CHKERRQ(ierr);
	
	thick=30000.;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-thick",&thick,PETSC_NULL);CHKERRQ(ierr);
	UnitsApplyInverseScaling(units->si_length,thick,&thick);
	
	hs=1e-12;   //radiogenic prod (K/s/m3)
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-hs",&hs,PETSC_NULL);CHKERRQ(ierr);
	UnitsApplyInverseScaling(units->si_heatsource,hs,&hs);

	hr=10e3;   //radiogenic efold
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-hr",&hr,PETSC_NULL);CHKERRQ(ierr);
	UnitsApplyInverseScaling(units->si_length,hr,&hr);
      
        left_cont_lower_crust_depth=-30e3;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-left_cont_lower_crust_depth",&left_cont_lower_crust_depth,PETSC_NULL);CHKERRQ(ierr);
	UnitsApplyInverseScaling(units->si_length,left_cont_lower_crust_depth,&left_cont_lower_crust_depth);
	
	right_cont_lower_crust_depth=-40e3;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-right_cont_lower_crust_depth",&right_cont_lower_crust_depth,PETSC_NULL);CHKERRQ(ierr);
        UnitsApplyInverseScaling(units->si_length,right_cont_lower_crust_depth,&right_cont_lower_crust_depth);
	
	left_cont_upper_crust_depth=-15e3;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-left_cont_upper_crust_depth",&left_cont_upper_crust_depth,PETSC_NULL);CHKERRQ(ierr);
	UnitsApplyInverseScaling(units->si_length,left_cont_upper_crust_depth,&left_cont_upper_crust_depth);
	
	right_cont_upper_crust_depth=-20e3;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-right_cont_upper_crust_depth",&right_cont_upper_crust_depth,PETSC_NULL);CHKERRQ(ierr);
	UnitsApplyInverseScaling(units->si_length,right_cont_upper_crust_depth,&right_cont_upper_crust_depth);
 
	left_cont_litho_depth=-160e3;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-left_cont_litho_depth",&left_cont_litho_depth,PETSC_NULL);CHKERRQ(ierr);
	UnitsApplyInverseScaling(units->si_length,left_cont_litho_depth,&left_cont_litho_depth);
	
	right_cont_litho_depth=-160e3;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-right_cont_litho_depth",&right_cont_litho_depth,PETSC_NULL);CHKERRQ(ierr);
	UnitsApplyInverseScaling(units->si_length,right_cont_litho_depth,&right_cont_litho_depth);
	
	oceanic_lower_crust_depth=-6e3;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-oceanic_lower_crust_depth",&oceanic_lower_crust_depth,PETSC_NULL);CHKERRQ(ierr);
	UnitsApplyInverseScaling(units->si_length,oceanic_lower_crust_depth,&oceanic_lower_crust_depth);
	
	oceanic_upper_crust_depth=-3e3;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-oceanic_upper_crust_depth",&oceanic_upper_crust_depth,PETSC_NULL);CHKERRQ(ierr);
	UnitsApplyInverseScaling(units->si_length,oceanic_upper_crust_depth,&oceanic_upper_crust_depth);
	
	left_cont_litho_length=300e3;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-left_cont_litho_length",&left_cont_litho_length,PETSC_NULL);CHKERRQ(ierr);
	UnitsApplyInverseScaling(units->si_length,left_cont_litho_length,&left_cont_litho_length);
	
	right_cont_litho_length=600e3;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-right_cont_litho_length",&right_cont_litho_length,PETSC_NULL);CHKERRQ(ierr);
	UnitsApplyInverseScaling(units->si_length,right_cont_litho_length,&right_cont_litho_length);
	
	oceanic_litho_length=800e3;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-oceanic_litho_length",&oceanic_litho_length,PETSC_NULL);CHKERRQ(ierr);
	UnitsApplyInverseScaling(units->si_length,oceanic_litho_length,&oceanic_litho_length);
	
	oceanic_litho_age=60.;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-oceanic_litho_age",&oceanic_litho_age,PETSC_NULL);CHKERRQ(ierr);
	
	max_cont_litho_age=800;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-max_cont_litho_age",&max_cont_litho_age,PETSC_NULL);CHKERRQ(ierr);
	
	oceanic_litho_depth=-70e3;
    	ierr = PetscOptionsGetScalar(PETSC_NULL,"-oceanic_litho_depth",&oceanic_litho_depth,PETSC_NULL);CHKERRQ(ierr);
        UnitsApplyInverseScaling(units->si_length,oceanic_litho_depth,&oceanic_litho_depth);
	
        Ox=0;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-Ox",&Ox,PETSC_NULL);CHKERRQ(ierr);

	Ly=0;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-Ly",&Ly,PETSC_NULL);CHKERRQ(ierr);

	Lx=left_cont_litho_length+oceanic_litho_length+right_cont_litho_length;    // total length of model:km
	//ierr = PetscOptionsGetScalar(PETSC_NULL,"-Lx",&Lx,PETSC_NULL);CHKERRQ(ierr);

	Oy=-600e3;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-Oy",&Oy,PETSC_NULL);CHKERRQ(ierr);
	UnitsApplyInverseScaling(units->si_length,Oy,&Oy);
	
	sf_left_cont_litho=.0001;    //scale factor for arctan functiom
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-sf_left_cont_litho",&sf_left_cont_litho,PETSC_NULL);CHKERRQ(ierr);
        UnitsApplyScaling(units->si_length,sf_left_cont_litho,&sf_left_cont_litho);
	
	shift_left_cont_litho=50;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-shift_left_cont_litho",&shift_left_cont_litho,PETSC_NULL);CHKERRQ(ierr);
        UnitsApplyInverseScaling(units->si_length,shift_left_cont_litho,&shift_left_cont_litho);
	
	sf_right_cont_litho=.0001;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-sf_right_cont_litho",&sf_right_cont_litho,PETSC_NULL);CHKERRQ(ierr);
	UnitsApplyScaling(units->si_length,sf_right_cont_litho,&sf_right_cont_litho);
	
	shift_right_cont_litho=50;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-shift_right_cont_litho",&shift_right_cont_litho,PETSC_NULL);CHKERRQ(ierr);
        UnitsApplyInverseScaling(units->si_length,shift_right_cont_litho,&shift_right_cont_litho);

	sf_left_lower_crust=.00001;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-sf_left_lower_crust",&sf_left_lower_crust,PETSC_NULL);CHKERRQ(ierr);
        UnitsApplyScaling(units->si_length,sf_left_lower_crust,&sf_left_lower_crust);
	
	shift_left_lower_crust=4;     // shift parameter for arctan function
        ierr = PetscOptionsGetScalar(PETSC_NULL,"-shift_left_lower_crust",&shift_left_lower_crust,PETSC_NULL);CHKERRQ(ierr);
        UnitsApplyInverseScaling(units->si_length,shift_left_lower_crust,&shift_left_lower_crust);

	sf_right_lower_crust=.00001;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-sf_right_lower_crust",&sf_right_lower_crust,PETSC_NULL);CHKERRQ(ierr);
        UnitsApplyScaling(units->si_length,sf_right_lower_crust,&sf_right_lower_crust);
	
	shift_right_lower_crust=4;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-shift_right_lower_crust",&shift_right_lower_crust,PETSC_NULL);CHKERRQ(ierr);
        UnitsApplyInverseScaling(units->si_length,shift_right_lower_crust,&shift_right_lower_crust);

	sf_left_upper_crust=.00001;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-sf_left_upper_crust",&sf_left_upper_crust,PETSC_NULL);CHKERRQ(ierr);
        UnitsApplyScaling(units->si_length,sf_left_upper_crust,&sf_left_upper_crust);
	
	shift_left_upper_crust=4;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-shift_left_upper_crust",&shift_left_upper_crust,PETSC_NULL);CHKERRQ(ierr);
        UnitsApplyInverseScaling(units->si_length,shift_left_upper_crust,&shift_left_upper_crust);

	sf_right_upper_crust=.00001;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-sf_right_upper_crust",&sf_right_upper_crust,PETSC_NULL);CHKERRQ(ierr);
        UnitsApplyScaling(units->si_length,sf_right_upper_crust,&sf_right_upper_crust);
	
	shift_right_upper_crust=4;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-shift_right_upper_crust",&shift_right_upper_crust,PETSC_NULL);CHKERRQ(ierr);
        UnitsApplyInverseScaling(units->si_length,shift_right_upper_crust,&shift_right_upper_crust);

        x1=Lx-right_cont_litho_length;
        y2=0;
        x2=x1+thick/sin(alfa*pi/180);
        //--------------------------
        //construction lithosphere geometry
        x_middle_ocean=left_cont_litho_length+oceanic_litho_length/2;
        s_litho_l1=-atan(sf_left_cont_litho*(-left_cont_litho_length-shift_left_cont_litho))+atan(sf_left_cont_litho*(x_middle_ocean-left_cont_litho_length-shift_left_cont_litho));
        //sf_tl1=s_litho_l1/(max_cont_litho_age-oceanic_litho_age);
        //sf_tl2=-atan(sf_left_cont_litho*(x_middle_ocean-left_cont_litho_length-shift_left_cont_litho))/sf_tl1;
        s_litho_l1=s_litho_l1/(left_cont_litho_depth-oceanic_litho_depth);
        s_litho_l2=-atan(sf_left_cont_litho*(x_middle_ocean-left_cont_litho_length-shift_left_cont_litho))/s_litho_l1;
        s_litho_r1=-atan(sf_right_cont_litho*(x_middle_ocean-(Lx-right_cont_litho_length)-shift_right_cont_litho))+atan(sf_right_cont_litho*(Lx-(Lx-right_cont_litho_length)-shift_right_cont_litho));
        //sf_tr1=s_litho_r1/(max_cont_litho_age-oceanic_litho_age);
        //sf_tr2=atan(sf_right_cont_litho*(x_middle_ocean-(Lx-right_cont_litho_length)-shift_right_cont_litho))/sf_tr1;
        s_litho_r1=s_litho_r1/(right_cont_litho_depth-oceanic_litho_depth);
        s_litho_r2=atan(sf_right_cont_litho*(x_middle_ocean-(Lx-right_cont_litho_length)-shift_right_cont_litho))/s_litho_r1;
        //--------------------------
        //construction continental lower crust geometry
        s_cont_lower_l1=-atan(sf_left_lower_crust*(-left_cont_litho_length-shift_left_lower_crust))+atan(sf_left_lower_crust*(-shift_left_lower_crust));
        s_cont_lower_l1=s_cont_lower_l1/(left_cont_lower_crust_depth-oceanic_lower_crust_depth);
        s_cont_lower_l2=-atan(sf_left_lower_crust*(-shift_left_lower_crust))/s_cont_lower_l1;
        s_cont_lower_r1=atan(sf_right_lower_crust*(+right_cont_litho_length+shift_right_lower_crust))-atan(sf_right_lower_crust*(+shift_right_lower_crust));
        s_cont_lower_r1=s_cont_lower_r1/(right_cont_lower_crust_depth-oceanic_lower_crust_depth);
        s_cont_lower_r2=atan(sf_right_lower_crust*(+shift_right_lower_crust))/s_cont_lower_r1;
        s_cont_upper_l1=-atan(sf_left_upper_crust*(-left_cont_litho_length-shift_left_upper_crust))+atan(sf_left_upper_crust*(-shift_left_upper_crust));
        s_cont_upper_l1=s_cont_upper_l1/(left_cont_upper_crust_depth-oceanic_upper_crust_depth);
        s_cont_upper_l2=-atan(sf_left_upper_crust*(-shift_left_upper_crust))/s_cont_upper_l1;
        s_cont_upper_r1=atan(sf_right_upper_crust*(+right_cont_litho_length+shift_right_upper_crust))-atan(sf_right_upper_crust*(+shift_right_upper_crust));
        s_cont_upper_r1=s_cont_upper_r1/(right_cont_upper_crust_depth-oceanic_upper_crust_depth);
        s_cont_upper_r2=atan(sf_right_upper_crust*(+shift_right_upper_crust))/s_cont_upper_r1;
        
	/* define properties on material points */
	db = ctx->db;
        DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd)); // phase , position 
	
	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes)); // rho eta effective

	DataBucketGetDataFieldByName(db,MPntPStokesPl_classname,&PField_stokespl);
	DataFieldGetAccess(PField_stokespl);
	DataFieldVerifyAccess(PField_stokespl,sizeof(MPntPStokesPl)); // damage yieldtype,
    
	DataBucketGetDataFieldByName(db,MPntPThermal_classname,&PField_thermal); // kappa hprod + shear heating 
	DataFieldGetAccess(PField_thermal);
	DataFieldVerifyAccess(PField_thermal,sizeof(MPntPThermal));
    
	DataBucketGetDataFieldByName(db,MPntPChrono_classname,&PField_chrono); // thermo-chrono 
	DataFieldGetAccess(PField_chrono);
	DataFieldVerifyAccess(PField_chrono,sizeof(MPntPChrono));
         
	DataBucketGetSizes(db,&n_mp_points,0,0);
  
	x_middle_ocean = left_cont_litho_length+oceanic_litho_length/2.0; 
	for (p=0; p<n_mp_points; p++) { // loop onallthe markers 
	    MPntStd       *material_point; // pos phase index
	    MPntPStokes   *mpprop_stokes; // eta rho
	    double        *position;
	    double        x,y;
	    PetscReal     litho_boundary, left_moho,left_lc;
	    PetscReal     right_moho, right_lc;
	    PetscReal     xr,xl;
	    MPntPChrono   *mpprop_chrono; /* ages variables */
	    MPntPThermal  *material_point_thermal;
	    MPntPStokesPl *mpprop_stokespl;
	    PetscBool    faille=PETSC_FALSE;
	    PetscScalar  faille_damage;
         
	    DataFieldAccessPoint(PField_std,p,(void**)&material_point);		
	    MPntStdGetField_global_coord(material_point,&position);
	    //UnitsApplyScaling(units->si_length,position[0],&x); // x is in meter now
	    //UnitsApplyScaling(units->si_length,position[1],&y); // x is in meter now
	    x=position[0];
	    y=position[1];
	    
	    if (x < x_middle_ocean) {
         	litho_boundary =-atan(sf_left_cont_litho*(x-left_cont_litho_length-shift_left_cont_litho))/s_litho_l1-(s_litho_l2-oceanic_litho_depth);
          	if (y> litho_boundary){
		    left_moho=-atan(sf_left_lower_crust*(x-left_cont_litho_length-shift_left_lower_crust))/s_cont_lower_l1-(s_cont_lower_l2-oceanic_lower_crust_depth);
		    if  ((x<left_cont_litho_length) & (y > left_moho)){
			left_lc=-atan(sf_left_upper_crust*(x-left_cont_litho_length-shift_left_upper_crust))/s_cont_upper_l1-(s_cont_upper_l2-oceanic_upper_crust_depth);
			if (y> left_lc){
			    phase = 6;
			}else{
			    phase = 4;
			}
		     }else{
			  phase = 1;
		   }
		}else{
		    phase = 0;
		}
	    }else{
		litho_boundary = atan(sf_right_cont_litho*(x-(Lx-right_cont_litho_length)-shift_right_cont_litho))/s_litho_r1-(s_litho_r2-oceanic_litho_depth);
          	if (y> litho_boundary){
		    right_moho=atan(sf_right_lower_crust*(x-Lx+right_cont_litho_length+shift_right_lower_crust))/s_cont_lower_r1-(s_cont_lower_r2-oceanic_lower_crust_depth);
		    if  ((x>Lx-right_cont_litho_length) & (y> right_moho)){
			right_lc=atan(sf_right_upper_crust*(x-Lx+right_cont_litho_length+shift_right_lower_crust))/s_cont_upper_r1-(s_cont_upper_r2-oceanic_upper_crust_depth);
			if (y> right_lc){
			    phase = 7;
			}else{
			    phase = 5;
			}
		     }else{
			  phase = 2;
		    }
		 }else{
		      phase =0;
		 }
	    }
             if ((x>left_cont_litho_length) &  (x<Lx-right_cont_litho_length)) {
           
		if (y>litho_boundary){
	    
		    if (y>oceanic_lower_crust_depth){
			if (y>oceanic_upper_crust_depth){
			    phase=8;
			}else{
			    phase=9;
			}
		    }else{
			phase=3;
		    }
		}
	    }
          
	    xr=x2+(y2-y)/tan(alfa*pi/180.);
	    if ((x<=xr) & (x>Lx-right_cont_litho_length)) {
		if (y>litho_boundary){
		    phase=3;
		}
	    }
	    MPntStdSetField_phase_index(material_point,phase);
        
	    /* set eta and rho */
	    DataFieldAccessPoint(PField_stokes,p,(void**)&mpprop_stokes);
	    UnitsApplyInverseScaling(units->si_viscosity,1.e+21,&eta_ND);
	    UnitsApplyInverseScaling(units->si_force_per_volume,rheology->const_rho0[phase],&rho_ND);
	    MPntPStokesSetField_eta_effective(mpprop_stokes,eta_ND);
	    MPntPStokesSetField_density(mpprop_stokes,rho_ND);
		
	    /* set kappa and Hprod */
	    DataFieldAccessPoint(PField_thermal,p,(void**)&material_point_thermal);
	    UnitsApplyInverseScaling(units->si_diffusivity,rheology->temp_kappa[phase],&kappa_ND);
	    MPntPThermalSetField_diffusivity(material_point_thermal,kappa_ND);
	    prod_ND = 0.0;
	    if ((x<left_cont_litho_length) | (x>Lx-right_cont_litho_length)){
		prod_ND = hs*exp(y/hr);
	    }
	    MPntPThermalSetField_heat_prod(material_point_thermal,prod_ND);
		
	    /* set initial plastic strain */
            DataFieldAccessPoint(PField_stokespl,p,(void**)&mpprop_stokespl);
	    eps_init = 0.0;
	    xr=x2+(y2-y)/tan(alfa*pi/180.);
	    xl=x1+(y2-y)/tan(alfa*pi/180.);
	    litho_boundary = atan(sf_right_cont_litho*(x-(Lx-right_cont_litho_length)-shift_right_cont_litho))/s_litho_r1-(s_litho_r2-oceanic_litho_depth);
	    if ((x>xl) & (x<xr) & (y >litho_boundary)){
		eps_init = 0.;
        MPntStdSetField_phase_index(material_point,11);
	    } 
	    MPntPStokesPlSetField_plastic_strain(mpprop_stokespl,eps_init);
	    MPntPStokesPlSetField_yield_indicator(mpprop_stokespl,0.0);
        
	    DataFieldAccessPoint(PField_chrono,p,(void**)&mpprop_chrono);
	    MPntPChronoSetField_age120(mpprop_chrono,-1.0);
	    MPntPChronoSetField_age350(mpprop_chrono,-1.0);
	    MPntPChronoSetField_age800(mpprop_chrono,-1.0);

        }
	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokes);
	DataFieldRestoreAccess(PField_stokespl);
	DataFieldRestoreAccess(PField_thermal);
	DataFieldRestoreAccess(PField_chrono);
    PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetInitialTemperatureOnNodes_reza"
PetscErrorCode pTatin2d_ModelSetInitialTemperatureOnNodes_reza(pTatinCtx ctx)
/* define properties on material points */
{
    PetscErrorCode         ierr;
    DM da;
    Vec T;
    PhysCompEnergyCtx phys_energy;
    PetscInt i,j,n,k,si,sj,nx,ny;
    Vec coords;
    DMDACoor2d **LA_coords;
    DM cda;
    PetscScalar **LA_T;
    PetscScalar Ttop,Tbot,Tlitho,Tradio,dtdy,jmin[2],jmax[2],ylab,characteristic_time,h_prod,y_prod;
    RheologyConstants      *rheology;
    pTatinUnits *units;
    PetscBool              addanomaly=PETSC_FALSE;
    PetscReal  t_litho,t_top,t_bot, hs, hr, t_radio,Tnostat,Tcont;
    PetscReal   kappa ;
    PetscReal  s_litho_l1, s_litho_l2, sf_tl1,sf_tl2;
    PetscReal  s_litho_r1, s_litho_r2, sf_tr1,sf_tr2;
    PetscReal  litho_boundary,x_middle_ocean;
    PetscReal  Lx ,ybottom,Oy;
    PetscReal  litho_age;
    PetscReal  pi,kr,Ma;
    PetscReal left_cont_lower_crust_depth,right_cont_lower_crust_depth;
    PetscReal left_cont_upper_crust_depth, right_cont_upper_crust_depth;
    PetscReal left_cont_litho_depth,right_cont_litho_depth;
    PetscReal oceanic_lower_crust_depth,oceanic_upper_crust_depth;
    PetscReal left_cont_litho_length, right_cont_litho_length;
    PetscReal oceanic_litho_length,oceanic_litho_age;
    PetscReal max_cont_litho_age; 
    PetscReal sf_left_cont_litho, shift_left_cont_litho;
    PetscReal sf_right_cont_litho, shift_right_cont_litho;
    PetscReal sf_left_lower_crust, shift_left_lower_crust;
    PetscReal sf_right_lower_crust, shift_right_lower_crust;
    PetscReal sf_left_upper_crust, shift_left_upper_crust;
    PetscReal sf_right_upper_crust, shift_right_upper_crust;
    PetscReal Ox,Ly;
    PetscReal oceanic_litho_depth;
          
    PetscFunctionBegin;
	
	phys_energy = ctx->phys_energy;
	// rheology    = &ctx->rheology_constants;
	
	da          = phys_energy->daT;
	T           = phys_energy->T;
	//kappa       = rheology->temp_kappa[1];
	//ylab        = rheology->ylab;
	//h_prod      = rheology->h_prod;
	//y_prod      = rheology->y_prod;
	units       = &ctx->units;
      
	t_litho=1300;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-t_litho",&t_litho,PETSC_NULL);CHKERRQ(ierr);
      
	t_top=0;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-t_top",&t_top,PETSC_NULL);CHKERRQ(ierr);
      
	t_bot=1450;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-t_bot",&t_bot,PETSC_NULL);CHKERRQ(ierr);
      
	pi=4*atan(1);
	Ma=(1e6*365.25*24*3600);
      
	kappa=1e-6;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-kappa",&kappa,PETSC_NULL);CHKERRQ(ierr);
	UnitsApplyInverseScaling(units->si_diffusivity,kappa,&kappa);
	
	hs=1e-12;   //radiogenic prod (K/s/m3)
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-hs",&hs,PETSC_NULL);CHKERRQ(ierr);
	UnitsApplyInverseScaling(units->si_heatsource,hs,&hs);

	hr=10e3;   //radiogenic efold
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-hr",&hr,PETSC_NULL);CHKERRQ(ierr);
	UnitsApplyInverseScaling(units->si_length,hr,&hr);
	//-------------------------
        left_cont_litho_depth=-160e3;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-left_cont_litho_depth",&left_cont_litho_depth,PETSC_NULL);CHKERRQ(ierr);
	UnitsApplyInverseScaling(units->si_length,left_cont_litho_depth,&left_cont_litho_depth);

	right_cont_litho_depth=-160e3;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-right_cont_litho_depth",&right_cont_litho_depth,PETSC_NULL);CHKERRQ(ierr);
	UnitsApplyInverseScaling(units->si_length,right_cont_litho_depth,&right_cont_litho_depth);
 
	left_cont_litho_length=300e3;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-left_cont_litho_length",&left_cont_litho_length,PETSC_NULL);CHKERRQ(ierr);
	UnitsApplyInverseScaling(units->si_length,left_cont_litho_length,&left_cont_litho_length);

	right_cont_litho_length=600e3;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-right_cont_litho_length",&right_cont_litho_length,PETSC_NULL);CHKERRQ(ierr);
	UnitsApplyInverseScaling(units->si_length,right_cont_litho_length,&right_cont_litho_length);

	oceanic_litho_length=800e3;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-oceanic_litho_length",&oceanic_litho_length,PETSC_NULL);CHKERRQ(ierr);
	UnitsApplyInverseScaling(units->si_length,oceanic_litho_length,&oceanic_litho_length);

	oceanic_litho_age=60.;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-oceanic_litho_age",&oceanic_litho_age,PETSC_NULL);CHKERRQ(ierr);
	
	max_cont_litho_age=800;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-max_cont_litho_age",&max_cont_litho_age,PETSC_NULL);CHKERRQ(ierr);
	
	oceanic_litho_depth=-70e3;
        ierr = PetscOptionsGetScalar(PETSC_NULL,"-oceanic_litho_depth",&oceanic_litho_depth,PETSC_NULL);CHKERRQ(ierr);
        UnitsApplyInverseScaling(units->si_length,oceanic_litho_depth,&oceanic_litho_depth);
 
	Ox=0;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-Ox",&Ox,PETSC_NULL);CHKERRQ(ierr);

	Ly=0;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-Ly",&Ly,PETSC_NULL);CHKERRQ(ierr);

	Lx=left_cont_litho_length+oceanic_litho_length+right_cont_litho_length;    // total length of model:km
	//ierr = PetscOptionsGetScalar(PETSC_NULL,"-Lx",&Lx,PETSC_NULL);CHKERRQ(ierr);

	Oy=-600e3;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-Oy",&Oy,PETSC_NULL);CHKERRQ(ierr);
        UnitsApplyInverseScaling(units->si_length,Oy,&Oy);
 
	sf_left_cont_litho=.0001;    //scale factor for arctan functiom
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-sf_left_cont_litho",&sf_left_cont_litho,PETSC_NULL);CHKERRQ(ierr);
        UnitsApplyScaling(units->si_length,sf_left_cont_litho,&sf_left_cont_litho);   
	
	shift_left_cont_litho=50;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-shift_left_cont_litho",&shift_left_cont_litho,PETSC_NULL);CHKERRQ(ierr);
        UnitsApplyInverseScaling(units->si_length,shift_left_cont_litho,&shift_left_cont_litho);

	sf_right_cont_litho=.0001;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-sf_right_cont_litho",&sf_right_cont_litho,PETSC_NULL);CHKERRQ(ierr);
        UnitsApplyScaling(units->si_length,sf_right_cont_litho,&sf_right_cont_litho);   
	
	shift_right_cont_litho=50;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-shift_right_cont_litho",&shift_right_cont_litho,PETSC_NULL);CHKERRQ(ierr);
        UnitsApplyInverseScaling(units->si_length,shift_right_cont_litho,&shift_right_cont_litho);
 
        x_middle_ocean=left_cont_litho_length+oceanic_litho_length/2;
        s_litho_l1=-atan(sf_left_cont_litho*(-left_cont_litho_length-shift_left_cont_litho))+atan(sf_left_cont_litho*(x_middle_ocean-left_cont_litho_length-shift_left_cont_litho));
        sf_tl1=s_litho_l1/(max_cont_litho_age-oceanic_litho_age);
        sf_tl2=-atan(sf_left_cont_litho*(x_middle_ocean-left_cont_litho_length-shift_left_cont_litho))/sf_tl1;
        s_litho_l1=s_litho_l1/(left_cont_litho_depth-oceanic_litho_depth);
        s_litho_l2=-atan(sf_left_cont_litho*(x_middle_ocean-left_cont_litho_length-shift_left_cont_litho))/s_litho_l1;

        s_litho_r1=-atan(sf_right_cont_litho*(x_middle_ocean-(Lx-right_cont_litho_length)-shift_right_cont_litho))+atan(sf_right_cont_litho*(Lx-(Lx-right_cont_litho_length)-shift_right_cont_litho));
        sf_tr1=s_litho_r1/(max_cont_litho_age-oceanic_litho_age);
        sf_tr2=atan(sf_right_cont_litho*(x_middle_ocean-(Lx-right_cont_litho_length)-shift_right_cont_litho))/sf_tr1;
        s_litho_r1=s_litho_r1/(right_cont_litho_depth-oceanic_litho_depth);
        s_litho_r2=atan(sf_right_cont_litho*(x_middle_ocean-(Lx-right_cont_litho_length)-shift_right_cont_litho))/s_litho_r1;
        
	//ierr = PetscOptionsGetScalar(PETSC_NULL,"-Thermal_age_Myr",&age,PETSC_NULL);CHKERRQ(ierr);
        //ierr = PetscOptionsGetScalar(PETSC_NULL,"-h_prod",&h_prod,PETSC_NULL);CHKERRQ(ierr);
        //ierr = PetscOptionsGetScalar(PETSC_NULL,"-y_prod",&y_prod,PETSC_NULL);CHKERRQ(ierr);
        // ierr = PetscOptionsGetScalar(PETSC_NULL,"-ylab",&ylab,PETSC_NULL);CHKERRQ(ierr);
	
	// UnitsApplyInverseScaling(units->si_time,age,&age);
        //UnitsApplyInverseScaling(units->si_diffusivity,kappa,&kappa);
	UnitsApplyInverseScaling(units->si_length,y_prod,&y_prod);
	UnitsApplyInverseScaling(units->si_heatsource,h_prod,&h_prod);
	UnitsApplyInverseScaling(units->si_length,ylab,&ylab);
	
	ierr = DMDAGetCoordinates(da,&coords);CHKERRQ(ierr);
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,coords,&LA_coords);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(da,T,&LA_T);CHKERRQ(ierr);
	ierr = DMDAGetCorners(da,&si,&sj,0 , &nx,&ny,0);CHKERRQ(ierr);
	
	DMDAGetBoundingBox(da,jmin,jmax);
	
	ierr = PetscOptionsGetBool(0,"-add_Tanomaly",&addanomaly,0);CHKERRQ(ierr);
	
        //if (addanomaly){
        //PetscScalar age_anom,Cx,Wx,age_new; 
        //PetscBool   found;
		
        //age_anom = 50;
        //Cx       = 300e3;
        //Wx       = 25e3;
		
        //ierr = PetscOptionsGetReal(0,"-Tanomaly_Centre",&Cx,&found);CHKERRQ(ierr);
        //ierr = PetscOptionsGetReal(0,"-Tanomaly_Width",&Wx,&found);CHKERRQ(ierr);	
        //ierr = PetscOptionsGetReal(0,"-age_anomaly",&age_anom,&found);CHKERRQ(ierr);
		
        //age_anom = age_anom * 3.14e13;
		
        //UnitsApplyInverseScaling(units->si_time,age_anom,&age_anom);
        //UnitsApplyInverseScaling(units->si_length,Cx,&Cx);
        //UnitsApplyInverseScaling(units->si_length,Wx,&Wx);
     
        DMDAGetBoundingBox(ctx->dav,jmin,jmax);
    
        //UnitsApplyScaling(units->si_length,jmax[0],&Lx);
        //UnitsApplyScaling(units->si_length,jmin[1],&ybottom);
        Lx=jmax[0];
        ybottom=jmin[1];
		
        for( j=sj; j<sj+ny; j++ ) {
           for( i=si; i<si+nx; i++ ) {
	      double position[2];
	      //UnitsApplyScaling(units->si_length,LA_coords[j][i].x,&position[0]);
	      //UnitsApplyScaling(units->si_length,LA_coords[j][i].y,&position[1]);
	      position[0]=LA_coords[j][i].x;
	      position[1]=LA_coords[j][i].y;
               
	      if (position[0]< x_middle_ocean) {
	 	  litho_boundary =-atan(sf_left_cont_litho*(position[0]-left_cont_litho_length-shift_left_cont_litho))/s_litho_l1-(s_litho_l2-oceanic_litho_depth);
                   if (position[1]>litho_boundary){
		      litho_age=-atan(sf_left_cont_litho*(position[0]-left_cont_litho_length-shift_left_cont_litho))/sf_tl1-(sf_tl2-oceanic_litho_age);
		      if ((position[0]<left_cont_litho_length) ){
			  t_radio = (hs*pow(hr,2)/kappa);
		      }else{
			  t_radio=0;
		      }
	  
		      Tnostat=0;
		      litho_age=litho_age*Ma;
		      UnitsApplyInverseScaling(units->si_time,litho_age,&litho_age);
		      for (k=1;k<=100;k++){
	 		  characteristic_time = litho_age/pow(litho_boundary,2)*kappa;
	 		  Tnostat=Tnostat+sin(k*pi*(-litho_boundary+position[1])/-litho_boundary)*(pow((-1),(k-1))/k*exp(-pow((k*pi),2)*characteristic_time))*2./pi*(t_litho+t_radio-t_top);
	 	      }
	 	      dtdy= (t_litho-t_radio-t_top)/-litho_boundary;
		      Tcont=(t_top-dtdy *position[1] +t_radio*(1-exp(position[1]/-litho_boundary)));
	 	      if (Tcont > t_litho){
			    Tcont=t_litho;
		      }
	 	      LA_T[j][i]= Tnostat+Tcont;
                   }else{
		      dtdy = -(t_litho-t_bot)/(litho_boundary-ybottom); 
		      LA_T[j][i]=dtdy*(litho_boundary-position[1])+t_litho;
		  }
              }else{
		   litho_boundary =atan(sf_right_cont_litho*(position[0]-(Lx-right_cont_litho_length)-shift_right_cont_litho))/s_litho_r1-(s_litho_r2-oceanic_litho_depth);
		   if (position[1]>litho_boundary){
			litho_age=atan(sf_right_cont_litho*(position[0]-(Lx-right_cont_litho_length)-shift_right_cont_litho))/sf_tr1-(sf_tr2-oceanic_litho_age);
           		if ( (position[0]>Lx-right_cont_litho_length)){
			    t_radio = (hs*pow(hr,2)/kappa);
                        }else{
			    t_radio=0;
	                }
	    		Tnostat=0;
			litho_age=litho_age*Ma;
		        UnitsApplyInverseScaling(units->si_time,litho_age,&litho_age);
			for (k=1;k<=100;k++){
		 	    characteristic_time = litho_age/pow(litho_boundary,2)*kappa;
			    Tnostat=Tnostat+sin(k*pi*(-litho_boundary+position[1])/-litho_boundary)*(pow((-1),(k-1))/k*exp(-pow((k*pi),2)*characteristic_time))*2/pi*(t_litho+t_radio-t_top);
			}
			dtdy= (t_litho-t_radio-t_top)/-litho_boundary;
			Tcont=(t_top-dtdy *position[1] +t_radio*(1-exp(position[1]/-litho_boundary)));
			if (Tcont > t_litho){
			Tcont=t_litho;
			}
                 	LA_T[j][i]= Tnostat+Tcont;
                   }else{
			dtdy = -(t_litho-t_bot)/(litho_boundary-ybottom); 
			LA_T[j][i]=dtdy*(litho_boundary-position[1])+t_litho;
		   }
              }
        
          }
      }
    PetscFunctionReturn(0);		      
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_reza"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_reza(pTatinCtx ctx)
{
     
    BCList         ubclist;
    PetscScalar    bcval;
    DM             dav;
    PetscErrorCode ierr;
    DM da;
    Vec T;
    PetscReal   jmin[3],jmax[3];
    PetscScalar opts_srH,v_init,opts_Lx,opts_Ly,dy;
    PhysCompEnergyCtx phys_energy;

    PetscScalar alpha_m;
    pTatinUnits *units;
    PetscBool              invert=PETSC_FALSE;
    PetscScalar  y1,coeff,rezareza;
    BC_Alabeaumont bcdata;
	
    PetscFunctionBegin;
	
        units   = &ctx->units;
        ubclist = ctx->u_bclist;
        dav     = ctx->dav;
        opts_srH = (3.14e-10);
        ierr = PetscOptionsGetScalar(PETSC_NULL,"-vx",&opts_srH,0);CHKERRQ(ierr);
        v_init   = opts_srH;

        UnitsApplyInverseScaling(units->si_velocity,opts_srH,&opts_srH);
        DMDAGetBoundingBox(dav,jmin,jmax);
        opts_Lx = jmax[0]-jmin[0];
        opts_Ly = jmax[1]-jmin[1];
        
	/* velocity at the bottom to fill up or empty down with material that leaves on the side* /
        bcval = -2*opts_srH*opts_Ly/opts_Lx;
        bcval = 0.0;
        ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
        bcval = -opts_srH; 
        ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
        bcval = opts_srH;
        //bcval = 0.0;
        ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
        bcval = opts_srH;
        bcval = 0.0;
        ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
        */
        ierr = PetscMalloc(sizeof(struct _p_BC_Alabeaumont),&bcdata);CHKERRQ(ierr);
			
        /*incompressible free surface à la beaumont*/
        y1=-160.e3;
        dy= 100.e3;
        ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_ylitho",&y1,0);CHKERRQ(ierr);
        ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_deltay",&dy,0);CHKERRQ(ierr);
			
        UnitsApplyInverseScaling(units->si_length,y1,&y1);
        UnitsApplyInverseScaling(units->si_length,dy,&dy);
			
        bcval = 0.0;
        bcdata->dim = 1;   
        bcdata->x0 = y1;   
        bcdata->x1 = y1-dy;
        coeff = (2*(jmax[1]-y1)+dy)/(2*(y1-dy-jmin[1]));
			
        ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
        //ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
        ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
        ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
        bcdata->v0 = +opts_srH; bcdata->v1 = -opts_srH*coeff; 
        ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_Alabeaumont,(void*)bcdata);CHKERRQ(ierr);	
        bcdata->v0 =- opts_srH; bcdata->v1 = +opts_srH*coeff;
        ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_Alabeaumont,(void*)bcdata);CHKERRQ(ierr);
        ierr = PetscFree(bcdata);CHKERRQ(ierr);
    
        /* check for energy solver */
    
        phys_energy = ctx->phys_energy;
        da          = phys_energy->daT;
         
        bcval = 0.0;
        ierr = PetscOptionsGetScalar(PETSC_NULL,"-t_top",&bcval,0);CHKERRQ(ierr);
        ierr = DMDABCListTraverse(phys_energy->T_bclist,da,DMDABCList_JMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
        bcval = 1340.0;
        ierr = PetscOptionsGetScalar(PETSC_NULL,"-t_bot",&bcval,0);CHKERRQ(ierr);
        ierr = DMDABCListTraverse(phys_energy->T_bclist,da,DMDABCList_JMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
            
        //bcval = 1338.0;        
        //ierr = DMDABCListTraverse(phys_energy->T_bclist,da,DMDABCList_JMIN2_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
 	 
    PetscFunctionReturn(0);
}









