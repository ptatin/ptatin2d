
static const char help[] = "Matrix free Stokes solver using Q2-Pm1 mixed finite elements.\n"
"2D prototype of the (p)ragmatic version of Tatin. (pTatin2d_v0.0)\n\n";

#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"
#include "pTatin2d_models.h"

//#define STD_CONTINUATION
//#define FRICTIONAL_CONTINUATION
typedef enum { STD_CONTINUATION=0, FRICTIONAL_CONTINUATION,NONE} ContinuationTypes;
extern PetscErrorCode FormFunction_Stokes_QCoords(SNES snes,Vec X,Vec F,void *ctx);
extern PetscErrorCode FormJacobian_Stokes_QCoords(SNES snes,Vec X,Mat *A,Mat *B,MatStructure *mstr,void *ctx);


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_material_points_std"
PetscErrorCode pTatin2d_material_points_std(int argc,char **argv)
{
	PetscInt        k,nlevels;
	DM							*dav_hierarchy;
	Mat             *interpolation;
	DM              multipys_pack;
	IS              *isg;
	SNES            snes,snes_pre;
	Mat             A,B,Auu,Aup,Apu,Spp;
	Vec             X,F;
	KSP             ksp;
	PC              pc;
	pTatinCtx       user;
	PetscInt        e,tk;
	PetscReal       time;
	PetscBool       skip_pre_snes = PETSC_FALSE;
	PetscBool       skip_snes = PETSC_FALSE;
	PetscBool       energy_solver_active =PETSC_FALSE;
	PetscBool       darcy_solver_active =PETSC_FALSE;
	PetscBool       spm_solver_active =PETSC_FALSE;
	PetscBool       use_quasi_newton_coordinate_update = PETSC_FALSE;
	PetscBool       force_pre_pre_solve = PETSC_FALSE; 
	PetscBool       use_adaptative_ls = PETSC_FALSE;
    PetscBool       skip_stokes_surface_courant = PETSC_FALSE;
	PetscBool       reset_stokes_solution_each_timestep = PETSC_FALSE ;
	PetscBool       passive_tracers = PETSC_FALSE; 
	ContinuationTypes continuation_type = STD_CONTINUATION; 
	PetscLogDouble  t0,t1;
	PetscErrorCode  ierr;
	
	
	ierr = pTatin2dCreateContext(&user);CHKERRQ(ierr);
	ierr = pTatin2dParseOptions(user);CHKERRQ(ierr);
	ierr = PetscOptionsGetBool(PETSC_NULL,"-skip_pre_snes",&skip_pre_snes,PETSC_NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetBool(PETSC_NULL,"-use_quasi_newton_coordinate_update",&use_quasi_newton_coordinate_update,PETSC_NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetBool(PETSC_NULL,"-force_pre_pre_solve",&force_pre_pre_solve,PETSC_NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetBool(PETSC_NULL,"-reset_stokes_solution_each_timestep",&reset_stokes_solution_each_timestep,PETSC_NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetInt(PETSC_NULL,"-continuation_type",&continuation_type,PETSC_NULL);CHKERRQ(ierr);
	
	
	if (skip_pre_snes) { PetscPrintf(PETSC_COMM_WORLD,"Option: -skip_pre_snes has been activated\n"); }
	if (use_quasi_newton_coordinate_update) { PetscPrintf(PETSC_COMM_WORLD,"Option: -use_quasi_newton_coordinate_update has been activated\n"); }
	if (force_pre_pre_solve) { PetscPrintf(PETSC_COMM_WORLD,"Option: -force_pre_pre_solve has been activated\n"); }
	if (reset_stokes_solution_each_timestep) { PetscPrintf(PETSC_COMM_WORLD,"Option: -reset_stokes_solution_each_timestep has been activated\n"); }
    if (reset_stokes_solution_each_timestep) { PetscPrintf(PETSC_COMM_WORLD,"Option: -reset_stokes_solution_each_timestep has been activated\n"); }
	if (continuation_type == FRICTIONAL_CONTINUATION ) { PetscPrintf(PETSC_COMM_WORLD,"Option: Use frictional continuation type\n"); }
	
	/*
	 {
	 PetscBool load_checkpoint=PETSC_FALSE;
	 
	 PetscOptionsGetBool(PETSC_NULL,"-load_cp",&load_checkpoint,0);
	 if (load_checkpoint==PETSC_TRUE) {
	 printf("loading checkpoint \n");
	 ierr = pTatinCheckpointLoad(user,PETSC_NULL);CHKERRQ(ierr);
	 }
	 }
	 */	
	ierr = pTatin2dCreateQ2PmMesh(user);CHKERRQ(ierr);
	user->dav->ops->coarsenhierarchy = DMCoarsenHierarchy2_DA;
	
	/* define mesh hiearchy for GMG */
	nlevels = 1;
	PetscOptionsGetInt(PETSC_NULL,"-dau_nlevels",&nlevels,0);
	ierr = PetscMalloc(sizeof(DM)*nlevels,&dav_hierarchy);CHKERRQ(ierr);
	dav_hierarchy[ nlevels-1 ] = user->dav;
	ierr = PetscObjectReference((PetscObject)user->dav);CHKERRQ(ierr);
	
	{
		DM *coarsened_list;
		ierr = PetscMalloc(sizeof(DM)*(nlevels-1),&coarsened_list);CHKERRQ(ierr);
		ierr = DMCoarsenHierarchy(user->dav,nlevels-1,coarsened_list);CHKERRQ(ierr);
		for (k=0; k<nlevels-1; k++) {
			dav_hierarchy[ nlevels-2-k ] = coarsened_list[k];
		}
		PetscFree(coarsened_list);
		
		for (k=0; k<nlevels; k++) {
			PetscPrintf(PETSC_COMM_WORLD,"level[%d] :\n", k );
			ierr = DMView(dav_hierarchy[k],PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
		}
	}
	
	ierr = PetscMalloc(sizeof(Mat)*nlevels,&interpolation);CHKERRQ(ierr);
	interpolation[0] = PETSC_NULL;
	for (k=0; k<nlevels-1; k++) {
		ierr = DMGetInterpolation(dav_hierarchy[k],dav_hierarchy[k+1],&interpolation[k+1],PETSC_NULL);CHKERRQ(ierr);
	}
	
	
	for (k=nlevels-1; k>=0; k--) {
		char dau_name[256];
		
		sprintf(dau_name,"dau[%d]",k);
		ierr = pTatinLogBasicDMDA(user,dau_name,dav_hierarchy[k]);CHKERRQ(ierr);	
	}
	
	ierr = pTatin2dCreateBoundaList(user);CHKERRQ(ierr);
	ierr = pTatin2dCreateQuadratureStokes(user);CHKERRQ(ierr);
	
	/* load energy equation */
	
	ierr = PetscOptionsGetBool(PETSC_NULL,"-energy_solver_active",&energy_solver_active,PETSC_NULL);CHKERRQ(ierr);
	if (energy_solver_active) {
		ierr = pTatinLoadPhysics_EnergyEquation(user);CHKERRQ(ierr);
	}

	/* load SPM equation */
	ierr = PetscOptionsGetBool(PETSC_NULL,"-spm_solver_active",&spm_solver_active,PETSC_NULL);CHKERRQ(ierr);
	if (spm_solver_active) {
		ierr = pTatinLoadPhysics_SPMEquation(user);CHKERRQ(ierr);
	}
	/* load Darcy equation */
	ierr = PetscOptionsGetBool(PETSC_NULL,"-darcy_solver_active",&darcy_solver_active,PETSC_NULL);CHKERRQ(ierr);
	if (darcy_solver_active) {
		ierr = pTatinLoadPhysics_DarcyEquation(user);CHKERRQ(ierr);
		ierr = DarcyConstantsInitialise(&user->darcy_constants);CHKERRQ(ierr);
	}
	
	ierr = pTatin2dCreateMaterialPoints(user);CHKERRQ(ierr);
	
	ierr = PetscOptionsGetBool(PETSC_NULL,"-use_passive_tracers",&passive_tracers,PETSC_NULL);CHKERRQ(ierr);
	

	
		/* report */
	{
		PetscBool active;
		ierr = pTatinPhysCompActivated(user,PhysComp_Stokes,&active);CHKERRQ(ierr);
		PetscPrintf(PETSC_COMM_WORLD,"[pTatin]: Physics component \"Stokes\" activated : %d \n", active );
		ierr = pTatinPhysCompActivated(user,PhysComp_Energy,&active);CHKERRQ(ierr);
		PetscPrintf(PETSC_COMM_WORLD,"[pTatin]: Physics component \"Energy\" activated : %d \n", active );
		ierr = pTatinPhysCompActivated(user,PhysComp_SPM,&active);CHKERRQ(ierr);
		PetscPrintf(PETSC_COMM_WORLD,"[pTatin]: Physics component \"SPM\" activated : %d \n", active );
		ierr = pTatinPhysCompActivated(user,PhysComp_Darcy,&active);CHKERRQ(ierr);
		PetscPrintf(PETSC_COMM_WORLD,"[pTatin]: Physics component \"Darcy\" activated : %d \n", active );
		if (passive_tracers) PetscPrintf(PETSC_COMM_WORLD,"[pTatin]: Passive Tracers activated : %d \n", passive_tracers );
	}
	
	/* mesh geometry */
	ierr = pTatin2d_ModelApplyInitialMeshGeometry(user);CHKERRQ(ierr);
     if (darcy_solver_active){
     ierr = DMDAProjectCoordinatesQ2toQ1_2d(user->dav,user->phys_darcy->daPf);
     }
	/* interpolate point coordinates (needed if mesh was modified) */
	ierr = QuadratureStokesCoordinateSetUp(user->Q,user->dav);CHKERRQ(ierr);
	ierr = MaterialPointCoordinateSetUp(user->db,user->dav);CHKERRQ(ierr);
	
	
	if (passive_tracers){
	ierr = pTatin2dCreatePassiveMarkers(user);CHKERRQ(ierr);
	ierr = MaterialPointCoordinateSetUp(user->db_passive,user->dav);CHKERRQ(ierr);
	}
	
	
	for (e=0; e<QUAD_EDGES; e++) {
		ierr = SurfaceQuadratureStokesGeometrySetUp(user->surfQ[e],user->dav);CHKERRQ(ierr);
	}
	
	
	/* material geometry and initial conditions for all physics */
	ierr = pTatin2d_ModelApplyInitialMaterialGeometry(user);CHKERRQ(ierr);
	
	/* boundary conditions */
	ierr = pTatin2d_ModelApplyBoundaryCondition(user);CHKERRQ(ierr);
	
	multipys_pack = user->pack;
	ierr = DMCreateGlobalVector(multipys_pack,&X);CHKERRQ(ierr);
	ierr = VecDuplicate(X,&F);CHKERRQ(ierr);
	
	ierr = pTatin2d_ModelOutput(user,X,"ic_bc");CHKERRQ(ierr);
	
	Auu = Aup = Apu = Spp = PETSC_NULL;
	ierr = DMCompositeStokes_MatCreate(multipys_pack,MATAIJ,&Auu,MATAIJ,&Aup,MATAIJ,PETSC_NULL,MATAIJ,&Spp);CHKERRQ(ierr);
	ierr = DMCompositeStokes_MatCreate(multipys_pack,MATAIJ,PETSC_NULL,MATAIJ,PETSC_NULL,MATAIJ,&Apu,MATAIJ,PETSC_NULL);CHKERRQ(ierr);
	
	{
		PetscBool upper,lower,full;
		
		upper = lower = full = PETSC_FALSE;
		ierr = PetscOptionsGetBool(PETSC_NULL,"-dmcomposite_pc_upper",&upper,PETSC_NULL);CHKERRQ(ierr);
		ierr = PetscOptionsGetBool(PETSC_NULL,"-dmcomposite_pc_lower",&lower,PETSC_NULL);CHKERRQ(ierr);
		ierr = PetscOptionsGetBool(PETSC_NULL,"-dmcomposite_pc_full",&full,PETSC_NULL);CHKERRQ(ierr);
		if ( (!full) && (!upper) && (!lower) ) {
			full = PETSC_TRUE;
			PetscPrintf(PETSC_COMM_WORLD,"Assuming Stokes preconditioner will be full: B = [ Auu, Aup ; Apu, Spp* ] \n");
		}
		
		if (full) {
			ierr = DMCompositeStokesCreateNest(multipys_pack,Auu,Aup,Apu,Spp,&B);CHKERRQ(ierr);
		} else if (upper) {
			ierr = DMCompositeStokesCreateNest(multipys_pack,Auu,Aup,PETSC_NULL,Spp,&B);CHKERRQ(ierr);
		} else if (lower) {
			ierr = DMCompositeStokesCreateNest(multipys_pack,Auu,PETSC_NULL,Apu,Spp,&B);CHKERRQ(ierr);
		}
	}
	
	if ( (!user->use_mf_stokes) || (skip_pre_snes==PETSC_FALSE) ) {
		ierr = DMCompositeStokesCreateNest(multipys_pack,Auu,Aup,Apu,PETSC_NULL,&A);CHKERRQ(ierr);
	}
	
	
	/* ====== Generate an initial condition for Newton ====== */
    

	
	switch (continuation_type){
          case STD_CONTINUATION : 
      	  {
		PetscInt k,maxit;
		SNES snesIC;
		Mat JA,JB;
		KSP kspIC;
		PC pcIC;
		MatStructure flg;
		
		//	ierr = DMGetMatrix_DMCompositeStokesQkPm(multipys_pack,MATNEST,&JA);CHKERRQ(ierr);
		ierr = DMCompositeStokesCreateNest(multipys_pack,Auu,Aup,Apu,PETSC_NULL,&JA);CHKERRQ(ierr);
		
		ierr = SNESCreate(PETSC_COMM_WORLD,&snesIC);CHKERRQ(ierr);
		ierr = SNESSetOptionsPrefix(snesIC,"ic_");CHKERRQ(ierr);
		if (use_quasi_newton_coordinate_update) {
			ierr = SNESSetFunction(snesIC,F,FormFunction_Stokes_QCoords,user);CHKERRQ(ierr);
		} else {
			ierr = SNESSetFunction(snesIC,F,FormFunction_Stokes,user);CHKERRQ(ierr);
		}
		
		/* 
		 ierr = SNESCreate(PETSC_COMM_WORLD,&snesIC);CHKERRQ(ierr);
		 ierr = SNESSetOptionsPrefix(snesIC,"ic_");CHKERRQ(ierr);
		 ierr = SNESSetFunction(snesIC,F,FormFunction_Stokes,user);CHKERRQ(ierr);
		 if (user->rheology_constants.rheology_type == RHEOLOGY_VISCO_ELASTIC_PLASTIC ) {
		 ierr = SNESSetFunction(snesIC,F,FormFunction_Stokes_ViscoElastoPlastic,user);CHKERRQ(ierr);
		 }
		 
		 // force evaluation of non-linear residual to update material points and quad point values 
		 {
		 VecZeroEntries(X); 
		 ierr = SNESComputeFunction(snesIC,X,F);CHKERRQ(ierr);
		 ierr = pTatinOutputParaViewMeshTemperature(user,user->phys_energy->T,user->outputpath,"Tfield_IC_dbg");CHKERRQ(ierr);
		 ierr = pTatinParaViewOutputQuadratureThermalAveragedFields(user,user->outputpath,"Tcell_IC_dbg");
		 ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(user,user->outputpath,"Scell_IC_dbg");CHKERRQ(ierr);
		 ierr = SwarmOutputParaView_MPntPStokes(user->db,user->outputpath,"mpoints_stk_IC_dbg");CHKERRQ(ierr);
		 }
		 
		 exit(0);
		 */
		/*if (user->rheology_constants.rheology_type == RHEOLOGY_VISCO_ELASTIC_PLASTIC ) {
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"RHEOLOGY_VISCO_ELASTIC_PLASTIC is no longer supported");
			ierr = SNESSetFunction(snesIC,F,FormFunction_Stokes_ViscoElastoPlastic,user);CHKERRQ(ierr);
		}*/
		
		if (use_quasi_newton_coordinate_update) {
			ierr = SNESSetJacobian(snesIC,JA,B,FormJacobian_Stokes_QCoords,user);CHKERRQ(ierr);
		} else {
			ierr = SNESSetJacobian(snesIC,JA,B,FormJacobian_Stokes,user);CHKERRQ(ierr);
		}
		
		
		ierr = SNESSetFromOptions(snesIC);CHKERRQ(ierr);
		ierr = SNESGetTolerances(snesIC,0,0,0,&maxit,0);CHKERRQ(ierr);
		if (maxit==0) {
			PetscPrintf(PETSC_COMM_WORLD,"snesIC: skipping initial solve for Newton\n");
		}
		ierr = pTatinSetSNESContMonitor(snesIC,user);CHKERRQ(ierr);
		
		
		ierr = SNESGetKSP(snesIC,&kspIC);CHKERRQ(ierr);
		ierr = KSPGetPC(kspIC,&pcIC);CHKERRQ(ierr);
		
		ierr = DMCompositeGetGlobalISs(multipys_pack,&isg);CHKERRQ(ierr);
		ierr = PCFieldSplitSetIS(pcIC,"u",isg[0]);CHKERRQ(ierr);
		ierr = PCFieldSplitSetIS(pcIC,"p",isg[1]);CHKERRQ(ierr);
		
		/* configure uu split for galerkin multi-grid */
		ierr = StokesKSPFSMGuSetUp(multipys_pack,kspIC,nlevels,interpolation);CHKERRQ(ierr);
		
		ierr = PetscOptionsGetInt(PETSC_NULL,"-continuation_M",&user->continuation_M,PETSC_NULL);CHKERRQ(ierr);
		user->continuation_m = 0;
		if (maxit>0) {
			PetscInt max_its,max_lits,max_nfuncevals;
			
			max_its = max_lits = max_nfuncevals = 0;
			for (k=0; k<=user->continuation_M; k++) {
				char contname[256];
				PetscInt its,lits,nfuncevals;
				PetscScalar alpha_m;
				Vec velocity_last;
				
				
				alpha_m = (double)(user->continuation_m)/( (double)(user->continuation_M) );
				PetscPrintf(PETSC_COMM_WORLD,"[%d] alpha_m = %lf \n", k,alpha_m);
				/* boundary conditions */
				ierr = pTatin2d_ModelApplyBoundaryCondition(user);CHKERRQ(ierr);
				
				ierr = PetscGetTime(&t0);CHKERRQ(ierr);
				ierr = SNESSolve(snesIC,0,X);CHKERRQ(ierr);
				ierr = PetscGetTime(&t1);CHKERRQ(ierr);
				user->continuation_m++;
				
				ierr = SNESGetIterationNumber(snesIC,&its);CHKERRQ(ierr);
				ierr = SNESGetLinearSolveIterations(snesIC,&lits);CHKERRQ(ierr);
				ierr = SNESGetNumberFunctionEvals(snesIC,&nfuncevals);CHKERRQ(ierr);
				PetscPrintf(PETSC_COMM_WORLD,"SNES_continuation[%d/%d](StokesIC) Statistics \n", k,user->continuation_M);
				PetscPrintf(PETSC_COMM_WORLD,"  nonlinear its    = %D \n", its);
				PetscPrintf(PETSC_COMM_WORLD,"  total linear its = %D \n", lits);
				PetscPrintf(PETSC_COMM_WORLD,"  total func evals = %D \n", nfuncevals);
				
				sprintf(contname,"stkct_%1.2d",k);
				//ierr = pTatinLogBasicKSP(user,contname,kspIC);CHKERRQ(ierr);
				ierr = pTatinLogBasicSNES(user,contname,snesIC);CHKERRQ(ierr);
				ierr = pTatinLogBasicCPUtime(user,contname,(double)(t1-t0));CHKERRQ(ierr);
				ierr = pTatinLogBasicStokesSolution(user,multipys_pack,X);CHKERRQ(ierr);
				ierr = pTatinLogBasicStokesSolutionResiduals(user,snesIC,multipys_pack,X);CHKERRQ(ierr);
				
				max_its        += its;
				max_lits       += lits;
				max_nfuncevals += nfuncevals;
					ierr = pTatin2d_ModelOutput(user,X,contname);CHKERRQ(ierr);
			}	
			PetscPrintf(PETSC_COMM_WORLD,"SNES_continuation[final](StokesIC) Statistics \n");
			PetscPrintf(PETSC_COMM_WORLD,"  sum nonlinear its    = %D \n", max_its);
			PetscPrintf(PETSC_COMM_WORLD,"  sum total linear its = %D \n", max_lits);
			PetscPrintf(PETSC_COMM_WORLD,"  sum total func evals = %D \n", max_nfuncevals);
		}
		/* force the the counters to match so alpha_m = 1 on subsequent solves */
		user->continuation_m = user->continuation_M;
		
		ierr = MatDestroy(&JA);CHKERRQ(ierr);CHKERRQ(ierr);
		ierr = SNESDestroy(&snesIC);CHKERRQ(ierr);
		ierr = ISDestroy(&isg[0]);CHKERRQ(ierr);
		ierr = ISDestroy(&isg[1]);CHKERRQ(ierr);
		ierr = PetscFree(isg);CHKERRQ(ierr);
	ierr = pTatin2d_ModelOutput(user,X,"continuation");CHKERRQ(ierr);
	}
	  break; 
      case FRICTIONAL_CONTINUATION : 	
	  {
		PetscInt k,maxit;
		SNES snesIC;
		Mat JA,JB;
		KSP kspIC;
		PC pcIC;
		MatStructure flg;
		SurfaceRheologyType model_surf_rheo_type;
		RheologyType model_rheo_type;
		PetscInt its,lits,nfuncevals;
		user->continuation_m =0;
		
		ierr = DMCompositeStokesCreateNest(multipys_pack,Auu,Aup,Apu,PETSC_NULL,&JA);CHKERRQ(ierr);
		
		ierr = SNESCreate(PETSC_COMM_WORLD,&snesIC);CHKERRQ(ierr);
		ierr = SNESSetOptionsPrefix(snesIC,"ic_");CHKERRQ(ierr);
		if (use_quasi_newton_coordinate_update) {
			ierr = SNESSetFunction(snesIC,F,FormFunction_Stokes_QCoords,user);CHKERRQ(ierr);
			ierr = SNESSetJacobian(snesIC,JA,B,FormJacobian_Stokes_QCoords,user);CHKERRQ(ierr);
		} else {
			ierr = SNESSetFunction(snesIC,F,FormFunction_Stokes,user);CHKERRQ(ierr);
			ierr = SNESSetJacobian(snesIC,JA,B,FormJacobian_Stokes,user);CHKERRQ(ierr);
		}
		
		ierr = SNESSetType(snesIC,SNESKSPONLY);CHKERRQ(ierr);
		ierr = SNESSetFromOptions(snesIC);CHKERRQ(ierr);
		ierr = SNESGetTolerances(snesIC,0,0,0,&maxit,0);CHKERRQ(ierr);
		if (maxit==0) {
			PetscPrintf(PETSC_COMM_WORLD,"snesIC: skipping initial solve for Newton\n");
		}
		ierr = pTatinSetSNESContMonitor(snesIC,user);CHKERRQ(ierr);
		
		
		ierr = SNESGetKSP(snesIC,&kspIC);CHKERRQ(ierr);
		ierr = KSPGetPC(kspIC,&pcIC);CHKERRQ(ierr);
		
		ierr = DMCompositeGetGlobalISs(multipys_pack,&isg);CHKERRQ(ierr);
		ierr = PCFieldSplitSetIS(pcIC,"u",isg[0]);CHKERRQ(ierr);
		ierr = PCFieldSplitSetIS(pcIC,"p",isg[1]);CHKERRQ(ierr);
		
		/* configure uu split for galerkin multi-grid */
		ierr = StokesKSPFSMGuSetUp(multipys_pack,kspIC,nlevels,interpolation);CHKERRQ(ierr);
		
		user->continuation_m = 0;
		user->continuation_M = 0;
		
		model_rheo_type      = user->rheology_constants.rheology_type;
		model_surf_rheo_type = user->surf_rheology_constants.rheology_type;
		
		printf("*** Nonlinear continuation stage [linear viscous only] ***\n");
		user->rheology_constants.rheology_type      = RHEOLOGY_VISCOUS;
		user->surf_rheology_constants.rheology_type = SURF_RHEOLOGY_NONE;
		
		/* boundary conditions */
		ierr = pTatin2d_ModelApplyBoundaryCondition(user);CHKERRQ(ierr);
		
		ierr = PetscGetTime(&t0);CHKERRQ(ierr);
		ierr = SNESSolve(snesIC,0,X);CHKERRQ(ierr);
		ierr = PetscGetTime(&t1);CHKERRQ(ierr);
		ierr = pTatin2d_ModelOutput(user,X,"linear_stage");CHKERRQ(ierr);
		
		ierr = SNESGetIterationNumber(snesIC,&its);CHKERRQ(ierr);
		ierr = SNESGetLinearSolveIterations(snesIC,&lits);CHKERRQ(ierr);
		ierr = SNESGetNumberFunctionEvals(snesIC,&nfuncevals);CHKERRQ(ierr);
		PetscPrintf(PETSC_COMM_WORLD,"SNES_continuation(StokesIC) Statistics \n");
		PetscPrintf(PETSC_COMM_WORLD,"  nonlinear its    = %D \n", its);
		PetscPrintf(PETSC_COMM_WORLD,"  total linear its = %D \n", lits);
		PetscPrintf(PETSC_COMM_WORLD,"  total func evals = %D \n", nfuncevals);
		
		
		
		printf("*** Nonlinear continuation stage [plasticity only] ***\n");
		user->rheology_constants.rheology_type      = model_rheo_type;
		user->surf_rheology_constants.rheology_type = SURF_RHEOLOGY_NONE;
		
		//boundary conditions 
		ierr = pTatin2d_ModelApplyBoundaryCondition(user);CHKERRQ(ierr);
		
		ierr = SNESSetType(snesIC,SNESLS);CHKERRQ(ierr);
		ierr = SNESSetFromOptions(snesIC);CHKERRQ(ierr);
		ierr = PetscGetTime(&t0);CHKERRQ(ierr);
		ierr = SNESSolve(snesIC,0,X);CHKERRQ(ierr);
		ierr = PetscGetTime(&t1);CHKERRQ(ierr);
		ierr = pTatin2d_ModelOutput(user,X,"plastic_stage");CHKERRQ(ierr);
		
		
		printf("*** Nonlinear continuation stage [plasticity + friction] ***\n");
		user->rheology_constants.rheology_type      = model_rheo_type;
		user->surf_rheology_constants.rheology_type = model_surf_rheo_type;
		user->continuation_m =1;
		
		/* boundary conditions */
		ierr = pTatin2d_ModelApplyBoundaryCondition(user);CHKERRQ(ierr);
		
		ierr = SNESSetType(snesIC,SNESLS);CHKERRQ(ierr);
		ierr = SNESSetFromOptions(snesIC);CHKERRQ(ierr);
		ierr = PetscGetTime(&t0);CHKERRQ(ierr);
		ierr = SNESSolve(snesIC,0,X);CHKERRQ(ierr);
		ierr = PetscGetTime(&t1);CHKERRQ(ierr);
		ierr = pTatin2d_ModelOutput(user,X,"plastic_fricition_stage");CHKERRQ(ierr);
		
		
		
		ierr = MatDestroy(&JA);CHKERRQ(ierr);CHKERRQ(ierr);
		ierr = SNESDestroy(&snesIC);CHKERRQ(ierr);
		ierr = ISDestroy(&isg[0]);CHKERRQ(ierr);
		ierr = ISDestroy(&isg[1]);CHKERRQ(ierr);
		ierr = PetscFree(isg);CHKERRQ(ierr);
	}
      break; 
      case NONE :
            break;
      }
	
	
	/* time step based on the continuation */
	
	{			
		Vec velocity,pressure;
		PetscReal step;
		
		ierr = DMCompositeGetAccess(user->pack,X,&velocity,&pressure);CHKERRQ(ierr);
		
		ierr = SwarmUpdatePosition_ComputeCourantStep_3(user->dav,velocity,&step);CHKERRQ(ierr);
    
		ierr = pTatin2d_SetTimestep(user,"continuation_stokes_courant3",step);CHKERRQ(ierr);
		
		ierr = SwarmUpdatePosition_ComputeCourantStep(user->dav,velocity,&step);CHKERRQ(ierr);
		ierr = pTatin2d_SetTimestep(user,"continuation_stokes_courant",step);CHKERRQ(ierr);
		
		ierr = SwarmUpdatePosition_ComputeCourantStep_2(user->dav,velocity,&step);CHKERRQ(ierr);
		ierr = pTatin2d_SetTimestep(user,"continuation_stokes_courant2",step);CHKERRQ(ierr);
		
		ierr = DMCompositeRestoreAccess(user->pack,X,&velocity,&pressure);CHKERRQ(ierr);
	}
	
	/* ================================== */
	/*  THERMAL ENERGY INITIAL CONDITION  */
	{
		PetscBool active = PETSC_FALSE;
		ierr = pTatinPhysCompActivated(user,PhysComp_Energy,&active);CHKERRQ(ierr);
		
		if (active) {
			PhysCompEnergyCtx energy = user->phys_energy;
			Vec coordinates;
			ierr = VecCopy(energy->T,energy->Told);CHKERRQ(ierr);
			ierr = DMDAGetCoordinates(energy->daT,&coordinates);CHKERRQ(ierr);
			ierr = VecCopy(coordinates,energy->Xold);CHKERRQ(ierr);
		}
		/////		ierr = VecZeroEntries(energy->T);CHKERRQ(ierr);
	}
	/* ================================== */
	
	/* ================================== */
	/*  DARCY INITIAL CONDITION  */
	{
		PetscBool active = PETSC_FALSE;
		ierr = pTatinPhysCompActivated(user,PhysComp_Darcy,&active);CHKERRQ(ierr);
		
		if (active) {
			PhysCompDarcyCtx darcy = user->phys_darcy;
			Vec coordinates;
			if (user->step == 0){
			ierr = VecZeroEntries(darcy->Pf);CHKERRQ(ierr);
			ierr = VecZeroEntries(darcy->Pfold);CHKERRQ(ierr);
			}
			ierr = VecCopy(darcy->Pf,darcy->Pfold);CHKERRQ(ierr);
			
			
		}

	}
	/* ================================== */
	
	
	
	/* ========================== */
	/*  THERMAL ENERGY TIME STEP  */
#if 0
	{
		Vec velocity,pressure;
		PetscScalar supg_dt;
		
		/* map velocity vector from Q2 space onto Q1 space */
		ierr = DMCompositeGetAccess(user->pack,X,&velocity,&pressure);CHKERRQ(ierr);
		ierr = DMDAProjectVelocityQ2toQ1_2d(user->dav,velocity,user->phys_energy->daT,user->phys_energy->u_minus_V);CHKERRQ(ierr);
		ierr = DMCompositeRestoreAccess(user->pack,X,&velocity,&pressure);CHKERRQ(ierr);
		
		printf(" *** thermal time step *** \n");
		ierr = DASUPG2dComputeTimestep_qp(user->phys_energy,0.5,user->phys_energy->u_minus_V,&supg_dt);CHKERRQ(ierr);
		
		user->dt = supg_dt;
		ierr = pTatin2d_SetTimestep(user,"energy_adv_diff",supg_dt);CHKERRQ(ierr);
	}
#endif
	
	for (tk=0; tk<=user->nsteps; tk++) {
		PetscInt pre_snes_its = 1;
		SNESConvergedReason pre_snes_reason = 1;
		SNESConvergedReason snes_reason = 1;
		PetscBool use_adaptative_timestepping = PETSC_FALSE;
		PetscBool found; 
		PetscScalar fac = 1.0; 
		skip_snes = PETSC_FALSE;
		time = time + user->dt;
		
		user->time = time; 
		user->step = tk;
        
        
		
		if( user->time > user->time_max) { 
			PetscPrintf(PETSC_COMM_WORLD,"reached max requested model time (current = %1.4e, max = %1.4e) \n", user->time, user->time_max );
			break; 
		}
#if 0
        if( tk > 5) {
        ierr = QuadratureStokesGravityModelCoordinateAlignedSetValues(user->Q,0.0, 0.0);CHKERRQ(ierr);
            PetscPrintf(PETSC_COMM_WORLD,"##################SWITCHING GRAVITY OFF ##########\n" );
        }
        
#endif
		ierr = DMCompositeGetGlobalISs(multipys_pack,&isg);CHKERRQ(ierr);
		
		PetscPrintf(PETSC_COMM_WORLD,"step = %1.5d, time = %1.4e, dt = %1.4e \n", tk, time, user->dt );
		ierr = pTatinLogBasic(user);
		
		
		/* boundary conditions */
        ierr= BCListInitGlobal(user->u_bclist);CHKERRQ(ierr);
        ierr = BCListGlobalToLocal(user->u_bclist);CHKERRQ(ierr);
        
        ierr = pTatin2d_ModelApplyBoundaryCondition(user);CHKERRQ(ierr);
		ierr = pTatin2d_ModelApplyMaterialBoundaryCondition(user);CHKERRQ(ierr);
		
		if (reset_stokes_solution_each_timestep) {
			ierr = VecZeroEntries(X);CHKERRQ(ierr);
		}
        
        
		
		/* ---- start picard stage ---- */
		if (!skip_pre_snes) {
			
			/* peform a pre-solve using picard */
			ierr = SNESCreate(PETSC_COMM_WORLD,&snes_pre);CHKERRQ(ierr);
			ierr = SNESSetOptionsPrefix(snes_pre,"pre_");CHKERRQ(ierr);
			
			if (use_quasi_newton_coordinate_update) {
				ierr = SNESSetFunction(snes_pre,F,FormFunction_Stokes_QCoords,user);CHKERRQ(ierr);
			} else {
				ierr = SNESSetFunction(snes_pre,F,FormFunction_Stokes,user);CHKERRQ(ierr);
			}
			
			/*if (user->rheology_constants.rheology_type == RHEOLOGY_VISCO_ELASTIC_PLASTIC ) {
				SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"RHEOLOGY_VISCO_ELASTIC_PLASTIC is no longer supported");
				//ierr = SNESSetFunction(snes_pre,F,FormFunction_Stokes_ViscoElastoPlastic,user);CHKERRQ(ierr);  
			}*/
			
			
			if (user->pre_use_mf_stokes) {
			    if (use_quasi_newton_coordinate_update) {
				   ierr = SNESSetJacobian(snes_pre,B,B,FormJacobian_Stokes_QCoords,user);CHKERRQ(ierr);
			    } else {
				   ierr = SNESSetJacobian(snes_pre,B,B,FormJacobian_Stokes,user);CHKERRQ(ierr);
			    }
		    } else {
			    if (use_quasi_newton_coordinate_update) {
				    ierr = SNESSetJacobian(snes_pre,A,B,FormJacobian_Stokes_QCoords,user);CHKERRQ(ierr);
			    } else {
				    ierr = SNESSetJacobian(snes_pre,A,B,FormJacobian_Stokes,user);CHKERRQ(ierr);
			    }
		    }
						
			ierr = SNESSetTolerances(snes_pre,1.0e-4,1.0e-4,PETSC_DEFAULT,1000,1e9);CHKERRQ(ierr);
			ierr = SNESSetType(snes_pre,SNESLS);CHKERRQ(ierr);
			ierr = SNESLineSearchSet(snes_pre,SNESLineSearchNo,PETSC_NULL);CHKERRQ(ierr);
			ierr = SNESSetFromOptions(snes_pre);CHKERRQ(ierr);
			use_adaptative_ls = PETSC_FALSE; 
			ierr = PetscOptionsGetBool("pre_snes_","-ls_adaptative",&use_adaptative_ls,PETSC_NULL);CHKERRQ(ierr);
			if (use_adaptative_ls){
				ierr = SNESLineSearchSet(snes_pre,SNESLineSearchAdaptiveRelaxed,PETSC_NULL);CHKERRQ(ierr);
			}
			
			
			/* configure for fieldsplit */
			ierr = SNESGetKSP(snes_pre,&ksp);CHKERRQ(ierr);
			ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
			ierr = PCFieldSplitSetIS(pc,"u",isg[0]);CHKERRQ(ierr);
			ierr = PCFieldSplitSetIS(pc,"p",isg[1]);CHKERRQ(ierr);
			
			/* configure uu split for galerkin multi-grid */
			ierr = StokesKSPFSMGuSetUp(multipys_pack,ksp,nlevels,interpolation);CHKERRQ(ierr);
			
			ierr = pTatinSetSNESMonitor(snes_pre,user);CHKERRQ(ierr);
			
			/* ================================================ */
			/* do a pre-pre solve for the current configuration */
			
			ierr = SNESGetKSP(snes_pre,&ksp);CHKERRQ(ierr);
			
			if (force_pre_pre_solve)
			{
				PetscReal nrm;
				Vec dX;
				
				PetscPrintf(PETSC_COMM_WORLD,"** Performing pre-pre-solve **\n");
				//ierr = SNESGetSolutionUpdate(snes_pre,&dX);CHKERRQ(ierr);
				//ierr = SNESGetSolution(snes_pre,&_X);CHKERRQ(ierr);
				//ierr = VecZeroEntries(dX);CHKERRQ(ierr);
				
				ierr = SNESComputeFunction(snes_pre,X,F);CHKERRQ(ierr);
				ierr= VecNorm(F,NORM_2,&nrm); CHKERRQ(ierr);
				PetscPrintf(PETSC_COMM_WORLD,"** norm(F) %e\n", nrm);
				
				ierr = VecScale(F,-1.0);CHKERRQ(ierr);
				ierr = VecDuplicate(F,&dX);CHKERRQ(ierr);
				ierr= VecNorm(dX,NORM_2,&nrm); CHKERRQ(ierr);
				PetscPrintf(PETSC_COMM_WORLD,"** norm(dX) %e\n", nrm);
				
				ierr = PetscGetTime(&t0);CHKERRQ(ierr);
				ierr = KSPSolve(ksp,F,dX);CHKERRQ(ierr);
				ierr = PetscGetTime(&t1);CHKERRQ(ierr);
				ierr = VecAXPY(X,1.0,dX);CHKERRQ(ierr);
				ierr = VecDestroy(&dX);CHKERRQ(ierr);
				
				ierr = pTatinLogBasicKSP(user,"stk_pre",ksp);CHKERRQ(ierr);
				ierr = pTatinLogBasicCPUtime(user,"stk_pre",(double)(t1-t0));CHKERRQ(ierr);
				ierr = pTatinLogBasicStokesSolution(user,multipys_pack,X);CHKERRQ(ierr);
			}
			
			
			/* ================================================ */
			
			
			PetscPrintf(PETSC_COMM_WORLD,"** Performing pre-solve [picard] **\n");
			ierr = PetscGetTime(&t0);CHKERRQ(ierr);
			ierr = SNESSolve(snes_pre,PETSC_NULL,X);CHKERRQ(ierr);
			ierr = PetscGetTime(&t1);CHKERRQ(ierr);
			ierr = SNESGetIterationNumber(snes_pre,&pre_snes_its);CHKERRQ(ierr);
			SNESGetConvergedReason(snes_pre,&pre_snes_reason);
			if (pre_snes_reason < 0){
				skip_snes = PETSC_FALSE;
				ierr = PetscOptionsGetBool(PETSC_NULL,"-skipnewton_on_picard_failure",&skip_snes,PETSC_NULL);CHKERRQ(ierr);
			}
			
			if (user->solverstatistics) {
				PetscInt its,lits,nfuncevals;
				ierr = SNESGetIterationNumber(snes_pre,&its);CHKERRQ(ierr);
				ierr = SNESGetLinearSolveIterations(snes_pre,&lits);CHKERRQ(ierr);
				ierr = SNESGetNumberFunctionEvals(snes_pre,&nfuncevals);CHKERRQ(ierr);
				PetscPrintf(PETSC_COMM_WORLD,"PRE-STAGE SNES(Stokes) Statistics \n");
				PetscPrintf(PETSC_COMM_WORLD,"  nonlinear its    = %D \n", its);
				PetscPrintf(PETSC_COMM_WORLD,"  total linear its = %D \n", lits);
				PetscPrintf(PETSC_COMM_WORLD,"  total func evals = %D \n", nfuncevals);
				
				pTatin2dSolverStatistics(user,snes_pre,X);
			}
			//ierr = pTatinLogBasicKSP(user,"stk_pcd",ksp);CHKERRQ(ierr);
			ierr = pTatinLogBasicSNES(user,"stk_pcd",snes_pre);CHKERRQ(ierr);
			ierr = pTatinLogBasicCPUtime(user,"stk_pcd",(double)(t1-t0));CHKERRQ(ierr);
			ierr = pTatinLogBasicStokesSolution(user,multipys_pack,X);CHKERRQ(ierr);
			ierr = pTatinLogBasicStokesSolutionResiduals(user,snes_pre,multipys_pack,X);CHKERRQ(ierr);
	
			ierr = SNESDestroy(&snes_pre);CHKERRQ(ierr);
		}
		/* ---- end picard stage ---- */
		
		/* creation */
		ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
		ierr = SNESSetErrorIfNotConverged(snes,PETSC_FALSE);CHKERRQ(ierr);
		
		if (use_quasi_newton_coordinate_update) {
			ierr = SNESSetFunction(snes,F,FormFunction_Stokes_QCoords,user);CHKERRQ(ierr);
		} else {
			ierr = SNESSetFunction(snes,F,FormFunction_Stokes,user);CHKERRQ(ierr);
		}
		
		/*if (user->rheology_constants.rheology_type == RHEOLOGY_VISCO_ELASTIC_PLASTIC ) {
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"RHEOLOGY_VISCO_ELASTIC_PLASTIC is no longer supported");
			//	ierr = SNESSetFunction(snes,F,FormFunction_Stokes_ViscoElastoPlastic,user);CHKERRQ(ierr);  
		}*/
		
		if (user->use_mf_stokes) {
			if (use_quasi_newton_coordinate_update) {
				ierr = SNESSetJacobian(snes,B,B,FormJacobian_Stokes_QCoords,user);CHKERRQ(ierr);
			} else {
				ierr = SNESSetJacobian(snes,B,B,FormJacobian_Stokes,user);CHKERRQ(ierr);
			}
		} else {
			if (use_quasi_newton_coordinate_update) {
				ierr = SNESSetJacobian(snes,A,B,FormJacobian_Stokes_QCoords,user);CHKERRQ(ierr);
			} else {
				ierr = SNESSetJacobian(snes,A,B,FormJacobian_Stokes,user);CHKERRQ(ierr);
			}
		}
		ierr = SNESSetTolerances(snes,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT,1000,1e9);CHKERRQ(ierr);
		ierr = SNESSetType(snes,SNESLS);CHKERRQ(ierr);
		ierr = SNESLineSearchSet(snes,SNESLineSearchNo,PETSC_NULL);CHKERRQ(ierr);
		ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
		
		
		if (tk>1){
			PetscReal rtol;
			PetscInt  max_it; 
			SNESGetTolerances(snes,PETSC_NULL,&rtol,PETSC_NULL,&max_it,PETSC_NULL);
			ierr = PetscOptionsGetReal(PETSC_NULL,"-tk_snes_rtol",&rtol,PETSC_NULL);CHKERRQ(ierr);  
			if (skip_snes){
				max_it = 0; 
			}
			SNESSetTolerances(snes,PETSC_DEFAULT,rtol,PETSC_DEFAULT,max_it,PETSC_DEFAULT);
		}
		
		
		/* configure for fieldsplit */
		ierr = SNESGetKSP(snes,&ksp);CHKERRQ(ierr);
		ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
		
		ierr = PCFieldSplitSetIS(pc,"u",isg[0]);CHKERRQ(ierr);
		ierr = PCFieldSplitSetIS(pc,"p",isg[1]);CHKERRQ(ierr);
		
		/* configure uu split for galerkin multi-grid */
		ierr = StokesKSPFSMGuSetUp(multipys_pack,ksp,nlevels,interpolation);CHKERRQ(ierr);
		
		ierr = pTatinSetSNESMonitor(snes,user);CHKERRQ(ierr);
		
		PetscPrintf(PETSC_COMM_WORLD,"** Performing solve [snes] **\n");
		ierr = PetscGetTime(&t0);CHKERRQ(ierr);
		ierr = SNESSolve(snes,PETSC_NULL,X);CHKERRQ(ierr);
		ierr = PetscGetTime(&t1);CHKERRQ(ierr);
		
		/* statistics */
		if (user->solverstatistics) {
			PetscInt its,lits,nfuncevals;
			ierr = SNESGetIterationNumber(snes,&its);CHKERRQ(ierr);
			ierr = SNESGetLinearSolveIterations(snes,&lits);CHKERRQ(ierr);
			ierr = SNESGetNumberFunctionEvals(snes,&nfuncevals);CHKERRQ(ierr);
			PetscPrintf(PETSC_COMM_WORLD,"SNES(Stokes) Statistics \n");
			PetscPrintf(PETSC_COMM_WORLD,"  nonlinear its    = %D \n", its);
			PetscPrintf(PETSC_COMM_WORLD,"  total linear its = %D \n", lits);
			PetscPrintf(PETSC_COMM_WORLD,"  total func evals = %D \n", nfuncevals);
			
			pTatin2dSolverStatistics(user,snes,X);
		}
		//ierr = pTatinLogBasicKSP(user,"stk_nwt",ksp);CHKERRQ(ierr);
		ierr = pTatinLogBasicSNES(user,"stk_nwt",snes);CHKERRQ(ierr);
		ierr = pTatinLogBasicCPUtime(user,"stk_nwt",(double)(t1-t0));CHKERRQ(ierr);
		ierr = pTatinLogBasicStokesSolution(user,multipys_pack,X);CHKERRQ(ierr);
		ierr = pTatinLogBasicStokesSolutionResiduals(user,snes,multipys_pack,X);CHKERRQ(ierr);
		ierr = pTatinLogBasicMaterialPoints(user,"stokes",user->db);CHKERRQ(ierr);
		ierr = SNESGetConvergedReason(snes,&snes_reason);
		
		
		/*time step based on convergence of picard iterations for current dt*/
		
		
		
		
		
		/* time step based on the picard/snes solve for stokes */

		{			
			Vec velocity,pressure;
			PetscReal step;
			
			ierr = DMCompositeGetAccess(user->pack,X,&velocity,&pressure);CHKERRQ(ierr);
			
			ierr = SwarmUpdatePosition_ComputeCourantStep_3(user->dav,velocity,&step);CHKERRQ(ierr);
			user->dt = step;
			ierr = pTatin2d_SetTimestep(user,"timestepper_stokes_courant3",step);CHKERRQ(ierr);
			
			ierr = SwarmUpdatePosition_ComputeCourantStep(user->dav,velocity,&step);CHKERRQ(ierr);
			ierr = pTatin2d_SetTimestep(user,"timestepper_stokes_courant",step);CHKERRQ(ierr);
			
			ierr = SwarmUpdatePosition_ComputeCourantStep_2(user->dav,velocity,&step);CHKERRQ(ierr);
			ierr = pTatin2d_SetTimestep(user,"timestepper_stokes_courant2",step);CHKERRQ(ierr);
			
			ierr = DMCompositeRestoreAccess(user->pack,X,&velocity,&pressure);CHKERRQ(ierr);
		}
#if 0
		{	
			Vec velocity,pressure;
			PetscReal step;
			PetscReal step0 = user->dt;
			
			ierr = DMCompositeGetAccess(user->pack,X,&velocity,&pressure);CHKERRQ(ierr);
			
			ierr = SwarmUpdatePosition_ComputeCourantStep_3(user->dav,velocity,&step);CHKERRQ(ierr);
			ierr = pTatin2d_SetTimestep(user,"timestepper_stokes_courant3",step);CHKERRQ(ierr);
			
			ierr = SwarmUpdatePosition_ComputeCourantStep(user->dav,velocity,&step);CHKERRQ(ierr);
			ierr = pTatin2d_SetTimestep(user,"timestepper_stokes_courant",step);CHKERRQ(ierr);
			
			ierr = SwarmUpdatePosition_ComputeCourantStep_2(user->dav,velocity,&step);CHKERRQ(ierr);
			ierr = pTatin2d_SetTimestep(user,"timestepper_stokes_courant2",step);CHKERRQ(ierr);
		
			ierr  = PetscOptionsGetBool(PETSC_NULL,"-use_adaptative_time_stepping",&use_adaptative_timestepping,&found);CHKERRQ(ierr);
			
			if (use_adaptative_timestepping){
				PetscInt    pre_snes_max_adapt_its = 10 ;
				PetscInt    pre_snes_min_adapt_its = 8; 
				PetscScalar pre_snes_dt_reduc_factor = 2.0 ;
				PetscScalar pre_snes_dt_increase_factor = 2.0;
				 if (pre_snes_reason < 0){
				fac = fac/pre_snes_dt_reduc_factor;
				user->dt = step0;
				 }else{
				 
				 user->dt = step0*2;
				 if (user->dt > step){
				    user->dt = step;
				    }
				 }
				 	ierr = DMCompositeRestoreAccess(user->pack,X,&velocity,&pressure);CHKERRQ(ierr);
		}
		}	
#endif				
		
		
		
		/* time step based on the picard/snes solve for stokes for surface */
		
		{			
			Vec velocity,pressure;
			PetscReal step;
			
			ierr = DMCompositeGetAccess(user->pack,X,&velocity,&pressure);CHKERRQ(ierr);
			ierr  = PetscOptionsGetBool(PETSC_NULL,"-skip_stokes_surface_courant",&skip_stokes_surface_courant,&found);CHKERRQ(ierr);
            if (skip_stokes_surface_courant == PETSC_FALSE) {
			ierr = UpdateMeshGeometry_SurfaceCourantStep(user->dav,velocity,user->max_disp_y,user->courant_surf, &step);CHKERRQ(ierr);
			ierr = pTatin2d_SetTimestep(user,"timestepper_stokes_surface_courant",step);CHKERRQ(ierr);
			}
			ierr = DMCompositeRestoreAccess(user->pack,X,&velocity,&pressure);CHKERRQ(ierr);
		}
		

		
		
#if 0		
		/* ====================== */
		/*  THERMAL ENERGY SOLVE  */
		{
			PetscBool active=PETSC_FALSE;
			ierr = pTatinPhysCompActivated(user,PhysComp_Energy,&active);CHKERRQ(ierr);
			if (active){
				
				Mat JE;
				Vec FE,velocity,pressure,coordinates;
				Vec T,Told,delta_T;
				PhysCompEnergyCtx energy;
				MatStructure mstr;
				KSP kspT;
				PetscScalar supg_dt;
				
				energy = user->phys_energy;
				T      = energy->T;
				Told   = energy->Told;
				
				ierr = DMCreateGlobalVector(energy->daT,&FE);CHKERRQ(ierr);
				ierr = DMGetMatrix(energy->daT,MATAIJ,&JE);CHKERRQ(ierr);
				
				ierr = DMDAProjectCoordinatesQ2toQ1_2d(user->dav,energy->daT);
				
				/* map velocity vector from Q2 space onto Q1 space */
				ierr = DMCompositeGetAccess(user->pack,X,&velocity,&pressure);CHKERRQ(ierr);
				ierr = DMDAProjectVelocityQ2toQ1_2d(user->dav,velocity,energy->daT,energy->u_minus_V);CHKERRQ(ierr);
				ierr = DMCompositeRestoreAccess(user->pack,X,&velocity,&pressure);CHKERRQ(ierr);
				
				
				/* ========================== */
				/*  THERMAL ENERGY TIME STEP  */
				/*
				 NOTE: The time step computed here is using the full fluid velocity field - not the ALE velocity.
				 This is the "worst" case velocity field we can consdier.
				 */
				ierr = DASUPG2dComputeTimestep_qp(energy,1.0,energy->u_minus_V,&supg_dt);CHKERRQ(ierr);
				ierr = pTatin2d_SetTimestep(user,"timestepper_energy_adv_diff",supg_dt);CHKERRQ(ierr);
				PetscPrintf(PETSC_COMM_WORLD," ****   using dt in thermal solver = %1.4e \n", user->dt );
				
				/* update V */
				/*
				 ierr = VecCopy(energy->T,energy->Told);CHKERRQ(ierr);
				 ierr = DMDAGetCoordinates(daT,&coordinates);CHKERRQ(ierr);
				 ierr = VecCopy(coordinates,energy->Xold);CHKERRQ(ierr);
				 */
				/*
				 u - V = u - (X_current - X_old)/dt
				 = (dt.u - X_current + X_old)/dt
				 */
				ierr = VecScale(energy->u_minus_V,user->dt);CHKERRQ(ierr);
				ierr = DMDAGetCoordinates(energy->daT,&coordinates);CHKERRQ(ierr);
				ierr = VecAXPY(energy->u_minus_V,-1.0,coordinates);CHKERRQ(ierr);
				ierr = VecAXPY(energy->u_minus_V, 1.0,energy->Xold);CHKERRQ(ierr);
				ierr = VecScale(energy->u_minus_V,1.0/user->dt);CHKERRQ(ierr);
				
				/* if you want to test diffusion */
				//			ierr = VecZeroEntries(energy->u_minus_V);CHKERRQ(ierr);
				
				
				
				/* this segment is written in defect correction form */
				/* guess for new temperature */
				/////			ierr = VecCopy(energy->Told,T);CHKERRQ(ierr);
				
				ierr = SUPGFormJacobian_qp(user->time,T,user->dt,&JE,&JE,&mstr,(void*)energy);CHKERRQ(ierr);
				ierr = SUPGFormFunction_qp(user->time,T,user->dt,FE,(void*)energy);CHKERRQ(ierr);
				
				ierr = KSPCreate(PETSC_COMM_WORLD,&kspT);CHKERRQ(ierr);
				ierr = KSPSetOptionsPrefix(kspT,"T_");CHKERRQ(ierr);
				ierr = KSPSetOperators(kspT,JE,JE,SAME_NONZERO_PATTERN);CHKERRQ(ierr);
				ierr = KSPSetFromOptions(kspT);CHKERRQ(ierr);
				//ierr = KSPSetInitialGuessNonzero(kspT,PETSC_TRUE);CHKERRQ(ierr);
				
				ierr = VecDuplicate(T,&delta_T);CHKERRQ(ierr);
				ierr = VecScale(FE,-1.0);CHKERRQ(ierr);
				ierr = PetscGetTime(&t0);CHKERRQ(ierr);
				ierr = KSPSolve(kspT,FE,delta_T);CHKERRQ(ierr);
				ierr = PetscGetTime(&t1);CHKERRQ(ierr);
				
				ierr = pTatinLogBasicKSP(user,"energy",kspT);CHKERRQ(ierr);
				ierr = pTatinLogBasicCPUtime(user,"energy",(double)(t1-t0));CHKERRQ(ierr);
				
				ierr = VecAXPY(T, 1.0, delta_T);CHKERRQ(ierr); /* phi = phi + delta_phi */
				
				/* clean up */
				ierr = VecDestroy(&delta_T);CHKERRQ(ierr);
				ierr = KSPDestroy(&kspT);CHKERRQ(ierr);
				ierr = MatDestroy(&JE);CHKERRQ(ierr);
				ierr = VecDestroy(&FE);CHKERRQ(ierr);
			}
		}
		/* ====================== */
#endif		
		
		ierr = pTatin_UpdateRheologyNonlinearities(user,X);CHKERRQ(ierr);		
		
/* ====================== */
		/*  DARCY SOLVE  */
		{
			PetscBool active=PETSC_FALSE;
			ierr = pTatinPhysCompActivated(user,PhysComp_Darcy,&active);CHKERRQ(ierr);
			if (active){
				int npoints;
				Mat JE;
				Vec FE,velocity,pressure,coordinates;
				Vec Pf,Pfold,delta_Pf;
				PhysCompDarcyCtx darcy;
				MatStructure mstr;
				KSP kspPf;
                DataField       PField_darcy, PField_std;
		        MPntStd         *mp_std;
		        MPntPDarcy      *mp_darcy;
		
				
				
				
				darcy = user->phys_darcy;
				Pf      = darcy->Pf;
				Pfold   = darcy->Pfold;
				
				 ierr = DMCreateGlobalVector(darcy->daPf,&FE);CHKERRQ(ierr);
				 ierr = DMGetMatrix(darcy->daPf,MATAIJ,&JE);CHKERRQ(ierr);
                
                 ierr = DMDAProjectCoordinatesQ2toQ1_2d(user->dav,darcy->daPf);
                
		         ierr = SwarmUpdateProperties_MPntPDarcy(user->db,user, X);CHKERRQ(ierr);
		         DataBucketGetDataFieldByName(user->db, MPntStd_classname      , &PField_std);
		         DataBucketGetDataFieldByName(user->db, MPntPDarcy_classname   , &PField_darcy);
                 DataBucketGetSizes(user->db,&npoints,PETSC_NULL,PETSC_NULL);
		         mp_std     = PField_std->data; /* should write a function to do this */
		 		 mp_darcy   = PField_darcy->data; /* should write a function to do this */

				 ierr = SwarmUpdateGaussPropertiesLocalL2Projection_P0_MPntPDarcy(npoints,mp_std,mp_darcy,darcy->daPf,darcy->Q);CHKERRQ(ierr);
		         
				ierr = FormJacobian_Darcy_qp(user->time,Pf,user->dt,&JE,&JE,&mstr,(void*)darcy);CHKERRQ(ierr);
				ierr = FormFunction_Darcy_qp(user->time,Pf,user->dt,FE,(void*)darcy);CHKERRQ(ierr);
				
				ierr = KSPCreate(PETSC_COMM_WORLD,&kspPf);CHKERRQ(ierr);
				ierr = KSPSetOptionsPrefix(kspPf,"Pf_");CHKERRQ(ierr);
				ierr = KSPSetOperators(kspPf,JE,JE,SAME_NONZERO_PATTERN);CHKERRQ(ierr);
				ierr = KSPSetFromOptions(kspPf);CHKERRQ(ierr);
				//ierr = KSPSetInitialGuessNonzero(kspPf,PETSC_TRUE);CHKERRQ(ierr);
				
				ierr = VecDuplicate(Pf,&delta_Pf);CHKERRQ(ierr);
				ierr = VecScale(FE,-1.0);CHKERRQ(ierr);
				ierr = PetscGetTime(&t0);CHKERRQ(ierr);
				ierr = KSPSolve(kspPf,FE,delta_Pf);CHKERRQ(ierr);
				ierr = PetscGetTime(&t1);CHKERRQ(ierr);
				
				ierr = pTatinLogBasicKSP(user,"darcy",kspPf);CHKERRQ(ierr);
				ierr = pTatinLogBasicCPUtime(user,"darcy",(double)(t1-t0));CHKERRQ(ierr);
				
				ierr = VecAXPY(Pf, 1.0, delta_Pf);CHKERRQ(ierr); /* phi = phi + delta_phi */
				
				/* clean up */
				ierr = VecDestroy(&delta_Pf);CHKERRQ(ierr);
				ierr = KSPDestroy(&kspPf);CHKERRQ(ierr);
				ierr = MatDestroy(&JE);CHKERRQ(ierr);
				ierr = VecDestroy(&FE);CHKERRQ(ierr);
			}
		}
		/* ====================== */		
		
		
		/* update tracers properties */
		if (passive_tracers){
		ierr = pTatin2dUpdatePassiveMarkers(user,X);CHKERRQ(ierr);
		}
		
		
	
		
		
		/* ADVECT MARKERS */
		{
			int npoints;
			MPntStd *mp_std;
			DataField PField;
			Vec velocity,pressure;
			
			DataBucketGetSizes(user->db,&npoints,PETSC_NULL,PETSC_NULL);
			DataBucketGetDataFieldByName(user->db, MPntStd_classname ,&PField);
			mp_std = PField->data;
			
			ierr = DMCompositeGetAccess(user->pack,X,&velocity,&pressure);CHKERRQ(ierr);
			ierr = SwarmUpdatePosition_MPntStd_Euler(user->dav,velocity,user->dt,npoints,mp_std);CHKERRQ(ierr);
			ierr = DMCompositeRestoreAccess(user->pack,X,&velocity,&pressure);CHKERRQ(ierr);
		}
		
		
		/* ADVECT Passive tracers */
		if (passive_tracers)
		{
			int npoints;
			MPntStd *mp_std;
			DataField PField;
			Vec velocity,pressure;
			
			DataBucketGetSizes(user->db_passive,&npoints,PETSC_NULL,PETSC_NULL);
			DataBucketGetDataFieldByName(user->db_passive, MPntStd_classname ,&PField);
			mp_std = PField->data;
			
			ierr = DMCompositeGetAccess(user->pack,X,&velocity,&pressure);CHKERRQ(ierr);
			ierr = SwarmUpdatePosition_MPntStd_Euler(user->dav,velocity,user->dt,npoints,mp_std);CHKERRQ(ierr);
			ierr = DMCompositeRestoreAccess(user->pack,X,&velocity,&pressure);CHKERRQ(ierr);
		}
		
							/* ======================= */
		/*  THERMAL ENERGY UPDATE  */
		{ PetscBool active;
			ierr = pTatinPhysCompActivated(user,PhysComp_Energy,&active);CHKERRQ(ierr);
			if (active) {
				PhysCompEnergyCtx energy = user->phys_energy;
				Vec coordinates;
				
				ierr = VecCopy(energy->T,energy->Told);CHKERRQ(ierr);
				ierr = DMDAGetCoordinates(energy->daT,&coordinates);CHKERRQ(ierr);
				ierr = VecCopy(coordinates,energy->Xold);CHKERRQ(ierr);
			}
		}
		/* ======================= */

		
		/* UPDATE MESH - advect + remesh */
		//
		ierr = pTatin2d_ModelUpdateMeshGeometry(user,X);CHKERRQ(ierr);
		ierr = QuadratureStokesCoordinateSetUp(user->Q,user->dav);CHKERRQ(ierr);
		for (e=0; e<QUAD_EDGES; e++) {
			ierr = SurfaceQuadratureStokesGeometrySetUp(user->surfQ[e],user->dav);CHKERRQ(ierr);
		}
		//
		
		{
		 PetscBool friction = PETSC_FALSE;
		 PetscOptionsGetBool("G2008_","-add_fric_layer",&friction,0);
		  if (friction){ 
		    pTatin2d_ModelSetMarkerIndexFricLayer_G2008(user);
		  }
		}
		
				{
			PetscBool active=PETSC_FALSE;
			ierr = pTatinPhysCompActivated(user,PhysComp_Energy,&active);CHKERRQ(ierr);
			if (active){
				
				Mat JE;
				Vec FE,velocity,pressure,coordinates;
				Vec T,Told,delta_T;
				PhysCompEnergyCtx energy;
				MatStructure mstr;
				KSP kspT;
				PetscScalar supg_dt;
				
				energy = user->phys_energy;
				T      = energy->T;
				Told   = energy->Told;
				
				ierr = DMCreateGlobalVector(energy->daT,&FE);CHKERRQ(ierr);
				ierr = DMGetMatrix(energy->daT,MATAIJ,&JE);CHKERRQ(ierr);
				
				ierr = DMDAProjectCoordinatesQ2toQ1_2d(user->dav,energy->daT);
				
				/* map velocity vector from Q2 space onto Q1 space */
				ierr = DMCompositeGetAccess(user->pack,X,&velocity,&pressure);CHKERRQ(ierr);
				ierr = DMDAProjectVelocityQ2toQ1_2d(user->dav,velocity,energy->daT,energy->u_minus_V);CHKERRQ(ierr);
				ierr = DMCompositeRestoreAccess(user->pack,X,&velocity,&pressure);CHKERRQ(ierr);
				
				
				/* ========================== */
				/*  THERMAL ENERGY TIME STEP  */
				/*
				 NOTE: The time step computed here is using the full fluid velocity field - not the ALE velocity.
				 This is the "worst" case velocity field we can consdier.
				 */
				ierr = DASUPG2dComputeTimestep_qp(energy,1.0,energy->u_minus_V,&supg_dt);CHKERRQ(ierr);
				ierr = pTatin2d_SetTimestep(user,"timestepper_energy_adv_diff",supg_dt);CHKERRQ(ierr);
				PetscPrintf(PETSC_COMM_WORLD," ****   using dt in thermal solver = %1.4e \n", user->dt );
				
				/* update V */
				/*
				 ierr = VecCopy(energy->T,energy->Told);CHKERRQ(ierr);
				 ierr = DMDAGetCoordinates(daT,&coordinates);CHKERRQ(ierr);
				 ierr = VecCopy(coordinates,energy->Xold);CHKERRQ(ierr);
				 */
				/*
				 u - V = u - (X_current - X_old)/dt
				 = (dt.u - X_current + X_old)/dt
				 */
				ierr = VecScale(energy->u_minus_V,user->dt);CHKERRQ(ierr);
				ierr = DMDAGetCoordinates(energy->daT,&coordinates);CHKERRQ(ierr);
				ierr = VecAXPY(energy->u_minus_V,-1.0,coordinates);CHKERRQ(ierr);
				ierr = VecAXPY(energy->u_minus_V, 1.0,energy->Xold);CHKERRQ(ierr);
				ierr = VecScale(energy->u_minus_V,1.0/user->dt);CHKERRQ(ierr);
				
				/* if you want to test diffusion */
				//			ierr = VecZeroEntries(energy->u_minus_V);CHKERRQ(ierr);
				
				
				
				/* this segment is written in defect correction form */
				/* guess for new temperature */
				/////			ierr = VecCopy(energy->Told,T);CHKERRQ(ierr);
				
				ierr = SUPGFormJacobian_qp(user->time,T,user->dt,&JE,&JE,&mstr,(void*)energy);CHKERRQ(ierr);
				ierr = SUPGFormFunction_qp(user->time,T,user->dt,FE,(void*)energy);CHKERRQ(ierr);
				
				ierr = KSPCreate(PETSC_COMM_WORLD,&kspT);CHKERRQ(ierr);
				ierr = KSPSetOptionsPrefix(kspT,"T_");CHKERRQ(ierr);
				ierr = KSPSetOperators(kspT,JE,JE,SAME_NONZERO_PATTERN);CHKERRQ(ierr);
				ierr = KSPSetFromOptions(kspT);CHKERRQ(ierr);
				//ierr = KSPSetInitialGuessNonzero(kspT,PETSC_TRUE);CHKERRQ(ierr);
				
				ierr = VecDuplicate(T,&delta_T);CHKERRQ(ierr);
				ierr = VecScale(FE,-1.0);CHKERRQ(ierr);
				ierr = PetscGetTime(&t0);CHKERRQ(ierr);
				ierr = KSPSolve(kspT,FE,delta_T);CHKERRQ(ierr);
				ierr = PetscGetTime(&t1);CHKERRQ(ierr);
				
				ierr = pTatinLogBasicKSP(user,"energy",kspT);CHKERRQ(ierr);
				ierr = pTatinLogBasicCPUtime(user,"energy",(double)(t1-t0));CHKERRQ(ierr);
				
				ierr = VecAXPY(T, 1.0, delta_T);CHKERRQ(ierr); /* phi = phi + delta_phi */
				
				/* clean up */
				ierr = VecDestroy(&delta_T);CHKERRQ(ierr);
				ierr = KSPDestroy(&kspT);CHKERRQ(ierr);
				ierr = MatDestroy(&JE);CHKERRQ(ierr);
				ierr = VecDestroy(&FE);CHKERRQ(ierr);
			}
		}
		/* ====================== */

		
		
		/* output */
		if (tk%user->output_frequency==0 ) {
			char *prefix;
			asprintf(&prefix,"step%1.5d",tk);
			ierr = pTatin2d_ModelOutput(user,X,prefix);CHKERRQ(ierr);
			
			free(prefix);
		}
		
		
		
	
		

			/* ====================== */
		/*  SPMSOLVE  */
		{
			PetscBool active=PETSC_FALSE;
			PetscBool activetemp=PETSC_FALSE;
            PetscInt  ny_rem=0;
		
			ierr = pTatinPhysCompActivated(user,PhysComp_SPM,&active);CHKERRQ(ierr);
			ierr = pTatinPhysCompActivated(user,PhysComp_Energy,&activetemp);CHKERRQ(ierr);
			
			if (active ){
				PhysCompSPMCtx spm;
				spm         = user->phys_spm;
				spm->time   = user->time; 
				if (user->time > spm->start_time) {
				           
					Mat JE;
					Vec FE,coordinates;
					Vec height,heightold,delta_height;
					
					MatStructure mstr;
					KSP kspSPM;
					PetscScalar dt;
					PetscReal gmin[3],gmax[3];
					PetscInt N,M ;
					PetscScalar kappa,bcval;
					pTatinUnits *units;
					
					
					height      = spm->height;
					heightold   = spm->height_old;
		  
	////////////////////// end of section that belongs to the model //////// 
	
					
					
				  
					// define heights and x spacing
					ierr = PhysCompSPMExtractInitialGeometry(spm,user->dav);CHKERRQ(ierr);
					ierr = PhysComp_SPMSetConstantParams(spm);CHKERRQ(ierr);
					  // add sediments marker at the surface of the model in the current config
					spm->time = user->time;   
					
                    PhysCompSPMAddSedimentMarkersOnTopSurface(spm,user->dav,user->db,activetemp);
					
	
	///////////////////////////////////////////
					
					dt =  user->dt;
					ierr = DMCreateGlobalVector(spm->daSPM,&FE);CHKERRQ(ierr);
					ierr = DMGetMatrix(spm->daSPM,MATAIJ,&JE);CHKERRQ(ierr);
					ierr = DMDAGetCoordinates(spm->daSPM,&coordinates);CHKERRQ(ierr);
                    
					ierr = VecCopy(spm->height_old,height);CHKERRQ(ierr);
					ierr = SPMFormJacobian(user->time,height,user->dt,&JE,&JE,&mstr,(void*)spm);CHKERRQ(ierr);
					ierr = SPMFormFunction(user->time,height,user->dt,FE,(void*)spm);CHKERRQ(ierr);
					
					ierr = KSPCreate(PETSC_COMM_SELF,&kspSPM);CHKERRQ(ierr);
					ierr = KSPSetOptionsPrefix(kspSPM,"SPM_");CHKERRQ(ierr);
					ierr = KSPSetOperators(kspSPM,JE,JE,SAME_NONZERO_PATTERN);CHKERRQ(ierr);
					ierr = KSPSetFromOptions(kspSPM);CHKERRQ(ierr);
					
					ierr = VecDuplicate(height,&delta_height);CHKERRQ(ierr);
					ierr = VecScale(FE,-1.0);CHKERRQ(ierr);
					ierr = PetscGetTime(&t0);CHKERRQ(ierr);
					ierr = KSPSolve(kspSPM,FE,delta_height);CHKERRQ(ierr);
					ierr = PetscGetTime(&t1);CHKERRQ(ierr);
					
					ierr = pTatinLogBasicKSP(user,"spm",kspSPM);CHKERRQ(ierr);
					ierr = pTatinLogBasicCPUtime(user,"spm",(double)(t1-t0));CHKERRQ(ierr);
					
					ierr = VecAXPY(height, 1.0, delta_height);CHKERRQ(ierr); /* phi = phi + delta_phi */
					
					/* clean up */
                    ierr = KSPDestroy(&kspSPM);CHKERRQ(ierr);
					ierr = MatDestroy(&JE);CHKERRQ(ierr);
					ierr = VecDestroy(&FE);CHKERRQ(ierr);
					
	///////////////////////////////////////////////				
					/* update the surface geometry */ 
					ierr = PhysCompSPMUpdateSurfaceGeometry(spm,user->dav);CHKERRQ(ierr);
                    // ierr=PhysCompSPMAddSedimentMarkersOnTopSurface(spm,user->dav,user->db,activetemp);
					ierr = DMDAGetInfo(user->dav,0,&M,&N,0,0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);
                    ny_rem = (N-1)/2;
                    ierr = PetscOptionsGetInt(PETSC_NULL,"-SPM_remesh_ny_elem",&ny_rem,0);
					ierr = DMDARemeshSetUniformCoordinatesBetweenJLayers2d(user->dav,N-2*ny_rem+1,N);CHKERRQ(ierr);
					//ierr = pTatin2d_ModelUpdateMeshGeometry(user,X);CHKERRQ(ierr);
					ierr = QuadratureStokesCoordinateSetUp(user->Q,user->dav);CHKERRQ(ierr);
					for (e=0; e<QUAD_EDGES; e++) {
						 ierr = SurfaceQuadratureStokesGeometrySetUp(user->surfQ[e],user->dav);CHKERRQ(ierr);
					}
			}
			}
		}		
		
		
		
		

		
		
		/* UPDATE local coordinates */
		
		pTatin2d_Update_local_coordinates_Markers(user->dav,user->db,user->ex);CHKERRQ(ierr);
		
		

		/* pop control */
		//ierr = MPPC_AVDPatch(7,100,1, 10,10, 0.0,user->dav,user->db);CHKERRQ(ierr);
		ierr = MaterialPointPopulationControl(user);CHKERRQ(ierr);
				
		/* UPDATE local coordinates Passive tracers */
		if (passive_tracers) {
		ierr = pTatin2d_Update_local_coordinates_Markers(user->dav,user->db_passive,user->ex_passive);CHKERRQ(ierr); 
		}
		
		
		
		
		
	
	
		
		
		
		
		
		
		
		
		/* tidy up */
		ierr = ISDestroy(&isg[0]);CHKERRQ(ierr);
		ierr = ISDestroy(&isg[1]);CHKERRQ(ierr);
		ierr = PetscFree(isg);CHKERRQ(ierr);
		ierr = SNESDestroy(&snes);CHKERRQ(ierr);
	}
	
	
	/* destroy GMG operators */
	for (k=1; k<nlevels; k++) {
		ierr = MatDestroy(&interpolation[k]);CHKERRQ(ierr);
	}
	ierr = PetscFree(interpolation);CHKERRQ(ierr);
	
	for (k=0; k<nlevels; k++) {
		ierr = DMDestroy(&dav_hierarchy[k]);CHKERRQ(ierr);
	}
	ierr = PetscFree(dav_hierarchy);CHKERRQ(ierr);
	
	/* destroy operators */
	if (Auu) { ierr = MatDestroy(&Auu);CHKERRQ(ierr); }
	if (Aup) { ierr = MatDestroy(&Aup);CHKERRQ(ierr); }
	if (Apu) { ierr = MatDestroy(&Apu);CHKERRQ(ierr); }
	if (Spp) { ierr = MatDestroy(&Spp);CHKERRQ(ierr); }
	
	if ( (!user->use_mf_stokes) || (skip_pre_snes==PETSC_FALSE) ) {
		ierr = MatDestroy(&A);CHKERRQ(ierr);
	}
	ierr = MatDestroy(&B);CHKERRQ(ierr);
	ierr = VecDestroy(&X);CHKERRQ(ierr);
	ierr = VecDestroy(&F);CHKERRQ(ierr);
	
	ierr = pTatin2dDestroyContext(&user);
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc,char **argv)
{
	PetscErrorCode ierr;
	
	ierr = PetscInitialize(&argc,&argv,0,help);CHKERRQ(ierr);
	
	// write options file into current directory //
	ierr = pTatinWriteOptionsFile(PETSC_NULL);CHKERRQ(ierr);
	
	ierr = pTatin2d_material_points_std(argc,argv);CHKERRQ(ierr);
	
	ierr = PetscFinalize();CHKERRQ(ierr);
	return 0;
}
