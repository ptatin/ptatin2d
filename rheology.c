/*
 *  rheology.c
 *  
 *
 *  Created by laetitia le pourhiet on 6/11/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"

#include "stokes_viscous.h"
#include "stokes_visco_plastic.h"
#include "stokes_visco_plasticT.h"
#include "stokes_VPT.h"
#include "stokes_visco_elasto_plastic.h"
#include "rheology.h"

#include "material_point_std_utils.h"
#include "material_point_stokes_utils.h"
#include "material_point_stokespl_utils.h"
#include "material_point_thermal_utils.h"
#include "material_point_stokes_elas_utils.h"


#undef __FUNCT__
#define __FUNCT__ "RheologyConstantsInitialise"
PetscErrorCode RheologyConstantsInitialise(RheologyConstants *R)
{	
  PetscInt p;
  PetscBool flg;
  PetscScalar vis;
  PetscErrorCode ierr; 
  
  /* Define defaults for the viscosity cut-offs */
  R->apply_viscosity_cutoff_global = PETSC_FALSE;
  R->eta_lower_cutoff_global = 1.0e-100;
  R->eta_upper_cutoff_global = 1.0e+100;
  
  
  flg = PETSC_FALSE;
  ierr = PetscOptionsGetReal(PETSC_NULL,"-eta_lower_cutoff_global",&vis,&flg);CHKERRQ(ierr);
  
  if (flg == PETSC_TRUE) { 
    R->apply_viscosity_cutoff_global = PETSC_TRUE;
    R->eta_lower_cutoff_global       = vis;
  }
  
	flg = PETSC_FALSE;
  ierr = PetscOptionsGetReal(PETSC_NULL,"-eta_upper_cutoff_global",&vis,&flg);CHKERRQ(ierr);  
  
  if (flg == PETSC_TRUE) { 
    R->apply_viscosity_cutoff_global = PETSC_TRUE;
    R->eta_upper_cutoff_global       = vis;
  }
  
  PetscPrintf(PETSC_COMM_WORLD,"Global viscosity cut-off, min= %1.6e, max = %1.6e  \n", R->eta_lower_cutoff_global, R->eta_upper_cutoff_global );
  
  
  /* phase cutoff is equal to global cutoff and for the moment there is no options to enforce it 
   I don't think it belongs here ... maybe to the model definition */ 
  R->apply_viscosity_cutoff        = PETSC_FALSE;
  
  for (p=0; p<MAX_PHASE; p++) {
    R->eta_lower_cutoff[p] = R->eta_lower_cutoff_global;
    R->eta_upper_cutoff[p] = R->eta_upper_cutoff_global;
  }
  
  /* Define defaults for the parameters for each rheology */
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SurfaceRheologyConstantsInitialise"
PetscErrorCode SurfaceRheologyConstantsInitialise(SurfaceRheologyConstants *R)
{	
  PetscInt p;
  PetscErrorCode ierr; 
  
	R->rheology_type = SURF_RHEOLOGY_NONE;
	for (p=0; p<MAX_SURFACE_PHASE; p++) {
		R->stress_type[p] = STRESS_NULL;
	}
  
  /* Define defaults for the parameters for each rheology */
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "RheologyConstantsEnforceRheologyConst"
PetscErrorCode RheologyConstantsEnforceRheologyConst(RheologyConstants *R,const PetscInt phase_index)
{	
  
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TkUpdateRheologyQuadratureStokes"
PetscErrorCode TkUpdateRheologyQuadratureStokes(pTatinCtx user,DM dau,DM dap,Vec X)
{	
  Vec               Uloc,Ploc;
  PetscScalar       *LA_Uloc,*LA_Ploc;
  RheologyConstants *rheo;
  PetscErrorCode    ierr;
  
  /* extract the ghosted values for velocity and pressure */
  ierr = DMCompositeGetLocalVectors(user->pack,&Uloc,&Ploc);CHKERRQ(ierr);
	ierr = DMCompositeScatter(user->pack,X,Uloc,Ploc);CHKERRQ(ierr);
	ierr = VecGetArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
	ierr = VecGetArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
  
  rheo = &user->rheology_constants;
	switch (rheo->rheology_type) {
		case RHEOLOGY_VISCOUS:
			ierr = pTatin_TkUpdateRheologyQuadratureStokes_Viscous();CHKERRQ(ierr);
			break;

		case RHEOLOGY_VISCO_PLASTIC:
			ierr = pTatin_TkUpdateRheologyQuadratureStokes_ViscoPlastic(user,dau);CHKERRQ(ierr);
			break;

		case RHEOLOGY_VISCO_ELASTIC_PLASTIC:
			ierr = pTatin_TkUpdateRheologyQuadratureStokes_ViscoElastoPlastic(user,dau,LA_Uloc,dap,LA_Ploc);CHKERRQ(ierr);
			break;
        case RHEOLOGY_VPT_STD:
            ierr = TkUpdateRheologyQuadraturePoints_VPT(user,dau);CHKERRQ(ierr);
			break;
                        
		default:

			break;
	}
  
	ierr = VecRestoreArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
	ierr = VecRestoreArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
  ierr = DMCompositeRestoreLocalVectors(user->pack,&Uloc,&Ploc);CHKERRQ(ierr);
  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatin_EvaluateRheologyNonlinearitiesQuadratureStokes"
PetscErrorCode pTatin_EvaluateRheologyNonlinearitiesQuadratureStokes(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[])
{	
  RheologyConstants *rheo;
	PetscErrorCode ierr;
  static int been_here=0;
	PetscBool active_thermal_energy = PETSC_FALSE;
	
	PhysCompEnergyCtx phys_energy;
	Vec               T_vec,T_local_vec;
	DM                daT;
	PetscScalar       *LA_T;
	
	/* fetch rheology */
  rheo = &user->rheology_constants;
	
	/* check for thermal properties */
	active_thermal_energy = PETSC_FALSE;
	ierr = pTatinPhysCompActivated(user,PhysComp_Energy,&active_thermal_energy);CHKERRQ(ierr);
	
	if (active_thermal_energy) {
		phys_energy = user->phys_energy;
		T_vec       = phys_energy->T;
		daT         = phys_energy->daT;
		
		ierr = DMGetLocalVector(daT,&T_local_vec);CHKERRQ(ierr);		
		ierr = DMGlobalToLocalBegin(daT,T_vec,INSERT_VALUES,T_local_vec);CHKERRQ(ierr);
		ierr = DMGlobalToLocalEnd(  daT,T_vec,INSERT_VALUES,T_local_vec);CHKERRQ(ierr);
		
		ierr = VecGetArray(T_local_vec,&LA_T);CHKERRQ(ierr);
	}
	
	switch (rheo->rheology_type) {
		case RHEOLOGY_VISCOUS:
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"*** Rheology update for RHEOLOGY_VISCOUS using quadrature points is not development ***");
			break;
			
		case RHEOLOGY_VISCO_PLASTIC:
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"*** Rheology update for RHEOLOGY_VISCO_PLASTIC using quadrature points is not development ***");
			break;
			
		case RHEOLOGY_VISCO_PLASTIC_STRAIN_WEAKENING:
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"*** Rheology update for RHEOLOGY_VISCO_PLASTIC_STRAIN_WEAKENING using quadrature points is not development ***");
			break;
			
		case RHEOLOGY_VISCO_PLASTICY:
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"*** Rheology update for RHEOLOGY_VISCO_PLASTICY using quadrature points is not development ***");
			break;
			
 		case RHEOLOGY_VISCO_PLASTIC_T:
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"*** Rheology update for RHEOLOGY_VISCO_PLASTIC_T using quadrature points is not development ***");
			break;
			
		case RHEOLOGY_VISCO_PLASTIC_STRAIN_WEAKENING_T:
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"*** Rheology update for RHEOLOGY_VISCO_PLASTIC_STRAIN_WEAKENING_T using quadrature points is not development ***");
			break;
			
		case RHEOLOGY_VISCO_ELASTIC_PLASTIC:
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"*** Rheology update for RHEOLOGY_VISCO_ELASTIC_PLASTIC using quadrature points is not development ***");
			break;

		case RHEOLOGY_VPT_STD:
		{
			if (been_here==0) {
				PetscPrintf(PETSC_COMM_WORLD,"*** WARNING: Rheology update for RHEOLOGY_VPT_STD using quadrature points is under development ***\n");
			}
			/* update on quadrature points */
			ierr = StokesEvaluateRheologyNonLinearitiesQuadraturePoints_VPT(user,dau,u,dap,p);CHKERRQ(ierr);	
			if (user->continuation_m < user->continuation_M){
				ierr = ApplyContinuationViscousCutOffQuadrature(user);CHKERRQ(ierr);    
			}
		}
			break;
			
	}
	
	if (active_thermal_energy) {
		ierr = VecRestoreArray(T_local_vec,&LA_T);CHKERRQ(ierr);
		ierr = DMRestoreLocalVector(daT,&T_local_vec);CHKERRQ(ierr);				
	}
	
	been_here = 1;
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatin_EvaluateRheologyNonlinearitiesMarkers"
PetscErrorCode pTatin_EvaluateRheologyNonlinearitiesMarkers(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[])
{	
  RheologyConstants *rheo;
	int npoints;
	DataField PField_std;
	DataField PField_stokes;
    DataField PField_stokeselas;
    MPntStd     *mp_std;
	MPntPStokes *mp_stokes;
    MPntPStokesElas *mp_stokes_el;
	PetscErrorCode ierr;
  static int been_here=0;
	PetscBool active_thermal_energy = PETSC_FALSE;
    BTruth is_elastic = PETSC_FALSE;

	PhysCompEnergyCtx phys_energy;
	Vec               T_vec,T_local_vec;
	DM                daT;
	PetscScalar       *LA_T;
	MaterialCoeffAvergeType average_type;
	
  rheo = &user->rheology_constants;

	/* check for thermal properties */
	
	active_thermal_energy = PETSC_FALSE;
	ierr = pTatinPhysCompActivated(user,PhysComp_Energy,&active_thermal_energy);CHKERRQ(ierr);
/*
	switch (rheo->rheology_type) {
		case RHEOLOGY_VISCO_PLASTIC_T:
			active_thermal_energy = PETSC_TRUE;
			break;
		case RHEOLOGY_VISCO_PLASTIC_STRAIN_WEAKENING_T: 
			active_thermal_energy = PETSC_TRUE;
			break;
	}
*/
	
	if (active_thermal_energy) {
		phys_energy = user->phys_energy;
		T_vec       = phys_energy->T;
		daT         = phys_energy->daT;
		
		ierr = DMGetLocalVector(daT,&T_local_vec);CHKERRQ(ierr);		
		ierr = DMGlobalToLocalBegin(daT,T_vec,INSERT_VALUES,T_local_vec);CHKERRQ(ierr);
		ierr = DMGlobalToLocalEnd(  daT,T_vec,INSERT_VALUES,T_local_vec);CHKERRQ(ierr);

		ierr = VecGetArray(T_local_vec,&LA_T);CHKERRQ(ierr);
	}
	
	switch (rheo->rheology_type) {
		case RHEOLOGY_VISCOUS:
			/* update on markers */
			ierr = EvaluateRheologyNonlinearitiesMarkers_Viscous(user,dau,u,dap,p);CHKERRQ(ierr);
			break;
			
		case RHEOLOGY_VISCO_PLASTIC:
			if (been_here==0) {
				PetscPrintf(PETSC_COMM_WORLD,"*** WARNING: Rheology update for RHEOLOGY_VISCO_PLASTIC using markers is under development ***\n");
			}
			/* update on markers */
			ierr = EvaluateRheologyNonlinearitiesMarkers_ViscoPlastic(user,dau,u,dap,p);CHKERRQ(ierr);
			
			break;

		case RHEOLOGY_VISCO_PLASTIC_STRAIN_WEAKENING:
			if (been_here==0) {
				PetscPrintf(PETSC_COMM_WORLD,"*** WARNING: Rheology update for RHEOLOGY_VISCO_PLASTIC_STRAIN_WEAKENING using markers is under development ***\n");
			}
			/* update on markers */
			ierr = EvaluateRheologyNonlinearitiesMarkers_ViscoPlasticStrainWeakening(user,dau,u,dap,p);CHKERRQ(ierr);
			break;

			
        case RHEOLOGY_VISCO_PLASTICY:
			if (been_here==0) {
				PetscPrintf(PETSC_COMM_WORLD,"*** WARNING: Rheology update for RHEOLOGY_VISCO_PLASTICY using markers is under development ***\n");
			}
			/* update on markers */
			ierr = EvaluateRheologyNonlinearitiesMarkers_ViscoPlasticY(user,dau,u,dap,p);CHKERRQ(ierr);
						
			break;

 		case RHEOLOGY_VISCO_PLASTIC_T:
			if (been_here==0) {
				PetscPrintf(PETSC_COMM_WORLD,"*** WARNING: Rheology update for RHEOLOGY_VISCO_PLASTIC_T using markers is under development ***\n");
			}
			/* update on markers */
			ierr = EvaluateRheologyNonlinearitiesMarkers_ViscoPlasticT(user,dau,u,dap,p,daT,LA_T);CHKERRQ(ierr);
			break;
            
		case RHEOLOGY_VISCO_PLASTIC_STRAIN_WEAKENING_T:
			if (been_here==0) {
				PetscPrintf(PETSC_COMM_WORLD,"*** WARNING: Rheology update for RHEOLOGY_VISCO_PLASTIC_STRAIN_WEAKENING_T using markers is under development ***\n");
			}
			/* update on markers */
			ierr = EvaluateRheologyNonlinearitiesMarkers_ViscoPlasticStrainWeakeningT(user,dau,u,dap,p,daT,LA_T);CHKERRQ(ierr);						
			break;

		case RHEOLOGY_VPT_STD:
			
			/* update on markers */
			ierr = StokesEvaluateRheologyNonLinearitiesMarkers_VPT(user,dau,u,dap,p);CHKERRQ(ierr);	
            if (user->continuation_m < user->continuation_M){
                ierr = ApplyContinuationViscousCutOffMarker(user);CHKERRQ(ierr);    
            }
			break;
            
           
            
		case RHEOLOGY_VISCO_ELASTIC_PLASTIC:
            if (been_here==0) {
			PetscPrintf(PETSC_COMM_WORLD,"*** WARNING: Rheology update for RHEOLOGY_VISCO_ELASTIC_PLASTIC using markers under development ***\n");
            }
            DataBucketQueryDataFieldByName(user->db,MPntPStokesElas_classname,&is_elastic);
            if (is_elastic == PETSC_FALSE)
                SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Cant use EVPT if elastic databucket is not on !!!");
            /* update on markers */
            ierr = StokesEvaluateRheologyNonLinearitiesMarkers_EVPT(user,dau,u,dap,p);CHKERRQ(ierr);
            
			break;
			
		default:
			
			break;
	}

    
	DataBucketGetDataFieldByName(user->db, MPntStd_classname     , &PField_std);
	DataBucketGetDataFieldByName(user->db, MPntPStokes_classname , &PField_stokes);
	
	DataBucketGetSizes(user->db,&npoints,PETSC_NULL,PETSC_NULL);

	mp_std    = PField_std->data; /* should write a function to do this */
	mp_stokes = PField_stokes->data; /* should write a function to do this */
	
    average_type = 0;
    ierr = PetscOptionsGetInt(PETSC_NULL,"-coefficient_average_type",(PetscInt*)&average_type,0);CHKERRQ(ierr);
    if ((been_here==0) && (average_type!=AVERAGE_TYPE_ARITHMETIC)) {
        PetscPrintf(PETSC_COMM_WORLD,"*** WARNING: coefficient averaging other than ARITHMETIC is under development ***\n");
    }
	
    if (is_elastic){
        DataBucketGetDataFieldByName(user->db, MPntPStokesElas_classname , &PField_stokeselas);
        mp_stokes_el = PField_stokeselas->data;
        ierr = SwarmUpdateGaussPropertiesLocalL2Projection_Q1_MPntPStokes_elas(npoints,mp_std,mp_stokes,mp_stokes_el,average_type,user->dav,user->Q);CHKERRQ(ierr);
        
    }else{
	switch (user->coefficient_projection_type) {
		case 0:			/* Perform P0 projection over Q2 element directly onto uadrature points */
           
			ierr = SwarmUpdateGaussPropertiesLocalL2Projection_P0_MPntPStokes(npoints,mp_std,mp_stokes,average_type,user->dav,user->Q);CHKERRQ(ierr);

			break;
			
		case 1:			/* Perform Q1 projection over Q1 element and interpolate back to quadrature points */
			ierr = SwarmUpdateGaussPropertiesLocalL2Projection_Q1_MPntPStokes(npoints,mp_std,mp_stokes,average_type,user->dav,user->Q);CHKERRQ(ierr);
			break;
			
		case 2: 			/* Perform Q2 projection and interpolate back to quadrature points */
			ierr = SwarmUpdateGaussPropertiesLocalL2Projection_MPntPStokes(npoints,mp_std,mp_stokes,user->dav,user->Q);CHKERRQ(ierr);
			break;
	}
    }
	if (active_thermal_energy==PETSC_TRUE) {
		DataField         PField_thermal;
		MPntStd           *mp_std;
		MPntPThermal      *mp_thermal;
		
		DataBucketGetDataFieldByName(user->db, MPntStd_classname      , &PField_std);
		DataBucketGetDataFieldByName(user->db, MPntPThermal_classname , &PField_thermal);

		mp_std     = PField_std->data; /* should write a function to do this */
		mp_thermal = PField_thermal->data; /* should write a function to do this */
		
		switch (user->coefficient_projection_type) {
			case 0:			/* Perform P0 projection over Q1 element directly onto quadrature points */
				ierr = SwarmUpdateGaussPropertiesLocalL2Projection_P0_MPntPThermal(npoints,mp_std,mp_thermal,phys_energy->daT,phys_energy->Q);CHKERRQ(ierr);
				break;
				
			case 1:			/* Perform Q1 projection over Q1 element and interpolate back to quadrature points */
				ierr = SwarmUpdateQuadraturePropertiesLocalL2Projection_Q1_MPntPThermal(npoints,mp_std,mp_thermal,phys_energy->daT,phys_energy->Q);CHKERRQ(ierr);
				break;
				
			default:
				SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Marker->quadrature point projection supprted for energy equations is P0 or Q1");
				break;
		}
	}

	if (active_thermal_energy) {
		ierr = VecRestoreArray(T_local_vec,&LA_T);CHKERRQ(ierr);
		ierr = DMRestoreLocalVector(daT,&T_local_vec);CHKERRQ(ierr);				
	}
	
    been_here = 1;
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatin_EvaluateRheologyNonlinearities"
PetscErrorCode pTatin_EvaluateRheologyNonlinearities(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[])
{
	PetscErrorCode ierr;
	PetscBool use_gausspoint = PETSC_FALSE;
	PetscFunctionBegin;
	
	
	ierr = PetscOptionsGetBool(PETSC_NULL,"-update_nonlinearities_qp",&use_gausspoint,PETSC_NULL);CHKERRQ(ierr);
	if (use_gausspoint) {
		ierr = pTatin_EvaluateRheologyNonlinearitiesQuadratureStokes(user,dau,u,dap,p);CHKERRQ(ierr);
	} else {
		ierr = pTatin_EvaluateRheologyNonlinearitiesMarkers(user,dau,u,dap,p);CHKERRQ(ierr);
	}

	PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "pTatin_UpdateRheologyNonlinearities"
PetscErrorCode pTatin_UpdateRheologyNonlinearities(pTatinCtx user,Vec X)
{
	PetscErrorCode ierr;
	PetscBool use_gausspoint = PETSC_FALSE;
	PetscFunctionBegin;
	
	
	ierr = PetscOptionsGetBool(PETSC_NULL,"-update_nonlinearities_qp",&use_gausspoint,PETSC_NULL);CHKERRQ(ierr);
	if (use_gausspoint) {
        DM                dau,dap;
		ierr = DMCompositeGetEntries(user->pack,&dau,&dap);CHKERRQ(ierr);
        ierr = TkUpdateRheologyQuadratureStokes(user,dau,dap,X);CHKERRQ(ierr);
	} else {
		ierr = pTatin2dUpdateMaterialPoints(user,X);CHKERRQ(ierr);
	}
    
	PetscFunctionReturn(0);
}




/* ------------------------------------------------------------------------------------------- */
/* surface quadrature */
#undef __FUNCT__
#define __FUNCT__ "pTatin_Stokes_EvaluateSurfaceNonlinearities_MaterialPoints"
PetscErrorCode pTatin_Stokes_EvaluateSurfaceNonlinearities_MaterialPoints(pTatinCtx user,DM dau,PetscScalar LA_u[],DM dap,PetscScalar LA_p[])
{	
  SurfaceRheologyConstants *rheo;
	int npoints;
	DataField PField_std;
	DataField PField_stokes;
  MPntStd     *mp_std;
	MPntPStokes *mp_stokes;
	PetscInt edge;
	PetscErrorCode ierr;
  static int been_here=0;
	
	
	PetscFunctionBegin;
  
	rheo = &user->surf_rheology_constants;
	
	/* check for thermal properties */
	switch (rheo->rheology_type) {

		case SURF_RHEOLOGY_NONE:
			if (been_here==0) {
				PetscPrintf(PETSC_COMM_WORLD,"*** Applying NonlinearUpdate(surface) for SURF_RHEOLOGY_NONE ***\n");
			}
			break;

		case SURF_RHEOLOGY_SLIDING:
			if (been_here==0) {
				PetscPrintf(PETSC_COMM_WORLD,"*** WARNING NonlinearUpdate(surface) for SURF_RHEOLOGY_SLIDING not implemented ***\n");
			}
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"SURF_RHEOLOGY_SLIDING under development");
			break;

		case SURF_RHEOLOGY_STRESS:
			if (been_here==0) {
				PetscPrintf(PETSC_COMM_WORLD,"*** WARNING NonlinearUpdate(surface) for SURF_RHEOLOGY_STRESS under developmen ***\n");
			}
			
			/* Map material point viscosities onto surface quadrature points */
			//ierr = SurfaceQuadraturePointProjection_LocalL2ProjectionQ1_MPntPStokes(npoints,mp_std,mp_stokes,user->dav,user->surfQ);CHKERRQ(ierr);

			/* Compute stress on surface quadrature points */
			for (edge=0; edge<QUAD_EDGES; edge++) {
				ierr = SurfaceQuadratureStokesComputeStress(user,user->surfQ[edge],dau,LA_u,dap,LA_p);CHKERRQ(ierr);
			}

			
			break;

	}
	
	
	been_here = 1;
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatin_Stokes_EvaluateSurfaceNonlinearities"
PetscErrorCode pTatin_Stokes_EvaluateSurfaceNonlinearities(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[])
{
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	ierr = pTatin_Stokes_EvaluateSurfaceNonlinearities_MaterialPoints(user,dau,u,dap,p);CHKERRQ(ierr);
	PetscFunctionReturn(0);
}



