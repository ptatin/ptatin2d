
#include "petsc.h"
#include "pTatin2d.h"
#include "pTatin2d_models.h"

const char *pTatin2dModelTypeName[] = {
	"SolCx",
	"RT",
	"SN",
    "MAP_LV",
    "CrystalChannel",
	"Delamination",
	"PureShearFree",
	"Camembert",
	"SimpleShearFree",
	"StepShearFree",  
	"Notch",
	"NotchEl",
    "NotchGP",
	"winkler",
	"mms_linear",
	"GENE",	
	"Sinker",	
	"AdvDiff",	
	"HotBlob",
	"Drunk",
	"G2008",
    "Schmalholz2011",
    "afonso",
    "mumu",
    "WUSA",
    "anthony",
    "reza",
    "paul",
    "julia",
	"CrystalJCP",
        "Elastic", 
	"report", 0 }; /* DO NOT EVER EDIT THIS LINE!! */


#undef __FUNCT__
#define __FUNCT__ "pTatinModelLoad"
PetscErrorCode pTatinModelLoad(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	PetscBool flg,flgname,flgidx;
	char modelname[PETSC_MAX_PATH_LEN];
	int model_index=-1;
	
	
	flgname = PETSC_FALSE;
	ierr = PetscOptionsGetString(PETSC_NULL,"-ptatin_model",modelname,PETSC_MAX_PATH_LEN-1,&flgname);CHKERRQ(ierr);
	if (flgname) {
		/* string match */
		ierr = ptatin_match_model_index(modelname,&model_index);CHKERRQ(ierr);
		if (model_index==-1) {
			ierr = pTatin2d_ModelTypeReport();CHKERRQ(ierr);
			SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"  [pTatinModel]: -ptatin_model %s has not been registered",modelname );
		}
	}

	flgidx = PETSC_FALSE;
	ierr = PetscOptionsGetInt(PETSC_NULL,"-model",&model_index,&flgidx);CHKERRQ(ierr);
	if (flgidx) {
		if ( (model_index < 0) || (model_index >= __PTATIN_MODEL_LIST_TERMINATOR__) ) {
			ierr = pTatin2d_ModelTypeReport();CHKERRQ(ierr);
			SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_SUP,"  [pTatinModel]: -model index is not valid. Index must in range [0,%d]",__PTATIN_MODEL_LIST_TERMINATOR__-1);
		}
	}
	
	ctx->model_index = model_index;
	
	PetscPrintf(PETSC_COMM_WORLD,"  [pTatinModel]: Loading model \"%s\" \n", pTatin2dModelTypeName[model_index] );
	switch (model_index) {
			
		case PTATIN_MODEL_SolCx:
			ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_SolCx;
			ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_SolCx;
			ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_SolCx;
			ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_SolCx;
			break;

		case PTATIN_MODEL_RT:
			ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_RT;
			ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_RT;
			ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_RT;
			ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_RT;
			break;
			
		case PTATIN_MODEL_SN:
			ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_SN;
			ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_SN;
			ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_SN;
			ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_SN;
			ctx->FP_pTatin2d_ModelUpdateMeshGeometry           = &pTatin2d_ModelUpdateMeshGeometry_SN;
			break;

		 case PTATIN_MODEL_MAP_LV:
			ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_MAP_LV;
			ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_MAP_LV;
			ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_MAP_LV;
			ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_MAP_LV;
			ctx->FP_pTatin2d_ModelUpdateMeshGeometry           = &pTatin2d_ModelUpdateMeshGeometry_MAP_LV;
			break;

		case PTATIN_MODEL_CrystalChannel:
			ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_CrystalChannel;
			ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_CrystalChannel;
			ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_CrystalChannel;
			ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_CrystalChannel;
			ctx->FP_pTatin2d_ModelUpdateMeshGeometry           = PETSC_NULL;
			break;

      		case PTATIN_MODEL_Notch:
			
			ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_Notch;
			ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_Notch;
			ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_Notch;
			ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_Notch;
            ctx->FP_pTatin2d_ModelUpdateMeshGeometry           = &pTatin2d_ModelUpdateMeshGeometry_Notch;
			break;
      		
		case PTATIN_MODEL_GENE:
			
			ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_GENE;
			ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_GENE;
			ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_GENE;
			ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_GENE;
                        ctx->FP_pTatin2d_ModelUpdateMeshGeometry           = &pTatin2d_ModelUpdateMeshGeometry_GENE;
			break;
        case PTATIN_MODEL_G2008:
			
			ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_G2008;
			ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_G2008;
			ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_G2008;
			ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_G2008;
            ctx->FP_pTatin2d_ModelUpdateMeshGeometry           = &pTatin2d_ModelUpdateMeshGeometry_G2008;
			break;
		case PTATIN_MODEL_Elastic:
			
			ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_Elastic;
			ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_Elastic;
			ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_Elastic;
			ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_Elastic;
            ctx->FP_pTatin2d_ModelUpdateMeshGeometry           = &pTatin2d_ModelUpdateMeshGeometry_Elastic;
			break;	

		case PTATIN_MODEL_Schmalholz2011:
			
			ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_Schmalholz2011;
			ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_Schmalholz2011;
			ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_Schmalholz2011;
			ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_Schmalholz2011;
            ctx->FP_pTatin2d_ModelUpdateMeshGeometry           = &pTatin2d_ModelUpdateMeshGeometry_Schmalholz2011;
			break;	
		case PTATIN_MODEL_afonso:
			
			ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_afonso;
			ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_afonso;
			ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_afonso;
			ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_GENE;
            ctx->FP_pTatin2d_ModelUpdateMeshGeometry           = &pTatin2d_ModelUpdateMeshGeometry_GENE;
			break;
			
		case PTATIN_MODEL_mumu:
			
			ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_mumu;
			ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_mumu;
			ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_mumu;
			ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_GENE;
            ctx->FP_pTatin2d_ModelUpdateMeshGeometry           = &pTatin2d_ModelUpdateMeshGeometry_GENE;
			break;
            
            
        case PTATIN_MODEL_anthony:
            
            ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_anthony;
            ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_anthony;
            ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_anthony;
            ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_GENE;
            ctx->FP_pTatin2d_ModelUpdateMeshGeometry           = &pTatin2d_ModelUpdateMeshGeometry_GENE;
            break;
   
        case PTATIN_MODEL_paul:
            
            ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_paul;
            ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_paul;
            ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_paul;
            ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_GENE;
            ctx->FP_pTatin2d_ModelUpdateMeshGeometry           = &pTatin2d_ModelUpdateMeshGeometry_GENE;
            break;
        case PTATIN_MODEL_reza:
            
            ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_GENE;
            ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_reza;
            ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_reza;
            ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_GENE;
            ctx->FP_pTatin2d_ModelUpdateMeshGeometry           = &pTatin2d_ModelUpdateMeshGeometry_GENE;
            break;
        case PTATIN_MODEL_julia:
            
            ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_julia;
            ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_julia;
            ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_julia;
            ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_GENE;
            ctx->FP_pTatin2d_ModelUpdateMeshGeometry           = &pTatin2d_ModelUpdateMeshGeometry_GENE;
            break;

	    case PTATIN_MODEL_WUSA:
			
			ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_GENE;
			ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_WUSA;
			ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_WUSA;
			ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_GENE;
            ctx->FP_pTatin2d_ModelUpdateMeshGeometry           = &pTatin2d_ModelUpdateMeshGeometry_GENE;
			break;		
			
		case PTATIN_MODEL_Delamination:
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Model case: PTATIN_MODEL_Delamination hasn't been updated for pTatinMP");
			ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_Delamination;
			ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_Delamination;
			ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_Delamination;
			ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_Delamination;
			break;

		case PTATIN_MODEL_PureShearFree:
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Model case: PTATIN_MODEL_PureShearFree hasn't been updated for pTatinMP");
			ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_PureShearFree;
			ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_PureShearFree;
			ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_PureShearFree;
			ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_PureShearFree;
			break;

		case PTATIN_MODEL_Camembert:
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Model case: PTATIN_MODEL_Camembert hasn't been updated for pTatinMP");
			ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_Camembert;
			ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_Camembert;
			ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_Camembert;
			ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_Camembert;
			break;
		
		case PTATIN_MODEL_SimpleShearFree:
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Model case: PTATIN_MODEL_SimpleShearFree hasn't been updated for pTatinMP");
			ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_SimpleShearFree;
			ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_SimpleShearFree;
			ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_SimpleShearFree;
			ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_SimpleShearFree;
			break;
		
		case PTATIN_MODEL_StepShearFree:
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Model case: PTATIN_MODEL_StepShearFree hasn't been updated for pTatinMP");
			ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_StepShearFree;
			ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_StepShearFree;
			ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_StepShearFree;
			ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_StepShearFree;
			break;
		
		

		case PTATIN_MODEL_NotchEl:
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Model case: PTATIN_MODEL_NotchEl hasn't been updated for pTatinMP");
			ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_NotchEl;
			ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_NotchEl;
			ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_NotchEl;
			ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_NotchEl;
			break;

		case PTATIN_MODEL_winkler:
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Model case: PTATIN_MODEL_winkler hasn't been updated for pTatinMP");
			ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_winkler;
			ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_winkler;
			ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_winkler;
			ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_winkler;
			break;

		case PTATIN_MODEL_mms_linear:
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Model case: PTATIN_MODEL_mms_linear hasn't been updated for pTatinMP");
			ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_mms_linear;
			ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_mms_linear;
			ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_mms_linear;
			ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_mms_linear;
			break;
			
		case PTATIN_MODEL_Sinker:
			ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_Sinker;
			ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_Sinker;
			ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_Sinker;
			ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_Sinker;
			break;
			
		case PTATIN_MODEL_AdvDiff:
			ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_AdvDiff;
			ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_AdvDiff;
			ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_AdvDiff;
			ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_AdvDiff;
			ctx->FP_pTatin2d_ModelUpdateMeshGeometry           = &pTatin2d_ModelUpdateMeshGeometry_AdvDiff;

		case PTATIN_MODEL_HotBlob:
			ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_HotBlob;
			ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_HotBlob;
			ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_HotBlob;
			ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_HotBlob;
			ctx->FP_pTatin2d_ModelUpdateMeshGeometry           = &pTatin2d_ModelUpdateMeshGeometry_HotBlob;
			
			break;

		case PTATIN_MODEL_Drunk:
			ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_DrunkSeaman;
			ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_DrunkSeamanDeformed;
			ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_DrunkSeaman;
			ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_DrunkSeaman;
			ctx->FP_pTatin2d_ModelUpdateMeshGeometry           = &pTatin2d_ModelUpdateMeshGeometry_Drunk;
			break;

		case PTATIN_MODEL_CrystalJCP:
			ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_CrystalJCP;
			ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_CrystalJCP;
			ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_CrystalJCP;
			ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_CrystalJCP;
			ctx->FP_pTatin2d_ModelUpdateMeshGeometry           = PETSC_NULL;
			break;
        
        case PTATIN_MODEL_NotchGP:
			
			ctx->FP_pTatin2d_ModelApplyBoundaryCondition       = &pTatin2d_ModelApplyBoundaryCondition_NotchGP;
			ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry     = &pTatin2d_ModelApplyInitialMeshGeometry_NotchGP;
			ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry = &pTatin2d_ModelApplyInitialMaterialGeometry_NotchGP;
			ctx->FP_pTatin2d_ModelOutput                       = &pTatin2d_ModelOutput_NotchGP;
            ctx->FP_pTatin2d_ModelUpdateMeshGeometry           = &pTatin2d_ModelUpdateMeshGeometry_NotchGP;
			break;    
			
		case PTATIN_MODEL_report:
			ierr = pTatin2d_ModelTypeReport();CHKERRQ(ierr);
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"  [pTatinModel]: This is not a model, just a model report. Specify a model with -model INT, or -ptatin_model TEXTUAL_NAME");
			break;
				
		default:
			ierr = pTatin2d_ModelTypeReport();CHKERRQ(ierr);
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"  [pTatinModel]: No default model supported. Specify a model with -model INT, or -ptatin_model TEXTUAL_NAME");
			break;
			
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ptatin_match_model_index"
PetscErrorCode ptatin_match_model_index(const char modelname[],int *index)
{
	PetscErrorCode ierr;
	PetscBool match;
	char *item;	
	int cnt;

	*index = -1;
	cnt = 0;
	item = (char*)pTatin2dModelTypeName[0];
	while (item!=NULL) {
		match = PETSC_FALSE;
		PetscStrcmp(modelname,item,&match);
		if (match) {
			*index = cnt;
			break;
		}
		cnt++;
		item = (char*)pTatin2dModelTypeName[cnt];
	}
	
	if ( (*index)==-1 ) {
		PetscPrintf(PETSC_COMM_WORLD,"  [pTatinModel]: -ptatin_model \"%s\" wasn't identified in list pTatin2dModelTypeName[]\n",modelname );
	}
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelTypeReport"
PetscErrorCode pTatin2d_ModelTypeReport(void)
{
	PetscInt i,max;
	max = 0;
	while (pTatin2dModelTypeName[max]!=PETSC_NULL) {
		max++;
	}
	PetscPrintf(PETSC_COMM_WORLD,"  [pTatinModel]: models registered \n");
	for (i=0; i<max; i++) {
//		PetscPrintf(PETSC_COMM_WORLD,"    [-ptatin_model %d]: name = \"%s\"\n",i,pTatin2dModelTypeName[i]);
		PetscPrintf(PETSC_COMM_WORLD,"    [-model %4D] or [-ptatin_model %s]\n",i,pTatin2dModelTypeName[i]);
	}
	PetscFunctionReturn(0);
}


/* interfaces */

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	if (ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry) {
		ierr = ctx->FP_pTatin2d_ModelApplyInitialMeshGeometry(ctx);CHKERRQ(ierr);
	} else {
		SETERRQ2(PETSC_COMM_WORLD,PETSC_ERR_SUP,"  [pTatinModel]: model[%d] \"%s\" wasn't provided with an ApplyInitialGeometry function",ctx->model_index,pTatin2dModelTypeName[ctx->model_index]);
	}	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	if (ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry) {
		ierr = ctx->FP_pTatin2d_ModelApplyInitialMaterialGeometry(ctx);CHKERRQ(ierr);
	} else {
		SETERRQ2(PETSC_COMM_WORLD,PETSC_ERR_SUP,"  [pTatinModel]: model[%d] \"%s\" wasn't provided with an ApplyMaterialGeometry function",ctx->model_index,pTatin2dModelTypeName[ctx->model_index]);
	}	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	if (ctx->FP_pTatin2d_ModelApplyBoundaryCondition) {
		ierr = ctx->FP_pTatin2d_ModelApplyBoundaryCondition(ctx);CHKERRQ(ierr);
	} else {
		SETERRQ2(PETSC_COMM_WORLD,PETSC_ERR_SUP,"  [pTatinModel]: model[%d] \"%s\" wasn't provided with an ApplyBoundaryCondition function",ctx->model_index,pTatin2dModelTypeName[ctx->model_index]);
	}	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelOutput"
PetscErrorCode pTatin2d_ModelOutput(pTatinCtx ctx,Vec X,const char name[])
{
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	PetscFunctionBegin;
	if (ctx->FP_pTatin2d_ModelOutput) {
		ierr = ctx->FP_pTatin2d_ModelOutput(ctx,X,name);CHKERRQ(ierr);
	} else {
		SETERRQ2(PETSC_COMM_WORLD,PETSC_ERR_SUP,"  [pTatinModel]: model[%d] \"%s\" wasn't provided with an Output function",ctx->model_index,pTatin2dModelTypeName[ctx->model_index]);
	}	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelUpdateMeshGeometry"
PetscErrorCode pTatin2d_ModelUpdateMeshGeometry(pTatinCtx ctx,Vec X)
{
	PetscErrorCode ierr;
	static PetscInt been_here = 0;
	
	PetscFunctionBegin;
	PetscFunctionBegin;
	if (ctx->FP_pTatin2d_ModelUpdateMeshGeometry) {
		ierr = ctx->FP_pTatin2d_ModelUpdateMeshGeometry(ctx,X);CHKERRQ(ierr);
	} else {
		if (been_here==0) {
			PetscPrintf(PETSC_COMM_WORLD,"  [pTatinModel]: model[%d] \"%s\" wasn't provided with an UpdateMeshGeometry function\n",ctx->model_index,pTatin2dModelTypeName[ctx->model_index]);
			been_here = 1;
		}
	}	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyMaterialBoundaryCondition"
PetscErrorCode pTatin2d_ModelApplyMaterialBoundaryCondition(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	static PetscInt been_here = 0;
	
	PetscFunctionBegin;
	if (ctx->FP_pTatin2d_ModelApplyMaterialBoundaryCondition) {
		ierr = ctx->FP_pTatin2d_ModelApplyMaterialBoundaryCondition(ctx);CHKERRQ(ierr);
	} else {
		if (been_here==0) {
			PetscPrintf(PETSC_COMM_WORLD,"  [pTatinModel]: model[%d] \"%s\" wasn't provided with an ApplyMaterialBoundaryCondition function\n",ctx->model_index,pTatin2dModelTypeName[ctx->model_index]);
			been_here = 1;
		}
	}
	PetscFunctionReturn(0);
}

