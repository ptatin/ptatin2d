//
//  topo_map.c
//  
//
//  Created by le pourhiet laetitia on 4/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"

#include "pTatin2d.h"
#include "topo_map.h"

void TopoMapCreate(TopoMap *map)
{
	TopoMap pm;
	pm = malloc(sizeof(struct _p_TopoMap));
	memset(pm,0,sizeof(struct _p_TopoMap));
	*map = pm;
}

void TopoMapDestroy(TopoMap *map)
{
	TopoMap pm;
    
	if (map==NULL) { return; }
	pm = *map;
	
	if (pm->data!=NULL) {
		free(pm->data);
		pm->data = NULL;
	}
	*map = NULL;
}

void TopoMapGetIndex(TopoMap pm,const int i,const int j, int *index)
{
	if (i<0) { printf("ERROR(%s): i = %d  <0 \n", __func__, i ); exit(EXIT_FAILURE); }
	if (j<0) { printf("ERROR(%s): j = %d < 0 \n", __func__, j ); exit(EXIT_FAILURE); }
	if (i>=pm->mx) { printf("ERROR(%s): i = %d > %d\n", __func__, i, pm->mx ); exit(EXIT_FAILURE); }
	if (j>=pm->mz) { printf("ERROR(%s): j = %d > %d\n", __func__, j, pm->mz ); exit(EXIT_FAILURE); }
    
	
	*index = i + j * pm->mx;
}

void TopoMapLoadFromFile_ASCII(const char filename[],TopoMap *map)
{
	FILE *fp = NULL;
	TopoMap topomap;
	char dummy[1000];
	double max_height;
	int i,j;
	int index;
	
	/* open file to parse */
	fp = fopen(filename,"r");
	if (fp==NULL) {
		printf("Error(%s): Could not open file: %s \n",__func__, filename );
		exit(EXIT_FAILURE);
	}
	
	/* create data structure */
	TopoMapCreate(&topomap);
	
	/* read header information, mx,my,x0,y0,x1,y1 */
	//  fscanf(fp,"%s\n",dummy);
	
    fgets(dummy,sizeof(dummy),fp);
	fscanf(fp,"%d\n",&topomap->mx);
	fscanf(fp,"%d\n",&topomap->mz);
	fscanf(fp,"%lf %lf %lf %lf\n",&topomap->x0,&topomap->z0,&topomap->x1,&topomap->z1);
	//
	topomap->dx = (topomap->x1 - topomap->x0)/(double)(topomap->mx);
	topomap->dz = (topomap->z1 - topomap->z0)/(double)(topomap->mz);
	
	
	/* allocate data */
	topomap->data = malloc( sizeof(double)* topomap->mx * topomap->mz);
	
	/* parse phase map from file */
	//
	index = 0;
	max_height = -1.e+32;
	for (j=0; j<topomap->mz; j++) {
		for (i=0; i<topomap->mx; i++) {
			fscanf(fp,"%lf",&topomap->data[index]);
			if(topomap->data[index] > max_height){
				max_height = topomap->data[index]; 
			}
			//printf("%d \n", topomap->data[index]);
			index++; 
		}
	}
	topomap->max_height = max_height; 
	//		
	/* set pointer */
	*map = topomap;
	fclose(fp);
}
void TopoMapLoadFromFileScale_ASCII(const char filename[],TopoMap *map,pTatinUnits *units)
{
	FILE *fp = NULL;
	TopoMap topomap;
	char dummy[1000];
	double max_height;
	int i,j;
	int index;
	
	/* open file to parse */
	fp = fopen(filename,"r");
	if (fp==NULL) {
		printf("Error(%s): Could not open file: %s \n",__func__, filename );
		exit(EXIT_FAILURE);
	}
	
	/* create data structure */
	TopoMapCreate(&topomap);
	
	/* read header information, mx,my,x0,y0,x1,y1 */
	//  fscanf(fp,"%s\n",dummy);
	
    fgets(dummy,sizeof(dummy),fp);
	fscanf(fp,"%d\n",&topomap->mx);
	fscanf(fp,"%d\n",&topomap->mz);
	fscanf(fp,"%lf %lf %lf %lf\n",&topomap->x0,&topomap->z0,&topomap->x1,&topomap->z1);
    
    UnitsApplyInverseScaling(units->si_length,topomap->x0,&topomap->x0);
    UnitsApplyInverseScaling(units->si_length,topomap->z0,&topomap->z0);
    UnitsApplyInverseScaling(units->si_length,topomap->x1,&topomap->x1);
    UnitsApplyInverseScaling(units->si_length,topomap->z1,&topomap->z1);
    
	//
	topomap->dx = (topomap->x1 - topomap->x0)/(double)(topomap->mx);
	topomap->dz = (topomap->z1 - topomap->z0)/(double)(topomap->mz);
	
	
	/* allocate data */
	topomap->data = malloc( sizeof(double)* topomap->mx * topomap->mz);
	
	/* parse phase map from file */
	//
	index = 0;
	max_height = -1.e+32;
	for (j=0; j<topomap->mz; j++) {
		for (i=0; i<topomap->mx; i++) {
			fscanf(fp,"%lf",&topomap->data[index]);
            UnitsApplyInverseScaling(units->si_length,topomap->data[index],&topomap->data[index]);
			if(topomap->data[index] > max_height){
				max_height = topomap->data[index]; 
			}
			//printf("%d \n", topomap->data[index]);
			index++; 
		}
	}
	topomap->max_height = max_height; 
	//		
	/* set pointer */
	*map = topomap;
	fclose(fp);
}

#undef __FUNCT__  
#define __FUNCT__ "TopoMapLoadFromFile_ASCII_ZIPPED"
void TopoMapLoadFromFile_ASCII_ZIPPED(const char filename[],TopoMap *map)
{
	SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Ascii zipped loading is not supported");
}
#undef __FUNCT__  
#define __FUNCT__ "TopoMapLoadFromFileScale_ASCII_ZIPPED"
void TopoMapLoadFromFileScale_ASCII_ZIPPED(const char filename[],TopoMap *map,pTatinUnits *units)
{
	SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Ascii zipped loading is not supported");
}


void TopoMapLoadFromFile(const char filename[],TopoMap *map)
{
	size_t len;
	int is_zipped;
	int matched_extension;
	
	is_zipped = 0;
    
	/* check extensions for common zipped file extensions */
	len = strlen(filename);
	matched_extension = strcmp(&filename[len-8],".tar.gz");
	if (matched_extension == 0) {
		printf("  Detected .tar.gz\n");
		is_zipped = 1;
	}
	matched_extension = strcmp(&filename[len-5],".tgz");
	if (matched_extension == 0) {
		printf("  Detected .tgz\n");
		is_zipped = 1;
	}
	matched_extension = strcmp(&filename[len-3],".Z");
	if (matched_extension == 0) {
		printf("  Detected .Z\n");
		is_zipped = 1;
	}
    
	if (is_zipped == 1) {
		TopoMapLoadFromFile_ASCII_ZIPPED(filename,map);
	} else {
		TopoMapLoadFromFile_ASCII(filename,map);
	}
}

void TopoMapLoadFromFileScale(const char filename[],TopoMap *map,pTatinUnits *units)
{
	size_t len;
	int is_zipped;
	int matched_extension;
	
	is_zipped = 0;
    
	/* check extensions for common zipped file extensions */
	len = strlen(filename);
	matched_extension = strcmp(&filename[len-8],".tar.gz");
	if (matched_extension == 0) {
		printf("  Detected .tar.gz\n");
		is_zipped = 1;
	}
	matched_extension = strcmp(&filename[len-5],".tgz");
	if (matched_extension == 0) {
		printf("  Detected .tgz\n");
		is_zipped = 1;
	}
	matched_extension = strcmp(&filename[len-3],".Z");
	if (matched_extension == 0) {
		printf("  Detected .Z\n");
		is_zipped = 1;
	}
    
	if (is_zipped == 1) {
		TopoMapLoadFromFileScale_ASCII_ZIPPED(filename,map,units);
	} else {
		TopoMapLoadFromFileScale_ASCII(filename,map,units);
	}
}

void TopoMapGetHeight(TopoMap topomap,double xp[],double *height)
{
	int i,j,index;
		
	if (xp[0] < topomap->x0) { return; }
	if (xp[0] > topomap->x1) { return; }
    i = (xp[0] - topomap->x0)/topomap->dx;
    if (i==topomap->mx) { i--; } 
    
    if (topomap->mz > 1) {
	      if (xp[2] < topomap->z0) { return; }
	      if (xp[2] > topomap->z1) { return; }
	      j = (xp[2] - topomap->z0)/topomap->dz;
          if (j==topomap->mz) { j--; }
    } else {
        j=0;
    }
    
	TopoMapGetIndex(topomap,i,j,&index);
	
	*height = topomap->data[index];
	
}

/*
 gnuplot> set pm3d map
 gnuplot> splot "filename" 
 */
void TopoMapViewGnuplot(const char filename[],TopoMap topomap)
{
	FILE *fp = NULL;
	int i,j;
	
	/* open file to parse */
	fp = fopen(filename,"w");
	if (fp==NULL) {
		printf("Error(%s): Could not open file: %s \n",__func__, filename );
		exit(EXIT_FAILURE);
	}
	fprintf(fp,"# topo map information \n");
	fprintf(fp,"# topo map : (x0,z0) = (%1.4e,%1.4e) \n",topomap->x0,topomap->z0);
	fprintf(fp,"# topo map : (x1,z1) = (%1.4e,%1.4e) \n",topomap->x1,topomap->z1);
	fprintf(fp,"# topo map : (dx,dz) = (%1.4e,%1.4e) \n",topomap->dx,topomap->dz);
	fprintf(fp,"# topo map : (mx,mz) = (%d,%d) \n",topomap->mx,topomap->mz);
	
	for (j=0; j<topomap->mz; j++) {
		for (i=0; i<topomap->mx; i++) {
			double x,z;
			int index;
			
			x = topomap->x0 + topomap->dx * 0.5 + i * topomap->dx;
			z = topomap->z0 + topomap->dz * 0.5 + j * topomap->dz;
			TopoMapGetIndex(topomap,i,j,&index);
			
			fprintf(fp,"%1.4e %1.4e %1.4e \n", x,z,(double)topomap->data[index]);
		}fprintf(fp,"\n");
	}
	fclose(fp);
	
}

#undef __FUNCT__  
#define __FUNCT__ "pTatinCtxAttachTopoMap"
PetscErrorCode pTatinCtxAttachTopoMap(pTatinCtx ctx,TopoMap map)
{
	PetscContainer container;
	PetscErrorCode ierr;
	
  PetscFunctionBegin;
  ierr = PetscContainerCreate(PETSC_COMM_WORLD,&container);CHKERRQ(ierr);
  ierr = PetscContainerSetPointer(container,(void*)map);CHKERRQ(ierr);
	
	ierr = PetscObjectCompose((PetscObject)ctx->model_data,"topomap",(PetscObject)container);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatinCtxGetTopoMap"
PetscErrorCode pTatinCtxGetTopoMap(pTatinCtx ctx,TopoMap *map)
{
	TopoMap mymap;
	PetscContainer container;
	PetscErrorCode ierr;
	
  PetscFunctionBegin;
	ierr = PetscObjectQuery((PetscObject)ctx->model_data,"topomap",(PetscObject*)&container);CHKERRQ(ierr);
	if (!container) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONG,"No data with name \"topomap\" was composed with ctx->model_data");
	ierr = PetscContainerGetPointer(container,(void**)&mymap);CHKERRQ(ierr);
	(*map) = mymap;
	
	PetscFunctionReturn(0);
}

