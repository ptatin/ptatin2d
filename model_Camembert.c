/*
 
 Model Description:

 These functions define the setup for an isoviscous problem with free surface normale to the gravity field and velocity applied in the direction of the velocity field. 
 Domain is [0,aspect,-1, 0] with free surface at the imax, free slip at the jmin and imin face and dirichlet at jmax. 


The solution for pressure is  (I got to check there is a parabolic components I believe) 

 
  Input / command line parameters:
 -Camembert_eta   : viscosity (default =1.0);
 -Camembert_srH   : strainrate (default =1.0); 
 -Camembert_body  : rho*g (default =-1.0); 
 -Camembert_aspect: Lx=aspect*Ly (default =1.0);
 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"

/* ================= Camembert ================= */

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelOutput_Camembert"
PetscErrorCode pTatin2d_ModelOutput_Camembert(pTatinCtx ctx,Vec X,const char prefix[])
{
	PetscErrorCode ierr;

	//ierr = pTatin2d_ModelOutput_Default(ctx,X,prefix);CHKERRQ(ierr);
	if (prefix == PETSC_NULL) {
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,"qpoints");CHKERRQ(ierr);
		ierr = pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(ctx,ctx->outputpath,"faceqpoints");CHKERRQ(ierr);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,"vp");CHKERRQ(ierr);
		ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,"int_fields");CHKERRQ(ierr);
	} else { /* exanple of adding a different prefix to the file names - not sure this is useful */
		char *name;
		
		asprintf(&name,"%s_qpoints",prefix);
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
		
		asprintf(&name,"%s_faceqpoints",prefix);
		ierr = pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
		
		asprintf(&name,"%s_vp",prefix);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
		asprintf(&name,"%s_int_fields",prefix);
		ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_Camembert"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_Camembert(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
	PetscScalar opts_aspect,Lx,Ly;
  opts_aspect=1.0;
	Ly=-1.0;
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-Camembert_aspect",&opts_aspect,0);CHKERRQ(ierr);
	Lx= (-opts_aspect)*Ly; 
	ierr = DMDASetUniformCoordinates(ctx->dav,0.0,Lx, Ly ,0.0, 0.0,0.0);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_Camembert"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_Camembert(pTatinCtx ctx)
{
	PetscInt e,ncells;
	PetscInt p,ngp;
	GaussPointCoefficientsStokes *gausspoints;
	PetscScalar opts_eta,opts_body;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	opts_eta  = 1.0;
	opts_body = 1.0;

	
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-Camembert_eta",&opts_eta,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-Camembert_body",&opts_body,0);CHKERRQ(ierr);

	
	ncells = ctx->Q->ncells;
	ngp    = ctx->Q->ngp;
	ctx->rheology_constants.rheology_type = RHEOLOGY_VISCOUS;

	for (e=0;e<ncells;e++) {
		ierr = QuadratureStokesGetCell(ctx->Q,e,&gausspoints);CHKERRQ(ierr);
		
		for (p=0; p<ngp; p++) {
			/*viscosity*/
			gausspoints[p].eta     = opts_eta;
			gausspoints[p].eta_ref = opts_eta;
			/* rhs */
			gausspoints[p].Fu[0] = 0.0 * opts_body;
			gausspoints[p].Fu[1] = -1.0 * opts_body;
		}
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_Camembert"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_Camembert(pTatinCtx ctx)
{
	PetscBool      opts_traction;
	BCList         ubclist;
	PetscScalar    bcval,p_atm;
	DM             dav;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	PetscScalar opts_srV=-1.0;
	
	ubclist = ctx->u_bclist;
	dav     = ctx->dav;
/*  -Camembert_srH   : strainrate (default =1.0); */ 

 	ierr = PetscOptionsGetScalar(PETSC_NULL,"-Camembert_srV",&opts_srV,0);CHKERRQ(ierr);


	bcval = 0.0; 			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	
	bcval = opts_srV; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
	bcval = 0.0; 			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

	opts_traction = PETSC_FALSE;
	ierr = PetscOptionsGetBool(PETSC_NULL,"-Camembert_apply_traction",&opts_traction,0);CHKERRQ(ierr);

	if (opts_traction) {
		PetscPrintf(PETSC_COMM_WORLD,"  [Camembert] Imposing external traction boundary condition\n");
		p_atm = 10.0;
		bcval = -p_atm; ierr = SurfaceQuadratureStokesTraverseSetTraction(ctx->surfQ,DMDATracLoc_IMAX_LOC,SurfaceQuadatureEvaluator_Isotropic,(void*)&bcval);CHKERRQ(ierr);
	}
	
	PetscFunctionReturn(0);
}


