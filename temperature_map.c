
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"

#include "pTatin2d.h"
#include "temperature_map.h"


void TempMapCreate(TempMap *map)
{
	TempMap pm;
	pm = malloc(sizeof(struct _p_TempMap));
	memset(pm,0,sizeof(struct _p_TempMap));
	*map = pm;
}

void TempMapDestroy(TempMap *map)
{
	TempMap pm;

	if (map==NULL) { return; }
	pm = *map;
	
	if (pm->data!=NULL) {
		free(pm->data);
		pm->data = NULL;
	}
	*map = NULL;
}

void TempMapGetIndex(TempMap pm,const int i,const int j, int *index)
{
	if (i<0) { printf("ERROR(%s): i = %d  <0 \n", __func__, i ); exit(EXIT_FAILURE); }
	if (j<0) { printf("ERROR(%s): j = %d < 0 \n", __func__, j ); exit(EXIT_FAILURE); }
	if (i>=pm->mx) { printf("ERROR(%s): i = %d > %d\n", __func__, i, pm->mx ); exit(EXIT_FAILURE); }
	if (j>=pm->my) { printf("ERROR(%s): j = %d > %d\n", __func__, j, pm->my ); exit(EXIT_FAILURE); }

	
	*index = i + j * pm->mx;
}
void TempMapLoadFromFile_ASCII(const char filename[],TempMap *map)
{
	FILE *fp = NULL;
	TempMap Tempmap;
    char dummy[1000];
    
	int i,j,Tempmap_max;
    int index;
	
	/* open file to parse */
	fp = fopen(filename,"r");
	if (fp==NULL) {
		printf("Error(%s): Could not open file: %s \n",__func__, filename );
		exit(EXIT_FAILURE);
	}
	
	/* create data structure */
	TempMapCreate(&Tempmap);
	
	/* read header information, mx,my,x0,y0,x1,y1 */
    //  fscanf(fp,"%s\n",dummy);
	fgets(dummy,sizeof(dummy),fp);
    fscanf(fp,"%d\n",&Tempmap->mx);
    fscanf(fp,"%d\n",&Tempmap->my);
    fscanf(fp,"%lf %lf %lf %lf\n",&Tempmap->x0,&Tempmap->y0,&Tempmap->x1,&Tempmap->y1);
    
	//
	Tempmap->dx = (Tempmap->x1 - Tempmap->x0)/(double)(Tempmap->mx);
	Tempmap->dy = (Tempmap->y1 - Tempmap->y0)/(double)(Tempmap->my);
    
	
	/* allocate data */
	Tempmap->data = malloc(sizeof(double)* Tempmap->mx * Tempmap->my );
	
	/* parse Temp map from file */
	//
    index = 0;
    for (j=0; j<Tempmap->my; j++) {
		for (i=0; i<Tempmap->mx; i++) {
            fscanf(fp,"%lf",&Tempmap->data[index]);
			//printf("%d \n", Tempmap->data[index]);
            index++; 
        }
    }
	//		
	/* set pointer */
	*map = Tempmap;
	fclose(fp);
}


void TempMapApplyScale(TempMap map,pTatinUnits *units)
{
	
    UnitsApplyInverseScaling(units->si_length,map->x0,&map->x0);
    UnitsApplyInverseScaling(units->si_length,map->y0,&map->y0);
    UnitsApplyInverseScaling(units->si_length,map->x1,&map->x1);
    UnitsApplyInverseScaling(units->si_length,map->y1,&map->y1);
    UnitsApplyInverseScaling(units->si_length,map->dx,&map->dx);
    UnitsApplyInverseScaling(units->si_length,map->dy,&map->dy);
    
}

void ProdMapApplyScale(TempMap map,pTatinUnits *units)
{
	/* NB production is in K/s/m^3 */
    PetscInt i; 
    
    UnitsApplyInverseScaling(units->si_length,map->x0,&map->x0);
    UnitsApplyInverseScaling(units->si_length,map->y0,&map->y0);
    UnitsApplyInverseScaling(units->si_length,map->x1,&map->x1);
    UnitsApplyInverseScaling(units->si_length,map->y1,&map->y1);
    UnitsApplyInverseScaling(units->si_length,map->dx,&map->dx);
    UnitsApplyInverseScaling(units->si_length,map->dy,&map->dy);
    
    for (i=0;i<(map->mx * map->my);i++){
        UnitsApplyInverseScaling(units->si_heatsource,map->data[i],&map->data[i]);   
    }
    
}

void DensMapApplyScale(TempMap map,pTatinUnits *units)
{
    PetscInt i; 
    
    UnitsApplyInverseScaling(units->si_stress,map->y0,&map->y0);
    UnitsApplyInverseScaling(units->si_stress,map->y1,&map->y1);
    UnitsApplyInverseScaling(units->si_stress,map->dy,&map->dy);
    
    for (i=0;i<(map->mx * map->my);i++){
        UnitsApplyInverseScaling(units->si_force_per_volume,map->data[i],&map->data[i]);   
    }
}

void RheolMapApplyScale(TempMap map,pTatinUnits *units)
{
    PetscInt i; 
    
    UnitsApplyInverseScaling(units->si_stress,map->y0,&map->y0);
    UnitsApplyInverseScaling(units->si_stress,map->y1,&map->y1);
    UnitsApplyInverseScaling(units->si_stress,map->dy,&map->dy);
    
}


void TempMapLoadFromFileScale_ASCII(const char filename[],TempMap *map,pTatinUnits *units)
{
	FILE *fp = NULL;
	TempMap Tempmap;
  char dummy[1000];
  
	int i,j,Tempmap_max;
  int index;
	
	/* open file to parse */
	fp = fopen(filename,"r");
	if (fp==NULL) {
		printf("Error(%s): Could not open file: %s \n",__func__, filename );
		exit(EXIT_FAILURE);
	}
	
	/* create data structure */
	TempMapCreate(&Tempmap);
	
	/* read header information, mx,my,x0,y0,x1,y1 */
//  fscanf(fp,"%s\n",dummy);
	fgets(dummy,sizeof(dummy),fp);
  fscanf(fp,"%d\n",&Tempmap->mx);
  fscanf(fp,"%d\n",&Tempmap->my);
  fscanf(fp,"%lf %lf %lf %lf\n",&Tempmap->x0,&Tempmap->y0,&Tempmap->x1,&Tempmap->y1);
    
    UnitsApplyInverseScaling(units->si_length,Tempmap->x0,&Tempmap->x0);
    UnitsApplyInverseScaling(units->si_length,Tempmap->y0,&Tempmap->y0);
    UnitsApplyInverseScaling(units->si_length,Tempmap->x1,&Tempmap->x1);
    UnitsApplyInverseScaling(units->si_length,Tempmap->y1,&Tempmap->y1);
    
	//
	Tempmap->dx = (Tempmap->x1 - Tempmap->x0)/(double)(Tempmap->mx);
	Tempmap->dy = (Tempmap->y1 - Tempmap->y0)/(double)(Tempmap->my);
		
	
	/* allocate data */
	Tempmap->data = malloc(sizeof(double)* Tempmap->mx * Tempmap->my );
	
	/* parse Temp map from file */
	//
  index = 0;
  for (j=0; j<Tempmap->my; j++) {
		for (i=0; i<Tempmap->mx; i++) {
      fscanf(fp,"%lf",&Tempmap->data[index]);
			//printf("%d \n", Tempmap->data[index]);
      index++; 
    }
  }
	//		
	/* set pointer */
	*map = Tempmap;
	fclose(fp);
}

#undef __FUNCT__  
#define __FUNCT__ "TempMapLoadFromFile_ASCII_ZIPPED"
void TempMapLoadFromFile_ASCII_ZIPPED(const char filename[],TempMap *map)
{
	SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Ascii zipped loading is not supported");
}
#undef __FUNCT__  
#define __FUNCT__ "TempMapLoadFromFileScale_ASCII_ZIPPED"
void TempMapLoadFromFileScale_ASCII_ZIPPED(const char filename[],TempMap *map,pTatinUnits *units)
{
	SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Ascii zipped loading is not supported");
}
void TempMapLoadFromFile(const char filename[],TempMap *map)
{
	size_t len;
	int is_zipped;
	int matched_extension;
	
	is_zipped = 0;
    
	/* check extensions for common zipped file extensions */
	len = strlen(filename);
	matched_extension = strcmp(&filename[len-8],".tar.gz");
	if (matched_extension == 0) {
		printf("  Detected .tar.gz\n");
		is_zipped = 1;
	}
	matched_extension = strcmp(&filename[len-5],".tgz");
	if (matched_extension == 0) {
		printf("  Detected .tgz\n");
		is_zipped = 1;
	}
	matched_extension = strcmp(&filename[len-3],".Z");
	if (matched_extension == 0) {
		printf("  Detected .Z\n");
		is_zipped = 1;
	}
    
	if (is_zipped == 1) {
		TempMapLoadFromFile_ASCII_ZIPPED(filename,map);
	} else {
		TempMapLoadFromFile_ASCII(filename,map);
	}
}


void TempMapLoadFromFileScale(const char filename[],TempMap *map,pTatinUnits *units)
{
	size_t len;
	int is_zipped;
	int matched_extension;
	
	is_zipped = 0;

	/* check extensions for common zipped file extensions */
	len = strlen(filename);
	matched_extension = strcmp(&filename[len-8],".tar.gz");
	if (matched_extension == 0) {
		printf("  Detected .tar.gz\n");
		is_zipped = 1;
	}
	matched_extension = strcmp(&filename[len-5],".tgz");
	if (matched_extension == 0) {
		printf("  Detected .tgz\n");
		is_zipped = 1;
	}
	matched_extension = strcmp(&filename[len-3],".Z");
	if (matched_extension == 0) {
		printf("  Detected .Z\n");
		is_zipped = 1;
	}

	if (is_zipped == 1) {
		TempMapLoadFromFileScale_ASCII_ZIPPED(filename,map,units);
	} else {
		TempMapLoadFromFileScale_ASCII(filename,map,units);
	}
}


void TempMapGetTemperature(TempMap Tempmap,double xp[],double *Temp)
{
	double temp;
	TempMapGetDouble(Tempmap,xp,&temp);
    *Temp = temp;
}


void TempMapGetDouble(TempMap Tempmap,double xp[],double *Temp)
{
	int i,j,index;
	
	(*Temp) = (int)TEMP_MAP_POINT_OUTSIDE;
	
	if (xp[0] < Tempmap->x0) { return; }
	if (xp[0] > Tempmap->x1) { return; }
	if (xp[1] < Tempmap->y0) { return; }
	if (xp[1] > Tempmap->y1) { return; }
	
	i = (xp[0] - Tempmap->x0)/Tempmap->dx;
	j = (xp[1] - Tempmap->y0)/Tempmap->dy;
	if (i==Tempmap->mx) { i--; }
	if (j==Tempmap->my) { j--; }
	
	TempMapGetIndex(Tempmap,i,j,&index);
	
	*Temp = Tempmap->data[index];
	
}

void RheolMapGetDouble(TempMap Tempmap,double xp[],double *Temp)
{
	int i,j,index;
	
	(*Temp) = (int)TEMP_MAP_POINT_OUTSIDE;
	
	if (xp[0] < Tempmap->x0) { xp[0] = Tempmap->x0; }
	if (xp[0] > Tempmap->x1) { xp[0] = Tempmap->x1; }
	if (xp[1] < Tempmap->y0) { xp[1] = Tempmap->y0; }
	if (xp[1] > Tempmap->y1) { xp[1] = Tempmap->y1; }
	
	i = (xp[0] - Tempmap->x0)/Tempmap->dx;
	j = (xp[1] - Tempmap->y0)/Tempmap->dy;
	if (i==Tempmap->mx) { i--; }
	if (j==Tempmap->my) { j--; }
	
	TempMapGetIndex(Tempmap,i,j,&index);
	
	*Temp = Tempmap->data[index];
	
}

/*
 
 gnuplot> set pm3d map
 gnuplot> splot "filename" 

*/
void TempMapViewGnuplot(const char filename[],TempMap Tempmap)
{
	FILE *fp = NULL;
	int i,j;
	
	/* open file to parse */
	fp = fopen(filename,"w");
	if (fp==NULL) {
		printf("Error(%s): Could not open file: %s \n",__func__, filename );
		exit(EXIT_FAILURE);
	}
	fprintf(fp,"# Temp map information \n");
	fprintf(fp,"# Temp map : (x0,y0) = (%1.4e,%1.4e) \n",Tempmap->x0,Tempmap->y0);
	fprintf(fp,"# Temp map : (x1,y1) = (%1.4e,%1.4e) \n",Tempmap->x1,Tempmap->y1);
	fprintf(fp,"# Temp map : (dx,dy) = (%1.4e,%1.4e) \n",Tempmap->dx,Tempmap->dy);
	fprintf(fp,"# Temp map : (mx,my) = (%d,%d) \n",Tempmap->mx,Tempmap->my);

	for (j=0; j<Tempmap->my; j++) {
		for (i=0; i<Tempmap->mx; i++) {
			double x,y;
			int index;
			
			x = Tempmap->x0 + Tempmap->dx * 0.5 + i * Tempmap->dx;
			y = Tempmap->y0 + Tempmap->dy * 0.5 + j * Tempmap->dy;
			TempMapGetIndex(Tempmap,i,j,&index);
			
			fprintf(fp,"%1.4e %1.4e %1.4e \n", x,y,(double)Tempmap->data[index]);
		}fprintf(fp,"\n");
	}
	fclose(fp);
	
}

#undef __FUNCT__  
#define __FUNCT__ "pTatinCtxAttachTempMap"
PetscErrorCode pTatinCtxAttachTempMap(pTatinCtx ctx,TempMap map)
{
    char *name;
	PetscContainer container;
	PetscErrorCode ierr;
	
     PetscFunctionBegin;
    asprintf(&name,"tempmap");    
    ierr = pTatinCtxAttachMap(ctx, map, name);    
 	free(name);
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatinCtxGetTempMap"
PetscErrorCode pTatinCtxGetTempMap(pTatinCtx ctx,TempMap *map)
{
    char *name;
    TempMap mymap;
	PetscErrorCode ierr;
    PetscFunctionBegin;    
    asprintf(&name,"tempmap");
    ierr = pTatinCtxGetMap(ctx, &mymap , name);    
    free(name);
    *map = mymap;
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "pTatinCtxGetMap"
PetscErrorCode pTatinCtxGetMap(pTatinCtx ctx,TempMap *map, char *name)
{
	TempMap mymap;
	PetscContainer container;
	PetscErrorCode ierr;
    
    PetscFunctionBegin;
	ierr = PetscObjectQuery((PetscObject)ctx->model_data,name,(PetscObject*)&container);CHKERRQ(ierr);
	if (!container) SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONG,"No data with name %s was composed with ctx->model_data",name);
	ierr = PetscContainerGetPointer(container,(void**)&mymap);CHKERRQ(ierr);
	*map = mymap;
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatinCtxAttachMap"
PetscErrorCode pTatinCtxAttachMap(pTatinCtx ctx,TempMap map, char *name)
{
    PetscContainer container;
	PetscErrorCode ierr;
    
    PetscFunctionBegin;
    ierr = PetscContainerCreate(PETSC_COMM_WORLD,&container);CHKERRQ(ierr);
    ierr = PetscContainerSetPointer(container,(void*)map);CHKERRQ(ierr);
	
	ierr = PetscObjectCompose((PetscObject)ctx->model_data,name,(PetscObject)container);CHKERRQ(ierr);
	
	
	PetscFunctionReturn(0);
}





#if 0
void test_TempMap(void)
{
	TempMap Tempmap;
	double xp[2];
	int Temp;
	
//	TempMapLoadFromFile("test.bmp",&Tempmap);
	TempMapLoadFromFile("model_geometry",&Tempmap);
	
	xp[0] = 0.0;	xp[1] = 1.0;
	TempMapGetTempIndex(Tempmap,xp,&Temp);
	printf("x = ( %lf , %lf ) ==> Temp = %d \n", xp[0],xp[1],Temp); 

	xp[0] = 5.0;	xp[1] = 3.2;
	TempMapGetTempIndex(Tempmap,xp,&Temp);
	printf("x = ( %lf , %lf ) ==> Temp = %d \n", xp[0],xp[1],Temp); 
	
	xp[0] = -1.0;	xp[1] = 1.0;
	TempMapGetTempIndex(Tempmap,xp,&Temp);
	printf("x = ( %lf , %lf ) ==> Temp = %d \n", xp[0],xp[1],Temp); 
	
	TempMapViewGnuplot("test.gp",Tempmap);
	
	TempMapDestroy(&Tempmap);
	
}


int main(int nargs,char *args[])
{

	test_TempMap();
	
	return EXIT_SUCCESS;
}
#endif