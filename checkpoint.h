
#ifndef __checkpoint_h__
#define __checkpoint_h__

PetscErrorCode pTatinCheckpointWrite_options(const char dirname[]);
PetscErrorCode pTatinCheckpointWrite_context(pTatinCtx ctx,const char dirname[]);

PetscErrorCode pTatinCheckpointLoad_options(const char dirname[]);
PetscErrorCode pTatinCheckpointLoad_context(pTatinCtx ctx,const char dirname[]);

PetscErrorCode pTatinCheckpointWrite(pTatinCtx ctx,const char dirname[]);
PetscErrorCode pTatinCheckpointLoad(pTatinCtx ctx,const char dirname[]);

#endif