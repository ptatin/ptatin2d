//
//  topo_map.h
//  
//
//  Created by le pourhiet laetitia on 4/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef _TOPO_MAP_h
#define _TOPO_MAP_h

typedef struct _p_TopoMap *TopoMap;
struct _p_TopoMap {
	double x0,z0,x1,z1;
	double dx,dz;
	int mx,mz;
	double *data;
  double max_height;
};

void TopoMapCreate(TopoMap *map);
void TopoMapDestroy(TopoMap *map);
void TopoMapGetIndex(TopoMap pm,const int i,const int j, int *index);
void TopoMapLoadFromFile(const char filename[],TopoMap *map);
void TopoMapLoadFromFile_ASCII(const char filename[],TopoMap *map);
void TopoMapLoadFromFile_ASCII_ZIPPED(const char filename[],TopoMap *map);
void TopoMapLoadFromFileScale(const char filename[],TopoMap *map,pTatinUnits *units);
void TopoMapLoadFromFileScale_ASCII(const char filename[],TopoMap *map,pTatinUnits *units);
void TopoMapLoadFromFileScale_ASCII_ZIPPED(const char filename[],TopoMap *map,pTatinUnits *units);
void TopoMapGetHeight(TopoMap topomap,double xp[],double *height);
void TopoMapViewGnuplot(const char filename[],TopoMap topomap);
void TopoMapLoadFromFile_ASCII_ZIPPED(const char filename[],TopoMap *map);
void TopoMapLoadFromFile_ASCII(const char filename[],TopoMap *map);


PetscErrorCode pTatinCtxAttachTopoMap(pTatinCtx ctx,TopoMap map);
PetscErrorCode pTatinCtxGetTopoMap(pTatinCtx ctx,TopoMap *map);

#endif
