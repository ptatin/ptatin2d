/*

Design choices:
 + Store the velocity vector, u_minus_V
 This is the vector which will be used by the adv-diff solver and should
 contain the difference between the fluid velocity u, and corrections from
 mesh moments (the arbitrariness) V.
 + The other reason to store V is that it is likely to be represented in
 function spaces. I.e., on the stokes solver side its a Q2 space, on the SUPG
 side it will be represented as a Q1 space.
 
 
*/
#ifndef __phys_darcy_equation_h__
#define __phys_darcy_equation_h__

typedef struct _p_PhysCompDarcyCtx      *PhysCompDarcyCtx;
typedef struct _p_QuadratureVolumeDarcy *QuadratureVolumeDarcy;
typedef struct _p_CoefficientsDarcyEq   CoefficientsDarcyEq;

struct _p_PhysCompDarcyCtx {
    DM                      daPf;
	Vec                     Pf;
	BCList                  Pf_bclist;
	QuadratureVolumeDarcy   Q;
	PetscInt                mx,my; /* global mesh size */
	/* SUPG DATA */
	Vec                     Pfold; /* old pressure solution vector */
	
};


struct _p_DarcyConstants {
    PetscReal perm_lower_cutoff, perm_upper_cutoff;
    PetscBool apply_perm_cutoff; 
    /* rheology; "constant" ==>> const (short name) */
    PetscReal phi0[MAX_PHASE];
    PetscReal perm0[MAX_PHASE];
    PetscReal sr0;
    PetscReal eta0;
    PetscReal beta_H20;
    PetscReal rho_H20;
    PetscReal vis_H20;
    PetscReal beta_rock;
};




struct _p_CoefficientsDarcyEq {
  PetscScalar diff;
  PetscScalar source;
};

struct _p_QuadratureVolumeDarcy {
	/* for each element */
	PetscInt    ngp;
	PetscScalar *xi;
	PetscScalar *weight;
	/* basis function evaluations */
	PetscInt             u_basis;
	PetscScalar          **U_Ni; /* U_Ni[p][...] */
	PetscScalar          ***U_GNi;
	PetscInt             ncells;
	CoefficientsDarcyEq *cellproperties;
};	


PetscErrorCode QuadratureVolumeDarcyCreate(QuadratureStokesRule rule,PetscInt ncells,QuadratureVolumeDarcy *Q);
PetscErrorCode QuadratureVolumeDarcyDestroy(QuadratureVolumeDarcy *Q);
PetscErrorCode QuadratureVolumeDarcyGetCell(QuadratureVolumeDarcy Q,PetscInt cidx,CoefficientsDarcyEq **points);

	
PetscErrorCode PhysComp_DarcyEquationCreate1(PhysCompDarcyCtx *p);
PetscErrorCode PhysComp_DarcyEquationCreate2(DM davq2,PhysCompDarcyCtx *p);
PetscErrorCode PhysComp_DarcyEquationDestroy(PhysCompDarcyCtx *p);

PetscErrorCode FormJacobian_Darcy_qp(PetscReal time,Vec X,PetscReal dt,Mat *A,Mat *B,MatStructure *mstr,void *ctx);
PetscErrorCode FormFunction_Darcy_Pf_qp(PhysCompDarcyCtx data,PetscReal dt,DM da,
						PetscScalar *LA_phi,PetscScalar *LA_philast,PetscScalar *LA_R);
PetscErrorCode FormFunction_Darcy_qp(PetscReal time,Vec X,PetscReal dt,Vec Pf,void *ctx);

PetscErrorCode pTatinLoadPhysics_DarcyEquation(pTatinCtx ctx);

void FormMass_FormResidual_qp(
	PetscScalar Re[],
	PetscReal dt,
	PetscScalar el_coords[],
	PetscScalar el_phi[],PetscScalar el_phi_last[],
	PetscScalar gp_kappa[],PetscScalar gp_Q[],
	PetscInt ngp,PetscScalar gp_xi[],PetscScalar gp_weight[] );
	
	void AElement_qp( PetscScalar Re[],PetscReal dt,PetscScalar el_coords[],
				  PetscScalar gp_kappa[],PetscInt ngp,PetscScalar gp_xi[],PetscScalar gp_weight[] );




#endif

