

#include "stdio.h"
#include "stdlib.h"

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscdm.h"

#include "daSUPG_Q1.h"
#include "pTatin2d.h"


/*
 Structure for solving
 \dot\phi + a.v_i \phi_{i} = - b.(kappa \phi_i)_{,i} + f
 */

PetscReal THETA = 1.0; /* 1 = fully implicit */

/* create the work vectors */
#undef __FUNCT__
#define __FUNCT__ "DMDACreateSUPGWorkVectors"
PetscErrorCode DMDACreateSUPGWorkVectors(DM dm,Vec *V,Vec *phi)
{
  Vec coords;
  PetscErrorCode ierr;
	
  PetscFunctionBegin;
  if (V) {
    DM cda;
    ierr = DMDAGetCoordinateDA(dm,&cda);CHKERRQ(ierr);
    ierr = DMCreateGlobalVector(cda,V);CHKERRQ(ierr);
  }
  if (phi) {
    ierr = DMCreateGlobalVector(dm,phi);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_DMDACreateSUPG"
PetscErrorCode _DMDACreateSUPG(DM da,DMDASUPGParameters *supg)
{
  DMDASUPGParameters params;
  PetscInt int_opt;
  PetscScalar real_opt;
  PetscBool found;
  PetscErrorCode ierr;
	
  PetscFunctionBegin;
	
  /* setup parameters for supg method */
  ierr = PetscMalloc(sizeof(struct _DMDASUPGParameters),&params);CHKERRQ(ierr);
  params->n_corrector_steps   = 1;
  params->courant_factor      = 1.0;
  params->theta               = 1.0;
  params->approximation_type  = 1;
	params->daT = da;
	
  found = PETSC_FALSE;
  PetscOptionsGetInt(PETSC_NULL,"-supg_corrector_steps",&int_opt,&found);
  if (found) {
    if (int_opt < 1) {
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"SUPG: Number of corrector steps must be >= 1");
    }
    params->n_corrector_steps = int_opt;
  }
	
  found = PETSC_FALSE;
  PetscOptionsGetScalar(PETSC_NULL,"-supg_courant_factor",&real_opt,&found);
  if (found) {
    params->courant_factor = real_opt;
  }

  found = PETSC_FALSE;
  PetscOptionsGetScalar(PETSC_NULL,"-supg_theta",&real_opt,&found);
  if (found) {
    params->theta = real_opt;
  }
	
  found = PETSC_FALSE;
  PetscOptionsGetInt(PETSC_NULL,"-supg_approx_scheme",&int_opt,&found);
  if (found) {
    if (int_opt < 1) {
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"SUPG: Approximation scheme >= 1");
    }
    params->approximation_type = int_opt;
  }
	
  ierr = DMDACreateSUPGWorkVectors(da,PETSC_NULL,&params->phi_last);CHKERRQ(ierr);
  ierr = DMDACreateSUPGWorkVectors(da,PETSC_NULL,&params->phi);CHKERRQ(ierr);
  ierr = DMDACreateSUPGWorkVectors(da,PETSC_NULL,&params->F);CHKERRQ(ierr);
  ierr = DMGetMatrix(da,MATAIJ,&params->J);CHKERRQ(ierr);

	*supg = params;
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDACreateSUPG2d"
PetscErrorCode DMDACreateSUPG2d(MPI_Comm comm,PetscInt Mx,PetscInt My,DM *da)
{
	DM daT;
  DMDASUPGParameters params;
  PetscContainer container;
  PetscInt int_opt;
  PetscScalar real_opt;
  PetscBool found;
  PetscErrorCode ierr;
	
  PetscFunctionBegin;
  /* create the da with 1 dof, and stencil width of 1 (Q1 elements) */
  ierr = DMDACreate2d(comm,DMDA_BOUNDARY_NONE,DMDA_BOUNDARY_NONE,DMDA_STENCIL_BOX,Mx+1,My+1,PETSC_DECIDE,PETSC_DECIDE,1,1,PETSC_NULL,PETSC_NULL,&daT);CHKERRQ(ierr);
	
  /* setup parameters for supg method */
	ierr = _DMDACreateSUPG(daT,&params);CHKERRQ(ierr);
	
  /* add params to the DM */
  ierr = PetscContainerCreate(PETSC_COMM_WORLD,&container);CHKERRQ(ierr);
  ierr = PetscContainerSetPointer(container,(void*)params);CHKERRQ(ierr);
  ierr = PetscObjectCompose((PetscObject)(daT),"DASUPGparams",(PetscObject)container);CHKERRQ(ierr);
	
	*da = daT;
	
  PetscFunctionReturn(0);
}

/*
 Create da for adv-diffusion by refining another da.
 This enables us to easily call DMGetInterpolation
 */
#undef __FUNCT__
#define __FUNCT__ "DMDACreateSUPG2dFromDM"
PetscErrorCode DMDACreateSUPG2dFromDM(DM dacoarse,PetscInt Refx,PetscInt Refy,DM *da)
{
	DM daT;
  DMDASUPGParameters params;
  PetscContainer container;
  PetscInt int_opt,dofcoarse;
  PetscScalar real_opt;
  PetscBool found;
  DM daref;
  PetscErrorCode ierr;
	
  PetscFunctionBegin;
  /* create the da with 1 dof, and stencil width of 1 (Q1 elements) */
  ierr = DMDASetRefinementFactor(dacoarse,Refx,Refy,1);CHKERRQ(ierr);
  ierr = DMRefine(dacoarse,PETSC_COMM_WORLD,&daref);CHKERRQ(ierr);
  ierr = DMDAGetInfo(dacoarse,0, 0,0,0, 0,0,0, &dofcoarse,0, 0,0,0, 0 );CHKERRQ(ierr);
  if (dofcoarse!=1) { /* pull out the info you need to create a compatible, but single dof da */
    const PetscInt *lx,*ly,*lz;
    PetscInt M,N,P,Mx,My,Mz;
		
    ierr = DMDAGetInfo(daref,0, &Mx,&My,&Mz, &M,&N,&P, 0,0, 0,0,0, 0 );CHKERRQ(ierr);

    ierr = DMDAGetOwnershipRanges(daref,&lx,&ly,&lz);CHKERRQ(ierr);
    ierr = DMDACreate2d(((PetscObject)daref)->comm,DMDA_BOUNDARY_NONE,DMDA_BOUNDARY_NONE,DMDA_STENCIL_BOX,Mx,My,M,N,1,1,lx,ly,&daT);CHKERRQ(ierr);
    ierr = DMDestroy(&daref);CHKERRQ(ierr);
  } else {
    daT = daref;
  }
	
  /* setup parameters for supg method */
	ierr = _DMDACreateSUPG(daT,&params);CHKERRQ(ierr);
	
  /* add params to the DM */
  ierr = PetscContainerCreate(PETSC_COMM_WORLD,&container);CHKERRQ(ierr);
  ierr = PetscContainerSetPointer(container,(void*)params);CHKERRQ(ierr);
  ierr = PetscObjectCompose((PetscObject)(daT),"DASUPGparams",(PetscObject)container);CHKERRQ(ierr);
	
	*da = daT;
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDADestroySUPG"
PetscErrorCode DMDADestroySUPG(DM *da)
{
  DMDASUPGParameters params;
  PetscContainer container;
  PetscErrorCode ierr;
	
  PetscFunctionBegin;
	
  ierr = PetscObjectQuery((PetscObject)*da,"DASUPGparams",(PetscObject*)&container);CHKERRQ(ierr);
  if (!container) { SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONG,"No DMDASUPGParameters composed with given DM"); }
  ierr = PetscContainerGetPointer(container,(void**)&params);CHKERRQ(ierr);
	
  ierr = VecDestroy(&params->phi_last);CHKERRQ(ierr);
  ierr = VecDestroy(&params->phi);CHKERRQ(ierr);
  ierr = VecDestroy(&params->F);CHKERRQ(ierr);
  ierr = MatDestroy(&params->J);CHKERRQ(ierr);
  ierr = PetscFree(params);CHKERRQ(ierr);
  ierr = PetscContainerDestroy(&container);CHKERRQ(ierr);
  ierr = DMDestroy(da);CHKERRQ(ierr);
  *da = PETSC_NULL;
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DASUPGGetParameters"
PetscErrorCode DASUPGGetParameters(DM da,DMDASUPGParameters *p)
{
  DMDASUPGParameters params = PETSC_NULL;
  PetscContainer container = PETSC_NULL;
  PetscErrorCode ierr;
	
  PetscFunctionBegin;
  ierr = PetscObjectQuery((PetscObject)da,"DASUPGparams",(PetscObject*)&container);CHKERRQ(ierr);
  if (!container) { PetscPrintf(PETSC_COMM_WORLD,"No DMDASUPGParameters composed with given DM"); }
  else { ierr = PetscContainerGetPointer(container,(void**)&params);CHKERRQ(ierr); }
	
  *p = params;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DASUPGView"
PetscErrorCode DASUPGView(DM da,Vec kappa,Vec V,Vec phi,PetscInt step)
{
  PetscViewer viewer;
  char fname[1000];
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  sprintf(fname,"phi_%d.vtk",step);
  ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, fname, &viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
  ierr = DMView(da, viewer);CHKERRQ(ierr);
  ierr = VecView(phi, viewer);CHKERRQ(ierr);
  ierr = VecView(V, viewer);CHKERRQ(ierr);
  ierr = VecView(kappa, viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/* fem business */
void ConstructGaussQuadrature_2x2_2d(PetscInt *ngp,PetscScalar gp_xi[][2],PetscScalar gp_weight[])
{
  *ngp         = 4;
  gp_xi[0][0]  = -0.57735026919;gp_xi[0][1] = -0.57735026919;
  gp_xi[1][0]  = -0.57735026919;gp_xi[1][1] =  0.57735026919;
  gp_xi[2][0]  =  0.57735026919;gp_xi[2][1] =  0.57735026919;
  gp_xi[3][0]  =  0.57735026919;gp_xi[3][1] = -0.57735026919;
  gp_weight[0] = 1.0;
  gp_weight[1] = 1.0;
  gp_weight[2] = 1.0;
  gp_weight[3] = 1.0;
}

/* procs to the left claim the ghost node as their element */
#undef __FUNCT__  
#define __FUNCT__ "DMDAGetLocalElementSize_Q1"
PetscErrorCode DMDAGetLocalElementSize_Q1(DM da,PetscInt *mxl,PetscInt *myl,PetscInt *mzl)
{
  PetscInt m,n,p,M,N,P;
  PetscInt sx,sy,sz;
  PetscInt ml,nl,pl;
	
  PetscFunctionBegin;
  DMDAGetInfo(da,0,&M,&N,&P,0,0,0,0,0,0,0,0,0);
  DMDAGetCorners(da,&sx,&sy,&sz,&m,&n,&p);
	
  ml = nl = pl = 0;
  if (mxl != PETSC_NULL) {
    *mxl = m;
    if ((sx+m) == M) {  /* last proc */
      *mxl = m-1;
    }
		
    ml = *mxl;
  }
  if (myl != PETSC_NULL) {
    *myl = n;
    if ((sy+n) == N) {  /* last proc */
      *myl = n-1;
    }
		
    nl = *myl;
  }
  if (mzl != PETSC_NULL) {
    *mzl = p;
    if ((sz+p) == P) {  /* last proc */
      *mzl = p-1;
    }
		
    pl = *mzl;
  }
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "DMDAGetElementCorners_Q1"
PetscErrorCode DMDAGetElementCorners_Q1(DM da,
																						PetscInt *sx,PetscInt *sy,PetscInt *sz,
																						PetscInt *mx,PetscInt *my,PetscInt *mz)
{
  PetscInt si,sj,sk;
	
  PetscFunctionBegin;
  DMDAGetGhostCorners(da,&si,&sj,&sk,0,0,0);
	
  *sx = si;
  if (si != 0) {
    *sx = si+1;
  }
	
  *sy = sj;
  if (sj != 0) {
    *sy = sj+1;
  }
	
  if (sk != PETSC_NULL) {
    *sz = sk;
    if (sk != 0) {
      *sz = sk+1;
    }
  }
	
  DMDAGetLocalElementSize_Q1(da,mx,my,mz);
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "GetElementCoords_Q1"
PetscErrorCode GetElementCoords_Q1(DMDACoor2d **_coords,PetscInt ei,PetscInt ej,PetscScalar el_coords[])
{
  PetscFunctionBegin;
  /* get coords for the element */
  el_coords[NSD*0+0] = _coords[ej  ][ei  ].x; el_coords[NSD*0+1] = _coords[ej  ][ei  ].y;
  el_coords[NSD*1+0] = _coords[ej+1][ei  ].x; el_coords[NSD*1+1] = _coords[ej+1][ei  ].y;
  el_coords[NSD*2+0] = _coords[ej+1][ei+1].x; el_coords[NSD*2+1] = _coords[ej+1][ei+1].y;
  el_coords[NSD*3+0] = _coords[ej  ][ei+1].x; el_coords[NSD*3+1] = _coords[ej  ][ei+1].y;
  PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "DMDAGetElementValues_Q1_scalar"
PetscErrorCode DMDAGetElementValues_Q1_scalar(PetscScalar **data,PetscInt i,PetscInt j,PetscScalar el_data[])
{
  /* scalar */
  PetscFunctionBegin;
  el_data[0] = data[j  ][i  ];   /* P0 */
  el_data[1] = data[j+1][i  ];   /* P1 */
  el_data[2] = data[j+1][i+1];   /* P2 */
  el_data[3] = data[j  ][i+1];   /* P3 */
  PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "DMDAGetElementValues_Q1_vector"
PetscErrorCode DMDAGetElementValues_Q1_vector(PetscScalar ***data,PetscInt i,PetscInt j,PetscScalar el_data[])
{
  /* 2 vector */
  PetscFunctionBegin;
  el_data[NSD*0  ] = data[j  ][i  ][0];
  el_data[NSD*0+1] = data[j  ][i  ][1];
	
  el_data[NSD*1  ] = data[j+1][i  ][0];
  el_data[NSD*1+1] = data[j+1][i  ][1];
	
  el_data[NSD*2  ] = data[j+1][i+1][0];
  el_data[NSD*2+1] = data[j+1][i+1][1];
	
  el_data[NSD*3  ] = data[j  ][i+1][0];
  el_data[NSD*3+1] = data[j  ][i+1][1];
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "GetElementEqnIndicesQ1_scalar"
PetscErrorCode GetElementEqnIndicesQ1_scalar(PetscInt ei,PetscInt ej,PetscInt nxg, PetscInt globalindices[],PetscInt eldofidx[])
{
	PetscInt nid,idx,n,c;
	
	PetscFunctionBegin;
	n = 0; c = 0;
  nid = (ei  )+(ej  )*nxg;	eldofidx[c++] = globalindices[nid];
  nid = (ei  )+(ej+1)*nxg;	eldofidx[c++] = globalindices[nid];
  nid = (ei+1)+(ej+1)*nxg;	eldofidx[c++] = globalindices[nid];
  nid = (ei+1)+(ej  )*nxg;	eldofidx[c++] = globalindices[nid];
	
  PetscFunctionReturn(0);
}

void ConstructQ12D_Ni(PetscScalar _xi[],PetscScalar Ni[])
{
  PetscScalar xi  = _xi[0];
  PetscScalar eta = _xi[1];
	
  Ni[0] = 0.25*(1.0-xi)*(1.0-eta);
  Ni[1] = 0.25*(1.0-xi)*(1.0+eta);
  Ni[2] = 0.25*(1.0+xi)*(1.0+eta);
  Ni[3] = 0.25*(1.0+xi)*(1.0-eta);
}

void ConstructQ12D_GNi(PetscScalar _xi[],PetscScalar GNi[][NODES_PER_EL_Q1])
{
  PetscScalar xi  = _xi[0];
  PetscScalar eta = _xi[1];
	
  GNi[0][0] = -0.25*(1.0-eta);
  GNi[0][1] = -0.25*(1.0+eta);
  GNi[0][2] =   0.25*(1.0+eta);
  GNi[0][3] =   0.25*(1.0-eta);
	
  GNi[1][0] = -0.25*(1.0-xi);
  GNi[1][1] =   0.25*(1.0-xi);
  GNi[1][2] =   0.25*(1.0+xi);
  GNi[1][3] = -0.25*(1.0+xi);
}

void ConstructQ12D_GNx(PetscScalar GNi[][NODES_PER_EL_Q1],PetscScalar GNx[][NODES_PER_EL_Q1],PetscScalar coords[],PetscScalar *det_J)
{
  PetscScalar J00,J01,J10,J11,J;
  PetscScalar iJ00,iJ01,iJ10,iJ11;
  PetscInt    i;
	
  J00 = J01 = J10 = J11 = 0.0;
  for (i = 0; i < NODES_PER_EL_Q1; i++) {
    PetscScalar cx = coords[ 2*i+0 ];
    PetscScalar cy = coords[ 2*i+1 ];
		
    J00 = J00+GNi[0][i]*cx;      /* J_xx = dx/dxi */
    J01 = J01+GNi[0][i]*cy;      /* J_xy = dy/dxi */
    J10 = J10+GNi[1][i]*cx;      /* J_yx = dx/deta */
    J11 = J11+GNi[1][i]*cy;      /* J_yy = dy/deta */
  }
  J = (J00*J11)-(J01*J10);
	
  iJ00 =  J11/J;
  iJ01 = -J01/J;
  iJ10 = -J10/J;
  iJ11 =  J00/J;
  for (i = 0; i < NODES_PER_EL_Q1; i++) {
    GNx[0][i] = GNi[0][i]*iJ00+GNi[1][i]*iJ01;
    GNx[1][i] = GNi[0][i]*iJ10+GNi[1][i]*iJ11;
  }
  *det_J = J;
}

void ConstructQ12D_ElementTransformation(PetscScalar coords[],PetscScalar GNi[][NODES_PER_EL_Q1],PetscScalar _J[][2],PetscScalar _invJ[][2])
{
  PetscScalar J00,J01,J10,J11,J;
  PetscScalar iJ00,iJ01,iJ10,iJ11;
  PetscInt    i;
	
  J00 = J01 = J10 = J11 = 0.0;
  for (i = 0; i < NODES_PER_EL_Q1; i++) {
    PetscScalar cx = coords[ 2*i+0 ];
    PetscScalar cy = coords[ 2*i+1 ];
		
    J00 = J00+GNi[0][i]*cx;      /* J_xx = dx/dxi */
    J01 = J01+GNi[0][i]*cy;      /* J_xy = dy/dxi */
    J10 = J10+GNi[1][i]*cx;      /* J_yx = dx/deta */
    J11 = J11+GNi[1][i]*cy;      /* J_yy = dy/deta */
  }
	_J[0][0] = J00;
	_J[0][1] = J01;
	_J[1][0] = J10;
	_J[1][1] = J11;
  
	J = (J00*J11)-(J01*J10);
	
  iJ00 =  J11/J;
  iJ01 = -J01/J;
  iJ10 = -J10/J;
  iJ11 =  J00/J;
	
	_invJ[0][0] = iJ00;
	_invJ[0][1] = iJ01;
	_invJ[1][0] = iJ10;
	_invJ[1][1] = iJ11;
}

#undef __FUNCT__  
#define __FUNCT__ "DMDASetValuesLocalStencil_Q1_ADD_VALUES"
PetscErrorCode DMDASetValuesLocalStencil_Q1_ADD_VALUES(PetscScalar **data,MatStencil eqn[],PetscScalar Re[])
{
  PetscInt n;
  PetscFunctionBegin;
  for (n=0; n<NODES_PER_EL_Q1; n++) {
    data[ eqn[n].j ][ eqn[n].i ] = data[ eqn[n].j ][ eqn[n].i ] + Re[n];
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "DMDAGetElementEqnums_Q1_scalar"
PetscErrorCode DMDAGetElementEqnums_Q1_scalar(PetscInt i,PetscInt j,MatStencil s_p[])
{
  PetscFunctionBegin;
  s_p[0].i = i;   s_p[0].j = j;   s_p[0].c = 0;   /* P0 */
  s_p[1].i = i;   s_p[1].j = j+1; s_p[1].c = 0;   /* P0 */
  s_p[2].i = i+1; s_p[2].j = j+1; s_p[2].c = 0;   /* P1 */
  s_p[3].i = i+1; s_p[3].j = j;   s_p[3].c = 0;   /* P1 */
  PetscFunctionReturn(0);
}

/* SUPG business */
/** AdvectionDiffusion_UpwindXiExact - Brooks, Hughes 1982 equation 2.4.2
 * \bar \xi = coth( \alpha ) - \frac{1}{\alpha} */
double AdvDiffResidualForceTerm_UpwindXiExact(double pecletNumber)
{
  if (fabs(pecletNumber) < 1.0e-8 ) {
    return 0.33333333333333 * pecletNumber;
  } else if (pecletNumber < -20.0) {
    return -1.0 - 1.0/pecletNumber;
  } else if (pecletNumber > 20.0) {
    return +1.0 - 1.0/pecletNumber;
  }
  return cosh( pecletNumber )/sinh( pecletNumber ) - 1.0/pecletNumber;
}

/** AdvectionDiffusion_UpwindXiDoublyAsymptoticAssumption - Brooks, Hughes 1982 equation 3.3.1
 * Simplification of \bar \xi = coth( \alpha ) - \frac{1}{\alpha} from Brooks, Hughes 1982 equation 2.4.2
 * 
 *            { -1               for \alpha <= -3
 * \bar \xi ~ { \frac{/alpha}{3} for -3 < \alpha <= 3
 *            { +1               for \alpha > +3 */
double AdvDiffResidualForceTerm_UpwindXiDoublyAsymptoticAssumption(double pecletNumber)
{
  if (pecletNumber <= -3.0) {
    return -1;
  } else if (pecletNumber <= 3.0) {
    return 0.33333333333333 * pecletNumber;
  } else {
    return 1.0;
  }
}

/** AdvectionDiffusion_UpwindXiCriticalAssumption - Brooks, Hughes 1982 equation 3.3.2
 * Simplification of \bar \xi = coth( \alpha ) - \frac{1}{\alpha} from Brooks, Hughes 1982 equation 2.4.2
 * 
 *            { -1 - \frac{1}{\alpha}   for \alpha <= -1
 * \bar \xi ~ { 0                       for -1 < \alpha <= +1
 *            { +1 - \frac{1}{\alpha}   for \alpha > +1             */

double AdvDiffResidualForceTerm_UpwindXiCriticalAssumption(double pecletNumber)
{
  if (pecletNumber <= -1.0) {
    return -1.0 - 1.0/pecletNumber;
  } else if (pecletNumber <= 1.0) {
    return 0.0;
  } else {
    return 1.0 - 1.0/pecletNumber;
  }
}

/*
 Eqns 3.3.11 (transient) + 3.3.4, 3.3.5, 3.3.6
 */
#undef __FUNCT__
#define __FUNCT__ "DASUPG2dComputeElementStreamlineDiffusion"
PetscErrorCode DASUPG2dComputeElementStreamlineDiffusion(PetscScalar DX[],PetscScalar kappa[],PetscScalar u[],PetscScalar *khat)
{
  PetscScalar u_xi,v_eta;
  PetscScalar kappa_el,alpha_xi,alpha_eta,_khat,dxi,deta,xi,eta;
	
  PetscFunctionBegin;
  u_xi  = 0.25 * ( u[NSD*0  ] + u[NSD*1  ] + u[NSD*2  ] + u[NSD*3  ] );
  v_eta = 0.25 * ( u[NSD*0+1] + u[NSD*1+1] + u[NSD*2+1] + u[NSD*3+1] );
  kappa_el = 0.25 * ( kappa[0] + kappa[1] + kappa[2] + kappa[3] );
  
  dxi  = DX[0];
  deta = DX[1];
  alpha_xi  = 0.5 * u_xi  * dxi / kappa_el;
  alpha_eta = 0.5 * v_eta * deta / kappa_el;
	
  xi  = AdvDiffResidualForceTerm_UpwindXiExact(alpha_xi);
  eta = AdvDiffResidualForceTerm_UpwindXiExact(alpha_eta);
	
	//  khat = 0.5*(xi*u_xi*dxi + eta*v_eta*deta); /* steady state */
  _khat = 1.0 * 0.258198889747161 * ( xi * u_xi * dxi + eta * v_eta * deta ); /* transient case, sqrt(1/15) */
  *khat = _khat;
	
  PetscFunctionReturn(0);
}

/* Eqn 4.3.7 */
#undef __FUNCT__
#define __FUNCT__ "DASUPG2dComputeElementPecletNumber"
PetscErrorCode DASUPG2dComputeElementPecletNumber(PetscScalar DX[],PetscScalar kappa[],PetscScalar u[],PetscScalar *alpha)
{
  PetscScalar u_xi,v_eta,one_dxi2,one_deta2;
  PetscScalar kappa_el,_alpha,u_norm,dxi,deta;
	
  PetscFunctionBegin;
  u_xi  = 0.25 * ( u[NSD*0  ] + u[NSD*1  ] + u[NSD*2  ] + u[NSD*3  ] );
  v_eta = 0.25 * ( u[NSD*0+1] + u[NSD*1+1] + u[NSD*2+1] + u[NSD*3+1] );
  kappa_el = 0.25 * ( kappa[0] + kappa[1] + kappa[2] + kappa[3] );
	
  dxi  = DX[0];
  deta = DX[1];
  one_dxi2  = 1.0/dxi/dxi;
  one_deta2 = 1.0/deta/deta;
	
  u_norm = sqrt( u_xi*u_xi + v_eta*v_eta );
	
  _alpha = 0.5 * u_norm / ( (1.0e-32+kappa_el) * sqrt( one_dxi2 + one_deta2 ) );
  *alpha  = _alpha;  
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BROOKS_DASUPG2dComputeElementTimestep"
PetscErrorCode BROOKS_DASUPG2dComputeElementTimestep(PetscScalar Cr,PetscScalar DX[],PetscScalar kappa[],PetscScalar u[],PetscScalar *dta,PetscScalar *dtd)
{
  PetscScalar u_xi,v_eta,one_dxi2,one_deta2,dxi,deta,khat,alpha_eff;
  PetscScalar kappa_el;
  PetscScalar U,H,dt_advective,dt_diffusive;
	
  PetscFunctionBegin;
  u_xi  = 0.25 * ( u[NSD*0  ] + u[NSD*1  ] + u[NSD*2  ] + u[NSD*3  ] );
  v_eta = 0.25 * ( u[NSD*0+1] + u[NSD*1+1] + u[NSD*2+1] + u[NSD*3+1] );
  kappa_el = 0.25 * ( kappa[0] + kappa[1] + kappa[2] + kappa[3] );
	
  dxi  = DX[0];
  deta = DX[1];
	
  /* advective limit */
  one_dxi2  = 1.0/dxi/dxi;
  one_deta2 = 1.0/deta/deta;
	
  U = sqrt( u_xi*u_xi + v_eta*v_eta );
  H = 1.0 / sqrt( one_dxi2 + one_deta2 );
	
  dt_advective = Cr * H / (U+1.0e-32);
	
  /* diffusive limit */
  DASUPG2dComputeElementStreamlineDiffusion(DX,kappa,u,&khat);
	
  alpha_eff = 0.5 * U * H / (kappa_el + khat );
  dt_diffusive = alpha_eff * H / (U+1.0e-32);
	
  *dta = dt_advective;
  *dtd = dt_diffusive;
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DASUPG2dComputeElementTimestep"
PetscErrorCode DASUPG2dComputeElementTimestep(PetscScalar DX[],PetscScalar kappa[],PetscScalar u[],PetscScalar *dta,PetscScalar *dtd)
{
  PetscScalar u_xi,v_eta,one_dxi2,one_deta2,dxi,deta,khat,alpha_eff;
  PetscScalar kappa_el,CrFAC,dt_optimal,alpha;
  PetscScalar U,H,dt_advective,dt_diffusive;
	
  PetscFunctionBegin;
  u_xi  = 0.25 * ( u[NSD*0  ] + u[NSD*1  ] + u[NSD*2  ] + u[NSD*3  ] );
  v_eta = 0.25 * ( u[NSD*0+1] + u[NSD*1+1] + u[NSD*2+1] + u[NSD*3+1] );
  kappa_el = 0.25 * ( kappa[0] + kappa[1] + kappa[2] + kappa[3] );
	
  dxi  = DX[0];
  deta = DX[1];
	
  /* advective limit */
  one_dxi2  = 1.0/dxi/dxi;
  one_deta2 = 1.0/deta/deta;
	
  U = sqrt( u_xi*u_xi + v_eta*v_eta );
  H = 1.0 / sqrt( one_dxi2 + one_deta2 );
	
  DASUPG2dComputeElementPecletNumber(DX,kappa,u,&alpha);
	
  if (U<1.0e-32) {
		//    printf("diffusive limit \n");
    dt_diffusive = 0.5 * H*H / kappa_el;
    *dtd = dt_diffusive;
    *dta = dt_diffusive;
  } else {
    if (alpha >= 100.0 ) {
      CrFAC = 0.4;
    } else {
      CrFAC = PetscMin(1.0,alpha);
    }
    dt_optimal = CrFAC * H / (U+1.0e-32);
    *dta = dt_optimal;
    *dtd = dt_optimal;
  }
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DASUPG2dComputeElementTimestep_exp"
PetscErrorCode DASUPG2dComputeElementTimestep_exp(PetscScalar DX[],PetscScalar kappa[],PetscScalar u[],PetscScalar *dta,PetscScalar *dtd)
{
  PetscScalar u_xi,v_eta,one_dxi2,one_deta2,dxi,deta,khat,alpha_eff;
  PetscScalar kappa_el,CrFAC,dt_optimal,alpha;
  PetscScalar U,H,dt_advective,dt_diffusive,dt_adv_diff,fac;
	
  PetscFunctionBegin;
  u_xi  = 0.25 * ( u[NSD*0  ] + u[NSD*1  ] + u[NSD*2  ] + u[NSD*3  ] );
  v_eta = 0.25 * ( u[NSD*0+1] + u[NSD*1+1] + u[NSD*2+1] + u[NSD*3+1] );
  kappa_el = 0.25 * ( kappa[0] + kappa[1] + kappa[2] + kappa[3] );
	
  dxi  = DX[0];
  deta = DX[1];
	
  /* advective limit */
  one_dxi2  = 1.0/dxi/dxi;
  one_deta2 = 1.0/deta/deta;
	
  U = sqrt( u_xi*u_xi + v_eta*v_eta );
  H = 1.0 / sqrt( one_dxi2 + one_deta2 );
	
  fac = sqrt( U*U/one_dxi2 + U*U/one_deta2 );
	
  DASUPG2dComputeElementPecletNumber(DX,kappa,u,&alpha);
	printf("alpha = %1.4e\n",alpha);
	
  /* diffusive limit */
  DASUPG2dComputeElementStreamlineDiffusion(DX,kappa,u,&khat);
  alpha_eff = 0.5 * U * H / (kappa_el + khat );
	printf("alpha_eff = %1.4e\n",alpha_eff);
	
	
	dt_diffusive = alpha_eff *H/(U+1.0e-32);
	*dtd = dt_diffusive;
	
	dt_adv_diff = (1.0/alpha_eff) *H/(U+1.0e-32);
	*dta = dt_adv_diff;
	
	printf("adv = %lf : diff = %lf \n", dt_adv_diff,dt_diffusive );
	//	if (dt_adv_diff < dt_diffusive) {
	if (alpha >= 100.0 ) {
		CrFAC = 0.8;
	} else {
		CrFAC = PetscMin(1.0,alpha);
	}
	dt_optimal = CrFAC *H/(U+1.0e-32);
	*dta = dt_optimal;
	*dtd = dt_optimal;
	printf("dt_optimal = %lf \n", dt_optimal );
	//	}
	
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_DASUPG2dComputeElementTimestep"
PetscErrorCode _DASUPG2dComputeElementTimestep(PetscScalar Cr,PetscScalar DX[],PetscScalar kappa[],PetscScalar u[],PetscScalar *dta,PetscScalar *dtd)
{
  PetscScalar u_xi,v_eta,one_dxi2,one_deta2,dxi,deta,khat,alpha_eff;
  PetscScalar kappa_el;
  PetscScalar U,H,dt_advective,dt_diffusive;
	
  PetscFunctionBegin;
  u_xi  = 0.25 * ( u[NSD*0  ] + u[NSD*1  ] + u[NSD*2  ] + u[NSD*3  ] );
  u_xi = fabs(u_xi);
  v_eta = 0.25 * ( u[NSD*0+1] + u[NSD*1+1] + u[NSD*2+1] + u[NSD*3+1] );
  v_eta = fabs(v_eta);
  kappa_el = 0.25 * ( kappa[0] + kappa[1] + kappa[2] + kappa[3] );
	
  dxi  = DX[0];
  deta = DX[1];
	
  /* advective limit */
  dt_advective = Cr * dxi / ( u_xi + 1.0e-300 );
  dt_advective = PetscMin( dt_advective, Cr * deta / ( v_eta + 1.0e-300 ) );
	
  /* diffusive limit */
  dt_diffusive = Cr * dxi*dxi / kappa_el;
  dt_diffusive = PetscMin( dt_diffusive, Cr * deta*deta / kappa_el );
	
  *dta = dt_advective;
  *dtd = dt_diffusive;
	
  PetscFunctionReturn(0);
}
void ConstructQ12d_Ni_supg(PetscScalar Up[],PetscScalar kappa_hat,PetscScalar Ni[],PetscScalar GNx[NSD][NODES_PER_EL_Q1],PetscScalar Ni_supg[])
{
  PetscScalar uhat[2],unorm;
  PetscInt i;
	
  unorm = PetscSqrtScalar(Up[0]*Up[0] + Up[1]*Up[1]);
  uhat[0] = Up[0]/unorm;
  uhat[1] = Up[1]/unorm;
	
  if (kappa_hat < 1.0e-30) {
    for (i=0; i<NODES_PER_EL_Q1; i++) {
      Ni_supg[i] = Ni[i];
    }
  }else {
    for (i=0; i<NODES_PER_EL_Q1; i++) {
      Ni_supg[i] = Ni[i] + kappa_hat * ( uhat[0] * GNx[0][i] + uhat[1] * GNx[1][i] ) / unorm;
    }
  }
}

#undef __FUNCT__  
#define __FUNCT__ "DASUPG2dComputeTimestep"
PetscErrorCode DASUPG2dComputeTimestep(DM da,PetscScalar Cr,Vec kappa,Vec V,PetscScalar *DT)
{
  DM                     cda;
  Vec                    coords,local_kappa,local_V;
  DMDACoor2d             **LA_coords;
  PetscScalar            **LA_kappa, ***LA_V;
  PetscInt               sex,sey,mx,my;
  PetscInt               ei,ej,n;
  PetscScalar            el_coords[NODES_PER_EL_Q1*NSD],el_kappa[NODES_PER_EL_Q1],el_V[NODES_PER_EL_Q1*NSD];
  PetscScalar            dt_local,dt_global,dt_element;
  PetscScalar            el_V_avg_norm,el_V_avg[2],el_kappa_avg,el_Pe,dta,dtd,DX[2];
  PetscErrorCode         ierr;
	
  PetscFunctionBegin;
	
  /* setup for coords */
  ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
  ierr = DMDAGetGhostedCoordinates(da,&coords);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(cda,coords,&LA_coords);CHKERRQ(ierr);
	
  /* get acces to the vector kappa */
  ierr = DMGetLocalVector(da,&local_kappa);CHKERRQ(ierr);
  ierr = VecZeroEntries(local_kappa);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(da,kappa,INSERT_VALUES,local_kappa);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(  da,kappa,INSERT_VALUES,local_kappa);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da,local_kappa,&LA_kappa);CHKERRQ(ierr);
	
  /* get acces to the vector V */
  ierr = DMGetLocalVector(cda,&local_V);CHKERRQ(ierr);
  ierr = VecZeroEntries(local_V);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(cda,V,INSERT_VALUES,local_V);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(  cda,V,INSERT_VALUES,local_V);CHKERRQ(ierr);
  ierr = DMDAVecGetArrayDOF(cda,local_V,&LA_V);CHKERRQ(ierr);
	
  ierr = DMDAGetElementCorners_Q1(da,&sex,&sey,0,&mx,&my,0);CHKERRQ(ierr);
  dt_local = 1.0e308;
  for (ej = sey; ej < sey+my; ej++) {
    for (ei = sex; ei < sex+mx; ei++) {
      /* get coords for the element */
      GetElementCoords_Q1(LA_coords,ei,ej,el_coords);
      /* get coefficients for the element */
      DMDAGetElementValues_Q1_scalar(LA_kappa,ei,ej,el_kappa);
      DMDAGetElementValues_Q1_vector(LA_V,ei,ej,el_V);
			
      /* compute constants for the element */
      DX[0] = el_coords[NSD*3  ] - el_coords[NSD*0  ];
      DX[1] = el_coords[NSD*1+1] - el_coords[NSD*0+1]; 
			
			
      ierr = DASUPG2dComputeElementPecletNumber(DX,el_kappa,el_V,&el_Pe);CHKERRQ(ierr);
			ierr = DASUPG2dComputeElementTimestep(DX,el_kappa,el_V,&dta,&dtd);CHKERRQ(ierr);
			//    ierr = DASUPG2dComputeElementTimestep_exp(DX,el_kappa,el_V,&dta,&dtd);CHKERRQ(ierr);
			
      el_kappa_avg = el_V_avg[0] = el_V_avg[1] = 0.0;
      for (n=0; n<NODES_PER_EL_Q1; n++) {
        el_kappa_avg += el_kappa[n];
        el_V_avg[0]  += el_V[NSD*n];
        el_V_avg[1]  += el_V[NSD*n+1];
      }
      el_kappa_avg = el_kappa_avg / (PetscScalar)NODES_PER_EL_Q1;
      el_V_avg[0] = el_V_avg[0] / (PetscScalar)NODES_PER_EL_Q1;
      el_V_avg[1] = el_V_avg[1] / (PetscScalar)NODES_PER_EL_Q1;
      el_V_avg_norm = PetscSqrtScalar( el_V_avg[0]*el_V_avg[0] + el_V_avg[1]*el_V_avg[1] );
			/*
			 PetscPrintf(PETSC_COMM_WORLD,"Timestep diagnostics (%D,%D):\n",ei,ej);
			 PetscPrintf(PETSC_COMM_WORLD,"  el_[dx,dy]     : %e , %e \n",DX[0],DX[1]);
			 PetscPrintf(PETSC_COMM_WORLD,"  el_kappa_avg   : %e \n",el_kappa_avg);
			 PetscPrintf(PETSC_COMM_WORLD,"  el_||v_avg||   : %e \n",el_V_avg_norm);
			 PetscPrintf(PETSC_COMM_WORLD,"  el_Pe          : %e \n",el_Pe);
			 PetscPrintf(PETSC_COMM_WORLD,"  el_dt_advection: %e \n",dta);
			 PetscPrintf(PETSC_COMM_WORLD,"  el_dt_diffusion: %e \n",dtd);
			 */  
      dt_element = PetscMin(dta,dtd);
      dt_local = PetscMin(dt_local,dt_element);
    }
  }
	
  MPI_Allreduce(&dt_local,&dt_global,1,MPIU_REAL,MPI_MIN,((PetscObject)da)->comm);
  dt_global = dt_global * Cr;
  *DT = dt_global;
	/*
	 PetscPrintf(PETSC_COMM_WORLD,"Timestep diagnostics:\n");
	 PetscPrintf(PETSC_COMM_WORLD,"  el_dt_global: %e \n",dt_global);
	 */
  /* tidy up local arrays (input) */
  ierr = DMDAVecRestoreArray(   cda,coords,      &LA_coords  );CHKERRQ(ierr);
  ierr = DMDAVecRestoreArrayDOF(da, local_V,     &LA_V       );CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(   da, local_kappa, &LA_kappa   );CHKERRQ(ierr);
	
  /* tidy up local vecs (input) */
  ierr = DMRestoreLocalVector(cda,&local_V       );CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da, &local_kappa   );CHKERRQ(ierr);
	
  PetscFunctionReturn(0);
}

void AElement_SUPG2d( PetscScalar Re[],PetscReal dt,
														PetscScalar el_coords[],PetscScalar el_kappa[],PetscScalar el_V[])
{
  PetscInt    ngp;
  PetscScalar gp_xi[GAUSS_POINTS_2x2][2];
  PetscScalar gp_weight[GAUSS_POINTS_2x2];
  PetscInt    p,i,j;
  PetscScalar Ni_p[NODES_PER_EL_Q1],Ni_supg_p[NODES_PER_EL_Q1];
  PetscScalar GNi_p[NSD][NODES_PER_EL_Q1],GNx_p[NSD][NODES_PER_EL_Q1];
  PetscScalar J_p,fac;
  PetscScalar kappa_p,v_p[2];
  PetscScalar kappa_hat,DX[2];
	
  /* define quadrature rule */
  ConstructGaussQuadrature_2x2_2d(&ngp,gp_xi,gp_weight);
	
  /* compute constants for the element */
  DX[0] = el_coords[NSD*3  ] - el_coords[NSD*0  ];
  DX[1] = el_coords[NSD*1+1] - el_coords[NSD*0+1]; 
  DASUPG2dComputeElementStreamlineDiffusion(DX,el_kappa,el_V,&kappa_hat);
	
	
  /* evaluate integral */
  for (p = 0; p < ngp; p++) {
    ConstructQ12D_Ni(gp_xi[p],Ni_p);
    ConstructQ12D_GNi(gp_xi[p],GNi_p);
    ConstructQ12D_GNx(GNi_p,GNx_p,el_coords,&J_p);
		
    fac = gp_weight[p]*J_p;
		
    kappa_p      = 0.0;
    v_p[0]       = v_p[1]       = 0.0;
    for (j=0; j<NODES_PER_EL_Q1; j++) {
      kappa_p    += Ni_p[j] * el_kappa[j];
      v_p[0]     += Ni_p[j] * el_V[NSD*j+0];      /* compute vx on the particle */
      v_p[1]     += Ni_p[j] * el_V[NSD*j+1];      /* compute vy on the particle */
    }
    ConstructQ12d_Ni_supg(v_p,kappa_hat,Ni_p,GNx_p,Ni_supg_p);
		
    /* R = f - m phi_dot - c phi*/
		for (i=0; i<NODES_PER_EL_Q1; i++) {
			for (j=0; j<NODES_PER_EL_Q1; j++) {
				Re[j+i*NODES_PER_EL_Q1] += fac * ( 
																			 //a * Ni_p[i] * Ni_p[j]
//																			 + dt * kappa_p * ( GNx_p[0][i] * GNx_p[0][j] + GNx_p[1][i] * GNx_p[1][j] )
//																			 + dt * Ni_supg_p[i] * ( v_p[0] * GNx_p[0][j] + v_p[1] * GNx_p[1][j] )
																					+ THETA * dt * kappa_p * ( GNx_p[0][i] * GNx_p[0][j] + GNx_p[1][i] * GNx_p[1][j] )
																				  + THETA * dt * Ni_supg_p[i] * ( v_p[0] * GNx_p[0][j] + v_p[1] * GNx_p[1][j] )
																				+ Ni_supg_p[i] * Ni_p[j]
																			//		+ Ni_p[i] * Ni_p[j]
																			 );
			}
		}
  }
	/*
	 printf("e=\n");
	 for (i=0; i<NODES_PER_EL_Q1; i++) {
	 for (j=0; j<NODES_PER_EL_Q1; j++) {
	 printf("%lf ", Re[j+i*NODES_PER_EL_Q1]); 
	 }printf("\n");
	 }
	 */ 
}


/* 
 Computes M + dt.(L + A)
 */
#undef __FUNCT__  
#define __FUNCT__ "SUPGFormJacobian"
PetscErrorCode SUPGFormJacobian(PetscReal time,Vec X,PetscReal dt,Mat *A,Mat *B,MatStructure *mstr,void *ctx)
{
  DMDASUPGParameters data = (DMDASUPGParameters)ctx;
  DM             da,cda;
	Vec coords;
  DMDACoor2d             **LA_coords;
  PetscInt    p,i,j;
	PetscScalar ADe[NODES_PER_EL_Q1*NODES_PER_EL_Q1],el_coords[NSD*NODES_PER_EL_Q1],el_kappa[NODES_PER_EL_Q1],el_V[NODES_PER_EL_Q1*NSD];
	/**/
  PetscInt               sex,sey,mx,my;
  PetscInt               ei,ej;
	BCList bclist;
	PetscInt nxg,Len_local, *gidx_bclocal;
	PetscInt *gidx,elgidx[NODES_PER_EL_Q1],elgidx_bc[NODES_PER_EL_Q1];
	Vec                    kappa,V;
  Vec                    local_kappa,local_V;
  PetscScalar            **LA_kappa, ***LA_V;
	PetscErrorCode ierr;
	
	
  PetscFunctionBegin;
	da     = data->daT;
	kappa  = data->kappa;
	V      = data->V;
	bclist = data->phi_bclist;
	
	/* trash old entries */
  ierr = MatZeroEntries(*B);CHKERRQ(ierr);
	
  /* setup for coords */
  ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
  ierr = DMDAGetGhostedCoordinates(da,&coords);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(cda,coords,&LA_coords);CHKERRQ(ierr);
	
  /* get acces to the vector kappa */
  ierr = DMGetLocalVector(da,&local_kappa);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(da,kappa,INSERT_VALUES,local_kappa);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(  da,kappa,INSERT_VALUES,local_kappa);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da,local_kappa,&LA_kappa);CHKERRQ(ierr);
	
  /* get acces to the vector V */
  ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
  ierr = DMGetLocalVector(cda,&local_V);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(cda,V,INSERT_VALUES,local_V);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(  cda,V,INSERT_VALUES,local_V);CHKERRQ(ierr);
  ierr = DMDAVecGetArrayDOF(cda,local_V,&LA_V);CHKERRQ(ierr);
	
	
	/* stuff for eqnums */
	ierr = DMDAGetGlobalIndices(da,0,&gidx);CHKERRQ(ierr);
	ierr = BCListGetDofIdx(bclist,0,0,&Len_local,&gidx_bclocal);CHKERRQ(ierr);
	
  ierr = DMDAGetGhostCorners(da,0,0,0,&nxg,0,0);CHKERRQ(ierr);
  ierr = DMDAGetElementCorners_Q1(da,&sex,&sey,0,&mx,&my,0);CHKERRQ(ierr);
  for (ej = sey; ej < sey+my; ej++) {
    for (ei = sex; ei < sex+mx; ei++) {
      /* get coords for the element */
      GetElementCoords_Q1(LA_coords,ei,ej,el_coords);
      /* get coefficients for the element */
      DMDAGetElementValues_Q1_scalar(LA_kappa,ei,ej,el_kappa);
      DMDAGetElementValues_Q1_vector(LA_V,ei,ej,el_V);
			
      /* initialise element stiffness matrix */
      ierr = PetscMemzero(ADe,sizeof(PetscScalar)*NODES_PER_EL_Q1*NODES_PER_EL_Q1);CHKERRQ(ierr);
			
      /* form element stiffness matrix */
			AElement_SUPG2d( ADe,dt,
											 el_coords, el_kappa, el_V);
			
      /* insert element matrix into global matrix */
			ierr = GetElementEqnIndicesQ1_scalar(ei,ej,nxg, gidx,elgidx);CHKERRQ(ierr);
			ierr = GetElementEqnIndicesQ1_scalar(ei,ej,nxg, gidx_bclocal,elgidx_bc);CHKERRQ(ierr);
			
			ierr = MatSetValues(*B,NODES_PER_EL_Q1,elgidx_bc, NODES_PER_EL_Q1,elgidx_bc, ADe, ADD_VALUES );CHKERRQ(ierr);
		}
  }
	/* tidy up */
	ierr = DMDAVecRestoreArray(cda,coords,&LA_coords);CHKERRQ(ierr);
	
  ierr = DMDAVecRestoreArrayDOF(da, local_V,     &LA_V       );CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da, local_kappa, &LA_kappa   );CHKERRQ(ierr);
	
  ierr = DMRestoreLocalVector(cda,&local_V       );CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da, &local_kappa   );CHKERRQ(ierr);
	
	/* partial assembly */
	ierr = MatAssemblyBegin(*B, MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(*B, MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
	
	
	/* boundary conditions */
	{
		PetscInt Len_ghost,*gidx_bcghost;
		PetscBool is_dirich;
		PetscInt nproc,*procs,*mapL,**mapIdx;
		ISLocalToGlobalMapping map;
		PetscMPIInt rank;
		
		MPI_Comm_rank( ((PetscObject)da)->comm,&rank);
		ierr = BCListGetDofIdx(bclist,0,0,&Len_ghost,&gidx_bcghost);CHKERRQ(ierr);
		
		ierr = DMGetLocalToGlobalMapping(da,&map);CHKERRQ(ierr);
		ierr = ISLocalToGlobalMappingGetInfo(map,&nproc,&procs,&mapL,&mapIdx);CHKERRQ(ierr);
		
		if (Len_ghost!=mapL[rank]) {
			//			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"map indices are fucked up");
		}
		for (i=0; i<Len_ghost; i++) {
			ierr = BCListIsDirichlet(gidx_bcghost[i],&is_dirich);CHKERRQ(ierr);
			if (is_dirich) {
				ierr = MatSetValueLocal(*B,i,i,1.0,INSERT_VALUES);CHKERRQ(ierr);
			}
		}
	}
	
	/* assemble */
	ierr = MatAssemblyBegin(*B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(*B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  if (*A != *B) {
    ierr = MatAssemblyBegin(*A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(*A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }
	*mstr = SAME_NONZERO_PATTERN;
	
  PetscFunctionReturn(0);
}

void SUPG2d_FormMass_FormResidual_2(
																					 PetscScalar Re[],
																		       PetscReal dt,
																					 PetscScalar el_coords[],PetscScalar el_kappa[],PetscScalar el_V[],
																					 PetscScalar el_phi[],PetscScalar el_phi_last[])
{
  PetscInt    ngp;
  PetscScalar gp_xi[GAUSS_POINTS_2x2][2];
  PetscScalar gp_weight[GAUSS_POINTS_2x2];
  PetscInt    p,i,j;
  PetscScalar Ni_p[NODES_PER_EL_Q1],Ni_supg_p[NODES_PER_EL_Q1];
  PetscScalar GNi_p[NSD][NODES_PER_EL_Q1],GNx_p[NSD][NODES_PER_EL_Q1];
  PetscScalar J_p,fac;
  PetscScalar phi_p,phi_last_p,f_p,v_p[2],kappa_p, gradphi_p[2],gradphiold_p[2],M_dotT_p;
  PetscScalar kappa_hat,DX[2],JJ[2][2],invJJ[2][2];
	
  /* define quadrature rule */
  ConstructGaussQuadrature_2x2_2d(&ngp,gp_xi,gp_weight);
	
  /* compute constants for the element */
  DX[0] = el_coords[NSD*3  ] - el_coords[NSD*0  ];
  DX[1] = el_coords[NSD*1+1] - el_coords[NSD*0+1]; 
  DASUPG2dComputeElementStreamlineDiffusion(DX,el_kappa,el_V,&kappa_hat);
	
	
  /* evaluate integral */
  for (p = 0; p < ngp; p++) {
    ConstructQ12D_Ni(gp_xi[p],Ni_p);
    ConstructQ12D_GNi(gp_xi[p],GNi_p);
    ConstructQ12D_GNx(GNi_p,GNx_p,el_coords,&J_p);
		ConstructQ12D_ElementTransformation(el_coords,GNi_p,JJ,invJJ);
		
		
    fac = gp_weight[p]*J_p;
		
		f_p          = 0.0;
    phi_p        = 0.0;
    phi_last_p   = 0.0;
    kappa_p      = 0.0;
    gradphi_p[0] = gradphi_p[1] = 0.0;
    gradphiold_p[0] = gradphiold_p[1] = 0.0;
    v_p[0]       = v_p[1]       = 0.0;
    for (j=0; j<NODES_PER_EL_Q1; j++) {
      phi_p        += Ni_p[j] * el_phi[j];      /* compute phi on the particle */
      phi_last_p   += Ni_p[j] * el_phi_last[j];  /* compute phi_dot on the particle */
      gradphiold_p[0] += GNx_p[0][j] * el_phi_last[j];
      gradphiold_p[1] += GNx_p[1][j] * el_phi_last[j];

      gradphi_p[0] += GNx_p[0][j] * el_phi[j];
      gradphi_p[1] += GNx_p[1][j] * el_phi[j];
      kappa_p      += Ni_p[j] * el_kappa[j];
			
      v_p[0]     += Ni_p[j] * el_V[NSD*j+0];      /* compute vx on the particle */
      v_p[1]     += Ni_p[j] * el_V[NSD*j+1];      /* compute vy on the particle */
    }
    ConstructQ12d_Ni_supg(v_p,kappa_hat,Ni_p,GNx_p,Ni_supg_p);
		
		M_dotT_p = 0.0;
		for (j=0; j<NODES_PER_EL_Q1; j++) {
			M_dotT_p += Ni_supg_p[j] * el_phi_last[j];
		}
		
    /* R = f - m phi_dot - c phi*/
    for (i = 0; i < NODES_PER_EL_Q1; i++) {
      Re[ i ] += fac * (
												// -f - M T^k
												- dt * Ni_p[i] * f_p 
												// (C+L) T^k+1
//                        + dt * kappa_p * ( GNx_p[0][i] * gradphi_p[0] + GNx_p[1][i] * gradphi_p[1] )  /* - C_diffusive . phi */
//                        + dt * Ni_supg_p[i] * ( v_p[0] * gradphi_p[0] + v_p[1] * gradphi_p[1] )       /* - C_advective . phi */
												+ (THETA)*dt * kappa_p * ( GNx_p[0][i] * gradphi_p[0] + GNx_p[1][i] * gradphi_p[1] )
												+ (THETA)*dt * Ni_supg_p[i] * ( v_p[0] * gradphi_p[0] + v_p[1] * gradphi_p[1] ) 
												+ (1.0-THETA)*dt * kappa_p * ( GNx_p[0][i] * gradphiold_p[0] + GNx_p[1][i] * gradphiold_p[1] )
												+ (1.0-THETA)*dt * Ni_supg_p[i] * ( v_p[0] * gradphiold_p[0] + v_p[1] * gradphiold_p[1] ) 
												// M T^k+1
                        + Ni_supg_p[i] * phi_p 
                        - Ni_supg_p[i] * phi_last_p 
                        //+ Ni_p[i] * phi_p 
                        //- Ni_p[i] * phi_last_p 
												);
    }
  }
	
}

#undef __FUNCT__  
#define __FUNCT__ "FormFunction_SUPG_T"
PetscErrorCode FormFunction_SUPG_T(
																					DMDASUPGParameters data,PetscReal dt,
																					DM da,PetscScalar ***LA_V,PetscScalar **LA_kappa,
																					PetscScalar **LA_phi,PetscScalar **LA_philast,PetscScalar **LA_R)
{
	Vec kappa,V;
  DM                     cda;
  Vec                    coords;
  DMDACoor2d             **LA_coords;
  MatStencil             phi_eqn[NODES_PER_EL_Q1];
  PetscInt               sex,sey,mx,my;
  PetscInt               ei,ej;
  PetscScalar            Re[NODES_PER_EL_Q1];
  PetscScalar            el_coords[NODES_PER_EL_Q1*NSD],el_kappa[NODES_PER_EL_Q1],el_V[NODES_PER_EL_Q1*NSD],el_phi[NODES_PER_EL_Q1],el_philast[NODES_PER_EL_Q1];
	
  PetscInt    ngp;
  PetscScalar gp_xi[GAUSS_POINTS_2x2][2];
  PetscScalar gp_weight[GAUSS_POINTS_2x2];
  PetscScalar Ni_p[NODES_PER_EL_Q1];
  PetscScalar GNi_p[NSD][NODES_PER_EL_Q1],GNx_p[NSD][NODES_PER_EL_Q1];
  PetscScalar J_p,fac;
  PetscScalar phi_p;
  PetscInt    p,i;
	PetscReal   cg,c = 0.0;
  PetscErrorCode         ierr;
	
  PetscFunctionBegin;
	
  ConstructGaussQuadrature_2x2_2d(&ngp,gp_xi,gp_weight);
	
  /* setup for coords */
  ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
  ierr = DMDAGetGhostedCoordinates(da,&coords);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(cda,coords,&LA_coords);CHKERRQ(ierr);
	
  ierr = DMDAGetElementCorners_Q1(da,&sex,&sey,0,&mx,&my,0);CHKERRQ(ierr);
  for (ej = sey; ej < sey+my; ej++) {
    for (ei = sex; ei < sex+mx; ei++) {
      /* get coords for the element */
      GetElementCoords_Q1(LA_coords,ei,ej,el_coords);
      /* get coefficients for the element */
      DMDAGetElementValues_Q1_scalar(LA_kappa,ei,ej,el_kappa);
      DMDAGetElementValues_Q1_vector(LA_V,ei,ej,el_V);
      /* get phi */
      DMDAGetElementValues_Q1_scalar(LA_phi,ei,ej,el_phi);
      DMDAGetElementValues_Q1_scalar(LA_philast,ei,ej,el_philast);
			
      /* initialise element stiffness matrix */
      ierr = PetscMemzero(Re,sizeof(PetscScalar)*NODES_PER_EL_Q1);CHKERRQ(ierr);
			
      /* form element stiffness matrix */
      SUPG2d_FormMass_FormResidual_2(Re,dt,el_coords,el_kappa,el_V,el_phi,el_philast);
			
			for (p=0; p<ngp; p++) {
				ConstructQ12D_Ni(gp_xi[p],Ni_p);
				ConstructQ12D_GNi(gp_xi[p],GNi_p);
				ConstructQ12D_GNx(GNi_p,GNx_p,el_coords,&J_p);
				
				phi_p = 0.0;
				for (i=0; i<NODES_PER_EL_Q1; i++) {
					phi_p = phi_p + Ni_p[i] * el_phi[i];
				}
				
				fac = gp_weight[p]*J_p;
				c = c + phi_p * fac;
				
			}
			
      /* insert element matrix into global matrix */
      ierr = DMDAGetElementEqnums_Q1_scalar(ei,ej,phi_eqn);CHKERRQ(ierr);
			
      ierr = DMDASetValuesLocalStencil_Q1_ADD_VALUES(LA_R,phi_eqn,Re);CHKERRQ(ierr);
    }
  }
  MPI_Allreduce(&c,&cg,1,MPIU_REAL,MPI_MIN,((PetscObject)da)->comm);
	PetscPrintf(PETSC_COMM_WORLD,"\\int \\phi dV = %1.12e \n", cg );
	
  /* tidy up local arrays (input) */
  ierr = DMDAVecRestoreArray(cda,coords,      &LA_coords  );CHKERRQ(ierr);
	
	
  PetscFunctionReturn(0);
}

/*
 Computes f - C.phi - M.phiDot
 */
#undef __FUNCT__  
#define __FUNCT__ "SUPGFormFunction"
PetscErrorCode SUPGFormFunction(PetscReal time,Vec X,PetscReal dt,Vec F,void *ctx)
{
  DMDASUPGParameters data  = (DMDASUPGParameters)ctx;
  DM             da,cda;
	Vec philoc, philastloc, Fphiloc;
	Vec Vloc,kappaloc;
	PetscScalar **LA_philoc, **LA_philastloc, **LA_Fphiloc;
	PetscScalar ***LA_V,**LA_kappa;
  PetscErrorCode ierr;
	
  PetscFunctionBegin;
	da = data->daT;
	
	
	ierr = VecZeroEntries(F);CHKERRQ(ierr);
	
  ierr = DMGetLocalVector(da,&philoc);CHKERRQ(ierr);
  ierr = DMGetLocalVector(da,&philastloc);CHKERRQ(ierr);
	
  ierr = DMGetLocalVector(da,&Fphiloc);CHKERRQ(ierr);
	
	/* get local solution and time derivative */
	ierr = VecZeroEntries(philoc);CHKERRQ(ierr);
	ierr = DMGlobalToLocalBegin(da,X,ADD_VALUES,philoc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd  (da,X,ADD_VALUES,philoc);CHKERRQ(ierr);
	
	ierr = VecZeroEntries(philastloc);CHKERRQ(ierr);
	ierr = DMGlobalToLocalBegin(da,data->phi_last,ADD_VALUES,philastloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd  (da,data->phi_last,ADD_VALUES,philastloc);CHKERRQ(ierr);
	
	/* insert boundary conditions into local vectors */
	ierr = BCListInsertLocal(data->phi_bclist,philoc);CHKERRQ(ierr);
	
	/* what do i do with the phi_dot */
	
	/* init residual */
	ierr = VecZeroEntries(Fphiloc);CHKERRQ(ierr);
	
	/* get arrays */
	ierr = DMDAVecGetArray(da,philoc,    &LA_philoc);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(da,philastloc,&LA_philastloc);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(da,Fphiloc,   &LA_Fphiloc);CHKERRQ(ierr);
	
	/* ============= */
	/* FORM_FUNCTION */
  /* get acces to the vector kappa */
  ierr = DMGetLocalVector(da,&kappaloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(da,data->kappa,INSERT_VALUES,kappaloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(  da,data->kappa,INSERT_VALUES,kappaloc);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da,kappaloc,&LA_kappa);CHKERRQ(ierr);
	
  /* get acces to the vector V */
  ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
  ierr = DMGetLocalVector(cda,&Vloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(cda,data->V,INSERT_VALUES,Vloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(  cda,data->V,INSERT_VALUES,Vloc);CHKERRQ(ierr);
  ierr = DMDAVecGetArrayDOF(cda,Vloc,&LA_V);CHKERRQ(ierr);
	
	ierr = FormFunction_SUPG_T(data,dt,da,LA_V,LA_kappa, LA_philoc,LA_philastloc,LA_Fphiloc);CHKERRQ(ierr);
	
  ierr = DMDAVecRestoreArrayDOF(cda,Vloc,&LA_V);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da,kappaloc,&LA_kappa);CHKERRQ(ierr);
	
  ierr = DMRestoreLocalVector(cda,&Vloc);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da,&kappaloc);CHKERRQ(ierr);
	/* ============= */
	
	ierr = DMDAVecRestoreArray(da,Fphiloc,   &LA_Fphiloc);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(da,philastloc,&LA_philastloc);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(da,philoc,    &LA_philoc);CHKERRQ(ierr);
	
	
	/* do global fem summation */
	ierr = VecZeroEntries(F);CHKERRQ(ierr);
	
  ierr = DMLocalToGlobalBegin(da,Fphiloc,ADD_VALUES,F);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd  (da,Fphiloc,ADD_VALUES,F);CHKERRQ(ierr);
	
  ierr = DMRestoreLocalVector(da,&Fphiloc);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da,&philoc);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da,&philastloc);CHKERRQ(ierr);
	
	/* modify F for the boundary conditions, F_k = scale_k(x_k - phi_k) */
	ierr = BCListResidualDirichlet(data->phi_bclist,X,F);CHKERRQ(ierr);
	
  PetscFunctionReturn(0);
}

