/*
  
 
 Input / command line parameters:
 -Ttop
 -Tbot 
 -mx
 -my
 -vx -3.000000e-10
 
-ptatin_model julia
-output_frequency 1
-output_path julia
-remesh_type 2
-store_ages
-stokes_scale_length 1.000000e+04
-stokes_scale_velocity 1.000000e-10
-stokes_scale_viscosity 1.000000e+27
-dt_max 3e11
-nsteps 500
-time_max 2.000000e+15
 

 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"

#define WRITE_VP
#define WRITE_TFIELD
#define WRITE_MPOINTS
//#define WRITE_QPOINTS
#define WRITE_TCELL
//#define WRITE_INT_FIELDS
//#define WRITE_FACEQPOINTS


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_julia"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_julia(pTatinCtx ctx)
{
//	PhaseMap phasemap; //use a phase map
//	TopoMap topomap;   //use initial topogrpahy
	PetscInt M,N;      /* number of elments in x and y */
	PetscErrorCode ierr;
	PetscReal Ox,Oy,Lx,Ly,gravity_acceleration;
	PetscScalar OOx[21],OOy[21];
	PetscInt Nx[20], Ny[20],test,ndomainx,ndomainy;
    PetscInt nlimx,nlimy, i,j;
    PhysCompEnergyCtx phys_energy;
    DM daT;
	pTatinUnits *units;
	PetscFunctionBegin;
	units = &ctx->units;
	PetscBool              notuniform_mesh=PETSC_FALSE;
    
	/* set gravity */
    gravity_acceleration = 9.81;
    ierr = QuadratureStokesGravityModelCoordinateAlignedSetValues(ctx->Q,0.0, -gravity_acceleration);CHKERRQ(ierr);
    /* set model size*/
    Ox = 0.0;
    Oy = -250.e3;
    Lx = 1000.e3;
    Ly = 0.0;
	ierr  = PetscOptionsGetReal(PETSC_NULL,"-Ox",&Ox,0);CHKERRQ(ierr);
	ierr  = PetscOptionsGetReal(PETSC_NULL,"-Oy",&Oy,0);CHKERRQ(ierr);
	ierr  = PetscOptionsGetReal(PETSC_NULL,"-Lx",&Lx,0);CHKERRQ(ierr);
	ierr  = PetscOptionsGetReal(PETSC_NULL,"-Ly",&Ly,0);CHKERRQ(ierr);
	PetscPrintf(PETSC_COMM_WORLD,"ApplyInitialMeshGeometry: options Domain_SI = [ %1.4e , %1.4e ] x [ %1.4e , %1.4e ] \n", Ox, Lx,Oy,Ly );	
	UnitsApplyInverseScaling(units->si_length,Ox,&Ox);
	UnitsApplyInverseScaling(units->si_length,Oy,&Oy);
	UnitsApplyInverseScaling(units->si_length,Lx,&Lx);
	UnitsApplyInverseScaling(units->si_length,Ly,&Ly);
	PetscPrintf(PETSC_COMM_WORLD,"ApplyInitialMeshGeometry : options Domain_ND = [ %1.4e , %1.4e ] x [ %1.4e , %1.4e ] \n", Ox, Lx,Oy,Ly );
    /* create 2D mesh*/
	ierr = DMDASetUniformCoordinates(ctx->dav,Ox,Lx,Oy,Ly, 0.0,0.0);CHKERRQ(ierr);
	
	/* if activated (-not_uniform_mesh 1) this option will create the mesh described below */
	ierr = PetscOptionsGetBool(PETSC_NULL,"-not_uniform_mesh",&notuniform_mesh,0);CHKERRQ(ierr);
	if (notuniform_mesh){
		PetscPrintf(PETSC_COMM_WORLD,"Not Uniform mesh has been activated\n");
	/* PetscOptionsSetValue("-refine_n_domain_x","5");
    PetscOptionsSetValue("-refine_n_domain_y","3");
    PetscOptionsSetValue("-refine_x_dom_el","10,3,250,3,10");
    PetscOptionsSetValue("-refine_y_dom_el","9,30,20");
    PetscOptionsSetValue("-refine_x_lim","0.0,3.0,3.5,12.5,13.0,16.0");
    PetscOptionsSetValue("-refine_y_lim","-3.0,-1.2,-0.2,0.0");
	*/
    
		PetscOptionsGetInt(PETSC_NULL,"-refine_n_domain_x",&ndomainx,0);
		PetscOptionsGetInt(PETSC_NULL,"-refine_n_domain_y",&ndomainy,0);
	
		nlimx=ndomainx+1;
		nlimy=ndomainy+1;
		PetscOptionsGetRealArray(PETSC_NULL,"-refine_x_lim",OOx,&nlimx,PETSC_NULL);
		PetscOptionsGetRealArray(PETSC_NULL,"-refine_y_lim",OOy,&nlimy,PETSC_NULL);
    
		PetscOptionsGetIntArray(PETSC_NULL,"-refine_x_dom_el",Nx,&ndomainx,PETSC_NULL);
		PetscOptionsGetIntArray(PETSC_NULL,"-refine_y_dom_el",Ny,&ndomainy,PETSC_NULL);
    
		test=0;
		for (i=0;i<ndomainx;i++) { test = test+ Nx[i];}
		if (test != ctx->mx) {SETERRQ2(PETSC_COMM_WORLD,PETSC_ERR_SUP,"mx = %d is not equal to the number of element = %d in the refined mesh", ctx->mx, test);}
		test=0;
		for (j=0;j<ndomainy;j++) { test += Ny[j];}
		if (test != ctx->my) {SETERRQ2(PETSC_COMM_WORLD,PETSC_ERR_SUP,"my = %d is not equal to the number of element = %d in the refined mesh", ctx->my, test);}
		if (nlimx > 20 | nlimy >20) {SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"maximum refinement domain is set to 20 in the current version of the code");}

		ierr = DMDASetRefinedCoordinates(ctx->dav); CHKERRQ(ierr);
	}
	
	/* create 2D mesh for temperature aligned on the veocity mesh*/
    phys_energy = ctx->phys_energy;
    daT         = phys_energy->daT;
	ierr = DMDAProjectCoordinatesQ2toQ1_2d(ctx->dav,daT);CHKERRQ(ierr);
    
	PetscFunctionReturn(0);
}



#undef __FUNCT__ 
#define __FUNCT__ "pTatin2d_ModelSetMaterialProperty_julia"
PetscErrorCode pTatin2d_ModelSetMaterialProperty_julia(pTatinCtx ctx)

{
	RheologyConstants      *rheology;
	PetscInt               nphase, i, viscous_type,melt;
	PetscErrorCode         ierr;
	char   		       *option_name;
	PetscBool              found;
	PetscBool              phase_change=PETSC_FALSE;	
	char file[PETSC_MAX_PATH_LEN];
	char name[PETSC_MAX_PATH_LEN],mapname[PETSC_MAX_PATH_LEN];
	pTatinUnits *units;
	TempMap map;
        PetscScalar ps_min_cut_off,ps_max_cut_off;

	
	PetscFunctionBegin;
	
	
    ctx->rheology_constants.rheology_type = 6;// use stockes vpt standard
	nphase = 11;    /* or marker index  */
    
    rheology   = &ctx->rheology_constants;
    rheology->eta_upper_cutoff_global = 1.e25;
	  rheology->eta_lower_cutoff_global = 1.e19;
	  ierr       = PetscOptionsGetReal(PETSC_NULL,"-eta_max_cut_off",&rheology->eta_upper_cutoff_global,0);CHKERRQ(ierr);
	  ierr       = PetscOptionsGetReal(PETSC_NULL,"-eta_min_cut_off",&rheology->eta_lower_cutoff_global,0);CHKERRQ(ierr);
	PetscPrintf(PETSC_COMM_WORLD,"ApplyViscouscuttoff: eta min =  %1.2e , eta_max =  %1.2e \n", rheology->eta_lower_cutoff_global, rheology->eta_upper_cutoff_global);

ierr       = PetscOptionsGetBool(PETSC_NULL,"-store_melt",&rheology->store_melt,0);CHKERRQ(ierr);
       ps_min_cut_off = 0.5; 
       ps_max_cut_off = 1.5;
       ierr       = PetscOptionsGetReal(PETSC_NULL,"-ps_min_cut_off",&ps_min_cut_off,0);CHKERRQ(ierr);
       ierr       = PetscOptionsGetReal(PETSC_NULL,"-ps_max_cut_off",&ps_max_cut_off,0);CHKERRQ(ierr);
    
	
     for (i=0;i<nphase; i++){
	  rheology->temp_kappa[i]      = 1e-6;
	  rheology->temp_prod[i]       = 0.;
    rheology->density_type[i]    = 1;
    rheology->temp_alpha[i]      = 3e-5;
    rheology->temp_beta[i]       = 1e-11;
    rheology->viscous_type[i]    = 2;
    rheology->arrh_Tref[i]       = 273;
    rheology->plastic_type[i]    = 2;
    rheology->melt_type[i]       = 2;
    rheology->tens_cutoff[i]     = 1.e6;
    rheology->softening_type[i]  = 1;
    rheology->Hst_cutoff[i]      = 4.e8;
    rheology->soft_min_strain_cutoff[i] = ps_min_cut_off ;
    rheology->soft_max_strain_cutoff[i] = ps_max_cut_off ;
    rheology->melt_eta[i] = 1e18;
    }
   
    

    
    
    /*4 arabic lower crust maryland diabiase strong Mackwell 1998 */
    rheology->const_rho0[4]	     =2700.; 	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
    rheology->arrh_nexp[4]       = 3;
    rheology->arrh_entalpy[4]    = 3.56e5;
    rheology->arrh_preexpA[4]    = 4.0e2;
    rheology->arrh_Ascale[4]     = 1.e6;
    rheology->arrh_Vmol[4]       = 0.0;
    rheology->mises_tau_yield[4] = 2.e7;
    rheology->dp_pressure_dependance[4]=30.;
    rheology->softening_type[4]=1;
    rheology->soft_Co_inf[4] = 2.e7;
    rheology->soft_phi_inf[4] = 5.;
    rheology->melt_rho[4] = 2000;
    rheology->meltP_type[4]=1;
    
   /* 5 iranian lower crust  Rybacki and Dresen 2000 for Wet anorthite  */

  rheology->const_rho0[5]      = 2800.;
    rheology->arrh_nexp[5]       = 4.7;
    rheology->arrh_entalpy[5]    = 2.23e5;
    rheology->arrh_preexpA[5]    = 1.1e-4;
    rheology->arrh_Ascale[5]     = 1.e6;
    rheology->arrh_Vmol[5]       = 0.0;
    rheology->mises_tau_yield[5] = 2.e7;
    rheology->dp_pressure_dependance[5]=30.;
    rheology->softening_type[5]=1;
    rheology->soft_Co_inf[5] = 2.e7;
    rheology->soft_phi_inf[5] = 5.;
     rheology->melt_rho[5] = 2000;
    rheology->meltP_type[5]=1;    
   
    
    /* 1 arabian lithos mantle Dry olivine, H&K */
    
    rheology->const_rho0[1]   = 3300.;
    rheology->arrh_nexp[1]    = 3.4;
    rheology->arrh_entalpy[1] = 510.e3;
    rheology->arrh_preexpA[1] = 7.0e3;
    rheology->arrh_Ascale[1]  = 1.e6; /* si A[MPA-n] Ascale=1.e6 else if A[Pa-n] then A=1. */
    rheology->arrh_Vmol[1]    = 8.0e-6;
    rheology->mises_tau_yield[1] = 2.e7; /* Cohesion initial */
    rheology->dp_pressure_dependance[1]=30.; /* Phi initial */
    rheology->tens_cutoff[1] =1.e7;
    rheology->Hst_cutoff[1] = 4.e8;
    rheology->softening_type[1]=1;
    rheology->soft_Co_inf[1] = 2.e7;
    rheology->soft_phi_inf[1] = 5.;
    rheology->melt_rho[1]     = 2200;
    rheology->meltP_type[1]   = 0;
    
    /* 0 deep mantle  Dry olivine, H&K */
    
    rheology->const_rho0[0]   = 3300.;
    rheology->arrh_nexp[0]    = 3.5;
    rheology->arrh_entalpy[0] = 510.e3;
    rheology->arrh_preexpA[0] = 7.0e3;
    rheology->arrh_Ascale[0]  = 1.e6; /* si A[MPA-n] Ascale=1.e6 else if A[Pa-n] then A=1. */
    rheology->arrh_Vmol[0]    = 8.0e-6;
    rheology->mises_tau_yield[0] = 2.e7; /* Cohesion initial */
    rheology->dp_pressure_dependance[0]=30.; /* Phi initial */
    rheology->tens_cutoff[0] =1.e7;
    rheology->Hst_cutoff[0] = 4.e8;
    rheology->softening_type[0]=1;
    rheology->soft_Co_inf[0] = 2.e7;
    rheology->soft_phi_inf[0] = 5.;
    rheology->melt_rho[0]     = 2200;
    rheology->meltP_type[0]   = 0;
 
    
        /* 10 sediments   */

        rheology->const_rho0[10]   = 2200.;
        rheology->arrh_nexp[10]    = 4.;
        rheology->arrh_entalpy[10] = 2.23e5;
        rheology->arrh_preexpA[10] = 1.1e-4;
        rheology->arrh_Ascale[10]  = 1.e6; /* si A[MPA-n] Ascale=1.e6 else if A[Pa-n] then A=1. */
        rheology->arrh_Vmol[10]    = 0.0;
        rheology->arrh_Tref[10]    = 273; /* celcius 273 Kelvin 0 */
        rheology->mises_tau_yield[10] = 2.e7;
        rheology->dp_pressure_dependance[10]=5.;
        rheology->soft_Co_inf[10] = 2.e7;
        rheology->soft_phi_inf[10] = 5.;
        rheology->temp_prod[10]              = 1e-12;
        rheology->melt_type[10]       = 0;
	
	PetscFunctionReturn(0);
}







#undef __FUNCT__
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_julia"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_julia(pTatinCtx ctx)
{
    PetscErrorCode         ierr;
    
    pTatinUnits *units;
    
    PetscFunctionBegin;
    units   = &ctx->units;
    UnitsApplyInverseScaling(units->si_time,ctx->dt_min,&ctx->dt_min);
    UnitsApplyInverseScaling(units->si_time,ctx->dt,&ctx->dt);
    UnitsApplyInverseScaling(units->si_time,ctx->dt_adv,&ctx->dt_adv);
    UnitsApplyInverseScaling(units->si_time,ctx->dt_max,&ctx->dt_max);
    UnitsApplyInverseScaling(units->si_time,ctx->time_max,&ctx->time_max);
    UnitsApplyInverseScaling(units->si_length,ctx->max_disp_y,&ctx->max_disp_y);
    
    ierr = pTatin2d_ModelSetMaterialProperty_julia(ctx); CHKERRQ(ierr);
    ierr = pTatin2d_ModelSetMarker_julia(ctx); CHKERRQ(ierr);
    ierr= pTatin2d_ModelSetInitialTemperatureOnNodes_julia(ctx); CHKERRQ(ierr);
    ierr= pTatin2d_ModelSetSPMParameters_GENE(ctx); CHKERRQ(ierr);
   
    
    PetscFunctionReturn(0);
}



#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetMarker_julia"
PetscErrorCode pTatin2d_ModelSetMarker_julia(pTatinCtx ctx)
{

	PetscInt               p,n_mp_points;
  PetscInt               crust_phase[2]={4,4};
  PetscInt               crust_n_layer=2; 
	DataBucket             db;
    DataField            PField_std, PField_stokes,PField_stokespl, PField_thermal, PField_chrono;
	int                    phase;
	PetscScalar            ymoho,yconrad,yc_moy,y_salt,ylab, y_prod, h_prod, eps_rand, eps_max, eps_init, mu, sigma, sigma2_1, eta_ND,rho_ND,kappa_ND,prod_ND,e_croute,mod;
	DM da; 
	PetscErrorCode ierr;
    RheologyConstants      *rheology;
	pTatinUnits *units;
	PetscBool              noise_strain=PETSC_FALSE;

	
	PetscFunctionBegin;
    rheology   = &ctx->rheology_constants;
	units=&ctx->units;
	da=ctx->dav;
	
	ymoho   = -35.0e3; // SI units
	yconrad = -20.0e3; // SI units
	yc_moy  = -10.0e3; // SI units
  ylab    = -120.0e3; // SI units
	e_croute = 2.0e3;
    ierr    = PetscOptionsGetReal(PETSC_NULL,"-conrad",&yconrad,0);CHKERRQ(ierr);
    ierr    = PetscOptionsGetReal(PETSC_NULL,"-moho",&ymoho,0);CHKERRQ(ierr);
	  ierr    = PetscOptionsGetReal(PETSC_NULL,"-c_moy",&yc_moy,0);CHKERRQ(ierr);
    ierr    = PetscOptionsGetReal(PETSC_NULL,"-lab",&ylab,0);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_WORLD,"Initial layering : Conrad =  %1.2e , Moho =  %1.2e , LAB =  %1.2e \n", yconrad, ymoho, ylab);
    UnitsApplyInverseScaling(units->si_length,yconrad,&yconrad);
	  UnitsApplyInverseScaling(units->si_length,ymoho,&ymoho);
	  UnitsApplyInverseScaling(units->si_length,yc_moy ,&yc_moy);
	  UnitsApplyInverseScaling(units->si_length,ylab ,&ylab);
UnitsApplyInverseScaling(units->si_length,e_croute ,&e_croute);    
    h_prod   =  1.e-12; // SI units
    y_prod   = -10.0e3; // SI units
    ierr = PetscOptionsGetScalar(PETSC_NULL,"-h_prod",&h_prod,PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsGetScalar(PETSC_NULL,"-y_prod",&y_prod,PETSC_NULL);CHKERRQ(ierr);
	PetscPrintf(PETSC_COMM_WORLD,"Initial layering : y_prod =  %1.2e , h_prod =  %1.2e \n", y_prod, h_prod);
    UnitsApplyInverseScaling(units->si_length,y_prod,&y_prod);
    UnitsApplyInverseScaling(units->si_heatsource,h_prod,&h_prod);
    
    eps_max = 1.0;
    ierr = PetscOptionsGetScalar(PETSC_NULL,"-eps_max",&eps_max,PETSC_NULL);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_WORLD,"Initial plastic strain noise amplitude =  %1.2e  \n", eps_max);
	
	sigma   = 250.e+3; // SI units
	mu      = 500.e+3; // SI units
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-noise_sigma",&sigma,PETSC_NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-noise_mu",&mu,PETSC_NULL);CHKERRQ(ierr);
	PetscPrintf(PETSC_COMM_WORLD,"Initial plastic strain noise Gaussian, sigma =  %1.2e mu = %1.2e \n", sigma,mu);
	UnitsApplyInverseScaling(units->si_length,sigma,&sigma);
   UnitsApplyInverseScaling(units->si_length,mu,&mu);
	
	/* define properties on material points */
	db = ctx->db;

    DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	  DataFieldGetAccess(PField_std);
	  DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
    DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
    DataFieldGetAccess(PField_stokes);
    DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));

    DataBucketGetDataFieldByName(db,MPntPStokesPl_classname,&PField_stokespl);
    DataFieldGetAccess(PField_stokespl);
    DataFieldVerifyAccess(PField_stokespl,sizeof(MPntPStokesPl));
    
    DataBucketGetDataFieldByName(db,MPntPThermal_classname,&PField_thermal);
    DataFieldGetAccess(PField_thermal);
    DataFieldVerifyAccess(PField_thermal,sizeof(MPntPThermal));
    
    DataBucketGetDataFieldByName(db,MPntPChrono_classname,&PField_chrono);
    DataFieldGetAccess(PField_chrono);
    DataFieldVerifyAccess(PField_chrono,sizeof(MPntPChrono));
    PetscOptionsGetIntArray(PETSC_NULL,"-crust_type",crust_phase,&crust_n_layer,PETSC_NULL);
    
    DataBucketGetSizes(db,&n_mp_points,0,0);
	  for (p=0; p<n_mp_points; p++) {
		MPntStd       *material_point; // pos phase index
        MPntPStokes   *mpprop_stokes; // eta rho
		    double        *position;
        MPntPChrono   *mpprop_chrono; /* ages variables */
        MPntPThermal  *material_point_thermal;
        MPntPStokesPl *mpprop_stokespl;
		    DataFieldAccessPoint(PField_std,p,(void**)&material_point);		
        MPntStdGetField_global_coord(material_point,&position);
		/*set phase index*/
        if (position[1] < ylab){
            phase = 0;
		      } else {
            if (position[1] < ymoho){
                phase = 1;
            } else {

							     if (fmod(fabs(position[1]),2*e_croute)>=e_croute){
												phase = crust_phase[0];
											} else {
														phase = crust_phase[1];
										  } 
           }  
         }


#if 0
{
PetscBool			croute_Q=PETSC_FALSE;
PetscBool			croute_FQ=PETSC_FALSE;
PetscBool			croute_F=PETSC_FALSE;

ierr=PetscOptionsGetBool(Petsc_NULL,"-croute_Q",&croute_Q,0);CHKERRQ(ierr);
ierr=PetscOptionsGetBool(Petsc_NULL,"-croute_FQ",&croute_FQ,0);CHKERRQ(ierr);
ierr=PetscOptionsGetBool(Petsc_NULL,"-croute_F",&croute_F,0);CHKERRQ(ierr);

if (position[1] < ylab){
            phase = 0;
		      } else {
            if (position[1] < ymoho){
                phase = 1;
            } else {
								if (croute_Q){
										PetscPrintf(PETSC_COMM_WORLD,"Croute full quartz activated\n");
										if (fmod(fabs(position[1]),2*e_croute)>=e_croute){
												phase=4;
												}else{
													phase=4;
												}
								}else{
								if (croute_FQ){
										PetscPrintf(PETSC_COMM_WORLD,"Croute quartz-feldspath activated\n");
										if (fmod(fabs(position[1]),2*e_croute)>=e_croute){
												phase=4;
												}else{
													phase=5;
												}
								}else{
								if (croute_F){
										PetscPrintf(PETSC_COMM_WORLD,"Croute full feldspath activated\n");
										if (fmod(fabs(position[1]),2*e_croute)>=e_croute){
												phase=5;
												}else{
													phase=5;
												}
								}else{
								if (fmod(fabs(position[1]),2*e_croute)>=e_croute){
												phase=5;
												}else{
													phase=5;
												}
								}

}
#endif

        MPntStdSetField_phase_index(material_point,phase);
        /* set eta and rho */
        DataFieldAccessPoint(PField_stokes,p,(void**)&mpprop_stokes);
        UnitsApplyInverseScaling(units->si_viscosity,1.e+21,&eta_ND);
        UnitsApplyInverseScaling(units->si_force_per_volume,rheology->const_rho0[phase],&rho_ND);
        MPntPStokesSetField_eta_effective(mpprop_stokes,eta_ND);
        MPntPStokesSetField_density(mpprop_stokes,rho_ND);
		
        /* set kappa and Hprod */
        DataFieldAccessPoint(PField_thermal,p,(void**)&material_point_thermal);
        UnitsApplyInverseScaling(units->si_diffusivity,rheology->temp_kappa[phase],&kappa_ND);
        MPntPThermalSetField_diffusivity(material_point_thermal,kappa_ND);
        prod_ND = h_prod*exp(-position[1]/y_prod);
        MPntPThermalSetField_heat_prod(material_point_thermal,prod_ND);
		
        /* set initial plastic strain */
        DataFieldAccessPoint(PField_stokespl,p,(void**)&mpprop_stokespl);
		
		ierr = PetscOptionsGetBool(PETSC_NULL,"-noise_strain",&noise_strain,0);CHKERRQ(ierr);
		if(noise_strain){
		/* Gaussian repartition centered on mu with an amplitude sigma*/
		sigma2_1 = -1.0/(2*pow(sigma,2));
        eps_rand  =  rand() / (RAND_MAX + 1.0);
		eps_init  = eps_rand*(0.1+eps_max*exp(sigma2_1*pow(position[0]-mu,2.0)));
		}else{
		eps_init = 0.0;
		}
        MPntPStokesPlSetField_plastic_strain(mpprop_stokespl,eps_init);  
        MPntPStokesPlSetField_yield_indicator(mpprop_stokespl,0.0);
        
        
        DataFieldAccessPoint(PField_chrono,p,(void**)&mpprop_chrono);
        MPntPChronoSetField_age120(mpprop_chrono,-1.0);
        MPntPChronoSetField_age350(mpprop_chrono,-1.0);
        MPntPChronoSetField_age800(mpprop_chrono,-1.0);

    }
	DataFieldRestoreAccess(PField_std);
    DataFieldRestoreAccess(PField_stokes);
    DataFieldRestoreAccess(PField_stokespl);
    DataFieldRestoreAccess(PField_thermal);
    DataFieldRestoreAccess(PField_chrono);
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetInitialTemperatureOnNodes_julia"
PetscErrorCode pTatin2d_ModelSetInitialTemperatureOnNodes_julia(pTatinCtx ctx)
/* define properties on material points */
{
	PetscErrorCode         ierr;
	DM da;
	Vec T;
	PhysCompEnergyCtx phys_energy;
	PetscInt i,j,n,k,si,sj,nx,ny;
	Vec coords;
	DMDACoor2d **LA_coords;
	DM cda;
	PetscScalar **LA_T;
    PetscScalar Ttop,Tbot,Tlitho,Tradio,dtdy,jmin[2],jmax[2],age,kappa,ylab,pp[100],characteristic_time,h_prod,y_prod;
	RheologyConstants      *rheology;
	pTatinUnits *units;
	PetscBool              addanomaly=PETSC_FALSE;
	
	PetscFunctionBegin;
	
	phys_energy = ctx->phys_energy;
	rheology    = &ctx->rheology_constants;
	
	da          = phys_energy->daT;
	T           = phys_energy->T;
	//kappa       = rheology->temp_kappa[1];
	//ylab        = rheology->ylab;
	//h_prod      = rheology->h_prod;
	//y_prod      = rheology->y_prod;
	
	units       = &ctx->units;
	
	Tbot   = 1400.0;
	Ttop   = 0.0; 
	Tlitho = 1300.0;
	age    = 150.0;
	h_prod = 1e-12;
	y_prod = -10.0e3;
	ylab   = -120.0e3;
	kappa  = 1.0e-6;

	ierr = PetscOptionsGetScalar(PETSC_NULL,"-Ttop",&Ttop,PETSC_NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetScalar(PETSC_NULL,"-Tbot",&Tbot,PETSC_NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-Tlitho",&Tlitho,PETSC_NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-Thermal_age_Myr",&age,PETSC_NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-h_prod",&h_prod,PETSC_NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-y_prod",&y_prod,PETSC_NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-ylab",&ylab,PETSC_NULL);CHKERRQ(ierr);
	
	age = age * 3.14e13; 
	
  UnitsApplyInverseScaling(units->si_time,age,&age);
  UnitsApplyInverseScaling(units->si_diffusivity,kappa,&kappa);
	UnitsApplyInverseScaling(units->si_length,y_prod,&y_prod);
  UnitsApplyInverseScaling(units->si_heatsource,h_prod,&h_prod);
	UnitsApplyInverseScaling(units->si_length,ylab,&ylab);
	
	ierr = DMDAGetCoordinates(da,&coords);CHKERRQ(ierr);
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,coords,&LA_coords);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(da,T,&LA_T);CHKERRQ(ierr);
	ierr = DMDAGetCorners(da,&si,&sj,0 , &nx,&ny,0);CHKERRQ(ierr);
	
	DMDAGetBoundingBox(da,jmin,jmax);
	
	Tradio = (h_prod*pow(y_prod,2.0))/kappa;
	dtdy   = (Tlitho-Tradio-Ttop)/(-ylab);
	
	
	
	ierr = PetscOptionsGetBool(0,"-add_Tanomaly",&addanomaly,0);CHKERRQ(ierr);
	
{
		PetscScalar age_anom,Cx,Wx,age_new, T; 
		PetscBool   found;
		
			characteristic_time = age/pow(ylab,2)/kappa;
		
		for( j=sj; j<sj+ny; j++ ) {
			for( i=si; i<si+nx; i++ ) {
				double position[2];
				position[0] = LA_coords[j][i].x;
				position[1] = LA_coords[j][i].y;
			
	
			
			
				if (position[1] > ylab){
				
					
				

           T = Ttop-dtdy*position[1]+Tradio*(1-exp(position[1]/(-y_prod)));

				


          if (T < Tlitho){
						LA_T[j][i] = T;
					}else{
						LA_T[j][i] = Tlitho;
					}

				}else{ /* Adiabatic gradient from Tlitho to Tbot */
					LA_T[j][i] = -((Tbot-Tlitho)/(jmin[1]-ylab))*(ylab-position[1])+Tlitho;
				} 
			
			}
		
		}
}
	
	ierr = DMDAVecRestoreArray(da,T,&LA_T);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(cda,coords,&LA_coords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_julia"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_julia(pTatinCtx ctx)
{
	BCList         ubclist;
	PetscScalar    bcval;
	DM             dav;
	PetscErrorCode ierr;
    DM da;
    Vec T;
	PetscReal   jmin[3],jmax[3],I;
    PetscScalar opts_srH,v_init,opts_Lx,opts_Ly;
    PhysCompEnergyCtx phys_energy;

	PetscScalar alpha_m;
	pTatinUnits *units;
	PetscBool              invert=PETSC_FALSE;
	
    
	PetscFunctionBegin;
	
	units   = &ctx->units;
	ubclist = ctx->u_bclist;
	dav     = ctx->dav;
    opts_srH = 1.6e-10;
    ierr = PetscOptionsGetScalar(PETSC_NULL,"-vx",&opts_srH,0);CHKERRQ(ierr);
	v_init   = opts_srH;
    UnitsApplyInverseScaling(units->si_velocity,opts_srH,&opts_srH);
    DMDAGetBoundingBox(dav,jmin,jmax);
    opts_Lx = jmax[0]-jmin[0];
    opts_Ly = jmax[1]-jmin[1];

	
    	
    /* velocity at the bottom to fill up or empty down with material that leaves on the side*/
    bcval = 2*opts_srH*opts_Ly/opts_Lx; 
    ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
    bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
    bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
    
    /* check for energy solver */
    
            phys_energy = ctx->phys_energy;
            da          = phys_energy->daT;
            

            bcval = 0.0;
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-Ttop",&bcval,0);CHKERRQ(ierr);
            ierr = DMDABCListTraverse(phys_energy->T_bclist,da,DMDABCList_JMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
            bcval = 1400.0;
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-Tbot",&bcval,0);CHKERRQ(ierr);
            ierr = DMDABCListTraverse(phys_energy->T_bclist,da,DMDABCList_JMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
                    
    
    PetscFunctionReturn(0);
}
