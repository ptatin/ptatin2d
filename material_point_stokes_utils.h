
#ifndef __MATERIAL_POINT_PSTOKES_UTILS_H__
#define __MATERIAL_POINT_PSTOKES_UTILS_H__

#include "swarm_fields.h"
#include "data_exchanger.h"
#include "MPntStd_def.h"
#include "MPntPStokes_def.h"

PetscErrorCode SwarmOutputParaView_MPntPStokes(DataBucket db,const char path[],const char prefix[]);
PetscErrorCode SwarmUpdateGaussPropertiesLocalL2Projection_MPntPStokes(const int npoints,MPntStd mp_std[],MPntPStokes mp_stokes[],DM da,QuadratureStokes Q);
PetscErrorCode SwarmUpdateGaussPropertiesLocalL2Projection_P0_MPntPStokes(const int npoints,MPntStd mp_std[],MPntPStokes mp_stokes[],MaterialCoeffAvergeType average_type,DM da,QuadratureStokes Q);
PetscErrorCode SwarmUpdateGaussPropertiesLocalL2Projection_Q1_MPntPStokes(const int npoints,MPntStd mp_std[],MPntPStokes mp_stokes[],
    MaterialCoeffAvergeType average_type,DM da,QuadratureStokes Q);
PetscErrorCode SwarmUpdatePosition_Communication_MPntPStokes(DM da,DataBucket db,DataEx de);
PetscErrorCode SwarmUpdateProperties_MPntPStokes(DataBucket db,pTatinCtx ctx,Vec X);

PetscErrorCode QuadraturePointProjection_LocalL2Projection_P0_MPntPStokes(
									const int npoints,MPntStd mp_std[],MPntPStokes mp_stokes[],MaterialCoeffAvergeType average_type,DM da,
									QuadratureStokes Q,SurfaceQuadratureStokes surfQlist[]);


#endif