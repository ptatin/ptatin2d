

#include "petsc.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"
#include "element_type_Q1.h"
#include "dmdae.h"
#include "dmda_compare.h"
#include "dmda_update_coords.h"

PetscErrorCode DMDAGetElements_DA_Q1_2D(DM dm,PetscInt *nel,PetscInt *npe,const PetscInt **eidx);

void ConstructNi_Q1_2D(PetscScalar _xi[],PetscScalar Ni[])
{
  PetscScalar xi  = _xi[0];
  PetscScalar eta = _xi[1];
	
  Ni[0] = 0.25*(1.0-xi)*(1.0-eta);
  Ni[1] = 0.25*(1.0+xi)*(1.0-eta);
  Ni[2] = 0.25*(1.0-xi)*(1.0+eta);
  Ni[3] = 0.25*(1.0+xi)*(1.0+eta);
}

void ConstructGNi_Q1_2D(PetscScalar _xi[],PetscScalar GNi[][NODES_PER_EL_Q1_2D])
{
  PetscScalar xi  = _xi[0];
  PetscScalar eta = _xi[1];
	
  GNi[0][0] = -0.25*(1.0-eta);
  GNi[0][1] =   0.25*(1.0-eta);
  GNi[0][2] = -0.25*(1.0+eta);
  GNi[0][3] =   0.25*(1.0+eta);
	
  GNi[1][0] = -0.25*(1.0-xi);
  GNi[1][1] = -0.25*(1.0+xi);
  GNi[1][2] =   0.25*(1.0-xi);
  GNi[1][3] =   0.25*(1.0+xi);
}

void ConstructGNx_Q1_2D(PetscScalar GNi[][NODES_PER_EL_Q1_2D],PetscScalar GNx[][NODES_PER_EL_Q1_2D],PetscScalar coords[],PetscScalar *det_J)
{
  PetscScalar J00,J01,J10,J11,J;
  PetscScalar iJ00,iJ01,iJ10,iJ11;
  PetscInt    i;
	
  J00 = J01 = J10 = J11 = 0.0;
  for (i = 0; i < NODES_PER_EL_Q1_2D; i++) {
    PetscScalar cx = coords[ 2*i+0 ];
    PetscScalar cy = coords[ 2*i+1 ];
		
    J00 = J00+GNi[0][i]*cx;      /* J_xx = dx/dxi */
    J01 = J01+GNi[0][i]*cy;      /* J_xy = dy/dxi */
    J10 = J10+GNi[1][i]*cx;      /* J_yx = dx/deta */
    J11 = J11+GNi[1][i]*cy;      /* J_yy = dy/deta */
  }
  J = (J00*J11)-(J01*J10);
	
  iJ00 =  J11/J;
  iJ01 = -J01/J;
  iJ10 = -J10/J;
  iJ11 =  J00/J;
  for (i = 0; i < NODES_PER_EL_Q1_2D; i++) {
    GNx[0][i] = GNi[0][i]*iJ00+GNi[1][i]*iJ01;
    GNx[1][i] = GNi[0][i]*iJ10+GNi[1][i]*iJ11;
  }
  *det_J = J;
}

void ConstructElementTransformation_Q1_2D(PetscScalar coords[],PetscScalar GNi[][NODES_PER_EL_Q1_2D],PetscScalar _J[][2],PetscScalar _invJ[][2])
{
  PetscScalar J00,J01,J10,J11,J;
  PetscScalar iJ00,iJ01,iJ10,iJ11;
  PetscInt    i;
	
  J00 = J01 = J10 = J11 = 0.0;
  for (i = 0; i < NODES_PER_EL_Q1_2D; i++) {
    PetscScalar cx = coords[ 2*i+0 ];
    PetscScalar cy = coords[ 2*i+1 ];
		
    J00 = J00+GNi[0][i]*cx;      /* J_xx = dx/dxi */
    J01 = J01+GNi[0][i]*cy;      /* J_xy = dy/dxi */
    J10 = J10+GNi[1][i]*cx;      /* J_yx = dx/deta */
    J11 = J11+GNi[1][i]*cy;      /* J_yy = dy/deta */
  }
	_J[0][0] = J00;
	_J[0][1] = J01;
	_J[1][0] = J10;
	_J[1][1] = J11;
  
	J = (J00*J11)-(J01*J10);
	
  iJ00 =  J11/J;
  iJ01 = -J01/J;
  iJ10 = -J10/J;
  iJ11 =  J00/J;
	
	_invJ[0][0] = iJ00;
	_invJ[0][1] = iJ01;
	_invJ[1][0] = iJ10;
	_invJ[1][1] = iJ11;
}

#undef __FUNCT__  
#define __FUNCT__ "DMDACreateOverlappingQ1FromQ2"
PetscErrorCode DMDACreateOverlappingQ1FromQ2(DM dmq2,DM *dmq1)
{
	DM dm;
	DMDAE dae;
	PetscInt sei,sej,lmx,lmy,MX,MY,Mp,Np,i,j,n;
	PetscInt *siq2,*sjq2,*lmxq2,*lmyq2,*lxq1,*lyq1;
	PetscInt *lsip,*lsjp;
	PetscErrorCode ierr;
	int rank;
	PetscFunctionBegin;
	
	
	ierr = DMDAGetCornersElementQ2(dmq2,&sei,&sej,0,&lmx,&lmy,0);CHKERRQ(ierr);
	ierr = DMDAGetSizeElementQ2(dmq2,&MX,&MY,0);CHKERRQ(ierr);
	ierr = DMDAGetInfo(dmq2,0,0,0,0,&Mp,&Np,PETSC_NULL,0,0, 0,0,0, 0);CHKERRQ(ierr);

	ierr = DMDAGetOwnershipRangesElementQ2(dmq2,0,0,0,&siq2,&sjq2,0,&lmxq2,&lmyq2,0);CHKERRQ(ierr);

	
	ierr = PetscMalloc(sizeof(PetscInt)*Mp,&lsip);CHKERRQ(ierr);
	ierr = PetscMalloc(sizeof(PetscInt)*Np,&lsjp);CHKERRQ(ierr);
	
	for (i=0; i<Mp; i++) {
		lsip[i] = siq2[i]/2;
	}

	for (j=0; j<Np; j++) {
		lsjp[j] = sjq2[j]/2;
	}
	
	ierr = PetscMalloc(sizeof(PetscInt)*Mp,&lxq1);CHKERRQ(ierr);
	ierr = PetscMalloc(sizeof(PetscInt)*Np,&lyq1);CHKERRQ(ierr);

	for (i=0; i<Mp-1; i++) {
		lxq1[i] = siq2[i+1]/2 - siq2[i]/2;
	}
	lxq1[Mp-1] = (2*MX - siq2[Mp-1])/2;
	lxq1[Mp-1]++;

	for (j=0; j<Np-1; j++) {
		lyq1[j] = sjq2[j+1]/2 - sjq2[j]/2;
	}
	lyq1[Np-1] = (2*MY - sjq2[Np-1])/2;
	lyq1[Np-1]++;
	
	

	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	if (rank==0) {
		for (i=0; i<Mp; i++) {
			printf("rank[%d] startI = %d: lmxq1 = %d \n",i,lsip[i],lxq1[i]);
		}
		for (j=0; j<Np; j++) {
			printf("rank[%d] startJ = %d: lmyq1 = %d \n",j,lsjp[j],lyq1[j]);
		}
	}
	
	
	ierr = DMDACreate2d(((PetscObject)dmq2)->comm,DMDA_BOUNDARY_NONE,DMDA_BOUNDARY_NONE, DMDA_STENCIL_BOX, MX+1,MY+1, Mp,Np, 1,1, lxq1,lyq1, &dm );CHKERRQ(ierr);

	
	/* add the space for the data structure */
	ierr = DMAttachDMDAE(dm);CHKERRQ(ierr);
	/* fetch  the data structure */
	ierr = DMGetDMDAE(dm,&dae);CHKERRQ(ierr);
	dae->ne = MX * MY;
	dae->lne = lmx * lmy;

	dae->mx = MX;
	dae->my = MY;
	dae->lmx = lmx;
	dae->lmy = lmy;
	
	dae->si = sei/2;
	dae->sj = sej/2;

	dae->npe = 4;
	dae->nps = 2;
	dae->overlap = 0;
	
	for (i=0; i<Mp; i++) {
		lxq1[i] = lmxq2[i];
	}
	for (j=0; j<Np; j++) {
		lyq1[j] = lmyq2[j];
	}
	dae->lmxp = lxq1;
	dae->lmyp = lyq1;
	
	dae->lsip = lsip;
	dae->lsjp = lsjp;
	
	*dmq1 = dm;
	
	
	PetscFree(siq2);
	PetscFree(sjq2);
	PetscFree(lmxq2);
	PetscFree(lmyq2);

//  attached to structure for Q1 mesh //
//	PetscFree(lxq1);
//	PetscFree(lyq1);
		
	/* force element creation using MY numbering */
	{
		PetscInt nel,nen;
		const PetscInt *els;
		ierr = DMDAGetElements_DA_Q1_2D(dm,&nel,&nen,&els);CHKERRQ(ierr);
	}	
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "DMDAProjectCoordinatesQ2toQ1_2d"
PetscErrorCode DMDAProjectCoordinatesQ2toQ1_2d(DM daq2,DM daq1)
{
	PetscErrorCode ierr;
	Vec coordsQ2, coordsQ1;
	DMDACoor2d **LA_coordsQ2;
	DMDACoor2d **LA_coordsQ1;
	PetscInt si1,sj1,sk1,nx1,ny1,nz1,i,j;
	DM cdaQ2,cdaQ1;
	PetscBool overlap;
	
	PetscFunctionBegin;
	
	ierr = DMVerifyOverlappingIndices(daq2,daq1,PETSC_FALSE,&overlap);CHKERRQ(ierr);
	if (overlap==PETSC_FALSE) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"DA(Q2) must overlap DA(Q1) in global space");
	}
	
	ierr = DMDAGetCoordinates(daq2,&coordsQ2);CHKERRQ(ierr);
	ierr = DMDAGetCoordinateDA(daq2,&cdaQ2);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cdaQ2,coordsQ2,&LA_coordsQ2);CHKERRQ(ierr);
	
	ierr = DMDASetUniformCoordinates(daq1,-2.0,2.0,-2.0,2.0,-2.0,2.0);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(daq1,&coordsQ1);CHKERRQ(ierr);
	ierr = DMDAGetCoordinateDA(daq1,&cdaQ1);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cdaQ1,coordsQ1,&LA_coordsQ1);CHKERRQ(ierr);

	ierr = DMDAGetCorners(daq1,&si1,&sj1,&sk1 , &nx1,&ny1,&nz1);CHKERRQ(ierr);
	for( j=sj1; j<sj1+ny1; j++ ) {
		for( i=si1; i<si1+nx1; i++ ) {
			LA_coordsQ1[j][i].x = LA_coordsQ2[2*j][2*i].x;
			LA_coordsQ1[j][i].y = LA_coordsQ2[2*j][2*i].y;
		}
	}
	
	ierr = DMDAVecRestoreArray(cdaQ1,coordsQ1,&LA_coordsQ1);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(cdaQ2,coordsQ2,&LA_coordsQ2);CHKERRQ(ierr);
	
	ierr = DMDAUpdateGhostedCoordinates(daq1);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}


#if 0
#undef __FUNCT__
#define __FUNCT__ "DMDAGetElements_Q1_2D"
PetscErrorCode DMDAGetElements_Q1_2D(DM dm,PetscInt *nel,PetscInt *nen,const PetscInt *e[])
{
  PetscErrorCode ierr;
  DM_DA          *da = (DM_DA*)dm->data;
  PetscInt       i,xs,xe,Xs,Xe;
  PetscInt       j,ys,ye,Ys,Ye;
  PetscInt       cnt=0, cell[4], ns=2, nn=4;
  PetscInt       c, split[] = {0,1,3,
		2,3,1};
  PetscFunctionBegin;
  if (!da->e) {
    if (da->elementtype == DMDA_ELEMENT_Q1) {ns=1; nn=4;}
    ierr = DMDAGetCorners(dm,&xs,&ys,0,&xe,&ye,0);CHKERRQ(ierr);
    ierr = DMDAGetGhostCorners(dm,&Xs,&Ys,0,&Xe,&Ye,0);CHKERRQ(ierr);
    xe += xs; Xe += Xs; if (xs != Xs) xs -= 1;
    ye += ys; Ye += Ys; if (ys != Ys) ys -= 1;
    da->ne = ns*(xe - xs - 1)*(ye - ys - 1);
    ierr = PetscMalloc((1 + nn*da->ne)*sizeof(PetscInt),&da->e);CHKERRQ(ierr);
    for (j=ys; j<ye-1; j++) {
      for (i=xs; i<xe-1; i++) {
        cell[0] = (i-Xs  ) + (j-Ys  )*(Xe-Xs); /* bottom left */
        cell[1] = (i-Xs+1) + (j-Ys  )*(Xe-Xs); /* bottom right */
        cell[3] = (i-Xs+1) + (j-Ys+1)*(Xe-Xs); /* top right */
        cell[2] = (i-Xs  ) + (j-Ys+1)*(Xe-Xs); /* top left */
        if (da->elementtype == DMDA_ELEMENT_Q1) {
          for (c=0; c<ns*nn; c++)
            da->e[cnt++] = cell[c];
        }
      }
    }
  }
  *nel = da->ne;
  *nen = 4;
  *e   = da->e;
  PetscFunctionReturn(0);
}
#endif

#undef __FUNCT__
#define __FUNCT__ "DMDAGetElements_DA_Q1_2D"
PetscErrorCode DMDAGetElements_DA_Q1_2D(DM dm,PetscInt *nel,PetscInt *npe,const PetscInt **eidx)
{
  DM_DA          *da = (DM_DA*)dm->data;
	const PetscInt order = 1;
	PetscErrorCode ierr;
	PetscInt *idx,mx,my,_npe, M,N,P;
	PetscInt ei,ej,i,j,elcnt,esi,esj,gsi,gsj,nid[4],n,d,X,width;
	PetscInt *el;
	int rank;
	PetscFunctionBegin;
	
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	ierr = DMDAGetInfo(dm,0, &M,&N,&P, 0,0,0, 0,&width, 0,0,0, 0);CHKERRQ(ierr);
	if (width!=1) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Stencil width must be 1 for Q1");
	}
	
	_npe = (order + 1)*(order + 1);
  if (!da->e) {
		DMDAE dmdae;
		
		ierr = DMGetDMDAE(dm,&dmdae);CHKERRQ(ierr);
		mx = dmdae->lmx;
		my = dmdae->lmy;
		esi = dmdae->si;
		esj = dmdae->sj;
		//ierr = DMDAGetCornersElementQ2(dm,&esi,&esj,0,&mx,&my,0);CHKERRQ(ierr);
		ierr = PetscMalloc(sizeof(PetscInt)*(mx*my*_npe+1),&idx);CHKERRQ(ierr);
		//ierr = DMDAGetGlobalIndices(dm,&ngidx,&gidx);CHKERRQ(ierr);
		ierr = DMDAGetGhostCorners(dm,&gsi,&gsj,0, &X,0,0);CHKERRQ(ierr);
		//ierr = DMDAGetInfo(dm,0, 0,0,0, 0,0,0, &dof,0, 0,0,0, 0);CHKERRQ(ierr);
		
		elcnt = 0;
		printf("[%d]: esi=%d, gsi=%d\n", rank,esi,gsi);
		for (ej=0; ej<my; ej++) {
			j = (esj-gsj) + ej;
			for (ei=0; ei<mx; ei++) {
				i = (esi-gsi) + ei;
				el = &idx[_npe*elcnt];
				
//				printf("[%d]: i=%d \n", rank,i );
				nid[0] = (i  ) + (j  ) *X; 
				nid[1] = (i+1) + (j  ) *X; 
				nid[2] = (i  ) + (j+1) *X; 
				nid[3] = (i+1) + (j+1) *X; 
/*				
				if(rank==0) 
					printf("[rank=%d] %d,%d, %d %d %d,%d \n", rank,ei,ej,
																	 nid[0],nid[1],nid[2],nid[3] );
*/				
				for (n=0; n<_npe; n++) {
					if (nid[n]>M*N*P) { 
						SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_CORRUPT,"Local indexing exceeds number of global nodes");
					}
					el[n] = nid[n]; //gidx[dof*nid[n]+0]/dof;
				}
				
				elcnt++;
			}
		}
		
		da->e  = idx;
		da->ne = elcnt;
	}
	
	*eidx = da->e;
	*npe = _npe;
	*nel = da->ne;
	
	PetscFunctionReturn(0);
}



#undef __FUNCT__
#define __FUNCT__ "DMDAGetElementsQ1"
PetscErrorCode DMDAGetElementsQ1(DM dm,PetscInt *nel,PetscInt *nen,const PetscInt *e[])
{
  DM_DA          *da = (DM_DA*)dm->data;
  PetscErrorCode ierr;
  PetscFunctionBegin;
  if (da->dim==-1) {
    *nel = 0; *nen = 0; *e = PETSC_NULL;
  } else if (da->dim==1) {
    SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"DMDA doesn't support Q1 in 1D");
  } else if (da->dim==2) {
    ierr = DMDAGetElements_DA_Q1_2D(dm,nel,nen,e);CHKERRQ(ierr);
  } else if (da->dim==3) {
    SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"DMDA doesn't support Q1 in 3D");
  } else {
    SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_ARG_CORRUPT,"DMDA dimension not 1, 2, or 3, it is %D\n",da->dim);
  }
	PetscFunctionReturn(0);
}

/* element helpers */
#undef __FUNCT__
#define __FUNCT__ "DMDAGetElementCoordinatesQ1_2D"
PetscErrorCode DMDAGetElementCoordinatesQ1_2D(PetscScalar elcoords[],PetscInt elnid[],PetscScalar LA_gcoords[])
{
	PetscInt n;
	PetscErrorCode ierr;
	PetscFunctionBegin;
	for (n=0; n<4; n++) {
		elcoords[2*n  ] = LA_gcoords[2*elnid[n]  ];
		elcoords[2*n+1] = LA_gcoords[2*elnid[n]+1];
	}
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDAGetScalarElementFieldQ1_2D"
PetscErrorCode DMDAGetScalarElementFieldQ1_2D(PetscScalar elfield[],PetscInt elnid[],PetscScalar LA_gfield[])
{
	PetscInt n;
	PetscErrorCode ierr;
	PetscFunctionBegin;
	for (n=0; n<4; n++) {
		elfield[n] = LA_gfield[elnid[n]];
	}
	PetscFunctionReturn(0);
}
#undef __FUNCT__
#define __FUNCT__ "DMDAGetVectorElementFieldQ1_2D"
PetscErrorCode DMDAGetVectorElementFieldQ1_2D(PetscScalar elfield[],PetscInt elnid[],PetscScalar LA_gfield[])
{
	PetscInt n;
	PetscErrorCode ierr;
	PetscFunctionBegin;
	for (n=0; n<4; n++) {
		elfield[2*n  ] = LA_gfield[2*elnid[n]  ];
		elfield[2*n+1] = LA_gfield[2*elnid[n]+1];
	}
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "GetElementEqnIndicesScalarQ1"
PetscErrorCode GetElementEqnIndicesScalarQ1(PetscInt elnidx[],PetscInt globalindices[],PetscInt eldofidx[])
{
	PetscInt nid,idx,n,c;
	
	PetscFunctionBegin;
	n = 0; c = 0;
	
  nid = elnidx[n++];	eldofidx[c++] = globalindices[nid];
  
	nid = elnidx[n++];	eldofidx[c++] = globalindices[nid];
  
	nid = elnidx[n++];	eldofidx[c++] = globalindices[nid];
  
	nid = elnidx[n++];	eldofidx[c++] = globalindices[nid];
	
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "GetElementEqnIndicesVectorQ1"
PetscErrorCode GetElementEqnIndicesVectorQ1(PetscInt elnidx[],PetscInt globalindices[],PetscInt eldofidx[])
{
	PetscInt nid,idx,n,c;
	
	PetscFunctionBegin;
	n = 0; c = 0;

  nid = elnidx[n++];	eldofidx[c++] = globalindices[2*nid];	  eldofidx[c++] = globalindices[2*nid+1];
  
	nid = elnidx[n++];	eldofidx[c++] = globalindices[2*nid];	  eldofidx[c++] = globalindices[2*nid+1];
  
	nid = elnidx[n++];	eldofidx[c++] = globalindices[2*nid];	  eldofidx[c++] = globalindices[2*nid+1];
  
	nid = elnidx[n++];	eldofidx[c++] = globalindices[2*nid];	  eldofidx[c++] = globalindices[2*nid+1];
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "GetElementLocalIndicesQ1"
PetscErrorCode GetElementLocalIndicesQ1(PetscInt el_localIndices[],PetscInt elnid[])
{
	PetscInt n;
	PetscErrorCode ierr;
	PetscFunctionBegin;
	for (n=0; n<NODES_PER_EL_Q1_2D; n++) {
		el_localIndices[n  ] = elnid[n];
	}
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDASetValuesLocalStencil_AddValues_Scalar_Q1"
PetscErrorCode DMDASetValuesLocalStencil_AddValues_Scalar_Q1(PetscScalar *fields_F,PetscInt u_eqn[],PetscScalar Fe_u[])
{
  PetscInt n,idx;
	
  PetscFunctionBegin;
  for (n = 0; n<NODES_PER_EL_Q1_2D; n++) {
		idx = u_eqn[n  ];
    fields_F[idx] += Fe_u[n  ];
  }
  PetscFunctionReturn(0);
}

