

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"

#include "petscksp.h"
#include "petscdm.h"

#include "pTatin2d.h"
#include "swarm_fields.h"
#include "data_exchanger.h"
#include "MPntStd_def.h"
#include "MPntPStokes_def.h"


#undef __FUNCT__
#define __FUNCT__ "SwarmView_MPntPStokes_VTKascii"
PetscErrorCode SwarmView_MPntPStokes_VTKascii(DataBucket db,const char name[])
{
	PetscMPIInt rank;
	FILE *vtk_fp;
	PetscInt k;
	int npoints;
	PetscLogDouble t0,t1;
	DataField PField_std;
	DataField PField_stokes;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	ierr = PetscGetTime(&t0);CHKERRQ(ierr);
	
	if ((vtk_fp = fopen ( name, "w")) == NULL)  {
		SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
	}
	
	fprintf( vtk_fp, "<?xml version=\"1.0\"?>\n");
	
#ifdef WORDSIZE_BIGENDIAN
	fprintf( vtk_fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
#else
	fprintf( vtk_fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
#endif
	fprintf( vtk_fp, "\t<UnstructuredGrid>\n" );
	
	DataBucketGetSizes(db,&npoints,PETSC_NULL,PETSC_NULL);
	fprintf( vtk_fp, "\t\t<Piece NumberOfPoints=\"%d\" NumberOfCells=\"%d\">\n",npoints,npoints );
	
	
	fprintf( vtk_fp, "\n");
	fprintf( vtk_fp, "\t\t\t<Cells>\n");
	
	// connectivity //
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n");
	fprintf( vtk_fp, "\t\t\t\t");
	for(k=0;k<npoints;k++) {
		fprintf( vtk_fp,"%d ",k);
	}
	fprintf( vtk_fp, "\n");
	fprintf( vtk_fp, "\t\t\t\t</DataArray>\n");	
	
	// offsets //
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n");
	fprintf( vtk_fp, "\t\t\t\t");
	for(k=0;k<npoints;k++) {
		fprintf( vtk_fp,"%d ",k+1);
	}
	fprintf( vtk_fp, "\n");
	fprintf( vtk_fp, "\t\t\t\t</DataArray>\n");
	
	// types //
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\">\n");
	fprintf( vtk_fp, "\t\t\t\t");
	for(k=0;k<npoints;k++) {
		fprintf( vtk_fp,"1 "); // VTK_VERTEX //
	}
	fprintf( vtk_fp, "\n");
	fprintf( vtk_fp, "\t\t\t\t</DataArray>\n");
	
	fprintf( vtk_fp, "\t\t\t</Cells>\n");
	
	fprintf( vtk_fp, "\n");
	fprintf( vtk_fp, "\t\t\t<CellData>\n");
	fprintf( vtk_fp, "\t\t\t</CellData>\n");
	fprintf( vtk_fp, "\n");
	
	
	DataBucketGetDataFieldByName(db, MPntStd_classname ,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess( PField_std,sizeof(MPntStd));

	DataBucketGetDataFieldByName(db, MPntPStokes_classname ,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	DataFieldVerifyAccess( PField_stokes,sizeof(MPntPStokes));
	
	
	/* point coordinates */
	fprintf( vtk_fp, "\t\t\t<Points>\n");
	
	/* copy coordinates */
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	for(k=0;k<npoints;k++) {
		MPntStd *marker;
		double *coords;
		
		DataFieldAccessPoint(PField_std,k,(void**)&marker);
		
		
		/* extract coords from your data type */
		//coords = elasticParticle->pos;
		MPntStdGetField_global_coord( marker,&coords );
		
		fprintf( vtk_fp,"\t\t\t\t\t%lf %lf %lf \n",coords[0],coords[1],0.0);
	}
	fprintf( vtk_fp, "\t\t\t\t</DataArray>\n");
	
	fprintf( vtk_fp, "\t\t\t</Points>\n");
	fprintf( vtk_fp, "\n");
	
	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokes);
	
	
	
	/* point data BEGIN */
	fprintf( vtk_fp, "\t\t\t<PointData>\n");
	
	/* auto generated shit goes here */
	{
		MPntStd     *marker_std    = PField_std->data; /* should write a function to do this */
		MPntPStokes *marker_stokes = PField_stokes->data; /* should write a function to do this */
		
		MPntStdVTKWriteAsciiAllFields(vtk_fp,(const int)npoints,(const MPntStd*)marker_std );
		MPntPStokesVTKWriteAsciiAllFields(vtk_fp,(const int)npoints,(const MPntPStokes*)marker_stokes);
	}
	fprintf( vtk_fp, "\t\t\t</PointData>\n");
	fprintf( vtk_fp, "\n");
	/* point data END */
	
	
	fprintf( vtk_fp, "\t\t</Piece>\n");
	fprintf( vtk_fp, "\t</UnstructuredGrid>\n");
	
	fprintf( vtk_fp, "</VTKFile>\n");
	
	
	if( vtk_fp!= NULL ) {
		fclose( vtk_fp );
		vtk_fp = NULL;
	}
	
	ierr = PetscGetTime(&t1);CHKERRQ(ierr);
#ifdef PROFILE_TIMING
	PetscPrintf(PETSC_COMM_WORLD,"VTKWriter(%s): Time %1.4e sec\n",__FUNCT__,t1-t0);
#endif
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SwarmView_MPntPStokes_VTKappended_binary"
PetscErrorCode SwarmView_MPntPStokes_VTKappended_binary(DataBucket db,const char name[])
{
	PetscMPIInt rank;
	FILE *vtk_fp;
	PetscInt k;
	int npoints;
	PetscLogDouble t0,t1;
	DataField PField_std;
	DataField PField_stokes;
	int byte_offset,length;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	ierr = PetscGetTime(&t0);CHKERRQ(ierr);
	
	if ((vtk_fp = fopen ( name, "w")) == NULL)  {
		SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
	}
	
	fprintf( vtk_fp, "<?xml version=\"1.0\"?>\n");
	
#ifdef WORDSIZE_BIGENDIAN
	fprintf( vtk_fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
#else
	fprintf( vtk_fp, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
#endif
	fprintf( vtk_fp, "\t<UnstructuredGrid>\n" );
	
	DataBucketGetSizes(db,&npoints,PETSC_NULL,PETSC_NULL);
	fprintf( vtk_fp, "\t\t<Piece NumberOfPoints=\"%d\" NumberOfCells=\"%d\">\n",npoints,npoints );
	
	
	fprintf( vtk_fp, "\n");
	fprintf( vtk_fp, "\t\t\t<Cells>\n");
	
	byte_offset = 0;
	
	// connectivity //
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Int32\" Name=\"connectivity\" format=\"appended\" offset=\"%d\" />\n",byte_offset);
  byte_offset = byte_offset + sizeof(int) + npoints * sizeof(int);
	
	// offsets //
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Int32\" Name=\"offsets\" format=\"appended\" offset=\"%d\" />\n",byte_offset);
  byte_offset = byte_offset + sizeof(int) + npoints * sizeof(int);
	
	// types //
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"UInt8\" Name=\"types\" format=\"appended\" offset=\"%d\" />\n",byte_offset);
  byte_offset = byte_offset + sizeof(int) + npoints * sizeof(unsigned char);
	
	fprintf( vtk_fp, "\t\t\t</Cells>\n");
	
	fprintf( vtk_fp, "\n");
	fprintf( vtk_fp, "\t\t\t<CellData>\n");
	fprintf( vtk_fp, "\t\t\t</CellData>\n");
	fprintf( vtk_fp, "\n");

	
	DataBucketGetDataFieldByName(db, MPntStd_classname ,&PField_std);
	DataBucketGetDataFieldByName(db, MPntPStokes_classname ,&PField_stokes);
	
	
	/* point coordinates */
	fprintf( vtk_fp, "\t\t\t<Points>\n");
	
	fprintf( vtk_fp, "\t\t\t\t<DataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\" format=\"appended\" offset=\"%d\" />\n",byte_offset);
  byte_offset = byte_offset + sizeof(int) + npoints * 3 * sizeof(double);
	
	fprintf( vtk_fp, "\t\t\t</Points>\n");
	fprintf( vtk_fp, "\n");

	/* point data BEGIN */
	fprintf( vtk_fp, "\t\t\t<PointData>\n");
	
	/* auto generated shit goes here */
	{
		MPntStd     *marker_std    = PField_std->data; /* should write a function to do this */
		MPntPStokes *marker_stokes = PField_stokes->data; /* should write a function to do this */
		
		MPntStdVTKWriteBinaryAppendedHeaderAllFields(vtk_fp,&byte_offset,(const int)npoints,(const MPntStd*)marker_std );
		MPntPStokesVTKWriteBinaryAppendedHeaderAllFields(vtk_fp,&byte_offset,(const int)npoints,(const MPntPStokes*)marker_stokes);
	}
	fprintf( vtk_fp, "\t\t\t</PointData>\n");
	fprintf( vtk_fp, "\n");
	/* point data END */
	
	
	fprintf( vtk_fp, "\t\t</Piece>\n");
	fprintf( vtk_fp, "\t</UnstructuredGrid>\n");
	
	/* WRITE APPENDED DATA HERE */
	fprintf( vtk_fp,"\t<AppendedData encoding=\"raw\">\n");
	fprintf( vtk_fp,"_");
	
	/* connectivity, offsets, types, coords */
	////////////////////////////////////////////////////////
	/* write connectivity */
	length = sizeof(int)*npoints;
	fwrite( &length,sizeof(int),1,vtk_fp);
	for (k=0; k<npoints; k++) {
		int idx = k;
		fwrite( &idx, sizeof(int),1, vtk_fp );
	}
	////////////////////////////////////////////////////////
	/* write offset */
	length = sizeof(int)*npoints;
	fwrite( &length,sizeof(int),1,vtk_fp);
	for (k=0; k<npoints; k++) {
		int idx = k+1;
		fwrite( &idx, sizeof(int),1, vtk_fp );
	}
	////////////////////////////////////////////////////////
	/* write types */
	length = sizeof(unsigned char)*npoints;
	fwrite( &length,sizeof(int),1,vtk_fp);
	for (k=0; k<npoints; k++) {
		unsigned char idx = 1; /* VTK_VERTEX */
		fwrite( &idx, sizeof(unsigned char),1, vtk_fp );
	}
	////////////////////////////////////////////////////////
	/* write coordinates */
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	length = sizeof(double)*npoints*3;
	fwrite( &length,sizeof(int),1,vtk_fp);
	for (k=0; k<npoints; k++) {
		MPntStd *marker;
		double  *coor;
		double  coords_k[] = {0.0, 0.0, 0.0};
		
		DataFieldAccessPoint(PField_std,k,(void**)&marker);
		MPntStdGetField_global_coord(marker,&coor);
		coords_k[0] = coor[0];
		coords_k[1] = coor[1];
		
		fwrite( coords_k, sizeof(double), 3, vtk_fp );
	}
	DataFieldRestoreAccess(PField_std);
	
	/* auto generated shit for the marker data goes here */
	{
		MPntStd     *markers_std    = PField_std->data;
		MPntPStokes *markers_stokes = PField_stokes->data;

		MPntStdVTKWriteBinaryAppendedDataAllFields(vtk_fp,npoints,markers_std);
		MPntPStokesVTKWriteBinaryAppendedDataAllFields(vtk_fp,npoints,markers_stokes);
	}
	
	fprintf( vtk_fp,"\n\t</AppendedData>\n");
	
	
	fprintf( vtk_fp, "</VTKFile>\n");
	
	
	if( vtk_fp!= NULL ) {
		fclose( vtk_fp );
		vtk_fp = NULL;
	}
	
	ierr = PetscGetTime(&t1);CHKERRQ(ierr);
#ifdef PROFILE_TIMING
	PetscPrintf(PETSC_COMM_WORLD,"VTKWriter(%s): Time %1.4e sec\n",__FUNCT__,t1-t0);
#endif
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "__SwarmView_MPntPStokes_PVTU"
PetscErrorCode __SwarmView_MPntPStokes_PVTU(const char prefix[],const char name[])
{
	PetscMPIInt nproc,rank;
	FILE *vtk_fp;
	PetscInt i;
	char *sourcename;
	
	PetscFunctionBegin;
	
	if ((vtk_fp = fopen ( name, "w")) == NULL)  {
		SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
	}
	
	/* (VTK) generate pvts header */
	fprintf( vtk_fp, "<?xml version=\"1.0\"?>\n");
	
#ifdef WORDSIZE_BIGENDIAN
	fprintf( vtk_fp, "<VTKFile type=\"PUnstructuredGrid\" version=\"0.1\" byte_order=\"BigEndian\">\n");
#else
	fprintf( vtk_fp, "<VTKFile type=\"PUnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
#endif
	
	/* define size of the nodal mesh based on the cell DM */
	fprintf( vtk_fp, "  <PUnstructuredGrid GhostLevel=\"0\">\n" ); /* note overlap = 0 */
	
	
	/* DUMP THE CELL REFERENCES */
	fprintf( vtk_fp, "    <PCellData>\n");
	fprintf( vtk_fp, "    </PCellData>\n");
	
	
	///////////////
	fprintf( vtk_fp, "    <PPoints>\n");
	fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\"/>\n");
	fprintf( vtk_fp, "    </PPoints>\n");
	///////////////
	
	///////////////
  fprintf(vtk_fp, "    <PPointData>\n");
	MPntStdPVTUWriteAllPPointDataFields(vtk_fp);
	MPntPStokesPVTUWriteAllPPointDataFields(vtk_fp);
  fprintf(vtk_fp, "    </PPointData>\n");
	///////////////
	
	
	/* write out the parallel information */
	MPI_Comm_size(PETSC_COMM_WORLD,&nproc);
	for (i=0; i<nproc; i++) {
		asprintf( &sourcename, "%s-subdomain%1.5d.vtu", prefix, i );
		fprintf( vtk_fp, "    <Piece Source=\"%s\"/>\n",sourcename);
		free(sourcename);
	}
	
	/* close the file */
	fprintf( vtk_fp, "  </PUnstructuredGrid>\n");
	fprintf( vtk_fp, "</VTKFile>\n");
	
	if(vtk_fp!=NULL){
		fclose( vtk_fp );
		vtk_fp = NULL;
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SwarmOutputParaView_MPntPStokes"
PetscErrorCode SwarmOutputParaView_MPntPStokes(DataBucket db,const char path[],const char prefix[])
{ 
	char *vtkfilename,*filename;
	PetscMPIInt rank;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ierr = pTatinGenerateParallelVTKName(prefix,"vtu",&vtkfilename);CHKERRQ(ierr);
	if (path) {
		asprintf(&filename,"%s/%s",path,vtkfilename);
	} else {
		asprintf(&filename,"./%s",vtkfilename);
	}

//#ifdef __VTK_ASCII__
//	ierr = SwarmView_MPntPStokes_VTKascii( db,filename );CHKERRQ(ierr);
//#endif
//#ifndef __VTK_ASCII__
	ierr = SwarmView_MPntPStokes_VTKappended_binary(db,filename);CHKERRQ(ierr);
//#endif
	free(filename);
	free(vtkfilename);
	
	
	ierr = pTatinGenerateVTKName(prefix,"pvtu",&vtkfilename);CHKERRQ(ierr);
	if (path) {
		asprintf(&filename,"%s/%s",path,vtkfilename);
	} else {
		asprintf(&filename,"./%s",vtkfilename);
	}
	
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	if (rank==0) {
		ierr = __SwarmView_MPntPStokes_PVTU( prefix, filename );CHKERRQ(ierr);
	}
	free(filename);
	free(vtkfilename);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_SwarmUpdateGaussPropertiesLocalL2Projection_MPntPStokes"
PetscErrorCode _SwarmUpdateGaussPropertiesLocalL2Projection_MPntPStokes(
										DM clone,Vec properties_A1,Vec properties_A2,Vec properties_B,
										const int npoints,MPntStd mp_std[],MPntPStokes mp_stokes[],QuadratureStokes Q) 
{
	PetscScalar Ni_p[Q2_NODES_PER_EL_2D];
	PetscScalar Ae1[Q2_NODES_PER_EL_2D], Ae2[Q2_NODES_PER_EL_2D], Be[Q2_NODES_PER_EL_2D];
	PetscInt el_lidx[U_BASIS_FUNCTIONS];
	Vec Lproperties_A1, Lproperties_A2, Lproperties_B;
	PetscScalar *LA_properties_A1, *LA_properties_A2, *LA_properties_B;
	PetscLogDouble t0,t1;
	PetscInt p,i;
	PetscInt nel,nen,e;
	const PetscInt *elnidx;

	PetscInt ngp;
	PetscScalar *xi_mp;
	PetscScalar NIu[MAX_QUAD_PNTS][U_BASIS_FUNCTIONS];
	GaussPointCoefficientsStokes *gausspoints;
	PetscScalar range_eta[2],range_rho[2];
	PetscErrorCode ierr;
	
	
	PetscFunctionBegin;
		
	ierr = DMGetLocalVector(clone,&Lproperties_A1);CHKERRQ(ierr);		ierr = VecZeroEntries(Lproperties_A1);CHKERRQ(ierr);
	ierr = DMGetLocalVector(clone,&Lproperties_A2);CHKERRQ(ierr);		ierr = VecZeroEntries(Lproperties_A2);CHKERRQ(ierr);
	ierr = DMGetLocalVector(clone,&Lproperties_B);CHKERRQ(ierr);		ierr = VecZeroEntries(Lproperties_B);CHKERRQ(ierr);
	
	ierr = VecGetArray(Lproperties_A1,&LA_properties_A1);CHKERRQ(ierr);
	ierr = VecGetArray(Lproperties_A2,&LA_properties_A2);CHKERRQ(ierr);
	ierr = VecGetArray(Lproperties_B, &LA_properties_B);CHKERRQ(ierr);

	ierr = DMDAGetElements_pTatin(clone,&nel,&nen,&elnidx);CHKERRQ(ierr);
	
	PetscGetTime(&t0);
	for (p=0; p<npoints; p++) {
		double *xi_p  = &mp_std[p].xi[0];
		double eta_p  = mp_stokes[p].eta;
		double rho_p  = mp_stokes[p].rho;
		
		ierr = PetscMemzero(Ae1,sizeof(PetscScalar)*Q2_NODES_PER_EL_2D);CHKERRQ(ierr);
		ierr = PetscMemzero(Ae2,sizeof(PetscScalar)*Q2_NODES_PER_EL_2D);CHKERRQ(ierr);
		ierr = PetscMemzero(Be, sizeof(PetscScalar)*Q2_NODES_PER_EL_2D);CHKERRQ(ierr);
		
		PTatinConstructNI_Q2_2D(xi_p,Ni_p);
		
		for (i=0; i<Q2_NODES_PER_EL_2D; i++) {
			Ae1[i] = Ni_p[i] * eta_p;
			Ae2[i] = Ni_p[i] * rho_p;
			Be[i]  = Ni_p[i];
		}
		
		
		/* sum into local vectors */
		e = mp_std[p].wil;
		ierr = Q2GetElementLocalIndicesDOF(el_lidx,1,(PetscInt*)&elnidx[nen*e]);CHKERRQ(ierr);

		ierr = DMDASetValuesLocalStencil_AddValues_DOF(LA_properties_A1, 1, el_lidx,Ae1);CHKERRQ(ierr);
		ierr = DMDASetValuesLocalStencil_AddValues_DOF(LA_properties_A2, 1, el_lidx,Ae2);CHKERRQ(ierr);
		ierr = DMDASetValuesLocalStencil_AddValues_DOF(LA_properties_B,  1, el_lidx,Be);CHKERRQ(ierr);

	}
	PetscGetTime(&t1);
//	PetscPrintf(PETSC_COMM_WORLD,"  [ L2 projection (summation): %1.4lf ]\n",t1-t0);
	
  ierr = VecRestoreArray(Lproperties_B,&LA_properties_B);CHKERRQ(ierr);
  ierr = VecRestoreArray(Lproperties_A2,&LA_properties_A2);CHKERRQ(ierr);
  ierr = VecRestoreArray(Lproperties_A1,&LA_properties_A1);CHKERRQ(ierr);
	
	
	/* scatter to quadrature points */
	ierr = DMLocalToGlobalBegin(clone,Lproperties_A1,ADD_VALUES,properties_A1);CHKERRQ(ierr);
	ierr = DMLocalToGlobalEnd(  clone,Lproperties_A1,ADD_VALUES,properties_A1);CHKERRQ(ierr);
	
	ierr = DMLocalToGlobalBegin(clone,Lproperties_A2,ADD_VALUES,properties_A2);CHKERRQ(ierr);
	ierr = DMLocalToGlobalEnd(  clone,Lproperties_A2,ADD_VALUES,properties_A2);CHKERRQ(ierr);
	
	ierr = DMLocalToGlobalBegin(clone,Lproperties_B,ADD_VALUES,properties_B);CHKERRQ(ierr);
	ierr = DMLocalToGlobalEnd(  clone,Lproperties_B,ADD_VALUES,properties_B);CHKERRQ(ierr);
	
	/* scale */
	ierr = VecPointwiseDivide( properties_A1, properties_A1, properties_B );CHKERRQ(ierr);
	ierr = VecPointwiseDivide( properties_A2, properties_A2, properties_B );CHKERRQ(ierr);
	/* ========================================= */
	
	/* scatter result back to local array and do the interpolation onto the quadrature points */
	ngp       = Q->ngp;
	xi_mp     = Q->xi;
	for (p=0; p<ngp; p++) {
		PetscScalar *xip = &xi_mp[2*p];
		
		PTatinConstructNI_Q2_2D(xip,NIu[p]);
	}
	
	
	PetscGetTime(&t0);
	ierr = VecZeroEntries(Lproperties_A1);CHKERRQ(ierr);
	ierr = VecZeroEntries(Lproperties_A2);CHKERRQ(ierr);
	
	ierr = DMGlobalToLocalBegin(clone,properties_A1,INSERT_VALUES,Lproperties_A1);CHKERRQ(ierr);
	ierr = DMGlobalToLocalEnd(  clone,properties_A1,INSERT_VALUES,Lproperties_A1);CHKERRQ(ierr);
	
	ierr = DMGlobalToLocalBegin(clone,properties_A2,INSERT_VALUES,Lproperties_A2);CHKERRQ(ierr);
	ierr = DMGlobalToLocalEnd(  clone,properties_A2,INSERT_VALUES,Lproperties_A2);CHKERRQ(ierr);
	PetscGetTime(&t1);
//	PetscPrintf(PETSC_COMM_WORLD,"  [ L2 projection (scatter): %1.4lf ]\n",t1-t0);
	
	PetscGetTime(&t0);
	ierr = VecGetArray(Lproperties_A1,&LA_properties_A1);CHKERRQ(ierr);
	ierr = VecGetArray(Lproperties_A2,&LA_properties_A2);CHKERRQ(ierr);
	
	/* traverse elements and interpolate */
	for (e=0;e<nel;e++) {
		ierr = QuadratureStokesGetCell(Q,e,&gausspoints);CHKERRQ(ierr);

		ierr = Q2GetElementLocalIndicesDOF(el_lidx,1,(PetscInt*)&elnidx[nen*e]);CHKERRQ(ierr);

//		ierr = DMDAGetScalarElementField_2D(LA_properties_A1,nen,(PetscInt*)&elnidx[nen*e],Ae1);CHKERRQ(ierr);
//		ierr = DMDAGetScalarElementField_2D(LA_properties_A2,nen,(PetscInt*)&elnidx[nen*e],Ae2);CHKERRQ(ierr);

		ierr = DMDAGetScalarElementField_2D(Ae1,nen,(PetscInt*)&elnidx[nen*e],LA_properties_A1);CHKERRQ(ierr);
		ierr = DMDAGetScalarElementField_2D(Ae2,nen,(PetscInt*)&elnidx[nen*e],LA_properties_A2);CHKERRQ(ierr);
		
		/* The Q2 interpolant tends to overshoot / undershoot when you have viscosity jumps */
		range_eta[0] = 1.0e32;  /* min */
		range_eta[1] = -1.0e32; /* max */
		range_rho[0] = 1.0e32;
		range_rho[1] = -1.0e32;
		for (i=0; i<Q2_NODES_PER_EL_2D; i++) {
			if (Ae1[i]<range_eta[0]) { range_eta[0] = Ae1[i]; }
			if (Ae1[i]>range_eta[1]) { range_eta[1] = Ae1[i]; }
			if (Ae2[i]<range_rho[0]) { range_rho[0] = Ae2[i]; }
			if (Ae2[i]>range_rho[1]) { range_rho[1] = Ae2[i]; }
		}
		
		/* interpolate eta into Fu[0] */
		for (p=0; p<ngp; p++) {
			gausspoints[p].eta = 0.0;
			gausspoints[p].Fu[0] = 0.0;
			gausspoints[p].Fu[1] = 0.0;
            gausspoints[p].stress_old[0] = 0.0;
            gausspoints[p].stress_old[1] = 0.0;
            gausspoints[p].stress_old[2] = 0.0;
			for (i=0; i<Q2_NODES_PER_EL_2D; i++) {
				gausspoints[p].eta    += NIu[p][i] * Ae1[i];
				gausspoints[p].Fu[0]  += NIu[p][i] * Ae2[i];
			}

			if (gausspoints[p].eta < range_eta[0]) { gausspoints[p].eta = range_eta[0]; }
			if (gausspoints[p].eta > range_eta[1]) { gausspoints[p].eta = range_eta[1]; }
			if (gausspoints[p].Fu[0] < range_rho[0]) { gausspoints[p].Fu[0] = range_rho[0]; }
			if (gausspoints[p].Fu[0] > range_rho[1]) { gausspoints[p].Fu[0] = range_rho[1]; }

			gausspoints[p].Fu[1] = gausspoints[p].Fu[0];

			gausspoints[p].Fu[0] = gausspoints[p].Fu[0] * gausspoints[p].gravity[0];
			gausspoints[p].Fu[1] = gausspoints[p].Fu[1] * gausspoints[p].gravity[1];
		}
	}
	
  ierr = VecRestoreArray(Lproperties_A2,&LA_properties_A2);CHKERRQ(ierr);
  ierr = VecRestoreArray(Lproperties_A1,&LA_properties_A1);CHKERRQ(ierr);
	
	PetscGetTime(&t1);
//	PetscPrintf(PETSC_COMM_WORLD,"  [ L2 projection (interpolation): %1.4lf ]\n",t1-t0);
	
	ierr = DMRestoreLocalVector(clone,&Lproperties_B);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(clone,&Lproperties_A2);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(clone,&Lproperties_A1);CHKERRQ(ierr);
	
	
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SwarmUpdateGaussPropertiesLocalL2Projection_MPntPStokes"
PetscErrorCode SwarmUpdateGaussPropertiesLocalL2Projection_MPntPStokes(const int npoints,MPntStd mp_std[],MPntPStokes mp_stokes[],DM da,QuadratureStokes Q)
{
	PetscInt  dof;
	DM        clone;
	Vec       properties_A1, properties_A2, properties_B;
	PetscBool view;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	/* setup */
	dof = 1;
	ierr = DMDADuplicateLayout(da,dof,2,DMDA_STENCIL_BOX,&clone);CHKERRQ(ierr); /* Q2 */
	
	ierr = DMGetGlobalVector(clone,&properties_A1);CHKERRQ(ierr);  ierr = PetscObjectSetName( (PetscObject)properties_A1, "LocalL2Proj_nu");CHKERRQ(ierr);
	ierr = DMGetGlobalVector(clone,&properties_A2);CHKERRQ(ierr);  ierr = PetscObjectSetName( (PetscObject)properties_A2, "LocalL2Proj_rho");CHKERRQ(ierr);
	ierr = DMGetGlobalVector(clone,&properties_B);CHKERRQ(ierr);
	
	ierr = VecZeroEntries(properties_A1);CHKERRQ(ierr);
	ierr = VecZeroEntries(properties_A2);CHKERRQ(ierr);
	ierr = VecZeroEntries(properties_B);CHKERRQ(ierr);
	
	/* compute */
	ierr = _SwarmUpdateGaussPropertiesLocalL2Projection_MPntPStokes(
							clone, properties_A1,properties_A2,properties_B, 																																	 
							npoints, mp_std,mp_stokes, Q );CHKERRQ(ierr);
	
	/* view */
	view = PETSC_FALSE;
	PetscOptionsGetBool(PETSC_NULL,"-view_projected_marker_fields",&view,PETSC_NULL);
	if (view) {
		PetscViewer viewer;
		
		ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, "SwarmUpdateProperties_LocalL2Proj_Stokes.vtk", &viewer);CHKERRQ(ierr);
		ierr = PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
		ierr = DMView(clone, viewer);CHKERRQ(ierr);
		ierr = VecView(properties_A1, viewer);CHKERRQ(ierr);
		ierr = VecView(properties_A2, viewer);CHKERRQ(ierr);
		ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
	}
	
	
	/* destroy */
	ierr = DMRestoreGlobalVector(clone,&properties_B);CHKERRQ(ierr);
	ierr = DMRestoreGlobalVector(clone,&properties_A2);CHKERRQ(ierr);
	ierr = DMRestoreGlobalVector(clone,&properties_A1);CHKERRQ(ierr);
	
	ierr = DMDestroy(&clone);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "_SwarmUpdateGaussPropertiesLocalL2ProjectionQ1_MPntPStokes"
PetscErrorCode _SwarmUpdateGaussPropertiesLocalL2ProjectionQ1_MPntPStokes(
																																				DM clone,Vec properties_A1,Vec properties_A2,Vec properties_B,
																																				const int npoints,MPntStd mp_std[],MPntPStokes mp_stokes[],MaterialCoeffAvergeType average_type, QuadratureStokes Q) 
{
	PetscScalar Ni_p[Q2_NODES_PER_EL_2D];
	PetscScalar NiQ1_p[4];
	PetscScalar Ae1[Q2_NODES_PER_EL_2D], Ae2[Q2_NODES_PER_EL_2D], Be[Q2_NODES_PER_EL_2D];
	PetscInt el_lidx[U_BASIS_FUNCTIONS];
	Vec Lproperties_A1, Lproperties_A2, Lproperties_B;
	PetscScalar *LA_properties_A1, *LA_properties_A2, *LA_properties_B;
	PetscLogDouble t0,t1;
	PetscInt p,i;
	PetscInt nel,nen,e;
	const PetscInt *elnidx;
	
	PetscInt ngp;
	PetscScalar *xi_mp;
	PetscScalar NIu[MAX_QUAD_PNTS][U_BASIS_FUNCTIONS];
	GaussPointCoefficientsStokes *gausspoints;
	PetscScalar range_eta[2],range_rho[2];
	PetscErrorCode ierr;
	
	
	PetscFunctionBegin;
	
	
	ierr = DMGetLocalVector(clone,&Lproperties_A1);CHKERRQ(ierr);		ierr = VecZeroEntries(Lproperties_A1);CHKERRQ(ierr);
	ierr = DMGetLocalVector(clone,&Lproperties_A2);CHKERRQ(ierr);		ierr = VecZeroEntries(Lproperties_A2);CHKERRQ(ierr);
	ierr = DMGetLocalVector(clone,&Lproperties_B);CHKERRQ(ierr);		ierr = VecZeroEntries(Lproperties_B);CHKERRQ(ierr);
	
	ierr = VecGetArray(Lproperties_A1,&LA_properties_A1);CHKERRQ(ierr);
	ierr = VecGetArray(Lproperties_A2,&LA_properties_A2);CHKERRQ(ierr);
	ierr = VecGetArray(Lproperties_B, &LA_properties_B);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(clone,&nel,&nen,&elnidx);CHKERRQ(ierr);
	
	PetscGetTime(&t0);
	for (p=0; p<npoints; p++) {
		double *xi_p  = &mp_std[p].xi[0];
        double eta_p  = mp_stokes[p].eta;
        double rho_p  = mp_stokes[p].rho;
        
        
		ierr = PetscMemzero(Ae1,sizeof(PetscScalar)*Q2_NODES_PER_EL_2D);CHKERRQ(ierr);
		ierr = PetscMemzero(Ae2,sizeof(PetscScalar)*Q2_NODES_PER_EL_2D);CHKERRQ(ierr);
		ierr = PetscMemzero(Be, sizeof(PetscScalar)*Q2_NODES_PER_EL_2D);CHKERRQ(ierr);
		
		PTatinConstructNI_Q1_2D(xi_p,NiQ1_p);
		
		Ni_p[0] = NiQ1_p[0];
		Ni_p[2] = NiQ1_p[1];
		Ni_p[6] = NiQ1_p[2];
		Ni_p[8] = NiQ1_p[3];
		
		Ni_p[1] = Ni_p[7] = 1.0;
		Ni_p[3] = Ni_p[4] = Ni_p[5] = 1.0;
		
        switch (average_type) {
            case AVERAGE_TYPE_ARITHMETIC:
                for (i=0; i<Q2_NODES_PER_EL_2D; i++) {
                    Ae1[i] = Ni_p[i] * eta_p;
                    Ae2[i] = Ni_p[i] * rho_p;
                    Be[i]  = Ni_p[i];
                }
                break;

            case AVERAGE_TYPE_HARMONIC:
                for (i=0; i<Q2_NODES_PER_EL_2D; i++) {
                    Ae1[i] = Ni_p[i] * (1.0/eta_p);
                    Ae2[i] = Ni_p[i] * rho_p;
                    Be[i]  = Ni_p[i];
                }
                break;

            default:
                SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Average type must be ARITHMETIC or HARMONIC");
                break;
        }
		
		/* sum into local vectors */
		e = mp_std[p].wil;
		ierr = Q2GetElementLocalIndicesDOF(el_lidx,1,(PetscInt*)&elnidx[nen*e]);CHKERRQ(ierr);
		
		ierr = DMDASetValuesLocalStencil_AddValues_DOF(LA_properties_A1, 1, el_lidx,Ae1);CHKERRQ(ierr);
		ierr = DMDASetValuesLocalStencil_AddValues_DOF(LA_properties_A2, 1, el_lidx,Ae2);CHKERRQ(ierr);
		ierr = DMDASetValuesLocalStencil_AddValues_DOF(LA_properties_B,  1, el_lidx,Be);CHKERRQ(ierr);
		
	}
	PetscGetTime(&t1);
//	PetscPrintf(PETSC_COMM_WORLD,"  [ L2 projectionQ1 (summation): %1.4lf ]\n",t1-t0);
	
  ierr = VecRestoreArray(Lproperties_B,&LA_properties_B);CHKERRQ(ierr);
  ierr = VecRestoreArray(Lproperties_A2,&LA_properties_A2);CHKERRQ(ierr);
  ierr = VecRestoreArray(Lproperties_A1,&LA_properties_A1);CHKERRQ(ierr);

	
	/* scatter to quadrature points */
	ierr = DMLocalToGlobalBegin(clone,Lproperties_A1,ADD_VALUES,properties_A1);CHKERRQ(ierr);
	ierr = DMLocalToGlobalEnd(  clone,Lproperties_A1,ADD_VALUES,properties_A1);CHKERRQ(ierr);
	
	ierr = DMLocalToGlobalBegin(clone,Lproperties_A2,ADD_VALUES,properties_A2);CHKERRQ(ierr);
	ierr = DMLocalToGlobalEnd(  clone,Lproperties_A2,ADD_VALUES,properties_A2);CHKERRQ(ierr);
	
	ierr = DMLocalToGlobalBegin(clone,Lproperties_B,ADD_VALUES,properties_B);CHKERRQ(ierr);
	ierr = DMLocalToGlobalEnd(  clone,Lproperties_B,ADD_VALUES,properties_B);CHKERRQ(ierr);
	
	/* scale */
	ierr = VecPointwiseDivide( properties_A1, properties_A1, properties_B );CHKERRQ(ierr);
	ierr = VecPointwiseDivide( properties_A2, properties_A2, properties_B );CHKERRQ(ierr);

	/*
     Two choices are possible for harmonic averaging:
     We have projected 1/eta to the nodes, we can either
     a) take reciporcal of the nodal 1/eta and interpolate the result to the quad points, or
     b) interpolate 1/eta to the quad points AND THEN take the reciporcal.
     Here we use option a)
     */
    switch (average_type) {
        case AVERAGE_TYPE_HARMONIC:
            ierr = VecReciprocal(properties_A1);CHKERRQ(ierr);
            break;
    }
    /* ========================================= */
	
	/* scatter result back to local array and do the interpolation onto the quadrature points */
	ngp       = Q->ngp;
	xi_mp     = Q->xi;
	for (p=0; p<ngp; p++) {
		PetscScalar *xip = &xi_mp[2*p];
		
		ierr = PetscMemzero(NIu[p], sizeof(PetscScalar)*Q2_NODES_PER_EL_2D);CHKERRQ(ierr);

		PTatinConstructNI_Q1_2D(xip,NiQ1_p);
		NIu[p][0] = NiQ1_p[0];
		NIu[p][2] = NiQ1_p[1];
		NIu[p][6] = NiQ1_p[2];
		NIu[p][8] = NiQ1_p[3];
	}
	
	
	PetscGetTime(&t0);
	ierr = VecZeroEntries(Lproperties_A1);CHKERRQ(ierr);
	ierr = VecZeroEntries(Lproperties_A2);CHKERRQ(ierr);
	
	ierr = DMGlobalToLocalBegin(clone,properties_A1,INSERT_VALUES,Lproperties_A1);CHKERRQ(ierr);
	ierr = DMGlobalToLocalEnd(  clone,properties_A1,INSERT_VALUES,Lproperties_A1);CHKERRQ(ierr);
	
	ierr = DMGlobalToLocalBegin(clone,properties_A2,INSERT_VALUES,Lproperties_A2);CHKERRQ(ierr);
	ierr = DMGlobalToLocalEnd(  clone,properties_A2,INSERT_VALUES,Lproperties_A2);CHKERRQ(ierr);
	PetscGetTime(&t1);
//	PetscPrintf(PETSC_COMM_WORLD,"  [ L2 projectionQ1 (scatter): %1.4lf ]\n",t1-t0);
	
	PetscGetTime(&t0);
	ierr = VecGetArray(Lproperties_A1,&LA_properties_A1);CHKERRQ(ierr);
	ierr = VecGetArray(Lproperties_A2,&LA_properties_A2);CHKERRQ(ierr);
	
	/* traverse elements and interpolate */
	for (e=0;e<nel;e++) {
		ierr = QuadratureStokesGetCell(Q,e,&gausspoints);CHKERRQ(ierr);
		
		ierr = Q2GetElementLocalIndicesDOF(el_lidx,1,(PetscInt*)&elnidx[nen*e]);CHKERRQ(ierr);
		
		ierr = DMDAGetScalarElementField_2D(Ae1,nen,(PetscInt*)&elnidx[nen*e],LA_properties_A1);CHKERRQ(ierr);
		ierr = DMDAGetScalarElementField_2D(Ae2,nen,(PetscInt*)&elnidx[nen*e],LA_properties_A2);CHKERRQ(ierr);

		/* zero out the junk */
		Ae1[1] = Ae1[7] = 0.0;
		Ae1[3] = Ae1[4] = Ae1[5] = 0.0;

		Ae2[1] = Ae2[7] = 0.0;
		Ae2[3] = Ae2[4] = Ae2[5] = 0.0;
		
		
		/* The Q2 interpolant tends to overshoot / undershoot when you have viscosity jumps */
		range_eta[0] = 1.0e32;  /* min */
		range_eta[1] = -1.0e32; /* max */
		range_rho[0] = 1.0e32;
		range_rho[1] = -1.0e32;
		for (i=0; i<Q2_NODES_PER_EL_2D; i++) {
			if (Ae1[i]<range_eta[0]) { range_eta[0] = Ae1[i]; }
			if (Ae1[i]>range_eta[1]) { range_eta[1] = Ae1[i]; }
			if (Ae2[i]<range_rho[0]) { range_rho[0] = Ae2[i]; }
			if (Ae2[i]>range_rho[1]) { range_rho[1] = Ae2[i]; }
		}
		
		for (p=0; p<ngp; p++) {
			gausspoints[p].eta = 0.0;
			gausspoints[p].Fu[0] = 0.0;
			gausspoints[p].Fu[1] = 0.0;
            gausspoints[p].stress_old[0] = 0.0;
            gausspoints[p].stress_old[1] = 0.0;
            gausspoints[p].stress_old[2] = 0.0;
			for (i=0; i<Q2_NODES_PER_EL_2D; i++) {
				gausspoints[p].eta    += NIu[p][i] * Ae1[i];
				gausspoints[p].Fu[0]  += NIu[p][i] * Ae2[i];
			}
			if (gausspoints[p].eta < range_eta[0]) { gausspoints[p].eta = range_eta[0]; }
			if (gausspoints[p].eta > range_eta[1]) { gausspoints[p].eta = range_eta[1]; }
			if (gausspoints[p].Fu[0] < range_rho[0]) { gausspoints[p].Fu[0] = range_rho[0]; }
			if (gausspoints[p].Fu[0] > range_rho[1]) { gausspoints[p].Fu[0] = range_rho[1]; }
			
			/*
			 switch (average_type) {
			 case AVERAGE_TYPE_HARMONIC:
			 gausspoints[p].eta = 1.0 / gausspoints[p].eta;
			 break;
			 }
			 */
			gausspoints[p].Fu[1] = gausspoints[p].Fu[0];

			gausspoints[p].Fu[0] = gausspoints[p].Fu[0] * gausspoints[p].gravity[0];
			gausspoints[p].Fu[1] = gausspoints[p].Fu[1] * gausspoints[p].gravity[1];
			
			
		}
		
	}
	
  ierr = VecRestoreArray(Lproperties_A2,&LA_properties_A2);CHKERRQ(ierr);
  ierr = VecRestoreArray(Lproperties_A1,&LA_properties_A1);CHKERRQ(ierr);
	
	PetscGetTime(&t1);
//	PetscPrintf(PETSC_COMM_WORLD,"  [ L2 projectionQ1 (interpolation): %1.4lf ]\n",t1-t0);
	
	ierr = DMRestoreLocalVector(clone,&Lproperties_B);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(clone,&Lproperties_A2);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(clone,&Lproperties_A1);CHKERRQ(ierr);
	
	
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SwarmUpdateGaussPropertiesLocalL2Projection_Q1_MPntPStokes"
PetscErrorCode SwarmUpdateGaussPropertiesLocalL2Projection_Q1_MPntPStokes(const int npoints,MPntStd mp_std[],MPntPStokes mp_stokes[],MaterialCoeffAvergeType average_type,DM da,QuadratureStokes Q)
{
	PetscInt  dof;
	DM        clone;
	Vec       properties_A1, properties_A2, properties_B;
	PetscBool view;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	/* setup */
	dof = 1;
	ierr = DMDADuplicateLayout(da,dof,2,DMDA_STENCIL_BOX,&clone);CHKERRQ(ierr); /* Q2 - but we'll fake it as a Q1 with cells the same size as the Q2 guys */
	
	ierr = DMGetGlobalVector(clone,&properties_A1);CHKERRQ(ierr);  ierr = PetscObjectSetName( (PetscObject)properties_A1, "LocalL2ProjQ1_nu");CHKERRQ(ierr);
	ierr = DMGetGlobalVector(clone,&properties_A2);CHKERRQ(ierr);  ierr = PetscObjectSetName( (PetscObject)properties_A2, "LocalL2ProjQ1_rho");CHKERRQ(ierr);
	ierr = DMGetGlobalVector(clone,&properties_B);CHKERRQ(ierr);
	
	ierr = VecZeroEntries(properties_A1);CHKERRQ(ierr);
	ierr = VecZeroEntries(properties_A2);CHKERRQ(ierr);
	ierr = VecZeroEntries(properties_B);CHKERRQ(ierr);
	
	/* compute */
	ierr = _SwarmUpdateGaussPropertiesLocalL2ProjectionQ1_MPntPStokes(
																																	clone, properties_A1,properties_A2,properties_B, 																																	 
                                                                      npoints, mp_std,mp_stokes,average_type, Q );CHKERRQ(ierr);
	
	/* view */
	view = PETSC_FALSE;
	PetscOptionsGetBool(PETSC_NULL,"-view_projected_marker_fields",&view,PETSC_NULL);
	if (view) {
		PetscViewer viewer;
		
		ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, "SwarmUpdateProperties_LocalL2Proj_Stokes.vtk", &viewer);CHKERRQ(ierr);
		ierr = PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
		ierr = DMView(clone, viewer);CHKERRQ(ierr);
		ierr = VecView(properties_A1, viewer);CHKERRQ(ierr);
		ierr = VecView(properties_A2, viewer);CHKERRQ(ierr);
		ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
	}
	
	
	/* destroy */
	ierr = DMRestoreGlobalVector(clone,&properties_B);CHKERRQ(ierr);
	ierr = DMRestoreGlobalVector(clone,&properties_A2);CHKERRQ(ierr);
	ierr = DMRestoreGlobalVector(clone,&properties_A1);CHKERRQ(ierr);
	
	ierr = DMDestroy(&clone);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SwarmUpdateGaussPropertiesLocalL2Projection_P0_MPntPStokes"
PetscErrorCode SwarmUpdateGaussPropertiesLocalL2Projection_P0_MPntPStokes(const int npoints,MPntStd mp_std[],MPntPStokes mp_stokes[],MaterialCoeffAvergeType average_type,DM da,QuadratureStokes Q)
{
	PetscInt p,i,ngp;
	PetscInt nel,nen,e;
	const PetscInt *elnidx;
	int *el_pcount;
	double *el_avg_eta, *el_avg_rho;
	GaussPointCoefficientsStokes *gausspoints;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ierr = DMDAGetElements_pTatin(da,&nel,&nen,&elnidx);CHKERRQ(ierr);
	
	ierr = PetscMalloc( sizeof(int)*nel, &el_pcount );CHKERRQ(ierr);
	ierr = PetscMalloc( sizeof(double)*nel, &el_avg_eta );CHKERRQ(ierr);
	ierr = PetscMalloc( sizeof(double)*nel, &el_avg_rho );CHKERRQ(ierr);

	/* init */
	for (e=0; e<nel; e++) {
		el_pcount[e]  = 0;
		el_avg_eta[e] = 0.0;
		el_avg_rho[e] = 0.0;
	}

    
    switch (average_type) {
        case AVERAGE_TYPE_ARITHMETIC:
            
            for (p=0; p<npoints; p++) {
                double eta_p  = mp_stokes[p].eta;
                double rho_p  = mp_stokes[p].rho;
                
                e = mp_std[p].wil;
                if (e >= nel) {
                    SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"marker->wil >= num local elements");
                }
                
                el_avg_eta[e] = el_avg_eta[e] + eta_p;
                el_avg_rho[e] = el_avg_rho[e] + rho_p;
                
                el_pcount[e]++;
            }
            
            break;
            
        case AVERAGE_TYPE_HARMONIC:
            
            for (p=0; p<npoints; p++) {
                double eta_p  = mp_stokes[p].eta;
                double rho_p  = mp_stokes[p].rho;
                
                e = mp_std[p].wil;
                if (e >= nel) {
                    SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"marker->wil >= num local elements");
                }
                
                el_avg_eta[e] = el_avg_eta[e] + 1.0/eta_p;
                el_avg_rho[e] = el_avg_rho[e] + rho_p;
                
                el_pcount[e]++;
            }
            
            break;
            
            case AVERAGE_TYPE_GEOMETRIC:
            
            for (p=0; p<npoints; p++) {
                double eta_p  = mp_stokes[p].eta;
                double rho_p  = mp_stokes[p].rho;
                
                e = mp_std[p].wil;
                if (e >= nel) {
                    SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"marker->wil >= num local elements");
                }
                
                el_avg_eta[e] = el_avg_eta[e] + log(eta_p);
                el_avg_rho[e] = el_avg_rho[e] + rho_p;
                
                el_pcount[e]++;
            }
            break;
        default:
            SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Average type must be ARITHMETIC or HARMONIC");
            break;
    }
    
    
	for (e=0; e<nel; e++) {
		if (el_pcount[e]==0) {
			SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Cell doesn't contain any material points");
		}
		
		el_avg_eta[e] = el_avg_eta[e] / (double)(el_pcount[e]);
		el_avg_rho[e] = el_avg_rho[e] / (double)(el_pcount[e]);
	}
	
	
	/* traverse elements and interpolate */
	ngp = Q->ngp;
	for (e=0;e<nel;e++) {
		ierr = QuadratureStokesGetCell(Q,e,&gausspoints);CHKERRQ(ierr);
        
        switch (average_type) {
            case AVERAGE_TYPE_ARITHMETIC:
                for (p=0; p<ngp; p++) {
                    gausspoints[p].eta   = el_avg_eta[e];
                    gausspoints[p].Fu[0] = el_avg_rho[e] * gausspoints[p].gravity[0];
                    gausspoints[p].Fu[1] = el_avg_rho[e] * gausspoints[p].gravity[1];
                    gausspoints[p].stress_old[0] = 0.0;
                    gausspoints[p].stress_old[1] = 0.0;
                    gausspoints[p].stress_old[2] = 0.0;
                }
                break;
                
            case AVERAGE_TYPE_HARMONIC:
                for (p=0; p<ngp; p++) {
                    gausspoints[p].eta   = 1.0/el_avg_eta[e];
                    gausspoints[p].Fu[0] = el_avg_rho[e] * gausspoints[p].gravity[0];
                    gausspoints[p].Fu[1] = el_avg_rho[e] * gausspoints[p].gravity[1];
                    gausspoints[p].stress_old[0] = 0.0;
                    gausspoints[p].stress_old[1] = 0.0;
                    gausspoints[p].stress_old[2] = 0.0;
                }
                break;
            case AVERAGE_TYPE_GEOMETRIC:
                for (p=0; p<ngp; p++) {
                    gausspoints[p].eta   = exp(el_avg_eta[e]);
                    gausspoints[p].Fu[0] = el_avg_rho[e] * gausspoints[p].gravity[0];
                    gausspoints[p].Fu[1] = el_avg_rho[e] * gausspoints[p].gravity[1];
                    gausspoints[p].stress_old[0] = 0.0;
                    gausspoints[p].stress_old[1] = 0.0;
                    gausspoints[p].stress_old[2] = 0.0;
                }
                break;   
            default:
                SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Average type must be ARITHMETIC or HARMONIC");
                break;
        }
	}
	
	ierr = PetscFree(el_pcount);CHKERRQ(ierr);
	ierr = PetscFree(el_avg_eta);CHKERRQ(ierr);
	ierr = PetscFree(el_avg_rho);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadraturePointProjection_LocalL2Projection_P0_MPntPStokes"
PetscErrorCode QuadraturePointProjection_LocalL2Projection_P0_MPntPStokes(
									const int npoints,MPntStd mp_std[],MPntPStokes mp_stokes[],MaterialCoeffAvergeType average_type,DM da,
									QuadratureStokes Q,SurfaceQuadratureStokes surfQlist[])
{
	PetscInt p,i,ngp;
	PetscInt nel,nen,e;
	const PetscInt *elnidx;
	int *el_pcount;
	double *el_avg_eta, *el_avg_rho;
	GaussPointCoefficientsStokes    *quadpoints;
	SurfaceQPointCoefficientsStokes *surf_quadpoints;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ierr = DMDAGetElements_pTatin(da,&nel,&nen,&elnidx);CHKERRQ(ierr);
	
	ierr = PetscMalloc( sizeof(int)*nel, &el_pcount );CHKERRQ(ierr);
	ierr = PetscMalloc( sizeof(double)*nel, &el_avg_eta );CHKERRQ(ierr);
	ierr = PetscMalloc( sizeof(double)*nel, &el_avg_rho );CHKERRQ(ierr);
	
	/* init */
	for (e=0; e<nel; e++) {
		el_pcount[e]  = 0;
		el_avg_eta[e] = 0.0;
		el_avg_rho[e] = 0.0;
	}
	
	
	switch (average_type) {
		case AVERAGE_TYPE_ARITHMETIC:
			
			for (p=0; p<npoints; p++) {
				double eta_p  = mp_stokes[p].eta;
				double rho_p  = mp_stokes[p].rho;
				
				e = mp_std[p].wil;
				if (e >= nel) {
					SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"marker->wil >= num local elements");
				}
				
				el_avg_eta[e] = el_avg_eta[e] + eta_p;
				el_avg_rho[e] = el_avg_rho[e] + rho_p;
				
				el_pcount[e]++;
			}
			
			break;
			
		case AVERAGE_TYPE_HARMONIC:
			
			for (p=0; p<npoints; p++) {
				double eta_p  = mp_stokes[p].eta;
				double rho_p  = mp_stokes[p].rho;
				
				e = mp_std[p].wil;
				if (e >= nel) {
					SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"marker->wil >= num local elements");
				}
				
				el_avg_eta[e] = el_avg_eta[e] + 1.0/eta_p;
				el_avg_rho[e] = el_avg_rho[e] + rho_p;
				
				el_pcount[e]++;
			}
			
			break;
		 case AVERAGE_TYPE_GEOMETRIC:
            
			for (p=0; p<npoints; p++) {
				double eta_p  = mp_stokes[p].eta;
				double rho_p  = mp_stokes[p].rho;
				
				e = mp_std[p].wil;
				if (e >= nel) {
					SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"marker->wil >= num local elements");
				}
				
				el_avg_eta[e] = el_avg_eta[e] + log(eta_p);
				el_avg_rho[e] = el_avg_rho[e] + rho_p;
				
				el_pcount[e]++;
			}	
			break;
		default:
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Average type must be ARITHMETIC or HARMONIC");
			break;
	}
	
	
	for (e=0; e<nel; e++) {
		if (el_pcount[e]==0) {
			SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Cell doesn't contain any material points");
		}
		
		el_avg_eta[e] = el_avg_eta[e] / (double)(el_pcount[e]);
		el_avg_rho[e] = el_avg_rho[e] / (double)(el_pcount[e]);
	}
	
	
	/* traverse elements and interpolate */
	/* volume quadrature */
	if (Q) {
		ngp = Q->ngp;
		for (e=0;e<nel;e++) {
			ierr = QuadratureStokesGetCell(Q,e,&quadpoints);CHKERRQ(ierr);
			
			switch (average_type) {
				case AVERAGE_TYPE_ARITHMETIC:
					for (p=0; p<ngp; p++) {
						quadpoints[p].eta   = el_avg_eta[e];
						quadpoints[p].Fu[0] = el_avg_rho[e] * quadpoints[p].gravity[0];
						quadpoints[p].Fu[1] = el_avg_rho[e] * quadpoints[p].gravity[1];
					}
					break;
					
				case AVERAGE_TYPE_HARMONIC:
					for (p=0; p<ngp; p++) {
						quadpoints[p].eta   = 1.0/el_avg_eta[e];
						quadpoints[p].Fu[0] = el_avg_rho[e] * quadpoints[p].gravity[0];
						quadpoints[p].Fu[1] = el_avg_rho[e] * quadpoints[p].gravity[1];
					}
					break;
				case AVERAGE_TYPE_GEOMETRIC:
                for (p=0; p<ngp; p++) {
                        quadpoints[p].eta   = exp(el_avg_eta[e]);
						quadpoints[p].Fu[0] = el_avg_rho[e] * quadpoints[p].gravity[0];
						quadpoints[p].Fu[1] = el_avg_rho[e] * quadpoints[p].gravity[1];
                }
                break;  
				default:
					SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Average type must be ARITHMETIC or HARMONIC");
					break;
			}
		}
	}
	/* surface quadrature */
	if (surfQlist) {
		PetscInt fe,vol_e_idx,face;
		SurfaceQuadratureStokes surfQ;

		for (face=0; face<QUAD_EDGES; face++) {
			surfQ = surfQlist[face];
			ngp = surfQ->ngp;
			
			for (fe=0; fe<surfQ->nfaces; fe++) {
				vol_e_idx = surfQ->cell_list[fe];
				
				ierr = SurfaceQuadratureStokesGetCell(surfQ,fe,&surf_quadpoints);CHKERRQ(ierr);
				
				switch (average_type) {
					case AVERAGE_TYPE_ARITHMETIC:
						for (p=0; p<ngp; p++) {
							surf_quadpoints[p].effective_eta = el_avg_eta[vol_e_idx];
							surf_quadpoints[p].effective_rho = el_avg_rho[vol_e_idx];
						}
						break;
						
					case AVERAGE_TYPE_HARMONIC:
						for (p=0; p<ngp; p++) {
							surf_quadpoints[p].effective_eta = 1.0/el_avg_eta[vol_e_idx];
							surf_quadpoints[p].effective_rho = el_avg_rho[vol_e_idx];
						}
						break;
					case AVERAGE_TYPE_GEOMETRIC:
						for (p=0; p<ngp; p++) {
							surf_quadpoints[p].effective_eta = exp(el_avg_eta[vol_e_idx]);
							surf_quadpoints[p].effective_rho = el_avg_rho[vol_e_idx];
						}
						break;	
					default:
						SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Average type must be ARITHMETIC or HARMONIC");
						break;
				}
			}
			
		}
	}
	
	
	
	ierr = PetscFree(el_pcount);CHKERRQ(ierr);
	ierr = PetscFree(el_avg_eta);CHKERRQ(ierr);
	ierr = PetscFree(el_avg_rho);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "SwarmUpdatePosition_Communication_MPntPStokes"
PetscErrorCode SwarmUpdatePosition_Communication_MPntPStokes(DM da,DataBucket db,DataEx de)
{
	DataField            PField,PFieldStokes;
	PetscInt p,npoints,npoints_global_init,npoints_global_fin;
	void     *recv_data;
	void     *data_p;
	int n,neighborcount, *neighborranks2;
	int recv_length,npoints_accepted;
	PetscMPIInt rank,size;
	MPntStd     *marker_std;
	MPntPStokes *marker_stokes;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	/* communucate */
	MPI_Comm_size(((PetscObject)da)->comm,&size);
	if (size==1) {
		PetscFunctionReturn(0);
	}
	
	MPI_Comm_rank(((PetscObject)da)->comm,&rank);
	
	neighborcount  = de->n_neighbour_procs;
	neighborranks2 = de->neighbour_procs;
	
	
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField);
	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PFieldStokes);

	DataFieldGetAccess(PField);
	DataFieldVerifyAccess( PField,sizeof(MPntStd));
	DataBucketGetSizes(db,&npoints,0,0);
	
	MPI_Allreduce(&npoints,&npoints_global_init,1,MPI_INT,MPI_SUM,de->comm);
	
	/* figure out how many points left processor */
	ierr = DataExInitializeSendCount(de);CHKERRQ(ierr);
	for (p=0; p<npoints; p++) {
		PetscBool onproc;
		MPntStd   *marker;
		
		DataFieldAccessPoint(PField,p,(void**)&marker);
		onproc = PETSC_TRUE;
		if (marker->wil==-1) {
			onproc = PETSC_FALSE;
		}
		
		if (onproc==PETSC_FALSE) {
			for (n=0; n<neighborcount; n++) {
				//	printf("  DataEx: rank %d sending %d to %d \n",rank,1,neighborranks2[n] );
				ierr = DataExAddToSendCount( de, neighborranks2[n], 1 );CHKERRQ(ierr);
			}
		}
	}
	ierr = DataExFinalizeSendCount(de);CHKERRQ(ierr);
	
	DataFieldRestoreAccess(PField);
	
	/* pack points which left processor */
	marker_std    = PField->data; /* should write a function to do this */
	marker_stokes = PFieldStokes->data; /* should write a function to do this */
	
	ierr = DataExPackInitialize(de,sizeof(MPntStd)+sizeof(MPntPStokes));CHKERRQ(ierr);

	/* allocate a temporary buffer whihc is large enough to store all marker fields from an individual point,p */
	ierr = PetscMalloc(sizeof(MPntStd)+sizeof(MPntPStokes),&data_p);CHKERRQ(ierr);
	
	for (p=0; p<npoints; p++) {
		PetscBool onproc;
		MPntStd     *marker_p;
		MPntPStokes *marker_stk_p;
		
		/* access fields from the bucket */
		marker_p     = &marker_std[p];
		marker_stk_p = &marker_stokes[p];
		
		onproc = PETSC_TRUE;
		if (marker_p->wil==-1) {
			onproc = PETSC_FALSE;
			/* pack together */
			//(void*)((char*)field->data + index*field->atomic_size)
			
			/* copy all fields from the bucket into a temporary buffer */
			PetscMemcpy((void*)data_p,                         marker_p,    sizeof(MPntStd));
			PetscMemcpy((void*)((char*)data_p+sizeof(MPntStd)),marker_stk_p,sizeof(MPntPStokes));
		}
		
		if (onproc==PETSC_FALSE) {
			for (n=0; n<neighborcount; n++) {
				ierr = DataExPackData( de, neighborranks2[n], 1,(void*)data_p );CHKERRQ(ierr);
			}
		}
	}		
	ierr = DataExPackFinalize(de);CHKERRQ(ierr);
	
	/* remove points which left processor */
	DataBucketGetSizes(db,&npoints,0,0);
	DataFieldGetAccess(PField);
	for (p=0; p<npoints; p++) {
		PetscBool onproc;
		MPntStd   *marker_p;
		
		DataFieldAccessPoint(PField,p,(void**)&marker_p);
		onproc = PETSC_TRUE;
		if (marker_p->wil==-1) {
			onproc = PETSC_FALSE;
		}
		
		if (onproc==PETSC_FALSE) { 
			/* kill point */
			DataBucketRemovePointAtIndex(db,p);
			DataBucketGetSizes(db,&npoints,0,0); /* you need to update npoints as the list size decreases! */
			p--; /* check replacement point */
		}
	}		
	DataFieldRestoreAccess(PField);
	
	// START communicate //
	ierr = DataExBegin(de);CHKERRQ(ierr);
	ierr = DataExEnd(de);CHKERRQ(ierr);
	// END communicate //
	
	// receive, if i own them, add new points to list //
	ierr = DataExGetRecvData( de, &recv_length, (void**)&recv_data );CHKERRQ(ierr);
	{
		int totalsent;
		MPI_Allreduce(&recv_length,&totalsent,1,MPI_INT,MPI_SUM,de->comm);
		PetscPrintf(PETSC_COMM_WORLD,"  DataEx: total points sent = %d \n", totalsent);
	}
	
	
	
	/* update the local coordinates and cell owner for all recieved points */
	{
		DM cda;
		Vec gcoords;
		PetscScalar *LA_gcoords;
		double tolerance;
		int max_its;
		Truth use_nonzero_guess, monitor, log;
		PetscInt lmx,lmy;
		PetscInt nel,nen_u;
		MPntStd *marker_p;
		const PetscInt *elnidx_u;
		
		/* setup for coords */
		ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
		ierr = DMDAGetGhostedCoordinates(da,&gcoords);CHKERRQ(ierr);
		ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
		
		ierr = DMDAGetElements_pTatin(da,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
		
		ierr = DMDAGetLocalSizeElementQ2(da,&lmx,&lmy,PETSC_NULL);CHKERRQ(ierr);
		
		/* point location parameters */
		tolerance         = 1.0e-10;
		max_its           = 10;
		use_nonzero_guess = _FALSE; /* for markers sent across processors, it is necessary to NOT use the last known values! */
		monitor           = _FALSE;
		log               = _FALSE;
		
		/* 
		 NOTE: My algorithm will break down if you try to use an non-zero initial guess
		 if the marker has been sent to another cpu. This could be fixed, however if would
		 require additional logic to be added into the function InverseMappingDomain_2dQ2()
		 
		 It seems more reasonable to simply assume that if the marker was sent to another cpu,
		 the value of wil currently stored on the marker is completely meaningless and thus we
		 should ALWAYS use a zero initial guess (i.e. assume we know nothing)
		*/
		
		for (p=0; p<recv_length; p++) {
			marker_p = (MPntStd*)( (char*)recv_data + p*(sizeof(MPntStd)+sizeof(MPntPStokes)) );
			//printf("revc p=%d : (%lf,%lf) \n", p, marker_p->coor[0], marker_p->coor[1] );
			
			InverseMappingDomain_2dQ2( 		 tolerance, max_its,
																use_nonzero_guess, 
																monitor, log,
																(const double*)LA_gcoords, (const int)lmx,(const int)lmy, (const int*)elnidx_u,
																1, marker_p );
			//printf("++[%d] revc p=%d : wil=%d \n", rank,p, marker_p->wil );
		}
		
		ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	}
	
	/* accepte all points living locally */
	npoints_accepted = 0;
	for (p=0; p<recv_length; p++) {
		PetscBool onproc;
		MPntStd     *marker_p;
		MPntPStokes *marker_stk_p;
		
		marker_p     = (MPntStd*)(     (char*)recv_data + p*(sizeof(MPntStd)+sizeof(MPntPStokes)) );
		marker_stk_p = (MPntPStokes*)( (char*)recv_data + p*(sizeof(MPntStd)+sizeof(MPntPStokes)) + sizeof(MPntStd) );

		//printf("++[%d] revc p=%d : wil=%d [eta,rho] = (%lf,%lf) \n", rank,p, marker_p->wil, marker_stk_p->eta, marker_stk_p->rho );
		
		onproc = PETSC_TRUE;
		if (marker_p->wil==-1) {
			onproc = PETSC_FALSE;
		}
		
		if (onproc == PETSC_TRUE) {
			int end;
			
			DataBucketAddPoint(db);
			DataBucketGetSizes(db,&end,0,0);
			end = end - 1;

			DataFieldInsertPoint(PField,       end, (void*)marker_p );
			DataFieldInsertPoint(PFieldStokes, end, (void*)marker_stk_p );
		
			npoints_accepted++;
		}
	}	
	//printf("  DataEx: rank %d accepted %d new points \n",rank,npoints_accepted );
	
	
	DataBucketGetSizes(db,&npoints,0,0);
	MPI_Allreduce(&npoints,&npoints_global_fin,1,MPI_INT,MPI_SUM,de->comm);
	PetscPrintf(PETSC_COMM_WORLD,"  SwarmUpdatePosition(Communication): num. points global ( init. = %d : final = %d )\n", npoints_global_init,npoints_global_fin);
	ierr = PetscFree(data_p);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "SwarmUpdateProperties_MPntPStokes"
PetscErrorCode SwarmUpdateProperties_MPntPStokes(DataBucket db,pTatinCtx ctx,Vec X)
{
	BTruth found;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	
	DataBucketQueryDataFieldByName(db,MPntPStokes_classname,&found);
	if(found==BFALSE) {
		SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot find DataField with name %s \n", MPntPStokes_classname );
	}
	
	
	
	PetscFunctionReturn(0);
}
