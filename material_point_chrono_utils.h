
#ifndef __MATERIAL_POINT_CHRONO_UTILS_H__
#define __MATERIAL_POINT_CHRONO_UTILS_H__

#include "swarm_fields.h"
#include "data_exchanger.h"
#include "MPntStd_def.h"
#include "MPntPChrono_def.h"


PetscErrorCode SwarmUpdateProperties_MPntPChrono(DataBucket db,pTatinCtx user,Vec X);
PetscErrorCode pTatin_TkUpdateMarker_Chrono(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[]);
PetscErrorCode pTatin2d_Update_local_coordinates_Markers(DM dav, DataBucket db, DataEx ex);

#endif