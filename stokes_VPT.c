//
//  stokes_VPT.c
//  
//
//  Created by le pourhiet laetitia on 6/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#include <stdio.h>
#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"
#include "rheology.h"
#include "stokes_VPT.h"
#include "element_type_Q1.h"

typedef enum { YTYPE_NONE=0, YTYPE_MISES=1, YTYPE_DP=2, YTYPE_TENSILE_FAILURE=3,YTYPE_DIFF=4,YTYPE_DISC=5,YTYPE_DISG} YieldTypeDefinition;

static inline void ComputeStressIsotropic2d(PetscReal eta,double D[2][2],double T[2][2])
{
	
}
static inline void ComputeStrainRate2d(double nx[],double ny[],double D[2][2])
{
	
	
}
static inline void ComputeDeformationGradient2d(double nx[],double ny[],double L[2][2])
{
	
	
}
static inline void ComputeSecondInvariant2d(double A[2][2],double *A2)
{
	
	
}
static inline void ComputeAverageTrace2d(double A[2][2],double *A2)
{
	
	
}

/*
 In this function, all properties marked with mp are scaled, other properties are in SI units.
 */
#undef __FUNCT__
#define __FUNCT__ "StokesEvaluateRheologyNonLinearitiesQuadraturePoints_VPT"
PetscErrorCode StokesEvaluateRheologyNonLinearitiesQuadraturePoints_VPT(pTatinCtx user,DM dau,PetscScalar LA_u[],DM dap,PetscScalar LA_p[])
{	
	PetscErrorCode ierr;
	
	PetscInt e,ncells;
	PetscInt p,ngp;
	GaussPointCoefficientsStokes *gausspoints;
	PetscScalar *gp_xi;
    
	PetscScalar GNIu[2][Q2_NODES_PER_EL_2D],NIp[P_BASIS_FUNCTIONS];
	PetscScalar NIu[U_BASIS_FUNCTIONS];
	PetscScalar NIT[NODES_PER_EL_Q1_2D];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen_u,nen_p,nen_T,i,j,k;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	const PetscInt *elnidx_T;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	PetscScalar elT[NODES_PER_EL_Q1_2D];
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	PetscLogDouble t0,t1;
	PetscScalar min_eta,max_eta,min_eta_g,max_eta_g;
	PetscScalar min_rho,max_rho,min_rho_g,max_rho_g;
	PetscInt npoints_yielded_g, npoints_yielded_tens_g,npoints_yielded,npoints_yielded_tens;
	PetscScalar eta_lower_cutoff_global_mp,eta_upper_cutoff_global_mp;
	PetscBool         active_thermal_energy = PETSC_FALSE;    
	PhysCompEnergyCtx phys_energy;
	Vec               T_vec,T_local_vec;
	DM                daT;
	PetscScalar       *LA_T;
	pTatinUnits       *units;	
	RheologyConstants *rheology;
	
	PetscFunctionBegin;
	
	/* fetch constants */
	units    = &user->units;
	rheology = &user->rheology_constants;
    
	/* stokes */
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
    
	/* setup for coords */
	ierr = DMDAGetCoordinateDA(dau,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(dau,&gcoords);CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
    
	/* energy */
	active_thermal_energy = PETSC_FALSE;
	ierr = pTatinPhysCompActivated(user,PhysComp_Energy,&active_thermal_energy);CHKERRQ(ierr);
	if (active_thermal_energy) {
		phys_energy = user->phys_energy;
		T_vec       = phys_energy->T;
		daT         = phys_energy->daT;
		
		ierr = DMGetLocalVector(daT,&T_local_vec);CHKERRQ(ierr);		
		ierr = DMGlobalToLocalBegin(daT,T_vec,INSERT_VALUES,T_local_vec);CHKERRQ(ierr);
		ierr = DMGlobalToLocalEnd(  daT,T_vec,INSERT_VALUES,T_local_vec);CHKERRQ(ierr);
		
		ierr = VecGetArray(T_local_vec,&LA_T);CHKERRQ(ierr);
		ierr = DMDAGetElementsQ1(daT,&nel,&nen_T,&elnidx_T);CHKERRQ(ierr);
	}
	
	UnitsApplyInverseScaling(units->si_viscosity,rheology->eta_upper_cutoff_global,&eta_upper_cutoff_global_mp);
	UnitsApplyInverseScaling(units->si_viscosity,rheology->eta_lower_cutoff_global,&eta_lower_cutoff_global_mp);
    
	npoints_yielded = 0;    
	min_eta = 1.0e100;
	max_eta = 1.0e-100;
	min_rho = 1.0e100;
	max_rho = 1.0e-100;
	
	/* quadrature point loop */
	ncells = user->Q->ncells;
	ngp    = user->Q->ngp;
	gp_xi  = user->Q->xi;
	
	PetscGetTime(&t0);
	for (e=0; e<ncells; e++) {
		char          is_yielding, yield_type;        
		double        position[2], *xip;
		int           phase_mp;
		PetscScalar   J[2][2], iJ[2][2];
		PetscScalar   J_p,ojp;
		PetscScalar   d1,exx_mp,eyy_mp,exy_mp,eta_mp,strainrateinv_mp,tau_mp;
		PetscScalar   sxx_mp,syy_mp,sxy_mp,u_mp,v_mp,trace_mp,pressure_mp;
		PetscScalar   T_mp,rho_mp,rho;	
		PetscScalar   alpha_mp;
		
		/* get element coordinates */
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		/* get element coordinates in component form */
		for (i=0; i<Q2_NODES_PER_EL_2D; i++) {
			xc[i] = elcoords[2*i  ];
			yc[i] = elcoords[2*i+1];
		}
        
		/* get element velocity */
		ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],LA_u);CHKERRQ(ierr);
		for (i=0; i<Q2_NODES_PER_EL_2D; i++) {
			ux[i] = elu[2*i  ];
			uy[i] = elu[2*i+1];
		}
		
		/* get element pressure */
		ierr = DMDAGetScalarElementField_2D(elp,nen_p,(PetscInt*)&elnidx_p[nen_p*e],LA_p);CHKERRQ(ierr);
		
		/* get element temperature */
		if (active_thermal_energy) {
			ierr = DMDAGetScalarElementFieldQ1_2D(elT,(PetscInt*)&elnidx_T[nen_T*e],LA_T);CHKERRQ(ierr);
		}
		
		/* get quadrature for the element */
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		for (p=0; p<ngp; p++) {
			
			//MPntStdGetField_local_element_index(material_point,&wil);
            
			//MPntStdGetField_local_coord(material_point,&xip);
			xip = &gp_xi[2*p];
			
			//MPntStdGetField_phase_index(material_point,&phase_mp);
			phase_mp = gausspoints[p].phase;
			
			PTatinConstructNI_Q2_2D(xip,NIu);
			PTatinConstructGNI_Q2_2D(xip,GNIu);
			ConstructNi_pressure(xip,elcoords,NIp);
            
			/* coord transformation */
			for (i=0; i<2; i++) {
				for (j=0; j<2; j++) { J[i][j] = 0.0; }
			}
			for (k=0; k<Q2_NODES_PER_EL_2D; k++) {
				J[0][0] += GNIu[0][k] * xc[k] ;
				J[0][1] += GNIu[0][k] * yc[k] ;
				
				J[1][0] += GNIu[1][k] * xc[k] ;
				J[1][1] += GNIu[1][k] * yc[k] ;
			}
			J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
			ojp = 1.0/J_p;
			iJ[0][0] =  J[1][1]*ojp;
			iJ[0][1] = -J[0][1]*ojp;
			iJ[1][0] = -J[1][0]*ojp;
			iJ[1][1] =  J[0][0]*ojp;
			
			/* global derivs at each quadrature point */
			for (k=0; k<Q2_NODES_PER_EL_2D; k++) {
				nx[k] = iJ[0][0]*GNIu[0][k] + iJ[0][1]*GNIu[1][k];
				ny[k] = iJ[1][0]*GNIu[0][k] + iJ[1][1]*GNIu[1][k];
			}
			
			/* interpolate position */
			//MPntStdGetField_global_coord(material_point,&position);
			position[0] = position[1] = 0.0;
			for (k=0; k<U_BASIS_FUNCTIONS; k++) {
				position[0] += NIu[k] * xc[k];
				position[1] += NIu[k] * yc[k];
			}
			
			/* interpolate pressure */
			pressure_mp = 0.0;
			for (k=0; k<P_BASIS_FUNCTIONS; k++) {
				pressure_mp += NIp[k] * elp[k];
			}
			//pressure_mp = pressure_mp -(sxx_mp+syy_mp)/2; 
            
			/* interpolate strain rate (e= B u) at gp */
			exx_mp = eyy_mp = exy_mp = 0.0;
			for (k=0; k<U_BASIS_FUNCTIONS; k++) {
				exx_mp += nx[k] * ux[k];
				eyy_mp += ny[k] * uy[k];
				exy_mp += ny[k] * ux[k] + nx[k] * uy[k];
			}
			exy_mp = 0.5 * exy_mp;
			strainrateinv_mp = sqrt(0.25*(exx_mp-eyy_mp)*(exx_mp-eyy_mp)+exy_mp*exy_mp);
			/*compute stress*/
            
            d1 = 2.0 * gausspoints[p].eta;
            sxx_mp = d1 * exx_mp;
            syy_mp = d1 * eyy_mp;
            sxy_mp = d1 * exy_mp;
            tau_mp = sqrt(0.25*(sxx_mp-syy_mp)*(sxx_mp-syy_mp)+sxy_mp*sxy_mp);
            
			/* interpolate temperature */
			T_mp = 0; 
			alpha_mp = 0; 
			if (active_thermal_energy) {
                
				ConstructNi_Q1_2D(xip,NIT);
				for (k=0; k<NODES_PER_EL_Q1_2D; k++) {
					T_mp = T_mp + NIT[k] * elT[k];
				}
			}
			
			/* density */ // TO BE PLACE In the tk update maybe 
			switch (rheology->density_type[phase_mp]) {
                    
				case DENSITY_CONSTANT:
				{
					rho = rheology->const_rho0[phase_mp];
					UnitsApplyInverseScaling(units->si_force_per_volume,rho,&rho_mp);
				}
					break; 
					
				case DENSITY_FUNCTION:
				{
					PetscScalar alpha_mp = rheology->temp_alpha[phase_mp];
					PetscScalar beta_mp; 
					UnitsApplyScaling(units->si_stress,rheology->temp_beta[phase_mp],&beta_mp);
					
					rho = rheology->const_rho0[phase_mp]*(1-alpha_mp*T_mp+beta_mp*pressure_mp);        
					UnitsApplyInverseScaling(units->si_force_per_volume,rho,&rho_mp);
				}
					break; 
                    
				case DENSITY_TABLE:
				{
					double position[2],rhodummy;
					char *name;
					rho = rheology->const_rho0[phase_mp];
					UnitsApplyInverseScaling(units->si_force_per_volume,rho,&rho_mp);
					
					asprintf(&name,"densmap_%d",phase_mp);
					TempMap map = PETSC_NULL;
					ierr = pTatinCtxGetMap(user,&map,name);CHKERRQ(ierr);
					position[0] = T_mp;
					position[1] = pressure_mp;
					TempMapGetDouble(map,position,&rhodummy);
					if (rhodummy == -1){
						PetscPrintf(PETSC_COMM_WORLD,"marker outside PT array,P= %e T=%e \n",pressure_mp,T_mp);
					} else if (rhodummy == 1) {
						PetscPrintf(PETSC_COMM_WORLD,"I don' get what PT array does");                    
					} else {
						rho_mp = rhodummy;
					}
				}
					break;     
					
			}
			
			/* viscosity */;
			switch (rheology->viscous_type[phase_mp]) {
					
				case VISCOUS_CONSTANT:
				{
					//eta_mp = scaling_eta*rheology->const_eta0[phase_mp];
					UnitsApplyInverseScaling(units->si_viscosity,rheology->const_eta0[phase_mp],&eta_mp);
				}
					break;
					
				case VISCOUS_FRANKK:
				{
					PetscReal eta;
					
					eta  = rheology->const_eta0[phase_mp]*exp(-rheology->temp_theta[phase_mp]*T_mp);
					UnitsApplyInverseScaling(units->si_viscosity,rheology->const_eta0[phase_mp],&eta_mp);
					
				}
					break;
					
				case VISCOUS_ARRHENIUS:
				{
					PetscScalar R       = 8.31440;
					PetscReal nexp      = rheology->arrh_nexp[phase_mp];
					PetscReal entalpy   = rheology->arrh_entalpy[phase_mp];
					PetscReal preexpA   = rheology->arrh_preexpA[phase_mp];
					PetscReal Vmol      = rheology->arrh_Vmol[phase_mp];
					PetscReal Tref      = rheology->arrh_Tref[phase_mp];
					PetscReal Ascale    = rheology->arrh_Ascale[phase_mp];
					PetscReal T_arrh    = T_mp + Tref ;
					PetscReal sr, eta, pressure; 
					
					UnitsApplyScaling(units->si_strainrate,strainrateinv_mp,&sr);
					
					if (sr < 1.0e-17) {
						sr = 1.0e-17;
					}
					UnitsApplyScaling(units->si_stress,pressure_mp,&pressure);
					
					
					entalpy = entalpy + pressure*Vmol;
					eta  = Ascale*0.25*pow(sr,1.0/nexp - 1.0)*pow(0.75*preexpA,-1.0/nexp)*exp(entalpy/(nexp*R*T_arrh));
					UnitsApplyInverseScaling(units->si_viscosity,eta,&eta_mp);
				}
					break;
			}
			/* update viscosity on quadrature point */
			gausspoints[p].eta = eta_mp;
			
			{
				is_yielding = (char)YTYPE_NONE; 
				switch (rheology->plastic_type[phase_mp]) {
						
					case PLASTIC_NONE: 
					{
					}
						break;
                        
					case PLASTIC_M:
					{
						PetscScalar tauyield,dummy,tauyield_mp,stressinv_mp,plastic_strain_mp;
						
						plastic_strain_mp = 0.0;
						ApplyPlasticSofteningQuadraturePoint(rheology,phase_mp, &tauyield, &dummy, plastic_strain_mp);                
						UnitsApplyInverseScaling(units->si_stress,tauyield,&tauyield_mp);
						yield_type = YTYPE_MISES; 
						/* viscous stress predictor */ 
						d1 = 2.0 * eta_mp;
						sxx_mp = d1 * exx_mp;
						syy_mp = d1 * eyy_mp;
						sxy_mp = d1 * exy_mp;
						stressinv_mp = sqrt(0.25*(sxx_mp-syy_mp)*(sxx_mp-syy_mp)+sxy_mp*sxy_mp);
						tauyield_mp = -stressinv_mp/10000.+tauyield_mp;
						/* apply plastic correction when marker is yielding */ 
						if (stressinv_mp > tauyield_mp) {
							//PetscScalar eta_vis = eta_mp;
							eta_mp = 0.5*tauyield_mp/strainrateinv_mp;
							//PetscPrintf(PETSC_COMM_WORLD," eta_vis %e eta_plas %e tauyield %e stress_inv %e \n", eta_vis,eta_mp,tauyield_mp, stressinv_mp);
							npoints_yielded++;
							is_yielding = (char)yield_type; 
						}
					}
						break;
						
					case PLASTIC_DAMAGE:
                        // see exemple usage in the model_NotchGP.c
					{
						PetscScalar tens_cutoff_mp,Hst_cutoff_mp,phi,Co,A,B,Co_mp;
						PetscScalar tauyield_mp,stressinv_mp,plastic_strain_mp,plastic_strainrate_mp,eta_mpd;
                        PetscScalar eta_h,eta_h_inf,eta_h_0; 
                        PetscScalar eta_0     = eta_mp;
                        PetscInt factor       = rheology->dam_factor[phase_mp];     
                        PetscScalar eps_min   = rheology->dam_eps_min[phase_mp];  
                        PetscScalar eps_max   = rheology->dam_eps_max[phase_mp];
                        
                        /*update plastic strainrate */ 
                        gausspoints[p].plsr  = strainrateinv_mp-tau_mp/2.0/eta_mp-0.2;
                       
                        
                        
                        plastic_strain_mp    = gausspoints[p].pls+gausspoints[p].plsr*user->dt; 
                        
                        if (plastic_strain_mp < 0.0){
                            plastic_strain_mp = 0.0;
                        }
                        
                        UnitsApplyInverseScaling(units->si_viscosity,rheology->dam_eta_h_inf[phase_mp],&eta_h_inf);                            
                        UnitsApplyInverseScaling(units->si_viscosity,rheology->dam_eta_h_0[phase_mp],&eta_h_0);
                        
                        eta_h    =  eta_h_inf;
                        if (plastic_strain_mp < eps_max){
                            if (plastic_strain_mp >= eps_min){
                                
                                eta_h    =  eta_h_0 - (eta_h_0-eta_h_inf) * (plastic_strain_mp-eps_min)/(eps_max-eps_min);
                            }else{
                                eta_h    = eta_h_0;    
                            }
                        }
                        
                        /* viscous stress predictor */                        
                        d1 = 2.0 * eta_mp;
						sxx_mp = d1 * exx_mp;
						syy_mp = d1 * eyy_mp;
						sxy_mp = d1 * exy_mp;
						stressinv_mp = sqrt(0.25*(sxx_mp-syy_mp)*(sxx_mp-syy_mp)+sxy_mp*sxy_mp);
						
                        /*get DP parameter and cutoff*/ 
						UnitsApplyInverseScaling(units->si_stress,rheology->tens_cutoff[phase_mp],&tens_cutoff_mp);
						UnitsApplyInverseScaling(units->si_stress,rheology->Hst_cutoff[phase_mp],&Hst_cutoff_mp);
						ApplyPlasticSofteningQuadraturePoint(rheology,phase_mp, &Co, &phi, plastic_strain_mp);
						A           = sin(phi/180.0*M_PI);
						B           = cos(phi/180.0*M_PI);	
						UnitsApplyInverseScaling(units->si_stress,Co,&Co_mp);
                        
						tauyield_mp = A * pressure_mp + B * Co_mp;    
						yield_type  = YTYPE_DP; 
						
						if (tauyield_mp < tens_cutoff_mp) {
							/* failure in tension cutoff */
							tauyield_mp = tens_cutoff_mp;
							yield_type = YTYPE_TENSILE_FAILURE;
						} else if (tauyield_mp > Hst_cutoff_mp) {   
							/* failure at High stress cut off à la boris */
							tauyield_mp = Hst_cutoff_mp;
							yield_type = YTYPE_MISES;
						}  
                        
                        /*Apply Yielding */ 
                        if (stressinv_mp > tauyield_mp) {
                            eta_mp   =  0.5*tauyield_mp/strainrateinv_mp+eta_h;
                            npoints_yielded++;
                            is_yielding = (char)yield_type; 
						}
                        /*Apply regularisation à la Dave */                        
                        eta_mp = pow( eta_mp, -factor) + pow(eta_0, -factor);
                        eta_mp = pow( eta_mp, -1.0/factor );
                        
                        
                        
                    }
						break;
                    case PLASTIC_DP:
					{
                        PetscScalar tens_cutoff_mp,Hst_cutoff_mp,phi,Co,A,B,Co_mp;
						PetscScalar tauyield_mp,stressinv_mp,plastic_strain_mp,plastic_strainrate_mp,eta_mpd;
                        /*update plastic strainrate */ 
                        gausspoints[p].plsr  = strainrateinv_mp-tau_mp/eta_mp;
                        plastic_strain_mp = gausspoints[p].pls+gausspoints[p].plsr*user->dt;                        
                        /* viscous stress predictor */                        
                        d1 = 2.0 * eta_mp;
						sxx_mp = d1 * exx_mp;
						syy_mp = d1 * eyy_mp;
						sxy_mp = d1 * exy_mp;
						stressinv_mp = sqrt(0.25*(sxx_mp-syy_mp)*(sxx_mp-syy_mp)+sxy_mp*sxy_mp);
						
                        
                        /*get DP parameter and cutoff*/ 
						UnitsApplyInverseScaling(units->si_stress,rheology->tens_cutoff[phase_mp],&tens_cutoff_mp);
						UnitsApplyInverseScaling(units->si_stress,rheology->Hst_cutoff[phase_mp],&Hst_cutoff_mp);
						ApplyPlasticSofteningQuadraturePoint(rheology,phase_mp, &Co, &phi, plastic_strain_mp);
						A           = sin(phi/180.0*M_PI);
						B           = cos(phi/180.0*M_PI);	
						UnitsApplyInverseScaling(units->si_stress,Co,&Co_mp);
                        
						tauyield_mp = A * pressure_mp + B * Co_mp;// + pow(strainrateinv_mp,1) * pow(0.5,1);    
						yield_type  = YTYPE_DP; 
						
						if (tauyield_mp < tens_cutoff_mp) {
							/* failure in tension cutoff */
							tauyield_mp = tens_cutoff_mp;
							yield_type = YTYPE_TENSILE_FAILURE;
						} else if (tauyield_mp > Hst_cutoff_mp) {   
							/* failure at High stress cut off à la boris */
							tauyield_mp = Hst_cutoff_mp;
							yield_type = YTYPE_MISES;
						}  
                        
                        
                        if (stressinv_mp > tauyield_mp) {
                            eta_mp               = 0.5*tauyield_mp/strainrateinv_mp;
                            
                            //printf("  [phase %d] eta_effective = %1.4e \n",phase_mp,eta_mp);
                            // PetscPrintf(PETSC_COMM_WORLD," eta_vis %e eta_plas %e tauyield %e stress_inv %e \n", eta_vis,eta_mp,tauyield_mp, stressinv_mp);
                            npoints_yielded++;
                            //if ( yield_type > 1) PetscPrintf(PETSC_COMM_WORLD," yield type %d \n",yield_type);
                            is_yielding = (char)yield_type;
                        }
						
                    }
                        break;
				}
				//MPntPStokesPlSetField_yield_indicator(mpprop_stokespl,is_yielding);
			}
			
			/* Global cuttoffs for viscosity */
			if (eta_mp > eta_upper_cutoff_global_mp) {
				eta_mp = eta_upper_cutoff_global_mp;
			}
			if (eta_mp < eta_lower_cutoff_global_mp) {
				eta_mp = eta_lower_cutoff_global_mp;
			}
			
			/* update viscosity on quadrature point */
			//MPntPStokesSetField_eta_effective(mpprop_stokes,eta_mp);
			gausspoints[p].eta = eta_mp;
			
			/* update density on quadrature point */
			//MPntPStokesSetField_density(mpprop_stokes,rho_mp);
			gausspoints[p].Fu[0] = gausspoints[p].gravity[0] * rho_mp;
			gausspoints[p].Fu[1] = gausspoints[p].gravity[1] * rho_mp;
			
			/* monitor bounds */
			if (eta_mp > max_eta) { max_eta = eta_mp; }
			if (eta_mp < min_eta) { min_eta = eta_mp; }
			/* monitor bounds */
			if (rho_mp > max_rho) { max_rho = rho_mp; }
			if (rho_mp < min_rho) { min_rho = rho_mp; }
            
		} /* end on quad points */
	} /* end on cells */
	
	PetscGetTime(&t1);
#if 0    
	ierr = MPI_Allreduce(&min_eta,&min_eta_g,1, MPI_DOUBLE, MPI_MIN, PETSC_COMM_WORLD);CHKERRQ(ierr);
	ierr = MPI_Allreduce(&max_eta,&max_eta_g,1, MPI_DOUBLE, MPI_MAX, PETSC_COMM_WORLD);CHKERRQ(ierr);
	
	ierr = MPI_Allreduce(&min_rho,&min_rho_g,1, MPI_DOUBLE, MPI_MIN, PETSC_COMM_WORLD);CHKERRQ(ierr);
	ierr = MPI_Allreduce(&max_rho,&max_rho_g,1, MPI_DOUBLE, MPI_MAX, PETSC_COMM_WORLD);CHKERRQ(ierr);
    
	PetscPrintf(PETSC_COMM_WORLD,"  VPT: Update stokes non linearities [qpoint]: (min/max)_eta %1.2e / %1.2e; log10(max/min) %1.2e; (min/max)_rho %1.2e / %1.2e \n",  min_eta_g, max_eta_g, log10(max_eta_g/min_eta_g),min_rho_g, max_rho_g);
#endif
	
	UnitsApplyScaling(units->si_viscosity,min_eta_g,&min_eta_g);
	UnitsApplyScaling(units->si_viscosity,max_eta_g,&max_eta_g);
	UnitsApplyScaling(units->si_force_per_volume,min_rho_g,&min_rho_g);
	UnitsApplyScaling(units->si_force_per_volume,max_rho_g,&max_rho_g);
	
	
#if 0		
	ierr = MPI_Allreduce(&npoints_yielded,&npoints_yielded_g,1,MPI_INT,MPI_SUM,PETSC_COMM_WORLD);CHKERRQ(ierr);
	PetscPrintf(PETSC_COMM_WORLD,"  VPT: Update stokes non linearities [qpoint]: npoints_yielded %d\n", npoints_yielded_g);
    
	PetscPrintf(PETSC_COMM_WORLD,"  VPT: Update stokes non linearities [qpoint]: cpu time %1.2e (sec)\n", t1-t0 );
#endif
	if (active_thermal_energy) {
		ierr = VecRestoreArray(T_local_vec,&LA_T);CHKERRQ(ierr);
		ierr = DMRestoreLocalVector(daT,&T_local_vec);CHKERRQ(ierr);		
	}
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "StokesEvaluateRheologyNonLinearitiesMarkers_VPT"
PetscErrorCode StokesEvaluateRheologyNonLinearitiesMarkers_VPT(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[])
{	
	PetscErrorCode ierr;
	int         pidx,n_mp_points;
	DataBucket  db;
	DataField   PField_std, PField_stokes, PField_thermal, PField_stokespl, PField_stokesmelt;
	PetscScalar GNIu[2][Q2_NODES_PER_EL_2D],NIp[P_BASIS_FUNCTIONS];
	PetscScalar NIu[U_BASIS_FUNCTIONS];
	PetscScalar NIT[NODES_PER_EL_Q1_2D];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen_u,nen_p,nen_T,e,i,j,k;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	const PetscInt *elnidx_T;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	PetscScalar elT[NODES_PER_EL_Q1_2D];
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	PetscLogDouble t0,t1;
	PetscScalar min_eta,max_eta,min_eta_g,max_eta_g;
	PetscScalar min_rho,max_rho,min_rho_g,max_rho_g;
	PetscInt npoints_yielded_g, npoints_yielded_tens_g,npoints_yielded,npoints_yielded_tens;
	
	PetscScalar eta_lower_cutoff_global_mp,eta_upper_cutoff_global_mp;
	
	BTruth plastic = PETSC_FALSE;
	BTruth melt    = PETSC_FALSE;
	RheologyConstants *rheology;
	
	PetscBool         active_thermal_energy = PETSC_FALSE;    
	PhysCompEnergyCtx phys_energy;
	Vec               T_vec,T_local_vec;
	DM                daT;
	PetscScalar       *LA_T;
	pTatinUnits *units;	
	
	PetscFunctionBegin;
	
	units   = &user->units;
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	/////////////////////* get physics */////////////////
	
	/* stokes */
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);

	db = user->db;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));
	
	DataBucketQueryDataFieldByName(db,MPntPStokesPl_classname,&plastic);
	npoints_yielded = 0;    
	if (plastic){
		DataBucketGetDataFieldByName(db,MPntPStokesPl_classname,&PField_stokespl);    
		DataFieldGetAccess(PField_stokespl);
		DataFieldVerifyAccess(PField_stokespl,sizeof(MPntPStokesPl));
	} 
	
	DataBucketQueryDataFieldByName(db,MPntPStokesMelt_classname,&melt);
    
	if (melt){
		DataBucketGetDataFieldByName(db,MPntPStokesMelt_classname,&PField_stokesmelt);    
		DataFieldGetAccess(PField_stokesmelt);
		DataFieldVerifyAccess(PField_stokesmelt,sizeof(MPntPStokesMelt));
	} 
	
	/* energy */
	active_thermal_energy = PETSC_FALSE;
	ierr = pTatinPhysCompActivated(user,PhysComp_Energy,&active_thermal_energy);CHKERRQ(ierr);
	if (active_thermal_energy) {
		phys_energy = user->phys_energy;
		T_vec       = phys_energy->T;
		daT         = phys_energy->daT;
		
		ierr = DMGetLocalVector(daT,&T_local_vec);CHKERRQ(ierr);		
		ierr = DMGlobalToLocalBegin(daT,T_vec,INSERT_VALUES,T_local_vec);CHKERRQ(ierr);
		ierr = DMGlobalToLocalEnd(  daT,T_vec,INSERT_VALUES,T_local_vec);CHKERRQ(ierr);
		
		ierr = VecGetArray(T_local_vec,&LA_T);CHKERRQ(ierr);
		ierr = DMDAGetElementsQ1(daT,&nel,&nen_T,&elnidx_T);CHKERRQ(ierr);
		DataBucketGetDataFieldByName(db,MPntPThermal_classname,&PField_thermal);
		DataFieldGetAccess(PField_thermal);
		DataFieldVerifyAccess(PField_thermal,sizeof(MPntPThermal));
	}
	/* new piece of physics to be hook here following energy exemple*/
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	min_eta = 1.0e100;
	max_eta = 1.0e-100;
	min_rho = 1.0e100;
	max_rho = 1.0e-100;
	
	PetscGetTime(&t0);
	rheology = &user->rheology_constants;
	
	UnitsApplyInverseScaling(units->si_viscosity,rheology->eta_upper_cutoff_global,&eta_upper_cutoff_global_mp);
	
	UnitsApplyInverseScaling(units->si_viscosity,rheology->eta_lower_cutoff_global,&eta_lower_cutoff_global_mp);
	/*
	 In this file, all properties marked with mp are scaled, other properties are in SI units.
	 */
	/* marker loop */
	
	for (pidx=0; pidx<n_mp_points; pidx++) {
		MPntStd       *material_point;
		MPntPStokes   *mpprop_stokes;
		MPntPStokesPl *mpprop_stokespl;
	    MPntPStokesMelt *mpprop_stokesmelt;
		char          is_yielding, yield_type;        
		MPntPThermal  *mpprop_thermal;
		double        *position, *xip;
		int           wil,phase_mp;
		PetscScalar J[2][2], iJ[2][2];
		PetscScalar J_p,ojp;
		PetscScalar  d1,exx_mp,eyy_mp,exy_mp,eta_mp,strainrateinv_mp;
		PetscScalar  sxx_mp,syy_mp,sxy_mp,u_mp,v_mp,trace_mp,pressure_mp;
		PetscScalar T_mp,rho_mp,rho;
		PetscScalar T_eq, P_eq; 
		PetscScalar alpha_mp;
		PetscReal strr;
		
		DataFieldAccessPoint(PField_std,    pidx,(void**)&material_point);
		DataFieldAccessPoint(PField_stokes, pidx,(void**)&mpprop_stokes);
		if (plastic) { DataFieldAccessPoint(PField_stokespl,pidx,(void**)&mpprop_stokespl); }
		if (melt) { DataFieldAccessPoint(PField_stokesmelt,pidx,(void**)&mpprop_stokesmelt); }		
		MPntStdGetField_local_element_index(material_point,&wil);
		MPntStdGetField_global_coord(material_point,&position);
		MPntStdGetField_local_coord(material_point,&xip);
		MPntStdGetField_phase_index(material_point,&phase_mp);
		
		e = wil;
		
		/* get nodal properties for element e */
		/* coordinates*/
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		/* velocity*/
		ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],u);CHKERRQ(ierr);
		
		PTatinConstructNI_Q2_2D(xip,NIu);
		PTatinConstructGNI_Q2_2D(xip,GNIu);
		
		
		/* get element velocity/coordinates in component form */
		for (i=0; i<Q2_NODES_PER_EL_2D; i++) {
			ux[i] = elu[2*i  ];
			uy[i] = elu[2*i+1];
			
			xc[i] = elcoords[2*i  ];
			yc[i] = elcoords[2*i+1];
		}
		
		/* coord transformation */
		for (i=0; i<2; i++) {
			for (j=0; j<2; j++) { J[i][j] = 0.0; }
		}
		for (k=0; k<Q2_NODES_PER_EL_2D; k++) {
			J[0][0] += GNIu[0][k] * xc[k] ;
			J[0][1] += GNIu[0][k] * yc[k] ;
			
			J[1][0] += GNIu[1][k] * xc[k] ;
			J[1][1] += GNIu[1][k] * yc[k] ;
		}
		J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
		ojp = 1.0/J_p;
		iJ[0][0] =  J[1][1]*ojp;
		iJ[0][1] = -J[0][1]*ojp;
		iJ[1][0] = -J[1][0]*ojp;
		iJ[1][1] =  J[0][0]*ojp;
		
		/* global derivs at each quadrature point */
		for (k=0; k<Q2_NODES_PER_EL_2D; k++) {
			nx[k] = iJ[0][0]*GNIu[0][k] + iJ[0][1]*GNIu[1][k];
			ny[k] = iJ[1][0]*GNIu[0][k] + iJ[1][1]*GNIu[1][k];
		}
		
		
		
		
		
		
		/* strain rate (e= B u) at gp */
		exx_mp = eyy_mp = exy_mp = 0.0;
		for (k=0; k<U_BASIS_FUNCTIONS; k++) {
			exx_mp += nx[k] * ux[k];
			eyy_mp += ny[k] * uy[k];
			exy_mp += ny[k] * ux[k] + nx[k] * uy[k];
		}
		exy_mp = 0.5 * exy_mp;
		strainrateinv_mp = sqrt(0.25*(exx_mp-eyy_mp)*(exx_mp-eyy_mp)+exy_mp*exy_mp);
        
		/* viscous stress predictor */ 
		//MPntPStokesGetField_eta_effective(mpprop_stokes,&eta_mp);
		//d1 = 2.0 * eta_mp;
		//sxx_mp = d1 * exx_mp;
		//syy_mp = d1 * eyy_mp;
		/* if the strainrate is 0 the Arhenius reaches a viscosity of 0 and a 0/0 may arrise in the plasticity */ 
		/*pressure*/
		ierr = DMDAGetScalarElementField_2D(elp,nen_p,(PetscInt*)&elnidx_p[nen_p*e],p);CHKERRQ(ierr);
		ConstructNi_pressure(xip,elcoords,NIp);
		pressure_mp = 0.0;
		for (k=0; k<P_BASIS_FUNCTIONS; k++) {
			pressure_mp += NIp[k] * elp[k];
		}
		//pressure_mp = pressure_mp -(sxx_mp+syy_mp)/2; 
		
		
		
		/* temperature */
		T_mp = 0; 
		alpha_mp = 0; 
		if (active_thermal_energy) {
			DataFieldAccessPoint(PField_thermal,pidx,(void**)&mpprop_thermal);
			ierr = DMDAGetScalarElementFieldQ1_2D(elT,(PetscInt*)&elnidx_T[nen_T*e],LA_T);CHKERRQ(ierr);
			ConstructNi_Q1_2D(xip,NIT);
			for (k=0; k<NODES_PER_EL_Q1_2D; k++) {
				T_mp = T_mp + NIT[k] * elT[k];
			}
			
		}
		
		/* phase change */
		T_eq = T_mp; 
		P_eq = pressure_mp;
		
		
		/*density*/ // TO BE PLACE In the tk update maybe 
		switch (rheology->density_type[phase_mp]) {
			case DENSITY_CONSTANT:{
				rho = rheology->const_rho0[phase_mp];
				UnitsApplyInverseScaling(units->si_force_per_volume,rho,&rho_mp);
			}
				break; 
			case DENSITY_FUNCTION:{
				PetscScalar alpha_mp = rheology->temp_alpha[phase_mp];
				PetscScalar beta_mp; 
				UnitsApplyScaling(units->si_stress,rheology->temp_beta[phase_mp],&beta_mp);
				
				rho = rheology->const_rho0[phase_mp]*(1-alpha_mp*T_mp+beta_mp*pressure_mp);        
				UnitsApplyInverseScaling(units->si_force_per_volume,rho,&rho_mp);
			}
				break; 
			case DENSITY_TABLE:{
				double position[2],rhodummy;
				char name[PETSC_MAX_PATH_LEN];
				rho = rheology->const_rho0[phase_mp];
				UnitsApplyInverseScaling(units->si_force_per_volume,rho,&rho_mp);
				
				sprintf(&name,"densmap_%d",phase_mp);
				TempMap map = PETSC_NULL;
				ierr = pTatinCtxGetMap(user,&map,name);CHKERRQ(ierr);
				
				position[0] = T_eq;
				position[1] = P_eq;
				RheolMapGetDouble(map,position,&rhodummy);
				if (rhodummy == -1){
					PetscPrintf(PETSC_COMM_WORLD,"marker outside PT array,P= %e T=%e \n",pressure_mp,T_mp);
				}else if(rhodummy==1) {
					PetscPrintf(PETSC_COMM_WORLD,"I don' get what PT array does");                    
				}else{
					rho_mp = rhodummy;
					
				}
			}
				break;     
				
		}
		is_yielding = (char)YTYPE_NONE;
		/* viscosity */;
		switch (rheology->viscous_type[phase_mp]) {
				
			case VISCOUS_CONSTANT: {
				//eta_mp = scaling_eta*rheology->const_eta0[phase_mp];
				UnitsApplyInverseScaling(units->si_viscosity,rheology->const_eta0[phase_mp],&eta_mp);
			}
				break;
				
			case VISCOUS_FRANKK: {
				PetscReal eta;
				eta  = rheology->const_eta0[phase_mp]*exp(-rheology->temp_theta[phase_mp]*T_mp);
				UnitsApplyInverseScaling(units->si_viscosity,rheology->const_eta0[phase_mp],&eta_mp);
				
			}
				break;
				
			case VISCOUS_ARRHENIUS: {
				PetscScalar R       = 8.31440;
				PetscReal nexp      = rheology->arrh_nexp[phase_mp];
				PetscReal entalpy   = rheology->arrh_entalpy[phase_mp];
				PetscReal preexpA   = rheology->arrh_preexpA[phase_mp];
				PetscReal Vmol      = rheology->arrh_Vmol[phase_mp];
				PetscReal Tref      = rheology->arrh_Tref[phase_mp];
				PetscReal Ascale    = rheology->arrh_Ascale[phase_mp];
				PetscReal T_arrh    = T_mp + Tref ;
				PetscReal sr, eta, pressure; 
				
				UnitsApplyScaling(units->si_strainrate,strainrateinv_mp,&sr);
				
				if (sr < 1.0e-17) {
					sr = 1.0e-17;
				}
				UnitsApplyScaling(units->si_stress,pressure_mp,&pressure);
				
				
				entalpy = entalpy + pressure*Vmol;
				eta  = Ascale*0.25*pow(sr,1.0/nexp - 1.0)*pow(0.75*preexpA,-1.0/nexp)*exp(entalpy/(nexp*R*T_arrh));
				UnitsApplyInverseScaling(units->si_viscosity,eta,&eta_mp);
			}
				break;
				
			case VISCOUS_POWERLAW:{
					PetscReal I2, eta; 
			        PetscReal nexp      = rheology->arrh_nexp[phase_mp];
					PetscReal preexpA   = rheology->arrh_preexpA[phase_mp];			        
			        UnitsApplyScaling(units->si_strainrate,strainrateinv_mp,&I2);
					
					if (I2 < 1.0e-32) {
						I2 = 1.0e-32;
					}
			        
			        eta = preexpA*pow(I2,1.0/(nexp)-1.0); 
			        UnitsApplyInverseScaling(units->si_viscosity,eta,&eta_mp);
			        }
			    break;
			    
			 case VISCOUS_ARRHENIUS_MIXING:{		 
				PetscScalar R       = 8.31440;
				double position[2],preexpA,entalpy,nexp;
				char name[PETSC_MAX_PATH_LEN];
				TempMap map = PETSC_NULL;
				PetscReal Tref      = rheology->arrh_Tref[phase_mp];
				PetscReal Ascale    = rheology->arrh_Ascale[phase_mp];
				PetscReal T_arrh    = T_mp + Tref ;
				PetscReal sr, eta,pressure; 
				PetscReal Vmol      = rheology->arrh_Vmol[phase_mp];
                
				position[0] = T_eq;
				position[1] = P_eq; /// might be better to store a converged P for convergence
				
				sprintf(&name,"Amap_%d",phase_mp);
				ierr = pTatinCtxGetMap(user,&map,name);CHKERRQ(ierr);
				RheolMapGetDouble(map,position,&preexpA);
				
				sprintf(&name,"Qmap_%d",phase_mp);
				ierr = pTatinCtxGetMap(user,&map,name);CHKERRQ(ierr);
				RheolMapGetDouble(map,position,&entalpy);
				
				sprintf(&name,"nexpmap_%d",phase_mp);
				ierr = pTatinCtxGetMap(user,&map,name);CHKERRQ(ierr);
				RheolMapGetDouble(map,position,&nexp);
				

				UnitsApplyScaling(units->si_strainrate,strainrateinv_mp,&sr);
				
				if (sr < 1.0e-32) {
					sr = 1.0e-32;
				}
				UnitsApplyScaling(units->si_stress,pressure_mp,&pressure);
				entalpy = entalpy + pressure*Vmol;
				eta  = Ascale*0.25*pow(sr,1.0/nexp - 1.0)*pow(0.75*preexpA,-1.0/nexp)*exp(entalpy/(nexp*R*T_arrh));
                 
                 
				UnitsApplyInverseScaling(units->si_viscosity,eta,&eta_mp);
			}
				break;
                
			case VISCOUS_IO:{
                PetscScalar R       = 8.31440;
                PetscReal T_arrh    = T_mp + 273.0 ;
                PetscReal sr, eta, pressure,eta_test;
                PetscScalar C_H20 = 1.0;
                PetscScalar d     = 1.e3;
	///// mettre d en micron	
                if (phase_mp >=8) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONG,"you have not define enough phase in VISCOUS_IO");
                UnitsApplyScaling(units->si_strainrate,strainrateinv_mp,&sr);
                
                if (sr < 1.0e-19) {
                    sr = 1.0e-19;
                }
                UnitsApplyScaling(units->si_stress,pressure_mp,&pressure);
                UnitsApplyInverseScaling(units->si_viscosity,eta_upper_cutoff_global_mp,&eta);
                
                /* 0 good old laeti's quartz
                   1 quartzite Burgman paper
                   2 olivine Hirth kolsted
                   3 pyroxen Dimanov wet diopside
                   4 anortite wet ribaky et al 2006
                 */
                eta=1.e25; 
                {
                 /* diffusion*/
                    PetscScalar  m[5]    = {3.0,0,0,0,0,3.0,0,3.0};
                    PetscScalar  E[5]    = {156e3,0,0,0,0,156e3,0,156e3};
                    PetscScalar  A[5]    = {1.5e-6,0,0,0,0,1.5e-6,0,1.5e-6};
                    PetscScalar  Vmol[5] = {6.0e-6,0,0,0,0,6.0e-6,0,6.0e-6};
                    //PetscScalar  m[5]    = {3.0   ,2.0,3.0,3.0,3.0};
                    //PetscScalar  E[5]    = {375.e3,220e3,375.e3,337e3,159.e3};
                    //PetscScalar  A[5]    = {1.5e9,0.3981,2.5119e+07,1.2589e+08,0.1995};
                    //PetscScalar  Vmol[5] = {6.e-6, 0.     ,20.e-6,0.,38.e-6};
                    PetscScalar  entalpy = E[phase_mp] + pressure*Vmol[phase_mp];
                    
                    eta_test    = 1.e6*pow(d,m[phase_mp])*C_H20*pow(0.75*A[phase_mp],-1.0)*exp(entalpy/(R*T_arrh));
                    if (eta > eta_test){
                        yield_type = (char)YTYPE_DIFF;
                        eta = eta_test;
                    }
                    
                }
                {
                    /* dislocation creep*/
                    PetscScalar n[5] = {3.5,0,0,0,0,3.5,0,3.5};
                    PetscScalar E[5] = {530e3,0,0,0,0,530e3,0,530e3};
                    PetscScalar A[5] = {1.1e5,0,0,0,0,1.1e5,0,1.1e5};
                    PetscScalar Vmol[5] = {10e-6,0,0,0,0,10e-6,0,10e-6};
                    //PetscScalar n[5] = {3.5   ,3.0,3.5,5.5,3.0};
                    //PetscScalar E[5] = {520.e+03,242.E3, 520e3, 534.e3    ,345e3};
                    //PetscScalar A[5] = {1.58e+3 ,0.3236,1.5849e+03,6.3096,1.5849};
                    //PetscScalar Vmol[5] = {22.e-6,0.0   ,22.e-6    ,0.    ,38.e-6};
                    PetscScalar entalpy = E[phase_mp] + pressure*Vmol[phase_mp];
                    
                    eta_test    = 1.e6*pow(C_H20,-1/n[phase_mp])*0.25*pow(sr,1.0/n[phase_mp] - 1.0)*pow(0.75*A[phase_mp],-1.0/n[phase_mp])*exp(entalpy/(n[phase_mp]*R*T_arrh));
                    if (eta > eta_test){
                        yield_type = (char)YTYPE_DISC;
                        eta = eta_test;
                    }
                    
                }
#if 0
                {
                    /* dislocation glide */
                    PetscScalar n[5]     = {3.0,3.0,3.5,5.5,3.0};
                    PetscScalar E[5]     = {156e3,242.e3,520e3, 534.e3, 345e3};
                    PetscScalar A[5]    = {1e-6,0.3236,1.5849e+03,6.3096,1.5849};
                    PetscScalar tau_y[5] = {3.e8,3.e8,3.e8,3.e8,3.e8};
                    PetscScalar Vmol[5]  = {0.,0.0,22.e-6,0.,38.e-6};
                    PetscScalar entalpy = E[phase_mp] + pressure*Vmol[phase_mp];
                    
                   // eta_test    = 1.e6*0.25*tau_y[phase_mp]*R*T_arrh/entalpy/sr*(log(0.75*A[phase_mp])-entalpy/R/T_arrh-log(sr));
                    eta_test    = 1.e6*0.25*(tau_y*(1-R*T_arrh/entalpy*log(sr/(0.75*A[phase_mp])));
                    
                    if (eta > eta_test){
                        yield_type = (char)YTYPE_DISG;
                        eta = eta_test;
                    }
                }

                

                {
                    /* GBS */
                    PetscScalar n = {}
                    PetscScalar E = {}
                    PetscScalar A = {}
                    PetscScalar Vmol = {}
                    PetscScalar entalpy = E[phase_mp] + pressure*Vmol[phase_mp];
                    
                    eta_test    = pow(C_H20,-1/n[phase_mp])*0.25*pow(sr,1.0/n[phase_mp] - 1.0)*pow(0.75*A[phase_mp],-1.0/n[phase_mp])*exp(entalpy/(n[phase_mp]*R*T_arrh));
                    if (eta > eta_test){
                        yield_type = 7;
                    }
                    
                }
#endif
                UnitsApplyInverseScaling(units->si_viscosity,eta,&eta_mp);
                //PetscPrintf(PETSC_COMM_WORLD,"vis= %e yield =%d\n",eta,yield_type);
                //MPntPStokesPlSetField_yield_indicator(mpprop_stokespl,yield_type);
		is_yielding = yield_type; 
                
            }
                break;
                
                
                
		}
		
		if (rheology->store_melt & active_thermal_energy) {
			 
			switch (rheology->melt_type[phase_mp]) {
		     	case MELT_NONE: {
				
				}
					break;
					
				case MELT_MUMU: {
				PetscScalar eta_melt,rho_melt;
				PetscScalar eta_melt_mp,rho_melt_mp;
				float melt_fraction;   
				
				Compute_melt_fraction_mumu(rheology,phase_mp,T_mp,pressure_mp,&melt_fraction);
			    MPntPStokesMeltSetField_melt_portion(mpprop_stokesmelt,melt_fraction);		
				eta_melt      = rheology->melt_eta[phase_mp];
				UnitsApplyInverseScaling(units->si_viscosity,eta_melt,&eta_melt_mp);
				eta_mp   = 1.0/(melt_fraction/eta_melt_mp+(1.0-melt_fraction)/eta_mp);
				
				rho_melt      = rheology->melt_rho[phase_mp];
				UnitsApplyInverseScaling(units->si_force_per_volume,rho_melt,&rho_melt_mp);
				rho_mp   = rho_mp*(1.0-melt_fraction)+melt_fraction*rho_melt_mp;
				}
				break;
                case MELT_P: {
                    PetscScalar eta_melt,rho_melt,pressure;
                    PetscScalar eta_melt_mp,rho_melt_mp;
                    float melt_fraction;
                    UnitsApplyScaling(units->si_stress,pressure_mp,&pressure);
                   
                    Compute_melt_fraction_P(rheology->meltP_type[phase_mp],T_mp,pressure,&melt_fraction);
                    MPntPStokesMeltSetField_melt_portion(mpprop_stokesmelt,melt_fraction);
                    
                    eta_melt      = rheology->melt_eta[phase_mp];
                    UnitsApplyInverseScaling(units->si_viscosity,eta_melt,&eta_melt_mp);
                    eta_mp   = 1.0/(melt_fraction/eta_melt_mp+(1.0-melt_fraction)/eta_mp);

                    rho_melt      = rheology->melt_rho[phase_mp];
                    UnitsApplyInverseScaling(units->si_force_per_volume,rho_melt,&rho_melt_mp);
                    rho_mp   = rho_mp*(1.0-melt_fraction)+melt_fraction*rho_melt_mp;
                }
                    break;
			
			}
          }
		if (plastic) {
			
			switch (rheology->plastic_type[phase_mp]) {
					
				case PLASTIC_NONE: {
				}
					break;
					
				case PLASTIC_M: {
					PetscScalar tauyield,dummy,tauyield_mp,stressinv_mp;
					
					ApplyPlasticSofteningMarker(rheology,phase_mp, &tauyield, &dummy, mpprop_stokespl, 0.0, user->step);
					UnitsApplyInverseScaling(units->si_stress,tauyield,&tauyield_mp);
					yield_type = YTYPE_MISES; 
					/* viscous stress predictor */ 
					d1 = 2.0 * eta_mp;
					sxx_mp = d1 * exx_mp;
					syy_mp = d1 * eyy_mp;
					sxy_mp = d1 * exy_mp;
					stressinv_mp = sqrt(0.25*(sxx_mp-syy_mp)*(sxx_mp-syy_mp)+sxy_mp*sxy_mp);
					
					/* apply plastic correction when marker is yielding */ 
					if (stressinv_mp > tauyield_mp) {
						//PetscScalar eta_vis = eta_mp;
						eta_mp = 0.5*tauyield_mp/strainrateinv_mp;
						//PetscPrintf(PETSC_COMM_WORLD," eta_vis %e eta_plas %e tauyield %e stress_inv %e \n", eta_vis,eta_mp,tauyield_mp, stressinv_mp);
						npoints_yielded++;
						is_yielding = (char)yield_type; 
					}
				}
					break;
					
				case PLASTIC_DP: {
					PetscScalar tens_cutoff_mp,Hst_cutoff_mp,phi,Co,A,B,Co_mp;
					PetscScalar tauyield_mp,stressinv_mp,eta_mp_pred,eps;    
					MPntPStokesGetField_eta_effective(mpprop_stokes,&eta_mp_pred);
					PetscScalar eta_0 = eta_lower_cutoff_global_mp;
					PetscScalar factor = 1.0; 
					/* viscous stress predictor */ 
					d1 = 2.0 * eta_mp;
					sxx_mp = d1 * exx_mp;
					syy_mp = d1 * eyy_mp;
					sxy_mp = d1 * exy_mp;
					stressinv_mp = sqrt(0.25*(sxx_mp-syy_mp)*(sxx_mp-syy_mp)+sxy_mp*sxy_mp);
					
					UnitsApplyInverseScaling(units->si_stress,rheology->tens_cutoff[phase_mp],&tens_cutoff_mp);
					UnitsApplyInverseScaling(units->si_stress,rheology->Hst_cutoff[phase_mp],&Hst_cutoff_mp);
                    eps = 0.0;
                    if (eta_mp_pred > eta_mp) {
						eps = user->dt*strainrateinv_mp;
					}
					ApplyPlasticSofteningMarker(rheology,phase_mp, &Co, &phi, mpprop_stokespl,eps,user->step);
					A           = sin(phi/180.0*M_PI);
					B           = cos(phi/180.0*M_PI);	
					UnitsApplyInverseScaling(units->si_stress,Co,&Co_mp);
					tauyield_mp = A * pressure_mp + B * Co_mp; //+stressinv_mp/1000.;    
					yield_type  = (char)YTYPE_DP;
					
					if (tauyield_mp < tens_cutoff_mp) {
						/* failure in tension cutoff */
						tauyield_mp = tens_cutoff_mp;
						yield_type = (char)YTYPE_TENSILE_FAILURE;
					} else if (tauyield_mp > Hst_cutoff_mp) {   
						/* failure at High stress cut off à la boris */
						tauyield_mp = Hst_cutoff_mp;
						yield_type = (char)YTYPE_MISES;
					}  
					
					/* apply plastic correction when marker is yielding */ 
					
					if (stressinv_mp > tauyield_mp) {
					    PetscScalar eta_vis = eta_mp; 
						eta_mp = 0.5*tauyield_mp/strainrateinv_mp;
						// PetscPrintf(PETSC_COMM_WORLD," eta_vis %e eta_plas %e tauyield %e stress_inv %e \n", eta_vis,eta_mp,tauyield_mp, stressinv_mp);
	
						//if (eta_mp/eta_vis < 0.00001){
						npoints_yielded++;
						//if ( yield_type > 1) PetscPrintf(PETSC_COMM_WORLD," yield type %d \n",yield_type);
						is_yielding = yield_type;
                        //PetscPrintf(PETSC_COMM_WORLD,"yield =%d\n",yield_type);
						//}
						 /*Apply regularisation à la Dave */                        
                        eta_mp = eta_mp;//+ 100.0*eta_0; 
                        //eta_mp = pow( eta_mp, -factor) + pow(eta_0, -factor);
                        //eta_mp = pow( eta_mp, -1.0/factor );
					}
					
				}
					break;
			}
            
			MPntPStokesPlSetField_yield_indicator(mpprop_stokespl,is_yielding);
		}
		/* Global cuttoffs for viscosity*/
		if (eta_mp > eta_upper_cutoff_global_mp) {
			eta_mp = eta_upper_cutoff_global_mp;
		}
		if (eta_mp < eta_lower_cutoff_global_mp) {
			eta_mp = eta_lower_cutoff_global_mp;
		}
		
		/* update viscosity on marker */
		MPntPStokesSetField_eta_effective(mpprop_stokes,eta_mp);
		
		/* update density on marker */
		/* density at mp */
		
		MPntPStokesSetField_density(mpprop_stokes,rho_mp);
		
		/* monitor bounds */
		if (eta_mp > max_eta) { max_eta = eta_mp; }
		if (eta_mp < min_eta) { min_eta = eta_mp; }
		/* monitor bounds */
		if (rho_mp > max_rho) { max_rho = rho_mp; }
		if (rho_mp < min_rho) { min_rho = rho_mp; }
	}
	
	PetscGetTime(&t1);
    
#if 0
	ierr = MPI_Allreduce(&min_eta,&min_eta_g,1, MPI_DOUBLE, MPI_MIN, PETSC_COMM_WORLD);CHKERRQ(ierr);
	ierr = MPI_Allreduce(&max_eta,&max_eta_g,1, MPI_DOUBLE, MPI_MAX, PETSC_COMM_WORLD);CHKERRQ(ierr);
	
	ierr = MPI_Allreduce(&min_rho,&min_rho_g,1, MPI_DOUBLE, MPI_MIN, PETSC_COMM_WORLD);CHKERRQ(ierr);
	ierr = MPI_Allreduce(&max_rho,&max_rho_g,1, MPI_DOUBLE, MPI_MAX, PETSC_COMM_WORLD);CHKERRQ(ierr);
    
	PetscPrintf(PETSC_COMM_WORLD,"Update stokes non linearities (VPT) ND [mpoint]: (min,max)_eta %1.2e,%1.2e; log10(max/min) %1.2e; (min,max)_rho %1.2e,%1.2e; cpu time %1.2e (sec)\n",  min_eta_g, max_eta_g, log10(max_eta_g/min_eta_g),min_rho_g, max_rho_g, t1-t0 );
#endif
	
	UnitsApplyScaling(units->si_viscosity,min_eta_g,&min_eta_g);
	UnitsApplyScaling(units->si_viscosity,max_eta_g,&max_eta_g);
	UnitsApplyScaling(units->si_force_per_volume,min_rho_g,&min_rho_g);
	UnitsApplyScaling(units->si_force_per_volume,max_rho_g,&max_rho_g);
	
    //  PetscPrintf(PETSC_COMM_WORLD,"Update stokes non linearities (VPT) SI [mpoint]: (min,max)_eta %1.2e,%1.2e; log10(max/min) %1.2e; (min,max)_rho %1.2e,%1.2e; cpu time %1.2e (sec)\n",  min_eta_g, max_eta_g, log10(max_eta_g/min_eta_g),min_rho_g, max_rho_g, t1-t0 );
	
	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokes);
	
	
	if (plastic) { 
		DataFieldRestoreAccess(PField_stokespl);
#if 0		
		ierr = MPI_Allreduce(&npoints_yielded,&npoints_yielded_g,1, MPI_INT, MPI_SUM, PETSC_COMM_WORLD);CHKERRQ(ierr);
		PetscPrintf(PETSC_COMM_WORLD,"Update stokes non linearities (VPT) [mpoint]: npoints_yielded %d\n", npoints_yielded_g);
#endif		
	}
	
	if (melt) { 
		DataFieldRestoreAccess(PField_stokesmelt);
	}
	if (active_thermal_energy) {
		DataFieldRestoreAccess(PField_thermal);
		ierr = VecRestoreArray(T_local_vec,&LA_T);CHKERRQ(ierr);
		ierr = DMRestoreLocalVector(daT,&T_local_vec);CHKERRQ(ierr);		
	}
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "TkUpdateRheologyMarkerStokes_VPT"
PetscErrorCode TkUpdateRheologyMarkerStokes_VPT(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[])
{	
	PetscErrorCode ierr;
	int         pidx,n_mp_points;
	DataBucket  db;
	DataField   PField_std, PField_stokes, PField_stokespl, PField_thermal,PField_stokesmelt;
	PetscScalar GNIu[2][Q2_NODES_PER_EL_2D],NIp[P_BASIS_FUNCTIONS];
	PetscScalar NIu[U_BASIS_FUNCTIONS];
	PetscScalar NIT[NODES_PER_EL_Q1_2D];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen_u,nen_p,nen_T,e,i,j,k;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	const PetscInt *elnidx_T;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	PetscScalar elT[NODES_PER_EL_Q1_2D];
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	PetscInt vel_el_lidx[U_BASIS_FUNCTIONS*2];
	PetscInt *gidx;
	PetscScalar  d1,exx_mp,eyy_mp,exy_mp,eta_mp,strainrateinv_mp,u_mp,v_mp;
    pTatinUnits *units;
	RheologyConstants *rheology;
	BTruth plastic;
	BTruth melt    = PETSC_FALSE;
	PetscBool         active_thermal_energy = PETSC_FALSE;    
	PhysCompEnergyCtx phys_energy;
	Vec               T_vec,T_local_vec;
	DM                daT;
	PetscScalar       *LA_T;
	
	
	PetscFunctionBegin;
	db = user->db;
    units   = &user->units;
	
	DataBucketQueryDataFieldByName(db,MPntPStokesPl_classname,&plastic);
    ierr = pTatinPhysCompActivated(user,PhysComp_Energy,&active_thermal_energy);CHKERRQ(ierr);
    
	if (plastic) {
		
		/* setup for coords */
		ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
		ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
		ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
		
		ierr = DMDAGetGlobalIndices(dau,0,&gidx);CHKERRQ(ierr);
		
		ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
		ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
		
		/* setup for rheological parameters depending on phases */
		rheology  = &user->rheology_constants;
		
		/* marker loop */
		
		
		DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
		DataFieldGetAccess(PField_std);
		DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
		
		DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
		DataFieldGetAccess(PField_stokes);
		DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));
		
		
		DataBucketGetDataFieldByName(db,MPntPStokesPl_classname,&PField_stokespl);
		DataFieldGetAccess(PField_stokespl);
		DataFieldVerifyAccess(PField_stokespl,sizeof(MPntPStokesPl));
		
		if (active_thermal_energy){
		DataBucketGetDataFieldByName(db,MPntPThermal_classname,&PField_thermal);
		DataFieldGetAccess(PField_thermal);
		DataFieldVerifyAccess(PField_thermal,sizeof(MPntPThermal));
		}
		
		DataBucketGetSizes(db,&n_mp_points,0,0);
		
		
		for (pidx=0; pidx<n_mp_points; pidx++) {
			MPntStd       *material_point;
			MPntPStokes   *mpprop_stokes;
			MPntPStokesPl *mpprop_stokespl;
			MPntPThermal  *mpprop_thermal;
			double        *position, *xip;
			
			int           wil,phase_mp;
			char          is_yielding;
			float         eplastic,eviscous;
			PetscScalar   J[2][2], iJ[2][2];
			PetscScalar   J_p,ojp;
			
			
			DataFieldAccessPoint(PField_std,     pidx,(void**)&material_point);
			DataFieldAccessPoint(PField_stokes,  pidx,(void**)&mpprop_stokes);
			DataFieldAccessPoint(PField_stokespl,pidx,(void**)&mpprop_stokespl);
			
			
			
			MPntPStokesPlGetField_yield_indicator(mpprop_stokespl,&is_yielding);
			/* check the particle is yielding */
			
		//	if (is_yielding != YTYPE_NONE) {
				
				MPntStdGetField_local_element_index(material_point,&wil);
				MPntStdGetField_global_coord(material_point,&position);
				MPntStdGetField_local_coord(material_point,&xip);
				MPntStdGetField_phase_index(material_point,&phase_mp);
				MPntPStokesPlGetField_plastic_strain(mpprop_stokespl,&eplastic);
				MPntPStokesPlGetField_viscous_strain(mpprop_stokespl,&eviscous);
				
				e = wil;
				/* get nodal properties for element e */
				ierr = StokesVelocity_GetElementLocalIndices(vel_el_lidx,(PetscInt*)&elnidx_u[nen_u*e]);CHKERRQ(ierr);
				ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
				ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],u);CHKERRQ(ierr);
				ierr = DMDAGetScalarElementField_2D(elp,nen_p,(PetscInt*)&elnidx_p[nen_p*e],p);CHKERRQ(ierr);
				
				/* get element velocity/coordinates in component form */
				for (i=0; i<Q2_NODES_PER_EL_2D; i++) {
					ux[i] = elu[2*i  ];
					uy[i] = elu[2*i+1];
					
					xc[i] = elcoords[2*i  ];
					yc[i] = elcoords[2*i+1];
				}
				
				PTatinConstructNI_Q2_2D(xip,NIu);
				PTatinConstructGNI_Q2_2D(xip,GNIu);
				ConstructNi_pressure(xip,elcoords,NIp);
				
				
				
				/* coord transformation */
				for (i=0; i<2; i++) {
					for (j=0; j<2; j++) { J[i][j] = 0.0; }
				}
				for (k=0; k<Q2_NODES_PER_EL_2D; k++) {
					J[0][0] += GNIu[0][k] * xc[k] ;
					J[0][1] += GNIu[0][k] * yc[k] ;
					
					J[1][0] += GNIu[1][k] * xc[k] ;
					J[1][1] += GNIu[1][k] * yc[k] ;
				}
				J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
				ojp = 1.0/J_p;
				iJ[0][0] =  J[1][1]*ojp;
				iJ[0][1] = -J[0][1]*ojp;
				iJ[1][0] = -J[1][0]*ojp;
				iJ[1][1] =  J[0][0]*ojp;
				
				/* global derivs at each quadrature point */
				for (k=0; k<Q2_NODES_PER_EL_2D; k++) {
					nx[k] = iJ[0][0]*GNIu[0][k] + iJ[0][1]*GNIu[1][k];
					ny[k] = iJ[1][0]*GNIu[0][k] + iJ[1][1]*GNIu[1][k];
				}
				
				/* u,v and strain rate (e= B u) at gp */
				u_mp = v_mp = 0.0;
				exx_mp = eyy_mp = exy_mp = 0.0;
				for (k=0; k<U_BASIS_FUNCTIONS; k++) {
					u_mp   += NIu[k] * ux[k];
					v_mp   += NIu[k] * uy[k];
					
					exx_mp += nx[k] * ux[k];
					eyy_mp += ny[k] * uy[k];
					exy_mp += ny[k] * ux[k] + nx[k] * uy[k];
				}
				exy_mp = 0.5 * exy_mp;
				
				
				strainrateinv_mp = sqrt(0.25*(exx_mp-eyy_mp)*(exx_mp-eyy_mp)+exy_mp*exy_mp);
			
			if (is_yielding != YTYPE_NONE) {	
				/* community accepted version */ 	
				eplastic += strainrateinv_mp * user->dt;
				
				MPntPStokesPlSetField_plastic_strain(mpprop_stokespl,eplastic);
			}
			
			if (is_yielding == YTYPE_NONE){
				eviscous += strainrateinv_mp * user->dt;
				MPntPStokesPlSetField_viscous_strain(mpprop_stokespl,eviscous);
			}
			 
			 if(active_thermal_energy){
               PetscScalar eta_mp,H_mp,H0_mp,rho_mp, cp, cp_mp,Wshear_mp, rho,h_shear_max;
                
               PetscScalar fac =0.0, time_start=0.0, timeSI;
               cp    = 1000.0; // J/kg/K --> m^2s-2 --> m^2s-1 *s-1
               h_shear_max=1E32;
                 UnitsApplyInverseScaling(units->si_heatcapacity,cp,&cp_mp);
                 
               ierr  = PetscOptionsGetReal(PETSC_NULL,"-shear_heating_factor",&fac,0);CHKERRQ(ierr);
                 
                 UnitsApplyScaling(units->si_time,user->time,&timeSI);
                 ierr = PetscOptionsGetScalar(PETSC_NULL,"-shear_heating_start_time",&time_start,0);CHKERRQ(ierr);
                 if (time_start>timeSI){
                     fac = 0.0;
                 }
                 
               DataFieldAccessPoint(PField_thermal,pidx,(void**)&mpprop_thermal);
			   DataFieldAccessPoint(PField_stokes,pidx,(void**)&mpprop_stokes);
			   MPntPThermalGetField_heat_prod(mpprop_thermal,&H0_mp);
			   MPntPStokesGetField_eta_effective(mpprop_stokes,&eta_mp);
               MPntPStokesGetField_density(mpprop_stokes,&rho_mp);
               UnitsApplyScaling(units->si_force_per_volume,rho_mp,&rho);
               UnitsApplyInverseScaling(units->si_density,rho,&rho_mp);
               
               Wshear_mp = 2*eta_mp*(exx_mp*exx_mp + 2.0*exy_mp*exy_mp +eyy_mp*eyy_mp);
               ierr = PetscOptionsGetScalar(PETSC_NULL,"-h_shear_max",&h_shear_max,PETSC_NULL);CHKERRQ(ierr);
               UnitsApplyInverseScaling(units->si_heatsource,h_shear_max,&h_shear_max);
               if (Wshear_mp > h_shear_max) {Wshear_mp = h_shear_max;}
                 
			   H_mp = H0_mp+Wshear_mp/rho_mp/cp_mp*fac;
			   MPntPThermalSetField_heat_source(mpprop_thermal,H_mp);
			   }
			/// this needs to be integrated here and removed for non linearity  
 
		}
	
	
	
	
#if 0           
			switch (rheology->density_type[phase_mp]) {
				case DENSITY_CONSTANT:{
					rho = rheology->const_rho0[phase_mp];
					UnitsApplyInverseScaling(units->si_force_per_volume,rho,&rho_mp);
				}
					break; 
				case DENSITY_FUNCTION:{
					PetscScalar alpha_mp = rheology->temp_alpha[phase_mp];
					PetscScalar beta_mp; 
					UnitsApplyScaling(units->si_stress,rheology->temp_beta[phase_mp],&beta_mp);
					
					rho = rheology->const_rho0[phase_mp]*(1-alpha_mp*T_mp+beta_mp*pressure_mp);        
					UnitsApplyInverseScaling(units->si_force_per_volume,rho,&rho_mp);
				}
					break; 
				case DENSITY_TABLE:{
					double position[2],rhodummy;
					char *name;
					rho = rheology->const_rho0[phase_mp];
					UnitsApplyInverseScaling(units->si_force_per_volume,rho,&rho_mp);
					
					asprintf(&name,"densmap_%d",phase_mp);
					TempMap map = PETSC_NULL;
					ierr = pTatinCtxGetMap(user,&map,name);CHKERRQ(ierr);
					position[0] = T_mp;
					position[1] = pressure_mp;
					TempMapGetDouble(map,position,&rhodummy);
					if (rhodummy == -1){
						PetscPrintf(PETSC_COMM_WORLD,"marker outside PT array,P= %e T=%e \n",pressure_mp,T_mp);
					}else if(rhodummy==1) {
						PetscPrintf(PETSC_COMM_WORLD,"I don' get what PT array does");                    
					}else{
						rho_mp = rhodummy;
						
					}
				}
					break;     
					
			}
#endif		
		DataFieldRestoreAccess(PField_std);
		DataFieldRestoreAccess(PField_stokes);
		DataFieldRestoreAccess(PField_stokespl);
		if(active_thermal_energy){
		     DataFieldRestoreAccess(PField_thermal);
		}
		
		ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	}
	PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "TkUpdateRheologyQuadraturePoints_VPT"
PetscErrorCode TkUpdateRheologyQuadraturePoints_VPT(pTatinCtx user,DM dau)
{	
    PetscErrorCode ierr;
    PetscInt e,nel, n, ngp, dum;
    const PetscInt *dum2;
    GaussPointCoefficientsStokes *gausspoints;
    
	PetscFunctionBegin;
    ierr = DMDAGetElements_pTatin(dau, &nel, &dum, &dum2);CHKERRQ(ierr);
    ngp  = user->Q->ngp;
    PetscPrintf(PETSC_COMM_WORLD,"  VPT: Tk Update  [qpoint]\n");
    
    for (e=0;e<nel;e++) {
        ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
        for (n=0; n<ngp; n++) {
            
            gausspoints[n].pls += gausspoints[n].plsr * user->dt ; 
            if (gausspoints[n].pls < 0.0){
                gausspoints[n].pls = 0.0;
            }
        }
    }
    
    PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ApplyContinuationViscousCutOffMarker"
PetscErrorCode ApplyContinuationViscousCutOffMarker(pTatinCtx user)

{
	PetscErrorCode ierr;
	int         pidx,n_mp_points;
	RheologyConstants *rheology;
	DataBucket  db;
	PetscScalar eta_eff_continuation_min,eta_eff_continuation_max,min_eta_cut,max_eta_cut,alpha_m,contrast,eta_mp;
	PetscScalar min_eta,max_eta,min_eta_g,max_eta_g;
	DataField  PField_stokes;
	pTatinUnits *units;	
	
	PetscFunctionBegin;
	
	units   = &user->units;
	/* continuation parameter */
	rheology =  &user->rheology_constants;
	min_eta_cut = rheology->eta_lower_cutoff_global;
	max_eta_cut = rheology->eta_upper_cutoff_global;
	
	alpha_m = (double)(user->continuation_m+1)/( (double)(user->continuation_M+1) );
	contrast=log10(max_eta_cut/min_eta_cut);
	eta_eff_continuation_max = min_eta_cut*pow(10.0,(alpha_m*contrast));
	eta_eff_continuation_min = min_eta_cut;
	
	PetscPrintf(PETSC_COMM_WORLD," continuation viscosity cut-off:max  %1.6e  min %1.6e \n", eta_eff_continuation_max,eta_eff_continuation_min);
	db = user->db;
	/// Scale the viscosity cuttoff
	UnitsApplyInverseScaling(units->si_viscosity,eta_eff_continuation_min,&eta_eff_continuation_min);
	UnitsApplyInverseScaling(units->si_viscosity,eta_eff_continuation_max,&eta_eff_continuation_max);
	
	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	min_eta = 1.0e100;
	max_eta = 1.0e-100;
	for (pidx=0; pidx<n_mp_points; pidx++) {
		MPntPStokes  *mpprop_stokes;
		
		DataFieldAccessPoint(PField_stokes, pidx,(void**)&mpprop_stokes);        
		
		MPntPStokesGetField_eta_effective(mpprop_stokes,&eta_mp);
		/* Continuation cutt off */ 
		if (eta_mp > eta_eff_continuation_max) eta_mp = eta_eff_continuation_max ;
		if (eta_mp < eta_eff_continuation_min) eta_mp = eta_eff_continuation_min ;    
		/* update viscosity on marker */
		MPntPStokesSetField_eta_effective(mpprop_stokes,eta_mp);
		if (eta_mp > max_eta) { max_eta = eta_mp; }
		if (eta_mp < min_eta) { min_eta = eta_mp; }
		
	}
	MPI_Allreduce(&min_eta,&min_eta_g,1, MPI_DOUBLE, MPI_MIN, PETSC_COMM_WORLD);
	MPI_Allreduce(&max_eta,&max_eta_g,1, MPI_DOUBLE, MPI_MAX, PETSC_COMM_WORLD);
	
	// scale the viscosity back to SI for printing
	UnitsApplyScaling(units->si_viscosity,min_eta_g,&min_eta_g);
	UnitsApplyScaling(units->si_viscosity,max_eta_g,&max_eta_g);
	
	
	PetscPrintf(PETSC_COMM_WORLD,"Continuation viscosity (VPT) [mpoint]: (min,max)_eta %1.2e,%1.2e; log10(max/min) %1.2e\n",  min_eta_g, max_eta_g, log10(max_eta_g/min_eta_g));
	
	DataFieldRestoreAccess(PField_stokes);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ApplyContinuationViscousCutOffQuadrature"
PetscErrorCode ApplyContinuationViscousCutOffQuadrature(pTatinCtx user)

{
	PetscErrorCode ierr;
	RheologyConstants *rheology;
	PetscScalar eta_eff_continuation_min,eta_eff_continuation_max,min_eta_cut,max_eta_cut,alpha_m,contrast,eta_mp;
	PetscScalar min_eta,max_eta,min_eta_g,max_eta_g;
	pTatinUnits *units;	
	PetscInt ncells,ngp,e,p;
	GaussPointCoefficientsStokes *gausspoints;
	
	PetscFunctionBegin;
	
	units   = &user->units;
	/* continuation parameter */
	rheology =  &user->rheology_constants;
	min_eta_cut = rheology->eta_lower_cutoff_global;
	max_eta_cut = rheology->eta_upper_cutoff_global;
	
	alpha_m = (double)(user->continuation_m+1)/( (double)(user->continuation_M+1) );
	contrast=log10(max_eta_cut/min_eta_cut);
	eta_eff_continuation_max = min_eta_cut*pow(10.0,(alpha_m*contrast));
	eta_eff_continuation_min = min_eta_cut;
	
	PetscPrintf(PETSC_COMM_WORLD,"  VPT: Continuation viscosity cut-off; (min/max)  %1.6e / %1.6e \n", eta_eff_continuation_min,eta_eff_continuation_max);
    
	/// Scale the viscosity cuttoff
	UnitsApplyInverseScaling(units->si_viscosity,eta_eff_continuation_min,&eta_eff_continuation_min);
	UnitsApplyInverseScaling(units->si_viscosity,eta_eff_continuation_max,&eta_eff_continuation_max);
    
	/* quadrature point loop */
	ncells = user->Q->ncells;
	ngp    = user->Q->ngp;
	
	min_eta = 1.0e100;
	max_eta = 1.0e-100;
	for (e=0; e<ncells; e++) {
		
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		
		for (p=0; p<ngp; p++) {
			//MPntPStokesGetField_eta_effective(mpprop_stokes,&eta_mp);
			eta_mp = gausspoints[p].eta;
			
			/* Continuation cutt off */ 
			if (eta_mp > eta_eff_continuation_max) { eta_mp = eta_eff_continuation_max; }
			if (eta_mp < eta_eff_continuation_min) { eta_mp = eta_eff_continuation_min; }
            
			/* update viscosity on marker */
			//MPntPStokesSetField_eta_effective(mpprop_stokes,eta_mp);
			gausspoints[p].eta = eta_mp;
			
			if (eta_mp > max_eta) { max_eta = eta_mp; }
			if (eta_mp < min_eta) { min_eta = eta_mp; }
		}
		
	}
	ierr = MPI_Allreduce(&min_eta,&min_eta_g,1, MPI_DOUBLE, MPI_MIN, PETSC_COMM_WORLD);CHKERRQ(ierr);
	ierr = MPI_Allreduce(&max_eta,&max_eta_g,1, MPI_DOUBLE, MPI_MAX, PETSC_COMM_WORLD);CHKERRQ(ierr);
	
	// scale the viscosity back to SI for printing
	UnitsApplyScaling(units->si_viscosity,min_eta_g,&min_eta_g);
	UnitsApplyScaling(units->si_viscosity,max_eta_g,&max_eta_g);
	
	
	PetscPrintf(PETSC_COMM_WORLD,"  VPT: Continuation viscosity [qpoint]: (min/max)_eta %1.2e / %1.2e; log10(max/min) %1.2e\n",  min_eta_g, max_eta_g, log10(max_eta_g/min_eta_g));
	
	PetscFunctionReturn(0);
}


/*
 #undef __FUNCT__
 #define __FUNCT__ "ApplyContinuationWinkler"
 PetscErrorCode ApplyContinuationViscousCutOffMarker(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[])
 {
 Vec             Lvelocity, gcoords;
 PetscScalar     ***LA_velocity;
 DMDACoor2d      **LA_coords;
 PetscInt        i,j,si,sj,nx,ny,NX,NY;
 DM              cda;
 double          dt_min_local, dt_min;
 double          dt_min_local2, dt_min2;
 PetscReal       dt_step2;
 PetscReal       dt_step,surface_dy,surface_vy;
 MPI_Comm        comm;
 PetscErrorCode  ierr;
 
 PetscFunctionBegin;
 
 
 //setup velocity 
 ierr = DMGetLocalVector(dau,&Lvelocity);CHKERRQ(ierr);
 ierr = DMGlobalToLocalBegin(dau,velocity,INSERT_VALUES,Lvelocity);CHKERRQ(ierr);
 ierr = DMGlobalToLocalEnd(  dau,velocity,INSERT_VALUES,Lvelocity);CHKERRQ(ierr);
 ierr = DMDAVecGetArrayDOF(dau,Lvelocity,&LA_velocity);CHKERRQ(ierr);
 
 
 
 dt_min_local = 1.0e32;
 dt_min_local2 = 1.0e32;
 
 ierr = DMDAGetInfo(dau,0, &NX,&NY,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
 ierr = DMDAGetGhostCorners(dau,&si,&sj,0 , &nx,&ny,0);CHKERRQ(ierr);
 mean_surf_vy = 0.0;
 n_surf_local = 0.0;
 for( j=sj; j<sj+ny; j++ ) {
 for( i=si; i<si+nx; i++ ) {
 mean_vy= LA_velocity[j][i][1];
 if (j==NY-1) 
 {
 surf_vy = LA_velocity[j][i][1];
 if (surf_vy < surf_min_local) { surf_min_local = surf_vy; }
 if (surf_vy > surf_max_local) { surf_max_local = surf_vy; }                
 mean_surf_vy +=surf_vy;
 
 }
 }
 }
 
 ierr = DMDAVecRestoreArray(cda,gcoords,&LA_coords);CHKERRQ(ierr);
 ierr = DMDAVecRestoreArrayDOF(da,Lvelocity,&LA_velocity);CHKERRQ(ierr);
 ierr = DMRestoreLocalVector(da,&Lvelocity);CHKERRQ(ierr);
 
 ierr = PetscObjectGetComm((PetscObject)da,&comm);CHKERRQ(ierr);
 
 ierr = MPI_Allreduce(&surf_min_local,&surf_min,1,MPI_DOUBLE,MPI_MIN,comm);CHKERRQ(ierr);
 ierr = MPI_Allreduce(&surf_max_local,&surf_max,1,MPI_DOUBLE,MPI_MAX,comm);CHKERRQ(ierr);
 
 ierr = MPI_Allreduce(&mean_surf_vy_local,&mean_surf_vy,1,MPI_DOUBLE,MPI_SUM,comm);CHKERRQ(ierr);    
 
 ierr = MPI_Allreduce(&mean_vy_local,&mean_vy,1,MPI_DOUBLE,MPI_SUM,comm);CHKERRQ(ierr); 
 
 
 
 PetscPrintf(PETSC_COMM_WORLD,"UpdateMeshGeometry_SurfaceCourantStep: dt_surf = %1.4e * Cr (Cr = %1.2e) \n", dt_min, Courant_surf );
 dt_min = dt_min * Courant_surf;
 
 PetscPrintf(PETSC_COMM_WORLD,"UpdateMeshGeometry_SurfaceCourantStep: dt_surf = %1.4e \n", dt_min2);
 
 if (dt_min2 < dt_min) {dt_min = dt_min2;}  
 *step = dt_min;
 
 PetscFunctionReturn(0);
 
 
 }
 */


#undef __FUNCT__
#define __FUNCT__ "Compute_melt_fraction_mumu"
PetscErrorCode Compute_melt_fraction_mumu(RheologyConstants *rheology,PetscInt phase_mp,PetscScalar T_mp,PetscScalar P_mp,float *melt_fraction)
{
	PetscScalar T_sol,T_inf,M;
	PetscFunctionBegin;
	T_sol = rheology->melt_Tsol[phase_mp];
	T_inf = rheology->melt_Tinf[phase_mp];
	M = (T_mp-T_sol)/(T_inf-T_sol); 
	if (M < 0.0) {
		M = 0.0; 
	} else if (M > 1.0) 
	{
		M = 1.0;
	}
	
	*melt_fraction = (float)M; 
		
	PetscFunctionReturn(0);

}
                                             
#undef __FUNCT__
#define __FUNCT__ "Compute_melt_fraction_P"
                                             PetscErrorCode Compute_melt_fraction_P(PetscInt type_mp,PetscScalar T_mp,PetscScalar P,float *melt_fraction)
                    {
                        PetscScalar T_sol,T_inf,M,a,b,c;
                        PetscFunctionBegin;
                        P = P/1e9; // in GPa in the Paper
                        if (type_mp == 0) {
                        // number from Hirshmann peridotite solidus G3 2000  2000GC000070
                        a = -5.904;
                        //    a= -4.3;
                        b = 139.44;
                        //    b=100.0;
                        c = 1108.08; // in °C in the Paper

                        T_sol = a*P*P+b*P+c;
                         
                        T_inf = T_sol+300.;// figure 2
                   //         PetscPrintf(PETSC_COMM_WORLD," P= %e Tsol= %e T_mp %e type_mp %d \n", P,T_sol,T_mp,type_mp );
                            
                        } else {
                        
                        //Miller, C.F., McDowell, S.M. and Mapes, R.W., 2003. Hot and cold granites? Implications of zircon saturation temperatures and preservation of inheritance. Geology, 31(6), pp.529-532. Wet granite solidus
                        

                        T_sol  = 1./(P+0.1)/10*250+590;
                        T_inf = 900.;
                       //     PetscPrintf(PETSC_COMM_WORLD," P= %e Tsol= %e T_mp %e type_mp %d \n", P,T_sol,T_mp,type_mp );
                        }
                        
                        M = (T_mp-T_sol)/(T_inf-T_sol);
                        if (M < 0.0) {
                            M = 0.0; 
                        } else if (M > 1.0) 
                        {
                            M = 1.0;
                        }
                        
                        *melt_fraction = (float)M; 
                        
                        PetscFunctionReturn(0);
                        
                    }
                                             
                                             
#undef __FUNCT__
#define __FUNCT__ "ApplyPlasticSofteningMarker"
PetscErrorCode ApplyPlasticSofteningMarker(RheologyConstants *rheology,PetscInt phase_mp,PetscScalar *pCo, PetscScalar *pphi, MPntPStokesPl *mpprop_stokespl,PetscScalar strainrateinv_mp, PetscInt step)
{
	PetscScalar Co,phi;
	
	PetscFunctionBegin;
	
	Co             = rheology->mises_tau_yield[phase_mp];     
	phi            = rheology->dp_pressure_dependance[phase_mp];
	
    if (step > 0) {
	switch (rheology->softening_type[phase_mp]) {
		case SOFTENING_NONE: {
		}
			break;
			
		case SOFTENING_LINEAR: {
			PetscScalar epl_softmin = rheology->soft_min_strain_cutoff[phase_mp]; 
			PetscScalar epl_softmax = rheology->soft_max_strain_cutoff[phase_mp];
			float eplastic;   
			
			MPntPStokesPlGetField_plastic_strain(mpprop_stokespl,&eplastic);
			eplastic = eplastic+strainrateinv_mp;
			if (eplastic > epl_softmin) {
				if (eplastic > epl_softmax) {
					Co  = rheology->soft_Co_inf[phase_mp];
					phi = rheology->soft_phi_inf[phase_mp];
				} else {
                    float X  = (eplastic-epl_softmin)/(epl_softmax-epl_softmin);
					Co  = Co  - X * (Co-rheology->soft_Co_inf[phase_mp]);
					phi = phi - X * (phi-rheology->soft_phi_inf[phase_mp]);
				}
			}
		}
			break;    
			
		case SOFTENING_EXPONENTIAL: {
			
			PetscScalar phi_inf   = rheology->soft_phi_inf[phase_mp];
			PetscScalar Co_inf    = rheology->soft_Co_inf[phase_mp];
			PetscScalar epl_softmin = rheology->soft_min_strain_cutoff[phase_mp]; 
			PetscScalar epl_softmax = rheology->soft_max_strain_cutoff[phase_mp];
			float eplastic; 
			
			MPntPStokesPlGetField_plastic_strain(mpprop_stokespl,&eplastic);
			if (eplastic > epl_softmin){
				Co  = Co_inf + (Co-Co_inf) * exp(-(eplastic-epl_softmin)/epl_softmax);
				phi  = phi_inf + (phi-phi_inf) * exp(-(eplastic-epl_softmin)/epl_softmax);                    
			}
		}
			break;
			// TODO  ARC TANGEANT etc.... you can add as much softening law as you want in here
			// as long as it is define in rheology 
			
	}
    }
	*pCo = Co;
	*pphi = phi;
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ApplyPlasticSofteningQuadraturePoint"
PetscErrorCode ApplyPlasticSofteningQuadraturePoint(RheologyConstants *rheology,PetscInt phase_mp,PetscScalar *pCo, PetscScalar *pphi, PetscScalar eplastic)
{
	PetscScalar Co,phi;
	
	PetscFunctionBegin;
	
	Co             = rheology->mises_tau_yield[phase_mp];     
	phi            = rheology->dp_pressure_dependance[phase_mp];
	
	switch (rheology->softening_type[phase_mp]) {
            
		case SOFTENING_NONE: {
		}
			break;
			
		case SOFTENING_LINEAR: {
			PetscScalar epl_softmin = rheology->soft_min_strain_cutoff[phase_mp]; 
			PetscScalar epl_softmax = rheology->soft_max_strain_cutoff[phase_mp];
			
			//MPntPStokesPlGetField_plastic_strain(mpprop_stokespl,&eplastic);
			
			if (eplastic > epl_softmin) {
				if (eplastic > epl_softmax) {
					Co  = rheology->soft_Co_inf[phase_mp];
					phi = rheology->soft_phi_inf[phase_mp];
				} else {
					float X  = (eplastic-epl_softmin)/(epl_softmax-epl_softmin);
					
					Co  = Co  - X * (Co-rheology->soft_Co_inf[phase_mp]);
					phi = phi - X * (phi-rheology->soft_phi_inf[phase_mp]);
				}
			}
		}
			break;    
			
		case SOFTENING_EXPONENTIAL: {
			
			PetscScalar phi_inf   = rheology->soft_phi_inf[phase_mp];
			PetscScalar Co_inf    = rheology->soft_Co_inf[phase_mp];
			PetscScalar epl_softmin = rheology->soft_min_strain_cutoff[phase_mp]; 
			PetscScalar epl_softmax = rheology->soft_max_strain_cutoff[phase_mp];
			
			//MPntPStokesPlGetField_plastic_strain(mpprop_stokespl,&eplastic);
			if (eplastic > epl_softmin) {
				Co  = Co_inf + (Co-Co_inf) * exp(-(eplastic-epl_softmin)/epl_softmax);
				phi  = phi_inf + (phi-phi_inf) * exp(-(eplastic-epl_softmin)/epl_softmax);                    
			}
		}
			break;
			// TODO  ARC TANGEANT etc.... you can add as much softening law as you want in here
			// as long as it is define in rheology 
			
	}
	
	*pCo = Co;
	*pphi = phi;
	
	PetscFunctionReturn(0);
}


