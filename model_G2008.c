/*
 
 Model Description:
 Example reading geometry, rheology and everything from file and command line.
 Model size must be smaller than pmap.
 In case of an air layer, it must be prescribed in the map file! (diff with MAP_LV)  
 
 
 Input / command line parameters:
 
 
 
 
 
 
 
 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"
//#include "pTatin2d_models.h"
#define WRITE_VP
#define WRITE_TFIELD
#define WRITE_MPOINTS
//#define WRITE_QPOINTS
#define WRITE_TCELL
//#define WRITE_INT_FIELDS
#define WRITE_FACEQPOINTS



#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_G2008"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_G2008(pTatinCtx ctx)
{
	PhaseMap phasemap;
	PetscInt M,N,experiment;
	PetscBool flg;
	PetscBool found,friction= PETSC_FALSE;
	PetscErrorCode ierr;
	PetscReal Ox,Oy,Lx,Ly,gravity_acceleration;
	char name[PETSC_MAX_PATH_LEN];
	char map_file[PETSC_MAX_PATH_LEN];
	char topo_file[PETSC_MAX_PATH_LEN];
	pTatinUnits *units;	
	
	PetscFunctionBegin;
	units   = &ctx->units;
	
	experiment = 2; 
	friction   = 0;
	PetscOptionsGetInt("G2008_","-experiment",&experiment,0);
	PetscOptionsGetBool("G2008_","-add_fric_layer",&friction,0);
	
	/* make a regular non dimensional grid from option file parameters*/
	found = PETSC_TRUE; 
    gravity_acceleration = 9.81; 
	ierr = QuadratureStokesGravityModelCoordinateAlignedSetValues(ctx->Q,0.0, -gravity_acceleration);CHKERRQ(ierr);
	switch(experiment){
		case 1 : 
			Ox = 0.0;
    		Oy = 0.0;
    		Lx = 13.24e-2;
    		Ly = 4.0e-2;
		break;

		case 2 : 
			Ox = 0.0;
    		Oy = 0.0;
    		Lx = 3.5e-1;
    		Ly = 3.0e-2;
		break;
		
		case 3 : 
			Ox = 0.0;
    		Oy = 0.0;
    		Lx = 3.5e-1;
    		Ly = 3.0e-2;
		break;
	}
	if (friction) {
	Oy=-2.e-3;
	Ox=-2.e-3;
	Lx=Lx+2.e-3;
	
	}
	
	PetscPrintf(PETSC_COMM_WORLD,"ApplyInitialMeshGeometry_G2008: options Domain_SI = [ %1.4e , %1.4e ] x [ %1.4e , %1.4e ] \n", Ox, Lx,Oy,Ly );	
	UnitsApplyInverseScaling(units->si_length,Ox,&Ox);
	UnitsApplyInverseScaling(units->si_length,Oy,&Oy);
	UnitsApplyInverseScaling(units->si_length,Lx,&Lx);
	UnitsApplyInverseScaling(units->si_length,Ly,&Ly);
	
	PetscPrintf(PETSC_COMM_WORLD,"ApplyInitialMeshGeometry_G2008: options Domain_ND = [ %1.4e , %1.4e ] x [ %1.4e , %1.4e ] \n", Ox, Lx,Oy,Ly );
	ierr = DMDASetUniformCoordinates(ctx->dav,Ox,Lx,Oy,Ly, 0.0,0.0);CHKERRQ(ierr);
	
	/* load phase repartition data from file *.pmap */    
	
	flg = PETSC_FALSE;
	ierr = PetscOptionsGetString(PETSC_NULL,"-map_file",map_file,PETSC_MAX_PATH_LEN-1,&flg);CHKERRQ(ierr);

	sprintf(name,"%s.pmap",map_file);
	PhaseMapLoadFromFileScale(name,&phasemap,units);
	
	sprintf(name,"%s_phase_map.gp",map_file);
	PhaseMapViewGnuplot(name,phasemap);
	
	ierr = pTatinCtxAttachPhaseMap(ctx,phasemap);CHKERRQ(ierr);
	PetscPrintf(PETSC_COMM_WORLD,"ApplyInitialMeshGeometry_G2008: phasemap Domain_ND = [ %1.4e , %1.4e ] x [ %1.4e , %1.4e ] \n", phasemap->x0,phasemap->x1, phasemap->y0,phasemap->y1 );
	
	// Check the size of phase map vs domain
	//if (Ox < phasemap->x0 || Oy < phasemap->y0 || Lx > phasemap->x1 || Ly > phasemap->y1 ){
	//	SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Your phasemap is smaller than the domain defined by -Ox,-Oy,-Lx and -Ly \n");
	//}    
	
	PetscFunctionReturn(0);
}
/*pTatin2d_ModelDeformUpperSurfaceFromOptions_GENE
 this function is used to deform the upper surface of the mesh 
 */
#undef __FUNCT__ 
#define __FUNCT__ "pTatin2d_ModelSetMaterialProperty_G2008"
PetscErrorCode pTatin2d_ModelSetMaterialProperty_G2008(pTatinCtx ctx)

{
	RheologyConstants      *rheology;
	PetscInt               nphase, i,rheol_type, M,N,P,air,experiment;
	PetscErrorCode         ierr;
	char   				   *option_name;
	PetscBool              found,friction; 
	DM             dav,dap;    
    
	PetscFunctionBegin;
	
    ierr = DMCompositeGetEntries(ctx->pack,&dav,&dap);CHKERRQ(ierr);
    ierr = DMDAGetInfo(dav,0,&M,&N,&P,0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);	
    
	rheology   = &ctx->rheology_constants;
	rheology->rheology_type = 6;
    
    rheology->eta_upper_cutoff_global = 1.e12;
	rheology->eta_lower_cutoff_global = 1.e1;
	ierr       = PetscOptionsGetReal("GENE_","-eta_max_cut_off",&rheology->eta_upper_cutoff_global,0);CHKERRQ(ierr);
	ierr       = PetscOptionsGetReal("GENE_","-eta_min_cut_off",&rheology->eta_lower_cutoff_global,0);CHKERRQ(ierr);
	experiment = 2; 
	PetscOptionsGetInt("G2008_","-experiment",&experiment,0);
	PetscOptionsGetBool("G2008_","-add_fric_layer",&friction,0);
	nphase = 4;
	    if (friction){
            nphase = 5;
            }
	if (experiment !=1){
	
	for (i=0; i< nphase ;i++) {
		
        rheology->density_type[i]           = 0; 
        rheology->viscous_type[i]           = 0; 
        rheology->const_eta0[i]             = 1e12; // 12
		rheology->plastic_type[i]           = PLASTIC_DP;
        rheology->softening_type[i]         = SOFTENING_LINEAR;
        rheology->soft_min_strain_cutoff[i] = 0.5;//*M*N/288/48; 
        rheology->soft_max_strain_cutoff[i] = 1;//*M*N/288/48; 
        rheology->mises_tau_yield[i]        = 30.0; 
        rheology->tens_cutoff[i]            = 10.0; 		
        rheology->Hst_cutoff[i]             = 5000.0; 
        rheology->soft_Co_inf[i]            = 30.0;
    }
    
    rheology->const_rho0[0]             = 1560.0;
    rheology->dp_pressure_dependance[0] = 36.0;
    rheology->soft_phi_inf[0]           = 31.0;
    rheology->const_rho0[1]             = 1560.0; 
    rheology->dp_pressure_dependance[1] = 36.0;
    rheology->soft_phi_inf[1]           = 31.0;
    rheology->const_rho0[2]             = 1890.0;
    rheology->dp_pressure_dependance[2] = 36.0;
    rheology->soft_phi_inf[2]           = 31.0;
    rheology->const_rho0[3]             = 1890.0;
    rheology->dp_pressure_dependance[3] = 36.0;
    rheology->soft_phi_inf[3]           = 31.0;
    
    if (friction){
    rheology->const_rho0[4]             = 1560.0;
    rheology->dp_pressure_dependance[4] = 16.0;
    rheology->soft_phi_inf[4]           = 15.0;
    }
   
   } else {
   	for (i=0; i< nphase ;i++) {
		
        rheology->density_type[i]           = 0; 
        rheology->viscous_type[i]           = 0; 
        rheology->const_eta0[i]             = 1.0e12;
		rheology->plastic_type[i]           = PLASTIC_DP;
        rheology->softening_type[i]         = SOFTENING_LINEAR;
        rheology->soft_min_strain_cutoff[i] = 0.5*M*N/288/48; 
        rheology->soft_max_strain_cutoff[i] = 1.0*M*N/288/48; 
        rheology->mises_tau_yield[i]        = 30.0; 
        rheology->tens_cutoff[i]            = 10.0; 		
        rheology->Hst_cutoff[i]             = 5000.0; 
        rheology->soft_Co_inf[i]            = 30.0;
        rheology->const_rho0[i]             = 1560.0;
        rheology->dp_pressure_dependance[i] = 36.0;
        rheology->soft_phi_inf[i]           = 31.0;
    }
    if (friction){
    rheology->const_rho0[4]             = 1560.0;
    rheology->dp_pressure_dependance[4] = 16.0;
    rheology->soft_phi_inf[4]           = 15.0;
    }
    
    // air

    rheology->const_rho0[3]             = 0.0;
    rheology->const_eta0[3]             = 1.0e4;
    rheology->density_type[3]           = 0; 
    rheology->viscous_type[3]           = 0; 
    rheology->plastic_type[3]           = 0;
    rheology->softening_type[3]         = 0;
    
    }
    
    
	PetscFunctionReturn(0);
}

#undef __FUNCT__ 
#define __FUNCT__ "ModelSetFrictionalBoundaryType_G2008"
PetscErrorCode ModelSetFrictionalBoundaryType_G2008(pTatinCtx ctx)

{
	SurfaceRheologyConstants    *rheology;
	PetscInt             edge,fe,nfaces,p,ngp;
	PetscInt             edge_phase[4];
	PetscErrorCode       ierr;
	
	PetscFunctionBegin;
	
  rheology = &ctx->surf_rheology_constants;
	rheology->rheology_type = SURF_RHEOLOGY_STRESS;
	
	edge_phase[QUAD_EDGE_Peta] = 0;
	
	edge_phase[QUAD_EDGE_Pxi]  = 1; //1;
	edge_phase[QUAD_EDGE_Nxi]  = 1; //1;
	edge_phase[QUAD_EDGE_Neta] = 1;
	
	rheology->stress_type[0] = STRESS_NONE;
	rheology->stress_type[1] = STRESS_VOLUMETRIC_VISCOSITY;
	
	for (edge=0; edge<QUAD_EDGES; edge++) {
		SurfaceQuadratureStokes sQ;
		SurfaceQPointCoefficientsStokes *qpoint;

		sQ      = ctx->surfQ[edge];
		nfaces  = sQ->nfaces;
		ngp     = sQ->ngp;

		for (fe=0; fe<nfaces; fe++) {
			ierr = SurfaceQuadratureStokesGetCell(sQ,fe,&qpoint);CHKERRQ(ierr);

			for (p=0; p<ngp; p++) {
				qpoint[p].phase = edge_phase[edge];
			}
		}
	}

	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_G2008"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_G2008(pTatinCtx ctx)
{
	BCList         ubclist;
	PetscScalar    bcval;
	DM             dav;
	PetscErrorCode ierr;
	
	PetscReal jmin[3],jmax[3]; 
	
	PetscScalar opts_srH, opts_Lx,opts_Ly,p_base;
	PetscInt opts_bcs,experiment;
	PetscScalar alpha_m;
	pTatinUnits *units;
	BC_Geomod3   bcdatageomod3;
	PetscScalar short_vel;
    PetscBool friction=PETSC_FALSE;
    PetscFunctionBegin;
	
	experiment  = 2; 
	units   = &ctx->units;
	ubclist = ctx->u_bclist;
	dav     = ctx->dav;
	
	UnitsApplyInverseScaling(units->si_velocity,-6.94444444e-6,&short_vel);
	PetscPrintf(PETSC_COMM_WORLD,"ApplyBC_G2008: shortening vel ADIM =  %1.4e \n", short_vel);
	
	PetscOptionsGetInt("G2008_","-experiment",&experiment,0);
	PetscOptionsGetBool("G2008_","-add_fric_layer",&friction,0);
	 
	 switch (experiment) {
	        
	        case 1 : 
	        bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = short_vel;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	        break; 
	        
	        case 2 :
	        bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = short_vel;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			//if (friction) {
     		bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
     		bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = 0.0;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC_NC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			//}
			
			break; 
			
			case 3: 
			ierr = PetscMalloc(sizeof(struct _p_BC_Geomod3),&bcdatageomod3);CHKERRQ(ierr);
			bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = short_vel;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcdatageomod3->vx    =  short_vel;
			bcdatageomod3->time  =  ctx->time;
		    UnitsApplyInverseScaling(units->si_length,0.23,&bcdatageomod3->x0);
		    bcdatageomod3->dim   =  0;
		    if (friction) {
		    ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,0,BCListEvaluator_Geomod3_fric,bcdatageomod3);CHKERRQ(ierr);
     		bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
			bcval = 0.0;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC_NC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
		    }else{ 
            ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,0,BCListEvaluator_Geomod3,bcdatageomod3);CHKERRQ(ierr);
            }
            ierr = PetscFree(bcdatageomod3);CHKERRQ(ierr);
            break; 
            }
    /*
    if (ctx->continuation_m ==0){
        PetscPrintf(PETSC_COMM_WORLD,"ApplyBC_G2008: applying RIGID BC \n" );
        bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
        bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
        bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
    
    }
     */
    
    PetscFunctionReturn(0);
}



#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelUpdateMeshGeometry_G2008"
PetscErrorCode pTatin2d_ModelUpdateMeshGeometry_G2008(pTatinCtx ctx,Vec X)
{
	static int     beenhere=0;
	PetscReal      step;
	Vec            velocity,pressure;
	DM             dav,dap;
	PetscScalar    Unormal[2],Vnormal_base;
	PetscInt       remesh_type;
	PetscInt       remesh_every;
  Vec            coors,coors_omega_k;
  PetscBool      remesh_occurred = PETSC_FALSE;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	step = ctx->dt;
	
	
    
	ierr = DMCompositeGetEntries(ctx->pack,&dav,&dap);CHKERRQ(ierr);
	ierr = DMCompositeGetAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);

  /*
  // DEBUG
  {
		PetscViewer viewer;
		
		ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, "omega_k_v.vtk",&viewer);CHKERRQ(ierr);
		ierr = PetscViewerSetFormat(viewer,PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
		ierr = DMView(dav,viewer);CHKERRQ(ierr);
		ierr = VecView(velocity,viewer);CHKERRQ(ierr);
		ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  }
	*/
  
	//	ierr = UpdateMeshGeometry_FullLagSurface(dav,velocity,step);CHKERRQ(ierr);

  ierr = DMDAGetCoordinates(dav,&coors);CHKERRQ(ierr);
  ierr = VecDuplicate(coors,&coors_omega_k);CHKERRQ(ierr);
  ierr = VecCopy(coors,coors_omega_k);CHKERRQ(ierr);
  
	remesh_type = 2;
	PetscOptionsGetInt(PETSC_NULL,"-remesh_type",&remesh_type,0);
	
	remesh_every = 1; 
	PetscOptionsGetInt(PETSC_NULL,"-remesh_every",&remesh_every,0);
	
     if (fmod(ctx->step,remesh_every)==0){
	
	switch (remesh_type) {
		case 1:
			if (beenhere) { PetscPrintf(PETSC_COMM_WORLD,"pTatin2d_ModelUpdateMeshGeometry_GENE => RemeshType \"VerticalDisplacemen\"\n"); }
			ierr = UpdateMeshGeometry_VerticalDisplacement(dav,velocity,step);CHKERRQ(ierr);
      remesh_occurred = PETSC_TRUE;
			break;
			
		case 2:
			if (beenhere) { PetscPrintf(PETSC_COMM_WORLD,"pTatin2d_ModelUpdateMeshGeometry_GENE => RemeshType \"FullLagSurfaceJMAX_RemeshInteriorJMIN_JMAX\"\n"); }
			ierr = UpdateMeshGeometry_FullLagSurfaceJMAX_RemeshInteriorJMIN_JMAX(dav,velocity,step);CHKERRQ(ierr);
      remesh_occurred = PETSC_TRUE;
			break;
			
		case 3:
			if (beenhere) { PetscPrintf(PETSC_COMM_WORLD,"pTatin2d_ModelUpdateMeshGeometry_GENE => RemeshType \"NormalVPrescribedWithLagrangianFreeSurface\"\n"); }
			Unormal[0]   = 0.0;
			Unormal[1]   = 0.0;
			Vnormal_base = 0.0;
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-remeshtype3_vx_left",&Unormal[0],0);CHKERRQ(ierr);
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-remeshtype3_vx_right",&Unormal[1],0);CHKERRQ(ierr);
			ierr = PetscOptionsGetScalar(PETSC_NULL,"-remeshtype3_vy_bot",&Vnormal_base,0);CHKERRQ(ierr);
            ierr = UpdateMeshGeometry_NormalVPrescribedWithLagrangianFreeSurface(dav,velocity,Unormal,Vnormal_base,step);CHKERRQ(ierr);
      remesh_occurred = PETSC_TRUE;
			break;
            
        case 4: {
             
            if (beenhere) { PetscPrintf(PETSC_COMM_WORLD,"pTatin2d_ModelUpdateMeshGeometry_GENE => RemeshType \"UpdateMeshGeometry_DecoupledHorizontalVerticalMeshMovement\"\n"); }
            ierr = UpdateMeshGeometry_DecoupledHorizontalVerticalMeshMovement(dav,velocity,step);CHKERRQ(ierr);
          remesh_occurred = PETSC_TRUE;

          
            
			break; 
		}
			
		case 5:
		{
			PetscReal gmin[3],gmax[3];
			DM cdm;
			Vec coordinates,new_mesh_coords;
			PetscScalar delta_x,delta_y;
			PetscInt M,N,P;
			PetscInt span_node_x,span_node_y;
			
			/* update mesh with the full velocity field */
			ierr = UpdateMeshGeometry_FullLagSurface(dav,velocity,step);CHKERRQ(ierr);

			/* define new mesh onto which we wish to interpolate onto */
			ierr = DMDAGetBoundingBox(dav,gmin,gmax);CHKERRQ(ierr);
			
			ierr = DMDAGetCoordinateDA(dav,&cdm);CHKERRQ(ierr);
			ierr = DMDAGetCoordinates(dav,&coordinates);CHKERRQ(ierr);
			ierr = DMCreateGlobalVector(cdm,&new_mesh_coords);CHKERRQ(ierr);
			
			ierr = DMDAGetInfo(dav,0,&M,&N,&P,0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);
			
			/* set uniform mesh */
			ierr = RMTSetUniformCoordinatesRange_X(cdm,new_mesh_coords,0,gmin[0],M,gmax[0]);CHKERRQ(ierr);
			ierr = RMTSetUniformCoordinatesRange_Y(cdm,new_mesh_coords,0,gmin[1],N,gmax[1]);CHKERRQ(ierr);

			/* graduate mesh in y */
			delta_y = 0.2;
			span_node_y = 4;
			ierr = RMTSetUniformCoordinatesRange_Y(cdm,new_mesh_coords,0,          gmin[1],        span_node_y+1,gmin[1]+delta_y);CHKERRQ(ierr);
			ierr = RMTSetUniformCoordinatesRange_Y(cdm,new_mesh_coords,span_node_y,gmin[1]+delta_y,N,            gmax[1]);CHKERRQ(ierr);
			
			/* graduate mesh in x */
			delta_x = 0.2;
			span_node_x = M - 5;
			ierr = RMTSetUniformCoordinatesRange_X(cdm,new_mesh_coords,0,          gmin[0],        span_node_x+1,gmax[0]-delta_x);CHKERRQ(ierr);
			ierr = RMTSetUniformCoordinatesRange_X(cdm,new_mesh_coords,span_node_x,gmax[0]-delta_x,M,            gmax[0]);CHKERRQ(ierr);

//			ierr = RMTSetCoordinates(dav,new_mesh_coords);CHKERRQ(ierr);
//			ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,"test_rms");CHKERRQ(ierr);

			/* interpolate free surface from dav->coords onto new_mesh_coords */
			ierr = UpdateMeshGeometry_Interp(dav,new_mesh_coords);CHKERRQ(ierr);

			/* remesh nodes using upper and lower surfaces */
			ierr = DMDARemeshSetUniformCoordinatesBetweenJLayers2d(dav,0,span_node_y+1);CHKERRQ(ierr);
			ierr = DMDARemeshSetUniformCoordinatesBetweenJLayers2d(dav,span_node_y,N);CHKERRQ(ierr);
			//ierr = DMDARemeshSetUniformCoordinatesBetweenJLayers2d(dav,0,NY);CHKERRQ(ierr);
      remesh_occurred = PETSC_TRUE;
			
//			ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,"test_rms");CHKERRQ(ierr);
//			exit(0);
			
			ierr = VecDestroy(&new_mesh_coords);CHKERRQ(ierr);
			
		}
			break;
			
			
        default:
			ierr = UpdateMeshGeometry_FullLagSurfaceJMAX_RemeshInteriorJMIN_JMAX(dav,velocity,step);CHKERRQ(ierr);
      remesh_occurred = PETSC_TRUE;
			break;
	}
	}else{
	PetscPrintf(PETSC_COMM_WORLD," RemeshType \"UpdateMeshGeometry_FullLagSurfaceG2008\"\n");
	ierr = UpdateMeshGeometry_FullLagSurface_fricG2008(dav,velocity,step);CHKERRQ(ierr);
  remesh_occurred = PETSC_FALSE;
	
		
	}
	
	
	ierr = DMCompositeRestoreAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
	
	/* snappy bilinearize mesh */
	ierr = UpdateMeshGeometry_BilinearizeQ2Elements(dav);CHKERRQ(ierr);

  
	
  /* interpolate velocity */
  if (remesh_occurred) {
    Vec velocity_omega_k;
    
    ierr = DMCompositeGetAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);

    ierr = VecDuplicate(velocity,&velocity_omega_k);CHKERRQ(ierr);
    ierr = VecCopy(velocity,velocity_omega_k);CHKERRQ(ierr);
    
    
    ierr = InterpolateFieldBetweenDMs(dav,coors_omega_k,velocity_omega_k,velocity);CHKERRQ(ierr);

    /* 
     // DEBUG
    {
      PetscViewer viewer;
      
      ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, "omega_v.vtk",&viewer);CHKERRQ(ierr);
      ierr = PetscViewerSetFormat(viewer,PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
      ierr = DMView(dav,viewer);CHKERRQ(ierr);
      ierr = VecView(velocity,viewer);CHKERRQ(ierr);
      ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
    }
     */
    
    ierr = DMCompositeRestoreAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
    ierr = VecDestroy(&velocity_omega_k);CHKERRQ(ierr);
  }
  
  ierr = VecDestroy(&coors_omega_k);CHKERRQ(ierr);
  
  
	beenhere = 1;
	PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelOutput_G2008"
PetscErrorCode pTatin2d_ModelOutput_G2008(pTatinCtx ctx,Vec X,const char prefix[])
{
	static int     beenhere = 0;
	char           pvdfilename[PETSC_MAX_PATH_LEN];
	PetscBool      active,use_dimensional_units;
	char           vtkfilename[PETSC_MAX_PATH_LEN];
	char           filename[PETSC_MAX_PATH_LEN];
	char           name[PETSC_MAX_PATH_LEN];
		PetscErrorCode ierr;
	use_dimensional_units = PETSC_FALSE;
	ierr = PetscOptionsGetBool(PETSC_NULL,"-use_dimensional_units",&use_dimensional_units,PETSC_NULL);CHKERRQ(ierr);
	
	if (use_dimensional_units) {
		ierr = pTatin_MaterialPointScaleProperties(ctx->db,&ctx->units);CHKERRQ(ierr);
	}
	
	if (prefix == PETSC_NULL) {
#ifdef WRITE_QPOINTS
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,"qpoints");CHKERRQ(ierr);
#endif
#ifdef WRITE_FACEQPOINTS
		ierr = pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(ctx,ctx->outputpath,"faceqpoints");CHKERRQ(ierr);
#endif
#ifdef WRITE_VP
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,"vp");CHKERRQ(ierr);
#endif
#ifdef WRITE_INT_FIELDS
		ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,"int_fields");CHKERRQ(ierr);
#endif
#ifdef WRITE_MPOINTS
		ierr = SwarmOutputParaView_MPntPStokes(ctx->db,ctx->outputpath,"mpoints_stk");CHKERRQ(ierr);
#endif
	} else { /* example of adding a different prefix to the file names - not sure this is useful */
		
#ifdef WRITE_QPOINTS
		sprintf(pvdfilename,"%s/timeseries_qpoints.pvd",ctx->outputpath);
		if (beenhere==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		sprintf(vtkfilename, "%s_qpoints.pvtu",prefix);
		ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
		
		sprintf(name,"%s_qpoints",prefix);
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
#endif
		
#ifdef WRITE_FACEQPOINTS
		/*		
		 if (beenhere==0) {
		 ierr = ParaviewPVDOpen("faceqpoints_timeseries.pvd");CHKERRQ(ierr);
		 }
		 sprintf(vtkfilename, "%s_faceqpoints.pvtu",prefix);
		 ierr = ParaviewPVDAppend("faceqpoints_timeseries.pvd",ctx->time, vtkfilename, ctx->outputpath);CHKERRQ(ierr);
		 */ 
		sprintf(name,"%s_faceqpoints",prefix);
		ierr = pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
#endif
        
#ifdef WRITE_VP
		sprintf(pvdfilename,"%s/timeseries_vp.pvd",ctx->outputpath);
		if (beenhere==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		sprintf(vtkfilename, "%s_vp.pvts",prefix);
		ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
		
		sprintf(name,"%s_vp",prefix);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
#endif
        
#ifdef WRITE_INT_FIELDS
		sprintf(pvdfilename,"%s/timeseries_int_fields.pvd",ctx->outputpath);
		if (beenhere==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		sprintf(vtkfilename, "%s_int_fields.pvts",prefix);
		ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
		
		sprintf(name,"%s_int_fields",prefix);
		ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
#endif
		
#ifdef WRITE_MPOINTS
		sprintf(pvdfilename,"%s/timeseries_mpoints_stk.pvd",ctx->outputpath);
		if (beenhere==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		sprintf(vtkfilename, "%s_mpoints_stk.pvtu",prefix);
		ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
		
		sprintf(name,"%s_mpoints_stk",prefix);
		
	    ierr = GENE_SwarmOutputParaView_MPntP(ctx->db,ctx->outputpath,name);CHKERRQ(ierr);
		
#endif
		
	}
	
	
	if (use_dimensional_units) {
		ierr = pTatin_MaterialPointUnScaleProperties(ctx->db,&ctx->units);CHKERRQ(ierr);
	}
	
	beenhere = 1;
	PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetMarkerIndexFromMap_G2008"
PetscErrorCode pTatin2d_ModelSetMarkerIndexFromMap_G2008(pTatinCtx ctx)
{
	PhaseMap               phasemap;
	PetscInt               p,n_mp_points;
	DataBucket             db;
	DataField              PField_std;
	int                    phase_init, phase, phase_index, is_valid;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	phasemap = PETSC_NULL;
	ierr = pTatinCtxGetPhaseMap(ctx,&phasemap);CHKERRQ(ierr);
	
	/* define properties on material points */
	db = ctx->db;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	for (p=0; p<n_mp_points; p++) {
		MPntStd     *material_point;
		
		double  *position;
		
		DataFieldAccessPoint(PField_std,p,(void**)&material_point);
		
		/* Access using the getter function provided for you (recommeneded for beginner user) */
		MPntStdGetField_global_coord(material_point,&position);
		// OR
		/* Access directly into material point struct (not recommeneded for beginner user) */
		/* Direct access only recommended for efficiency */
		/* position = material_point->coor; */
		
		MPntStdGetField_phase_index(material_point,&phase_init);		
		PhaseMapGetPhaseIndex(phasemap,position,&phase_index);
		PhaseMapCheckValidity(phasemap,phase_index,&is_valid);
		
		//PetscPrintf(PETSC_COMM_WORLD,"Phase index : %d  is_valid %d \n", phase_index,is_valid);
		
		if (is_valid==1) { /* point located in the phase map */
			phase = phase_index;
		} else {
			phase = 4;
		}	
		/* user the setters provided for you */
		MPntStdSetField_phase_index(material_point,phase);
		
	}
	
	DataFieldRestoreAccess(PField_std);
	
	
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetMarkerIndexFricLayer_G2008"
PetscErrorCode pTatin2d_ModelSetMarkerIndexFricLayer_G2008(pTatinCtx ctx)
{
	
	PetscInt               p,n_mp_points;
	DataBucket             db;
	DataField              PField_std;
	int                    phase_init, phase, phase_index, is_valid;
	PetscReal jmin[NSD],jmax[NSD],thicklayer;
	pTatinUnits *units;	
	

	PetscErrorCode ierr;
	
	PetscFunctionBegin;

	DMDAGetBoundingBox(ctx->dav,jmin,jmax);
	units   = &ctx->units;  
	UnitsApplyInverseScaling(units->si_length,2.e-3,&thicklayer);
	
	
	/* define properties on material points */
	db = ctx->db;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	for (p=0; p<n_mp_points; p++) {
		MPntStd     *material_point;
		
		double  *pos;
		
		DataFieldAccessPoint(PField_std,p,(void**)&material_point);
		
		/* Access using the getter function provided for you (recommeneded for beginner user) */
		MPntStdGetField_global_coord(material_point,&pos);
		// OR
		/* Access directly into material point struct (not recommeneded for beginner user) */
		/* Direct access only recommended for efficiency */
		/* position = material_point->coor; */
		
		MPntStdGetField_phase_index(material_point,&phase_init);		
	    
	    if (pos[0]-jmin[0] < thicklayer | pos[1]-jmin[1] < thicklayer | jmax[0]-pos[0]< thicklayer){
	    // inside the fric layer 
	    MPntStdSetField_phase_index(material_point,4);
	    }else{
	         if (phase_init == 4) {
	             MPntStdSetField_phase_index(material_point,0);
	         }
 	    }
		
		//PetscPrintf(PETSC_COMM_WORLD,"Phase index : %d  is_valid %d \n", phase_index,is_valid);
		
	}
	
	DataFieldRestoreAccess(PField_std);
	
	
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_G2008"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_G2008(pTatinCtx ctx)
{
	PetscErrorCode         ierr;
	PetscBool             friction=PETSC_FALSE;
	pTatinUnits *units;	
	
	PetscFunctionBegin;
	PetscOptionsGetBool("G2008_","-add_fric_layer",&friction,0);
	units   = &ctx->units;  
	UnitsApplyInverseScaling(units->si_time,ctx->dt_min,&ctx->dt_min);
	UnitsApplyInverseScaling(units->si_time,ctx->dt,&ctx->dt);
	UnitsApplyInverseScaling(units->si_time,ctx->dt_adv,&ctx->dt_adv);
	UnitsApplyInverseScaling(units->si_time,ctx->dt_max,&ctx->dt_max);
	UnitsApplyInverseScaling(units->si_time,ctx->time_max,&ctx->time_max);
	UnitsApplyInverseScaling(units->si_length,ctx->max_disp_y,&ctx->max_disp_y);
	
	
	ierr = pTatin2d_ModelSetMaterialProperty_G2008(ctx); CHKERRQ(ierr);
	ierr = pTatin2d_ModelSetMarkerIndexFromMap_G2008(ctx); CHKERRQ(ierr);
	ierr = pTatin2d_ModelSetInitialStokesVariableOnMarker_GENE(ctx); CHKERRQ(ierr);
   // ierr = pTatin2d_ModelSetInitialPlsVariableOnMarker_GENE(ctx); CHKERRQ(ierr);
	if (friction ==PETSC_FALSE){
	ierr = ModelSetFrictionalBoundaryType_G2008(ctx);CHKERRQ(ierr);
	}
	PetscFunctionReturn(0);
}

