

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"

#include "pTatin2d.h"
#include "swarm_fields.h"
#include "data_exchanger.h"
#include "MPntStd_def.h"
#include "MPntPChrono_def.h"
#include "material_point_chrono_utils.h"


#undef __FUNCT__
#define __FUNCT__ "SwarmUpdateProperties_MPntPChrono"
PetscErrorCode SwarmUpdateProperties_MPntPChrono(DataBucket db,pTatinCtx user,Vec X)
{
	BTruth found;
  DM                dau,dap;
  Vec               Uloc,Ploc;
	Vec               u,p;
  PetscScalar       *LA_Uloc,*LA_Ploc;
	PetscErrorCode    ierr;
	
	PetscFunctionBegin;
	
	
	DataBucketQueryDataFieldByName(db,MPntPChrono_classname,&found);
	if(found==BFALSE) {
		SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot find DataField with name %s \n", MPntPStokesMelt_classname );
	}
	
    ierr = DMCompositeGetEntries(user->pack,&dau,&dap);CHKERRQ(ierr);
    ierr = DMCompositeGetLocalVectors(user->pack,&Uloc,&Ploc);CHKERRQ(ierr);
	ierr = DMCompositeScatter(user->pack,X,Uloc,Ploc);CHKERRQ(ierr);
	ierr = VecGetArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
	ierr = VecGetArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
	
	ierr = pTatin_TkUpdateMarker_Chrono(user,dau,LA_Uloc,dap,LA_Ploc);CHKERRQ(ierr);
	
	ierr = VecRestoreArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
	ierr = VecRestoreArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
  ierr = DMCompositeRestoreLocalVectors(user->pack,&Uloc,&Ploc);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatin_TkUpdateMarker_Chrono"
PetscErrorCode pTatin_TkUpdateMarker_Chrono(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[])
{	
	PetscErrorCode ierr;
	int         pidx,n_mp_points;
	DataBucket  db;
	DataField   PField_std, PField_chrono;
	PetscScalar GNIu[2][Q2_NODES_PER_EL_2D],NIp[P_BASIS_FUNCTIONS];
	PetscScalar NIu[U_BASIS_FUNCTIONS];
	PetscScalar NIT[NODES_PER_EL_Q1_2D];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscScalar closure_temperature[]={120,350,800};
	PetscScalar age[3];
	PetscInt nel,nen_u,nen_p,nen_T,e,i,j,k;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	const PetscInt *elnidx_T;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	PetscScalar elT[NODES_PER_EL_Q1_2D];
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	
	PhysCompEnergyCtx phys_energy;
	Vec               T_vec,T_local_vec;
	DM                daT;
	PetscScalar       *LA_T;
	pTatinUnits *units;	
	
	PetscFunctionBegin;
	
	units   = &user->units;
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	/////////////////////* get physics */////////////////
	
	/* stokes */
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
	
	db = user->db;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	DataBucketGetDataFieldByName(db,MPntPChrono_classname,&PField_chrono);
	DataFieldGetAccess(PField_chrono);
	DataFieldVerifyAccess(PField_chrono,sizeof(MPntPChrono));
	
	/* energy */

		phys_energy = user->phys_energy;
		T_vec       = phys_energy->T;
		daT         = phys_energy->daT;
		
		ierr = DMGetLocalVector(daT,&T_local_vec);CHKERRQ(ierr);		
		ierr = DMGlobalToLocalBegin(daT,T_vec,INSERT_VALUES,T_local_vec);CHKERRQ(ierr);
		ierr = DMGlobalToLocalEnd(  daT,T_vec,INSERT_VALUES,T_local_vec);CHKERRQ(ierr);
		
		ierr = VecGetArray(T_local_vec,&LA_T);CHKERRQ(ierr);
		ierr = DMDAGetElementsQ1(daT,&nel,&nen_T,&elnidx_T);CHKERRQ(ierr);
		

	/* new piece of physics to be hook here following energy exemple*/
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	/* marker loop */
	
	for (pidx=0; pidx<n_mp_points; pidx++) {
		MPntStd       *material_point;
		MPntPChrono   *mpprop_chrono;
		double        *position, *xip;
		int           wil,phase_mp,itemp;
		PetscScalar J[2][2], iJ[2][2];
		PetscScalar J_p,ojp;
		PetscScalar  d1,exx_mp,eyy_mp,exy_mp,strainrateinv_mp;
		PetscScalar  pressure_mp;
		PetscScalar T_mp;	
		
		DataFieldAccessPoint(PField_std,    pidx,(void**)&material_point);	
		MPntStdGetField_local_element_index(material_point,&e);
		MPntStdGetField_global_coord(material_point,&position);
		MPntStdGetField_local_coord(material_point,&xip);
		MPntStdGetField_phase_index(material_point,&phase_mp);
		DataFieldAccessPoint(PField_chrono,    pidx,(void**)&mpprop_chrono);
		/* get nodal properties for element e */
		/* coordinates*/
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		
		
		/* temperature */
		T_mp = 0; 

			ierr = DMDAGetScalarElementFieldQ1_2D(elT,(PetscInt*)&elnidx_T[nen_T*e],LA_T);CHKERRQ(ierr);
			ConstructNi_Q1_2D(xip,NIT);
			for (k=0; k<NODES_PER_EL_Q1_2D; k++) {
				T_mp = T_mp + NIT[k] * elT[k];
			}
					
				
			if (T_mp >=120.0) MPntPChronoSetField_age120(mpprop_chrono,user->time);
		
			if (T_mp >=350.0) MPntPChronoSetField_age350(mpprop_chrono,user->time);
		
			if (T_mp >=800.0) MPntPChronoSetField_age800(mpprop_chrono,user->time);
		
			
			
	
		
		
	}
	
	
 	DataFieldRestoreAccess(PField_std);
  	DataFieldRestoreAccess(PField_chrono);
		
	ierr = VecRestoreArray(T_local_vec,&LA_T);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(daT,&T_local_vec);CHKERRQ(ierr);		

    ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

