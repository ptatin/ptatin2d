
//  
//
//  Created by laetitia le pourhiet on 25/08/17.
//
//


#ifndef __MATERIAL_POINT_PSTOKES_ELAS_UTILS_H__
#define __MATERIAL_POINT_PSTOKES_ELAS_UTILS_H__

#include "swarm_fields.h"
#include "data_exchanger.h"
#include "MPntStd_def.h"
#include "MPntPStokes_def.h"
#include "MPntPStokesElas_def.h"

PetscErrorCode SwarmOutputParaView_MPntPStokes_el(DataBucket db,const char path[],const char prefix[]);

PetscErrorCode SwarmUpdateGaussPropertiesLocalL2Projection_Q1_MPntPStokes_elas(const int npoints,MPntStd mp_std[],MPntPStokes mp_stokes[],MPntPStokesElas mp_stokes_el[],MaterialCoeffAvergeType average_type,DM da,QuadratureStokes Q);

PetscErrorCode SwarmUpdateProperties_MPntPStokes_el(DataBucket db,pTatinCtx ctx,Vec X);


#endif
