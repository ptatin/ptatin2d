





#ifndef __MPntPDarcy_DEF_H__
#define __MPntPDarcy_DEF_H__

typedef struct {
  double diff ;
  double pore ;
  double Xrock ;
  double source ;
} MPntPDarcy ;


typedef enum {
  MPPDarcy_coefficient_diffusion = 0,
  MPPDarcy_porosity,
  MPPDarcy_rock_fluid,
  MPPDarcy_source
} MPntPDarcyTypeName ;


extern const char MPntPDarcy_classname[];

extern const int MPntPDarcy_nmembers;

extern const size_t MPntPDarcy_member_sizes[];

extern const char *MPntPDarcy_member_names[];

/* prototypes */
void MPntPDarcyGetField_coefficient_diffusion(MPntPDarcy *point,double *data);
void MPntPDarcyGetField_porosity(MPntPDarcy *point,double *data);
void MPntPDarcyGetField_rock_fluid(MPntPDarcy *point,double *data);
void MPntPDarcyGetField_source(MPntPDarcy *point,double *data);
void MPntPDarcySetField_coefficient_diffusion(MPntPDarcy *point,double data);
void MPntPDarcySetField_porosity(MPntPDarcy *point,double data);
void MPntPDarcySetField_rock_fluid(MPntPDarcy *point,double data);
void MPntPDarcySetField_source(MPntPDarcy *point,double data);
void MPntPDarcyView(MPntPDarcy *point);
void MPntPDarcyVTKWriteAsciiAllFields(FILE *vtk_fp,const int N,const MPntPDarcy points[]);
void MPntPDarcyPVTUWriteAllPPointDataFields(FILE *vtk_fp);
void MPntPDarcyVTKWriteBinaryAppendedHeaderAllFields(FILE *vtk_fp,int *offset,const int N,const MPntPDarcy points[]);
void MPntPDarcyVTKWriteBinaryAppendedDataAllFields(FILE *vtk_fp,const int N,const MPntPDarcy points[]);

#endif
