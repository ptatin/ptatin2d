#
# Update script for svn+ssh://dmay@musashi.ethz.ch/var/svn/davemay/Codes/tatin/src_pmark2dv0.0
# Tested for revision: 5322
#
# Usage:
#   Copy this folder into the src directory
#   sh update_ptatin_petsc3p2_to3p3.sh
#

echo '!________________________________________________________________________'
echo '!  Applying changes to merge pTatin2d from PETSc 3.2 -> 3.3'
echo '!  Notes:'
echo '!  - valid for revision 5322'
echo '!  - removed usage of function: SNESLineSearchRelaxed()'
echo '!  - removed usage of function: SNESLineSearchAdaptiveRelaxed()'


patch < tools/pTatin2d.diff
patch < tools/pTatin2d_two_stage.diff

perl -p -i -e 's/private\/daimpl.h/petsc\-private\/daimpl.h/g' *.c
perl -p -i -e 's/private\/daimpl.h/petsc\-private\/daimpl.h/g' *.h

perl -p -i -e 's/private\/matimpl.h/petsc\-private\/matimpl.h/g' *.c
perl -p -i -e 's/private\/matimpl.h/petsc\-private\/matimpl.h/g' *.h


perl -p -i -e 's/DMGetInterpolation/DMCreateInterpolation/g' *.c
perl -p -i -e 's/DMGetMatrix/DMCreateMatrix/g' *.c
perl -p -i -e 's/DMGetInjection/DMCreateInjection/g' *.c
perl -p -i -e 's/DMGetInterpolationScale/DMCreateInterpolationScale/g' *.c
perl -p -i -e 's/PetscTypeCompare/PetscObjectTypeCompare/g' *.c

perl -p -i -e 's/ierr\s\=\sPCMGSetGalerkin\(pc\_i,PETSC\_FALSE\);CHKERRQ\(ierr\)\;/ierr = PCMGSetGalerkin(pc\_i,PETSC\_FALSE);CHKERRQ(ierr);\n\t\tierr = PCSetDM(pc_i,PETSC\_NULL);CHKERRQ(ierr);/g' *.c

perl -p -i -e 's/ierr\s=\sMatSetType\(\sA,\sMATAIJ\s\)\;CHKERRQ\(ierr\)\;/ierr = MatSetType( A, MATAIJ );CHKERRQ(ierr);\n\tierr = MatSetUp(A);CHKERRQ(ierr);/g' data_exchanger.c

perl -p -i -e 's/SNES_CONVERGED_PNORM_RELATIVE/SNES_CONVERGED_SNORM_RELATIVE/g' *.c


cd test_option_files
perl -p -i -e 's/da\_mat\_type/dm\_mat\_type/g' *.opts 
perl -p -i -e 's/schur\_factorization\_type/schur\_fact\_type/g' *.opts 
perl -p -i -e 's/chebychev/chebyshev/g' *.opts

cd ../JCP_exemple/CRUSTAL_SHORTENING
perl -p -i -e 's/da\_mat\_type/dm\_mat\_type/g' *.opts 
perl -p -i -e 's/schur\_factorization\_type/schur\_fact\_type/g' *.opts 
perl -p -i -e 's/chebychev/chebyshev/g' *.opts

cd ../SUBDUCTION/surfaceplots
perl -p -i -e 's/da\_mat\_type/dm\_mat\_type/g' *.opts 
perl -p -i -e 's/schur\_factorization\_type/schur\_fact\_type/g' *.opts 
perl -p -i -e 's/chebychev/chebyshev/g' *.opts

cd ../residualsplots
perl -p -i -e 's/da\_mat\_type/dm\_mat\_type/g' *.opts 
perl -p -i -e 's/schur\_factorization\_type/schur\_fact\_type/g' *.opts 
perl -p -i -e 's/chebychev/chebyshev/g' *.opts
