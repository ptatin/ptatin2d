
echo '!________________________________________________________________________'
echo '!  Applying changes to merge pTatin2d from PETSc 3.3 -> 3.4'
echo '!  Notes:'
echo '!  - valid for revision 5322'


#perl -p -i -e 's/DMDA_BOUNDARY_/DM_BOUNDARY_/g' *.c
perl -p -i -e 's/DMDAGetGhostedCoordinates/DMGetCoordinatesLocal/g' *.c
perl -p -i -e 's/(\(\(PetscObject\)[^)]+\))->comm/PetscObjectComm((PetscObject)$1)/g' *.c
perl -p -i -e 's/DMDAGetCoordinates/DMGetCoordinates/g' *.c
perl -p -i -e 's/DMDAGetCoordinateDA/DMGetCoordinateDM/g' *.c

perl -p -i -e 's/(DMDASetUniformCoordinates.*)PETSC_NULL,PETSC_NULL,\sPETSC_NULL,PETSC_NULL/$1 0.0,0.0,0.0,0.0/g' *.c
perl -p -i -e 's/(DMDASetUniformCoordinates.*)PETSC_NULL,PETSC_NULL/$1 0.0,0.0/g' *.c
perl -p -i -e 's/DMDASetUniformCoordinates\(dmsq2,gmin\[0\],gmax\[0\],PETSC_NULL,PETSC_NULL, 0.0,0.0\)/DMDASetUniformCoordinates(dmsq2,gmin[0],gmax[0],0.0,0.0,0.0,0.0)/g' physcomp_SPM.c

perl -p -i -e 's/DMDAGetCoordinateDA/DMGetCoordinateDM/g' *.c

perl -p -i -e 's/petsc\-private\/daimpl.h/petsc\-private\/dmdaimpl.h/g' *.c
perl -p -i -e 's/dd->da_coordinates = dd_da_coordinates/_sda->coordinateDM = dd_da_coordinates/g' dmda_redundant.c

perl -p -i -e 's/count\s=\sPetscMPIIntCast\(dim\)/PetscMPIIntCast(dim,&count)/g' mesh_update.c

perl -p -i -e 's/PetscGetTime/PetscTime/g' *.c
perl -p -i -e 's/\(\(PetscObject\)\(da\)\)->comm/PetscObjectComm((PetscObject)da)/g' phys_energy_equation.c

perl -p -i -e 's/DMView_DA_Private\(\*da\)/DMViewFromOptions(*da,NULL,"-dm_view")/g' dmda_redundant.c

perl -p -i -e 's/SNESLS/SNESNEWTONLS/g' *.c