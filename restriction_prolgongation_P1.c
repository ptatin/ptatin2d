
#include "petsc.h"
#include "petscmat.h"
#include "petscvec.h"
#include "petscdm.h"
#include "petscdmda.h"


typedef struct _p_RPMatShell *RPShell;
struct _p_RPMatShell {
	DM dac,daf;
};

#undef __FUNCT__
#define __FUNCT__ "MatMultAdd_basic"
PetscErrorCode MatMultAdd_basic(Mat mat,Vec v1,Vec v2,Vec v3)
{
	Vec            t1;
	PetscErrorCode ierr;
	PetscFunctionBegin;

	ierr = VecDuplicate(v2,&t1);CHKERRQ(ierr);
	ierr = VecCopy(v2,t1);CHKERRQ(ierr);

	ierr = MatMult(mat,v1,v3);CHKERRQ(ierr);
	ierr = VecAXPY(v3,1.0,t1);CHKERRQ(ierr);
	ierr = VecDestroy(&t1);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

/* p_coarse = R p_fine */
#undef __FUNCT__
#define __FUNCT__ "MatMult_RestrictionP1_average"
PetscErrorCode MatMult_RestrictionP1_average(Mat A,Vec x,Vec y)
{
	RPShell data;
	PetscInt Mc,Nc,Mf,Nf,sic,sjc,mc,nc,eic,ejc,eif,ejf,i,j;
	PetscScalar ***LA_x,***LA_y;
	PetscInt ratio_x,ratio_y;
	PetscErrorCode ierr;
	PetscFunctionBegin;

	ierr = MatShellGetContext(A,(void**)&data);CHKERRQ(ierr);
	ierr = VecZeroEntries(y);CHKERRQ(ierr);
	
	ierr = DMDAGetInfo(data->dac,0, &Mc,&Nc,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
	ierr = DMDAGetInfo(data->daf,0, &Mf,&Nf,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);

	ratio_x = Mf/Mc;
	ratio_y = Nf/Nc;
	
	ierr = DMDAGetCorners(data->dac,&sic,&sjc,0,&mc,&nc,0);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(data->daf,x,&LA_x);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(data->dac,y,&LA_y);CHKERRQ(ierr);
	
	for (ejc=sjc; ejc<sjc+nc; ejc++) {
		for (eic=sic; eic<sic+mc; eic++) {
			PetscScalar p0,p1,p2;
			PetscInt nd;
			
			p0 = p1 = p2 = 0.0;
			nd = 0;
			for (j=0; j<ratio_y; j++) {
				for (i=0; i<ratio_x; i++) {
					eif = ratio_x * eic + i;
					ejf = ratio_y * ejc + j;
					
					/* get fine cells */
					/* get entries */
					p0 += LA_x[ejf][eif][0];
					p1 += LA_x[ejf][eif][1];
					p2 += LA_x[ejf][eif][2];
					/* do linear fit of data */
					/* insert result */
					nd++;
				}
			}			
			p0 = p0 / (PetscScalar)nd;
			p1 = p1 / (PetscScalar)nd;
			p2 = p2 / (PetscScalar)nd;
			
			/* p(x,y) = P0 + P1.x + P2.y */
			LA_y[ejc][eic][0] = p0;
			LA_y[ejc][eic][1] = p1*0.0;
			LA_y[ejc][eic][2] = p2*0.0;
			
		}
	}
	
	ierr = DMDAVecRestoreArrayDOF(data->dac,y,&LA_y);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArrayDOF(data->daf,x,&LA_x);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MatMult_RestrictionP1_averageL2"
PetscErrorCode MatMult_RestrictionP1_averageL2(Mat A,Vec x,Vec y)
{
	RPShell data;
	PetscInt Mc,Nc,Mf,Nf,sic,sjc,mc,nc,eic,ejc,eif,ejf,i,j;
	PetscScalar ***LA_x,***LA_y;
	PetscInt ratio_x,ratio_y;
	double AA[3][3],RHS[3],B[3][3],COEFF[3];
	PetscErrorCode ierr;
	PetscFunctionBegin;
	
	ierr = MatShellGetContext(A,(void**)&data);CHKERRQ(ierr);
	ierr = VecZeroEntries(y);CHKERRQ(ierr);
	
	ierr = DMDAGetInfo(data->dac,0, &Mc,&Nc,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
	ierr = DMDAGetInfo(data->daf,0, &Mf,&Nf,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
	
	ratio_x = Mf/Mc;
	ratio_y = Nf/Nc;
	
	ierr = DMDAGetCorners(data->dac,&sic,&sjc,0,&mc,&nc,0);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(data->daf,x,&LA_x);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(data->dac,y,&LA_y);CHKERRQ(ierr);
	
	for (ejc=sjc; ejc<sjc+nc; ejc++) {
		for (eic=sic; eic<sic+mc; eic++) {
			PetscScalar p0,p1,p2;
			double coor_normalized[2];

			/* init */
			for (i=0; i<3; i++) {
				for (j=0; j<3; j++) {
					AA[i][j] = 0.0;
				}
				RHS[i] = 0.0;
			}

			
			for (j=0; j<ratio_y; j++) {
				for (i=0; i<ratio_x; i++) {
					
					eif = ratio_x * eic + i;
					ejf = ratio_y * ejc + j;

					
					coor_normalized[0] = -1.0 + 0.5*i;
					coor_normalized[1] = -1.0 + 0.5*j;
					
					AA[0][0] += (coor_normalized[0]) * (coor_normalized[0]);
					AA[0][1] += (coor_normalized[0]) * (coor_normalized[1]);
					AA[0][2] += coor_normalized[0];
					
					AA[1][1] += (coor_normalized[1]) * (coor_normalized[1]);
					AA[1][2] += (coor_normalized[1]);
					
					AA[2][2] += 1.0;

					
					p0 = LA_x[ejf][eif][0]; // drop x,y terms in local space

					
					RHS[0] += (coor_normalized[0]) * p0;
					RHS[1] += (coor_normalized[1]) * p0;
					RHS[2] +=                        p0;
					
				}
			}			

			/* solve */
			ElementHelper_matrix_inverse_3x3(AA,B);
			
			for (i=0; i<3; i++) {
				double sum_ij	= 0.0;
				for (j=0; j<3; j++) {
					sum_ij += B[i][j] * RHS[j];
				}
				COEFF[i] = sum_ij;
			}

			p0 = COEFF[0];
			p1 = COEFF[1];
			p2 = COEFF[2];
			
			/* p(x,y) = P0 + P1.x + P2.y */
			LA_y[ejc][eic][0] = p0*1.0;
			LA_y[ejc][eic][1] = p1*1.0;
			LA_y[ejc][eic][2] = p2*1.0;
			
		}
	}
	
	ierr = DMDAVecRestoreArrayDOF(data->dac,y,&LA_y);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArrayDOF(data->daf,x,&LA_x);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

/* p_fine = P p_coarse */
#undef __FUNCT__
#define __FUNCT__ "MatMult_ProlongateP1_injection"
PetscErrorCode MatMult_ProlongateP1_injection(Mat A,Vec x,Vec y)
{
	RPShell data;
	PetscInt Mc,Nc,Mf,Nf,sic,sjc,mc,nc,eic,ejc,eif,ejf,i,j;
	PetscScalar ***LA_x,***LA_y;
	PetscInt ratio_x,ratio_y;
	PetscErrorCode ierr;
	PetscFunctionBegin;
	
	ierr = MatShellGetContext(A,(void**)&data);CHKERRQ(ierr);
	ierr = VecZeroEntries(y);CHKERRQ(ierr);
	
	ierr = DMDAGetInfo(data->dac,0, &Mc,&Nc,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
	ierr = DMDAGetInfo(data->daf,0, &Mf,&Nf,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
	
	ratio_x = Mf/Mc;
	ratio_y = Nf/Nc;
	
	ierr = DMDAGetCorners(data->dac,&sic,&sjc,0,&mc,&nc,0);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(data->dac,x,&LA_x);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(data->daf,y,&LA_y);CHKERRQ(ierr);
	
	for (ejc=sjc; ejc<sjc+nc; ejc++) {
		for (eic=sic; eic<sic+mc; eic++) {
			PetscScalar p0,p1,p2;
			PetscInt nd;
			
			/* get coarse cell entries */
			p0 = LA_x[ejc][eic][0];
			p1 = LA_x[ejc][eic][1];
			p2 = LA_x[ejc][eic][2];
			
			for (j=0; j<ratio_y; j++) {
				for (i=0; i<ratio_x; i++) {
					eif = ratio_x * eic + i;
					ejf = ratio_y * ejc + j;
					
					/* insert result */
					LA_y[ejf][eif][0] = p0/4.0;
					LA_y[ejf][eif][1] = p1*0.0;
					LA_y[ejf][eif][2] = p2*0.0;
				}
			}			
			
		}
	}
	
	ierr = DMDAVecRestoreArrayDOF(data->daf,y,&LA_y);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArrayDOF(data->dac,x,&LA_x);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MatCreateRestrictionP1"
PetscErrorCode MatCreateRestrictionP1(DM dac,DM daf,Mat *A)
{
	RPShell data;
	Mat B;
	PetscInt Mc,Mf,mc,mf;
	Vec Xc,Xf;
	PetscErrorCode ierr;
	PetscFunctionBegin;

	ierr = PetscMalloc(sizeof(struct _p_RPMatShell),&data);CHKERRQ(ierr);
	data->dac = dac;
	data->daf = daf;
	
	ierr = MatCreate(PETSC_COMM_WORLD,&B);CHKERRQ(ierr);

	ierr = DMGetGlobalVector(dac,&Xc);CHKERRQ(ierr);
	ierr = DMGetGlobalVector(daf,&Xf);CHKERRQ(ierr);
	ierr = VecGetSize(Xc,&Mc);CHKERRQ(ierr);
	ierr = VecGetLocalSize(Xc,&mc);CHKERRQ(ierr);
	ierr = VecGetSize(Xf,&Mf);CHKERRQ(ierr);
	ierr = VecGetLocalSize(Xf,&mf);CHKERRQ(ierr);
	ierr = DMRestoreGlobalVector(dac,&Xc);CHKERRQ(ierr);
	ierr = DMRestoreGlobalVector(daf,&Xf);CHKERRQ(ierr);
	
	ierr = MatSetSizes(B,mc,mf,Mc,Mf);CHKERRQ(ierr);
	ierr = MatSetType(B,MATSHELL);CHKERRQ(ierr);
	ierr = MatShellSetContext(B,(void*)data);CHKERRQ(ierr);

	//ierr = MatShellSetOperation(B,MATOP_MULT,(void(*)(void))MatMult_RestrictionP1_average);CHKERRQ(ierr);
	ierr = MatShellSetOperation(B,MATOP_MULT,(void(*)(void))MatMult_RestrictionP1_averageL2);CHKERRQ(ierr);

	
	ierr = MatShellSetOperation(B,MATOP_MULT_ADD,(void(*)(void))MatMultAdd_basic);CHKERRQ(ierr);
	
	*A = B;
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MatCreateProlongationP1"
PetscErrorCode MatCreateProlongationP1(DM dac,DM daf,Mat *A)
{
	RPShell data;
	Mat B;
	PetscInt Mc,Mf,mc,mf;
	Vec Xc,Xf;
	PetscErrorCode ierr;
	PetscFunctionBegin;
	
	ierr = PetscMalloc(sizeof(struct _p_RPMatShell),&data);CHKERRQ(ierr);
	data->dac = dac;
	data->daf = daf;
	
	ierr = MatCreate(PETSC_COMM_WORLD,&B);CHKERRQ(ierr);
	
	ierr = DMGetGlobalVector(dac,&Xc);CHKERRQ(ierr);
	ierr = DMGetGlobalVector(daf,&Xf);CHKERRQ(ierr);
	ierr = VecGetSize(Xc,&Mc);CHKERRQ(ierr);
	ierr = VecGetLocalSize(Xc,&mc);CHKERRQ(ierr);
	ierr = VecGetSize(Xf,&Mf);CHKERRQ(ierr);
	ierr = VecGetLocalSize(Xf,&mf);CHKERRQ(ierr);
	ierr = DMRestoreGlobalVector(dac,&Xc);CHKERRQ(ierr);
	ierr = DMRestoreGlobalVector(daf,&Xf);CHKERRQ(ierr);
	
	ierr = MatSetSizes(B,mf,mc,Mf,Mc);CHKERRQ(ierr);
	ierr = MatSetType(B,MATSHELL);CHKERRQ(ierr);
	ierr = MatShellSetContext(B,(void*)data);CHKERRQ(ierr);
	
	ierr = MatShellSetOperation(B,MATOP_MULT,(void(*)(void))MatMult_ProlongateP1_injection);CHKERRQ(ierr);
	ierr = MatShellSetOperation(B,MATOP_MULT_ADD,(void(*)(void))MatMultAdd_basic);CHKERRQ(ierr);
	
	*A = B;
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMGetInterpolation_P1"
PetscErrorCode DMGetInterpolation_P1(DM dac,DM daf,Mat *A,Vec *X)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
	
	ierr = MatCreateProlongationP1(dac,daf,A);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}
