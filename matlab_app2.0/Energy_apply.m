function err = Energy_apply(filename)
    s = {'\fontname{times}\fontsize{16}\it{This part helps you define composite geotherms}',...
        '\fontsize{12}\rm{* you first need to define temperature field using provided analytical solution}',...
        '\rm{* then you define masks which define the regions of the temperature field you wish to keep}',...
        '\rm{* finally the program interpolates between the masks}',...
        'and lets you smooth your solution and export a temperature map which can be read by ptatin2d',...
        'if your analytical solution implies heat production, a map of heat production distribution is also generated', ...
        '\fontsize{24}READY?'
        };
    createmode.Interpreter='tex';
    createmode.Default='YES';
    h = questdlg(s,'Welcome to matlab_app2.0 geotherm design toolbox','YES','NO',createmode);
    if strcmp(h,'NO')
        error('You are not yet ready to create your geotherms... halting');
    end
    
load([filename,'_phase.mat']);
load([filename,'_basic.mat']);

fid = fopen([filename,'_temperature.mat']);
interpolat_geotherm = 'Yes';
if fid ~= -1
    fclose(fid);
    load([filename,'_temperature.mat'],'Anew','Prodnew');
    figure(500); pcolor(X,Y,Anew); shading interp; colorbar; title('This is your current initial geotherm')
    interpolat_geotherm = questdlg('Do you want to change your initial geotherm?','ptatin2dMatlabApp','Yes','No','No');
end

if strcmp(interpolat_geotherm,'Yes');
    fid = fopen([filename,'_therm_ana.mat'],'r');
    if fid > 2
        fclose(fid);
        load ([filename,'_therm_ana'],'name')
        s=sprintf('The following temperature field are already defined for this model:\n');
        l = length(name);
        for i = 1:l
            s=[s,sprintf(' %d %s \n', i, name{i})];
        end
        create_geotherm = questdlg([s,'Do you want to prepare additional temperature field from analytical solutions?'],'ptatin2dMatlabApp','Yes','No','No');
    else
        create_geotherm = 'Yes';
        l = 0;
    end
    if strcmp(create_geotherm,'Yes')
        makegeotherm(filename,l);
    end
    
    [Anew,Prodnew] = geothermscat(filename);
    save([filename,'_temperature'],'Anew','Prodnew');
end

if exist('Anew')
    figure(500); clf; pcolor(X,Y,Anew); shading interp; colorbar; title('This is your initial geotherm')
    fname = [filename2,'.tempmap'];
    tempmap_write(Anew,fname,Origine,Length);
    close(500);
    Production=0;
    if sum(Prodnew(:))>0
        fname = [filename2,'.prodmap'];
        tempmap_write(Prodnew,fname,Origine,Length);
        Production = 1;
    end
    save([filename,'_basic'],'Production','-append');
    err = 0
else
    err = 1
end
