function [Anew,Prodnew] = geothermscat(filename)
% load data
load([filename,'_phase.mat']);
load([filename,'_therm_ana.mat']);
load([filename,'_basic.mat']);
sh     = 4; % number of pixels in the border of the domain

phase2D = double(phase2D);

% create the T,X,Y,name variable
BORD=zeros(size(X));
DOMAIN=BORD;

i=0;

composite = questdlg(['Do you wish to create a geotherm from composite temperature field (Composite)',...
                        'or from a single temperature field already defined (Single)?'],'ptatin2dMatlabApp','Composite','Single','Single');

if strcmp(composite,'Single')
    type   = menu('Which temperature field do you want to use as initial geotherm?',name);
    Anew = Tana(:,:,type);
    Prodnew = Prod(:,:,type);
else
    interpolate = 'Continue adding mask';
    while strcmp(interpolate,'Continue adding mask')
        i=i+1;
        [M,type] = create_mask(Tana,X,Y,phase2D,name);
        [BORD(:,:,i), DOMAIN(:,:,i)]=contour_mask(X,Y,M,sh);
        A(:,:,i) = Tana(:,:,type);
        H(:,:,i) = Prod(:,:,type);
        figure(100); clf; set(100,'Position',[0 350 600 300]);
         pcolor(X,Y,sum(A.*DOMAIN,3));title('Your present temperature field patches');axis tight equal;shading flat; colorbar horiz;
        interpolate = questdlg('What do you wanna do?','ptatin2dMatlabApp','Continue adding mask','Undo last mask','Finalise interpolation','Finalise');
        if strcmp(interpolate,'Undo last mask')
            i=i-1;
            interpolate = 'Continue adding mask';
        else
        %assign -1 to the part of the phase array which have been assigned.    
        phase2D = ((phase2D+1).*~DOMAIN(:,:,i))-1;
        end
    end
    
    Anew = sum(A.*DOMAIN,3);
    Prodnew = sum(H.*DOMAIN,3);
    BORDnew = sum(BORD,3);
    DOMAINnew  = sum(DOMAIN,3);
    if ~isempty(Tbot)
    Anew(1,:)=Tbot;
    else
        if any(Anew(1,:))== 0
           for i = 1:size(Anew,2)
             Anew(1,i)= Anew(find(DOMAINnew(:,i),1,'first'),i);
           end
        end
    end
    BORDnew(1,:)= 1;
    DOMAINnew(1,:)= 1;
    
    if ~isempty(Ttop)
    Anew(end,:)=Ttop;
    else
        if any(Anew(end,:)== 0)
           for i = 1:size(Anew,2)
             Anew(end,i)= Anew(find(DOMAINnew(:,i),1,'last'),i);
           end
        end
    end
    BORDnew(end,:)= 1;
    DOMAINnew(end,:)= 1;
    
    if ~isempty(Tleft)
    Anew(:,1)=Tleft;
    else
        if any(Anew(:,1)==0)
           for i = 1:size(Anew,1)
               n =  Anew(i,find(DOMAINnew(i,:),1,'first'));
               if ~isempty(n)
                 Anew(i,1)= n;
                 BORDnew(i,1)= 1;
                 DOMAINnew(i,1)= 1;
               end
           end
        end
    end
    
    if ~isempty(Tright)
    Anew(:,end)=Tright;
    else
        if any(Anew(:,end)==0)
           for i = 1:size(Anew,1)
               n =  Anew(i,find(DOMAINnew(i,:),1,'last'));
               if ~isempty(n)
                 Anew(i,end)= n;
                 BORDnew(i,end)= 1;
                 DOMAINnew(i,end)= 1;
               end
           end
        end
    end
    
    
    indice   = find(BORDnew==1);
    xp       = X(indice);
    yp       = Y(indice);
    ap       = Anew(indice);
    
    Ainterp  = griddata(xp,yp,ap,X,Y);
    Anew     = Anew+~DOMAINnew.*Ainterp;
    
    figure(100);
    set(100,'Position',[0 350 600 300]);
    pcolor(X,Y,Anew);title('Initial geotherm');axis tight equal;shading flat; colorbar horiz;
    figure(101);
    set(101,'Position',[0 0 600 300]);
    pcolor(X,Y,Prodnew);title('initial heat production'); axis tight equal;shading flat; colorbar horiz;
    
    smooth = questdlg('Do you wish to apply more smoothing?','ptatin2dMatlabApp','Yes','No','No');
    
    if strcmp(smooth,'Yes')
        dummy = Anew;
        happy ='No'
        while strcmp(happy,'No')
            n = str2num(char(inputdlg({'smoothing factor (must be odd number)'},'pTatin2dMatlabApp',1,{'5'})));
%             h = ones(n*2);
%             Z = filter2(h,Anew,'valid');
%             Z = filter2(h,Anew,'valid');
%             Z = Z/max(Z(:))*max(Anew(:));
%             dummy(n:end-n,n:end-n)=Z;
            dummy = smooth2(Anew,n);
            figure(100);clf;pcolor(X,Y,dummy); shading flat; axis tight equal; colorbar;title('smoothed geotherm');
            happy = questdlg('Keep this result?','ptatin2dMatlabApp','Yes','No','Stop','No');
        end
        
        if strcmp(happy,'Yes')
            Anew=dummy;
        end
    end
    happy = questdlg('Do you want to store the geotherm with your analytical solution?','ptatin2dMatlabApp','Yes','No','Yes');
        if strcmp(happy,'Yes')
            index = size(Tana,3)+1;
            Tana(:,:,index) = Anew;
            Prod(:,:,index) = Prodnew;
            name{index} = char(inputdlg({'Name of the geotherm'},'pTatin2dmatlabApp ',1,{'mygeotherm_name'}));
            save([filename,'_therm_ana'],'Tana','Prod','name','X','Y');
        end
end
figure(100);clf;pcolor(X,Y,Anew);title('geotherm'); shading flat;colorbar
figure(101);clf;pcolor(X,Y,Prodnew); title('heat production');shading flat;colorbar
end

function [B,M]=contour_mask(X,Y,M,sh)
%X,Y,A are 2D arrays of the same size
% A is the analytical solution at point X,Y
%B and M are also 2 arrays containing respectively mask for the contour and
%for the domain where A applies
[nl,nc]=size(M);
Zhb=zeros(sh,nc);
Zdg=zeros(nl,sh);
Nb = [Zhb;M(1:end-sh,:)];
Nh = [M(sh+1:end,:);Zhb];
Nd = [Zdg M(:,1:end-sh)];
Ng = [M(:,sh+1:end) Zdg];
B  =  M&~(Nb&Nh&Nd&Ng);
end

function [ret]=smooth2(data,sz)
sz = [sz sz];
sz = sz(:)';
padSize = (sz-1)/2;
if ~isequal(padSize, floor(padSize)) || any(padSize<0)
  error(id('InvalidSizeValues'),'All elements of SIZE must be odd integers greater than or equal to 1.');
end
smooth = ones(sz)/prod(sz);
ret=convn(padreplicate(data,padSize),smooth, 'valid');

end

function b=padreplicate(a, padSize)
%Pad an array by replicating values.
numDims = length(padSize);
idx = cell(numDims,1);
for k = 1:numDims
  M = size(a,k);
  onesVector = ones(1,padSize(k));
  idx{k} = [onesVector 1:M M*onesVector];
end
b = a(idx{:});
end
