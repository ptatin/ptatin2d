function solver(filename)

withic = 'No';
Dsnesuserval_ic  = {'3','1e-1','1e-3','basic'}
Dsnesuserval_presnes  = {'10','1e-2','1e-5','basic'}
Dsnesuserval_snes  = {'10','1e-2','1e-9','basic'}
Dkspuserval_presnes = {'1e-5','1e-10'}
Dkspuserval_snes = {'1e-5','1e-10'}
Dkspuserval_ic = {'1e-4','1e-9'}

Dmonitor_ic = [1 0 1 0];
Dmonitor_presnes = [1 0 1 0];
Dmonitor_snes = [1 0 1 0];
Dpc      = 2;
Dsnes    = 2;
DMstep   = 0;
DMglevel = 1;
DmaxSmoothIt= 8;
fid = fopen([filename,'solver.mat']);
if fid ~= -1
    fclose(fid);
    restart = questdlg('Do you want to use the data checkpointed in the solver.mat file?','ptatin2dMatlabApp','Yes','No','Yes');
    if strcmp(restart,'Yes')
        load([filename,'solver.mat']);
        if exist('snesuserval_ic')
        Dsnesuserval_ic  = snesuserval_ic;
        end
        if exist('snesuserval_presnes')
        Dsnesuserval_presnes  = snesuserval_presnes;
        end
        if exist('snesuserval_snes')
        Dsnesuserval_snes  = snesuserval_snes;
        end
        if exist('kspuserval_presnes')
        Dkspuserval_presnes = kspuserval_presnes;
        end
        if exist('kspuserval_snes')
        Dkspuserval_snes = kspuserval_snes;
        end
        if exist('kspuserval_ic')
        Dkspuserval_ic = kspuserval_ic;
        end
        if exist('pc')
        Dpc      = pc;
        end
        if exist('snes')
        Dsnes    = snes;
        end
        if exist('Mstep')
        DMstep   = Mstep;
        end
        if exist('Mglevel')
        DMglevel = Mglevel;
        end
        if exist('maxSmoothIt')
        DmaxSmoothIt = maxSmoothIt;
        end
        if exist('monitor_ic')
            Dmonitor_ic = monitor_ic;
        end
        
        if exist('monitor_presnes')
            Dmonitor_presnes = monitor_presnes;
        end
        
        if exist('monitor_snes')
            Dmonitor_snes = monitor_snes;
        end
        
    end
end
snes_type={};
snesuserval_ic  = {};
snesuserval_presnes  = {};
snesuserval_snes  = {};
kspuserval_presnes = {};
kspuserval_snes = {};
kspuserval_ic = {};
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
monitoring_strings={'ksp_converged_reason' 'ksp_monitor' 'snes_converged_reason' 'snes_monitor'};
snes_options = {'snes_max_it','snes_rtol','snes_atol','snes_ls'};
kspuser_options = {'ksp_rtol','ksp_atol'};
ksp_options = {'ksp_rtol','ksp_atol','ksp_type','fieldsplit_u_ksp_type','fieldsplit_u_ksp_rtol'};
ksp_values  = {Dkspuserval_snes{1:end},'fgmres','fgmres','1.0e-5'};

pc_options ={'pc_type','pc_fieldsplit_type','pc_fieldsplit_schur_factorization_type', 'fieldsplit_p_ksp_type','fieldsplit_p_pc_type'};
pc_values  ={'fieldsplit','schur','upper','preonly','jacobi'};
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pc_prefix     = 'fieldsplit_u';
pc_MG    = '_mg_levels';
pc_MG_options    ={'_pc_type','_ksp_type','_ksp_max_it'};
pc_coarse = '_mg_coarse';
pc_options_coarse={'_pc_type','_pc_factor_mat_solver_package','_ksp_type','_ksp_max_it'};
pc_reg ='';
pc_options_reg={'_pc_type','_pc_factor_mat_solver_package','_ksp_type','_ksp_max_it'};


[pc,snes,Mstep,Mglevel,maxSmoothIt] = solvergui(Dpc,Dsnes,DMstep,DMglevel,DmaxSmoothIt);

switch pc
    case 1
        pc_valuesreg    = {'bjacobi','cg','preonly','20000'};
        isparallel = 'yes';
        withmg = 'No';
        augmented_lagrangian = false;
    case 2
        pc_valuesreg    = {'lu','umfpack','preonly','20000'};
        isparallel = 'No';
        withmg = 'No';
        augmented_lagrangian = true;
    case 3
        pc_valuesreg    = {'lu','superlu_dist','preonly','20000'};
        isparallel = 'Yes';
        withmg = 'No';
        augmented_lagrangian = false;
    case 4
        withmg = 'Yes';
        pc_valuesMG     = {'bjacobi','cg',num2str(maxSmoothIt)};
        pc_valuescoarse = {'lu','umfpack','preonly','1'};
        isparallel = 'No';
        augmented_lagrangian = false;
    case 5
        isparallel = 'Yes';
        withmg = 'Yes';
        pc_valuesMG     = {'bjacobi','cg',num2str(maxSmoothIt)};
        pc_valuescoarse = {'lu','superlu_dist','preonly','1'};
        augmented_lagrangian = false;
end

if Mstep > 1
    withic = 'Yes';
    snes_type={'-ic_'};
    extra={['-continuation_M ',num2str(Mstep)]};
    [kspuserval_ic,snesuserval_ic,monitor_ic] = snesgui(Dkspuserval_ic,Dsnesuserval_ic,'continuation',Dmonitor_ic);
    ksp_values{1}   = {kspuserval_ic{1:end},'fgmres','fgmres','1.0e-5'};
    snes_values{1}  = snesuserval_ic;
    monitoring{length(snes_type)}   = {monitoring_strings{find(monitor_ic)}};
else
    extra={'-continuation_M 1' '-ic_snes_max_it 0'};
    
end

switch snes
    case 1
        withsnes = 'none';
        snes_type={snes_type,'-'};
        extra= {extra{1:end},'-skip_pre_snes','-snes_type ksponly','-dmcomposite_pc_upper'};
    case 2
        withsnes = 'picard';
        snes_type={snes_type{1:end},'-'};
        extra={extra{1:end},'-skip_pre_snes'};
        
        [kspuserval_snes,snesuserval_snes,monitor_snes] = snesgui(Dkspuserval_snes,Dsnesuserval_snes,'picard',Dmonitor_snes);
        ksp_values{length(snes_type)}   = {kspuserval_snes{1:end},'fgmres','fgmres','1.0e-5'};
        snes_values{length(snes_type)}  =  snesuserval_snes;
        monitoring{length(snes_type)}   =  {monitoring_strings{find(monitor_snes)}};
        
    case 3
        withsnes = 'Newton';
        snes_type = {snes_type{1:end},'-pre_','-'};
        extra     = {extra{1:end},'-use_mf_stokes','-snes_mf_operator','-dmcomposite_pc_full','-pc_fieldsplit_real_diagonal'};
        
        [kspuserval_presnes,snesuserval_presnes,monitor_presnes] = snesgui(Dkspuserval_presnes,Dsnesuserval_presnes,'pre newton picard',Dmonitor_presnes);
        ksp_values{length(snes_type)-1}   = {kspuserval_presnes{1:end},'fgmres','fgmres','1.0e-5'};
        snes_values{length(snes_type)-1}  =  snesuserval_presnes;
        monitoring{length(snes_type)-1}   =  {monitoring_strings{find(monitor_presnes)}};
        
        [kspuserval_snes,snesuserval_snes,monitor_snes] = snesgui(Dkspuserval_snes,Dsnesuserval_snes,'newton',Dmonitor_snes);
        ksp_values{length(snes_type)}   = {kspuserval_snes{1:end},'fgmres','fgmres','1.0e-5'};
        snes_values{length(snes_type)}  =  snesuserval_snes;
        monitoring{length(snes_type)}   =  {monitoring_strings{find(monitor_snes)}};
end
save([filename,'solver.mat'])



