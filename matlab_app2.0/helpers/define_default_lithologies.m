function define_default_lithologies(fh)
load('default_lithologies.mat');
load('options_definitions.mat');
i = n_litho+1;

string  = {viscous_string,plas_string,soft_string,energy_string,density_string};
prompt  = {'Viscous type','Plastic type','Softening type','Energy type','Density type'};
typedef = [1,1,1,1,1];

opts = {viscous_opts,plastic_opts,softening_opts,energy_opts,density_opts};
prompts = {viscous_prompt,plastic_prompt,softening_prompt,energy_prompt,density_prompt};
figure(fh);clf;
set(fh,'units','points', 'Position',[50 100 220 400]);
[D_entry{i},D_litho_type{i}] = GUI_define_lithologies(fh,string,prompt,typedef);
options = [];
options_prompt = [];
for j =1:length(D_litho_type{i})
    options        = [options,opts{j}{D_litho_type{i}(j)}];
    options_prompt = [options_prompt,prompts{j}{D_litho_type{i}(j)}];
end
close(fh);
default            = cellstr(repmat('0',length(options),1));
opt.Resize='on';
opt.WindowStyle='modal';
opt.Interpreter='tex';
D_data{i}    = inputdlg(options_prompt,['Properties for',D_entry{i}],1,default,opt);
n_litho = i;
save('default_lithologies','D_data','D_litho_type','D_entry','n_litho');
end


function [mylitho,type]=GUI_define_lithologies (fh,string,prompt,typedef)

phase_h = uicontrol(fh,'Style','edit',...
    'String','Enter the name of the lithology',...
    'Position',[20 450 200 25]);

L=130;h=14;x0=20;y0=370;dy1=30;dy2=20;
for i= 1: length(typedef)
    y0=y0-dy1;
    text      = uicontrol(fh,'Style','text',...
        'String',prompt(i),'Value',1,'Position',[x0 y0 L h]);
    y0=y0-dy2;
    handle(i) = uicontrol(fh,'Style','popupmenu',...
        'String',string{i},...
        'Value',typedef(i),'Position',[x0 y0 L h]);
end

y0 = y0-50;
save_h   = uicontrol(fh,'Style','pushbutton',...
    'String','enter properties',...
    'Position',[x0 y0 L 30],...
    'Callback','uiresume(gcbf)');

uiwait(gcf);

mylitho     = get(phase_h,'String');

for i =1:length(typedef)
    type(i) = get(handle(i),'Value');
end
if (type(1) > 1)
    type(4) =2;
end

end