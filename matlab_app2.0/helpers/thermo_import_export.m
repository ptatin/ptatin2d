function thermo_import_export()
fname = 'wet_mantle'
load (fname);
Pscale = 1e5; % bar -> pascal 
%check

eta = 1e6*0.25*(1e-15).^((1./n)-1).*(0.75*A).^(-1./n).*exp(Q./(n.*8.3.*(T+273)));

figure(1);pcolor(T,P,log10(eta)); shading flat;colorbar;
title('Viscosity with strain rate set to 1e^{-15}');xlabel('Temperature ^oC'); ylabel('Pressure MPa');

figure(2);pcolor(T,P,rho); shading flat;colorbar;
title('Density');xlabel('Temperature ^oC'); ylabel('Pressure MPa');

% export 

rheolmap_write(rho,[fname,'.densmap'],P*Pscale,T);
rheolmap_write(A,[fname,'.Amap'],P*Pscale,T);
rheolmap_write(Q,[fname,'.Qmap'],P*Pscale,T);
rheolmap_write(n,[fname,'.nexpmap'],P*Pscale,T);

end






function rheolmap_write(data,filename,P,T)
[nT,nP]=size(data);
fid2 = fopen([filename],'w');
fprintf(fid2,'number vary first in T and than P \n');
fprintf(fid2,'%d\n', nT);
fprintf(fid2,'%d\n' ,nP);
fprintf(fid2,'%1.8e %1.8e %1.8e %1.8e\n',T(1,1),P(1,1),T(end,1),P(1,end));
fprintf(fid2,'%e ',data);
fclose(fid2);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ret]=smooth2(data,sz)
sz = [sz sz];
sz = sz(:)';
padSize = (sz-1)/2;
if ~isequal(padSize, floor(padSize)) || any(padSize<0)
    error(id('InvalidSizeValues'),'All elements of SIZE must be odd integers greater than or equal to 1.');
end
smooth = ones(sz)/prod(sz);
ret=convn(padreplicate(data,padSize),smooth, 'valid');

end

function b=padreplicate(a, padSize)
%Pad an array by replicating values.
numDims = length(padSize);
idx = cell(numDims,1);
for k = 1:numDims
    M = size(a,k);
    onesVector = ones(1,padSize(k));
    idx{k} = [onesVector 1:M M*onesVector];
end
b = a(idx{:});
end