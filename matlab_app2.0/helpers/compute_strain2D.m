%function [Xini,Yini,Xend,Yend,N_tetra,vertices]=compute_strain2D()
function compute_strain2D()

load ('./tracersdata');
path0= './';
step = 3;
ind = find(PID(:,step)==0);
X(ind,:)=[];
Y(ind,:)=[];

Xini = X(:,1)';
Yini = Y(:,1)';


Xend = X(:,step)';
Yend = Y(:,step)';


%We don't need this anymore
    clear X Y P T;

[N_tetra,vertices] = meshmarkers(Xini,Yini,Xend,Yend);
%return
[L1,L3,V1,V3,Centre,D,R] = compute_strain(N_tetra,Xini,Yini,Xend,Yend,vertices);

%figure(1); clf; 
%quiver(Centre(:,1),Centre(:,2),V1(:,1).*L1,V1(:,2).*L1,'r')
%hold on; quiver(Centre(:,1),Centre(:,2),V3(:,1).*L3,V3(:,2).*L3,'g')
%axis equal

write_paraview(path0,step,N_tetra,L1,L3,V1,V3,Centre,D,R)

end





function [L1,L3,V1,V3,D] = ellispoid2D_triangle(x,X)

% Compute finite strain ellipsoide in 3D from coordinates
%
%
% input :
%   - x : initial coordinates of nodes
%                -       -
%               |x1 x2 x3 |
%           x = |y1 y2 y3 |
%                -       -
%
%   - X : final coordinates of nodes
%                -       -
%               |X1 X2 X3 |
%           X = |Y1 Y2 Y3 |
%                -       -
%
% Output :
%   - L1,  L3 : eigen values in decreasing order
%   - V1,  V3 : unit eigen vectors same order
%==============================================================================================

% 1. Compute des vectors u et U describing initial and final mesh


u = [x(:,2)-x(:,1),x(:,3)-x(:,1)];

U = [X(:,2)-X(:,1),X(:,3)-X(:,1)];

% 2. least square computation of displacement gradient tensor

D = (u'\U')';

% 3. finite strain tensor
E = inv(D*D');

% 4. Eigen values
[Vect,Diag]=eig(E);
[Vect,Diag]=sortem(Vect,Diag);

% 5. norms
L1 = 1/sqrt(abs(Diag(1,1)))/2;
L3 = 1/sqrt(abs(Diag(2,2)))/2;

% 6. eigenvectors
V1 = Vect(:,1);
V3 = Vect(:,2);

end

function [P2,D2]=sortem(P,D)
% this function takes in two matrices P and D, presumably the output 
% from Matlab's eig function, and then sorts the columns of P to 
% match the sorted columns of D (going from largest to smallest)
D2=diag(sort(diag(D),'ascend')); % make diagonal matrix out of sorted diagonal values of input D
[c, ind]=sort(diag(D),'ascend'); % store the indices of which columns the sorted eigenvalues come from
P2=P(:,ind); % arrange the columns in this order
end


function [L1,L3,V1,V3,Centre,D,R] = compute_strain(N_tetra,Xini,Yini,Xend,Yend,vertices)

L1     = zeros(N_tetra,1);
L3     = zeros(N_tetra,1);
Centre = zeros(N_tetra,2);
V1     = zeros(N_tetra,2);
V3     = zeros(N_tetra,2);

for i =1 : N_tetra
    x = [Xini(vertices(i,:));Yini(vertices(i,:))];
    X = [Xend(vertices(i,:));Yend(vertices(i,:))];
    Centre(i,:)=mean(X,2);
    [LL1,LL3,VV1,VV3] = ellispoid2D_triangle(x,X);
    L1(i,1) = LL1;
    L3(i,1) = LL3;
    V1(i,:) = VV1';
    V3(i,:) = VV3';
    
end

R = L1./L3;      % aspect ratio of the finite strain ellipsoid
D = 1-L3./L1;    % intensity of strain 0: no strain, 1: infinite strain

end


function [to_trash] = check_jacobien(Xini,Yini,Xend,Yend,N_tetra,vertices)

V_ini = zeros(1,N_tetra);
V_end = zeros(1,N_tetra);


for i = 1 : N_tetra
    A = [Xini(vertices(i,1)) Yini(vertices(i,1))];
    B = [Xini(vertices(i,2)) Yini(vertices(i,2))];
    C = [Xini(vertices(i,3)) Yini(vertices(i,3))];
    AB = (B-A);
    AC = (C-A);
    S_ini(i) = det([AB;AC]);
    
    A = [Xend(vertices(i,1)) Yend(vertices(i,1)) ];
    B = [Xend(vertices(i,2)) Yend(vertices(i,2)) ];
    C = [Xend(vertices(i,3)) Yend(vertices(i,3)) ];
    AB = (B-A);
    AC = (C-A);
    S_end(i) = det([AB;AC]);
end

OKjack = sign(S_ini).*sign(S_end);
OKsurf = S_end./S_ini>eps*1e5;
OK=and(OKjack,OKsurf);
to_trash = find(OK~=1);
end

function [N_tetra,vertices] = meshmarkers(Xini,Yini,Xend,Yend)


vertices   = delaunay(Xini,Yini);
N_tetra = length(vertices);
we_dont_keep = check_jacobien(Xini,Yini,Xend,Yend,N_tetra,vertices);
vertices(we_dont_keep ,:)=[];
N_tetra = length(vertices);

%figure(1); 
%triplot(vertices,Xini,Yini,'g'); hold on; 
%triplot(vertices,Xend,Yend,'r'); hold off;

end


function [] = write_paraview(path0,N_step_gale,N_tetra,L1,L3,V1,V3,Centre,D,R)


%       ========================
Centre = [Centre,Centre(:,1)*0];
coord = reshape(Centre',1,N_tetra*3);
V1    = [V1,V1(:,1)*0];
V3    = [V3,V3(:,1)*0];
name = {'length_1','length_3','intensity_D','ratio_R','axis_1','axis_3'};
data = {L1,L3,D,R,V1,V3};

%
file = [path0,'lineation',num2str(N_step_gale),'.vtu'];
fid  = fopen(file,'w');
% ===========HEADER======================

fprintf(fid,'<?xml version="1.0"?>\n');
fprintf(fid,'<VTKFile type="UnstructuredGrid" version="0.1" byte_order="LittleEndian">\n');
fprintf(fid,'\t<UnstructuredGrid>\n');
fprintf(fid,'\t\t<Piece NumberOfPoints="%d" NumberOfCells="1">\n',N_tetra);

%=========COORDINATES========================================= 
fprintf(fid,'\t\t\t<Points>\n');
fprintf(fid,'\t\t\t\t<DataArray type="Float64" NumberOfComponents="3" format="ascii">\n');
fprintf(fid,'%7.6f ',coord);
fprintf(fid,'\n');
fprintf(fid,'\t\t\t\t</DataArray>\n');
fprintf(fid,'\t\t\t</Points>\n');

%=========DATA=========================================
fprintf(fid,'\t\t\t<PointData Scalars="intensity_D" Vectors="axis_1">\n');
%fprintf(fid,'\t\t\t<PointData Scalars="Strain" Vectors="Strain_axis">\n');
for i=1:size(name,2)    
    fprintf(fid,'\t\t\t\t<DataArray type="Float64" Name="%s" format="ascii" NumberOfComponents="%d">\n',name{i},size(data{i},2));
    fprintf(fid,'%7.6f ',(data{i})');
    fprintf(fid,'\t\t\t\t</DataArray>\n');
end
fprintf(fid,'\t\t\t</PointData>\n');

%%%%%%%%%%%% CELLS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf(fid,'\t\t\t<CellData>\n');
fprintf(fid,'\t\t\t</CellData>\n');

fprintf(fid,'\t\t\t<Cells>\n');
fprintf(fid,'\t\t\t\t<DataArray type="Int32" Name="connectivity" format="ascii">\n');
conn = 1:N_tetra;
fprintf(fid,'%d ',conn);
fprintf(fid,'\n');
fprintf(fid,'\t\t\t\t</DataArray>\n');

fprintf(fid,'\t\t\t\t<DataArray type="Int32" Name="offsets" format="ascii">\n');
fprintf(fid,'%d ',N_tetra);
fprintf(fid,'\n');
fprintf(fid,'\t\t\t\t</DataArray>\n');

fprintf(fid,'\t\t\t\t<DataArray type="UInt8" Name="types" format="ascii">\n');
fprintf(fid,'2\n');
fprintf(fid,'\t\t\t\t</DataArray>\n');
fprintf(fid,'\t\t\t</Cells>\n');
%%%%%%%%%%%%%%%%%CLOSE FILE %%%%%%%%%%%%%%%%%%%%%%%%

fprintf(fid,'\t\t</Piece>\n');
fprintf(fid,'\t</UnstructuredGrid>\n');
fprintf(fid,'</VTKFile>\n');


fclose(fid);
end
