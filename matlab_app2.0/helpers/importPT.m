function importPT()
nproc = 1; % number of processor used 
nstepsave = 20; % frequency output 
nstepsaved = 480; % total number of steps
npoints    = 10400; % total number of points all processors 
matfile    = '/Users/laetitia/codes/ptat2d-spm/bens_model/test2/tracersdata'
path       = '/Users/laetitia/codes/ptat2d-spm/bens_model/test2';
import_ptatin(path,npoints,nstepsaved,nstepsave,nproc,matfile); 

end

function import_ptatin(path,npoints,nstepsaved,nstepsave,nproc,matfile)

PID  = []; X= [];Y = [];P=[]; T=[]; 
for istep=0:nstepsave:nstepsaved
    data=zeros(npoints,6);
    for iproc=0:nproc-1
        nomf = sprintf('%s/step%05d_Passive_PT-subdomain%05d.dat',path,istep,iproc)
        newData1 = importdata(nomf, ' ', 2);
        indx = newData1.data(:,1);
        data(indx,1)=newData1.data(:,1);
        data(indx,2)=newData1.data(:,2);
        data(indx,3)=newData1.data(:,3);
        data(indx,4)=newData1.data(:,4);
        data(indx,5)=newData1.data(:,5);
        data(indx,6)=newData1.data(:,6);
    end
    PID = [PID,data(:,1)];
    X   = [X,data(:,3)];
    Y   = [Y,data(:,4)];
    P   = [P,data(:,5)];
    T   = [T,data(:,6)];
    t   = newData1.textdata{2};
    time(istep+1) = str2double(t(8:end));
    
    save(matfile,'X','Y','P','T','PID','time','npoints');
    %plot(X',Y');
end

end

