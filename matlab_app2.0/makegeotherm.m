
function makegeotherm(filename,l)

liste = {'follow asthenosphere','davies slab','ocean constant','ocean spreading',...
    'constant','linear','mantle','continent','continent_following asth','continent_following cont','spreading slabG','spreading slabD'};
s = sprintf(['Analytical solution always assume constant kappa...\n',...
    'Which constant value do you wish to use?']);
kappa = str2num(char(inputdlg({s},'pTatinMatlabApp',1,{'1e-6'})));

% load data
load ([filename,'_phase.mat']);
load ([filename,'_basic.mat']);
if l > 0
    load ([filename,'_therm_ana.mat']);
end
fh = 50;
fh2 = 10;
figure(fh);clf;
set(fh,'Position',[0 0 400 300]);
pcolor(X,Y,double(phase2D));shading interp; axis equal tight; caxis([0 max(phase2D(:))]); title('Your phase index');colorbar horiz;hold off

% initiate variable
i=l;
create_geotherm = 'Yes';
while strcmp(create_geotherm,'Yes')
    
    h =[];
    %increment the analytical solution counter
    i=i+1;
    % print to the screen the type of analytical solution that are
    % implemented
    typebc = menu('Which analytical solution do you want to use',liste);
    happy = 'No';
    while strcmp(happy,'No')
        switch typebc
            case find(strcmp('continent',liste))
                prompt  = {'temperature at the top','temperature at the base of the lithosphere','lithospheric depth','radiogenic prod (K/s)','radiogenic fold','thermal age'};
                default = {'0','1300','150e3','1e-12','10e3','200*3.14e13'};
                answer = str2num(char(inputdlg(prompt,'pTatin2DMatlabApp ',1,default)));
                [T,h]  = continental_lithosphere(X,Y,answer,kappa);
                size(T)
            case find(strcmp('continent_following asth',liste))
                prompt  = {'temperature at the top','temperature at the base of the lithosphere','asthenospheric phase','radiogenic prod','radiogenic fold','thermal age','max litho depth'};
                default = {'0','1300','3','1e-12','10e3','200*3.14e13','-200e3'};
                answer = str2num(char(inputdlg(prompt,'pTatin2DMatlabApp ',1,default)));
                [T,h] = continental_lithosphere_follow(X,Y,answer,kappa,double(phase2D));
            case find(strcmp('continent_following cont',liste))
                prompt  = {'temperature at the top','temperature at the base of the lithosphere','lithospheric phases','radiogenic prod','radiogenic fold','thermal age','max litho depth'};
                default = {'0','1300','3','1e-12','10e3','200*3.14e13','-200e3'};
                answer = inputdlg(prompt,'pTatin2DMatlabApp ',1,default);
                [T,h] = continental_lithosphere_follow_cont(X,Y,answer,kappa,double(phase2D));
            case find(strcmp('constant',liste))
                prompt  = {'Constant temperature'};
                default = {'1200'};
                answer = str2num(char(inputdlg(prompt,'pTatin2DMatlabApp ',1,default)));
                T0 = answer;
                T  = ones(size(X))*T0;
                
            case find(strcmp('linear',liste))
                prompt  = {'temperature at ytop','temperature at ybot','ytop','ybot'};
                default = {'0' '1300',num2str(max(Y(:))),num2str(min(Y(:)))};
                answer = str2num(char(inputdlg(prompt,'pTatin2DMatlabApp ',1,default)));
                T0 = answer(1);
                T1 = answer(2);
                Y0 = answer(3);
                Y1 = answer(4);
                T = (Y-Y0)*(T1-T0)/(Y1-Y0)+T0;
                
            case find(strcmp('ocean constant',liste))
                prompt  = {'temperature of the surface','temperature of the asthenosphere','Age of the ocenic lithosphere'};
                default = {'0' '1300','20*3.14e13'};
                answer = str2num(char(inputdlg(prompt,'pTatin2DMatlabApp ',1,default)));
                T0 = answer(1);
                T1 = answer(2);
                age = answer(3);
                T = erf(-Y/sqrt(4*kappa*age))*(T1-T0)+T0;
                
            case find(strcmp('ocean spreading',liste))
                
                prompt  = {'temperature of the surface','temperature of the asthenosphere',...
                    'spreading rate','abscisse of the spreading centre'};
                default = {'0' '1300','4*3.1e-10','300e3'};
                answer = str2num(char(inputdlg(prompt,'pTatin2DMatlabApp ',1,default)));
                T0 = answer(1);
                T1 = answer(2);
                v  = answer(3);
                xs = answer(4);
                age = abs(X-xs)/v;
                T = erf(-Y./sqrt(4*kappa.*age))*(T1-T0)+T0;
                
            case find(strcmp('follow asthenosphere',liste))
                prompt  = {'asthenospheric pixelID','temperature of the base of the lithosphere',...
                    'Y cuttoff the base of the lithosphere','temperature at the base of the model'};
                default = {'5' '1300','-200e3','1500'};
                answer  = str2num(char(inputdlg(prompt,'pTatin2DMatlabApp ',1,default)));
                i_astheno = answer(1);
                T_litho   = answer(2);
                z_max     = answer(3);
                Tbot      = answer(4);
                T         = follow_asthenosphere2(double(phase2D),Length,Origine,i_astheno,z_max,T_litho,X,Y,Tbot);
                
            case find(strcmp('mantle',liste))
                prompt  = {'depth of the lithosphere (m)','temperature of the base of the lithosphere (K)',...
                    'adiabatic gradient (K/km)','amplitude of the noise (peak-to-peak value)'};
                default = {'-200e3' '1300','0.3','100'};
                answer  = str2num(char(inputdlg(prompt,'pTatin2DMatlabApp ',1,default)));
                T_litho   = answer(2);
                z_max     = answer(1);
                Tl = answer(2);
                ga = -answer(3)/1000.0;
                A  = answer(4);
                y  = Y(1,:);
                
                Trand = (rand(size(Y))-0.5)*A;
                Tmantle = zeros(size(Y));
                Tmantle = ga * (Y-z_max) +  T_litho ;
                Tmantle(find(Y>z_max)) = T_litho;
                T = Tmantle + Trand;
                
            case find(strcmp('spreading slabD',liste))
                prompt  = {'thickness under the slab','velocity of the slab','age of the slab'...
                    'temperature at the top of the slab ','temperature of the asthenosphere'};
                default = {'200e3','4*3e-10','0*3.14e13','0','1350'};
                answer  = str2num(char(inputdlg(prompt,'pTatin2DMatlabApp ',1,default)));
                
                t = answer(1);
                velocity = answer(2);
                age_ini = answer(3);
                T0 = answer(4);
                T1 = answer(5);
                msgbox('Define the geometry of the slab (Press enter to begin point selection and enter to end selection)'); pause
                pause;
                [xslab,yslab]=ginput();
                figure(fh); hold on
                plot(xslab,yslab,'+k');
                eslab=linspace(0,t,50);
                eta  = [0,cumsum(sqrt((diff(yslab')).^2+(diff(xslab')).^2))];
                alpha        = atan ( diff(yslab') ./ diff(xslab') );
                alpha(end+1) = alpha(end);
                
                xr=[];yr=[];T_r=[];
                for i = 1:(length(eta)-1)
                    blah = linspace(eta(i),eta(i+1),ceil((eta(i+1)-eta(i))/(t/100)));%;
                    [XI,ETA] = meshgrid(blah,eslab);
                    %T_r= Davies_ana(d,hau,kappa,T1,T0,velocity,XI,ETA);
                    
                    age = abs(XI-0)/velocity+age_ini;
                    T_r = [T_r,erf(ETA./sqrt(4*kappa.*age))*(T1-T0)+T0];
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%
                    coord = [XI(:)-min(XI(:)),ETA(:)-min(ETA(:))]';
                    R = [cos(alpha(i)),sin(alpha(i));-sin(alpha(i)),cos(alpha(i))];
                    coord = R*coord;
                    xr           =[xr, xslab(i)+coord(1,:)];
                    yr           =[yr, yslab(i)-coord(2,:)];
                end
                F            = TriScatteredInterp(xr(:),yr(:),T_r(:));
                A            = F(X,Y);
                %A            = griddata (xr,yr,T_r,X,Y);
                A(isnan (A)) = T1;
                T            = A;
                
            case find(strcmp('spreading slabG',liste))
                prompt  = {'thickness under the slab','velocity of the slab','age of the slab'...
                    'temperature at the top of the slab ','temperature of the asthenosphere'};
                default = {'200e3','4*3e-10','0*3.14e13','0','1350'};
                answer  = str2num(char(inputdlg(prompt,'pTatin2DMatlabApp ',1,default)));
                
                t = answer(1);
                velocity = answer(2);
                age_ini = answer(3);
                T0 = answer(4);
                T1 = answer(5);
                msgbox('Define the geometry of the slab (Press enter to begin point selection and enter to end selection)'); pause
               pause
                figure(fh);
                [xslab,yslab]=ginput();
                figure(fh); hold on
                plot(xslab,yslab,'+k');
                eslab=linspace(0,t,50);
                eta  = [0,cumsum(sqrt((diff(yslab')).^2+(diff(xslab')).^2))];
                alpha        = atan ( diff(yslab') ./ diff(xslab') );
                alpha(end+1) = alpha(end);
                
                
                xr=[];yr=[];T_r=[];
                for i = 1:(length(eta)-1)
                    blah = linspace(eta(i),eta(i+1),ceil((eta(i+1)-eta(i))/(t/100)));%;
                    [XI,ETA] = meshgrid(blah,eslab);
                    age = abs(XI-0)/velocity+age_ini;
                    T_r = [T_r,erf(ETA./sqrt(4*kappa.*age))*(T1-T0)+T0];
                    
             
                    coord = [XI(:)-min(XI(:)),ETA(:)-min(ETA(:))]';
                    R = [cos(alpha(i)),-sin(alpha(i));sin(alpha(i)),cos(alpha(i))];
                    coord = R*coord;
                    xr           =[xr, xslab(i)-coord(1,:)];
                    yr           =[yr, yslab(i)-coord(2,:)];
                end
                F            = TriScatteredInterp(xr(:),yr(:),T_r(:),'natural');
                A            = F(X,Y);
                
                A(isnan (A)) = T1;
                T            = A;
                
                
                
            case find(strcmp('davies slab',liste))
                prompt  = {'thickness over the slab','thickness under the slab',...
                    'thickness of the slab ','velocity of the slab'...
                    'temperature at the top of the slab ','temperature of the asthenosphere'};
                default = {'50e3' '100e3','70e3','4*3e-10','300','1350'};
                answer  = str2num(char(inputdlg(prompt,'pTatin2DMatlabApp ',1,default)));
                
                ytop = answer(1);
                ybot = answer(2);
                hau = answer(3);
                d=hau/5;
                velocity = answer(4);
                T0 = answer(5);
                T1 = answer(6);
                msgbox('enter has precisely as you can the shape of the slab you should now if needed\n'); pause
                [xslab,yslab]=ginput();
                figure(10); hold on
                plot(xslab,yslab,'k');
                eslab=linspace(-ytop,ybot,300);
                eta  = [0,cumsum(sqrt((diff(yslab')).^2+(diff(xslab')).^2))];
                % x is the axis normal to the slab
                [ETA,XI] = meshgrid(eta,eslab);
                T_r= Davies_ana(d,hau,kappa,T1,T0,velocity,XI,ETA);
                %%%%%%%%%%%%%%%%%%%%%%%%%%%
                alpha        = atan ( diff(yslab') ./ diff(xslab') );
                alpha(end+1) = alpha(end);
                xr           = (ones(size(eslab'))*xslab')+XI.*(ones(size(eslab'))*sin(alpha));
                yr           = (ones(size(eslab'))*yslab')-XI.*(ones(size(eslab'))*cos(alpha));
                A            = griddata (xr,yr,T_r,X,Y);
                A(isnan (A)) = T1;
                T            = A;
                
        end
        figure(fh2);clf;
        set(fh2,'Position',[0 350 400 300]);
        pcolor(X,Y,T);shading interp; axis equal tight; title('Your temperature field');colorbar horiz;
        
        happy = questdlg('Happy with the result?','ptatin2dMatlabApp','Yes','No','No');
    end
    name{i} = char(inputdlg({'Name of the solution'},'Create subMask for particles ',1,{'mysolution_name'}));
    [Tana(:,:,i)] = T;
    if isempty(h)
        h=zeros(size(T));
    end
    [Prod(:,:,i)] = h;
    save([filename,'_therm_ana'],'Tana','name','X','Y','Prod');
    create_geotherm = questdlg('Do you want to prepare another temperature field?','ptatin2dMatlabApp','Yes','No','No');
    close(fh2);
end
close(fh);


end

function [T,h]  = continental_lithosphere(X,Y,answer,kappa)
age = answer(6);
t_litho = answer(2);
t_top = answer(1);
hl    = answer(3);
hs    = answer(4);
hr    = answer(5);
Y = Y(:,1);
% hs is in K/s
t_radio = (hs*hr^2/kappa);
if (age == 0)
    T = oneD_cont(hl,t_top,t_litho,t_radio,Y,hr);
else
    T = oneD_nstat(age,kappa,hl,t_top,t_litho,Y,t_radio,hr);
end
h  = hs*exp(Y/hr).*(Y >-hl);
T = repmat(T,1,size(X,2));
h = repmat(h,1,size(X,2));
end

function [T,h]  = continental_lithosphere_follow(X,Y,answer,kappa,limite)
i_astheno=answer(3);
age = answer(6);
t_litho = answer(2);
t_top = answer(1);
hs    = answer(4);
hr    = answer(5);
t_radio = (hs*hr^2/kappa);
y_max = answer(7);
limite(find(limite==i_astheno)) = -1;
limite(find(Y<y_max)) = -1;
YY=Y;
YY(find(limite<=0))=0;
y_litho = min(YY);
y_litho =smooth(y_litho,ceil(length(y_litho)/10),'sgolay');
for i = 1:size(X,2)
    if (age==0)
        T(:,i) = oneD_cont(-y_litho(i),t_top,t_litho,t_radio,Y(:,i),hr);
    else
        T(:,i) = oneD_nstat(age,kappa,-y_litho(i),t_top,t_litho,Y(:,i),t_radio,hr);
    end
    h(:,i)  = hs*exp(Y(:,i)/hr).*(Y(:,i)>y_litho(i));
end

end
function [T,h]  = continental_lithosphere_follow_cont(X,Y,answer,kappa,phase2D)
i_litho=str2num(answer{3});
age = str2num(answer{6});
t_litho = str2num(answer{2});
t_top = str2num(answer{1});
hs    = str2num(answer{4});
hr    = str2num(answer{5});
t_radio = (hs*hr^2/kappa);
for i =1:length(i_litho)
    phase2D(find(phase2D==i_litho(i))) = -1;
end
YY=Y;
dummy = min(Y(:));
YY(find(phase2D<0))=dummy;
y_litho = max(YY);
y_litho = smooth(y_litho,ceil(length(y_litho)/10),'sgolay');

for i = 1:size(X,2)
    if (age==0)
        T(:,i) = oneD_cont(-y_litho(i),t_top,t_litho,t_radio,Y(:,i),hr);
    else
        T(:,i) = oneD_nstat(age,kappa,-y_litho(i),t_top,t_litho,Y(:,i),t_radio,hr);
    end
    h(:,i)  = hs*exp(Y(:,i)/hr).*(Y(:,i)>y_litho(i));
end

end


function T = oneD_cont(hl,t_top,t_litho,t_radio,Y,hr)
T = t_litho*ones(size(Y));
dtdy     = (t_litho-t_radio-t_top)/hl;
T = min((t_top-dtdy*Y+t_radio*(1-exp(Y/hr))),T);
end

function T = oneD_nstat(age,kappa,hl,t_top,t_litho,Y,t_radio,hr)
T = oneD_cont(hl,t_top,t_litho,t_radio,Y,hr);
characteristic_time = age/hl^2*kappa;
pp = reshape([ones(1,50);-ones(1,50)],1,100);
n  = [1:100];
tt = sin(pi*(hl+Y)/hl*n)*(pp./n.*exp(-(n*pi).^2*characteristic_time))'*2./pi*(t_litho-t_top).*(Y>-hl);
T = T+tt;
end
function [T] = follow_asthenosphere(limite,Length,Origine,i_astheno,z_max,T_litho,XX,YY)
%z_max = -2;
%i_astheno = 7;
%T_litho =1300;

limite(find(limite==i_astheno)) = 0;
limite(find(YY<z_max)) = -1;
Y=YY;Y(find(limite<=0))=0;
y_litho = min(Y);
y  = YY(1,:);
x  = YY(:,1);
dy = [y*0;diff(YY,1)];
gradient(:,:,1) = ones(size(x))*(-T_litho./y_litho).*(YY>ones(size(x))*y_litho).*dy;
gradient(:,:,2) = ones(size(x))*(100./(y_litho-z_max)).*(YY<ones(size(x))*y_litho & YY>z_max).*dy;
gradient(:,:,3) = (ones(size(YY))+rand(size(YY))/10)*0.4*1e2.*dy.*(YY<=z_max);
T=cumsum(sum(gradient,3));
T=-T+max(T(:));
end



function [T] = follow_asthenosphere2(limite,Length,Origine,i_astheno,z_max,T_litho,XX,YY,Tbot)
%z_max = -2;
%i_astheno = 7;
%T_litho =1300;

limite(find(limite==i_astheno)) = 0;
limite(find(YY<z_max)) = -1;
Y=YY;Y(find(limite<=0))=0;
y_litho = min(Y);
y  = YY(1,:);
x  = YY(:,1);
dy = [y*0;diff(YY,1)];
Ybot = min(YY(:));
gradient(:,:,1) = ones(size(x))*(-T_litho./y_litho).*(YY>ones(size(x))*y_litho).*dy;
gradient(:,:,2) = ones(size(x))*((Tbot-T_litho)./(y_litho-Ybot)).*(YY<ones(size(x))*y_litho & YY>Ybot).*dy;
%gradient(:,:,2) = gradient(:,:,2)+(rand(size(YY))*50);


T=cumsum(sum(gradient,3));
T=-T+max(T(:));
%T=T+((rand(size(YY))-0.5)*50);
end


function [T] = Davies_ana(d,h,kappa,T1,T0,v,X,Y)
const = sqrt(4*Y*kappa/v);
a = X./const;
b = (X-h)./const;
e = (X+d)./const;
f = a;

T = T1+(T1-T0)/h.*((X-h)/2.*(erf(-b)-erf(-a))-sqrt(kappa*Y/pi/v).*(exp(-b.^2)-exp(-a.^2)))...
    +(T1-T0)/d.*((X+d)/2.*(erf(-e)-erf(-f))-sqrt(kappa*Y/pi/v).*(exp(-e.^2)-exp(-f.^2)));
end


