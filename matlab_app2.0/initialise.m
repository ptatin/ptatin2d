function [filename,EnergySolver,isostatic_topo] = initialise(path)

s = {'\fontname{times}\fontsize{16}\it{Welcome to matlab\_app2.0}',...
    '\fontsize{14}This program parses input file to be used with model GENE in ptatin2d',...
    '\fontsize{12}To get started make sure that:',...
    ' \rm * your geometry is stored in one of the following file formats \{png, tiff\}', ...
    '','This GUI will guide you to set up your input file to work with ptatin_two_stage.app and model GENE',...
    '', 'Do not hesitate to press on help or contact me to report bugs Laetitia.le_pourhiet@upmc.fr',...
    '\fontsize{24}READY?'
    };
createmode.Interpreter='tex';
createmode.Default='START';
h = questdlg(s,'Welcome to matlab_app2.0','START','NO','RESTART',createmode);
options.Resize='on';
options.WindowStyle='normal';
options.Interpreter='tex';
switch h
    case 'NO',
        error('You are not yet ready for pTatin_MatlabApp2.0');
    case 'START'
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%% SET PATH HERE %%%%%%%%%%%%%%%%%%
        % Define the filename for the geometry if you have one
        %fname = 'barles-01.png';
        [file,PathName] = uigetfile({'*.png';'*.tif'},'Select the graphic file',path);
        fname = [PathName,file];
        % what name of file do you want temporary auxiliary files ?
        filename = [PathName,file(1:end-4)];
        % what name of file do you want for your pTat option file ?
        filename2 = [PathName,file(1:end-4)];
        % what is the name of the directory in which you wanna store pTat output?
        outputpath = file(1:end-4);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%   MODEL DATA   %%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % what is the size of the model? in meter enter
        Origine = [0.0   -6. 0.0]*1e5; %[x0,y0]
        Length  = [18.0   0.0 0.0]*1e5; %[xend,yend]
        % what is resolution the model? [ nx, ny, nz ]
        nel     = [150 50 0];
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %DEFINE WHAT KIND OF MODEL YOU WANT TO RUN HERE%%%%%%%
        isostatic_topo = true; % true to compute isostatically compensated  topography
        EnergySolver   = true; % true to solve for temperature
        cordupdate     = true;
        %    ------------xend/yend
        %    |           |
        %    |           |
        % x0/y0-----------
        % What is your gravity acceleration?
        gravity = 10;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%   Numerical parameters            %%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%   Advanced numerical parameters   %%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % What is your scaling ?
        Length_scaling = 1e5;
        Vis_scaling    = 1e25;
        Vel_scaling    = 1e-10;
        Temp_scaling   = 1;
        % for non linear models it is menditory to set viscosity cut off for the continuation
        eta_min_cut_off = 1.e19;
        eta_max_cut_off = 1.e25;
        % timestep control
        courant_factor         = 0.1;
        surface_courant_factor = 1.0;
        dt_max                 = 6e12;
        max_surf_disp          = 50;
        dt_min                 = 1.5e11;
        cordupdate             = true;
        courant_factor         = 0.1;
        % maximum time you wanna run the model for? in second
        time_max = 50*3e13;
        nsteps   = 1000;
        % What is the output frequency for plotting?
        output_frequency = 20;
        
        %%%%%% TO DO IN A GUI ? %%%%
        % what kind of rheological model are you using ? (6)
        rheology_type = 6;
        % Velocity bc's type
        bctype = 0;
        % Velocity bc's value
        vx     = -3e-10*2;
        % temperature bc's ....
        Tbot=1500; Ttop=0;Tleft=[]; Tright=[];
        % particles parameters
        % initial number particle per cell in x and y
        lattice_layout_Nx = 4;
        lattice_layout_Ny = 4;
        % number of particles inserted per cell in x and y at each repopulation
        mp_popctrl_nxp = 3;
        mp_popctrl_nyp = 3;
        %minimum particule in the cell (repopulation criteria)
        mp_popctrl_np_lower = 12;
        % type of projection of particle value to mesh
        % 0 -> P_0  1-> Q_1
        coefficient_projection_type = 1;
        % Which viscosity Averaging ? 0 -> arithmetic 1->harmonic
        vis_average_type =0;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% NOTHING TO MODIFY HERE %%%%%%%%%%%%%%%%%%
    case 'RESTART'
        [file,PathName] = uigetfile({'*.mat'},'choose the *_basic.mat file to restart',path);
        restartfilename = [PathName,file];
        load(restartfilename);
        
end

prompt  = {'filename for the auxiliary files','filename for inputfiles',...
    'output directory name','graphic file name and path'};
default    = {filename, filename2,outputpath,fname};
answer     = inputdlg(prompt,'pTatin2DMatlabApp files',1,default,options);
filename   = answer{1};
filename2  = answer{2};
outputpath = answer{3};
fname      = answer{4};
if ~exist('cordupdate')
    cordupdate = true;
end


[Origine,Length,nel,isostatic_topo,EnergySolver,gravity,cordupdate] = basic_gui(Origine,Length,nel,isostatic_topo,EnergySolver,gravity,cordupdate)
prompt  = {'Characteristic length','Characteristic viscosity','Characteristic velocity','minimum viscosity','maximum viscosity'};
default         = {sprintf('%1.1g',Length_scaling),sprintf('%1.1g',Vis_scaling),sprintf('%1.1g',Vel_scaling),sprintf('%1.1g',eta_min_cut_off),sprintf('%1.1g',eta_max_cut_off)};
answer          = str2double(inputdlg(prompt,'pTatin2DMatlabApp Scaling & Cut off ',1,default,options));
Length_scaling  = answer(1);
Vis_scaling     = answer(2);
Vel_scaling     = answer(3);
eta_min_cut_off = answer(4);
eta_max_cut_off = answer(5);
%%%% just for compatibility to be removed
if ~exist('courant_factor')
    courant_factor = 0.1;
end

prompt           = {'Maximum time span','Maximum number of time steps','Frequency for graphics output',...
    'Maximum time step dt','Minimum time step dt','Courant Factor', 'viscosity averaging','projection marker to gausspoint'};
default          = {sprintf('%1.1g',time_max),sprintf('%1.5g',nsteps), sprintf('%1.5g',output_frequency),...
    sprintf('%1.1g',dt_max),sprintf('%1.1g',dt_min),sprintf('%1.1g',courant_factor), sprintf('%d',vis_average_type)...
    ,sprintf('%d',coefficient_projection_type)};
answer           = str2num(char(inputdlg(prompt,'pTatin2DMatlabApp time integration ',1,default)));
time_max         = answer(1);
nsteps           = answer(2);
output_frequency = answer(3);
dt_max           = answer(4);
dt_min           = answer(5);
courant_factor   = answer(6);
vis_average_type = answer(7);
coefficient_projection_type = answer(8);

[Ttop,Tbot,Tleft,Tright,bctype,vx] = bc_gui(Ttop,Tbot,Tleft,Tright,bctype,vx);


save([filename,'_basic']);
end