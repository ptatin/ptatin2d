function pixelmap_apply2(filename)
load([filename,'_basic.mat']);
fid = fopen([filename,'_phase.mat']);
if (fid > 2)
    fclose(fid);
    flag = questdlg('What do you want to do with your phasemap?','ptatin2dMatlabApp','Redefine phases','Update coeff','Nothing','Nothing');
else
    flag = 'Redefine phases';
end
if strcmp(flag,'Redefine phases')
    [nphase,phase2D,X,Y] = RGB2tatin(fname,Length,Origine);
    assign_properties(filename,double(phase2D),nphase,X,Y,EnergySolver);
    save([filename,'_phase'],'nphase','phase2D','X','Y');
end

if strcmp(flag,'Update coeff')
    [nphase,phase2D,X,Y] = RGB2tatin(fname,Length,Origine);
    modify_properties(filename,EnergySolver);
    save([filename,'_phase'],'nphase','phase2D','X','Y');
end
load([filename,'_phase.mat'],'phase2D');
pmap_write(phase2D,filename2,Origine,Length);
end

function assign_properties(filename,phasemap,nphase,X,Y,EnergySolver)
flag='No';
LayerPhase = zeros(nphase,1);
rho = zeros(nphase,1);
therm_exp = zeros(nphase,1);
done = 0;
i=0;
fid = fopen([filename,'_properties.mat']);
done = 'No';
while strcmp(done,'No')
    i=i+1;
    [myphase entry{i} litho_type{i} options{i} litho_prompt default]=...
        mygui(X,Y,phasemap,10,nphase,LayerPhase);
    opt.Resize='on';
    opt.WindowStyle='modal';
    opt.Interpreter='tex';
    data{i}    = inputdlg(litho_prompt,['Properties for ',entry{i}],1,default,opt);
    if ~isempty(data{i})
        LayerPhase(myphase+1) = i;
        rho(myphase+1) = str2num(data{i}{find(strcmp('rho',options{i}))});
        j = find(strcmp('alpha',options{i}));
        if isempty(j)
            therm_exp(myphase+1)  = 0;
        else
            therm_exp(myphase+1)  = str2num(data{i}{j});
        end
    else
        i = i-1;
    end
    save([filename,'_properties'],'LayerPhase','data', 'litho_type', 'options','done','entry','rho','therm_exp','i');
    if all(LayerPhase)
        done = questdlg('Are you done with your phasemap?','ptatin2dMatlabApp','Yes','No','No');
    end
end
n_litho = i;
save([filename,'_properties'],'LayerPhase','data', 'litho_type', 'options','n_litho','done','entry','rho','therm_exp','i');
end

function modify_properties(filename,EnergySolver)

load([filename,'_properties.mat']);
load('options_definitions.mat');
if EnergySolver
    energy_flag = 2;
else
    energy_flag = 1;
end

modify='Yes'
while strcmp(modify,'Yes')
    i = menu ('Which lithology to modify ?', entry);
    
    litho_prompt = [viscous_prompt{litho_type{i}(1)},plastic_prompt{litho_type{i}(2)},softening_prompt{litho_type{i}(3)},energy_prompt{energy_flag},density_prompt{litho_type{i}(4)}];
    opt.Resize='on';
    opt.WindowStyle='modal';
    opt.Interpreter='tex';
    datacorr    = inputdlg(litho_prompt,['Properties for',entry{i}],1,data{i},opt);
    if ~isempty (datacorr)
        data{i}=datacorr;
        myphase = find(LayerPhase==i)-1;
        
        rho(myphase+1) = str2num(data{i}{find(strcmp('rho',options{i}))});
        j = find(strcmp('alpha',options{i}));
        if isempty(j)
            therm_exp(myphase+1)  = 0;
        else
            therm_exp(myphase+1)  = str2num(data{i}{j});
        end
        
        save([filename,'_properties'],'LayerPhase','data', 'litho_type', 'options','n_litho','done','entry','rho','therm_exp','i');
    end
    modify = questdlg('Modify an other lithology?','ptatin2dMatlabApp','Yes','No','No');
end
end



function [nphase,phasemap,X,Y] = RGB2tatin(fname,Length,Origine)
% the file must be  RGB file, desactivate the anti-aliasing while saving
% it works fine with *.tif and *.png
% do not use jpg unless you want a thousands of phases
% the rest you can try
% on Illustrator save the file using artbox and create one that fits the
% model size
if isempty(fname)
    errordlg('User must provide a valid filename for the image')
end
I=imread(fname);
% figure(1);image(I);
% pause(1);
[X,map] = RGB2nphaseLocal(I);
nphase = size(map,1);
[nx,ny]=size(X);
for i=1:nx
    phasemap(nx-i+1,:)=X(i,:);
end
clear('X','I');

y=linspace(Origine(2),Length(2),size(phasemap,1)+1);
x=linspace(Origine(1),Length(1),size(phasemap,2)+1);
[X,Y] = meshgrid(x,y);
phasemap = [phasemap,phasemap(:,end)];
phasemap = [phasemap;phasemap(end,:)];
end

function [X,map] = RGB2nphaseLocal(img)
max_colors = 65536;
RGB=img;
N = floor(max_colors^(1/3)) - 1;

[x,y,z] = meshgrid((0:N)/N);
map = [x(:),y(:),z(:)];


RGB = round(im2doubleLocal(RGB)*N);
X = RGB(:,:,3)*((N+1)^2)+RGB(:,:,1)*(N+1)+RGB(:,:,2)+1;

[X,map] = cmunique(X,map);
end

function d = im2doubleLocal(img)

imgClass = class(img);

switch imgClass
    case {'uint8','uint16'}
        d = double(img) / double(intmax(imgClass));
    case {'single'}
        d = double(img);
    case 'double'
        d = img;
end
end



function  [myphase entry litho_type options options_prompt default] = mygui(X,Y,data,fh,nphase,i_litho)

load('default_lithologies.mat');
load('options_definitions.mat');
string  = {viscous_string,plas_string,soft_string,energy_string,density_string};
prompt  = {'Viscous type','Plastic type','Softening type','Energy type','Density type'};
typedef = [1,1,1,1,1];
opts = {viscous_opts,plastic_opts,softening_opts,energy_opts,density_opts};
prompts = {viscous_prompt,plastic_prompt,softening_prompt,energy_prompt,density_prompt};

figure(fh);
close(fh);
figure(fh);
set(fh,'units','points', 'Position',[50 100 800 400]);

uicontrol(fh,'Style','frame','Position',[10 10 240 480]);
h = axes('units','points','Position',[250 50 500 300]);
cmap  = hsv(nphase);
cmap1 = gray(nphase);
cmap(find(i_litho),:)=cmap1(find(i_litho),:);
pcolor(h,X,Y,data);shading flat; colorbar NorthOutside ; axis equal tight, title('Attribute the colored regions IDs to properties'); colormap(cmap);

%  Construct the components
text = uicontrol(fh,'Style','text',...
    'String','List the phase IDs to attribute',...
    'Value',1,'Position',[20 475 230 14]);

phase_h = uicontrol(fh,'Style','edit',...
    'String','0 1 3',...
    'Position',[20 450 200 25]);

text = uicontrol(fh,'Style','text',...
    'String','Name of the litho for these phases',...
    'Value',1,'Position',[20 425 230 14]);

entry_h = uicontrol(fh,'Style','edit',...
    'String','',...
    'Position',[20 400 200 25]);

text = uicontrol(fh,'Style','text',...
    'String','Choose material properties',...
    'Value',1,'Position',[20 385 130 14]);

litho_h = uicontrol(fh,'Style','popupmenu',...
    'String',['Start from scratch',D_entry],'Value',1,...
    'Position',[20 360 200 25],'Callback','uiresume(gcbf)');

help_h   = uicontrol(fh,'Style','pushbutton',...
    'String','HELP',...
    'Position',[20 15 100 50],...
    'Callback',@helpme);

uiwait(gcf);

options = [];
options_prompt = [];
i_litho = get(litho_h,'Value')-1;
name    = get(entry_h,'String');

if  i_litho > 0
    
    save_h   = uicontrol(fh,'Style','pushbutton',...
        'String','CONTINUE',...
        'Position',[140 30 100 50],...
        'Callback','uiresume(gcbf)');
    
    uiwait(gcf);
    if ~isempty(name)
        entry         = name;
    else
        entry         = D_entry{i_litho};
    end
    
    litho_type    = D_litho_type{i_litho};
    default       = D_data{i_litho};
    for i =1:length(litho_type)
        options        = [options,opts{i}{litho_type(i)}];
        options_prompt = [options_prompt,prompts{i}{litho_type(i)}];
    end
    
else
    [entry,litho_type] = GUI_define_lithologies(fh,string,prompt,typedef)
    for i =1:length(litho_type)
        options        = [options,opts{i}{litho_type(i)}];
        options_prompt = [options_prompt,prompts{i}{litho_type(i)}];
    end
    default            = cellstr(repmat('0',length(options_prompt),1));
end
myphase= str2num(get(phase_h,'String'));

close(fh)
end

function update(hObj,event,ax,data,nphase,X,Y) %#ok<INUSL>
% Called to set zlim of surface in figure axes
% when user moves the slider control
global myphase
cmap=hsv(nphase);

cmap1=gray(nphase);
cmap1(myphase+1,:)= cmap(myphase+1,:);
pcolor(ax,X,Y,data);shading flat; colorbar horiz; axis equal tight, title('PICKED UP REGIONS');colormap(cmap1);
%zlim(ax,[-val val]);
end

function [mylitho,type]=GUI_define_lithologies (fh,string,prompt,typedef)

phase_h = uicontrol(fh,'Style','edit',...
    'String','Enter the name of the lithology',...
    'Position',[20 350 200 25]);

help_h   = uicontrol(fh,'Style','pushbutton',...
    'String','HELP',...
    'Position',[20 30 100 50],...
    'Callback',@helpme2);

L=130;h=14;x0=20;y0=350;dy1=30;dy2=20;
for i= 1: length(typedef)
    y0=y0-dy1;
    text      = uicontrol(fh,'Style','text',...
        'String',prompt(i),'Value',1,'Position',[x0 y0 L h]);
    y0=y0-dy2;
    handle(i) = uicontrol(fh,'Style','popupmenu',...
        'String',string{i},...
        'Value',typedef(i),'Position',[x0 y0 L h]);
end

y0 = y0-50;
save_h   = uicontrol(fh,'Style','pushbutton',...
    'String','CONTINUE',...
    'Position',[x0+120 30 100 50],...
    'Callback','uiresume(gcbf)');

uiwait(gcf);

mylitho     = get(phase_h,'String');

for i =1:length(typedef)
    type(i) = get(handle(i),'Value');
end
if (type(1) > 1)
    type(4) =2;
end

end

function helpme(hObject,eventdata)
s = {'\fontname{times}\fontsize{12}\it{This GUI is meant to help you associate marker phase ID''s with material properties}',...
    '','\rm\bf{TOP BOX:}','\rm{Input the marker phase ID to which you want to assign properties}',...
    '\it{You have two possilities:}', '\rm   * list several marker phase ID by hand eg: 1 3 5','   * use '':'' to create list eg: 1:2:5',...
    '','\bf{DROP DOWN MENU} \rm gives you 2 options:',...
    '   * \it{option 1:\rm use one of the predefined properties in the menu and then click continue to modify these properties to your wish}',...
    '   * \it{option 2:\rm define material properties from scratch}','',...
    'If you use \it option 2\rm, new menu will appear to define your material'};

createmode.Interpreter='tex'
createmode.WindowStyle='nonmodal'
hhelp = msgbox(s,'How to assign properties to markers' ,'help',createmode);
set(hhelp,'Position',[412.8 364.8 500 300]);
end



function helpme2(hObject,eventdata)
s = {'\fontname{times}\fontsize{12}\it{This GUI is meant to help you to create new material}',...
    '{** YOU MUST ENTER A NAME FOR YOUR MATERIAL in the text box **}',...
    '',...
    '\rm{DROP DOWN MENUS help you to define the type of material properties:}',...
    '\it{Viscous:\rm constant, Frank Kamenetskii or Arrhenius law}',...
    '\it{Plastic:\rm set the plasticity type to none, mises or druker prager}',...
    '\it{Softening:\rm chose the type of law which relate current plastic properties to finite shear strain}',...
    '\it{Energy: \rm turn thermal dependance on or off for this phase}',...
    '\it{Density:\rm depends on nothing, on temperature (alpha), on pressure and temperature from thermodynamics}','',...
    '\rm{After choosing press CONTINUE, a list of the properties you need to define will appear in a POP UP window}'};

createmode.Interpreter='tex'
createmode.WindowStyle='nonmodal'
hhelp = msgbox(s,'How to assign properties to markers' ,'help',createmode);
set(hhelp,'Position',[412.8 364.8 500 300]);
end
