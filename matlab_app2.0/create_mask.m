function [MASK,type] = create_mask(Tana,X,Y,phase2D,name)
newmask = 'Continue';

figure(50);clf;
set(50,'Position',[0 0 400 300]);
pcolor(X,Y,phase2D);shading flat; axis equal tight; caxis([-2 max(phase2D(:))]); title('Your phase index');colorbar horiz;hold off
MASK = zeros(size(phase2D));

liste = {'following phase' 'graphically on the fig' 'circle' 'from isotherm','box','empty','not attributed','Help'};
operators = {'Union' 'Intersection' 'Union with complement' 'Intersection with complement' 'Cancel' 'Help'};


while strcmp(newmask,'Continue')
    
    figure(51); clf;set(51,'Position',[600 0 300 700]);
    help_h   = uicontrol(51,'Style','pushbutton',...
        'String','HELP',...
        'Position',[0 0 100 50],...
        'Callback',@helpme);
    
    subplot(311); pcolor(X,Y,double(MASK));shading interp; axis equal tight; colorbar; title('current MASK'); caxis([0,1]);
    type = menu ('What type of submask ?', liste); 
    flag = 1;
    switch type
        case find(strcmp('not attributed',liste))
            M = ~(phase2D == -1);
        case find(strcmp('box',liste))
            M = ones(size(phase2D));
        case find(strcmp('empty',liste))
            M = zeros(size(phase2D));
        case find(strcmp('following phase',liste))
            M = create_mask_from_phase(phase2D);
        case find(strcmp('graphically on the fig',liste))
            M = create_mask_from_figure(X,Y,(phase2D+1.*MASK)-1,100);
        case find(strcmp('circle',liste))
            M = create_circular_mask(X,Y);
        case find(strcmp('from isotherm',liste))
            M = create_mask_from_isotherm(X,Y,Tana,name);
        case find(strcmp('Help',liste))
            flag = 0;
            s = {'\fontname{times} \fontsize{12} \it Different type of submask can be created',...
                'these submask are meant to be assemble using logical operators to form a mask',...
                '*\it box                \rm selects the all domain',...
                '*\it empty              \rm selects nothing',...
                '*\it following phase    \rm selects all the part of the domain corresponding to a list of phase',...
                '*\it following isotherm \rm selects the area located between 2 isotherm of any of your temperature field. NB the temperature field used to delimitate the domain doesn''nt have to be the same as the temperature field affected to the final mask',...
                '*\it graphically        \rm let you click to define a convex shape containing all the points using qhull'...
                '*\it circle             \rm let you enter the center and radius of a circular mask'};
            createmode.Interpreter='tex'
            createmode.WindowStyle='replace'
            hhelp = msgbox(s,'Definining submasks' ,'help',createmode);
            set(hhelp,'Position',[412.8 364.8 500 300]);
            
            
    end
    if flag == 1  
    figure(51);subplot(312); pcolor(X,Y,double(M));shading flat; axis equal tight; caxis([0,1]);colorbar; title('New subMASK');

    happy = 'No';
    while strcmp(happy,'No')
        operator = menu('Which operator of mask',operators);
        switch operator
            case find(strcmp('Union',operators))
                M2 = MASK | M;
        figure(51);subplot(3,1,3,'replace');pcolor(X,Y,double(M2));shading flat; axis equal tight; colorbar; title('Updated MASK');
        happy = questdlg('Do you accept this modification?','ptatin2dMatlabApp','Yes','No','No');
            case find(strcmp('Intersection',operators))
                M2 = MASK & M;
             figure(51);subplot(3,1,3,'replace');pcolor(X,Y,double(M2));shading flat; axis equal tight; colorbar; title('Updated MASK');
        happy = questdlg('Do you accept this modification?','ptatin2dMatlabApp','Yes','No','No');
            case find(strcmp('Union with complement',operators))
                M2 = MASK | ~M;
             figure(51);subplot(3,1,3,'replace');pcolor(X,Y,double(M2));shading flat; axis equal tight; colorbar; title('Updated MASK');
        happy = questdlg('Do you accept this modification?','ptatin2dMatlabApp','Yes','No','No');
            case find(strcmp('Intersection with complement',operators))
                M2 = MASK & ~M;    
             figure(51);subplot(3,1,3,'replace');pcolor(X,Y,double(M2));shading flat; axis equal tight; colorbar; title('Updated MASK');
        happy = questdlg('Do you accept this modification?','ptatin2dMatlabApp','Yes','No','No');
            case find(strcmp('Cancel this submask',operators))
                M2 = MASK;
                happy = 'Yes';
            case find(strcmp('Help',operators))
                M2 = MASK;
                happy = 'No';
            s = {'\fontname{times} \fontsize{12}\it Operation on mask' ,...
                'The operations is meant to be of type A operator B',...
                'where A is the current mask at the top of fig 51',...
                'and B is the current submask in the middle of fig 51',...
                'the result is displayed on the bottom figure.',...
                '',...
                'In general the temperature of the masks are added one to each other if the masks are superposed',...
                'the not attributed button provides you with a mask that contains all the dof which are not yet assigned',...
                '',...                
                'If you made a mistake in the operator, just answer No',...
                'when the program ask you if you are happy with the result'
                'If you wanna restart from scratch, just take the intersection of your current mask with empty',...
                'and associate any of your temperature field to this empty mask',...
                '\fontsize{12}\bf',...
                'if everything goes wrong, finalize the mask and in the next dialog box'...
                'choose the option Undo last mask'}
            createmode.Interpreter='tex'
            createmode.WindowStyle='replace'
            hhelp = msgbox(s,'Logical Operators' ,'help',createmode);
            set(hhelp,'Position',[412.8 364.8 500 200]);

        end
    end
    MASK = M2;
    newmask = questdlg('Would you like to continue adding submask to this mask or finalise the mask? ','ptatin2dMatlabApp','Continue','Finalise','Finalise');
    else
     newmask = 'Continue';
    end
end
type   = menu('Which temperature field do you want to affect to this mask?',name);

figure(50);
set(50,'Position',[0 0 400 300]);
pcolor(X,Y,MASK.*Tana(:,:,type));shading flat; axis equal tight; colorbar; title('Temperature field in the mask');

end

function [M]=create_mask_from_phase(phase2D)
% phase2D is a 2D array which contains particule IDs
% phasein is a 1D array which particule index which must be included in the mask
options.Resize='on';
options.Window='normal';
phase_in = str2num(char(inputdlg({'Liste of particle IDs'},'Create subMask for particles ',1,{'1 3 5'},options)));
M = 0*phase2D;
for i = 1:length(phase_in)
    M = M+(phase2D == phase_in(i));
end
end

function [M]=create_mask_from_isotherm(X,Y,Tana,name)
% phase2D is a 2D array which contains particule IDs
% phasein is a 1D array which particule index which must be included in the mask
 
type   = menu('which geotherm do you want to use?',name);
T      = Tana(:,:,type);
figure(100);pcolor(X,Y,T); shading interp;colorbar;
prompt = {'min temperature' 'max temperature'};
options.Resize='on';
answer=inputdlg(prompt,'Temperature range',1,{'0','100'},options);
M = (T>=str2num(char(answer(1))) & T<=str2num(char(answer(2))));
close(100);
end

function [M]=create_mask_from_figure(X,Y,phase2D,numfig)
% phase2D is a 2D array which contains particule IDs
% phasein is a 1D array which particule index which must be included in the mask


figure(numfig);%clf;pcolor(X,Y,phase2D);shading interp; axis equal tight; caxis([-1,max(phase2D(:))]);colorbar;hold off
j=0;
addpoint='Yes';
while strcmp(addpoint,'Yes')
    j=j+1;
    figure(numfig);
    [xdomain(j),ydomain(j)] = ginput(1);
    figure(numfig);hold on ; plot(xdomain(j),ydomain(j),'+'); hold off;
    if j > 2 
    addpoint = questdlg('Add more points?','ptatin2dMatlabApp','Yes','No','No');
    end
end
%TRI = delaunay(xdomain,ydomain);
%M   = double(~isnan(tsearch(xdomain,ydomain,TRI,X,Y)));
         
dt = DelaunayTri(xdomain',ydomain');
qrypts = [X(:),Y(:)];
triids = pointLocation(dt, qrypts);
M   = reshape(double(~isnan(triids)),size(X));
figure(numfig);clf;close(numfig);
end


function [M]=create_circular_mask(X,Y)
M = X*0;
options.Resize='on';
options.Window='normal';
data=str2num(char(inputdlg({'Radius' 'Xcentre' 'Ycentre'},'Enter Circle Properies',1,{'1e5','1e5','1e5'},options)));
R    = data(1);
x    = data(2);
y    = data(3);
M= ((X-x).^2+(Y-y).^2-R^2) < 0;
end
function helpme(hObject,eventdata)
                 s = {'\fontname{times} \fontsize{12} \it This part helps you defining composite geotherms',...
                '\rm you first need to define mask in which you want you temperature field to apply',...
                'mask are created by assembling submask via logical operator',...
                'Initially, all the mask are created empty',...
                'At each operation: ',...
                '* the current mask is displayed at the top',...
                '* the middle image is your current submask',...
                '* the bottom image is the result after applying the operator of your choice','',...
                'you don''t have to accept the modification',...
                'Once you are happy with your mask, answer No to the question do you wanna add a submask',...
                'The program will prompt you for the thermal field you wanna apply within this mask','',...
                '\it NB: You should never have masks that super imposed one to each other',...
                'most of the function will insure that for you',...
                'We advise user to play a little bit with this part of the program to become familiar,'...
                'and to plan on paper their composite geotherms before doing it within app'};

createmode.Interpreter='tex'
createmode.WindowStyle='nonmodal'
hhelp = msgbox(s,'How to assign properties to markers' ,'help',createmode);
set(hhelp,'Position',[412.8 364.8 500 500]);
end


