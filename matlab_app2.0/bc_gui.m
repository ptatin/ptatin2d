function varargout = bc_gui(varargin)
%BC_GUI M-file for bc_gui.fig
%      BC_GUI, by itself, creates a new BC_GUI or raises the existing
%      singleton*.
%
%      H = BC_GUI returns the handle to a new BC_GUI or the handle to
%      the existing singleton*.
%
%      BC_GUI('Property','Value',...) creates a new BC_GUI using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to bc_gui_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      BC_GUI('CALLBACK') and BC_GUI('CALLBACK',hObject,...) call the
%      local function named CALLBACK in BC_GUI.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help bc_gui

% Last Modified by GUIDE v2.5 21-Nov-2012 11:33:37

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @bc_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @bc_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before bc_gui is made visible.
function bc_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)
BCtop          = varargin{1};
BCbot          = varargin{2};
BCleft         = varargin{3};
BCright        = varargin{4};
BCtype         = varargin{5}+1;
vx             = varargin{6};
% Choose default command line output for basic_gui
handles.output = hObject;
% Set default command line output from handles structure

if ~isempty(BCtop)
    set(handles.Ttop,'string',num2str(BCtop));
    set(handles.Ttop_type,'Value',1);
else
    set(handles.Ttop,'string',num2str(0));
    set(handles.Ttop_type,'Value',0);
end
if ~isempty(BCbot)
    set(handles.Tbot,'string',num2str(BCbot));
    set(handles.Tbot_type,'Value',1);
else
    set(handles.Tbot,'string',num2str(0));
    set(handles.Tbot_type,'Value',0);
end

if ~isempty(BCleft)
    set(handles.Tleft,'string',num2str(BCleft));
    set(handles.Ttop_type,'Value',1);
else
    set(handles.Tleft,'string',num2str(0));
    set(handles.Tleft_type,'Value',0);
end

if ~isempty(BCright)
    set(handles.Tright,'string',num2str(BCright));
    set(handles.Tright_type,'Value',1);
else
    set(handles.Tright,'string',num2str(0));
    set(handles.Tright_type,'Value',0);
end

set(handles.bctype,'Value',BCtype);
set(handles.vx,'string',num2str(vx));

% Update handles structure
guidata(hObject, handles);
uiwait(hObject);


% UIWAIT makes bc_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = bc_gui_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Get default command line output from handles structure

if get(handles.Ttop_type,'Value')
    BCtop        = str2num(get(handles.Ttop,'string'));
else
    BCtop = [];
end

if get(handles.Tbot_type,'Value')
    BCbot        = str2num(get(handles.Tbot,'string'));
else
    BCbot = [];
end

if get(handles.Tleft_type,'Value')
    BCleft        = str2num(get(handles.Tleft,'string'));
else
    BCleft = [];
end

if get(handles.Tright_type,'Value')
    BCright        = str2num(get(handles.Tright,'string'));
else
    BCright = [];
end

BCtype         = get(handles.bctype,'Value')-1;
vx             = str2num(get(handles.vx,'string'));

varargout = {BCtop,BCbot,BCleft,BCright,BCtype,vx};
% Get default command line output from handles structure



% --- Executes on button press in Ttop_type.
function Ttop_type_Callback(hObject, eventdata, handles)
% hObject    handle to Ttop_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Ttop_type


% --- Executes on button press in Tbot_type.
function Tbot_type_Callback(hObject, eventdata, handles)
% hObject    handle to Tbot_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Tbot_type


% --- Executes on button press in Tleft_type.
function Tleft_type_Callback(hObject, eventdata, handles)
% hObject    handle to Tleft_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Tleft_type


% --- Executes on button press in Tright_type.
function Tright_type_Callback(hObject, eventdata, handles)
% hObject    handle to Tright_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Tright_type



function Ttop_Callback(hObject, eventdata, handles)
% hObject    handle to Ttop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Ttop as text
%        str2double(get(hObject,'String')) returns contents of Ttop as a double


% --- Executes during object creation, after setting all properties.
function Ttop_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Ttop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Tbot_Callback(hObject, eventdata, handles)
% hObject    handle to Tbot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Tbot as text
%        str2double(get(hObject,'String')) returns contents of Tbot as a double


% --- Executes during object creation, after setting all properties.
function Tbot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Tbot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Tleft_Callback(hObject, eventdata, handles)
% hObject    handle to Tleft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Tleft as text
%        str2double(get(hObject,'String')) returns contents of Tleft as a double


% --- Executes during object creation, after setting all properties.
function Tleft_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Tleft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Tright_Callback(hObject, eventdata, handles)
% hObject    handle to Tright (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Tright as text
%        str2double(get(hObject,'String')) returns contents of Tright as a double


% --- Executes during object creation, after setting all properties.
function Tright_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Tright (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in bctype.
function bctype_Callback(hObject, eventdata, handles)
% hObject    handle to bctype (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns bctype contents as cell array
%        contents{get(hObject,'Value')} returns selected item from bctype


% --- Executes during object creation, after setting all properties.
function bctype_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bctype (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function vx_Callback(hObject, eventdata, handles)
% hObject    handle to vx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of vx as text
%        str2double(get(hObject,'String')) returns contents of vx as a double


% --- Executes during object creation, after setting all properties.
function vx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to vx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function pressureWinkler_Callback(hObject, eventdata, handles)
% hObject    handle to pressureWinkler (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pressureWinkler as text
%        str2double(get(hObject,'String')) returns contents of pressureWinkler as a double


% --- Executes during object creation, after setting all properties.
function pressureWinkler_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pressureWinkler (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in end_gui.
function end_gui_Callback(hObject, eventdata, handles)
% hObject    handle to end_gui (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiresume(handles.output)


% --- Executes when bcgui is resized.
function bcgui_ResizeFcn(hObject, eventdata, handles)
% hObject    handle to bcgui (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
