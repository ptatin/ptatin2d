clear;
clf;close all;
path = [];
path = '/home/';
[filename,EnergySolver,isostatic_topo] = initialise(path)
pixelmap_apply2(filename)

if EnergySolver
    err = Energy_apply(filename);
    if err 
       error('you have not define your geotherm, something went wrong exiting');
    end
end

if isostatic_topo
    P=initial_topo_apply2(filename)
end

solver(filename);

write_option_file2(filename);
close('all');
