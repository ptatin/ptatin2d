function P = initial_topo_apply2(filename)
load([filename,'_basic.mat']);
load([filename,'_phase.mat']);
load([filename,'_properties.mat']);
rho2D           = rho(phase2D+1);
if EnergySolver
    load([filename,'_temperature.mat']);
    alpha2D     = therm_exp(phase2D+1);
    rho2D       = (1-alpha2D.*Anew).*rho2D;
end

[P,h,Length(2),deltapixmax,ref,nbpix]=initial_topo_prepare(Length,Origine,phase2D,rho2D);
[phase2D,X,Y] = shift(double(phase2D),deltapixmax,nbpix,Origine,Length);
figure(51); clf;pcolor(X,Y,phase2D);shading flat; hold on; plot(X(1,:),h,'w','LineWidth',1); axis equal tight;
title('you material index with initial topo');
phase2D = uint8(phase2D);
tmap_write(h,filename2,Origine,Length);
pmap_write(phase2D,filename2,Origine,Length);
if EnergySolver
    load([filename,'_temperature.mat'],'Anew');
    [Anewdef,X,Y] = shift(Anew,deltapixmax,nbpix,Origine,Length);
    figure(52); clf;pcolor(X,Y,Anewdef);shading interp; hold on; plot(X(1,:),h,'w','LineWidth',1);axis equal tight;
    title('your geotherm with initial topo'); colorbar;
    fname = [filename2,'.tempmap'];
    tempmap_write(Anew,fname,Origine,Length);
    
    if Production
        [Prodnewdef,X,Y] = shift(Prodnew,deltapixmax,nbpix,Origine,Length);
        fname = [filename2,'.prodmap'];
        tempmap_write(Prodnew,fname,Origine,Length);
    end
end
flag = true;
end
function[P_bc,hs,Ly,deltapixmax,ref,nbpix]=initial_topo_prepare(Length,Origine,phase2D,rho2D,Y,X)
ref = find(all((double(phase2D(:,1))*ones(1,size(phase2D,2)) == phase2D)'), 1, 'last' );
l= (Length(2)-Origine(2));
dy          = l/size(phase2D,1);
P_c         = sum(rho2D(ref:end,:))*dy;
P_bc        = sum(rho2D(:,1))*dy;

h           = (P_c(1)-P_c)./rho2D(ref,:);
deltapixmax = ceil(max(h)/dy);
Ly = Length(2)+deltapixmax*dy;

L= (Length(1)-Origine(1));
x=linspace(0,L,length(h));
dx = x(2)-x(1);
Lc =50e3;
n = floor(Lc/dx);
h(1:10)=h(10);
h(end-10:end)=h(end-10);
hs = smooth(h,n)+Length(2);
figure(100);plot(x,h+Length(2),'+',x,hs,'Linewidth',2);
nbpix = ceil(hs/dy);
end

function [DATA2,X2,Y2]=shift(DATA,deltapixmax,nbpix,Origine,Length)

[m,n] = size(DATA);
DATA2=[DATA;ones(deltapixmax,1)*DATA(end,:)];
y=linspace(Origine(2),Length(2),size(DATA2,1));
x=linspace(Origine(1),Length(1),size(DATA2,2));
[X2,Y2] = meshgrid(x,y);
ref = 1;
for i=1:n
    if nbpix(i)<=0;
        DATA2(ref:m+nbpix(i),i)=DATA(-nbpix(i)+ref:m,i);
    else
        
        DATA2(nbpix(i)+ref:m+nbpix(i),i)=DATA(ref:end,i);
    end
end

end

function tmap_write(X2,filename,Origine,Length)

[nx,nz]=size(X2);
fid2 = fopen([filename,'.tmap'],'w');
fprintf(fid2,'number vary first in x and than z \n');
fprintf(fid2,'%d\n', nx);
fprintf(fid2,'%d\n' ,nz);
fprintf(fid2,'%1.8e %1.8e %1.8e %1.8e\n',Origine(1),Origine(3),Length(1),Length(3));
fprintf(fid2,'%d ',X2');
fclose(fid2);
end