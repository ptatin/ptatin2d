function write_option_file2(filename)
load('options_definitions.mat');   
load([filename,'_basic.mat']);   
load([filename,'_properties.mat']);
load([filename,'_phase.mat']);
%load([filename,'_phase.mat']);
load([filename,'solver.mat']);
model  = 'GENE';
prefix = ['-',model,'_'];
fid3 = fopen([filename2,'.opts'],'w');
fprintf(fid3,'#####################################\n');
fprintf(fid3,'#############Model Parameters###############\n');
fprintf(fid3,'#####################################\n');
s1 = [prefix,'rheol'];
fprintf(fid3,'%s %d\n',s1,rheology_type);
fprintf(fid3,'%s %s\n','-ptatin_model',model);
fprintf(fid3,'%s %s\n','-output_path',outputpath);
fprintf(fid3,'%s %d\n','-nsteps',nsteps);
fprintf(fid3,'%s %d\n','-output_frequency',output_frequency);
fprintf(fid3,'%s %s\n','-map_file',filename2);
if isostatic_topo
    fprintf(fid3,'%s %s\n','-topo_file',filename2);
end
if EnergySolver
    fprintf(fid3,'%s %s\n','-temp_file',filename2);
    fprintf(fid3,'%s\n','-energy_solver_active');
    if Production 
      fprintf(fid3,'%s %s\n','-prod_file',filename2);
    end
end
fprintf(fid3,'%s %d\n','-mx',nel(1));
fprintf(fid3,'%s %d\n','-my',nel(2));
fprintf(fid3,'%s %d\n','-mz',nel(3));

fprintf(fid3,'%s %d\n','-Lx',Length(1));
fprintf(fid3,'%s %d\n','-Ly',Length(2));
fprintf(fid3,'%s %d\n','-Lz',Length(3));

fprintf(fid3,'%s %d\n','-Ox',Origine(1));
fprintf(fid3,'%s %d\n','-Oy',Origine(2));
fprintf(fid3,'%s %d\n','-Oz',Origine(3));
fprintf(fid3,'%s %d\n','-grav',gravity);

fprintf(fid3,'######## population control#################\n');

fprintf(fid3,'%s %d\n','-lattice_layout_Nx', lattice_layout_Nx);
fprintf(fid3,'%s %d\n','-lattice_layout_Ny', lattice_layout_Ny);
fprintf(fid3,'%s %d\n','-mp_popctrl_nxp', mp_popctrl_nxp);
fprintf(fid3,'%s %d\n','-mp_popctrl_nyp', mp_popctrl_nyp);
fprintf(fid3,'%s %d\n','-mp_popctrl_np_lower', mp_popctrl_np_lower);

fprintf(fid3,'######## timestep control#################\n');
fprintf(fid3,'%s %e\n','-courant_surf', surface_courant_factor);
fprintf(fid3,'%s %e\n','-dt_max',dt_max);% 1.5e-2
fprintf(fid3,'%s %e\n','-time_max',time_max);% 3e-2
fprintf(fid3,'%s %e\n','-dt_min',dt_min);
fprintf(fid3,'%s %e\n','-max_disp_y',max_surf_disp);
fprintf(fid3,'%s %e\n','-courant_factor', courant_factor);
if cordupdate
 fprintf(fid3,'-use_quasi_newton_coordinate_update\n');
end
fprintf(fid3,'######## scaling parameters#################\n');
fprintf(fid3,'-use_dimensional_units\n');
fprintf(fid3,'%s %e\n','-stokes_scale_length', Length_scaling);
fprintf(fid3,'%s %e\n','-stokes_scale_viscosity',Vis_scaling);
fprintf(fid3,'%s %e\n','-stokes_scale_velocity',Vel_scaling);
fprintf(fid3,'%s %e\n','-energy_scale_temperature',Temp_scaling);
fprintf(fid3,'%s %d\n','-coefficient_average_type',vis_average_type);
fprintf(fid3,'%s %d\n','-coefficient_projection_type', coefficient_projection_type);
if (rheology_type > 0 )
    fprintf(fid3,'######## viscous cutoff#################\n');
    s1 = [prefix,'eta_min_cut_off'];
    fprintf(fid3,'%s %e\n',s1,eta_min_cut_off);
    s1 = [prefix,'eta_max_cut_off'];
    fprintf(fid3,'%s %e\n',s1,eta_max_cut_off);
end

fprintf(fid3,'#############Boundary Conditions###############\n');

fprintf(fid3,'#############velocity###############\n');
fprintf(fid3,'%s %d\n','-bc_type',bctype);
fprintf(fid3,'%s %e\n','-vx',vx);
if EnergySolver
    fprintf(fid3,'#############temperature###############\n');
    if ~isempty(Ttop); fprintf(fid3,'%s %f\n','-Ttop',Ttop);       end;
    if ~isempty(Tbot); fprintf(fid3,'%s %f\n','-Tbot',Tbot);       end;
    if ~isempty(Tright); fprintf(fid3,'%s %f\n','-Tright',Tright); end;
    if ~isempty(Tleft); fprintf(fid3,'%s %f\n','-Tleft',Tleft);    end;
end
prefix = '-GENE_'
s1 = [prefix,'nphase'];
fprintf(fid3,'%s %d\n',s1,nphase);
for i_phase = 0:nphase-1
    fprintf(fid3,'#### param for phase indexed %d behaving as %s #####\n',i_phase,char(entry(LayerPhase(i_phase+1))));
    litho = LayerPhase(i_phase+1) 
    for i_opt = 1:length(litho_type{litho});
        s = [prefix,char(rheol_opt{i_opt}),'_',num2str(i_phase)];
        fprintf(fid3,['%s %d \n'],s,litho_type{litho}(i_opt)-1);
    end
    for i_opt = 1:length(options{litho})
        s = [prefix,char(options{litho}{i_opt}),'_',num2str(i_phase)];
        fprintf(fid3,['%s %e \n'],s,str2num(data{litho}{i_opt}));
    end
    fprintf(fid3,'#####################################\n');
end
fprintf(fid3,'########################################\n');
fprintf(fid3,'#Numerics/solver happen below this line#\n');
fprintf(fid3,'########################################\n');    
fprintf(fid3,'#continuation: %s\n',withic);
fprintf(fid3,'#snes type :%s \n',withsnes);
fprintf(fid3,'#multigrid :%s \n',withmg);
fprintf(fid3,'#can run in parallel :%s \n',isparallel);
fprintf(fid3,'########################################\n');
  
for i = 1:length(extra)
    fprintf(fid3,[char(extra{i}),'\n']);
end

for k=1:length(snes_type)
    fprintf(fid3,'##################################\n');
    n=length(snes_options);
    if strcmp(snes_values{k}{n},'snes_relaxed')
        n=n-1;
    end
    for i = 1:n
        fprintf(fid3,[snes_type{k},snes_options{i},' ',snes_values{k}{i},'\n']);
    end
    fprintf(fid3,'#-----------------------------------\n');
    for i = 1:length(monitoring{k})
        fprintf(fid3,[snes_type{k},monitoring{k}{i},'\n']);
    end
    fprintf(fid3,'#-----------------------------------\n');
    for i = 1:length(pc_options)
        fprintf(fid3,[snes_type{k},pc_options{i},' ',pc_values{i},'\n']);
    end
    
    if Mglevel > 1
         
        fprintf(fid3,[snes_type{k},'dau_nlevels',' ',num2str(Mglevel),'\n']);
        fprintf(fid3,[snes_type{k},pc_prefix,'_pc_type mg \n']);
        fprintf(fid3,'#------------coarse grid-------------\n');
        for i = 1:length(pc_options_coarse)
            fprintf(fid3,[snes_type{k},pc_prefix,pc_coarse,pc_options_coarse{i},' ',pc_valuescoarse{i},'\n']);
        end
        fprintf(fid3,'#-------------other levels------------\n');  
        
            for i = 1: length(pc_MG_options)
                fprintf(fid3,[snes_type{k},pc_prefix,pc_MG,pc_MG_options{i},' ',pc_valuesMG{i},'\n']);
            end
        
        
    else
        for i = 1:length(pc_options_reg)
            fprintf(fid3,[snes_type{k},pc_prefix,pc_options_reg{i},' ',pc_valuesreg{i},'\n']);
        end
    end
    fprintf(fid3,'#-----------------------------------\n');
    for i=1:length(ksp_options)
        fprintf(fid3,[snes_type{k},ksp_options{i},' ',ksp_values{k}{i},'\n']);
    end
    fprintf(fid3,'##################################\n');
end
fprintf(fid3,'-snes_fieldsplit_p_pc_type mat \n');
fprintf(fid3,'-snes_stokes_pc_auglag \n');
fprintf(fid3,'-snes_stokes_pc_auglag_penalty 1.0e-6 \n');
fclose(fid3);