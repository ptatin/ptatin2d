function [pc,snes,Mstep,MgLevel,MaxSmoothIt] = solvergui(Dpc,Dsnes,DMstep,DMgLevel,DMaxSmoothIt)
fh = figure(110);
clf(fh);
width  = 400;
height = 300;
w = width; 
set(fh,'units','points', 'Position',[200 100 width height]);
yloc = height+100;
yloc = yloc-20;
text  = uicontrol(fh,'Style','text',...
    'String',{'General properties for the solver '},...
    'Value',1,'Position',[20 yloc width  14]);


pc_string = {'iterative','direct (umfpack)','direct parallel (superlu)','Multigrid Redundant (umfpack)','Multigrid parallel (superlu)'};
snes_string   = {'linear','Picard','Newton'};

yloc = yloc-30;
text      = uicontrol(fh,'Style','text',...
    'String',{'Chose your preconditioner for the AA block'},...
    'Value',0,'Position',[20 yloc w 14]);
yloc = yloc-20;

pctype  = uicontrol(fh,'Style','popupmenu',...
    'String',pc_string,...
    'Value',Dpc,'Position',[20 yloc w 14]);

yloc = yloc-30;
text      = uicontrol(fh,'Style','text',...
    'String',{'Chose your non linear scheme'},...
    'Value',0,'Position',[20 yloc w 14]);
yloc = yloc-20;

snestype  = uicontrol(fh,'Style','popupmenu',...
    'String',snes_string,...
    'Value',Dsnes,'Position',[20 yloc w 14]);

yloc = yloc-30;
text  = uicontrol(fh,'Style','text',...
    'String',{'number of continuation steps on viscosity (put 0 to skip continuation)'},...
    'Value',0,'Position',[20 yloc w  14]);
yloc = yloc-30;
continuation = uicontrol(fh,'Style','edit',...
    'String',DMstep,...
    'Position',[20 yloc w 25]);

yloc = yloc-30;
text  = uicontrol(fh,'Style','text',...
    'String',{'number of decomposition level for MG (only apply if MG is chosen'},...
    'Value',0,'Position',[20 yloc w  14]);
yloc = yloc-30;
Mglevel = uicontrol(fh,'Style','edit',...
    'String',DMgLevel,...
    'Position',[20 yloc w 25]);
yloc = yloc-30;

text  = uicontrol(fh,'Style','text',...
    'String',{'Maximum smoother iteration per MG levels(only apply if MG is chosen'},...
    'Value',0,'Position',[20 yloc w  14]);
yloc = yloc-30;
maxS_it = uicontrol(fh,'Style','edit',...
    'String',DMaxSmoothIt,...
    'Position',[20 yloc w 25]);



snes_save   = uicontrol('Style','pushbutton','String','Save these parameters','Position',[20 50 150 50],...
   'Callback','uiresume(gcbf)');            
waitforbuttonpress;
uiwait(gcf);


pc      = get(pctype,'Value');
snes    = get(snestype,'Value');
Mstep   = str2num(get(continuation,'string'));
MgLevel = str2num(get(Mglevel,'string'));
MaxSmoothIt=str2num(get(maxS_it,'string'));
close(fh);
end