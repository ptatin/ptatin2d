/*

 Input / command line parameters:
 -Ttop
 -Tbot
 -mx
 -my
 -vx -3.000000e-10
 -bc_type 0
 -output_frequency 1
 -output_path afonso
 -remesh_type 2
 -stokes_scale_length 1.000000e+04
 -stokes_scale_velocity 1.000000e-10
 -stokes_scale_viscosity 1.000000e+27
 -dt_max 3e11
 -nsteps 500
 -time_max 5.000000e+15
 -output_frequency 20

 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h"
#include "pTatin2d.h"

#define WRITE_VP
#define WRITE_TFIELD
#define WRITE_MPOINTS
//#define WRITE_QPOINTS
#define WRITE_TCELL
//#define WRITE_INT_FIELDS
//#define WRITE_FACEQPOINTS


#undef __FUNCT__
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_afonso"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_afonso(pTatinCtx ctx)
{
  PhaseMap phasemap;
  TopoMap topomap;
  PetscInt M,N;
  PetscBool flg;
  PetscBool found;
  PetscErrorCode ierr;
  PetscReal Ox,Oy,Lx,Ly,gravity_acceleration;
  char name[PETSC_MAX_PATH_LEN];
  char map_file[PETSC_MAX_PATH_LEN];
  char topo_file[PETSC_MAX_PATH_LEN];
  pTatinUnits *units;

  PetscFunctionBegin;
  units   = &ctx->units;
  gravity_acceleration = 10;
  Ox = 0.0;
  Oy = -200.e3;
  Lx = 600.e3;
  Ly = 0.0;

  /* make a regular non dimensional grid from option file parameters*/
  ierr  = PetscOptionsGetReal(PETSC_NULL,"-grav",&gravity_acceleration,&found);CHKERRQ(ierr);
  ierr = QuadratureStokesGravityModelCoordinateAlignedSetValues(ctx->Q,0.0, -gravity_acceleration);CHKERRQ(ierr);

  ierr  = PetscOptionsGetReal(PETSC_NULL,"-Ox",&Ox,&found);CHKERRQ(ierr);
  ierr  = PetscOptionsGetReal(PETSC_NULL,"-Oy",&Oy,&found);CHKERRQ(ierr);
  ierr  = PetscOptionsGetReal(PETSC_NULL,"-Lx",&Lx,&found);CHKERRQ(ierr);
  ierr  = PetscOptionsGetReal(PETSC_NULL,"-Ly",&Ly,&found);CHKERRQ(ierr);

  PetscPrintf(PETSC_COMM_WORLD,"ApplyInitialMeshGeometry: options Domain_SI = [ %1.4e , %1.4e ] x [ %1.4e , %1.4e ] \n", Ox, Lx,Oy,Ly );
  UnitsApplyInverseScaling(units->si_length,Ox,&Ox);
  UnitsApplyInverseScaling(units->si_length,Oy,&Oy);
  UnitsApplyInverseScaling(units->si_length,Lx,&Lx);
  UnitsApplyInverseScaling(units->si_length,Ly,&Ly);

  PetscPrintf(PETSC_COMM_WORLD,"ApplyInitialMeshGeometry : options Domain_ND = [ %1.4e , %1.4e ] x [ %1.4e , %1.4e ] \n", Ox, Lx,Oy,Ly );
  ierr = DMDASetUniformCoordinates(ctx->dav,Ox,Lx,Oy,Ly, 0.0,0.0);CHKERRQ(ierr);

    {
    PetscBool active = PETSC_FALSE;
    ierr = pTatinPhysCompActivated(ctx,PhysComp_Energy,&active);CHKERRQ(ierr);

    if (active) {
      DM da;
      Vec T;
      PhysCompEnergyCtx phys_energy;
      TempMap  map;
      char file[PETSC_MAX_PATH_LEN];
      char prodmapname[PETSC_MAX_PATH_LEN];
      double T0;

      phys_energy = ctx->phys_energy;
      da          = phys_energy->daT;
      T           = phys_energy->T;

      ierr = DMDAProjectCoordinatesQ2toQ1_2d(ctx->dav,da);CHKERRQ(ierr);
      }
    }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatin2d_ModelSetMaterialProperty_afonso"
PetscErrorCode pTatin2d_ModelSetMaterialProperty_afonso(pTatinCtx ctx)

{
  RheologyConstants      *rheology;
  PetscInt               nphase, i,rheol_type;
  PetscErrorCode         ierr;
  char                   *option_name;
  PetscBool              found;
  int                    model_type;

  PetscFunctionBegin;

  rheol_type = 6; // Rheology_VPT_STD
  model_type=1; // Using Sascha Brune "The rift to break-up evolution of the Gulf of Aden: Insights from 3D numerical lithospheric-scale modelling"
//  model_type=2; // Similar to 1, but with a 1km sedimentary layer on top
 ierr       = PetscOptionsGetInt(PETSC_NULL,"-rheol_model_type",&model_type,0);CHKERRQ(ierr);
  ctx->rheology_constants.rheology_type = rheol_type;
  rheology   = &ctx->rheology_constants;

  rheology->eta_upper_cutoff_global = 1.e25;
  rheology->eta_lower_cutoff_global = 1.e19;
  ierr       = PetscOptionsGetReal(PETSC_NULL,"-eta_max_cut_off",&rheology->eta_upper_cutoff_global,0);CHKERRQ(ierr);
  ierr       = PetscOptionsGetReal(PETSC_NULL,"-eta_min_cut_off",&rheology->eta_lower_cutoff_global,0);CHKERRQ(ierr);

  switch (model_type) {
    case 0: //Similar to 1, but with sedimentary layer
    printf( "watremez G3 like, sediments are phase 4 .\n" );  
    nphase = 5;
    rheology->temp_kappa[0] = 1e-6;  // diffusivity
    rheology->temp_kappa[1] = 1e-6;
    rheology->temp_kappa[2] = 1e-6;
    rheology->temp_kappa[3] = 1e-6;
    rheology->temp_kappa[4] = 1e-6;

    rheology->density_type[0] = 1; // See model_GENE.c
    rheology->density_type[1] = 1;
    rheology->density_type[2] = 1;
    rheology->density_type[3] = 1;
    rheology->density_type[4] = 1;

    rheology->const_rho0[0] = 2700; // layer material density at T=273K
    rheology->const_rho0[1] = 2900;
    rheology->const_rho0[2] = 3330;
    rheology->const_rho0[3] = 3360;
    rheology->const_rho0[4] = 2500; // Limestone

    rheology->temp_alpha[0] = 2.7e-5; // Linear thermal expansion coefficient
    rheology->temp_alpha[1] = 2.7e-5; // alpha = 1/rho.(d rho/d Temperature) ???
    rheology->temp_alpha[2] = 3.e-5;
    rheology->temp_alpha[3] = 3.e-5;
    rheology->temp_alpha[4] = 2.7e-5;

    rheology->temp_beta[0] = 1e-11; // Volumetric thermal expansion coefficient ???
    rheology->temp_beta[1] = 1e-11; // Compressibility ??? Beta=1/rho.(d rho/d Pressure) ???
    rheology->temp_beta[2] = 1e-11;
    rheology->temp_beta[3] = 1e-11;
    rheology->temp_beta[4] = 1e-11;

    rheology->viscous_type[0] = 2; // See model_GENE.c
    rheology->viscous_type[1] = 2;
    rheology->viscous_type[2] = 2;
    rheology->viscous_type[3] = 2;
    rheology->viscous_type[4] = 2;

    rheology->arrh_nexp[0]    = 1.9; // Arrhenius parameter????
    rheology->arrh_nexp[1]    = 3.1; // Power law for dislocation creep ???
    rheology->arrh_nexp[2]    = 3.0;
    rheology->arrh_nexp[3]    = 3.0;
    rheology->arrh_nexp[4]    = 1.9;

// Activation energy
    rheology->arrh_entalpy[0] = 137.e3; // wet Quartzite
    rheology->arrh_entalpy[1] = 243.e3; // pikwitonian granulite
    rheology->arrh_entalpy[2] = 520.e3; // dry olivine
    rheology->arrh_entalpy[3] = 520.e3; // wet olivine
    rheology->arrh_entalpy[4] = 137.e3; // calcite

    rheology->arrh_preexpA[0] = 2.e-4; // Arrhenius parameter????
    rheology->arrh_preexpA[1] = 8.e-3;
    rheology->arrh_preexpA[2] = 1.e4;
    rheology->arrh_preexpA[3] = 1.e4;
    rheology->arrh_preexpA[4] = 2.e-4;

    rheology->arrh_Ascale[0]  =1.e6; // Scaling factor A for phase
    rheology->arrh_Ascale[1]  =1.e6; // allows to convert from MPa to Pa
    rheology->arrh_Ascale[2]  =1.e6;
    rheology->arrh_Ascale[3]  =1.e6;
    rheology->arrh_Ascale[4]  =1.e6;

    rheology->arrh_Vmol[0]    =0.0; // Activation volume
    rheology->arrh_Vmol[1]    =0.0; // Generally only used for very large depths, where viscosity starts to increase with depth
    rheology->arrh_Vmol[2]    =0.0;
    rheology->arrh_Vmol[3]    =0.0;
    rheology->arrh_Vmol[4]    =0.0;

    rheology->arrh_Tref[0]    =273; // Arrhenius reference temperature
    rheology->arrh_Tref[1]    =273;
    rheology->arrh_Tref[2]    =273;
    rheology->arrh_Tref[3]    =273;
    rheology->arrh_Tref[4]    =273;

    rheology->plastic_type[0]=2; // See model_GENE.c
    rheology->plastic_type[1]=2;
    rheology->plastic_type[2]=2;
    rheology->plastic_type[3]=2;
    rheology->plastic_type[4]=2;

    rheology->mises_tau_yield[0]=2.e7; // cohesion 
    rheology->mises_tau_yield[1]=2.e7; // cohesion
    rheology->mises_tau_yield[2]=3.e8; // cohesion
    rheology->mises_tau_yield[3]=3.e8; // cohesion
    rheology->mises_tau_yield[4]=2.e6; // cohesion

    rheology->dp_pressure_dependance[0] = 30.; // Angle for MISSES behaviour
    rheology->dp_pressure_dependance[1] = 30.; // using Huismans numbers SP282 Fig3 Top Right
    rheology->dp_pressure_dependance[2] = 2.0;
    rheology->dp_pressure_dependance[3] = 2.0;
    rheology->dp_pressure_dependance[4] = 10.0;

    rheology->tens_cutoff[0] = 1.e6; // tau cutoff MIN
    rheology->tens_cutoff[1] = 1.e6; // Higher pressures mean that it has entered the viscous slip region
    rheology->tens_cutoff[2] = 1.e8; // ruptures here are shear ruptures, or faults
    rheology->tens_cutoff[3] = 1.e8;
    rheology->tens_cutoff[4] = 1.e6;

    rheology->Hst_cutoff[0]  = 3.e8; // tau cutoff MAX
    rheology->Hst_cutoff[1]  = 3.e8; // Higher pressures mean that it has entered MISSES region, or brittle behaviour
    rheology->Hst_cutoff[2]  = 3.e8; // ruptures in these region are ruptures from compression
    rheology->Hst_cutoff[3]  = 3.e8;
    rheology->Hst_cutoff[4]  = 3.e8;

    rheology->softening_type[0]=1; // ???
    rheology->softening_type[1]=1;
    rheology->softening_type[2]=1;
    rheology->softening_type[3]=1;
    rheology->softening_type[4]=1;

    rheology->soft_Co_inf[0] = 2.e7; // Cohesion 
    rheology->soft_Co_inf[1] = 2.e7;
    rheology->soft_Co_inf[2] = 3.e8;
    rheology->soft_Co_inf[3] = 3.e8;
    rheology->soft_Co_inf[4] = 1.e6;

    rheology->soft_phi_inf[0] = 10.; // min angle of internal friction
    rheology->soft_phi_inf[1] = 10.; // SP282
    rheology->soft_phi_inf[2] = 0.0;
    rheology->soft_phi_inf[3] = 0.0;
    rheology->soft_phi_inf[4] = 5.0;

    rheology->soft_min_strain_cutoff[0] = 0.0;
    rheology->soft_min_strain_cutoff[1] = 0.0;
    rheology->soft_min_strain_cutoff[2] = 0.0;
    rheology->soft_min_strain_cutoff[3] = 0.0;
    rheology->soft_min_strain_cutoff[4] = 0.0;

    rheology->soft_max_strain_cutoff[0] = 1e-1;
    rheology->soft_max_strain_cutoff[1] = 1e-1;
    rheology->soft_max_strain_cutoff[2] = 1.0;
    rheology->soft_max_strain_cutoff[3] = 1.0;
    rheology->soft_max_strain_cutoff[4] = 1e-1;
    break;

  case 1: //Sascha Brune "The rift to break-up evolution of the Gulf of Aden: Insights from 3D numerical lithospheric-scale modelling"
    printf( "Model: Sascha Brune 2013: \"The rift to break-up evolution of the Gulf of Aden: Insights from 3D numerical lithospheric-scale modelling\".\n" ); 
    nphase = 4;
    rheology->temp_kappa[0] = 1e-6;  // diffusivity
    rheology->temp_kappa[1] = 1e-6;
    rheology->temp_kappa[2] = 1e-6;
    rheology->temp_kappa[3] = 1e-6;

    rheology->density_type[0] = 1; // See model_GENE.c
    rheology->density_type[1] = 1;
    rheology->density_type[2] = 1;
    rheology->density_type[3] = 1;

    rheology->const_rho0[0] = 2700; // layer material density at T=273K
    rheology->const_rho0[1] = 2850;
    rheology->const_rho0[2] = 3300;
    rheology->const_rho0[3] = 3300;

    rheology->temp_alpha[0] = 2.7e-5; // Linear thermal expansion coefficient
    rheology->temp_alpha[1] = 2.7e-5; // alpha = 1/rho.(d rho/d Temperature) ???
    rheology->temp_alpha[2] = 3.e-5;
    rheology->temp_alpha[3] = 3.e-5;

    rheology->temp_beta[0] = 2e-12; // Volumetric thermal expansion coefficient ???
    rheology->temp_beta[1] = 2e-12; // Compressibility ??? Beta=1/rho.(d rho/d Pressure) ???
    rheology->temp_beta[2] = 2e-12;
    rheology->temp_beta[3] = 2e-12;

    rheology->viscous_type[0] = 2; // See model_GENE.c
    rheology->viscous_type[1] = 2;
    rheology->viscous_type[2] = 2;
    rheology->viscous_type[3] = 2;

    rheology->arrh_nexp[0]    = 3; // Arrhenius parameter????
    rheology->arrh_nexp[1]    = 3; // Power law for dislocation creep ???
    rheology->arrh_nexp[2]    = 3;
    rheology->arrh_nexp[3]    = 3;

    rheology->arrh_entalpy[0] = 223.e3; // wet Quartzite
    rheology->arrh_entalpy[1] = 445.e3; // pikwitonian granulite
    rheology->arrh_entalpy[2] = 530.e3; // dry olivine
    rheology->arrh_entalpy[3] = 480.e3; // wet olivine

    rheology->arrh_preexpA[0] = 1.e-6; // Arrhenius parameter????
    rheology->arrh_preexpA[1] = 190.;
    rheology->arrh_preexpA[2] = 190.;
    rheology->arrh_preexpA[3] = 190.;

    rheology->arrh_Ascale[0]  =1.e6; // Scaling factor A for phase
    rheology->arrh_Ascale[1]  =1.e6; // allows to convert from MPa to Pa
    rheology->arrh_Ascale[2]  =1.e6;
    rheology->arrh_Ascale[3]  =1.e6;

    rheology->arrh_Vmol[0]    =0.0; // Activation volume
    rheology->arrh_Vmol[1]    =0.0; // Generally only used for very large depths, where viscosity starts to increase with depth
    rheology->arrh_Vmol[2]    =0.0;
    rheology->arrh_Vmol[3]    =0.0;

    rheology->arrh_Tref[0]    =273; // Arrhenius reference temperature
    rheology->arrh_Tref[1]    =273;
    rheology->arrh_Tref[2]    =273;
    rheology->arrh_Tref[3]    =273;

    rheology->plastic_type[0]=2; // See model_GENE.c
    rheology->plastic_type[1]=2;
    rheology->plastic_type[2]=2;
    rheology->plastic_type[3]=2;

    rheology->mises_tau_yield[0]=2.e7; // cohesion 
    rheology->mises_tau_yield[1]=3.e7; // cohesion
    rheology->mises_tau_yield[2]=3.e8; // cohesion
    rheology->mises_tau_yield[3]=3.e8; // cohesion

    rheology->dp_pressure_dependance[0] = 7.; // Angle for MISSES behaviour
    rheology->dp_pressure_dependance[1] = 7.; // using Huismans numbers SP282 Fig3 Top Right
    rheology->dp_pressure_dependance[2] = 0.0;
    rheology->dp_pressure_dependance[3] = 0.0;

    rheology->tens_cutoff[0] = 1.e7; // tau cutoff MIN
    rheology->tens_cutoff[1] = 1.e7; // Higher pressures mean that it has entered the viscous slip region
    rheology->tens_cutoff[2] = 1.e8; // ruptures here are shear ruptures, or faults
    rheology->tens_cutoff[3] = 1.e8;

    rheology->Hst_cutoff[0] = 3.e8; // tau cutoff MAX
    rheology->Hst_cutoff[1]  = 3.e8; // Higher pressures mean that it has entered MISSES region, or brittle behaviour
    rheology->Hst_cutoff[2]  = 3.e8; // ruptures in these region are ruptures from compression
    rheology->Hst_cutoff[3]  = 3.e8;

    rheology->softening_type[0]=1; // ???
    rheology->softening_type[1]=1;
    rheology->softening_type[2]=1;
    rheology->softening_type[3]=1;

    rheology->soft_Co_inf[0] = 2.e7; // Cohesion 
    rheology->soft_Co_inf[1] = 2.e7;
    rheology->soft_Co_inf[2] = 2.e8;
    rheology->soft_Co_inf[3] = 2.e8;

    rheology->soft_phi_inf[0] = 1.; // min angle of internal friction
    rheology->soft_phi_inf[1] = 1.; // SP282
    rheology->soft_phi_inf[2] = 0.0;
    rheology->soft_phi_inf[3] = 0.0;

    rheology->soft_min_strain_cutoff[0] = 0.5;
    rheology->soft_min_strain_cutoff[1] = 0.5;
    rheology->soft_min_strain_cutoff[2] = 0.0;
    rheology->soft_min_strain_cutoff[3] = 0.0;

    rheology->soft_max_strain_cutoff[0] = 1.5;
    rheology->soft_max_strain_cutoff[1] = 1.5;
    rheology->soft_max_strain_cutoff[2] = 1.0;
    rheology->soft_max_strain_cutoff[3] = 1.0;
    break;
  case 2: //Similar to 1, but with sedimentary layer
    printf( "Ritske Panonian basin , 2001\n" );  
    nphase = 5;
    rheology->temp_kappa[0] = 1e-6;  // diffusivity
    rheology->temp_kappa[1] = 1e-6;
    rheology->temp_kappa[2] = 1e-6;
    rheology->temp_kappa[3] = 1e-6;
    rheology->temp_kappa[4] = 1e-6;

    rheology->density_type[0] = 1; // See model_GENE.c
    rheology->density_type[1] = 1;
    rheology->density_type[2] = 1;
    rheology->density_type[3] = 1;
    rheology->density_type[4] = 1;

    rheology->const_rho0[0] = 2700; // layer material density at T=273K
    rheology->const_rho0[1] = 2800;
    rheology->const_rho0[2] = 3300;
    rheology->const_rho0[3] = 3300;
    rheology->const_rho0[4] = 2500; // Limestone

    rheology->temp_alpha[0] = 3.1e-5; // Linear thermal expansion coefficient
    rheology->temp_alpha[1] = 3.1e-5; // alpha = 1/rho.(d rho/d Temperature) ???
    rheology->temp_alpha[2] = 3.1e-5;
    rheology->temp_alpha[3] = 3.1e-5;
    rheology->temp_alpha[4] = 3.1e-5;

    rheology->temp_beta[0] = 1e-11; // Volumetric thermal expansion coefficient ???
    rheology->temp_beta[1] = 1e-11; // Compressibility ??? Beta=1/rho.(d rho/d Pressure) ???
    rheology->temp_beta[2] = 1e-11;
    rheology->temp_beta[3] = 1e-11;
    rheology->temp_beta[4] = 1e-11;

    rheology->viscous_type[0] = 2; // See model_GENE.c
    rheology->viscous_type[1] = 2;
    rheology->viscous_type[2] = 2;
    rheology->viscous_type[3] = 2;
    rheology->viscous_type[4] = 2;

    rheology->arrh_nexp[0]    = 3.3; // Arrhenius parameter????
    rheology->arrh_nexp[1]    = 3.05; // Power law for dislocation creep ???
    rheology->arrh_nexp[2]    = 3.0;
    rheology->arrh_nexp[3]    = 3.0;
    rheology->arrh_nexp[4]    = 3.0;

// Activation energy
    rheology->arrh_entalpy[0] = 186.5e3; // wet Quartzite
    rheology->arrh_entalpy[1] = 276.0e3; // pikwitonian granulite
    rheology->arrh_entalpy[2] = 510.e3; // dry olivine
    rheology->arrh_entalpy[3] = 510.e3; // wet olivine
    rheology->arrh_entalpy[4] = 186.5e3; // calcite

    rheology->arrh_preexpA[0] = 3.16e-26; // Arrhenius parameter????
    rheology->arrh_preexpA[1] = 2.20e-20;
    rheology->arrh_preexpA[2] = 7.0e-14;
    rheology->arrh_preexpA[3] = 7.0e-14;
    rheology->arrh_preexpA[4] = 3.16e-26;

    rheology->arrh_Ascale[0]  =1.; // Scaling factor A for phase
    rheology->arrh_Ascale[1]  =1.; // allows to convert from MPa to Pa
    rheology->arrh_Ascale[2]  =1.;
    rheology->arrh_Ascale[3]  =1.;
    rheology->arrh_Ascale[4]  =1.;

    rheology->arrh_Vmol[0]    =0.0; // Activation volume
    rheology->arrh_Vmol[1]    =0.0; // Generally only used for very large depths, where viscosity starts to increase with depth
    rheology->arrh_Vmol[2]    =0.0;
    rheology->arrh_Vmol[3]    =0.0;
    rheology->arrh_Vmol[4]    =0.0;

    rheology->arrh_Tref[0]    =273.; // Arrhenius reference temperature
    rheology->arrh_Tref[1]    =273.;
    rheology->arrh_Tref[2]    =273.;
    rheology->arrh_Tref[3]    =273.;
    rheology->arrh_Tref[4]    =273.;

    rheology->plastic_type[0]=2; // See model_GENE.c
    rheology->plastic_type[1]=2;
    rheology->plastic_type[2]=2;
    rheology->plastic_type[3]=2;
    rheology->plastic_type[4]=2;

    rheology->mises_tau_yield[0]=2.e7; // cohesion 
    rheology->mises_tau_yield[1]=2.e7; // cohesion
    rheology->mises_tau_yield[2]=2.e7; // cohesion
    rheology->mises_tau_yield[3]=2.e7; // cohesion
    rheology->mises_tau_yield[4]=2.e7; // cohesion

    rheology->dp_pressure_dependance[0] = 30.; // Angle for MISSES behaviour
    rheology->dp_pressure_dependance[1] = 30.; // using Huismans numbers SP282 Fig3 Top Right
    rheology->dp_pressure_dependance[2] = 30.0;
    rheology->dp_pressure_dependance[3] = 30.0;
    rheology->dp_pressure_dependance[4] = 30.0;

    rheology->tens_cutoff[0] = 1.e7; // tau cutoff MIN
    rheology->tens_cutoff[1] = 1.e7; // Higher pressures mean that it has entered the viscous slip region
    rheology->tens_cutoff[2] = 1.e7; // ruptures here are shear ruptures, or faults
    rheology->tens_cutoff[3] = 1.e7;
    rheology->tens_cutoff[4] = 1.e7;

    rheology->Hst_cutoff[0]  = 10.e8; // tau cutoff MAX
    rheology->Hst_cutoff[1]  = 10.e8; // Higher pressures mean that it has entered MISSES region, or brittle behaviour
    rheology->Hst_cutoff[2]  = 10.e8; // ruptures in these region are ruptures from compression
    rheology->Hst_cutoff[3]  = 10.e8;
    rheology->Hst_cutoff[4]  = 10.e8;

    rheology->softening_type[0]=1; // ???
    rheology->softening_type[1]=1;
    rheology->softening_type[2]=1;
    rheology->softening_type[3]=1;
    rheology->softening_type[4]=1;

    rheology->soft_Co_inf[0] = 2.e7; // Cohesion 
    rheology->soft_Co_inf[1] = 2.e7;
    rheology->soft_Co_inf[2] = 2.e7;
    rheology->soft_Co_inf[3] = 2.e7;
    rheology->soft_Co_inf[4] = 2.e7;

    rheology->soft_phi_inf[0] = 30.0; // min angle of internal friction
    rheology->soft_phi_inf[1] = 30.0; // SP282
    rheology->soft_phi_inf[2] = 30.0;
    rheology->soft_phi_inf[3] = 30.0;
    rheology->soft_phi_inf[4] = 30.0;

    rheology->soft_min_strain_cutoff[0] = 0.5;
    rheology->soft_min_strain_cutoff[1] = 0.5;
    rheology->soft_min_strain_cutoff[2] = 0.0;
    rheology->soft_min_strain_cutoff[3] = 0.0;
    rheology->soft_min_strain_cutoff[4] = 0.5;

    rheology->soft_max_strain_cutoff[0] = 1.5;
    rheology->soft_max_strain_cutoff[1] = 1.5;
    rheology->soft_max_strain_cutoff[2] = 1.0;
    rheology->soft_max_strain_cutoff[3] = 1.0;
    rheology->soft_max_strain_cutoff[4] = 1.5;
    break;
  default:
 //   printf( "Model: Empty model.\n" );  
    nphase=1;
    rheology->temp_kappa[0] = 1e-6;  // diffusivity
    rheology->density_type[0] = 1; // ???
    rheology->const_rho0[0] = 2700; // layer material density at T=273K
    rheology->temp_alpha[0] = 2.7e-5; // Linear thermal expansion coefficient
    rheology->temp_beta[0] = 2e-12; // Volumetric thermal expansion coefficient ???
    rheology->viscous_type[0] = 2; // ???
    rheology->arrh_nexp[0]    = 3; // Arrhenius parameter????
    rheology->arrh_entalpy[0] = 223.e3; // wet Quartzite
    rheology->arrh_preexpA[0] = 1.e-6; // Arrhenius parameter????
    rheology->arrh_Ascale[0]  =1.e6; // Scaling factor A for phase
    rheology->arrh_Vmol[0]    =0.0; // Activation volume
    rheology->arrh_Tref[0]    =273; // Arrhenius reference temperature
    rheology->plastic_type[0]=2; // ???
    rheology->mises_tau_yield[0]=2.e7; // cohesion
    rheology->dp_pressure_dependance[0] = 7.; // Angle for MISSES behaviour
    rheology->tens_cutoff[0] = 1.e7; // tau cutoff MIN
    rheology->Hst_cutoff[0] = 3.e8; // tau cutoff MAX
    rheology->softening_type[0]=1; // ???
    rheology->soft_Co_inf[0] = 2.e7; // Cohesion 
    rheology->soft_phi_inf[0] = 1.; // min angle of internal friction
    rheology->soft_min_strain_cutoff[0] = 0.5;
    rheology->soft_max_strain_cutoff[0] = 1.5;
  }

  PetscFunctionReturn(0);
}

/*pTatin2d_ModelSetMarkerIndexFromMap_GENE
 assign index to markers from map that is in context
 */
#undef __FUNCT__
#define __FUNCT__ "pTatin2d_ModelSetMarkerIndex_afonso"
PetscErrorCode pTatin2d_ModelSetMarkerIndex_afonso(pTatinCtx ctx)
{

  PetscInt               p,n_mp_points;
  DataBucket             db;
  DataField              PField_std;
  int                    phase;
  PetscScalar            ymoho,ylowercrust,yastenosphere,ysedim;
  PetscErrorCode ierr;
  pTatinUnits *units;

  PetscFunctionBegin;
  units=&ctx->units;
  yastenosphere=-100.e3;
  ymoho = -35.0e3; // SI units
  ylowercrust = -20.0e3; // SI units
  ysedim=-1.0e3;
  UnitsApplyInverseScaling(units->si_length,yastenosphere,&yastenosphere); // scaled with the rest
  UnitsApplyInverseScaling(units->si_length,ymoho,&ymoho); // scaled with the rest
  UnitsApplyInverseScaling(units->si_length,ylowercrust,&ylowercrust); // scaled with the rest
  UnitsApplyInverseScaling(units->si_length,ysedim,&ysedim); // scaled with the rest

  /* define properties on material points */
  db = ctx->db;
  DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
  DataFieldGetAccess(PField_std);
  DataFieldVerifyAccess(PField_std,sizeof(MPntStd));

  DataBucketGetSizes(db,&n_mp_points,0,0);

  for (p=0; p<n_mp_points; p++) {
    MPntStd     *material_point;
    double  *position;
    DataFieldAccessPoint(PField_std,p,(void**)&material_point);
    /* Access using the getter function provided for you (recommeneded for beginner user) */
    MPntStdGetField_global_coord(material_point,&position);

//    if (position[1] > ysedim){phase = 4;}
//    else {
      if (position[1] > ylowercrust){phase = 0;}
      else {
        if (position[1] > ymoho){phase= 1;}
        else {
          if (position[1] > yastenosphere){phase=2;}
          else {phase= 3;}
        }
      }
//    }
    MPntStdSetField_phase_index(material_point,phase);
  }

  DataFieldRestoreAccess(PField_std);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatin2d_ModelSetInitialTemperatureOnNodes_afonso"
PetscErrorCode pTatin2d_ModelSetInitialTemperatureOnNodes_afonso(pTatinCtx ctx)
/* define properties on material points */
{
  PetscErrorCode         ierr;
  DM da;
  Vec T;
  PhysCompEnergyCtx phys_energy;
  PetscInt i,j,si,sj,nx,ny;
  Vec coords;
  DMDACoor2d **LA_coords;
  DM cda;
  PetscBool found;
  PetscScalar **LA_T;
  PetscScalar Ttop,Tbot,jmin[2],jmax[2];
  PetscReal age0,age_anom,age,L_Bar,Cx,Wx,kappa,yastenosphere;
  pTatinUnits *units;
  
  PetscFunctionBegin;

  phys_energy = ctx->phys_energy;
  da          = phys_energy->daT;
  T           = phys_energy->T;
        units   = &ctx->units;

  Tbot  = 1400.0;
  Ttop  = 0.0;
  yastenosphere=-100.e3;
  UnitsApplyInverseScaling(units->si_length,yastenosphere,&yastenosphere); // scaled with the rest

  ierr = PetscOptionsGetScalar(PETSC_NULL,"-Ttop",&Ttop,PETSC_NULL);CHKERRQ(ierr);
        ierr = PetscOptionsGetScalar(PETSC_NULL,"-Tbot",&Tbot,PETSC_NULL);CHKERRQ(ierr);

  ierr = DMDAGetCoordinates(da,&coords);CHKERRQ(ierr);
  ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(cda,coords,&LA_coords);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da,T,&LA_T);CHKERRQ(ierr);
  ierr = DMDAGetCorners(da,&si,&sj,0 , &nx,&ny,0);CHKERRQ(ierr);

  DMDAGetBoundingBox(da,jmin,jmax);

// age = (1-exp(-pow((x-xc)*wx,2)))*(age0-age_anom)/age0)*age0;
//    *val = -Tbot*erfc((-y*L_bar)/sqrt(1e-6*age*3.14e13))+Tbot;
//
    age0=300; //M years
    age_anom=200; //M years
    Cx=300.e3; // anomaly center
    Wx=25.e3; // anomaly width ??
    kappa=1e-6;

    ierr  = PetscOptionsGetReal(PETSC_NULL,"-Wx",&Wx,&found);CHKERRQ(ierr);
    ierr  = PetscOptionsGetReal(PETSC_NULL,"-age0",&age0,&found);CHKERRQ(ierr);
    ierr  = PetscOptionsGetReal(PETSC_NULL,"-age1",&age_anom,&found);CHKERRQ(ierr);

    UnitsApplyInverseScaling(units->si_length,Cx,&Cx);CHKERRQ(ierr);
    UnitsApplyInverseScaling(units->si_length,Wx,&Wx);CHKERRQ(ierr);
    UnitsApplyInverseScaling(units->si_length,L_Bar,&L_Bar);CHKERRQ(ierr);

  for( j=sj; j<sj+ny; j++ ) {
    for( i=si; i<si+nx; i++ ) {
      PetscScalar xn,yn,Tic;
      double position[2];
      position[0] = LA_coords[j][i].x;
      position[1] = LA_coords[j][i].y;

      /* linear gradient from top 0 to bottom temp Tic*/
//      LA_T[j][i] = (jmax[1]-position[1])/(jmax[1]-jmin[1])*(Tbot-Ttop)+Ttop;

      if (position[1] < yastenosphere ) {LA_T[j][i]=Tbot;}
      else {
        age=(1-exp(-pow((position[0]-Cx)/(sqrt(2.0)*Wx),2))*(age0-age_anom)/age0)*age0;
        L_Bar=sqrt(2*kappa*age*3.14e13);
        UnitsApplyInverseScaling(units->si_length,L_Bar,&L_Bar);CHKERRQ(ierr);
        LA_T[j][i] = Tbot-(Tbot*erfc((-position[1])/L_Bar));
      }

//    LA_T[j][i] = Tbot-Tbot*erfc((-position[1]*L_Bar)/sqrt(kappa*age*3.14e13)); // this gives only the perturbation

    }
  }
  ierr = DMDAVecRestoreArray(da,T,&LA_T);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(cda,coords,&LA_coords);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatin2d_ModelSetInitialStokesVariableOnMarker_afonso"
PetscErrorCode pTatin2d_ModelSetInitialStokesVariableOnMarker_afonso(pTatinCtx ctx)
/* define properties on material points */
{
  PhaseMap               phasemap;
  PetscInt               e,p,n_mp_points;
  DataBucket             db;
  DataField              PField_std,PField_stokes;
  int                    phase_index, i;
  PetscErrorCode         ierr;
  RheologyConstants      *rheology;
  double                 eta_ND,rho_ND;
  pTatinUnits            *units;

  PetscFunctionBegin;
  units   = &ctx->units;

  rheology   = &ctx->rheology_constants;
  db = ctx->db;

  DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
  DataFieldGetAccess(PField_std);
  DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
  DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
  DataFieldGetAccess(PField_stokes);
  DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));
  DataBucketGetSizes(db,&n_mp_points,0,0);

  for (p=0; p<n_mp_points; p++) {
    MPntStd     *material_point;
    MPntPStokes *mpprop_stokes;
    DataFieldAccessPoint(PField_std,p,(void**)&material_point);
    DataFieldAccessPoint(PField_stokes,p,(void**)&mpprop_stokes);
    MPntStdGetField_phase_index(material_point,&phase_index);

    UnitsApplyInverseScaling(units->si_viscosity,1.e+21,&eta_ND);
    UnitsApplyInverseScaling(units->si_force_per_volume,rheology->const_rho0[phase_index],&rho_ND);
    MPntPStokesSetField_eta_effective(mpprop_stokes,eta_ND);
    MPntPStokesSetField_density(mpprop_stokes,rho_ND);
  }

  DataFieldRestoreAccess(PField_std);
  DataFieldRestoreAccess(PField_stokes);

  PetscFunctionReturn(0);
}

/*pTatin2d_ModelSetInitialEnergyVariableOnMarker_GENE
 assign initial kappa and prod
 */
#undef __FUNCT__
#define __FUNCT__ "pTatin2d_ModelSetInitialEnergyVariableOnMarker_afonso"
PetscErrorCode pTatin2d_ModelSetInitialEnergyVariableOnMarker_afonso(pTatinCtx ctx)
{
  DataField         PField_thermal,PField_std,PField_stokes;
  DataBucket        db;
  int               phase_index;
  PetscInt          p,n_mp_points;
  PetscErrorCode    ierr;
  RheologyConstants *rheology;
  pTatinUnits *units;
  TempMap                map;
  PetscBool         isproduction;
  char file[PETSC_MAX_PATH_LEN];

  PetscFunctionBegin;
  units   = &ctx->units;
  rheology   = &ctx->rheology_constants;
  db = ctx->db;

  /* standard data */
  DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
  DataFieldGetAccess(PField_std);
  DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
  /* thermal data */
  DataBucketGetDataFieldByName(db,MPntPThermal_classname,&PField_thermal);
  DataFieldGetAccess(PField_thermal);
  DataFieldVerifyAccess(PField_thermal,sizeof(MPntPThermal));

  DataBucketGetSizes(db,&n_mp_points,0,0);

  for (p=0; p<n_mp_points; p++) {
    MPntStd      *material_point;
    MPntPThermal *material_point_thermal;
    double       *position,kappa_ND,prod_ND;

    DataFieldAccessPoint(PField_std,    p,(void**)&material_point);
    DataFieldAccessPoint(PField_thermal,p,(void**)&material_point_thermal);
    MPntStdGetField_phase_index(material_point,&phase_index);
    position = material_point->coor;

    UnitsApplyInverseScaling(units->si_diffusivity,rheology->temp_kappa[phase_index],&kappa_ND);
    MPntPThermalSetField_diffusivity(material_point_thermal,kappa_ND);

    prod_ND = 0.0;
    MPntPThermalSetField_heat_source(material_point_thermal,prod_ND);

  }
  DataFieldRestoreAccess(PField_std);
  DataFieldRestoreAccess(PField_thermal);
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "pTatin2d_ModelSetInitialPlsVariableOnMarker_afonso"
PetscErrorCode pTatin2d_ModelSetInitialPlsVariableOnMarker_afonso(pTatinCtx ctx)
/* define properties on material points */
{
  PhaseMap               phasemap;
  PetscInt               e,p,n_mp_points;
  DataBucket             db;
  DataField              PField_std,PField_stokes,PField_stokespl;
  int                    phase_index, i;
  PetscErrorCode         ierr;
  RheologyConstants      *rheology;

  PetscFunctionBegin;

  rheology   = &ctx->rheology_constants;
  db = ctx->db;


  DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
  DataFieldGetAccess(PField_std);
  DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
  /* get the plastic variables */
  DataBucketGetDataFieldByName(db,MPntPStokesPl_classname,&PField_stokespl);
  DataFieldGetAccess(PField_stokespl);
  DataFieldVerifyAccess(PField_stokespl,sizeof(MPntPStokesPl));

  DataBucketGetSizes(db,&n_mp_points,0,0);

  for (p=0; p<n_mp_points; p++) {
    MPntStd     *material_point;
    MPntPStokesPl *mpprop_stokespl; /* plastic variables */
    DataFieldAccessPoint(PField_std,p,(void**)&material_point);
    DataFieldAccessPoint(PField_stokespl,p,(void**)&mpprop_stokespl);

    MPntStdGetField_phase_index(material_point,&phase_index);
    MPntPStokesPlSetField_plastic_strain(mpprop_stokespl,0.0);
    //TODO  Add noise if needed
    MPntPStokesPlSetField_yield_indicator(mpprop_stokespl,0.0);
  }

  DataFieldRestoreAccess(PField_std);
  DataFieldRestoreAccess(PField_stokespl);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_afonso"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_afonso(pTatinCtx ctx)
{
  BCList         ubclist;
  PetscScalar    bcval;
  DM             dav;
  PetscErrorCode ierr;

  PetscReal jmin[3],jmax[3];
  PetscFunctionBegin;
  PetscScalar opts_srH, opts_Lx,opts_Ly,p_base;
  PetscInt opts_bcs;
  PetscScalar alpha_m;
  pTatinUnits *units;
  BC_Alabeaumont bcdata;
  BC_Step   bcdatastep;
  PetscScalar y1,dy,coeff;

  units   = &ctx->units;
  ubclist = ctx->u_bclist;
  dav     = ctx->dav;

  opts_srH = 0.0;
  ierr = PetscOptionsGetScalar(PETSC_NULL,"-vx",&opts_srH,0);CHKERRQ(ierr);

  UnitsApplyInverseScaling(units->si_velocity,opts_srH,&opts_srH);

  opts_bcs = 0;
  ierr = PetscOptionsGetInt(PETSC_NULL,"-bc_type",&opts_bcs,0);CHKERRQ(ierr);

  DMDAGetBoundingBox(dav,jmin,jmax);
  opts_Lx = jmax[0]-jmin[0];
  opts_Ly = jmax[1]-jmin[1];

  switch(opts_bcs){
      // free surface and free slip all side with symetric horizontal velocity
    case 0 : {
      bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
    }
      break;

      // incompressible symetric bottom fixed
    case 1 : {
      bcval = -opts_srH*2.0*opts_Ly/opts_Lx;
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
    }
      break;

      // incompressible right side
    case 2 : {
      bcval = -opts_srH*opts_Ly/opts_Lx;
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
    } bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      break;

      // incompressible 4 sides
    case 3 : {
      bcval = -opts_srH*opts_Ly/opts_Lx;
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

      bcval = opts_srH*opts_Ly/opts_Lx;
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

      bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
    }
      break;

      // free slip everywhere
    case 4 :
      bcval = 0.0;
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      break;
      // free surface both side with mid point fixed

    case 5 :
      bcval = opts_srH;
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = 0.0;
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_MIDPOINT,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_MIDPOINT,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      break;

      // free surface right side
    case 6 : {

      bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
    }
      break;

      // free surface left side
    case 7 : {

      bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = 0.0;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
    }
      break;
      // free surface and rigid wall on lateral side with symmetric horizontal velocity
    case 8 : {
      bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = 0.0;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = 0.0;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
    }
      break;

    case 9 : {
      BC_WrinklerData bcdata;
      PetscScalar p_base,deltarho,yref,gravity_acceleration;
      PetscInt remesh_type;
      PetscBool found;

      bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

      bcval = 0.0;
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_BOTLEFTCORNERPOINT,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_BOTRIGHTCORNERPOINT,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

      /* basement pressure */
      PetscOptionsGetInt(PETSC_NULL,"-remesh_type",&remesh_type,0);
      if (remesh_type != 6 & remesh_type!=7){
      SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"In order to use a wrinkler foundation, you need to set -remesh_type option to 6 \n");
      }
        ierr = PetscMalloc(sizeof(struct _p_BC_WrinklerData),&bcdata);CHKERRQ(ierr);
      ierr = PetscOptionsGetScalar(PETSC_NULL,"-winkler_p_base",&p_base,0);CHKERRQ(ierr);
        UnitsApplyInverseScaling(units->si_stress,p_base,&p_base);
        bcdata->Pisos=p_base;

        ierr = PetscOptionsGetScalar(PETSC_NULL,"-winkler_yref",&yref,0);CHKERRQ(ierr);
        UnitsApplyInverseScaling(units->si_length,yref,&yref);
        bcdata->yref=yref;
        ierr  = PetscOptionsGetReal(PETSC_NULL,"-grav",&gravity_acceleration,&found);CHKERRQ(ierr);
        if (found == PETSC_FALSE){
      SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"In order to use a wrinkler foundation, you need to set -grav option  \n");
      }

        ierr = PetscOptionsGetScalar(PETSC_NULL,"-winkler_deltarho",&deltarho,0);CHKERRQ(ierr);
        UnitsApplyInverseScaling(units->si_force_per_volume,deltarho,&deltarho);
        bcdata->deltarhog=deltarho*gravity_acceleration;

      ierr = SurfaceQuadratureStokesTraverseSetTraction(ctx->surfQ,DMDATracLoc_JMIN_LOC,SurfaceQuadatureEvaluator_Wrinkler,(void*)bcdata);CHKERRQ(ierr);
        ierr = PetscFree(bcdata);CHKERRQ(ierr);
    }
      break;

  case 10 : {
        BC_WrinklerData bcdata;
        PetscScalar p_base,deltarho,yref,gravity_acceleration;
            PetscInt remesh_type;
            PetscBool found;

      bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

      bcval = 0.0;
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_MIDPOINT,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_MIDPOINT,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

      /* basement pressure */
      PetscOptionsGetInt(PETSC_NULL,"-remesh_type",&remesh_type,0);
      if (remesh_type != 7){
      SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"In order to use a wrinkler foundation, you need to set -remesh_type option to 7 \n");
      }
        ierr = PetscMalloc(sizeof(struct _p_BC_WrinklerData),&bcdata);CHKERRQ(ierr);
      ierr = PetscOptionsGetScalar(PETSC_NULL,"-winkler_p_base",&p_base,0);CHKERRQ(ierr);
        UnitsApplyInverseScaling(units->si_stress,p_base,&p_base);
        bcdata->Pisos=p_base;

        ierr = PetscOptionsGetScalar(PETSC_NULL,"-winkler_yref",&yref,0);CHKERRQ(ierr);
        UnitsApplyInverseScaling(units->si_length,yref,&yref);
        bcdata->yref=yref;
        ierr  = PetscOptionsGetReal(PETSC_NULL,"-grav",&gravity_acceleration,&found);CHKERRQ(ierr);
        if (found == PETSC_FALSE){
      SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"In order to use a wrinkler foundation, you need to set -grav option  \n");
      }

        ierr = PetscOptionsGetScalar(PETSC_NULL,"-winkler_deltarho",&deltarho,0);CHKERRQ(ierr);
        UnitsApplyInverseScaling(units->si_force_per_volume,deltarho,&deltarho);
        bcdata->deltarhog=deltarho*gravity_acceleration;

      ierr = SurfaceQuadratureStokesTraverseSetTraction(ctx->surfQ,DMDATracLoc_JMIN_LOC,SurfaceQuadatureEvaluator_Wrinkler,(void*)bcdata);CHKERRQ(ierr);
        ierr = PetscFree(bcdata);CHKERRQ(ierr);
    }
      break;
      case 11 : {
      /*incompressible free surface, basically fixe the velocity at the bottom to fill up or empty down with material that leaves on the side*/
      bcval = 2*opts_srH*opts_Ly/opts_Lx;
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

      bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
    }
      break;
    case 12 : {
      ierr = PetscMalloc(sizeof(struct _p_BC_Alabeaumont),&bcdata);CHKERRQ(ierr);

      /*incompressible free surface à la beaumont*/
      ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_ylitho",&y1,0);CHKERRQ(ierr);
      ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_deltay",&dy,0);CHKERRQ(ierr);

      UnitsApplyInverseScaling(units->si_length,y1,&y1);
      UnitsApplyInverseScaling(units->si_length,dy,&dy);

      bcval = 0.0;
      bcdata->dim = 1;
      bcdata->x0 = y1;
      bcdata->x1 = y1-dy;
      coeff = (2*(jmax[1]-y1)+dy)/(2*(y1-dy-jmin[1]));
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

      bcdata->v0 = -opts_srH; bcdata->v1 = opts_srH*coeff;
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_Alabeaumont,(void*)bcdata);CHKERRQ(ierr);
      bcdata->v0 = opts_srH; bcdata->v1 = -opts_srH*coeff;
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_Alabeaumont,(void*)bcdata);CHKERRQ(ierr);
      ierr = PetscFree(bcdata);CHKERRQ(ierr);
    }
      break;

    case 13 : {
      ierr = PetscMalloc(sizeof(struct _p_BC_Alabeaumont),&bcdata);CHKERRQ(ierr);

      /*incompressible free surface à la beaumont*/
      ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_ylitho",&y1,0);CHKERRQ(ierr);
      ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_deltay",&dy,0);CHKERRQ(ierr);

      UnitsApplyInverseScaling(units->si_length,y1,&y1);
      UnitsApplyInverseScaling(units->si_length,dy,&dy);

      bcval = 0.0;
      bcdata->dim = 1;
      bcdata->x0 = y1;
      bcdata->x1 = y1-dy;
      coeff = (2*(jmax[1]-y1)+dy)/(2*(y1-dy-jmin[1]));
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

      bcdata->v0 = -opts_srH; bcdata->v1 = opts_srH*coeff;
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_Alabeaumont,(void*)bcdata);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcdata->v0 = opts_srH; bcdata->v1 = -opts_srH*coeff;
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_Alabeaumont,(void*)bcdata);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      ierr = PetscFree(bcdata);CHKERRQ(ierr);
    }
      break;

    case 14 : {
      ierr = PetscMalloc(sizeof(struct _p_BC_Alabeaumont),&bcdata);CHKERRQ(ierr);
      ierr =  PetscMalloc(sizeof(struct _p_BC_Step),&bcdatastep);CHKERRQ(ierr);

      /*incompressible free surface à la beaumont*/
      ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_ylitho",&y1,0);CHKERRQ(ierr);
      ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_deltay",&dy,0);CHKERRQ(ierr);

      UnitsApplyInverseScaling(units->si_length,y1,&y1);
      UnitsApplyInverseScaling(units->si_length,dy,&dy);

      bcval = 0.0;
      bcdata->dim = 1;
      bcdata->x0 = y1;
      bcdata->x1 = y1-dy;
      bcdatastep->beg = y1-dy;
      bcdatastep->end = y1;
      bcdatastep->dim = 1;
      bcdatastep->v0 = 0.0;
      bcdatastep->v1 = 0.0;
      bcdatastep->b = 0;
      bcdatastep->a = 1.0;

      coeff = (dy+2*jmax[1]-2*y1)/(2*y1-2*jmin[1]-dy);


      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

      bcdata->v0 = -opts_srH; bcdata->v1 = opts_srH*coeff;
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_Alabeaumont,(void*)bcdata);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_Step,(void*)bcdatastep);CHKERRQ(ierr);
      bcdata->v0 = opts_srH; bcdata->v1 = -opts_srH*coeff;
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_Alabeaumont,(void*)bcdata);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,1,BCListEvaluator_Step,(void*)bcdatastep);CHKERRQ(ierr);
      ierr = PetscFree(bcdata);CHKERRQ(ierr);
      ierr = PetscFree(bcdatastep);CHKERRQ(ierr);
    }
      break;

    case 15 : {
      ierr = PetscMalloc(sizeof(struct _p_BC_Alabeaumont),&bcdata);CHKERRQ(ierr);
      ierr =  PetscMalloc(sizeof(struct _p_BC_Step),&bcdatastep);CHKERRQ(ierr);

      /*incompressible free surface à la beaumont*/
      ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_ylitho",&y1,0);CHKERRQ(ierr);
      ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_deltay",&dy,0);CHKERRQ(ierr);

      UnitsApplyInverseScaling(units->si_length,y1,&y1);
      UnitsApplyInverseScaling(units->si_length,dy,&dy);

      bcval = 0.0;
      bcdata->dim = 1;
      bcdata->x0 = y1;
      bcdata->x1 = y1-dy;
      bcdatastep->beg = y1-dy;
      bcdatastep->end = y1;
      bcdatastep->dim = 1;
      bcdatastep->v0 = 0.0;
      bcdatastep->v1 = 0.0;
      bcdatastep->b = 0;
      bcdatastep->a = 1.0;

      coeff = (dy+2*jmax[1]-2*y1)/(2*y1-2*jmin[1]-dy);

      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

      bcdata->v0 = opts_srH; bcdata->v1 = -opts_srH*coeff;
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_Alabeaumont,(void*)bcdata);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      ierr = PetscFree(bcdata);CHKERRQ(ierr);
      ierr = PetscFree(bcdatastep);CHKERRQ(ierr);
    }
      break;

        case 16 : {
      PetscInt rigid,freeslip;
      // Geomod BC's

      ierr =  PetscMalloc(sizeof(struct _p_BC_Step),&bcdatastep);CHKERRQ(ierr);
      bcval = 0.0;
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

      rigid = 0;
      freeslip = 0;
      ierr = PetscOptionsGetInt(PETSC_NULL,"-bc_rigid",&rigid,0);CHKERRQ(ierr);
      ierr = PetscOptionsGetInt(PETSC_NULL,"-bc_freeslip",&freeslip,0);CHKERRQ(ierr);
      if (rigid==1){
          bcdatastep->beg = -1.0;
          bcdatastep->end = 2.8;
          bcdatastep->dim = 1;
          bcdatastep->v0 = bcval;
          bcdatastep->v1 = bcval;
          bcdatastep->b = -0.0;
          bcdatastep->a = -0.5;
          ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,1,BCListEvaluator_Step,(void*)bcdatastep);CHKERRQ(ierr);
      }

      if (rigid ==2){
          ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      }

      if (freeslip == 0){
          bcval = 0.0;
          ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
          bcval = -2.5;
          bcdatastep->beg = -1.0;
          bcdatastep->end = 10;
          bcdatastep->dim = 1;
          bcdatastep->v0 = bcval;
          bcdatastep->v1 = 0.0;
          bcdatastep->b = -0.0;
          bcdatastep->a = -0.2;
          ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_Step,(void*)bcdatastep);CHKERRQ(ierr);
            }

            if (freeslip ==1){
                bcval = -2.5;
                ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
                ierr = PetscFree(bcdatastep);CHKERRQ(ierr);
            }
        }
            break;
    }

    /* check for energy solver */
    {
        PetscBool active = PETSC_FALSE;
        ierr = pTatinPhysCompActivated(ctx,PhysComp_Energy,&active);CHKERRQ(ierr);

        if (active) {
            DM da;
            Vec T;
            PhysCompEnergyCtx phys_energy;
            PetscScalar bcval;
            PetscBool   found;

            phys_energy = ctx->phys_energy;
            da          = phys_energy->daT;
            T           = phys_energy->T;

            bcval = 0.0;
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-Ttop",&bcval,&found);CHKERRQ(ierr);
            ierr = DMDABCListTraverse(phys_energy->T_bclist,da,DMDABCList_JMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
            bcval = 1400.0;
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-Tbot",&bcval,&found);CHKERRQ(ierr);
            ierr = DMDABCListTraverse(phys_energy->T_bclist,da,DMDABCList_JMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
        }
    }


    PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_afonso"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_afonso(pTatinCtx ctx)
{
  PetscErrorCode         ierr;
  pTatinUnits *units;

  PetscFunctionBegin;
  units   = &ctx->units;
  UnitsApplyInverseScaling(units->si_time,ctx->dt_min,&ctx->dt_min);
  UnitsApplyInverseScaling(units->si_time,ctx->dt,&ctx->dt);
  UnitsApplyInverseScaling(units->si_time,ctx->dt_adv,&ctx->dt_adv);
  UnitsApplyInverseScaling(units->si_time,ctx->dt_max,&ctx->dt_max);
  UnitsApplyInverseScaling(units->si_time,ctx->time_max,&ctx->time_max);
  UnitsApplyInverseScaling(units->si_length,ctx->max_disp_y,&ctx->max_disp_y);

  ierr = pTatin2d_ModelSetMaterialProperty_afonso(ctx); CHKERRQ(ierr);
  ierr = pTatin2d_ModelSetMarkerIndex_afonso(ctx); CHKERRQ(ierr);
  ierr = pTatin2d_ModelSetInitialStokesVariableOnMarker_afonso(ctx); CHKERRQ(ierr);
  ierr= pTatin2d_ModelSetInitialTemperatureOnNodes_afonso(ctx); CHKERRQ(ierr);
  ierr = pTatin2d_ModelSetInitialEnergyVariableOnMarker_afonso(ctx); CHKERRQ(ierr);
  ierr = pTatin2d_ModelSetInitialPlsVariableOnMarker_afonso(ctx); CHKERRQ(ierr);

// Erosion
    {
      PetscBool active = PETSC_FALSE;
      ierr = pTatinPhysCompActivated(ctx,PhysComp_SPM,&active);CHKERRQ(ierr);
      if (active) {
          ierr= pTatin2d_ModelSetSPMParameters_GENE(ctx); CHKERRQ(ierr);
      }
    }
  PetscFunctionReturn(0);
}
#undef __FUNCT__
