
static const char help[] = "Matrix free Stokes solver using Q2-Pm1 mixed finite elements.\n"
"2D prototype of the (p)ragmatic version of Tatin employing various time stepping routines.\n\n";

#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"
#include "pTatin2d_models.h"

PetscErrorCode TimeUpdateComputeRHS(pTatinCtx user,DM multipys_pack,Mat A,Mat B,Vec F,Vec X)
{
	IS              *isg;
	SNES            snes;
	KSP             ksp;
	PC              pc;
	DM              dav,dap;
	PetscErrorCode  ierr;

	/* boundary conditions */
	ierr = pTatin2d_ModelApplyBoundaryCondition(user);CHKERRQ(ierr);
	ierr = pTatin2d_ModelApplyMaterialBoundaryCondition(user);CHKERRQ(ierr);
	
	/* creation */
	ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
	
	ierr = SNESSetFunction(snes,F,FormFunction_Stokes,user);CHKERRQ(ierr);  
	if (user->rheology_constants.rheology_type == RHEOLOGY_VISCO_ELASTIC_PLASTIC ) {
		ierr = SNESSetFunction(snes,F,FormFunction_Stokes_ViscoElastoPlastic,user);CHKERRQ(ierr);  
	}
	
	if (user->use_mf_stokes) {
		ierr = SNESSetJacobian(snes,B,B,FormJacobian_Stokes,user);CHKERRQ(ierr);
	} else {
		ierr = SNESSetJacobian(snes,A,B,FormJacobian_Stokes,user);CHKERRQ(ierr);
	}
	ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
	
	/* configure for fieldsplit */
	ierr = SNESGetKSP(snes,&ksp);CHKERRQ(ierr);
	ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
	
	ierr = DMCompositeGetGlobalISs(multipys_pack,&isg);CHKERRQ(ierr);
	ierr = PCFieldSplitSetIS(pc,"u",isg[0]);CHKERRQ(ierr);
	ierr = PCFieldSplitSetIS(pc,"p",isg[1]);CHKERRQ(ierr);
	
	ierr = pTatinSetSNESMonitor(snes,user);CHKERRQ(ierr);
	
	ierr = SNESSolve(snes,PETSC_NULL,X);CHKERRQ(ierr);
	
	/* statistics */
	if (user->solverstatistics) {
		PetscInt its,lits,nfuncevals;
		
		ierr = SNESGetIterationNumber(snes,&its);CHKERRQ(ierr);
		ierr = SNESGetLinearSolveIterations(snes,&lits);CHKERRQ(ierr);
		ierr = SNESGetNumberFunctionEvals(snes,&nfuncevals);CHKERRQ(ierr);
		PetscPrintf(PETSC_COMM_WORLD,"SNES(Stokes) Statistics \n");
		PetscPrintf(PETSC_COMM_WORLD,"  nonlinear its    = %D \n", its);
		PetscPrintf(PETSC_COMM_WORLD,"  total linear its = %D \n", lits);
		PetscPrintf(PETSC_COMM_WORLD,"  total func evals = %D \n", nfuncevals);
		
		pTatin2dSolverStatistics(user,snes,X);
	}
	
	/* tidy up */
	ierr = ISDestroy(&isg[0]);CHKERRQ(ierr);
	ierr = ISDestroy(&isg[1]);CHKERRQ(ierr);
	ierr = PetscFree(isg);CHKERRQ(ierr);
	ierr = SNESDestroy(&snes);CHKERRQ(ierr);

	PetscFunctionReturn(0);
}

PetscErrorCode _ComputeDerivs(Vec X,PetscReal time,pTatinCtx user,DM multipys_pack,Mat A,Mat B,Vec F)
{
	PetscReal       copy_time;
	PetscErrorCode  ierr;
	
	copy_time = time;
	user->time = time;
	
	ierr = TimeUpdateComputeRHS(user,multipys_pack,A,B,F,X);CHKERRQ(ierr);	
	user->time = copy_time;
	
	PetscFunctionReturn(0);
}


PetscErrorCode RK7_DormandPrince(PetscReal time,PetscReal dt,Vec X,pTatinCtx user,DM multipys_pack,Mat A,Mat B,Vec F)
{
	double a[] = { 1/5, 3/10, 4/5, 8/9, 1, 1 };
	double b[6][6] = {
		{ 1.0/5.0,         0.0,           0.0,           0.0,        0.0,          0.0 },
		{ 3.0/40.0,        9.0/40.0,        0.0,           0.0,        0.0,          0.0 },
		{ 44.0/45.0,      -56.0/15.0,       32.0/9.0,        0.0,        0.0,          0.0 },
		{ 19372.0/6561.0, -25360.0/2187.0,  64448.0/6561.0, -212.0/729.0,  0.0,          0.0 },
		{ 9017.0/3168.0,  -355.0/33.0,      46732.0/5247.0,  49.0/176.0,  -5103.0/18656.0, 0.0 },
		{ 35.0/384.0,      0.0,           500.0/1113.0,    125.0/192.0, -2187.0/6784.0,  11.0/84.0 }
	};
	double c[] = {     5179.0/57600.0, 0.0, 7571.0/16695.0, 393.0/640.0, -92097.0/339200.0, 187.0/2100.0, 1.0/40.0 };
	double cstar[] = { 35.0/384.0,     0.0, 500.0/1113.0,   125.0/192.0, -2187.0/6784.0,    11.0/84.0,    0.0 };
	PetscReal time_value;
	PetscReal error,s,optimal_dt,nrm;
	PetscInt N;
	Vec *XK;
	DM dav,dap;
	Vec gcoords,coord_sol,coord_sol_star,t,Xprime,Xv,Xp,delta;
	PetscInt j,k;
	PetscBool step_not_accepted;
	PetscErrorCode ierr;
	
	ierr = DMCompositeGetEntries(multipys_pack,&dav,&dap);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(dav,&gcoords);CHKERRQ(ierr);
	ierr = VecDuplicateVecs(gcoords,7,&XK);CHKERRQ(ierr);
		
	ierr = VecDuplicate(gcoords,&t);CHKERRQ(ierr);
	ierr = VecDuplicate(X,&Xprime);CHKERRQ(ierr);

	ierr = VecDuplicate(gcoords,&coord_sol);CHKERRQ(ierr);
	ierr = VecDuplicate(gcoords,&coord_sol_star);CHKERRQ(ierr);
	
	ierr = VecCopy(gcoords,coord_sol);CHKERRQ(ierr);
	
	
	step_not_accepted = PETSC_TRUE;
	while (step_not_accepted) {
	
		ierr = VecCopy(coord_sol,XK[0]);CHKERRQ(ierr);
		for (k=0; k<6; k++) {
			/* prepare time */
			time_value = time + a[k] * dt;
			
			/* prepare step */
			ierr = VecCopy(coord_sol,t);CHKERRQ(ierr);
			for (j=0; j<k; j++) {
				ierr = VecAXPY(t,b[k][j],XK[j]);CHKERRQ(ierr);
			}
			/* force coordinates */
			ierr = DMDAGetCoordinates(dav,&gcoords);CHKERRQ(ierr);
			ierr = VecCopy(t,gcoords);CHKERRQ(ierr);
			ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
			
			/* solve */
			ierr = _ComputeDerivs(Xprime,time_value,user,multipys_pack,A,B,F);CHKERRQ(ierr);
			
			ierr = DMCompositeGetAccess(multipys_pack,Xprime,&Xv,&Xp);CHKERRQ(ierr);
			ierr = VecCopy(Xv,XK[k]);CHKERRQ(ierr);
			ierr = VecScale(XK[k],dt);CHKERRQ(ierr);
			ierr = DMCompositeRestoreAccess(multipys_pack,Xprime,&Xv,&Xp);CHKERRQ(ierr);
		}

		ierr = VecCopy(coord_sol,coord_sol_star);CHKERRQ(ierr);
		for (k=0; k<6; k++) {
			ierr = VecAXPY(coord_sol,     c[k],    XK[k]);CHKERRQ(ierr);
			ierr = VecAXPY(coord_sol_star,cstar[k],XK[k]);CHKERRQ(ierr);	
		}
		
		/* time step */
		ierr = VecAXPY(coord_sol_star,-1.0,coord_sol);CHKERRQ(ierr);
		error = 0.01;
		ierr = VecGetSize(coord_sol_star,&N);CHKERRQ(ierr);
		ierr = VecNorm(coord_sol_star,NORM_2,&nrm);CHKERRQ(ierr);
		nrm = nrm / PetscSqrtScalar(N);
		//s = pow( error * dt / (2.0 * nrm ), 0.25 );
		//optimal_dt = s * dt;
		//printf("optimal = %1.4e \n", optimal_dt );
		
		/* choose new dt */
		//dt = optimal_dt * 0.1;
		
		//ierr = VecAbs(coord_sol_star);CHKERRQ(ierr);
		//ierr = VecMax(coord_sol_star,0,&nrm);CHKERRQ(ierr);
		//printf("max = %1.4e \n", nrm );
		
		
	}	
	/* update time step */
	user->time = user->time + dt;

	/* update mesh coordinates */
	ierr = DMDAGetCoordinates(dav,&gcoords);CHKERRQ(ierr);
	ierr = VecCopy(coord_sol,gcoords);CHKERRQ(ierr);
	ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
	
	
	ierr = VecDestroy(&coord_sol);CHKERRQ(ierr);
	ierr = VecDestroy(&coord_sol_star);CHKERRQ(ierr);
	ierr = VecDestroy(&Xprime);CHKERRQ(ierr);
	ierr = VecDestroy(&t);CHKERRQ(ierr);
	ierr = VecDestroyVecs(7,&XK);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}



#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_material_points_std"
PetscErrorCode pTatin2d_material_points_std(int argc,char **argv)
{
	DM              multipys_pack;
	IS              *isg;
	SNES            snes;
	Mat             A,B,Auu,Aup,Apu,Spp;
	Vec             X,F;
	KSP             ksp;
	PC              pc;
	pTatinCtx user;
	PetscInt        e,tk;
	PetscReal       time;
	PetscErrorCode  ierr;
	
  
	ierr = pTatin2dCreateContext(&user);CHKERRQ(ierr);
	ierr = pTatin2dParseOptions(user);CHKERRQ(ierr);
	/*
	 {
	 PetscBool load_checkpoint=PETSC_FALSE;
	 
	 PetscOptionsGetBool(PETSC_NULL,"-load_cp",&load_checkpoint,0);
	 if (load_checkpoint==PETSC_TRUE) {
	 printf("loading checkpoint \n");
	 ierr = pTatinCheckpointLoad(user,PETSC_NULL);CHKERRQ(ierr);
	 }
	 }
	 */	
	ierr = pTatin2dCreateQ2PmMesh(user);CHKERRQ(ierr);
	ierr = pTatin2dCreateBoundaList(user);CHKERRQ(ierr);
	ierr = pTatin2dCreateQuadratureStokes(user);CHKERRQ(ierr);
	
	ierr = pTatin2dCreateMaterialPoints(user);CHKERRQ(ierr);
	
	/* mesh geometry */
	ierr = pTatin2d_ModelApplyInitialMeshGeometry(user);CHKERRQ(ierr);
	
	/* interpolate point coordinates (needed if mesh was modified) */
	ierr = QuadratureStokesCoordinateSetUp(user->Q,user->dav);CHKERRQ(ierr);
	ierr = MaterialPointCoordinateSetUp(user->db,user->dav);CHKERRQ(ierr);
	
	for (e=0; e<QUAD_EDGES; e++) {
		ierr = SurfaceQuadratureStokesGeometrySetUp(user->surfQ[e],user->dav);CHKERRQ(ierr);
	}
	
	
	/* material geometry */
	ierr = pTatin2d_ModelApplyInitialMaterialGeometry(user);CHKERRQ(ierr);
	
	/* boundary conditions */
	ierr = pTatin2d_ModelApplyBoundaryCondition(user);CHKERRQ(ierr);
	
	/*
	 ierr = pTatin2d_TestSurfaceQuadratureStokes(user);CHKERRQ(ierr);
	 ierr = pTatin2d_TestSurfaceQuadratureStokes2(user);CHKERRQ(ierr);
	 */
	
	multipys_pack = user->pack;
	ierr = DMCreateGlobalVector(multipys_pack,&X);CHKERRQ(ierr);
  ierr = VecDuplicate(X,&F);CHKERRQ(ierr);
	
	ierr = pTatin2d_ModelOutput(user,X,"ic_bc");CHKERRQ(ierr);
	
/*	
	if (!user->use_mf_stokes) {
		ierr = DMGetMatrix_DMCompositeStokesQkPm(multipys_pack,MATNEST,&A);CHKERRQ(ierr);
	}
	ierr = DMGetMatrix_DMCompositeStokesPCQkPm(multipys_pack,MATNEST,&B);CHKERRQ(ierr);
*/
	Auu = Aup = Apu = Spp = PETSC_NULL;
	ierr = DMCompositeStokes_MatCreate(multipys_pack,MATAIJ,&Auu,MATAIJ,&Aup,MATAIJ,PETSC_NULL,MATAIJ,&Spp);CHKERRQ(ierr);
	ierr = DMCompositeStokes_MatCreate(multipys_pack,MATAIJ,PETSC_NULL,MATAIJ,PETSC_NULL,MATAIJ,&Apu,MATAIJ,PETSC_NULL);CHKERRQ(ierr);
	
	{
		PetscBool upper,lower,full;
		
		upper = lower = full = PETSC_FALSE;
		ierr = PetscOptionsGetBool(PETSC_NULL,"-dmcomposite_pc_upper",&upper,PETSC_NULL);CHKERRQ(ierr);
		ierr = PetscOptionsGetBool(PETSC_NULL,"-dmcomposite_pc_lower",&lower,PETSC_NULL);CHKERRQ(ierr);
		ierr = PetscOptionsGetBool(PETSC_NULL,"-dmcomposite_pc_full",&full,PETSC_NULL);CHKERRQ(ierr);

		if ( (!full) && (!upper) && (!lower) ) {
			full = PETSC_TRUE;
			PetscPrintf(PETSC_COMM_WORLD,"Assuming Stokes preconditioner will be full: B = [ Auu, Aup ; Apu, Spp* ] \n");
		}
		
		if (full) {
			ierr = DMCompositeStokesCreateNest(multipys_pack,Auu,Aup,Apu,Spp,&B);CHKERRQ(ierr);
		} else if (upper) {
			ierr = DMCompositeStokesCreateNest(multipys_pack,Auu,Aup,PETSC_NULL,Spp,&B);CHKERRQ(ierr);
		} else if (lower) {
			ierr = DMCompositeStokesCreateNest(multipys_pack,Auu,PETSC_NULL,Apu,Spp,&B);CHKERRQ(ierr);
		}
	}
	if (!user->use_mf_stokes) {
		ierr = DMCompositeStokesCreateNest(multipys_pack,Auu,Aup,Apu,PETSC_NULL,&A);CHKERRQ(ierr);
	}

	
	/* ====== Generate an initial condition for velocity & pressure ====== */
	time = user->time; /* 0 or from checkpoint */
	tk   = user->step;
	user->time = time; 
	user->step = tk;

	PetscPrintf(PETSC_COMM_WORLD,"[IC/CHECKPOINT] step = %1.5d, time = %1.4e \n", tk, time );
	ierr = TimeUpdateComputeRHS(user,multipys_pack,A,B,F,X);CHKERRQ(ierr);
#if 0
	{
    /* boundary conditions */
    ierr = pTatin2d_ModelApplyBoundaryCondition(user);CHKERRQ(ierr);
    ierr = pTatin2d_ModelApplyMaterialBoundaryCondition(user);CHKERRQ(ierr);
		
		/* creation */
		ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
		
		ierr = SNESSetFunction(snes,F,FormFunction_Stokes,user);CHKERRQ(ierr);  
		if (user->rheology_constants.rheology_type == RHEOLOGY_VISCO_ELASTIC_PLASTIC ) {
			ierr = SNESSetFunction(snes,F,FormFunction_Stokes_ViscoElastoPlastic,user);CHKERRQ(ierr);  
		}
		
		if (user->use_mf_stokes) {
			ierr = SNESSetJacobian(snes,B,B,FormJacobian_Stokes,user);CHKERRQ(ierr);
		} else {
			ierr = SNESSetJacobian(snes,A,B,FormJacobian_Stokes,user);CHKERRQ(ierr);
		}
		ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
		
		/* configure for fieldsplit */
		ierr = SNESGetKSP(snes,&ksp);CHKERRQ(ierr);
		ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
		
		ierr = DMCompositeGetGlobalISs(multipys_pack,&isg);CHKERRQ(ierr);
		ierr = PCFieldSplitSetIS(pc,"u",isg[0]);CHKERRQ(ierr);
		ierr = PCFieldSplitSetIS(pc,"p",isg[1]);CHKERRQ(ierr);
		
		ierr = pTatinSetSNESMonitor(snes,user);CHKERRQ(ierr);

		ierr = SNESSolve(snes,PETSC_NULL,X);CHKERRQ(ierr);
		
		/* statistics */
		if (user->solverstatistics) {
			PetscInt its,lits,nfuncevals;
			ierr = SNESGetIterationNumber(snes,&its);CHKERRQ(ierr);
			ierr = SNESGetLinearSolveIterations(snes,&lits);CHKERRQ(ierr);
			ierr = SNESGetNumberFunctionEvals(snes,&nfuncevals);CHKERRQ(ierr);
			PetscPrintf(PETSC_COMM_WORLD,"SNES(Stokes) Statistics \n");
			PetscPrintf(PETSC_COMM_WORLD,"  nonlinear its    = %D \n", its);
			PetscPrintf(PETSC_COMM_WORLD,"  total linear its = %D \n", lits);
			PetscPrintf(PETSC_COMM_WORLD,"  total func evals = %D \n", nfuncevals);
			
			pTatin2dSolverStatistics(user,snes,X);
		}
		
		/* tidy up */
		ierr = ISDestroy(&isg[0]);CHKERRQ(ierr);
		ierr = ISDestroy(&isg[1]);CHKERRQ(ierr);
		ierr = PetscFree(isg);CHKERRQ(ierr);
		ierr = SNESDestroy(&snes);CHKERRQ(ierr);
	}
#endif
	
	/* output initial condition */
	if ( (tk%user->output_frequency==0) || (tk==0) ) {
		char *prefix;
		
		asprintf(&prefix,"step%1.5d",tk);
		ierr = pTatin2d_ModelOutput(user,X,prefix);CHKERRQ(ierr);
		free(prefix);
	}
	
	
	/* do time stepping */

	for (tk=1; tk<=user->nsteps; tk++) {
		const PetscInt rk_order = 4;
		Vec gcoords,Xv,Xp;
		Vec *stage_coords; //[rk_order];
		Vec *stage_X; //[rk_order];
		PetscScalar DT;
		DM dav,dap;

		
		/* compute time step */
		{			
			Vec velocity,pressure;
			PetscReal dt_length;
			
			ierr = DMCompositeGetAccess(user->pack,X,&velocity,&pressure);CHKERRQ(ierr);
			
			ierr = SwarmUpdatePosition_ComputeCourantStep_3(user->dav,velocity,&dt_length);CHKERRQ(ierr);
			ierr = SwarmUpdatePosition_ComputeCourantStep(user->dav,velocity,&dt_length);CHKERRQ(ierr);
			ierr = SwarmUpdatePosition_ComputeCourantStep_2(user->dav,velocity,&dt_length);CHKERRQ(ierr);
			
			ierr = DMCompositeRestoreAccess(user->pack,X,&velocity,&pressure);CHKERRQ(ierr);
			user->dt = (1.0/2.0) * dt_length;
		}
		ierr = pTatin2d_ComputeTimestep(user,user->pack,X);CHKERRQ(ierr);
		DT = user->dt;
		
    PetscPrintf(PETSC_COMM_WORLD,"step = %1.5d ... using dt = %1.4e \n", tk, user->dt );

		
		/* BEGIN TIME STEPPING */
		//ierr = RK7_DormandPrince(user->time,user->dt,X,user,multipys_pack,A,B,F);CHKERRQ(ierr);
		
		
		// RK4
		ierr = DMCompositeGetEntries(multipys_pack,&dav,&dap);CHKERRQ(ierr);
		ierr = DMDAGetCoordinates(dav,&gcoords);CHKERRQ(ierr);
		ierr = VecDuplicateVecs(gcoords,rk_order,&stage_coords);CHKERRQ(ierr);
		ierr = VecDuplicateVecs(X,rk_order,&stage_X);CHKERRQ(ierr);
		
		ierr = VecCopy(gcoords,stage_coords[0]);CHKERRQ(ierr);
		
		
		/* STAGE 1 */
		PetscPrintf(PETSC_COMM_WORLD,"  # RK4 (stage 1)\n");
		/* use v(t^n,x^n) from remeshed domain - or re-solve ? */
		if (tk != 1) {
			// SOLVE
			ierr = TimeUpdateComputeRHS(user,multipys_pack,A,B,F,X);CHKERRQ(ierr);
		}
		ierr = VecCopy(X,stage_X[0]);CHKERRQ(ierr);
		
		ierr = VecCopy(stage_coords[0],stage_coords[1]);CHKERRQ(ierr); // x1 = x0		
		ierr = DMCompositeGetAccess(multipys_pack,X,&Xv,&Xp);CHKERRQ(ierr);
		ierr = VecAXPY(stage_coords[1],0.5*DT,Xv);CHKERRQ(ierr); // x1 = x0 + 0.5 * dt * v
		ierr = DMCompositeRestoreAccess(multipys_pack,X,&Xv,&Xp);CHKERRQ(ierr);
		
		
		/* STAGE 2 */
		PetscPrintf(PETSC_COMM_WORLD,"  # RK4 (stage 2)\n");
		ierr = DMDAGetCoordinates(dav,&gcoords);CHKERRQ(ierr);
		ierr = VecCopy(stage_coords[1],gcoords);CHKERRQ(ierr);
		
		/* push time */
    user->time = user->time + 0.5 * DT;
		/* push updated coords 1 into DMDA */
		ierr = DMDAGetCoordinates(dav,&gcoords);CHKERRQ(ierr);
		ierr = VecCopy(stage_coords[1],gcoords);CHKERRQ(ierr);
		ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
		
		ierr = TimeUpdateComputeRHS(user,multipys_pack,A,B,F,X);CHKERRQ(ierr);
		ierr = VecCopy(X,stage_X[1]);CHKERRQ(ierr);
		
		ierr = VecCopy(stage_coords[0],stage_coords[2]);CHKERRQ(ierr); // x2 = x0
		ierr = DMCompositeGetAccess(multipys_pack,X,&Xv,&Xp);CHKERRQ(ierr);
		ierr = VecAXPY(stage_coords[2],0.5*DT,Xv);CHKERRQ(ierr); // x2 = x0 + 0.5 * dt * v
		ierr = DMCompositeRestoreAccess(multipys_pack,X,&Xv,&Xp);CHKERRQ(ierr);
		
		// STAGE 3
		PetscPrintf(PETSC_COMM_WORLD,"  # RK4 (stage 3)\n");
		/* push updated coords 1 into DMDA */
		ierr = DMDAGetCoordinates(dav,&gcoords);CHKERRQ(ierr);
		ierr = VecCopy(stage_coords[2],gcoords);CHKERRQ(ierr);
		ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
		
		ierr = TimeUpdateComputeRHS(user,multipys_pack,A,B,F,X);CHKERRQ(ierr);
		ierr = VecCopy(X,stage_X[2]);CHKERRQ(ierr);
		
		ierr = VecCopy(stage_coords[0],stage_coords[3]);CHKERRQ(ierr); // x3 = x0
		ierr = DMCompositeGetAccess(multipys_pack,X,&Xv,&Xp);CHKERRQ(ierr);
		ierr = VecAXPY(stage_coords[3],DT,Xv);CHKERRQ(ierr); // x3 = x0 + dt * v
		ierr = DMCompositeRestoreAccess(multipys_pack,X,&Xv,&Xp);CHKERRQ(ierr);
		

		// STAGE 4
		PetscPrintf(PETSC_COMM_WORLD,"  # RK4 (stage 4)\n");
		/* push time */
    user->time = user->time + 0.5 * DT;
		/* push updated coords 1 into DMDA */
		ierr = DMDAGetCoordinates(dav,&gcoords);CHKERRQ(ierr);
		ierr = VecCopy(stage_coords[3],gcoords);CHKERRQ(ierr);
		ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
		
		ierr = TimeUpdateComputeRHS(user,multipys_pack,A,B,F,X);CHKERRQ(ierr);
		ierr = VecCopy(X,stage_X[3]);CHKERRQ(ierr);
		
		ierr = DMDAGetCoordinates(dav,&gcoords);CHKERRQ(ierr);
		ierr = VecCopy(stage_coords[0],gcoords);CHKERRQ(ierr); // xn = x0

		// 0
		ierr = DMCompositeGetAccess(multipys_pack,stage_X[0],&Xv,&Xp);CHKERRQ(ierr);
		ierr = VecAXPY(gcoords,DT/6.0,Xv);CHKERRQ(ierr); // xn = xn + dt/6 ( f0 )
		ierr = DMCompositeRestoreAccess(multipys_pack,X,&Xv,&Xp);CHKERRQ(ierr);
		// 1
		ierr = DMCompositeGetAccess(multipys_pack,stage_X[1],&Xv,&Xp);CHKERRQ(ierr);
		ierr = VecAXPY(gcoords,2.0*DT/6.0,Xv);CHKERRQ(ierr); // xn = xn + 2.0*dt/6 ( f1 )
		ierr = DMCompositeRestoreAccess(multipys_pack,X,&Xv,&Xp);CHKERRQ(ierr);
		// 2
		ierr = DMCompositeGetAccess(multipys_pack,stage_X[2],&Xv,&Xp);CHKERRQ(ierr);
		ierr = VecAXPY(gcoords,2.0*DT/6.0,Xv);CHKERRQ(ierr); // xn = xn + 2.0*dt/6 ( f2 )
		ierr = DMCompositeRestoreAccess(multipys_pack,X,&Xv,&Xp);CHKERRQ(ierr);
		// 3
		ierr = DMCompositeGetAccess(multipys_pack,stage_X[3],&Xv,&Xp);CHKERRQ(ierr);
		ierr = VecAXPY(gcoords,1.0*DT/6.0,Xv);CHKERRQ(ierr); // xn = xn + 1.0*dt/6 ( f3 )
		ierr = DMCompositeRestoreAccess(multipys_pack,X,&Xv,&Xp);CHKERRQ(ierr);

		// update V
		ierr = VecCopy(stage_X[0],X);CHKERRQ(ierr);
		ierr = VecAXPY(X,2.0,stage_X[1]);CHKERRQ(ierr);
		ierr = VecAXPY(X,2.0,stage_X[2]);CHKERRQ(ierr);
		ierr = VecAXPY(X,1.0,stage_X[3]);CHKERRQ(ierr);
		ierr = VecScale(X,1.0/6.0);CHKERRQ(ierr);
		
		/* push updated coords 1 into DMDA */
		ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
		
		
		ierr = VecDestroyVecs( rk_order, &stage_X );CHKERRQ(ierr);
		
		
		// RK2
#if 0
		/* NEW RK STYLE ============================================================== */		
		ierr = DMCompositeGetEntries(multipys_pack,&dav,&dap);CHKERRQ(ierr);
		ierr = DMDAGetCoordinates(dav,&gcoords);CHKERRQ(ierr);
		ierr = VecDuplicateVecs(gcoords,rk_order,&stage_coords);CHKERRQ(ierr);
		
		ierr = VecCopy(gcoords,stage_coords[0]);CHKERRQ(ierr);

		
		/* STAGE 1 */
		/* use v(t^n,x^n) from remeshed domain - or re-solve ? */
		if (tk != 1) {
			// SOLVE
			ierr = TimeUpdateComputeRHS(user,multipys_pack,A,B,F,X);CHKERRQ(ierr);
		}

		ierr = VecCopy(stage_coords[0],stage_coords[1]);CHKERRQ(ierr); // x1 = x0
		
		ierr = DMCompositeGetAccess(multipys_pack,X,&Xv,&Xp);CHKERRQ(ierr);
		ierr = VecAXPY(stage_coords[1],0.5*DT,Xv);CHKERRQ(ierr); // x1 = x0 + 0.5 * dt * v
		ierr = DMCompositeRestoreAccess(multipys_pack,X,&Xv,&Xp);CHKERRQ(ierr);
		
		
		/* STAGE 2 */
		ierr = DMDAGetCoordinates(dav,&gcoords);CHKERRQ(ierr);
		ierr = VecCopy(stage_coords[1],gcoords);CHKERRQ(ierr);

		/* push time */
    user->time = user->time + 0.5 * DT;
		/* push updated coords 1 into DMDA */
		ierr = DMDAGetCoordinates(dav,&gcoords);CHKERRQ(ierr);
		ierr = VecCopy(stage_coords[1],gcoords);CHKERRQ(ierr);
		ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);

		ierr = TimeUpdateComputeRHS(user,multipys_pack,A,B,F,X);CHKERRQ(ierr);

		ierr = VecCopy(stage_coords[0],stage_coords[1]);CHKERRQ(ierr); // x1 = x0
		ierr = DMCompositeGetAccess(multipys_pack,X,&Xv,&Xp);CHKERRQ(ierr);
		ierr = VecAXPY(stage_coords[1],DT,Xv);CHKERRQ(ierr); // x1 = x0 + dt * v
		ierr = DMCompositeRestoreAccess(multipys_pack,X,&Xv,&Xp);CHKERRQ(ierr);
		
		
		/* push time */
    user->time = user->time + 0.5 * DT;
		/* push updated coords 1 into DMDA */
		ierr = DMDAGetCoordinates(dav,&gcoords);CHKERRQ(ierr);
		ierr = VecCopy(stage_coords[1],gcoords);CHKERRQ(ierr);
		ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
		/* NEW RK STYLE ============================================================== */		
#endif		
		
		
// OLD RK2		
#if 0		
		/* STAGE 1 */
		ierr = VecCopy(stage_coords[0],stage_coords[1]);CHKERRQ(ierr); // x1 = x0
		
		ierr = DMCompositeGetAccess(multipys_pack,X,&Xv,&Xp);CHKERRQ(ierr);
		ierr = VecAXPY(stage_coords[1],0.5*DT,Xv);CHKERRQ(ierr); // x1 = x0 + 0.5 * dt * v
		ierr = DMCompositeRestoreAccess(multipys_pack,X,&Xv,&Xp);CHKERRQ(ierr);
		
		/* STAGE 2 */
		/* push time */
		time = time + 0.5 * DT;
    user->time = time;
		/* push updated coords 1 into DMDA */
		ierr = DMDAGetCoordinates(dav,&gcoords);CHKERRQ(ierr);
		ierr = VecCopy(stage_coords[1],gcoords);CHKERRQ(ierr);
		ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
		
		/* VELOCITY AT t^1/2, x^1/2 */
		{
			/* boundary conditions */
			ierr = pTatin2d_ModelApplyBoundaryCondition(user);CHKERRQ(ierr);
			ierr = pTatin2d_ModelApplyMaterialBoundaryCondition(user);CHKERRQ(ierr);
			
			/* creation */
			ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
			
			ierr = SNESSetFunction(snes,F,FormFunction_Stokes,user);CHKERRQ(ierr);  
			if (user->rheology_constants.rheology_type == RHEOLOGY_VISCO_ELASTIC_PLASTIC ) {
				ierr = SNESSetFunction(snes,F,FormFunction_Stokes_ViscoElastoPlastic,user);CHKERRQ(ierr);  
			}
			
			if (user->use_mf_stokes) {
				ierr = SNESSetJacobian(snes,B,B,FormJacobian_Stokes,user);CHKERRQ(ierr);
			} else {
				ierr = SNESSetJacobian(snes,A,B,FormJacobian_Stokes,user);CHKERRQ(ierr);
			}
			ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
			
			/* configure for fieldsplit */
			ierr = SNESGetKSP(snes,&ksp);CHKERRQ(ierr);
			ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
			
			ierr = DMCompositeGetGlobalISs(multipys_pack,&isg);CHKERRQ(ierr);
			ierr = PCFieldSplitSetIS(pc,"u",isg[0]);CHKERRQ(ierr);
			ierr = PCFieldSplitSetIS(pc,"p",isg[1]);CHKERRQ(ierr);
			
			ierr = pTatinSetSNESMonitor(snes,user);CHKERRQ(ierr);
			
			ierr = SNESSolve(snes,PETSC_NULL,X);CHKERRQ(ierr);
			
			/* statistics */
			if (user->solverstatistics) {
				PetscInt its,lits,nfuncevals;
				ierr = SNESGetIterationNumber(snes,&its);CHKERRQ(ierr);
				ierr = SNESGetLinearSolveIterations(snes,&lits);CHKERRQ(ierr);
				ierr = SNESGetNumberFunctionEvals(snes,&nfuncevals);CHKERRQ(ierr);
				PetscPrintf(PETSC_COMM_WORLD,"SNES(Stokes) Statistics \n");
				PetscPrintf(PETSC_COMM_WORLD,"  nonlinear its    = %D \n", its);
				PetscPrintf(PETSC_COMM_WORLD,"  total linear its = %D \n", lits);
				PetscPrintf(PETSC_COMM_WORLD,"  total func evals = %D \n", nfuncevals);
				
				pTatin2dSolverStatistics(user,snes,X);
			}
			
			/* tidy up */
			ierr = ISDestroy(&isg[0]);CHKERRQ(ierr);
			ierr = ISDestroy(&isg[1]);CHKERRQ(ierr);
			ierr = PetscFree(isg);CHKERRQ(ierr);
			ierr = SNESDestroy(&snes);CHKERRQ(ierr);
		}

		ierr = VecCopy(stage_coords[0],stage_coords[1]);CHKERRQ(ierr); // x1 = x0
		ierr = DMCompositeGetAccess(multipys_pack,X,&Xv,&Xp);CHKERRQ(ierr);
		ierr = VecAXPY(stage_coords[1],DT,Xv);CHKERRQ(ierr); // x1 = x0 + dt * v
		ierr = DMCompositeRestoreAccess(multipys_pack,X,&Xv,&Xp);CHKERRQ(ierr);
		
		/* push time */
		time = time + 0.5 * DT;
    user->time = time;
		/* push updated coords 1 into DMDA */
		ierr = DMDAGetCoordinates(dav,&gcoords);CHKERRQ(ierr);
		ierr = VecCopy(stage_coords[1],gcoords);CHKERRQ(ierr);
		ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
#endif
		
		
		
		
		
		/* ADVECT MARKERS */
		ierr = MaterialPointCoordinateSetUp(user->db,user->dav);CHKERRQ(ierr);
		
		/* reset mesh */
		ierr = DMDAGetCoordinates(dav,&gcoords);CHKERRQ(ierr);
		ierr = VecCopy(stage_coords[0],gcoords);CHKERRQ(ierr);
		ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
		
		ierr = VecDestroyVecs( rk_order, &stage_coords );CHKERRQ(ierr);
			
		/* END TIME STEPPING */
		
		
		
		
		
		
		
		
		
		
		
		
		/* UPDATE MESH - advect + remesh */
		ierr = pTatin2d_ModelUpdateMeshGeometry(user,X);CHKERRQ(ierr);
    ierr = QuadratureStokesCoordinateSetUp(user->Q,user->dav);CHKERRQ(ierr);
		for (e=0; e<QUAD_EDGES; e++) {
			ierr = SurfaceQuadratureStokesGeometrySetUp(user->surfQ[e],user->dav);CHKERRQ(ierr);
		}
		
		/* UPDATE local coordinates */
		{
			int p,npoints;
			MPntStd *mp_std;
			DataField PField;
			double tolerance;
			int max_its;
			Truth use_nonzero_guess, monitor, log;
			DM cda;
			Vec gcoords;
			PetscScalar *LA_gcoords;
			const PetscInt *elnidx_u;
			PetscInt nel,nen_u;
			PetscInt *gidx;
			PetscInt lmx,lmy;
			
			/* get marker fields */
			DataBucketGetSizes(user->db,&npoints,PETSC_NULL,PETSC_NULL);
			DataBucketGetDataFieldByName(user->db, MPntStd_classname ,&PField);
			mp_std = PField->data;
			
			/* setup for coords */
			ierr = DMDAGetCoordinateDA(user->dav,&cda);CHKERRQ(ierr);
			ierr = DMDAGetGhostedCoordinates(user->dav,&gcoords);CHKERRQ(ierr);
			ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
			
			ierr = DMDAGetGlobalIndices(user->dav,0,&gidx);CHKERRQ(ierr);
			ierr = DMDAGetElements_pTatin(user->dav,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
			
			ierr = DMDAGetLocalSizeElementQ2(user->dav,&lmx,&lmy,PETSC_NULL);CHKERRQ(ierr);
			
			/* point location parameters */
			tolerance         = 1.0e-10;
			max_its           = 10;
			use_nonzero_guess = _TRUE;
			monitor           = _FALSE;
			log               = _FALSE;
			
			InverseMappingDomain_2dQ2( 		 tolerance, max_its,
																use_nonzero_guess, 
																monitor, log,
																(const double*)LA_gcoords, (const int)lmx,(const int)lmy, (const int*)elnidx_u,
																npoints, mp_std );
			
			ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
			
		}
		
		ierr = SwarmUpdatePosition_Communication_Generic(user->dav,user->db,user->ex);CHKERRQ(ierr);
		
		/* remove all points which didn't find a home */
		{
			int p,npoints,escaped;
			MPntStd *mp_std;
			DataField PField;
			PetscMPIInt rank;
			
			MPI_Comm_rank(((PetscObject)user->dav)->comm,&rank);
			
			/* get marker fields */
			DataBucketGetSizes(user->db,&npoints,PETSC_NULL,PETSC_NULL);
			DataBucketGetDataFieldByName(user->db, MPntStd_classname ,&PField);
			mp_std = PField->data;
			
			escaped = 0;
			for (p=0; p<npoints; p++) {
				if (mp_std[p].wil == -1) {
					escaped++;
				}
			}
			//PetscPrintf(PETSC_COMM_SELF,"[%d] MPntStd: npoints = %d : found_locally = %d \n", rank, npoints, npoints-escaped );
			
			/* remove points which left processor */
			if (escaped!=0) {
				PetscPrintf(PETSC_COMM_SELF,"  *** MPntStd removal: Identified %d points which are not contained on subdomain (after communication) \n", escaped );
			}
			if (escaped!=0) {
				for (p=0; p<npoints; p++) {
					if (mp_std[p].wil == -1) {
						printf("############################ Point %d not located in domain (%1.6e , %1.6e) ############################# \n",p,mp_std[p].coor[0],mp_std[p].coor[1]);
						
						/* kill point */
						DataBucketRemovePointAtIndex(user->db,p);
						DataBucketGetSizes(user->db,&npoints,0,0); /* you need to update npoints as the list size decreases! */
						p--; /* check replacement point */
					}
				}
			}
			
		}
		
		/* pop control */
		//ierr = MPPC_AVDPatch(7,100,1, 10,10, 0.0,user->dav,user->db);CHKERRQ(ierr);
		ierr = MaterialPointPopulationControl(user);CHKERRQ(ierr);
		

    user->step = tk;
		
		/* output */
		if (tk%user->output_frequency==0) {
			char *prefix;
			
			asprintf(&prefix,"step%1.5d",tk);
			ierr = pTatin2d_ModelOutput(user,X,prefix);CHKERRQ(ierr);
			free(prefix);
		}
	}
	
	
	if (Auu) { ierr = MatDestroy(&Auu);CHKERRQ(ierr); }
	if (Aup) { ierr = MatDestroy(&Aup);CHKERRQ(ierr); }
	if (Apu) { ierr = MatDestroy(&Apu);CHKERRQ(ierr); }
	if (Spp) { ierr = MatDestroy(&Spp);CHKERRQ(ierr); }
	
	if (!user->use_mf_stokes) {
		ierr = MatDestroy(&A);CHKERRQ(ierr);
	}
  ierr = MatDestroy(&B);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
	
	ierr = pTatin2dDestroyContext(&user);
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc,char **argv)
{
	PetscErrorCode ierr;
	
	ierr = PetscInitialize(&argc,&argv,0,help);CHKERRQ(ierr);
	
	ierr = pTatinWriteOptionsFile(PETSC_NULL);CHKERRQ(ierr);
	
	ierr = pTatin2d_material_points_std(argc,argv);CHKERRQ(ierr);
	
	ierr = PetscFinalize();CHKERRQ(ierr);
	return 0;
}
