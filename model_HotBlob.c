/*
 
 Model Description:

 
 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"




/* ================= HotBlob ================= */

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelOutput_HotBlob"
PetscErrorCode pTatin2d_ModelOutput_HotBlob(pTatinCtx ctx,Vec X,const char prefix[])
{
	char *pvdfilename;
	PetscBool active;
	PetscErrorCode ierr;

	
	if (prefix == PETSC_NULL) {
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,"qpoints");CHKERRQ(ierr);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,"vp");CHKERRQ(ierr);

		ierr = SwarmOutputParaView_MPntPStokes(ctx->db,ctx->outputpath,"mpoints_stk");CHKERRQ(ierr);
	} else { /* exanple of adding a different prefix to the file names - not sure this is useful */
		char *name;
		
		asprintf(&name,"%s_qpoints",prefix);
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);

		asprintf(&name,"%s_vp",prefix);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);

		//////////////
		asprintf(&pvdfilename,"%s/timeseries_mpoints_stk.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;

			asprintf(&vtkfilename, "%s_mpoints_stk.pvtu",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			free(vtkfilename);
		}
		free(pvdfilename);
		
		/////////
		asprintf(&pvdfilename,"%s/timeseries_mpoints_thm.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;
			
			asprintf(&vtkfilename, "%s_mpoints_thm.pvtu",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			free(vtkfilename);
		}
		free(pvdfilename);
		/////////
		
		asprintf(&name,"%s_mpoints_stk",prefix);
		ierr = SwarmOutputParaView_MPntPStokes(ctx->db,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
		
		asprintf(&name,"%s_mpoints_thm",prefix);
		ierr = SwarmOutputParaView_MPntPThermal(ctx->db,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
	}
	
	ierr = pTatinPhysCompActivated(ctx,PhysComp_Energy,&active);CHKERRQ(ierr);
	if (active) {
		char *name;
		DM da;
		Vec T;
		PhysCompEnergyCtx phys_energy;
		
		phys_energy = ctx->phys_energy;
		da          = phys_energy->daT;
		T           = phys_energy->T;
		
		/* old shit, slow way */
		/*
		if (!prefix) {
			asprintf(&name,"%s/Tfield.vtk",ctx->outputpath); 
		} else {
			asprintf(&name,"%s/%s_Tfield.vtk",ctx->outputpath,prefix);
		}
		ierr = PhysCompView_EnergyEquation(phys_energy,T,name);CHKERRQ(ierr);
		free(name);
		*/
		
		/* new xml way */
		/*
		if (!prefix) {
			asprintf(&name,"%s/Tfield.vts",ctx->outputpath); 
		} else {
			asprintf(&name,"%s/%s_Tfield.vts",ctx->outputpath,prefix);
		}
		ierr = pTatinOutputQ1Mesh2dNodalFieldVTS(phys_energy->daT,1,T,name,"temperature");CHKERRQ(ierr);
		free(name);
		*/
		
		/* temperature */
		if (prefix == PETSC_NULL) {
			ierr = pTatinOutputParaViewMeshTemperature(ctx,T,ctx->outputpath,"Tfield");CHKERRQ(ierr);
		}
		else {
			asprintf(&name,"%s_Tfield",prefix);
			ierr = pTatinOutputParaViewMeshTemperature(ctx,T,ctx->outputpath,name);CHKERRQ(ierr);
			free(name);
		}
		
		/* quad props */
		if (prefix == PETSC_NULL) {
			asprintf(&name,"%Tcell");
		}
		else {
			asprintf(&name,"%s_Tcell",prefix);
		}
		ierr = pTatinParaViewOutputQuadratureThermalAveragedFields(ctx,ctx->outputpath,name);
		free(name);
		
		asprintf(&pvdfilename,"%s/timeseries_Tfield.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;
			
			asprintf(&vtkfilename, "%s_Tfield.pvts",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			free(vtkfilename);
		}
		free(pvdfilename);
	}	
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_HotBlob"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_HotBlob(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;

	ierr = DMDASetUniformCoordinates(ctx->dav,-1.0,1.0, -1.0,1.0, 0.0,0.0);CHKERRQ(ierr);
	
	/* check for energy solver */
	{
		PetscBool active = PETSC_FALSE;
		ierr = pTatinPhysCompActivated(ctx,PhysComp_Energy,&active);CHKERRQ(ierr);
		
		if (active) {
			DM da;
			Vec T;
			PhysCompEnergyCtx phys_energy;
			
			phys_energy = ctx->phys_energy;
			da          = phys_energy->daT;
			T           = phys_energy->T;
			
			ierr = DMDAProjectCoordinatesQ2toQ1_2d(ctx->dav,da);CHKERRQ(ierr);
		}
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_HotBlob"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_HotBlob(pTatinCtx ctx)
{
	PetscInt               e,ncells,n_mp_points;
	PetscInt               p,ngp;
	GaussPointCoefficientsStokes *gausspoints;
	PetscReal              opts_eta0,opts_eta1,opts_xc;
	PetscInt               opts_nz;
	DataBucket             db;
	DataField              PField_std,PField_stokes;
	double                 eta,rho;
	int                    phase_init,phase;
	PetscErrorCode         ierr;
	
	PetscFunctionBegin;
	
	ncells = ctx->Q->ncells;
	ngp    = ctx->Q->ngp;

	ctx->rheology_constants.rheology_type = RHEOLOGY_VISCOUS;
	{
		PetscBool active = PETSC_FALSE;
		ierr = pTatinPhysCompActivated(ctx,PhysComp_Energy,&active);CHKERRQ(ierr);
		
		if (active) {
				ctx->rheology_constants.rheology_type = RHEOLOGY_VISCOUS;
//			ctx->rheology_constants.rheology_type = RHEOLOGY_VISCO_PLASTIC_T;
		}
	}
	
	ctx->rheology_constants.const_eta0[0] = 1.0;
	ctx->rheology_constants.const_eta0[1] = 1.0e4;
	
	ctx->rheology_constants.const_rho0[0] = 1.0e2;
	ctx->rheology_constants.const_rho0[1] = 4.0e2;
	
	
	/* define properties on material points */
	db = ctx->db;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));

	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));
	
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	for (p=0; p<n_mp_points; p++) {
		MPntStd     *material_point;
		MPntPStokes *mpprop_stokes;
		double  *position;
		
		DataFieldAccessPoint(PField_std,p,(void**)&material_point);
		DataFieldAccessPoint(PField_stokes,p,(void**)&mpprop_stokes);

		
		position = material_point->coor;

		MPntStdGetField_phase_index(material_point,&phase_init);

		/* outside ball */
		MPntStdSetField_phase_index(material_point,0);

		MPntPStokesSetField_eta_effective(mpprop_stokes, ctx->rheology_constants.const_eta0[0] );
		MPntPStokesSetField_density(mpprop_stokes, ctx->rheology_constants.const_rho0[0] );

		/* inside ball */
		if (sqrt(position[0]*position[0] + position[1]*position[1]) < 0.2) {
			MPntStdSetField_phase_index(material_point,1);

			MPntPStokesSetField_eta_effective(mpprop_stokes, ctx->rheology_constants.const_eta0[1] );
			MPntPStokesSetField_density(mpprop_stokes, ctx->rheology_constants.const_rho0[1] );
		}

	}

	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokes);
	
	
	/* material point configuration */
	{
		PetscBool active = PETSC_FALSE;
		
		ierr = pTatinPhysCompActivated(ctx,PhysComp_Energy,&active);CHKERRQ(ierr);
		
		if (active) {
			DataField PField_thermal;
			
			/* standard data */
			DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
			DataFieldGetAccess(PField_std);
			DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
			/* thermal data */
			DataBucketGetDataFieldByName(db,MPntPThermal_classname,&PField_thermal);
			DataFieldGetAccess(PField_thermal);
			DataFieldVerifyAccess(PField_thermal,sizeof(MPntPThermal));
			
			
			DataBucketGetSizes(db,&n_mp_points,0,0);
			
			for (p=0; p<n_mp_points; p++) {
				MPntStd      *material_point;
				MPntPThermal *material_point_thermal;
				double       *position;
				
				DataFieldAccessPoint(PField_std,    p,(void**)&material_point);
				DataFieldAccessPoint(PField_thermal,p,(void**)&material_point_thermal);
				
				position = material_point->coor;

				MPntPThermalSetField_heat_source(material_point_thermal,0.0);

				MPntPThermalSetField_diffusivity(material_point_thermal,1.0e-6);

			}
			DataFieldRestoreAccess(PField_std);
			DataFieldRestoreAccess(PField_thermal);
		}
	}
	
	
	
	/* initial condition - yuck */
	/* check for energy solver */
	{
		PetscBool active = PETSC_FALSE;
		ierr = pTatinPhysCompActivated(ctx,PhysComp_Energy,&active);CHKERRQ(ierr);
		
		if (active) {
			DM da;
			Vec T;
			PhysCompEnergyCtx phys_energy;
			PetscInt i,j,si,sj,nx,ny;
			Vec coords;
			DMDACoor2d **LA_coords;
			DM cda;
			PetscScalar **LA_T;
			
			phys_energy = ctx->phys_energy;
			da          = phys_energy->daT;
			T           = phys_energy->T;

			ierr = DMDAGetCoordinates(da,&coords);CHKERRQ(ierr);
			ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
			ierr = DMDAVecGetArray(cda,coords,&LA_coords);CHKERRQ(ierr);

			ierr = DMDAVecGetArray(da,T,&LA_T);CHKERRQ(ierr);
			
			ierr = DMDAGetCorners(da,&si,&sj,0 , &nx,&ny,0);CHKERRQ(ierr);
			for( j=sj; j<sj+ny; j++ ) {
				for( i=si; i<si+nx; i++ ) {
					PetscScalar xn,yn,Tic;
					
					xn = LA_coords[j][i].x;
					yn = LA_coords[j][i].y;
					
					// 1
					Tic = exp( -( xn*xn + yn*yn )/0.05 );
					LA_T[j][i] = 50.0 * Tic;
				}
			}

			ierr = DMDAVecRestoreArray(da,T,&LA_T);CHKERRQ(ierr);
			ierr = DMDAVecRestoreArray(cda,coords,&LA_coords);CHKERRQ(ierr);
		}
	}
	
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_HotBlob"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_HotBlob(pTatinCtx ctx)
{
	BCList         ubclist;
	PetscScalar    bcval;
	DM             dav;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ubclist = ctx->u_bclist;
	dav     = ctx->dav;

	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	
	/* check for energy solver */
	{
		PetscBool active = PETSC_FALSE;
		ierr = pTatinPhysCompActivated(ctx,PhysComp_Energy,&active);CHKERRQ(ierr);
		
		if (active) {
			DM da;
			Vec T;
			PhysCompEnergyCtx phys_energy;
			
			phys_energy = ctx->phys_energy;
			da          = phys_energy->daT;
			T           = phys_energy->T;
			
			bcval = 0.0;       ierr = DMDABCListTraverse(phys_energy->T_bclist,da,DMDABCList_JMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(phys_energy->T_bclist,da,DMDABCList_JMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
		}
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelUpdateMeshGeometry_HotBlob"
PetscErrorCode pTatin2d_ModelUpdateMeshGeometry_HotBlob(pTatinCtx ctx,Vec X)
{
	PetscReal step;
	Vec velocity,pressure;
	DM dav,dap;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	step = ctx->dt;
  
	ierr = DMCompositeGetEntries(ctx->pack,&dav,&dap);CHKERRQ(ierr);

	/* snappy bilinearize mesh */
	ierr = UpdateMeshGeometry_BilinearizeQ2Elements(dav);CHKERRQ(ierr);
	
	/* check for energy solver - copy velocity coordinates */
	{
		PetscBool active = PETSC_FALSE;
		ierr = pTatinPhysCompActivated(ctx,PhysComp_Energy,&active);CHKERRQ(ierr);
		
		if (active) {
			DM da;
			Vec T;
			PhysCompEnergyCtx phys_energy;
			
			phys_energy = ctx->phys_energy;
			da          = phys_energy->daT;
			T           = phys_energy->T;
			
			ierr = DMDAProjectCoordinatesQ2toQ1_2d(ctx->dav,da);CHKERRQ(ierr);
		}
	}
	
	PetscFunctionReturn(0);
}


