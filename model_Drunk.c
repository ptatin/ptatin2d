/*
 
 Model Description:

 Model described on page 2 of the "Drunken seaman" paper by Kaus, Muhlhaus & May
 
 
 Input / command line parameters:
 -dnksm_eta0 : viscosity in lower layer (default = 1.0)
 -dnksm_rho0 : density in the lower layer (default = 1.0)
 -dnksm_eta1 : viscosity in upper layer (default = 10.0)
 -dnksm_rho1 : density in the upper layer (default = 2.0)
 
 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"

/* ================= DrunkSeaman ================= */

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelOutput_DrunkSeaman"
PetscErrorCode pTatin2d_ModelOutput_DrunkSeaman(pTatinCtx ctx,Vec X,const char prefix[])
{
	char *pvdfilename;
	char *vtkfilename;
	char *filename;
	PetscErrorCode ierr;
	PetscInt jj,i,j,si,sj,m,n;
	DM cda,da;
	Vec coords,gcoords;
	DMDACoor2d **LA_coords;
	PetscReal scaled_amp, amp, dy_init;
	
	
	/* write out the corner */
	da = ctx->dav;
	ierr = DMDAGetCorners(da,&si,&sj,0,&m,&n,0);CHKERRQ(ierr);
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(da,&coords);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,coords,&LA_coords);CHKERRQ(ierr);

	dy_init = 5.0/(ctx->my);
	jj = ( -1.0 - (-5.0) )/dy_init;
	jj = 2 * jj; /* q2 elements */
	if ( jj>=sj && jj<sj+n ) {
		if (si==0) {
			PetscPrintf(PETSC_COMM_WORLD,"[[INT_LOG]] interface y = %1.10lf time = %1.6lf \n", LA_coords[jj][0].y, ctx->time );
		}
	}
	
	ierr = DMDAVecRestoreArray(cda,coords,&LA_coords);CHKERRQ(ierr);
	
	
	
	if (prefix == PETSC_NULL) {
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,"qpoints");CHKERRQ(ierr);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,"vp");CHKERRQ(ierr);
		ierr = SwarmOutputParaView_MPntPStokes(ctx->db,ctx->outputpath,"mpoints_stk");CHKERRQ(ierr);
	} else { /* exanple of adding a different prefix to the file names - not sure this is useful */
		char *name;
		
		// PVD
		asprintf(&pvdfilename,"%s/timeseries_qpoints.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		asprintf(&vtkfilename, "%s_qpoints.pvtu",prefix);
		ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
		free(vtkfilename);
		free(pvdfilename);
		// PVTU
		asprintf(&name,"%s_qpoints",prefix);
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
	
		
		// PVD
		asprintf(&pvdfilename,"%s/timeseries_vp.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		asprintf(&vtkfilename, "%s_vp.pvts",prefix);
		ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
		free(vtkfilename);
		free(pvdfilename);
		// PVTS
		asprintf(&name,"%s_vp",prefix);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
		

		// PVD
		asprintf(&pvdfilename,"%s/timeseries_mpoints_stk.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		asprintf(&vtkfilename, "%s_mpoints_stk.pvtu",prefix);
		ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
		free(vtkfilename);
		free(pvdfilename);
		// PVTU
		asprintf(&name,"%s_mpoints_stk",prefix);
		ierr = SwarmOutputParaView_MPntPStokesPl(ctx->db,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
		
	
	}
	
	
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_DrunkSeamanDeformed"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_DrunkSeamanDeformed(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	PetscInt i,j,si,sj,m,n;
	DM cda,da;
	Vec coords,gcoords;
	DMDACoor2d **LA_coords;
	PetscReal scaled_amp, amp;
	
	
	PetscFunctionBegin;
	
	ierr = DMDASetUniformCoordinates(ctx->dav,-2.5,2.5, -5.0,0.0, 0.0,0.0);CHKERRQ(ierr);
	
	/* define intially deformed mesh */
	if (ctx->my<5) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Elements in y must be a >= 5");
	}
	if (ctx->my%5!=0) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Elements in y must be a multiple of 5");
	}
	
	da = ctx->dav;
	ierr = DMDAGetCorners(da,&si,&sj,0,&m,&n,0);CHKERRQ(ierr);
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(da,&coords);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,coords,&LA_coords);CHKERRQ(ierr);

	amp = 5.0/100;
	for (j=sj; j<sj+n; j++) {
		for (i=si; i<si+m; i++) {
			PetscScalar pos[2];
			
			pos[0] = LA_coords[j][i].x;
			pos[1] = LA_coords[j][i].y;

			//if (i==0) printf("y = %lf \n", pos[1]);
			if (LA_coords[j][i].y <= -1.0) { /* below */
				scaled_amp = amp * ( LA_coords[j][i].y + 5.0 )/4.0;
				LA_coords[j][i].y = LA_coords[j][i].y + scaled_amp * cos( 2.0 * M_PI * pos[0] / 5.0 );
				//if (i==0) printf("  j=%d y'' = %lf \n", j, LA_coords[j][i].y);
			} else { /* above */
				scaled_amp = amp * ( -LA_coords[j][i].y)/1.0;
				LA_coords[j][i].y = LA_coords[j][i].y + scaled_amp * cos( 2.0 * M_PI * pos[0] / 5.0 );
			}
			
		}
	}
	ierr = DMDAVecRestoreArray(cda,coords,&LA_coords);CHKERRQ(ierr);

	ierr = DMDAGetGhostedCoordinates(da,&gcoords);CHKERRQ(ierr);
	ierr = DMGlobalToLocalBegin(cda,coords,INSERT_VALUES,gcoords);CHKERRQ(ierr);
	ierr = DMGlobalToLocalEnd(cda,coords,INSERT_VALUES,gcoords);CHKERRQ(ierr);
	
	
	PetscFunctionReturn(0);
}

/*

10^21 . U/(L^2) = rho.g 
 
 L = 100 x 1000 (100 km)
 
 10^11.U = rho.9.81
 
 f = rho x 9.81 x 10^-11 x U^-1
 
 U = 1 cm/yr = 0.000000000317098
 
 f = (1000xrho_ref) ..
   = 309.052799999999969 * rho_ref
 
 T = L / U = 315359920277012
 
 dt = 5000 yrs => 5000 x (60.60.24.365) / T = 0.0005000001264
 
 t = 0.335 MYr = 0.0335000084688
 
 */
#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_DrunkSeaman"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_DrunkSeaman(pTatinCtx ctx)
{
	PetscInt   p,n_mp_points;
	DataBucket db;
	DataField  PField_std,PField_stokes;
	PetscReal  opts_eta0,opts_eta1,opts_rho0,opts_rho1,amp,grav;
	double     eta,rho;
	int        phase;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	
	ctx->rheology_constants.rheology_type = RHEOLOGY_VISCOUS;
	
	opts_eta0 = 1.0e-1;
	opts_eta1 = 1.0;
	opts_rho0 = 3.2;
	opts_rho1 = 3.3;
	grav = 309.052799999999969;
	
	ierr = QuadratureStokesGravityModelCoordinateAlignedSetValues(ctx->Q,0.0,-grav);CHKERRQ(ierr);
	
	
	amp = 5.0/100;
	ierr = PetscOptionsGetReal(PETSC_NULL,"-dnksm_eta0",&opts_eta0,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-dnksm_eta1",&opts_eta1,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-dnksm_rho0",&opts_rho0,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-dnksm_rho1",&opts_rho1,0);CHKERRQ(ierr);
	
	db = ctx->db;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	
	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	
	DataBucketGetSizes(db,&n_mp_points,0,0);

	for (p=0; p<n_mp_points; p++) {
		MPntStd     *material_point;
		MPntPStokes *mpprop_stokes;
		double      *position;
		
		DataFieldAccessPoint(PField_std,p,(void**)&material_point);
		DataFieldAccessPoint(PField_stokes,p,(void**)&mpprop_stokes);
		
		position = material_point->coor;
		
		rho = opts_rho1;
		eta = opts_eta1;
		phase = 1;
		if (position[1] < -1.0 + amp * cos(2.0*M_PI*position[0]/5.0) ) {
			phase = 0;
			rho = opts_rho0;
			eta = opts_eta0;
		}
		
		/* user the setters provided for you */
		MPntStdSetField_phase_index(material_point,phase);
		MPntPStokesSetField_eta_effective(mpprop_stokes,eta);
		MPntPStokesSetField_density(mpprop_stokes,rho);
	}
	
	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokes);
	
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_DrunkSeaman"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_DrunkSeaman(pTatinCtx ctx)
{
	BCList         ubclist;
	PetscScalar    bcval;
	DM             dav;
	PetscInt       dof;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ubclist = ctx->u_bclist;
	dav     = ctx->dav;
	/* free slip left/right sides */
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

	/* no slip base */
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

	/* free surface top */
	//bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	
	
	/* free surface top */
	//dof = 1; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_impose_coordinates,(void*)&dof);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelUpdateMeshGeometry_Drunk"
PetscErrorCode pTatin2d_ModelUpdateMeshGeometry_Drunk(pTatinCtx ctx,Vec X)
{
	PetscReal step;
	Vec velocity,pressure,coordinates;
	DM dav,dap;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	step = ctx->dt;
	
	ierr = DMCompositeGetEntries(ctx->pack,&dav,&dap);CHKERRQ(ierr);
	ierr = DMCompositeGetAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
	
	ierr = DMDAGetCoordinates(dav,&coordinates);CHKERRQ(ierr);
	ierr = VecAXPY(coordinates,step,velocity);CHKERRQ(ierr); /* x = x + dt.vel */
	ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
	
	ierr = DMCompositeRestoreAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}



