
#CFLAGS                 = -Wno-unused-variable
#CFLAGS	         = -Wno-unused-variable -DPBASIS_P1_GLOBAL_RELATIVE

## or run >make CFLAGS='-g -O0'
export CFLAGS		=	-O0 -g -Wall -Wno-unused-variable
#export CFLAGS		=	 -Wall -Wno-unused-variable -std=gnu99 -O2 -g -Wstrict-aliasing -fstrict-aliasing -funroll-loops
#export CFLAGS		=	-Wall -Wno-unused-variable -std=gnu99 -O3 -g -ftree-vectorize -ftree-vectorizer-verbose=2 -Wstrict-aliasing -fstrict-aliasing -funroll-loops
#export PETSC_DIR=/Users/laetitia/software/petsc-3.2-p7
#export PETSC_ARCH=arch-darwin-c-opt
include ${PETSC_DIR}/conf/variables
include ${PETSC_DIR}/conf/rules

export TATIN_DIR	=	${PWD}
export INC				=	-I. -I${TATIN_DIR} -I${PETSC_DIR} ${PETSC_CC_INCLUDES}
export LIB				=	${PETSC_LIB}


export T_SRC	=	 \
			restriction_prolgongation_P1.c \
			daSUPG_Q1.c \
			daVMS_Q1.c \
			dmda_compare.c \
			dmda_update_coords.c \
			dmda_duplicate.c \
			dmda_redundant.c \
			dmda_remesh.c \
			mesh_update.c \
			data_exchanger.c \
			swarm_fields.c \
			MPntStd_def.c \
			MPntPStokes_def.c \
			MPntPStokesPl_def.c \
			MPntPStokesMelt_def.c \
			MPntPThermal_def.c \
			MPntPChrono_def.c \
			MPntPPassive_def.c \
			MPntPDarcy_def.c \
			MPntPStokesElas_def.c \
			material_point_std_utils.c \
			material_point_stokes_utils.c \
			material_point_stokespl_utils.c \
			material_point_stokesmelt_utils.c \
			material_point_passive_utils.c \
			material_point_chrono_utils.c \
			material_point_thermal_utils.c \
			material_point_darcy_utils.c \
                        material_point_stokes_elas_utils.c\
			material_point_utils.c \
			AVD2d.c \
			material_point_population_control.c \
			phase_map.c \
			topo_map.c \
			temperature_map.c \
			map2d.c \
			element_type_Q1.c \
			element_type_Q2.c \
			dmdae.c \
			quadrature.c \
			output_paraview.c \
			checkpoint.c \
			phys_energy_equation.c \
			phys_darcy_equation.c \
			physcomp_SPM.c \
			stokes_form_function.c \
			pTatin2d_models.c\
			units.c \
			ptatin_log.c \
			model_SolCx.c \
			model_RT.c \
			model_SN.c \
			model_MAP_LV.c \
			model_CrystalChannel.c \
			model_Delamination.c\
			model_PureShearFree.c\
			model_SimpleShearFree.c\
			model_StepShearFree.c\
                        model_Schmalholz2011.c\
			model_Camembert.c\
			model_Notch.c\
                        model_NotchGP.c\
			model_NotchEl.c\
			model_Elastic.c\
			model_winkler.c\
			model_mms_linear.c\
			model_GENE.c\
			model_anthony.c\
			model_paul.c\
			model_julia.c\
			model_reza.c\
			model_Sinker.c \
			model_AdvDiff.c \
			model_HotBlob.c \
			model_Drunk.c \
			model_G2008.c \
			model_afonso.c \
			model_mumu.c \
                        model_WUSA.c \
			model_CrystalJCP.c \
			rheology.c\
			stokes_viscous.c\
			stokes_visco_plastic.c\
			stokes_visco_plasticT.c\
			stokes_VPT.c\
			stokes_EVPT.c\
			stokes_visco_elasto_plastic.c \
			stokes_stress_boundary_conditions.c

export T_OBJ	=	$(T_SRC:.c=.o)


all: 
	${MAKE} base_objects
	${MAKE} libs
	${MAKE} apps
	${MAKE} models

base_objects: ${T_OBJ}
	-@echo ""
	-@echo "*** Creating pTatin2d object files (empty) ***"

libs: base_objects pTatin2d.o
	-@echo ""
	-@echo "*** Creating pTatin2d libs (empty) ***"

apps: libs
	-@echo ""
	-@echo "*** Building pTatin2d applications ***"
	-@${MAKE} pTatin2d.app
	-@${MAKE} pTatin2d_ts.app
	-@${MAKE} pTatin2d_two_stage.app
	-@${MAKE} pTatin2d_gmg.app
	-@${MAKE} pTatin2d_full_gmg.app
	-@${MAKE} dasupg_test.app
	-@${MAKE} davms_test.app

dasupg_test.app: dasupg_test.o pTatin2d.o ${T_OBJ} chkopts
	-@echo ""
	${PCC} ${CFLAGS} -o dasupg_test.app dasupg_test.o pTatin2d.o ${T_OBJ} ${INC} ${LIB}
davms_test.app: davms_test.o pTatin2d.o ${T_OBJ} chkopts
	-@echo ""
	${PCC} ${CFLAGS} -o davms_test.app davms_test.o pTatin2d.o ${T_OBJ} ${INC} ${LIB}


pTatin2d.app: pTatin2d_std.o pTatin2d.o ${T_OBJ} chkopts
	-@echo ""
	${PCC} ${CFLAGS} -o pTatin2d.app pTatin2d_std.o pTatin2d.o ${T_OBJ} ${INC} ${LIB}

pTatin2d_ts.app: pTatin2d_ts.o pTatin2d.o ${T_OBJ} chkopts
	-@echo ""
	${PCC} ${CFLAGS} -o pTatin2d_ts.app pTatin2d_ts.o pTatin2d.o ${T_OBJ} ${INC} ${LIB}

pTatin2d_two_stage.app: pTatin2d_two_stage.o pTatin2d.o ${T_OBJ} chkopts
	-@echo ""
	${PCC} ${CFLAGS} -o pTatin2d_two_stage.app pTatin2d_two_stage.o pTatin2d.o ${T_OBJ} ${INC} ${LIB}

pTatin2d_gmg.app: pTatin2d_gmg.o pTatin2d.o ${T_OBJ} chkopts
	-@echo ""
	${PCC} ${CFLAGS} -o pTatin2d_gmg.app pTatin2d_gmg.o pTatin2d.o ${T_OBJ} ${INC} ${LIB}

pTatin2d_full_gmg.app: pTatin2d_full_gmg.o pTatin2d.o ${T_OBJ} chkopts
	-@echo ""
	${PCC} ${CFLAGS} -o pTatin2d_full_gmg.app pTatin2d_full_gmg.o pTatin2d.o ${T_OBJ} ${INC} ${LIB}

models: libs
	-@echo ""
	-@echo "*** Creating pTatin2d models (empty) ***"


%.o: %.c
	-@echo ""
	${PCC} ${CFLAGS} -c $*.c ${INC}



clean_all:
	rm -f *.o
	rm -f *.app

clean_output:
	rm -f *.pvtu *.pvts *.vts *.vtu

	
