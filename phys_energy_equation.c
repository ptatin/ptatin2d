
#include "pTatin2d.h"
#include "dmdae.h"
#include "element_type_Q2.h"
#include "element_type_Q1.h"
#include "phys_energy_equation.h"

#define SUPG_EPS 1.0e-10

#undef __FUNCT__
#define __FUNCT__ "QuadratureVolumeEnergyGetCell"
PetscErrorCode QuadratureVolumeEnergyGetCell(QuadratureVolumeEnergy Q,PetscInt cidx,CoefficientsEnergyEq **points)
{
  PetscFunctionBegin;
	if (cidx>=Q->ncells) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_SIZ,"cidx > max cells");
	}
	*points = &Q->cellproperties[cidx*Q->ngp];
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureVolumeEnergyCreate"
PetscErrorCode QuadratureVolumeEnergyCreate(QuadratureStokesRule rule,PetscInt ncells,QuadratureVolumeEnergy *Q)
{
	QuadratureVolumeEnergy quadrature;
	PetscInt p,ngp;
	PetscScalar gp_xi[MAX_QUAD_PNTS][2],gp_weight[MAX_QUAD_PNTS];
	PetscErrorCode ierr;
	
  PetscFunctionBegin;
	ierr = PetscMalloc( sizeof(struct _p_QuadratureVolumeEnergy), &quadrature);CHKERRQ(ierr);
	
	PetscPrintf(PETSC_COMM_WORLD,"QuadratureVolumeEnergyCreate:\n");
	switch (rule) {
		case QRule_GaussLegendre1:
			PetscPrintf(PETSC_COMM_WORLD,"\tUsing 1 pnt Gauss Legendre quadrature\n");
			QuadratureCreateGauss_1pnt_2D(&ngp,gp_xi,gp_weight);
			break;
			
		case QRule_GaussLegendre2:
			PetscPrintf(PETSC_COMM_WORLD,"\tUsing 2x2 pnt Gauss Legendre quadrature\n");
			QuadratureCreateGauss_2pnt_2D(&ngp,gp_xi,gp_weight);
			break;
			
		case QRule_GaussLegendre3:
			PetscPrintf(PETSC_COMM_WORLD,"\tUsing 3x3 pnt Gauss Legendre quadrature\n");
			QuadratureCreateGauss_3pnt_2D(&ngp,gp_xi,gp_weight);
			break;
			
		case QRule_Subcell2x2GaussLegendre3:
			PetscPrintf(PETSC_COMM_WORLD,"\tUsing 2x2 subcell, with 3x3 pnt Gauss Legendre quadrature in each\n");
			QuadratureCreateGauss_2x3pnt_2D(&ngp,gp_xi,gp_weight);
			break;
			
		default: /* QRule_GaussLegendre3 */
			PetscPrintf(PETSC_COMM_WORLD,"\tUsing 3x3 pnt Gauss Legendre quadrature\n");
			QuadratureCreateGauss_3pnt_2D(&ngp,gp_xi,gp_weight);
			break;
	}
	
	quadrature->ncells = ncells;
	if (ncells!=0) {
		ierr = PetscMalloc( sizeof(CoefficientsEnergyEq)*ncells*ngp, &quadrature->cellproperties);CHKERRQ(ierr);
		ierr = PetscMemzero(quadrature->cellproperties,sizeof(CoefficientsEnergyEq)*ncells*ngp);CHKERRQ(ierr);

		/* init values to zero */
		for (p=0; p<ncells*ngp; p++) {
			quadrature->cellproperties[p].diffusivity = 0.0;
			quadrature->cellproperties[p].heat_source = 0.0;
		}
		PetscPrintf(PETSC_COMM_WORLD,"\tCoefficientsEnergyEq using %1.2lf MBytes \n", (double)(sizeof(CoefficientsEnergyEq)*ncells*ngp)*1.0e-6 );
	}
	
	
	/* allocate */
	quadrature->ngp = ngp;
	ierr = PetscMalloc( sizeof(PetscScalar)*ngp*NSD, &quadrature->xi );CHKERRQ(ierr);
	ierr = PetscMalloc( sizeof(PetscScalar)*ngp, &quadrature->weight );CHKERRQ(ierr);
	/* copy */
	for (p=0; p<ngp; p++) {
		quadrature->xi[NSD*p  ] = gp_xi[p][0];
		quadrature->xi[NSD*p+1] = gp_xi[p][1];
		
		quadrature->weight[p] = gp_weight[p];
	}
	
	*Q = quadrature;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureVolumeEnergyDestroy"
PetscErrorCode QuadratureVolumeEnergyDestroy(QuadratureVolumeEnergy *Q)
{
	QuadratureVolumeEnergy quadrature;
	PetscInt ncells;
	PetscErrorCode ierr;
	
  PetscFunctionBegin;
	
	if (!Q) { PetscFunctionReturn(0); }
	quadrature = *Q;
	
	ncells = quadrature->ncells;
	if (ncells!=0) {
		ierr = PetscFree(quadrature->cellproperties);CHKERRQ(ierr);
	}
	
	/* allocate */
	ierr = PetscFree( quadrature->xi );CHKERRQ(ierr);
	ierr = PetscFree( quadrature->weight );CHKERRQ(ierr);

	ierr = PetscFree( quadrature );CHKERRQ(ierr);
	*Q = PETSC_NULL;

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PhysComp_EnergyEquationCreate1"
PetscErrorCode PhysComp_EnergyEquationCreate1(PhysCompEnergyCtx *p)
{
	PhysCompEnergyCtx phys;
	PetscErrorCode ierr;

	PetscFunctionBegin;
	ierr = PetscMalloc(sizeof(struct _p_PhysCompEnergyCtx),&phys);CHKERRQ(ierr);
	ierr = PetscMemzero(phys,sizeof(struct _p_PhysCompEnergyCtx));CHKERRQ(ierr);
	*p = phys;
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PhysComp_EnergyEquationCreate2"
PetscErrorCode PhysComp_EnergyEquationCreate2(DM davq2,PhysCompEnergyCtx *p)
{
	DMDAE dae;
	PhysCompEnergyCtx phys;
	DM dmq1;
	PetscInt ncells;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;

	/* create structure */
	ierr = PhysComp_EnergyEquationCreate1(&phys);CHKERRQ(ierr);
	
	ierr = DMDACreateOverlappingQ1FromQ2(davq2,&dmq1);CHKERRQ(ierr);

	phys->daT = dmq1;
	ierr = DMGetDMDAE(phys->daT,&dae);CHKERRQ(ierr);
	phys->mx = dae->mx;
	phys->my = dae->my;
	
	ierr = DMCreateGlobalVector(dmq1,&phys->T);CHKERRQ(ierr);
	{
		DM cda;
		
		ierr = DMDAGetCoordinateDA(dmq1,&cda);CHKERRQ(ierr);
		ierr = DMCreateGlobalVector(cda,&phys->u_minus_V);CHKERRQ(ierr);
	}	
	
	ncells = dae->lmx * dae->lmy;
	ierr = QuadratureVolumeEnergyCreate(QRule_GaussLegendre2,ncells,&phys->Q);CHKERRQ(ierr);

	ierr = DMDABCListCreate(phys->daT,&phys->T_bclist);CHKERRQ(ierr);
	
	/* SUPG information */
	ierr = DMCreateGlobalVector(dmq1,&phys->Told);CHKERRQ(ierr);
	{
		DM cda;
		
		ierr = DMDAGetCoordinateDA(dmq1,&cda);CHKERRQ(ierr);
		ierr = DMCreateGlobalVector(cda,&phys->Xold);CHKERRQ(ierr);
	}	
	
	
	*p = phys;
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PhysComp_EnergyEquationDestroy"
PetscErrorCode PhysComp_EnergyEquationDestroy(PhysCompEnergyCtx *p)
{
	PhysCompEnergyCtx phys;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	if (!p) { PetscFunctionReturn(0); }
	phys = *p;
	
	
	ierr = DMDestroyDMDAE(phys->daT);CHKERRQ(ierr);
	
	ierr = DMDestroy(&phys->daT);CHKERRQ(ierr);
	ierr = VecDestroy(&phys->T);CHKERRQ(ierr);
	ierr = VecDestroy(&phys->u_minus_V);CHKERRQ(ierr);
	
	ierr = QuadratureVolumeEnergyDestroy(&phys->Q);CHKERRQ(ierr);
	ierr = BCListDestroy(&phys->T_bclist);CHKERRQ(ierr);
	
	ierr = VecDestroy(&phys->Told);CHKERRQ(ierr);
	ierr = VecDestroy(&phys->Xold);CHKERRQ(ierr);
	
	ierr = PetscFree(phys);CHKERRQ(ierr);
	
	*p = PETSC_NULL;
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDAViewPetscVTK"
PetscErrorCode DMDAViewPetscVTK(DM da,Vec field,const char fname[])
{
	Vec x;
	PetscViewer vv;
	PetscErrorCode ierr;
	
	
	PetscFunctionBegin;
	ierr = PetscViewerASCIIOpen(((PetscObject)(da))->comm, fname, &vv);CHKERRQ(ierr);
	ierr = PetscViewerSetFormat(vv, PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
	
	/* view mesh */
	ierr = DMView(da,vv);CHKERRQ(ierr);
	
	/* view field */
	/* if the vector is valid, write it out - else create an empty field */
	if (field) {
		const char *name;
		name = PETSC_NULL;
		/* temp work around - calling GetName forces a name to be inserted if you isn't there 
		 - in parallel an error will occur if [1]PETSC ERROR: VecView_MPI_DA() line 464 in src/dm/impls/da/gr2.c
		 if the name is null
		 */
		ierr = PetscObjectGetName( (PetscObject)field,&name);CHKERRQ(ierr);
		ierr = VecView(field,vv);CHKERRQ(ierr);
	} else {
		ierr = DMCreateGlobalVector(da,&x);CHKERRQ(ierr);
		ierr = PetscObjectSetName( (PetscObject)x, "empty_field" );CHKERRQ(ierr);
		ierr = VecView(x,vv);CHKERRQ(ierr);
		ierr = VecDestroy(&x);CHKERRQ(ierr);
	}
	
	ierr = PetscViewerDestroy(&vv);CHKERRQ(ierr);
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PhysCompView_EnergyEquation"
PetscErrorCode PhysCompView_EnergyEquation(PhysCompEnergyCtx phys,Vec X,const char fname[])
{
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	ierr = DMDAViewPetscVTK(phys->daT,X,fname);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatinLoadPhysics_EnergyEquation"
PetscErrorCode pTatinLoadPhysics_EnergyEquation(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	
	
	PetscFunctionBegin;
	
	ierr = PhysComp_EnergyEquationCreate2(ctx->dav,&ctx->phys_energy);CHKERRQ(ierr);
	
  PetscFunctionReturn(0);
}

/* SUPG IMPLEMENTATION */
void ConstructNiSUPG_Q1_2D(PetscScalar Up[],PetscScalar kappa_hat,PetscScalar Ni[],PetscScalar GNx[NSD][NODES_PER_EL_Q1_2D],PetscScalar Ni_supg[])
{
  PetscScalar uhat[NSD],unorm;
  PetscInt i;
	
  uhat[0] = Up[0];
  uhat[1] = Up[1];
  unorm = PetscSqrtScalar(Up[0]*Up[0] + Up[1]*Up[1]);
	if (unorm > SUPG_EPS) {
		uhat[0] = Up[0]/(unorm*unorm);
		uhat[1] = Up[1]/(unorm*unorm);
	}
	
  if (kappa_hat < SUPG_EPS) {
    for (i=0; i<NODES_PER_EL_Q1_2D; i++) {
      Ni_supg[i] = Ni[i];
    }
  } else {
    for (i=0; i<NODES_PER_EL_Q1_2D; i++) {
      Ni_supg[i] = Ni[i] + kappa_hat *0.0* ( uhat[0] * GNx[0][i] + uhat[1] * GNx[1][i] );
    }
  }
}


/* SUPG business */

/*

 Structure for solving
 
 \dot\phi + a.v_i \phi_{i} = - b.(kappa \phi_i)_{,i} + f

 
 \dot\phi + v_i \phi_{i} = - (kappa \phi_i)_{,i} + f
 
 
 M.Tdot + A.T = K.T + f
 M.(T2-T1) + dt.0.5*(A.T2 + A.T1) = dt.0.5*(K.T2 + K.T1) + dt.f
 M.T2 + 0.5.dt.A.T2 - 0.5.dt.K.T2 = - 0.5.dt.A.T1 + 0.5.dt.K.T1 + dt.f
 
 M + (theta).X.T2 = (1-theta).Y.T1
 
 
*/

const PetscReal SUPG_THETA = 1.0; /* 1 = fully implicit */

/* Eqn 4.3.7 */
#undef __FUNCT__
#define __FUNCT__ "DASUPG2dComputeElementPecletNumber_qp"
PetscErrorCode DASUPG2dComputeElementPecletNumber_qp( PetscScalar el_coords[],PetscScalar u[],
																											PetscScalar kappa_el,
																											PetscScalar *alpha)
{
	PetscScalar DX[NSD];
  PetscScalar u_xi,v_eta,one_dxi2,one_deta2;
  PetscScalar _alpha,u_norm,dxi,deta;
	
  PetscFunctionBegin;

	/*
	 2----3
	 |    |
	 |    |
	 0----1 
	 */
  DX[0] = el_coords[NSD*1  ] - el_coords[NSD*0  ];
  DX[1] = el_coords[NSD*2+1] - el_coords[NSD*0+1]; 
  
	u_xi  = 0.25 * ( u[NSD*0  ] + u[NSD*1  ] + u[NSD*2  ] + u[NSD*3  ] );
  v_eta = 0.25 * ( u[NSD*0+1] + u[NSD*1+1] + u[NSD*2+1] + u[NSD*3+1] );

  dxi  = DX[0];
  deta = DX[1];
  one_dxi2  = 1.0/dxi/dxi;
  one_deta2 = 1.0/deta/deta;
	
  u_norm = sqrt( u_xi*u_xi + v_eta*v_eta );
	
  _alpha = 0.5 * u_norm / ( (1.0e-32+kappa_el) * sqrt( one_dxi2 + one_deta2 ) );
  *alpha  = _alpha;  
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DASUPG2dComputeElementTimestep_qp"
PetscErrorCode DASUPG2dComputeElementTimestep_qp(PetscScalar el_coords[],PetscScalar u[],PetscScalar kappa_el,PetscScalar *dta,PetscScalar *dtd)
{
	PetscScalar DX[NSD];
  PetscScalar u_xi,v_eta,one_dxi2,one_deta2,dxi,deta,khat,alpha_eff;
  PetscScalar CrFAC,dt_optimal,alpha;
  PetscScalar U,H,dt_advective,dt_diffusive;
	PetscErrorCode ierr;
	
  PetscFunctionBegin;

	/*
	 2----3
	 |    |
	 |    |
	 0----1 
	 */
  DX[0] = el_coords[NSD*1  ] - el_coords[NSD*0  ];
  DX[1] = el_coords[NSD*2+1] - el_coords[NSD*0+1]; 
  
	u_xi  = 0.25 * ( u[NSD*0  ] + u[NSD*1  ] + u[NSD*2  ] + u[NSD*3  ] );
  v_eta = 0.25 * ( u[NSD*0+1] + u[NSD*1+1] + u[NSD*2+1] + u[NSD*3+1] );
	
  dxi  = DX[0];
  deta = DX[1];
	
  /* advective limit */
  one_dxi2  = 1.0/dxi/dxi;
  one_deta2 = 1.0/deta/deta;
	
  U = sqrt( u_xi*u_xi + v_eta*v_eta );
  H = 1.0 / sqrt( one_dxi2 + one_deta2 );
	
  ierr = DASUPG2dComputeElementPecletNumber_qp(el_coords,u,kappa_el,&alpha);CHKERRQ(ierr);
	
  if (U<1.0e-32) {
		//    printf("diffusive limit \n");
    dt_diffusive = 0.5 * H*H / kappa_el;
    *dtd = dt_diffusive;
    *dta = 1.0e10;
  } else {
    if (alpha >= 100.0 ) {
      CrFAC = 0.4;
    } else {
      CrFAC = PetscMin(1.0,alpha);
    }
    dt_optimal = CrFAC * H / (U+1.0e-32);
    *dta = dt_optimal;
    *dtd = 1.0e10;
  }
/*
	printf("Element Pe:              %1.4f \n", alpha );
	printf("        dx/dv:           %1.4f \n", H/(U+1.0e-32) );
	printf("        0.5.dx.dx/kappa: %1.4f \n", 0.5*H*H/(kappa_el+1.0e-32) );
*/
	
	*dta = H/(U+1.0e-32);
	*dtd = 0.5*H*H/(kappa_el+1.0e-32);
	
  PetscFunctionReturn(0);
}


/*
 Eqns 3.3.11 (transient) + 3.3.4, 3.3.5, 3.3.6
 */
#undef __FUNCT__
#define __FUNCT__ "DASUPG2dComputeElementStreamlineDiffusion_qp"
PetscErrorCode DASUPG2dComputeElementStreamlineDiffusion_qp(PetscScalar el_coords[],PetscScalar u[],
																												 PetscInt nqp, PetscScalar qp_detJ[], PetscScalar qp_w[],
																												 PetscScalar qp_kappa[],
																												 PetscScalar *khat)
{
	PetscInt p;
	PetscScalar DX[NSD];
  PetscScalar u_xi,v_eta;
  PetscScalar kappa_el,alpha_xi,alpha_eta,_khat,dxi,deta,xi,eta,vol_el;
	
  PetscFunctionBegin;

	/* average velocity - cell centre velocity */
/*
	2----3
	|    |
	|    |
	0----1 
*/
  DX[0] = el_coords[NSD*1  ] - el_coords[NSD*0  ];
  DX[1] = el_coords[NSD*2+1] - el_coords[NSD*0+1]; 
	
	/* average velocity - cell centre velocity */
	u_xi  = 0.25 * ( u[NSD*0  ] + u[NSD*1  ] + u[NSD*2  ] + u[NSD*3  ] );
  v_eta = 0.25 * ( u[NSD*0+1] + u[NSD*1+1] + u[NSD*2+1] + u[NSD*3+1] );

	//printf("u_xi/v_eta = %1.4e %1.4e \n",u_xi,v_eta );
	
	/* integral average of diffusivity */
	kappa_el = 0.0;
	vol_el = 0.0;
	for (p=0; p<nqp; p++) {
	//	kappa_el = 0.25 * ( kappa[0] + kappa[1] + kappa[2] + kappa[3] );
		kappa_el += qp_w[p] * qp_kappa[p] * qp_detJ[p]; 
		vol_el   += qp_w[p] * qp_detJ[p];
	}
  kappa_el = kappa_el / vol_el;
	
  dxi  = DX[0];
  deta = DX[1];
	if (kappa_el < SUPG_EPS) {
		alpha_xi  = 1.0/SUPG_EPS;
		alpha_eta = 1.0/SUPG_EPS;
	} else {
		alpha_xi  = 0.5 * u_xi  * dxi  / kappa_el;
		alpha_eta = 0.5 * v_eta * deta / kappa_el;
	}
  xi  = AdvDiffResidualForceTerm_UpwindXiExact(alpha_xi);
  eta = AdvDiffResidualForceTerm_UpwindXiExact(alpha_eta);
	
	//  khat = 0.5*(xi*u_xi*dxi + eta*v_eta*deta); /* steady state */
  _khat = 1.0 * 0.258198889747161 * ( xi * u_xi * dxi + eta * v_eta * deta ); /* transient case, sqrt(1/15) */
  *khat = _khat;
	
  PetscFunctionReturn(0);
}

void AElement_SUPG2d_qp( PetscScalar Re[],PetscReal dt,PetscScalar el_coords[],
										 PetscScalar gp_kappa[],
										 PetscScalar el_V[],
										 PetscInt ngp,PetscScalar gp_xi[],PetscScalar gp_weight[] )
{
  PetscInt    p,i,j;
  PetscScalar Ni_p[NODES_PER_EL_Q1_2D],Ni_supg_p[NODES_PER_EL_Q1_2D];
  PetscScalar GNi_p[NSD][NODES_PER_EL_Q1_2D],GNx_p[NSD][NODES_PER_EL_Q1_2D];
  PetscScalar J_p,fac,gp_detJ[27];
  PetscScalar kappa_p,v_p[2];
  PetscScalar kappa_hat;
	
  /* compute constants for the element */
  for (p = 0; p < ngp; p++) {
    ConstructGNi_Q1_2D(&gp_xi[2*p],GNi_p);
    ConstructGNx_Q1_2D(GNi_p,GNx_p,el_coords,&gp_detJ[p]);
	}
	
	DASUPG2dComputeElementStreamlineDiffusion_qp(el_coords,el_V,
																						ngp,gp_detJ,gp_weight,gp_kappa,
																						&kappa_hat);
	
  /* evaluate integral */
  for (p = 0; p < ngp; p++) {
    ConstructNi_Q1_2D(&gp_xi[2*p],Ni_p);
    ConstructGNi_Q1_2D(&gp_xi[2*p],GNi_p);
    ConstructGNx_Q1_2D(GNi_p,GNx_p,el_coords,&J_p);
		
    fac = gp_weight[p]*J_p;

		kappa_p = gp_kappa[p];
    v_p[0] = 0.0;
		v_p[1] = 0.0;
    for (j=0; j<NODES_PER_EL_Q1_2D; j++) {
      v_p[0]     += Ni_p[j] * el_V[NSD*j+0];      /* compute vx on the particle */
      v_p[1]     += Ni_p[j] * el_V[NSD*j+1];      /* compute vy on the particle */
    }
    ConstructNiSUPG_Q1_2D(v_p,kappa_hat,Ni_p,GNx_p,Ni_supg_p);
		
    /* R = f - m phi_dot - c phi*/
		for (i=0; i<NODES_PER_EL_Q1_2D; i++) {
			for (j=0; j<NODES_PER_EL_Q1_2D; j++) {
				Re[j+i*NODES_PER_EL_Q1_2D] += fac * ( 
																					//a * Ni_p[i] * Ni_p[j]
																					//																			 + dt * kappa_p * ( GNx_p[0][i] * GNx_p[0][j] + GNx_p[1][i] * GNx_p[1][j] )
																					//																			 + dt * Ni_supg_p[i] * ( v_p[0] * GNx_p[0][j] + v_p[1] * GNx_p[1][j] )
																					+ SUPG_THETA * dt * kappa_p * ( GNx_p[0][i] * GNx_p[0][j] + GNx_p[1][i] * GNx_p[1][j] )
																				  + SUPG_THETA * dt * Ni_supg_p[i] * ( v_p[0] * GNx_p[0][j] + v_p[1] * GNx_p[1][j] )
																					+ Ni_supg_p[i] * Ni_p[j]
																					//		+ Ni_p[i] * Ni_p[j]
																					);
			}
		}
  }
	/*
	 printf("e=\n");
	 for (i=0; i<NODES_PER_EL_Q1; i++) {
	 for (j=0; j<NODES_PER_EL_Q1; j++) {
	 printf("%lf ", Re[j+i*NODES_PER_EL_Q1]); 
	 }printf("\n");
	 }
	 */ 
}

/* 
 Computes M + dt.(L + A)
*/
#undef __FUNCT__  
#define __FUNCT__ "SUPGFormJacobian_qp"
PetscErrorCode SUPGFormJacobian_qp(PetscReal time,Vec X,PetscReal dt,Mat *A,Mat *B,MatStructure *mstr,void *ctx)
{
  PhysCompEnergyCtx data = (PhysCompEnergyCtx)ctx;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
  DM          da,cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
  PetscInt    p,i,j;
	PetscScalar ADe[NODES_PER_EL_Q1_2D*NODES_PER_EL_Q1_2D],el_coords[NSD*NODES_PER_EL_Q1_2D],el_V[NODES_PER_EL_Q1_2D*NSD];
	PetscScalar gp_kappa[27];
	/**/
	PetscInt nel,nen,e,n;
	const PetscInt *elnidx;
	BCList bclist;
	PetscInt nxg,Len_local, *gidx_bclocal;
	PetscInt *gidx,elgidx[NODES_PER_EL_Q1_2D],elgidx_bc[NODES_PER_EL_Q1_2D];
	Vec                    V;
  Vec                    local_V;
  PetscScalar            *LA_V;
	CoefficientsEnergyEq   *quadpoints;
	PetscErrorCode ierr;
	
	
  PetscFunctionBegin;
	da     = data->daT;
	V      = data->u_minus_V;
	bclist = data->T_bclist;
	
	/* trash old entries */
  ierr = MatZeroEntries(*B);CHKERRQ(ierr);
	
	/* quadrature */
	ngp       = data->Q->ngp;
	gp_xi     = data->Q->xi;
	gp_weight = data->Q->weight;
	
  /* setup for coords */
  ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
  ierr = DMDAGetGhostedCoordinates(da,&gcoords);CHKERRQ(ierr);
  ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
  /* get acces to the vector V */
  ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
  ierr = DMGetLocalVector(cda,&local_V);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(cda,V,INSERT_VALUES,local_V);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(  cda,V,INSERT_VALUES,local_V);CHKERRQ(ierr);
  ierr = VecGetArray(local_V,&LA_V);CHKERRQ(ierr);
	
	
	/* stuff for eqnums */
	ierr = DMDAGetGlobalIndices(da,0,&gidx);CHKERRQ(ierr);
	ierr = BCListGetDofIdx(bclist,0,0,&Len_local,&gidx_bclocal);CHKERRQ(ierr);
	
	ierr = DMDAGetElementsQ1(da,&nel,&nen,&elnidx);CHKERRQ(ierr);
	for (e=0;e<nel;e++) {
		/* get coords for the element */
		ierr = DMDAGetElementCoordinatesQ1_2D(el_coords,(PetscInt*)&elnidx[nen*e],LA_gcoords);CHKERRQ(ierr);

		/* get velocity for the element */
		ierr = DMDAGetVectorElementFieldQ1_2D(el_V,(PetscInt*)&elnidx[nen*e],LA_V);CHKERRQ(ierr);
		
		ierr = QuadratureVolumeEnergyGetCell(data->Q,e,&quadpoints);CHKERRQ(ierr);
		
		/* copy the diffusivity */
		for (n=0; n<ngp; n++) {
			gp_kappa[n] = quadpoints[n].diffusivity;
		}
		
		/* initialise element stiffness matrix */
		ierr = PetscMemzero(ADe,sizeof(PetscScalar)*NODES_PER_EL_Q1_2D*NODES_PER_EL_Q1_2D);CHKERRQ(ierr);
		
		/* form element stiffness matrix */
		AElement_SUPG2d_qp( ADe,dt,el_coords, gp_kappa, el_V, ngp,gp_xi,gp_weight );
		

		/* insert element matrix into global matrix */
		ierr = GetElementEqnIndicesScalarQ1((PetscInt*)&elnidx[nen*e],gidx,elgidx);CHKERRQ(ierr);
		ierr = GetElementEqnIndicesScalarQ1((PetscInt*)&elnidx[nen*e],gidx_bclocal,elgidx_bc);CHKERRQ(ierr);
		
		ierr = MatSetValues(*B,NODES_PER_EL_Q1_2D,elgidx_bc, NODES_PER_EL_Q1_2D,elgidx_bc, ADe, ADD_VALUES );CHKERRQ(ierr);
  }
	/* tidy up */
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = VecRestoreArray(local_V,&LA_V);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(cda,&local_V);CHKERRQ(ierr);
	
	/* partial assembly */
	ierr = MatAssemblyBegin(*B, MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(*B, MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
	
	/* boundary conditions */
	{
		PetscInt Len_ghost,*gidx_bcghost;
		PetscBool is_dirich;
		PetscInt nproc,*procs,*mapL,**mapIdx;
		ISLocalToGlobalMapping map;
		PetscMPIInt rank;
		
		MPI_Comm_rank( ((PetscObject)da)->comm,&rank);
		ierr = BCListGetDofIdx(bclist,0,0,&Len_ghost,&gidx_bcghost);CHKERRQ(ierr);
		
		ierr = DMGetLocalToGlobalMapping(da,&map);CHKERRQ(ierr);
		ierr = ISLocalToGlobalMappingGetInfo(map,&nproc,&procs,&mapL,&mapIdx);CHKERRQ(ierr);
		
		if (Len_ghost!=mapL[rank]) {
			//			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"map indices are fucked up");
		}
		for (i=0; i<Len_ghost; i++) {
			ierr = BCListIsDirichlet(gidx_bcghost[i],&is_dirich);CHKERRQ(ierr);
			if (is_dirich) {
				ierr = MatSetValueLocal(*B,i,i,1.0,INSERT_VALUES);CHKERRQ(ierr);
			}
		}
	}
	
	/* assemble */
	ierr = MatAssemblyBegin(*B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(*B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  if (*A != *B) {
    ierr = MatAssemblyBegin(*A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(*A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  }
	*mstr = SAME_NONZERO_PATTERN;
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDAProjectVelocityQ2toQ1_2d"
PetscErrorCode DMDAProjectVelocityQ2toQ1_2d(DM daq2,Vec vq2,DM daq1,Vec vq1)
{
	PetscErrorCode ierr;
	Vec coordsQ2, coordsQ1;
	PetscScalar ***LA_Q2;
	PetscScalar ***LA_Q1;
	PetscInt si1,sj1,sk1,nx1,ny1,nz1,i,j;
	DM cdaQ2,cdaQ1;
	PetscBool overlap;
	
	PetscFunctionBegin;
	
	ierr = DMVerifyOverlappingIndices(daq2,daq1,PETSC_FALSE,&overlap);CHKERRQ(ierr);
	if (overlap==PETSC_FALSE) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"DA(Q2) must overlap DA(Q1) in global space");
	}
	
	ierr = DMDAGetCoordinates(daq2,&coordsQ2);CHKERRQ(ierr);
	ierr = DMDAGetCoordinateDA(daq2,&cdaQ2);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cdaQ2,vq2,&LA_Q2);CHKERRQ(ierr);
	
	ierr = DMDAGetCoordinates(daq1,&coordsQ1);CHKERRQ(ierr);
	ierr = DMDAGetCoordinateDA(daq1,&cdaQ1);CHKERRQ(ierr);
	ierr = DMDAVecGetArrayDOF(cdaQ1,vq1,&LA_Q1);CHKERRQ(ierr);
	
	ierr = DMDAGetCorners(daq1,&si1,&sj1,&sk1 , &nx1,&ny1,&nz1);CHKERRQ(ierr);
	for( j=sj1; j<sj1+ny1; j++ ) {
		for( i=si1; i<si1+nx1; i++ ) {
			LA_Q1[j][i][0] = LA_Q2[2*j][2*i][0];
			LA_Q1[j][i][1] = LA_Q2[2*j][2*i][1];
		}
	}
	
	ierr = DMDAVecRestoreArrayDOF(cdaQ1,vq1,&LA_Q1);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArrayDOF(cdaQ2,vq2,&LA_Q2);CHKERRQ(ierr);
	
	
	ierr = DMDAUpdateGhostedCoordinates(daq1);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

void SUPG2d_FormMass_FormResidual_2_qp(
																		PetscScalar Re[],
																		PetscReal dt,
																		PetscScalar el_coords[],PetscScalar el_V[],
																		PetscScalar el_phi[],PetscScalar el_phi_last[],
																		PetscScalar gp_kappa[],PetscScalar gp_Q[],
																		PetscInt ngp,PetscScalar gp_xi[],PetscScalar gp_weight[] )
{
  PetscInt    p,i,j;
  PetscScalar Ni_p[NODES_PER_EL_Q1_2D],Ni_supg_p[NODES_PER_EL_Q1_2D];
  PetscScalar GNi_p[NSD][NODES_PER_EL_Q1_2D],GNx_p[NSD][NODES_PER_EL_Q1_2D];
  PetscScalar phi_p,phi_last_p,f_p,v_p[2],kappa_p, gradphi_p[2],gradphiold_p[2],M_dotT_p;
  PetscScalar J_p,fac,gp_detJ[27];
  PetscScalar kappa_hat;
	
  /* compute constants for the element */
  for (p = 0; p < ngp; p++) {
    ConstructGNi_Q1_2D(&gp_xi[2*p],GNi_p);
    ConstructGNx_Q1_2D(GNi_p,GNx_p,el_coords,&gp_detJ[p]);
	}
	
	DASUPG2dComputeElementStreamlineDiffusion_qp(el_coords,el_V,
																							 ngp,gp_detJ,gp_weight,gp_kappa,
																							 &kappa_hat);
	
	
  /* evaluate integral */
  for (p = 0; p < ngp; p++) {
    ConstructNi_Q1_2D(&gp_xi[2*p],Ni_p);
    ConstructGNi_Q1_2D(&gp_xi[2*p],GNi_p);
    ConstructGNx_Q1_2D(GNi_p,GNx_p,el_coords,&J_p);
		
    fac = gp_weight[p]*J_p;
		
		kappa_p      = gp_kappa[p];
		f_p          = gp_Q[p];
    phi_p        = 0.0;
    phi_last_p   = 0.0;
    gradphi_p[0] = 0.0;
		gradphi_p[1] = 0.0;
    gradphiold_p[0] = 0.0;
		gradphiold_p[1] = 0.0;
    v_p[0] = 0.0;
		v_p[1] = 0.0;
    for (j=0; j<NODES_PER_EL_Q1_2D; j++) {
      phi_p        += Ni_p[j] * el_phi[j];      /* compute phi on the particle */
      phi_last_p   += Ni_p[j] * el_phi_last[j];  /* compute phi_dot on the particle */
      gradphiold_p[0] += GNx_p[0][j] * el_phi_last[j];
      gradphiold_p[1] += GNx_p[1][j] * el_phi_last[j];
			
      gradphi_p[0] += GNx_p[0][j] * el_phi[j];
      gradphi_p[1] += GNx_p[1][j] * el_phi[j];
			
      v_p[0]     += Ni_p[j] * el_V[NSD*j+0];      /* compute vx on the particle */
      v_p[1]     += Ni_p[j] * el_V[NSD*j+1];      /* compute vy on the particle */
    }
    ConstructNiSUPG_Q1_2D(v_p,kappa_hat,Ni_p,GNx_p,Ni_supg_p);
		
		
		M_dotT_p = 0.0;
		for (j=0; j<NODES_PER_EL_Q1_2D; j++) {
			M_dotT_p += Ni_supg_p[j] * el_phi_last[j];
		}
		
    /* R = f - m phi_dot - c phi*/
    for (i = 0; i < NODES_PER_EL_Q1_2D; i++) {
      Re[ i ] += fac * (
												// -f - M T^k
												//- dt * Ni_p[i] * f_p 
												- dt * Ni_supg_p[i] * f_p 
												// (C+L) T^k+1
												//                        + dt * kappa_p * ( GNx_p[0][i] * gradphi_p[0] + GNx_p[1][i] * gradphi_p[1] )  /* - C_diffusive . phi */
												//                        + dt * Ni_supg_p[i] * ( v_p[0] * gradphi_p[0] + v_p[1] * gradphi_p[1] )       /* - C_advective . phi */
												+ (SUPG_THETA)*dt * kappa_p * ( GNx_p[0][i] * gradphi_p[0] + GNx_p[1][i] * gradphi_p[1] )
												+ (SUPG_THETA)*dt * Ni_supg_p[i] * ( v_p[0] * gradphi_p[0] + v_p[1] * gradphi_p[1] ) 
												+ (1.0-SUPG_THETA)*dt * kappa_p * ( GNx_p[0][i] * gradphiold_p[0] + GNx_p[1][i] * gradphiold_p[1] )
												+ (1.0-SUPG_THETA)*dt * Ni_supg_p[i] * ( v_p[0] * gradphiold_p[0] + v_p[1] * gradphiold_p[1] ) 
												// M T^k+1
                        + Ni_supg_p[i] * phi_p 
                        - Ni_supg_p[i] * phi_last_p 
                        //+ Ni_p[i] * phi_p 
                        //- Ni_p[i] * phi_last_p 
												);
    }
  }
	
}

#undef __FUNCT__  
#define __FUNCT__ "FormFunction_SUPG_T_qp"
PetscErrorCode FormFunction_SUPG_T_qp(
																	 PhysCompEnergyCtx data,
																	 PetscReal dt,
																	 DM da,
																	 PetscScalar *LA_V,
																	 PetscScalar *LA_phi,
																	 PetscScalar *LA_philast,
																	 PetscScalar *LA_R)
{
	Vec kappa,V;
  DM                     cda;
  Vec                    gcoords;
  PetscScalar            *LA_gcoords;
  MatStencil             phi_eqn[NODES_PER_EL_Q1_2D];
  PetscInt               sex,sey,mx,my;
  PetscInt               ei,ej;
  PetscScalar            Re[NODES_PER_EL_Q1_2D];
  PetscScalar            el_coords[NODES_PER_EL_Q1_2D*NSD],el_V[NODES_PER_EL_Q1_2D*NSD],el_phi[NODES_PER_EL_Q1_2D],el_philast[NODES_PER_EL_Q1_2D];
	PetscScalar gp_kappa[27];
	PetscScalar gp_Q[27];
	PetscInt nel,nen,e,n;
	const PetscInt *elnidx;
	
  PetscInt    ngp;
  PetscScalar *gp_xi;
  PetscScalar *gp_weight;
  PetscScalar Ni_p[NODES_PER_EL_Q1_2D];
  PetscScalar GNi_p[NSD][NODES_PER_EL_Q1_2D],GNx_p[NSD][NODES_PER_EL_Q1_2D];
	PetscInt phi_el_lidx[NODES_PER_EL_Q1_2D];
  PetscScalar J_p,fac;
  PetscScalar phi_p;
  PetscInt    p,i;
	PetscReal   cg,c = 0.0;
	CoefficientsEnergyEq   *quadpoints;
  PetscErrorCode         ierr;
	
  PetscFunctionBegin;
	
	/* quadrature */
	ngp       = data->Q->ngp;
	gp_xi     = data->Q->xi;
	gp_weight = data->Q->weight;
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(da,&gcoords);CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);

	ierr = DMDAGetElementsQ1(da,&nel,&nen,&elnidx);CHKERRQ(ierr);
	for (e=0;e<nel;e++) {
		
		ierr = GetElementLocalIndicesQ1(phi_el_lidx,(PetscInt*)&elnidx[nen*e]);CHKERRQ(ierr);
		
		/* get coords for the element */
		ierr = DMDAGetElementCoordinatesQ1_2D(el_coords,(PetscInt*)&elnidx[nen*e],LA_gcoords);CHKERRQ(ierr);
		
		ierr = DMDAGetScalarElementFieldQ1_2D(el_phi,(PetscInt*)&elnidx[nen*e],LA_phi);CHKERRQ(ierr);
		ierr = DMDAGetScalarElementFieldQ1_2D(el_philast,(PetscInt*)&elnidx[nen*e],LA_philast);CHKERRQ(ierr);
		
		/* get velocity for the element */
		ierr = DMDAGetVectorElementFieldQ1_2D(el_V,(PetscInt*)&elnidx[nen*e],LA_V);CHKERRQ(ierr);
		
		ierr = QuadratureVolumeEnergyGetCell(data->Q,e,&quadpoints);CHKERRQ(ierr);
		
		/* copy the diffusivity and force */
		for (n=0; n<ngp; n++) {
			gp_kappa[n] = quadpoints[n].diffusivity;
			gp_Q[n]     = quadpoints[n].heat_source;
		}

		/* initialise element stiffness matrix */
		ierr = PetscMemzero(Re,sizeof(PetscScalar)*NODES_PER_EL_Q1_2D);CHKERRQ(ierr);
		
		/* form element stiffness matrix */
		SUPG2d_FormMass_FormResidual_2_qp(Re,dt,el_coords,el_V,el_phi,el_philast,gp_kappa,gp_Q,ngp,gp_xi,gp_weight);
		
		ierr = DMDASetValuesLocalStencil_AddValues_Scalar_Q1(LA_R,phi_el_lidx,Re);CHKERRQ(ierr);

	
		/* diagnostics */
		for (p=0; p<ngp; p++) {
			ConstructNi_Q1_2D(&gp_xi[2*p],Ni_p);
			ConstructGNi_Q1_2D(&gp_xi[2*p],GNi_p);
			ConstructGNx_Q1_2D(GNi_p,GNx_p,el_coords,&J_p);
			
			phi_p = 0.0;
			for (i=0; i<NODES_PER_EL_Q1_2D; i++) {
				phi_p = phi_p + Ni_p[i] * el_philast[i];
			}
			
			fac = gp_weight[p]*J_p;
			c = c + phi_p * fac;
		}
		
	}
  ierr = MPI_Allreduce(&c,&cg,1,MPIU_REAL,MPI_MIN,((PetscObject)da)->comm);CHKERRQ(ierr);
	PetscPrintf(PETSC_COMM_WORLD,"\\int \\phi dV = %1.12e \n", cg );
	
  /* tidy up local arrays (input) */
  ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
  PetscFunctionReturn(0);
}

/*
 Computes f - C.phi - M.phiDot
*/
#undef __FUNCT__  
#define __FUNCT__ "SUPGFormFunction_qp"
PetscErrorCode SUPGFormFunction_qp(PetscReal time,Vec X,PetscReal dt,Vec F,void *ctx)
{
  PhysCompEnergyCtx data  = (PhysCompEnergyCtx)ctx;
  DM             da,cda;
	Vec            philoc, philastloc, Fphiloc;
	Vec            Vloc;
	PetscScalar    *LA_philoc, *LA_philastloc, *LA_Fphiloc;
	PetscScalar    *LA_V;
  PetscErrorCode ierr;
	
  PetscFunctionBegin;
	da = data->daT;
	
	
	ierr = VecZeroEntries(F);CHKERRQ(ierr);
	
  ierr = DMGetLocalVector(da,&philoc);CHKERRQ(ierr);
  ierr = DMGetLocalVector(da,&philastloc);CHKERRQ(ierr);
	
  ierr = DMGetLocalVector(da,&Fphiloc);CHKERRQ(ierr);
	
	/* get local solution and time derivative */
	ierr = VecZeroEntries(philoc);CHKERRQ(ierr);
	ierr = DMGlobalToLocalBegin(da,X,ADD_VALUES,philoc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd  (da,X,ADD_VALUES,philoc);CHKERRQ(ierr);
	
	ierr = VecZeroEntries(philastloc);CHKERRQ(ierr);
	ierr = DMGlobalToLocalBegin(da,data->Told,ADD_VALUES,philastloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd  (da,data->Told,ADD_VALUES,philastloc);CHKERRQ(ierr);
	
	/* insert boundary conditions into local vectors */
	ierr = BCListInsertLocal(data->T_bclist,philoc);CHKERRQ(ierr);
	
	/* what do i do with the phi_dot */
	
	/* init residual */
	ierr = VecZeroEntries(Fphiloc);CHKERRQ(ierr);
	
	/* get arrays */
	ierr = VecGetArray(philoc,    &LA_philoc);CHKERRQ(ierr);
	ierr = VecGetArray(philastloc,&LA_philastloc);CHKERRQ(ierr);
	ierr = VecGetArray(Fphiloc,   &LA_Fphiloc);CHKERRQ(ierr);
	
	/* ============= */
	/* FORM_FUNCTION */
	
  /* get acces to the vector V */
  ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
  ierr = DMGetLocalVector(cda,&Vloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(cda,data->u_minus_V,INSERT_VALUES,Vloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(  cda,data->u_minus_V,INSERT_VALUES,Vloc);CHKERRQ(ierr);
  ierr = VecGetArray(Vloc,&LA_V);CHKERRQ(ierr);
	
	ierr = FormFunction_SUPG_T_qp(data,dt,da,LA_V, LA_philoc,LA_philastloc,LA_Fphiloc);CHKERRQ(ierr);
	
  ierr = VecRestoreArray(Vloc,&LA_V);CHKERRQ(ierr);
	
  ierr = DMRestoreLocalVector(cda,&Vloc);CHKERRQ(ierr);
	/* ============= */
	
	ierr = VecRestoreArray(Fphiloc,   &LA_Fphiloc);CHKERRQ(ierr);
	ierr = VecRestoreArray(philastloc,&LA_philastloc);CHKERRQ(ierr);
	ierr = VecRestoreArray(philoc,    &LA_philoc);CHKERRQ(ierr);
	
	
	/* do global fem summation */
	ierr = VecZeroEntries(F);CHKERRQ(ierr);
	
  ierr = DMLocalToGlobalBegin(da,Fphiloc,ADD_VALUES,F);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd  (da,Fphiloc,ADD_VALUES,F);CHKERRQ(ierr);
	
  ierr = DMRestoreLocalVector(da,&Fphiloc);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da,&philoc);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da,&philastloc);CHKERRQ(ierr);
	
	/* modify F for the boundary conditions, F_k = scale_k(x_k - phi_k) */
	ierr = BCListResidualDirichlet(data->T_bclist,X,F);CHKERRQ(ierr);
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "DASUPG2dComputeTimestep_qp"
PetscErrorCode DASUPG2dComputeTimestep_qp(PhysCompEnergyCtx data,PetscScalar Cr,Vec V,PetscScalar *DT)
{
  DM                     da,cda;
  Vec                    gcoords,local_V;
  PetscScalar            *LA_gcoords;
  PetscScalar            *LA_V;
  PetscScalar            el_coords[NODES_PER_EL_Q1_2D*NSD],el_V[NODES_PER_EL_Q1_2D*NSD];
  PetscScalar            dt_local_a,dt_local_d,dt_global_a,dt_global_d,dt_global,dt_element;
  PetscScalar            el_Pe,dta,dtd;
	CoefficientsEnergyEq   *quadpoints;
  PetscInt    nqp;
  PetscScalar *qp_xi;
  PetscScalar *qp_weight;
	PetscScalar qp_kappa[27];
	PetscInt nel,nen,e,n;
	const PetscInt *elnidx;
  PetscScalar GNi_p[NSD][NODES_PER_EL_Q1_2D],GNx_p[NSD][NODES_PER_EL_Q1_2D];
  PetscScalar qp_detJ[27];
	PetscScalar kappa_el,vol_el;
  PetscErrorCode         ierr;
	
  PetscFunctionBegin;
	
	da = data->daT;
	
	nqp       = data->Q->ngp;
	qp_xi     = data->Q->xi;
	qp_weight = data->Q->weight;

	
  /* setup for coords */
  ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
  ierr = DMDAGetGhostedCoordinates(da,&gcoords);CHKERRQ(ierr);
  ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
  /* get acces to the vector V */
  ierr = DMGetLocalVector(cda,&local_V);CHKERRQ(ierr);
  ierr = VecZeroEntries(local_V);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(cda,V,INSERT_VALUES,local_V);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(  cda,V,INSERT_VALUES,local_V);CHKERRQ(ierr);
  ierr = VecGetArray(local_V,&LA_V);CHKERRQ(ierr);
	
	
	ierr = DMDAGetElementsQ1(da,&nel,&nen,&elnidx);CHKERRQ(ierr);
  dt_local_a = 1.0e308;
  dt_local_d = 1.0e308;
	for (e=0;e<nel;e++) {
		/* get coords for the element */
		ierr = DMDAGetElementCoordinatesQ1_2D(el_coords,(PetscInt*)&elnidx[nen*e],LA_gcoords);CHKERRQ(ierr);
		
		/* get velocity for the element */
		ierr = DMDAGetVectorElementFieldQ1_2D(el_V,(PetscInt*)&elnidx[nen*e],LA_V);CHKERRQ(ierr);
		
		ierr = QuadratureVolumeEnergyGetCell(data->Q,e,&quadpoints);CHKERRQ(ierr);
		
		/* copy the diffusivity */
		for (n=0; n<nqp; n++) {
			qp_kappa[n] = quadpoints[n].diffusivity;
		}
		
		/* compute constants for the element */
		for (n=0; n<nqp; n++) {
			ConstructGNi_Q1_2D(&qp_xi[2*n],GNi_p);
			ConstructGNx_Q1_2D(GNi_p,GNx_p,el_coords,&qp_detJ[n]);
		}

		/* integral average of diffusivity */
		kappa_el = 0.0;
		vol_el = 0.0;
		for (n=0; n<nqp; n++) {
			kappa_el += qp_weight[n] * qp_kappa[n] * qp_detJ[n]; 
			vol_el   += qp_weight[n] * qp_detJ[n];
		}
		kappa_el = kappa_el / vol_el;
		
		//ierr = DASUPG2dComputeElementPecletNumber_qp(el_coords,el_V,kappa_el,&el_Pe);CHKERRQ(ierr);
		ierr = DASUPG2dComputeElementTimestep_qp(el_coords,el_V,kappa_el,&dta,&dtd);CHKERRQ(ierr);
		dt_local_a = PetscMin(dt_local_a,dta);
		
        /*dt_element = PetscMin(dta,dtd)
		dt_local_d = PetscMin(dt_local_d,dtd);
        if (isnan(dt_local_d)){
            PetscPrintf(PETSC_COMM_WORLD,"iel= %d  : vol_el = %1.4e : kappa_el = %1.4e \n", e, vol_el,kappa_el);
            //PetscPrintf(PETSC_COMM_WORLD,"x= %1.4e  : y = %1.4e : \n", e, el_coords,el_coords);
        }
         */
            
  }
  MPI_Allreduce(&dt_local_a,&dt_global_a,1,MPIU_REAL,MPI_MIN,((PetscObject)da)->comm);
    /*MPI_Allreduce(&dt_local_d,&dt_global_d,1,MPIU_REAL,MPI_MIN,((PetscObject)da)->comm);
	PetscPrintf(PETSC_COMM_WORLD,"advective dt = %1.4e : diffusive dt = %1.4e \n", dt_global_a,dt_global_d );
     dt_global = PetscMin(dt_global_a,dt_global_d);
     */
    PetscPrintf(PETSC_COMM_WORLD,"advective dt = %1.4e \n", dt_global_a);
	dt_global = dt_global_a;
    dt_global = dt_global * Cr;
    *DT = dt_global;

	PetscPrintf(PETSC_COMM_WORLD,"Timestep diagnostics:\n");
	PetscPrintf(PETSC_COMM_WORLD,"  el_dt_global: %e \n",dt_global);

	
  /* tidy up local arrays (input) */
  ierr = VecRestoreArray(gcoords,      &LA_gcoords  );CHKERRQ(ierr);
  ierr = VecRestoreArray(local_V,     &LA_V       );CHKERRQ(ierr);
	
  /* tidy up local vecs (input) */
  ierr = DMRestoreLocalVector(cda,&local_V       );CHKERRQ(ierr);
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatinOutputQ1Mesh2dNodalFieldVTS"
PetscErrorCode pTatinOutputQ1Mesh2dNodalFieldVTS(DM da,const PetscInt ncomponents,Vec X,const char filename[],const char fieldname[])
{
	PetscErrorCode ierr;
  Vec local_field;
  PetscScalar *LA_field;
	DMDAE dmdae;
	DM cda;
	Vec gcoords;
	DMDACoor2d **LA_gcoords;	
	PetscInt mx,my,cnt,d;
	PetscInt ei,ej,i,j,esi,esj;
	FILE*	vtk_fp = NULL;
	PetscInt gsi,gsj,gm,gn;
	
	if ((vtk_fp = fopen ( filename, "w")) == NULL)  {
		SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",filename );
	}
	
	ierr = DMGetDMDAE(da,&dmdae);CHKERRQ(ierr);
	
	ierr = DMDAGetGhostCorners(da,&gsi,&gsj,0,&gm,&gn,0);CHKERRQ(ierr);
//	gsi = dmdae->sgi;
//	gsj = dmdae->sgj;
	
//	ierr = DMDAGetCornersElementQ2(da,&esi,&esj,0,&mx,&my,0);CHKERRQ(ierr);
	esi = dmdae->si;
	esj = dmdae->sj;
	mx = dmdae->lmx;
	my = dmdae->lmy;
	
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(da,&gcoords);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,gcoords,&LA_gcoords);CHKERRQ(ierr);
	
  ierr = DMGetLocalVector(da,&local_field);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(da,X,INSERT_VALUES,local_field);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(da,X,INSERT_VALUES,local_field);CHKERRQ(ierr);
  ierr = VecGetArray(local_field,&LA_field);CHKERRQ(ierr);
	
	
	
	/* VTS HEADER - OPEN */	
	fprintf( vtk_fp, "<VTKFile type=\"StructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
	fprintf( vtk_fp, "  <StructuredGrid WholeExtent=\"%d %d %d %d %d %d\">\n", esi,esi+mx+1-1, esj,esj+my+1-1, 0,0);
	fprintf( vtk_fp, "    <Piece Extent=\"%d %d %d %d %d %d\">\n", esi,esi+mx+1-1, esj,esj+my+1-1, 0,0);
	
	/* VTS COORD DATA */	
	fprintf( vtk_fp, "    <Points>\n");
	fprintf( vtk_fp, "      <DataArray Name=\"coords\" type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	for (j=esj; j<esj+my+1; j++) {
		for (i=esi; i<esi+mx+1; i++) {
			fprintf( vtk_fp,"      %1.6e %1.6e %1.6e\n", LA_gcoords[j][i].x, LA_gcoords[j][i].y, 0.0 );
		}
	}
	fprintf( vtk_fp, "      </DataArray>\n");
	fprintf( vtk_fp, "    </Points>\n");
	
	/* VTS CELL DATA */	
	fprintf( vtk_fp, "    <CellData>\n");
	fprintf( vtk_fp, "    </CellData>\n");
	
	/* VTS NODAL DATA */
	fprintf( vtk_fp, "    <PointData>\n");

	fprintf( vtk_fp, "      <DataArray Name=\"%s\" type=\"Float64\" NumberOfComponents=\"%d\" format=\"ascii\">\n",fieldname,ncomponents);
	for (j=esj; j<esj+my+1; j++) {
		for (i=esi; i<esi+mx+1; i++) {
			cnt = (i-gsi) + (j-gsj)*gm;

			for (d=0; d<ncomponents; d++) {
				if (fabs(LA_field[ncomponents*cnt+d]) < 1.0e-32) {
					fprintf( vtk_fp,"%1.6e ", 0.0 );
				} else {
					fprintf( vtk_fp,"%1.6e ", LA_field[ncomponents*cnt+d] );
				}
			}
			fprintf( vtk_fp,"\n");
			
		}
	}
	fprintf( vtk_fp, "      </DataArray>\n");
	fprintf( vtk_fp, "    </PointData>\n");
	
	/* VTS HEADER - CLOSE */	
	fprintf( vtk_fp, "    </Piece>\n");
	fprintf( vtk_fp, "  </StructuredGrid>\n");
	fprintf( vtk_fp, "</VTKFile>\n");
	
  ierr = VecRestoreArray(local_field,&LA_field);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(da,&local_field);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(cda,gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	fclose( vtk_fp );
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DAQ1PieceExtendForGhostLevelZero"
PetscErrorCode DAQ1PieceExtendForGhostLevelZero( FILE *vtk_fp, int indent_level, DM dau, const char local_file_prefix[] )
{
	PetscMPIInt nproc,rank;
	MPI_Comm comm;
	const PetscInt *lx,*ly,*lz;
	PetscInt M,N,P,pM,pN,pP,sum;
	PetscInt i,j,k,II,dim,esi,esj,esk,mx,my,mz;
	PetscInt *olx,*oly,*olz;
	PetscInt *lmx,*lmy,*lmz,*tmp;
	DMDAE dmdae;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	/* create file name */
	PetscObjectGetComm( (PetscObject)dau, &comm );
	MPI_Comm_size( comm, &nproc );
	MPI_Comm_rank( comm, &rank );
	
	ierr = DMDAGetInfo( dau, &dim, &M,&N,&P, &pM,&pN,&pP, 0, 0, 0,0,0, 0 );CHKERRQ(ierr);
//	ierr = DMDAGetOwnershipRangesElementQ2(dau,&pM,&pN,&pP,&olx,&oly,&olz,&lmx,&lmy,&lmz);CHKERRQ(ierr);
	ierr = DMGetDMDAE(dau,&dmdae);CHKERRQ(ierr);
	olx = dmdae->lsip;
	oly = dmdae->lsjp;
	olz = dmdae->lskp;

	lmx = dmdae->lmxp;
	lmy = dmdae->lmyp;
	lmz = dmdae->lmzp;
	
	if (dim==3) {
		for( k=0;k<pP;k++ ) {
			for( j=0;j<pN;j++ ) {
				for( i=0;i<pM;i++ ) {
					char *name;
					PetscInt procid = i + j*pM + k*pM*pN; /* convert proc(i,j,k) to pid */
					asprintf( &name, "%s-subdomain%1.5d.vts", local_file_prefix, procid );
					for( II=0; II<indent_level; II++ ) {
						if(vtk_fp) fprintf(vtk_fp,"  ");
					}
					if(vtk_fp) fprintf( vtk_fp, "<Piece Extent=\"%d %d %d %d %d %d\"      Source=\"%s\"/>\n",
														 olx[i],olx[i]+lmx[i],
														 oly[j],oly[j]+lmy[j],
														 olz[k],olz[k]+lmz[k],
														 name);
					free(name);
				}
			}
		}
	} else if (dim==2) {
		for( j=0;j<pN;j++ ) {
			for( i=0;i<pM;i++ ) {
				char *name;
				PetscInt procid = i + j*pM; /* convert proc(i,j,k) to pid */
				asprintf( &name, "%s-subdomain%1.5d.vts", local_file_prefix, procid );
				for( II=0; II<indent_level; II++ ) {
					if(vtk_fp) fprintf(vtk_fp,"  ");
				}
				if(vtk_fp) fprintf( vtk_fp, "<Piece Extent=\"%d %d %d %d 0 0\"      Source=\"%s\"/>\n",
													 olx[i],olx[i]+lmx[i],
													 oly[j],oly[j]+lmy[j],
													 name);
				free(name);
			}
		}
	}
/*	
	ierr = PetscFree(olx);CHKERRQ(ierr);
	ierr = PetscFree(oly);CHKERRQ(ierr);
	ierr = PetscFree(olz);CHKERRQ(ierr);
	
	ierr = PetscFree(lmx);CHKERRQ(ierr);
	ierr = PetscFree(lmy);CHKERRQ(ierr);
	ierr = PetscFree(lmz);CHKERRQ(ierr);
*/	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatinOutputMeshTemperaturePVTS"
PetscErrorCode pTatinOutputMeshTemperaturePVTS(pTatinCtx ctx,const char prefix[],const char name[])
{
	PetscErrorCode ierr;
	PetscInt mx,my,cnt;
	PetscInt ei,ej,i,j,esi,esj;
  PetscScalar *LA_cell_data;
	FILE*	vtk_fp = NULL;
	PetscInt M,N,P,swidth;
	PetscMPIInt rank;
	DM da;
	
	
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	vtk_fp = NULL;
	if (rank==0) {
		if ((vtk_fp = fopen ( name, "w")) == NULL)  {
			SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
		}
	}
	
	
	da = ctx->phys_energy->daT;
	
	/* VTS HEADER - OPEN */	
	if(vtk_fp) fprintf( vtk_fp, "<?xml version=\"1.0\"?>\n");
	if(vtk_fp) fprintf( vtk_fp, "<VTKFile type=\"PStructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
	
	DMDAGetInfo( da, 0, &M,&N,&P, 0,0,0, 0,&swidth, 0,0,0, 0 );
	if(vtk_fp) fprintf( vtk_fp, "  <PStructuredGrid GhostLevel=\"%d\" WholeExtent=\"%d %d %d %d %d %d\">\n", swidth, 0,M-1, 0,N-1, 0,P-1 ); /* note overlap = 1 for Q1 */
	//	fprintf( vtk_fp, "    <Piece Extent=\"%d %d %d %d %d %d\">\n", esi,esi+2*mx+1-1, esj,esj+2*my+1-1, 0,0);
	
	/* VTS COORD DATA */	
	if(vtk_fp) fprintf( vtk_fp, "    <PPoints>\n");
	if(vtk_fp) fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\"/>\n");
	if(vtk_fp) fprintf( vtk_fp, "    </PPoints>\n");
	
	
	/* VTS CELL DATA */	
	if(vtk_fp) fprintf( vtk_fp, "    <PCellData>\n");
	if(vtk_fp) fprintf( vtk_fp, "    </PCellData>\n");
	
	
	/* VTS NODAL DATA */
	if(vtk_fp) fprintf( vtk_fp, "    <PPointData>\n");
	if(vtk_fp) fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"temperature\" NumberOfComponents=\"1\"/>\n");
	if(vtk_fp) fprintf( vtk_fp, "    </PPointData>\n");
	
	/* write out the parallel information */
	//DAViewVTK_write_PieceExtend(vtk_fp,2,dau,prefix);
	ierr = DAQ1PieceExtendForGhostLevelZero(vtk_fp,2,da,prefix);CHKERRQ(ierr);
	
	
	/* VTS HEADER - CLOSE */	
	if(vtk_fp) fprintf( vtk_fp, "  </PStructuredGrid>\n");
	if(vtk_fp) fprintf( vtk_fp, "</VTKFile>\n");
	
	if(vtk_fp) fclose( vtk_fp );
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatinOutputParaViewMeshTemperature"
PetscErrorCode pTatinOutputParaViewMeshTemperature(pTatinCtx user,Vec X,const char path[],const char prefix[])
{
	char *vtkfilename,*filename;
	PetscMPIInt rank;
	PetscBool use_dimensional_units = PETSC_FALSE;
	pTatinUnits *units;
	double L_bar;
	Vec coords;
	DM daT;
	PetscErrorCode ierr;
	
	ierr = PetscOptionsGetBool(PETSC_NULL,"-use_dimensional_units",&use_dimensional_units,PETSC_NULL);CHKERRQ(ierr);

	ierr = pTatinGenerateParallelVTKName(prefix,"vts",&vtkfilename);CHKERRQ(ierr);
	if (path) {
		asprintf(&filename,"%s/%s",path,vtkfilename);
	} else {
		asprintf(&filename,"./%s",vtkfilename);
	}

	daT = user->phys_energy->daT;
	
	/* apply scaling */
	if (use_dimensional_units) {
		units = &user->units;
		/* Get length scale */
		L_bar = units->si_length->characteristic_scale;

		/* scale coordinates */
		ierr = DMDAGetGhostedCoordinates(daT,&coords);CHKERRQ(ierr);
		//UnitsApplyScalingToArray(units->si_length,(int)N,(double*)LA_fields,(double*)LA_fields);
		ierr = VecScale(coords,L_bar);CHKERRQ(ierr);
		ierr = DMDAGetCoordinates(daT,&coords);CHKERRQ(ierr);
		ierr = VecScale(coords,L_bar);CHKERRQ(ierr);
	}

	/* write out file */
	ierr = pTatinOutputQ1Mesh2dNodalFieldVTS(daT,1,X,filename,"temperature");CHKERRQ(ierr);

	/* apply inverse scaling */
	if (use_dimensional_units) {
		units = &user->units;
		/* Get length scale */
		L_bar = units->si_length->characteristic_scale;

		/* unscale coordinates */
		ierr = DMDAGetGhostedCoordinates(daT,&coords);CHKERRQ(ierr);
		//UnitsApplyInverseScalingToArray(units->si_length,(int)N,(double*)LA_fields,(double*)LA_fields);
		ierr = VecScale(coords,1.0/L_bar);CHKERRQ(ierr);
		ierr = DMDAGetCoordinates(daT,&coords);CHKERRQ(ierr);
		ierr = VecScale(coords,1.0/L_bar);CHKERRQ(ierr);
	}
	
	free(filename);
	free(vtkfilename);
	
	ierr = pTatinGenerateVTKName(prefix,"pvts",&vtkfilename);CHKERRQ(ierr);
	if (path) {
		asprintf(&filename,"%s/%s",path,vtkfilename);
	} else {
		asprintf(&filename,"./%s",vtkfilename);
	}
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	//	if (rank==0) {
	ierr = pTatinOutputMeshTemperaturePVTS(user,prefix,filename);CHKERRQ(ierr);
	//	}
	free(filename);
	free(vtkfilename);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatinOutputQuadratureThermalAveragedFieldsVTS"
PetscErrorCode pTatinOutputQuadratureThermalAveragedFieldsVTS(PhysCompEnergyCtx ctx,const char filename[])
{
	PetscErrorCode ierr;
	DMDAE dmdae;
	DM da,cda;
	Vec gcoords;
	DMDACoor2d **LA_gcoords;	
	PetscInt mx,my,cnt,d,p,nqp,e;
	PetscInt ei,ej,i,j,esi,esj;
	FILE*	vtk_fp = NULL;
  PetscScalar *LA_cell_data;
	PetscInt gsi,gsj,gm,gn;
	CoefficientsEnergyEq *quadpoints;
	
	if ((vtk_fp = fopen ( filename, "w")) == NULL)  {
		SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",filename );
	}
	
	da = ctx->daT;
	ierr = DMGetDMDAE(da,&dmdae);CHKERRQ(ierr);
	
	ierr = DMDAGetGhostCorners(da,&gsi,&gsj,0,&gm,&gn,0);CHKERRQ(ierr);
	
	//	ierr = DMDAGetCornersElementQ2(da,&esi,&esj,0,&mx,&my,0);CHKERRQ(ierr);
	esi = dmdae->si;
	esj = dmdae->sj;
	mx = dmdae->lmx;
	my = dmdae->lmy;
	
	ierr = PetscMalloc(sizeof(PetscScalar)*mx*my,&LA_cell_data);CHKERRQ(ierr);
	
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(da,&gcoords);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	/* VTS HEADER - OPEN */	
	fprintf( vtk_fp, "<VTKFile type=\"StructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
	fprintf( vtk_fp, "  <StructuredGrid WholeExtent=\"%d %d %d %d %d %d\">\n", esi,esi+mx+1-1, esj,esj+my+1-1, 0,0);
	fprintf( vtk_fp, "    <Piece Extent=\"%d %d %d %d %d %d\">\n", esi,esi+mx+1-1, esj,esj+my+1-1, 0,0);
	
	/* VTS COORD DATA */	
	fprintf( vtk_fp, "    <Points>\n");
	fprintf( vtk_fp, "      <DataArray Name=\"coords\" type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\">\n");
	for (j=esj; j<esj+my+1; j++) {
		for (i=esi; i<esi+mx+1; i++) {
			fprintf( vtk_fp,"      %1.6e %1.6e %1.6e\n", LA_gcoords[j][i].x, LA_gcoords[j][i].y, 0.0 );
		}
	}
	fprintf( vtk_fp, "      </DataArray>\n");
	fprintf( vtk_fp, "    </Points>\n");
	
	/* VTS CELL DATA */	
	fprintf( vtk_fp, "    <CellData>\n");
	
	/* get quad point information */
	nqp = ctx->Q->ngp;
	
	/* =============================================================================== */
	/* integrated diffusivty */
	ierr = PetscMemzero(LA_cell_data,sizeof(PetscScalar)*mx*my);CHKERRQ(ierr);

	for (e=0; e<mx*my; e++) {
		ierr = QuadratureVolumeEnergyGetCell(ctx->Q,e,&quadpoints);CHKERRQ(ierr);
		
		for (p=0; p<nqp; p++) {
			LA_cell_data[e] += quadpoints[p].diffusivity;
		}
		LA_cell_data[e] = LA_cell_data[e] / (double)(nqp);
	}
	
	fprintf( vtk_fp, "      <DataArray Name=\"avg_kappa\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	fprintf( vtk_fp,"      ");
	for (ej=0; ej<my; ej++) { 
		for (ei=0; ei<mx; ei++) {
			fprintf( vtk_fp,"%1.6e ", LA_cell_data[ei+ej*mx] );
		}
	}
	fprintf( vtk_fp,"\n");
	fprintf( vtk_fp, "      </DataArray>\n");
	
	/* =============================================================================== */
	/* integrated diffusivty */
	ierr = PetscMemzero(LA_cell_data,sizeof(PetscScalar)*mx*my);CHKERRQ(ierr);
	
	for (e=0; e<mx*my; e++) {
		ierr = QuadratureVolumeEnergyGetCell(ctx->Q,e,&quadpoints);CHKERRQ(ierr);
		
		for (p=0; p<nqp; p++) {
			LA_cell_data[e] += quadpoints[p].heat_source;
		}
		LA_cell_data[e] = LA_cell_data[e] / (double)(nqp);
	}
	
	fprintf( vtk_fp, "      <DataArray Name=\"avg_H\" type=\"Float64\" NumberOfComponents=\"1\" format=\"ascii\">\n");
	fprintf( vtk_fp,"      ");
	for (ej=0; ej<my; ej++) { 
		for (ei=0; ei<mx; ei++) {
			fprintf( vtk_fp,"%1.6e ", LA_cell_data[ei+ej*mx] );
		}
	}
	fprintf( vtk_fp,"\n");
	fprintf( vtk_fp, "      </DataArray>\n");
	/* =============================================================================== */
	
	
	fprintf( vtk_fp, "    </CellData>\n");
	
	/* VTS NODAL DATA */
	fprintf( vtk_fp, "    <PointData>\n");
	fprintf( vtk_fp, "    </PointData>\n");
	
	/* VTS HEADER - CLOSE */	
	fprintf( vtk_fp, "    </Piece>\n");
	fprintf( vtk_fp, "  </StructuredGrid>\n");
	fprintf( vtk_fp, "</VTKFile>\n");
	
	ierr = DMDAVecRestoreArray(cda,gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	fclose( vtk_fp );
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatinOutputQuadratureThermalAveragedFieldsPVTS"
PetscErrorCode pTatinOutputQuadratureThermalAveragedFieldsPVTS(PhysCompEnergyCtx ctx,const char prefix[],const char name[])
{
	PetscErrorCode ierr;
	PetscInt mx,my,cnt;
	PetscInt ei,ej,i,j,esi,esj;
  PetscScalar *LA_cell_data;
	FILE*	vtk_fp = NULL;
	PetscInt M,N,P,swidth;
	PetscMPIInt rank;
	DM da;
	
	
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	vtk_fp = NULL;
	if (rank==0) {
		if ((vtk_fp = fopen ( name, "w")) == NULL)  {
			SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file %s",name );
		}
	}
	
	
	da = ctx->daT;
	
	/* VTS HEADER - OPEN */	
	if(vtk_fp) fprintf( vtk_fp, "<?xml version=\"1.0\"?>\n");
	if(vtk_fp) fprintf( vtk_fp, "<VTKFile type=\"PStructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n");
	
	DMDAGetInfo( da, 0, &M,&N,&P, 0,0,0, 0,&swidth, 0,0,0, 0 );
	if(vtk_fp) fprintf( vtk_fp, "  <PStructuredGrid GhostLevel=\"%d\" WholeExtent=\"%d %d %d %d %d %d\">\n", 0, 0,M-1, 0,N-1, 0,P-1 ); /* note overlap = 1 for Q1 */
	
	/* VTS COORD DATA */	
	if(vtk_fp) fprintf( vtk_fp, "    <PPoints>\n");
	if(vtk_fp) fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"Points\" NumberOfComponents=\"3\"/>\n");
	if(vtk_fp) fprintf( vtk_fp, "    </PPoints>\n");
	
	
	/* VTS CELL DATA */	
	if(vtk_fp) fprintf( vtk_fp, "    <PCellData>\n");
	if(vtk_fp) fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"avg_kappa\" NumberOfComponents=\"1\"/>\n");
	if(vtk_fp) fprintf( vtk_fp, "      <PDataArray type=\"Float64\" Name=\"avg_H\" NumberOfComponents=\"1\"/>\n");
	if(vtk_fp) fprintf( vtk_fp, "    </PCellData>\n");
	
	
	/* VTS NODAL DATA */
	if(vtk_fp) fprintf( vtk_fp, "    <PPointData>\n");
	if(vtk_fp) fprintf( vtk_fp, "    </PPointData>\n");
	
	/* write out the parallel information */
	ierr = DAQ1PieceExtendForGhostLevelZero(vtk_fp,2,da,prefix);CHKERRQ(ierr);
	
	
	/* VTS HEADER - CLOSE */	
	if(vtk_fp) fprintf( vtk_fp, "  </PStructuredGrid>\n");
	if(vtk_fp) fprintf( vtk_fp, "</VTKFile>\n");
	
	if(vtk_fp) fclose( vtk_fp );
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatinParaViewOutputQuadratureThermalAveragedFields"
PetscErrorCode pTatinParaViewOutputQuadratureThermalAveragedFields(pTatinCtx user,const char path[],const char prefix[])
{
	char *vtkfilename,*filename;
	PetscMPIInt rank;
	
	PetscErrorCode ierr;
	
	ierr = pTatinGenerateParallelVTKName(prefix,"vts",&vtkfilename);CHKERRQ(ierr);
	if (path) {
		asprintf(&filename,"%s/%s",path,vtkfilename);
	} else {
		asprintf(&filename,"./%s",vtkfilename);
	}
	
	ierr = pTatinOutputQuadratureThermalAveragedFieldsVTS(user->phys_energy,filename);CHKERRQ(ierr);
	free(filename);
	free(vtkfilename);
	
	ierr = pTatinGenerateVTKName(prefix,"pvts",&vtkfilename);CHKERRQ(ierr);
	if (path) {
		asprintf(&filename,"%s/%s",path,vtkfilename);
	} else {
		asprintf(&filename,"./%s",vtkfilename);
	}
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	//	if (rank==0) {
	ierr = pTatinOutputQuadratureThermalAveragedFieldsPVTS(user->phys_energy,prefix,filename);CHKERRQ(ierr);
	//	}
	free(filename);
	free(vtkfilename);
	
	PetscFunctionReturn(0);
}

