
#include "pTatin2d.h"
#include "element_type_Q2.h"
#include "physcomp_SPM.h"


void Q2_ConstructNi_1D( PetscScalar xi, PetscScalar Ni[] )
{
	Ni[0] = 0.5 * xi * (xi-1.0); // 0.5 * ( xi^2 - xi )
	Ni[1] = (1.0+xi) * (1.0-xi); // 1 - xi^2
	Ni[2] = 0.5 * (1.0+xi) * xi; // 0.5 * ( xi^2 + xi )
}

void Q2_ConstructGNi_1D( PetscScalar xi, PetscScalar GNi[] )
{
	GNi[0] = 0.5 * ( 2.0*xi - 1.0 );
	GNi[1] = - 2.0*xi;
	GNi[2] = 0.5 * ( 2.0*xi + 1.0 );
}


void Q2_ConstructGNx_1D(PetscScalar GNi[Q2_NODES_PER_EL_1D],PetscScalar GNx[Q2_NODES_PER_EL_1D],PetscScalar coords[],PetscScalar *det_J)
{
  PetscScalar J,iJ;
  PetscInt    i;
	
  J = 0.0;
  for (i = 0; i < Q2_NODES_PER_EL_1D; i++) {
    J = J + GNi[i]*coords[i];      
  }
	iJ = 1.0/J;	
	for (i = 0; i < Q2_NODES_PER_EL_1D; i++) {
    GNx[i] = GNi[i]*iJ;
    
  }
  *det_J = PetscAbsScalar(J);
}

void QuadratureCreateGauss_3pnt_1D(PetscInt *ngp,PetscScalar gp_xi[],PetscScalar gp_weight[])
{
	*ngp = 3;
	gp_xi[0] = -0.774596669241483;    
	gp_xi[1] = 0.0; 
	gp_xi[2] =  0.774596669241483;
	
	gp_weight[0] = 0.555555555555556;
	gp_weight[1] = 0.888888888888889; 
	gp_weight[2] = 0.555555555555556;
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureSPMCreate"
PetscErrorCode QuadratureSPMCreate(PetscInt ncells,QuadratureSPM *Q)
{
	QuadratureSPM quadrature;
	PetscInt p,ngp;
	PetscScalar gp_xi[MAX_QUAD_PNTS],gp_weight[MAX_QUAD_PNTS];
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	ierr = PetscMalloc( sizeof(struct _p_QuadratureSPM), &quadrature);CHKERRQ(ierr);
	
	PetscPrintf(PETSC_COMM_WORLD,"QuadratureSPMCreate:\n");
	PetscPrintf(PETSC_COMM_WORLD,"\tUsing 3 pnt Gauss Legendre quadrature\n");
	QuadratureCreateGauss_3pnt_1D(&ngp,gp_xi,gp_weight);
	quadrature->ncells = ncells;
	if (ncells!=0) {
		ierr = PetscMalloc( sizeof(CoefficientsSPMEq)*ncells*ngp, &quadrature->cellproperties);CHKERRQ(ierr);
		ierr = PetscMemzero(quadrature->cellproperties,sizeof(CoefficientsSPMEq)*ncells*ngp);CHKERRQ(ierr);
		
		/* init values to zero */
		for (p=0; p<ncells*ngp; p++) {
			quadrature->cellproperties[p].diffusivity = 0.0;
			quadrature->cellproperties[p].sediment_source = 0.0;
		}
		PetscPrintf(PETSC_COMM_WORLD,"\tCoefficientsSPMEq using %1.2lf MBytes \n", (double)(sizeof(CoefficientsSPMEq)*ncells*ngp)*1.0e-6 );
	}
	
	
	/* allocate */
	quadrature->ngp = ngp;
	ierr = PetscMalloc( sizeof(PetscScalar)*ngp, &quadrature->xi );CHKERRQ(ierr);
	ierr = PetscMalloc( sizeof(PetscScalar)*ngp, &quadrature->weight );CHKERRQ(ierr);
	/* copy */
	for (p=0; p<ngp; p++) {
		quadrature->xi[p] = gp_xi[p];
		quadrature->weight[p] = gp_weight[p];
	}
	
	*Q = quadrature;
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureSPMGetCell"
PetscErrorCode QuadratureSPMGetCell(QuadratureSPM Q,PetscInt cidx,CoefficientsSPMEq **points)
{
	PetscFunctionBegin;
	if (cidx>=Q->ncells) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_SIZ,"cidx > max cells");
	}
	*points = &Q->cellproperties[cidx*Q->ngp];
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PhysComp_SPMEquationCreate1"
PetscErrorCode PhysComp_SPMEquationCreate1(PhysCompSPMCtx *p)
{
	PhysCompSPMCtx phys;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	ierr = PetscMalloc(sizeof(struct _p_PhysCompSPMCtx),&phys);CHKERRQ(ierr);
	ierr = PetscMemzero(phys,sizeof(struct _p_PhysCompSPMCtx));CHKERRQ(ierr);
	*p = phys;
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PhysComp_SPMEquationCreate2"
PetscErrorCode PhysComp_SPMEquationCreate2(DM davq2,PhysCompSPMCtx *p)
{
	PhysCompSPMCtx phys;
	DM dmsq2;
	PetscInt ncells;
	PetscReal gmin[3],gmax[3];
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	/* create structure */
	ierr = PhysComp_SPMEquationCreate1(&phys);CHKERRQ(ierr);
	
	ierr = DMDAGetSizeElementQ2(davq2,&ncells,PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
	ierr = DMDAGetBoundingBox(davq2,gmin,gmax);CHKERRQ(ierr);
	phys->mx = ncells;
	
	// TODO
	//ierr = DMDACreate1DQ2FromSurfaceOf2DQ2(davq2,&dmsq2);CHKERRQ(ierr);
	ierr = DMDACreate1d(PETSC_COMM_SELF,DMDA_BOUNDARY_NONE, 2*ncells+1,1,2,PETSC_NULL,&dmsq2);CHKERRQ(ierr);
	
	ierr = DMDASetUniformCoordinates(dmsq2,gmin[0],gmax[0],PETSC_NULL,PETSC_NULL,PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
	
	phys->daSPM = dmsq2;
	
	ierr = DMCreateGlobalVector(dmsq2,&phys->height);CHKERRQ(ierr);
	ierr = DMCreateGlobalVector(dmsq2,&phys->height_old);CHKERRQ(ierr);	
	
	ierr   = QuadratureSPMCreate(ncells,&phys->Q);CHKERRQ(ierr);
	ierr   = DMDABCListCreate(phys->daSPM,&phys->bclist);CHKERRQ(ierr);
	
	*p = phys;
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "SPMFormJacobian"
PetscErrorCode SPMFormJacobian(PetscReal time,Vec X,PetscReal dt,Mat *A,Mat *B,MatStructure *mstr,void *ctx)
{
	PhysCompSPMCtx data = (PhysCompSPMCtx)ctx;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
	DM          da,cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt    p,i,j;
	PetscScalar ADe[Q2_NODES_PER_EL_1D*Q2_NODES_PER_EL_1D],el_coords[Q2_NODES_PER_EL_1D],el_V[Q2_NODES_PER_EL_1D];
	PetscScalar gp_kappa[27];
	PetscScalar J_p,fac;
	PetscScalar kappa_p,sediment_source_p;
	/**/
	PetscInt nel,nen,e,n;
	PetscScalar Ni_p[Q2_NODES_PER_EL_1D];
	PetscScalar GNi_p[Q2_NODES_PER_EL_1D],GNx_p[Q2_NODES_PER_EL_1D];
	const PetscInt *elnidx;
	BCList bclist;
	PetscInt nxg,Len_local, *gidx_bclocal;
	PetscInt *gidx,elgidx[Q2_NODES_PER_EL_1D],elgidx_bc[Q2_NODES_PER_EL_1D];
	
	CoefficientsSPMEq   *quadpoints;
	PetscErrorCode ierr;
	
	
	PetscFunctionBegin;
	da     = data->daSPM;
	bclist = data->bclist;
	
	/* trash old entries */
	ierr = MatZeroEntries(*B);CHKERRQ(ierr);
	
	/* quadrature */
	ngp       = data->Q->ngp;
	gp_xi     = data->Q->xi;
	gp_weight = data->Q->weight;
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(da,&gcoords);CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	
	/* stuff for eqnums */
	ierr = DMDAGetGlobalIndices(da,0,&gidx);CHKERRQ(ierr);
	ierr = BCListGetDofIdx(bclist,0,0,&Len_local,&gidx_bclocal);CHKERRQ(ierr);
	
	
	nel = data->mx;
	
	
	for (e=0;e<nel;e++) {
		/* get coords for the element */
		el_coords[0] = LA_gcoords[2*e+0];
		el_coords[1] = LA_gcoords[2*e+1];
		el_coords[2] = LA_gcoords[2*e+2];
		
		ierr = QuadratureSPMGetCell(data->Q,e,&quadpoints);CHKERRQ(ierr);
		
		/* copy the diffusivity */
		for (n=0; n<ngp; n++) {
			gp_kappa[n] = quadpoints[n].diffusivity;
		}
		
		/* initialise element stiffness matrix */
		ierr = PetscMemzero(ADe,sizeof(PetscScalar)*Q2_NODES_PER_EL_1D*Q2_NODES_PER_EL_1D);CHKERRQ(ierr);
		
		/* form element stiffness matrix */
		
		for (p = 0; p < ngp; p++) {
			Q2_ConstructNi_1D(gp_xi[p],Ni_p);
			Q2_ConstructGNi_1D(gp_xi[p],GNi_p);
			Q2_ConstructGNx_1D(GNi_p,GNx_p,el_coords,&J_p);
			fac = gp_weight[p]*J_p;
			kappa_p = gp_kappa[p];
			for (i=0; i<Q2_NODES_PER_EL_1D; i++) {
				for (j=0; j<Q2_NODES_PER_EL_1D; j++) {
					ADe[j+i*Q2_NODES_PER_EL_1D] += fac * ( -dt * kappa_p * ( GNx_p[i] * GNx_p[j] )
																								- Ni_p[i] * Ni_p[j]);
				}
			}
		}
		
		
		/* insert element matrix into global matrix */
		for (i=0; i<3; i++)  {
			elgidx[i]    = gidx[2*e+i];
			elgidx_bc[i] = gidx_bclocal[2*e+i];
		}
		
		ierr = MatSetValues(*B,Q2_NODES_PER_EL_1D,elgidx_bc, Q2_NODES_PER_EL_1D,elgidx_bc, ADe, ADD_VALUES );CHKERRQ(ierr);
	}
	/* tidy up */
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	/* partial assembly */
	ierr = MatAssemblyBegin(*B, MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(*B, MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
	
	/* boundary conditions */
	{
		PetscInt Len_ghost,*gidx_bcghost;
		PetscBool is_dirich;
		PetscInt nproc,*procs,*mapL,**mapIdx;
		ISLocalToGlobalMapping map;
		PetscMPIInt rank;
		
		MPI_Comm_rank( ((PetscObject)da)->comm,&rank);
		ierr = BCListGetDofIdx(bclist,0,0,&Len_ghost,&gidx_bcghost);CHKERRQ(ierr);
		
		ierr = DMGetLocalToGlobalMapping(da,&map);CHKERRQ(ierr);
		ierr = ISLocalToGlobalMappingGetInfo(map,&nproc,&procs,&mapL,&mapIdx);CHKERRQ(ierr);
		
		if (Len_ghost!=mapL[rank]) {
			//			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"map indices are fucked up");
		}
		for (i=0; i<Len_ghost; i++) {
			ierr = BCListIsDirichlet(gidx_bcghost[i],&is_dirich);CHKERRQ(ierr);
			if (is_dirich) {
				ierr = MatSetValueLocal(*B,i,i,1.0,INSERT_VALUES);CHKERRQ(ierr);
			}
		}
	}
	
	/* assemble */
	ierr = MatAssemblyBegin(*B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(*B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	if (*A != *B) {
		ierr = MatAssemblyBegin(*A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
		ierr = MatAssemblyEnd(*A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	}
	*mstr = SAME_NONZERO_PATTERN;
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "FormFunction_SPM"
PetscErrorCode FormFunction_SPM( PhysCompSPMCtx data,PetscReal dt,DM da,PetscScalar *LA_height,
																PetscScalar *LA_height_old, PetscScalar *LA_R)
{
	Vec kappa;
	DM                     cda;
	Vec                    gcoords;
	PetscScalar            *LA_gcoords;
	MatStencil             height_eqn[Q2_NODES_PER_EL_1D];
	PetscInt               sex,mx;
	PetscInt               ei;
	PetscScalar            Re[Q2_NODES_PER_EL_1D];
	PetscScalar            el_coords[Q2_NODES_PER_EL_1D],el_height[Q2_NODES_PER_EL_1D],el_height_old[Q2_NODES_PER_EL_1D];
	PetscScalar gp_kappa[27];
	PetscScalar gp_Q[27];
	PetscInt nel,nen,e,n;
	const PetscInt *elnidx;
	PetscScalar kappa_p,sedsource_p;
	PetscScalar height_p,height_old_p,topograd_p;
	PetscInt    ngp;
	PetscScalar *gp_xi;
	PetscScalar *gp_weight;
	PetscScalar Ni_p[Q2_NODES_PER_EL_1D];
	PetscScalar GNi_p[Q2_NODES_PER_EL_1D],GNx_p[Q2_NODES_PER_EL_1D];
	PetscInt height_el_lidx[Q2_NODES_PER_EL_1D];
	PetscScalar J_p,fac;
	PetscInt    p,i,j;
	PetscReal   cg,c = 0.0;
	CoefficientsSPMEq   *quadpoints;
	PetscErrorCode         ierr;
	
	PetscFunctionBegin;
	
	/* quadrature */
	ngp       = data->Q->ngp;
	gp_xi     = data->Q->xi;
	gp_weight = data->Q->weight;
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(da,&gcoords);CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	nel = data->mx;
	nen = Q2_NODES_PER_EL_1D;
	
	
	
	for (e=0; e<nel; e++) {
		
		height_el_lidx[0] = 2*e + 0;
		height_el_lidx[1] = 2*e + 1;
		height_el_lidx[2] = 2*e + 2;
		
		/* get coords for the element */
		el_coords[0] = LA_gcoords[2*e + 0];
		el_coords[1] = LA_gcoords[2*e + 1];
		el_coords[2] = LA_gcoords[2*e + 2];
		
		el_height[0] = LA_height[2*e + 0];
		el_height[1] = LA_height[2*e + 1];
		el_height[2] = LA_height[2*e + 2];
		
		el_height_old[0] = LA_height_old[2*e + 0];
		el_height_old[1] = LA_height_old[2*e + 1];
		el_height_old[2] = LA_height_old[2*e + 2];
		
		ierr = QuadratureSPMGetCell(data->Q,e,&quadpoints);CHKERRQ(ierr);
		
		/* initialise element stiffness matrix */
		ierr = PetscMemzero(Re,sizeof(PetscScalar)*Q2_NODES_PER_EL_1D);CHKERRQ(ierr);
		
		for (n=0; n<ngp; n++) {
			Q2_ConstructNi_1D(gp_xi[n],Ni_p);
			Q2_ConstructGNi_1D(gp_xi[n],GNi_p);
			Q2_ConstructGNx_1D(GNi_p,GNx_p,el_coords,&J_p);
			fac = gp_weight[n]*J_p;
			/* copy the diffusivity and force */	
			kappa_p       = quadpoints[n].diffusivity;
			sedsource_p   = quadpoints[n].sediment_source;
			/* compute the heights and topo gradients */	
			height_p        = 0.0;
			height_old_p    = 0.0;
			topograd_p      = 0.0;
			for (j=0; j<Q2_NODES_PER_EL_1D; j++) {
				height_p       += Ni_p[j] * el_height[j];      
				height_old_p   += Ni_p[j] * el_height_old[j];  
				topograd_p     += GNx_p[j]* el_height[j];
			}
			for (i = 0; i < Q2_NODES_PER_EL_1D; i++) {
				Re[ i ] += fac * (-dt*GNx_p[i]*kappa_p*topograd_p + Ni_p[i]*height_old_p - Ni_p[i]*height_p - dt*Ni_p[i]*sedsource_p);
			}
		}
		
		LA_R[2*e+0] += Re[0];
		LA_R[2*e+1] += Re[1];
		LA_R[2*e+2] += Re[2];
		
	}
	/* tidy up local arrays (input) */
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

/*
 Computes f - C.phi - M.phiDot
 */
#undef __FUNCT__  
#define __FUNCT__ "SPMFormFunction"
PetscErrorCode SPMFormFunction(PetscReal time,Vec X,PetscReal dt,Vec F,void *ctx)
{
	PhysCompSPMCtx data  = (PhysCompSPMCtx)ctx;
	DM             da,cda;
	Vec            heightloc, heightlastloc, Fheightloc;
	PetscScalar    *LA_heightloc, *LA_heightlastloc, *LA_Fheightloc;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	da = data->daSPM;
	
	
	ierr = VecZeroEntries(F);CHKERRQ(ierr);
	
	ierr = DMGetLocalVector(da,&heightloc);CHKERRQ(ierr);
	ierr = DMGetLocalVector(da,&heightlastloc);CHKERRQ(ierr);
	
	ierr = DMGetLocalVector(da,&Fheightloc);CHKERRQ(ierr);
	
	/* get local solution and time derivative */
	ierr = VecZeroEntries(heightloc);CHKERRQ(ierr);
	ierr = DMGlobalToLocalBegin(da,X,ADD_VALUES,heightloc);CHKERRQ(ierr);
	ierr = DMGlobalToLocalEnd  (da,X,ADD_VALUES,heightloc);CHKERRQ(ierr);
	
	ierr = VecZeroEntries(heightlastloc);CHKERRQ(ierr);
	ierr = DMGlobalToLocalBegin(da,data->height_old,ADD_VALUES,heightlastloc);CHKERRQ(ierr);
	ierr = DMGlobalToLocalEnd  (da,data->height_old,ADD_VALUES,heightlastloc);CHKERRQ(ierr);
	
	/* insert boundary conditions into local vectors */
	ierr = BCListInsertLocal(data->bclist,heightloc);CHKERRQ(ierr);
	
	
	
	/* init residual */
	ierr = VecZeroEntries(Fheightloc);CHKERRQ(ierr);
	
	/* get arrays */
	ierr = VecGetArray(heightloc,    &LA_heightloc);CHKERRQ(ierr);
	ierr = VecGetArray(heightlastloc,&LA_heightlastloc);CHKERRQ(ierr);
	ierr = VecGetArray(Fheightloc,   &LA_Fheightloc);CHKERRQ(ierr);
	
	/* ============= */
	/* FORM_FUNCTION */	
	
	ierr = FormFunction_SPM(data,dt,da,LA_heightloc,LA_heightlastloc,LA_Fheightloc);CHKERRQ(ierr);
	/* ============= */
	
	ierr = VecRestoreArray(Fheightloc,   &LA_Fheightloc);CHKERRQ(ierr);
	ierr = VecRestoreArray(heightlastloc,&LA_heightlastloc);CHKERRQ(ierr);
	ierr = VecRestoreArray(heightloc,    &LA_heightloc);CHKERRQ(ierr);
	
	
	/* do global fem summation */
	ierr = VecZeroEntries(F);CHKERRQ(ierr);
	
	ierr = DMLocalToGlobalBegin(da,Fheightloc,ADD_VALUES,F);CHKERRQ(ierr);
	ierr = DMLocalToGlobalEnd  (da,Fheightloc,ADD_VALUES,F);CHKERRQ(ierr);
	
	ierr = DMRestoreLocalVector(da,&Fheightloc);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(da,&heightloc);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(da,&heightlastloc);CHKERRQ(ierr);
	
	/* modify F for the boundary conditions, F_k = scale_k(x_k - phi_k) */
	ierr = BCListResidualDirichlet(data->bclist,X,F);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatinLoadPhysics_SPMEquation"
PetscErrorCode pTatinLoadPhysics_SPMEquation(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ierr = PhysComp_SPMEquationCreate2(ctx->dav,&ctx->phys_spm);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PhysComp_SPMEquationDestroy"
PetscErrorCode PhysComp_SPMEquationDestroy(PhysCompSPMCtx *p)
{
	PhysCompSPMCtx phys;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	if (!p) { PetscFunctionReturn(0); }
	phys = *p;
	
	
	ierr = DMDestroy(&phys->daSPM);CHKERRQ(ierr);
	ierr = VecDestroy(&phys->height_old);CHKERRQ(ierr);
	ierr = VecDestroy(&phys->height);CHKERRQ(ierr);
	
	ierr = QuadratureSPMDestroy(&phys->Q);CHKERRQ(ierr);
	ierr = BCListDestroy(&phys->bclist);CHKERRQ(ierr);
	
	ierr = PetscFree(phys);CHKERRQ(ierr);
	
	*p = PETSC_NULL;
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureSPMDestroy"
PetscErrorCode QuadratureSPMDestroy(QuadratureSPM *Q)
{
	QuadratureSPM quadrature;
	PetscInt ncells;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	if (!Q) { PetscFunctionReturn(0); }
	quadrature = *Q;
	
	ncells = quadrature->ncells;
	if (ncells!=0) {
		ierr = PetscFree(quadrature->cellproperties);CHKERRQ(ierr);
	}
	
	/* allocate */
	ierr = PetscFree( quadrature->xi );CHKERRQ(ierr);
	ierr = PetscFree( quadrature->weight );CHKERRQ(ierr);
	
	ierr = PetscFree( quadrature );CHKERRQ(ierr);
	*Q = PETSC_NULL;
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PhysComp_SPMSetConstantParams"
PetscErrorCode PhysComp_SPMSetConstantParams(PhysCompSPMCtx p)
{
	PetscInt c,n;
	CoefficientsSPMEq   *quadpoints;
	PetscErrorCode   ierr; 
	PetscScalar diffusivity = p->kappa_0; 
    PetscScalar source = p->source_0; 	
	
	for (c=0; c<p->Q->ncells; c++) {
	ierr = QuadratureSPMGetCell(p->Q,c,&quadpoints);CHKERRQ(ierr);
	      for (n=0;n<p->Q->ngp;n++){
	  	    quadpoints[n].diffusivity = diffusivity;
		    quadpoints[n].sediment_source = source;
	      }
	}
	PetscFunctionReturn(0);
}
#undef __FUNCT__
#define __FUNCT__ "PhysCompSPMExtractInitialGeometry"
PetscErrorCode PhysCompSPMExtractInitialGeometry(PhysCompSPMCtx p,DM dav)
{
	PetscInt c;
	DM redundant;
	PetscInt i,j,NX,NY;
	Vec coord,coordspm;
	DM cda,cdaspm;
	DMDACoor2d **LA_coord;
	PetscScalar *LA_hold,*LA_coordspm;
	PetscErrorCode ierr;
	
	ierr = DMDAGetInfo(dav,0, &NX,&NY,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
	
	ierr = DMDACreate2dRedundant( dav, 0,NX, NY-1,  NY, 1, &redundant );CHKERRQ(ierr);
	
	// copy height
	ierr = DMDAGetCoordinateDA(redundant,&cda);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(redundant,&coord);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,coord,&LA_coord);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(p->daSPM,p->height_old,&LA_hold);CHKERRQ(ierr);
	j = 0;
	for (i=0; i<NX; i++) {
		LA_hold[i] = LA_coord[j][i].y;
	}
	ierr = DMDAVecRestoreArray(p->daSPM,p->height_old,&LA_hold);CHKERRQ(ierr);
	
	// copy x coords
	ierr = DMDAGetCoordinateDA(p->daSPM,&cdaspm);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(p->daSPM,&coordspm);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cdaspm,coordspm,&LA_coordspm);CHKERRQ(ierr);
	j = 0;
	for (i=0; i<NX; i++) {
		LA_coordspm[i] = LA_coord[j][i].x;
	}
	ierr = DMDAVecRestoreArray(cdaspm,coordspm,&LA_coordspm);CHKERRQ(ierr);

	ierr = DMDAVecRestoreArray(cda,coord,&LA_coord);CHKERRQ(ierr);
	
	ierr = DMDestroy(&redundant);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PhysCompSPMUpdateSurfaceGeometry"
PetscErrorCode PhysCompSPMUpdateSurfaceGeometry(PhysCompSPMCtx p,DM dav)
{
	PetscInt c;
	PetscInt i,j,NX,NY,si,sj,ni,nj;
	Vec coord;
	DM cda;
	DMDACoor2d **LA_coord;
	PetscScalar *LA_h;
	PetscErrorCode ierr;
	PetscReal   river_normal = 0.0;
    PetscReal   bl = -1.e32;
    PetscBool   base_level=PETSC_FALSE;
    PetscReal   base_level_shift = 0.0;
    
  	ierr = PetscOptionsGetReal(PETSC_NULL,"-SPM_river_normal",&river_normal,0);CHKERRQ(ierr);
    
	ierr = PetscOptionsGetReal(PETSC_NULL,"-SPM_base_level_ND",&base_level_shift,&base_level);CHKERRQ(ierr);
    
	ierr = DMDAGetInfo(dav,0, &NX,&NY,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
	ierr = DMDAGetCorners(dav,&si,&sj,PETSC_NULL,&ni,&nj,PETSC_NULL);CHKERRQ(ierr);
	
	ierr = DMDAGetCoordinateDA(dav,&cda);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(dav,&coord);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,coord,&LA_coord);CHKERRQ(ierr);
	
	/* 
	 p->height is duplicated on every processors - just pull out the part which matches the local
	 chunk defined by the parallel DM (dav)
	*/
    
	ierr = DMDAVecGetArray(p->daSPM,p->height,&LA_h);CHKERRQ(ierr);
    if (base_level){
        bl = LA_h[0]-base_level_shift;
    }
    
	if (sj+nj == NY) {
		j = NY-1;
		for (i=si; i<si+ni; i++) {
            if (LA_h[i] > bl){
                LA_coord[j][i].y = LA_h[i];
                if (LA_coord[j][i].y < LA_h[i]){
                   LA_coord[j][i].y = (-LA_h[i]+LA_coord[j][i].y)*river_normal+LA_h[i];
                }
            }else{
		      if (LA_coord[j][i].y < LA_h[i]) {
                  /* sedimenting*/
                  if ((-LA_h[i]+LA_coord[j][i].y)*river_normal+LA_h[i] > bl) {
			       LA_coord[j][i].y = bl;
                  }else{
                   LA_coord[j][i].y = (-LA_h[i]+LA_coord[j][i].y)*river_normal+LA_h[i];
                  }
              } /*no erosion under base level */
            }
		}
	}
	ierr = DMDAVecRestoreArray(p->daSPM,p->height,&LA_h);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(cda,coord,&LA_coord);CHKERRQ(ierr);
	
	/* update ghost coordinates */
	ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
		
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PhysCompSPMUpdateSurfaceGeometry_BaseLevel"
PetscErrorCode PhysCompSPMUpdateSurfaceGeometry_BaseLevel(PhysCompSPMCtx p,DM dav)
{
    PetscInt c;
    PetscInt i,j,NX,NY,si,sj,ni,nj;
    Vec coord;
    DM cda;
    DMDACoor2d **LA_coord;
    PetscScalar *LA_h;
    PetscErrorCode ierr;
    PetscReal   river_normal = 0.0;
    
    ierr = PetscOptionsGetReal(PETSC_NULL,"-SPM_river_normal",&river_normal,0);CHKERRQ(ierr);
    
    ierr = DMDAGetInfo(dav,0, &NX,&NY,0, 0,0,0, 0,0, 0,0,0, 0);CHKERRQ(ierr);
    ierr = DMDAGetCorners(dav,&si,&sj,PETSC_NULL,&ni,&nj,PETSC_NULL);CHKERRQ(ierr);
    
    ierr = DMDAGetCoordinateDA(dav,&cda);CHKERRQ(ierr);
    ierr = DMDAGetCoordinates(dav,&coord);CHKERRQ(ierr);
    ierr = DMDAVecGetArray(cda,coord,&LA_coord);CHKERRQ(ierr);
    
    /*
     p->height is duplicated on every processors - just pull out the part which matches the local
     chunk defined by the parallel DM (dav)
     */
    ierr = DMDAVecGetArray(p->daSPM,p->height,&LA_h);CHKERRQ(ierr);
    if (sj+nj == NY) {
        j = NY-1;
        for (i=si; i<si+ni; i++) {
            if (LA_coord[j][i].y > LA_h[i]) {
                LA_coord[j][i].y = LA_h[i];
            }else{
                LA_coord[j][i].y = (-LA_h[i]+LA_coord[j][i].y)*river_normal+LA_h[i];
            }
        }
    }
    ierr = DMDAVecRestoreArray(p->daSPM,p->height,&LA_h);CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(cda,coord,&LA_coord);CHKERRQ(ierr);
    
    /* update ghost coordinates */
    ierr = DMDAUpdateGhostedCoordinates(dav);CHKERRQ(ierr);
    
    PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PhysCompSPMAddSedimentMarkersOnTopSurface"
PetscErrorCode PhysCompSPMAddSedimentMarkersOnTopSurface(PhysCompSPMCtx p,DM  da,DataBucket db,PetscBool active)
{
	PetscInt       imark,NSedMarkPerCell;
	DM             cda;
	Vec            gcoords;
	PetscScalar    *LA_gcoords;
	PetscInt       nel,nen,e,iel,jel;
	const PetscInt *elnidx;
	PetscScalar    elcoords[2*Q2_NODES_PER_EL_2D];
	double         Niu[9];
	PetscReal      dxi,xi_mark[2], xmark,ymark;
	PetscInt       end,i,mx,my,lmx,lmy,esi,esj,ielstart,ielend;
	DataField      PField,PField_thermal,PFieldChrono,PField_stokespl;
	MPntStd        *marker;
	MPntPThermal   *material_point_thermal;
	MPntPChrono    *material_point_chrono;
	MPntPStokesPl  *mpprop_stokespl;
	PetscBool      store_ages; 
	PetscErrorCode ierr;
	BTruth plastic;
	
	PetscFunctionBegin;
	NSedMarkPerCell = p->NSedMarkPerCell;
	
	DataBucketGetDataFieldByName(db, MPntStd_classname ,&PField);
	DataFieldGetAccess(PField);
	
	DataBucketGetDataFieldByName(db,MPntPStokesPl_classname,&PField_stokespl);
	DataFieldGetAccess(PField_stokespl);
	DataFieldVerifyAccess(PField_stokespl,sizeof(MPntPStokesPl));	
		
    if (active) {
	    DataBucketGetDataFieldByName(db,MPntPThermal_classname,&PField_thermal);
	    DataFieldGetAccess(PField_thermal);
	    DataFieldVerifyAccess(PField_thermal,sizeof(MPntPThermal));
	    ierr = PetscOptionsGetBool(PETSC_NULL,"-store_ages",&store_ages,0);CHKERRQ(ierr);
	    if (store_ages) {
	       DataBucketGetDataFieldByName(db,MPntPChrono_classname,&PFieldChrono);
	       DataFieldGetAccess(PFieldChrono);
	       DataFieldVerifyAccess(PFieldChrono,sizeof(MPntPChrono));
	    }
	}
		
	/* setup for coords */
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(da,&gcoords);CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);

	ierr = DMDAGetElements_pTatin(da,&nel,&nen,&elnidx);CHKERRQ(ierr);

	ierr = DMDAGetSizeElementQ2(da,&mx,&my,PETSC_NULL);CHKERRQ(ierr);
	ierr = DMDAGetCornersElementQ2(da,&esi,&esj,PETSC_NULL,&lmx,&lmy,PETSC_NULL);CHKERRQ(ierr);
	
	dxi = 2.0/((PetscReal)NSedMarkPerCell);
    
    esj = esj/2;
    esi = esi/2;
    ielstart = 0;
    ielend   = lmx;
    if (esi==0) ielstart= 1;
    if (esi+lmx==mx) ielend= lmx-1;
    
	if (esj + lmy == my) { /* then we are processing the row of elements at the top of the mesh */
		
		jel = lmy-1;
		for (iel=ielstart; iel<ielend; iel++){
			e = iel + jel*lmx;
            
			
            
			ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx[nen*e],LA_gcoords);CHKERRQ(ierr);
			
			for (imark=0; imark<NSedMarkPerCell; imark++){
				
				//xi_mark[0] = (1.0/NSedMarkPerCell + imark*2.0/NSedMarkPerCell) -1.0;
				xi_mark[0] = -1.0 + 0.5*dxi + imark*dxi;
				xi_mark[1] = 0.999;
				PTatinConstructNI_Q2_2D(xi_mark,Niu);

				xmark = 0.0; 
				ymark = 0.0; 
				for (i=0; i<Q2_NODES_PER_EL_2D; i++) {
					xmark = xmark + Niu[i] * elcoords[NSD*i  ];
					ymark = ymark + Niu[i] * elcoords[NSD*i+1];
				}
				
				DataBucketAddPoint(db);
				DataBucketGetSizes(db,&end,0,0);
				end = end - 1;
				DataFieldAccessPoint(PField,end,(void**)&marker);
				marker->coor[0] = xmark;
				marker->coor[1] = ymark;
				marker->xi[0]   = xi_mark[0];
				marker->xi[1]   = xi_mark[1];
				marker->wil     = e;
				marker->phase   = p->sed_mark_id;
				marker->pid     = end; /* this will not be unique in parallel - but I don't think we care */
				DataBucketQueryDataFieldByName(db,MPntPStokesPl_classname,&plastic);
				
				if (plastic) {
				DataFieldAccessPoint(PField_stokespl,end,(void**)&mpprop_stokespl);
		        MPntPStokesPlSetField_yield_indicator(mpprop_stokespl,0);
		        MPntPStokesPlSetField_plastic_strain(mpprop_stokespl,0.0);
				}
				
				if (active) {
				DataFieldAccessPoint(PField_thermal,end,(void**)&material_point_thermal);
		        MPntPThermalSetField_diffusivity(material_point_thermal,p->heat_diffusivity_ND);
                MPntPThermalSetField_heat_prod(material_point_thermal,p->heat_productivity_ND);
				}
				
				if (store_ages) {
				DataFieldAccessPoint(PFieldChrono,end,(void**)&material_point_chrono);
		        MPntPChronoSetField_age120(material_point_chrono,p->time);
		        MPntPChronoSetField_age350(material_point_chrono,p->time);
		        MPntPChronoSetField_age800(material_point_chrono,p->time);
				}
				
			}
		}
		
	}	
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	DataFieldRestoreAccess(PField);
	
	if (active) {
	DataFieldRestoreAccess(PField_thermal);
	}
	
	if (store_ages) {
	DataFieldRestoreAccess(PFieldChrono);
	}
	//if (plastic) {
	DataFieldRestoreAccess(PField_stokespl);
	//}
	
	PetscFunctionReturn(0);
}
#if 0
#undef __FUNCT__
#define __FUNCT__ "DMDABCListTraverseSPM"
PetscErrorCode DMDABCListTraverseSPM(BCList list,DM da,DMDABCListConstraintLoc doflocation,PetscInt dof_idx,PetscBool (*eval)(PetscScalar*,PetscScalar*,void*),void *ctx)
{
	PetscInt i,si,m,M,ndof;
	DM cda;
	Vec coords;
	DMDACoor1d *LA_coords;	
	PetscInt L,*idx;
	PetscScalar pos[1];
	PetscScalar *vals,bc_val;
	PetscBool impose_dirichlet;
	PetscErrorCode ierr;
	
	ierr = DMDAGetInfo(da,0, &M,0,0, 0,0,0, &ndof,0, 0,0,0, 0);CHKERRQ(ierr);
	if (dof_idx >= ndof) { SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONG,"dof_index >= dm->blocksize"); }
	
	ierr = DMDAGetCorners(da,&si,0,0,&m,0,0);CHKERRQ(ierr);
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(da,&coords);CHKERRQ(ierr);
	if (!coords) { SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Coordinates must be set"); }
	ierr = DMDAVecGetArray(cda,coords,&LA_coords);CHKERRQ(ierr);
	
	ierr = BCListGetGlobalIndices(list,&L,&idx);
	ierr = BCListGetGlobalValues(list,&L,&vals);
	
	
	/* i=0 plane (left) */
	if (doflocation==DMDABCList_IMIN_LOC) {
		if (si==0) {
			PetscInt blockloc = (i-si);
			PetscInt loc = blockloc*ndof+dof_idx;
			i = 0;
			pos[0] = LA_coords[i].x;
			impose_dirichlet = eval(pos,&bc_val,ctx);
				if (impose_dirichlet) {
					idx[loc] = BCList_DIRICHLET;
					vals[loc] = bc_val;
			    }
		}
	}
	
	/* i=si+m == M plane (right) */
	if (doflocation==DMDABCList_IMAX_LOC) {
		if (si+m==M) {
			    PetscInt blockloc = (i-si);
				PetscInt loc = blockloc*ndof+dof_idx;
			    i = si+m-1;
				pos[0] = LA_coords[i].x;
				impose_dirichlet = eval(pos,&bc_val,ctx);
				if (impose_dirichlet) {
					idx[loc] = BCList_DIRICHLET;
					vals[loc] = bc_val;
				}
		}
	}
	
	
	ierr = BCListRestoreGlobalIndices(list,&L,&idx);
	ierr = DMDAVecRestoreArray(cda,coords,&LA_coords);CHKERRQ(ierr);
	ierr = BCListGlobalToLocal(list);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}
#endif


