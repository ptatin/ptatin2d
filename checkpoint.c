
#include "petsc.h"
#include "pTatin2d.h"
#include "checkpoint.h"


#undef __FUNCT__  
#define __FUNCT__ "pTatinCheckpointWrite_options"
PetscErrorCode pTatinCheckpointWrite_options(const char dirname[])
{
	char *fname;
	PetscErrorCode ierr;
	
	asprintf(&fname,"%s/checkpoint.options",dirname);
	ierr = pTatinWriteOptionsFile(fname);CHKERRQ(ierr);
	free(fname);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatinCheckpointWrite_context"
PetscErrorCode pTatinCheckpointWrite_context(pTatinCtx ctx,const char dirname[])
{
	char *fname;
	PetscViewer viewer;
	PetscErrorCode ierr;
	
	
	asprintf(&fname,"%s/checkpoint.context",dirname);

	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,fname,FILE_MODE_WRITE,&viewer);CHKERRQ(ierr);

	ierr = PetscViewerBinaryWrite(viewer,(void*)ctx,sizeof(struct _p_pTatinCtx),PETSC_CHAR,PETSC_FALSE);CHKERRQ(ierr);
	
	ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
	free(fname);
	
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatinCheckpointWrite"
PetscErrorCode pTatinCheckpointWrite(pTatinCtx ctx,const char dirname[])
{
	char *mydirname;
	PetscErrorCode ierr;
	
	
	if (!dirname) {
		asprintf(&mydirname,"checkpoint");
	} else {
		asprintf(&mydirname,"%s",dirname);
	}
	ierr = pTatinCreateDirectory(mydirname);CHKERRQ(ierr);
	
	ierr = pTatinCheckpointWrite_options(mydirname);CHKERRQ(ierr);
	ierr = pTatinCheckpointWrite_context(ctx,mydirname);CHKERRQ(ierr);
	
	free(mydirname);
	
	PetscFunctionReturn(0);
}




// Inserting options via PetscOptionsInsertFile() clobbers any extra options placed on the command line
//ierr = PetscOptionsInsertFile(PETSC_COMM_WORLD,"test.opts",PETSC_FALSE);CHKERRQ(ierr);
// Loading options via Insert() allows command line override 
//ierr = PetscOptionsInsert(&argc,&argv,"test.opts");CHKERRQ(ierr);
//ierr = PetscOptionsInsert(&argc,&argv,0);CHKERRQ(ierr); // null file loading permitted
#undef __FUNCT__  
#define __FUNCT__ "pTatinCheckpointLoad_options"
PetscErrorCode pTatinCheckpointLoad_options(const char dirname[])
{
	char *fname;
	int nargs;
	char **args;
	PetscErrorCode ierr;
	
	asprintf(&fname,"%s/checkpoint.options",dirname);
	ierr = PetscGetArgs(&nargs,&args);CHKERRQ(ierr);
	ierr = PetscOptionsInsert(&nargs,&args,fname);CHKERRQ(ierr);
	free(fname);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatinCheckpointLoad_context"
PetscErrorCode pTatinCheckpointLoad_context(pTatinCtx ctx,const char dirname[])
{
	char *fname;
	PetscViewer viewer;
	PetscErrorCode ierr;
	
	
	asprintf(&fname,"%s/checkpoint.context",dirname);
	
	ierr = PetscMemzero((void*)ctx,sizeof(struct _p_pTatinCtx));CHKERRQ(ierr);
	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,fname,FILE_MODE_READ,&viewer);CHKERRQ(ierr);
	
	ierr = PetscViewerBinaryRead(viewer,(void*)ctx,sizeof(struct _p_pTatinCtx),PETSC_CHAR);CHKERRQ(ierr);
	
	ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
	free(fname);
	
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatinCheckpointLoad"
PetscErrorCode pTatinCheckpointLoad(pTatinCtx ctx,const char dirname[])
{
	char *mydirname;
	PetscErrorCode ierr;
	
	
	if (!dirname) {
		asprintf(&mydirname,"checkpoint");
	} else {
		asprintf(&mydirname,"%s",dirname);
	}
	
	ierr = pTatinCreateDirectory(mydirname);CHKERRQ(ierr);
	
	//ierr = pTatinCheckpointLoad_options(mydirname);CHKERRQ(ierr);
	ierr = pTatinCheckpointLoad_context(ctx,mydirname);CHKERRQ(ierr);
	
	free(mydirname);
	
	PetscFunctionReturn(0);
}