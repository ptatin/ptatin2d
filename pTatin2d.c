static const char help[] = "Matrix free Stokes solver using Q2-Pm1 mixed finite elements.\n"
"2D prototype of the (p)ragmatic version of Tatin. (pTatin2d_v0.0)\n\n";

#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"
#include "pTatin2d_models.h"


#undef __FUNCT__
#define __FUNCT__ "pTatinCreateDirectory"
PetscErrorCode pTatinCreateDirectory(const char dirname[])
{
	mode_t mode;
	PetscMPIInt rank;
	int num,error_number;
	
	PetscFunctionBegin;
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	
	/* Generate a new directory on proc 0 */
	if (rank == 0) {
		num = mkdir(dirname,S_IRWXU);
		error_number = errno;
	}
	MPI_Bcast(&num,1,MPI_INT,0,PETSC_COMM_WORLD);
	MPI_Bcast(&error_number,1,MPI_INT,0,PETSC_COMM_WORLD);
	
	if (error_number == EEXIST) {
		PetscPrintf(PETSC_COMM_WORLD,"Writing output to existing directory %s \n",dirname);
	} else if (error_number == EACCES) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Write permission is denied for the parent directory in which the new directory is to be added");
	} else if (error_number == EMLINK) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"The parent directory has too many links (entries)");
	} else if (error_number == ENOSPC) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"The file system doesn't have enough room to create the new directory");
	} else if (error_number == ENOSPC) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"The parent directory of the directory being created is on a read-only file system and cannot be modified");
	} else {
		PetscPrintf(PETSC_COMM_WORLD,"Created output directory %s \n",dirname);
	}
	
	MPI_Barrier(PETSC_COMM_WORLD);
	PetscFunctionReturn(0);
}

/* FILE: Boundary conditions */
#undef __FUNCT__
#define __FUNCT__ "BCListIsDirichlet"
PetscErrorCode BCListIsDirichlet(PetscInt value,PetscBool *flg)
{
	if (value==BCList_DIRICHLET) { *flg = PETSC_TRUE;  }
	else                         { *flg = PETSC_FALSE; }
	PetscFunctionReturn(0);
}
#undef __FUNCT__
#define __FUNCT__ "BCListInitialize"
PetscErrorCode BCListInitialize(BCList list)
{
	PetscInt       n;
	PetscErrorCode ierr;
	
	for (n=0; n<list->L; n++) {
		list->dofidx_global[n] = 0;
		list->scale_global[n]  = 1.0;
	}
	
	for (n=0; n<list->L_local; n++) {
		list->dofidx_local[n] = 0.0;
		list->vals_local[n]   = 0.0;
	}
	
	PetscFunctionReturn(0);
}
#undef __FUNCT__
#define __FUNCT__ "BCListCreate"
PetscErrorCode BCListCreate(BCList *list)
{
	BCList ll;
	PetscErrorCode ierr;
	
	ierr = PetscMalloc( sizeof(struct _p_BCList),&ll);CHKERRQ(ierr);
	ll->allEmpty = PETSC_FALSE;
	*list = ll;
	PetscFunctionReturn(0);
}
#undef __FUNCT__
#define __FUNCT__ "BCListDestroy"
PetscErrorCode BCListDestroy(BCList *list)
{
	BCList         ll = *list;
	PetscErrorCode ierr;
	
	ierr = PetscFree(ll->vals_global);CHKERRQ(ierr);
	ierr = PetscFree(ll->vals_local);CHKERRQ(ierr);
	ierr = PetscFree(ll->dofidx_global);CHKERRQ(ierr);
	ierr = PetscFree(ll->dofidx_local);CHKERRQ(ierr);
	ierr = PetscFree(ll->scale_global);CHKERRQ(ierr);
	
	ierr = DMDestroy(&ll->dm);CHKERRQ(ierr);
	ierr = PetscFree(ll);CHKERRQ(ierr);
	*list = PETSC_NULL;
	PetscFunctionReturn(0);
}
#undef __FUNCT__
#define __FUNCT__ "BCListSetSizes"
PetscErrorCode BCListSetSizes(BCList list,PetscInt bs,PetscInt N,PetscInt N_local)
{
	PetscErrorCode ierr;
	
	list->blocksize = bs;
	list->N  = N;
	list->L  = bs * N;
	ierr = PetscMalloc( sizeof(PetscInt)*list->L,    &list->dofidx_global);CHKERRQ(ierr);
	ierr = PetscMalloc( sizeof(PetscScalar)*list->L, &list->vals_global);CHKERRQ(ierr);
	ierr = PetscMalloc( sizeof(PetscScalar)*list->L, &list->scale_global); CHKERRQ(ierr);
	
	list->N_local  = N_local;
	list->L_local  = bs * N_local;
	ierr = PetscMalloc( sizeof(PetscInt)*list->L_local,    &list->dofidx_local);CHKERRQ(ierr);
	ierr = PetscMalloc( sizeof(PetscScalar)*list->L_local, &list->vals_local);CHKERRQ(ierr);
	
	ierr = BCListInitialize(list);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}
#undef __FUNCT__
#define __FUNCT__ "BCListUpdateCache"
PetscErrorCode BCListUpdateCache(BCList list)
{
	PetscInt       n,cnt;
	PetscBool      isdir;
	PetscErrorCode ierr;
	
	cnt = 0;
	for (n=0; n<list->L; n++) {
		ierr = BCListIsDirichlet(list->dofidx_global[n],&isdir);CHKERRQ(ierr);
		if (!isdir) { cnt++; }
	}
	list->allEmpty = PETSC_FALSE;
	if (cnt==list->L) { list->allEmpty = PETSC_TRUE; }
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BCListInitGlobal"
PetscErrorCode BCListInitGlobal(BCList list)
{
	PetscInt i,max,*indices,lsize;
	Vec dindices,dindices_g;
	PetscScalar *_dindices;
	PetscErrorCode ierr;
	
	
	ierr = DMDAGetGlobalIndices(list->dm,&max,&indices);CHKERRQ(ierr);
	ierr = DMGetGlobalVector(list->dm,&dindices_g);CHKERRQ(ierr);
	ierr = DMGetLocalVector(list->dm,&dindices);CHKERRQ(ierr);
	ierr = VecGetLocalSize(dindices,&lsize);CHKERRQ(ierr);
	if (lsize!=max) { SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Sizes don't match 1"); }
	ierr = VecGetArray(dindices,&_dindices);CHKERRQ(ierr);
	/* convert to scalar */
	for (i=0; i<lsize; i++) {
		_dindices[i] = (PetscScalar)indices[i] + 1.0e-3;
	}
	ierr = VecRestoreArray(dindices,&_dindices);CHKERRQ(ierr);
	
	/* scatter (ignore ghosts) */
	ierr = DMLocalToGlobalBegin(list->dm,dindices,INSERT_VALUES,dindices_g);CHKERRQ(ierr);
	ierr = DMLocalToGlobalEnd(  list->dm,dindices,INSERT_VALUES,dindices_g);CHKERRQ(ierr);
	
	/* convert to int */
	ierr = VecGetLocalSize(dindices_g,&lsize);CHKERRQ(ierr);
	if (list->L!=lsize) { SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Sizes don't match 2"); }
	ierr = VecGetArray(dindices_g,&_dindices);CHKERRQ(ierr);
	for (i=0; i<lsize; i++) {
		list->dofidx_global[i] = (PetscInt)_dindices[i];
	}
	ierr = VecRestoreArray(dindices_g,&_dindices);CHKERRQ(ierr);
	
	ierr = DMRestoreGlobalVector(list->dm,&dindices_g);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(list->dm,&dindices);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BCListGlobalToLocal"
PetscErrorCode BCListGlobalToLocal(BCList list)
{
	PetscInt i,lsize;
	Vec dindices,dindices_g;
	PetscScalar *_dindices;
	PetscBool is_dirich;
	PetscErrorCode ierr;
	
	
	ierr = DMGetGlobalVector(list->dm,&dindices_g);CHKERRQ(ierr);
	ierr = DMGetLocalVector(list->dm,&dindices);CHKERRQ(ierr);
	
	/* clean up indices (global -> local) */
	ierr = VecGetLocalSize(dindices_g,&lsize);CHKERRQ(ierr);
	if (lsize!=list->L) { SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Sizes don't match 1"); }
	ierr = VecGetArray(dindices_g,&_dindices);CHKERRQ(ierr);
	/* convert to scalar and copy values */
	for (i=0; i<lsize; i++) {
		BCListIsDirichlet(list->dofidx_global[i],&is_dirich);
		if (is_dirich) {
			_dindices[i] = (PetscScalar)list->dofidx_global[i] - 1.0e-3;
		} else {
			_dindices[i] = (PetscScalar)list->dofidx_global[i] + 1.0e-3;
		}
	}
	ierr = VecRestoreArray(dindices_g,&_dindices);CHKERRQ(ierr);
	
	/* scatter (ignore ghosts) */
	ierr = DMGlobalToLocalBegin(list->dm,dindices_g,INSERT_VALUES,dindices);CHKERRQ(ierr);
	ierr = DMGlobalToLocalEnd(  list->dm,dindices_g,INSERT_VALUES,dindices);CHKERRQ(ierr);
	
	/* convert to int */
	ierr = VecGetLocalSize(dindices,&lsize);CHKERRQ(ierr);
	if (list->L_local!=lsize) { SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Sizes don't match 2"); }
	ierr = VecGetArray(dindices,&_dindices);CHKERRQ(ierr);
	for (i=0; i<lsize; i++) {
		list->dofidx_local[i] = (PetscInt)_dindices[i];
	}
	ierr = VecRestoreArray(dindices,&_dindices);CHKERRQ(ierr);
	
	
	/* setup values (global -> local) */
	ierr = VecGetLocalSize(dindices_g,&lsize);CHKERRQ(ierr);
	ierr = VecGetArray(dindices_g,&_dindices);CHKERRQ(ierr);
	/* copy global values into a vector */
	for (i=0; i<lsize; i++) {
       
		_dindices[i] = list->vals_global[i];
        
	}
	ierr = VecRestoreArray(dindices_g,&_dindices);CHKERRQ(ierr);
	
	/* scatter (ignore ghosts) */
	ierr = DMGlobalToLocalBegin(list->dm,dindices_g,INSERT_VALUES,dindices);CHKERRQ(ierr);
	ierr = DMGlobalToLocalEnd(  list->dm,dindices_g,INSERT_VALUES,dindices);CHKERRQ(ierr);
	
	/* retrieve entries */
	ierr = VecGetLocalSize(dindices,&lsize);CHKERRQ(ierr);
	ierr = VecGetArray(dindices,&_dindices);CHKERRQ(ierr);
	for (i=0; i<lsize; i++) {
		list->vals_local[i] = _dindices[i];
	}
	ierr = VecRestoreArray(dindices,&_dindices);CHKERRQ(ierr);
	
	ierr = DMRestoreGlobalVector(list->dm,&dindices_g);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(list->dm,&dindices);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDABCListCreate"
PetscErrorCode DMDABCListCreate(DM da,BCList *list)
{
	BCList ll;
	PetscInt bs,N,m,n,p,Ng,mg,ng,pg;
	PetscInt gidx,gdofidx,blockloc,loc,dof_idx,dim,ndof;
	PetscInt si,sj,sk,nx,ny,nz,gsi,gsj,gsk,gnx,gny,gnz,i,j,k;
	PetscInt max_index,*globalindices;
	PetscErrorCode ierr;
	
	ierr = DMDAGetInfo(da,0, 0,0,0, 0,0,0, &bs,0, 0,0,0, 0);CHKERRQ(ierr);
	ierr = DMDAGetCorners(da, 0,0,0, &m,&n,&p);CHKERRQ(ierr);
	ierr = DMDAGetGhostCorners(da, 0,0,0, &mg,&ng,&pg);CHKERRQ(ierr);
	N = m;
	if (n!=0) N = N * n; /* 2d */
	if (p!=0) N = N * p; /* 3d */
	Ng = mg;
	if (ng!=0) Ng = Ng * ng; /* 2d */
	if (pg!=0) Ng = Ng * pg; /* 3d */
	
	
	ierr = BCListCreate(&ll);CHKERRQ(ierr);
	ll->dm = da;
	ierr = PetscObjectReference((PetscObject)da);CHKERRQ(ierr);
	ierr = BCListSetSizes(ll,bs,N,Ng);CHKERRQ(ierr);
	ierr = BCListInitialize(ll);CHKERRQ(ierr);
	
	ierr = DMDAGetGhostCorners(da,&gsi,&gsj,&gsk,&gnx,&gny,&gnz);CHKERRQ(ierr);
	ierr = DMDAGetCorners(da,&si,&sj,&sk,&nx,&ny,&nz);CHKERRQ(ierr);
	
	ierr = DMDAGetGlobalIndices(da,&max_index,&globalindices);CHKERRQ(ierr);
	ierr = DMDAGetInfo(da,&dim, 0,0,0, 0,0,0, &ndof,0, 0,0,0, 0);CHKERRQ(ierr);
	
	ierr = BCListInitGlobal(ll);CHKERRQ(ierr);
	ierr = BCListGlobalToLocal(ll);CHKERRQ(ierr);
	
	*list = ll;
	PetscFunctionReturn(0);
}

PetscBool BCListEvaluator_constant( PetscScalar position[], PetscScalar *value, void *ctx ) 
{
	PetscBool impose_dirichlet = PETSC_TRUE;
	PetscScalar dv = *((PetscScalar*)ctx);
	
	*value = dv;
	return impose_dirichlet;
}




/*
 
 PetscBool *eval(PetscScalar*,PetscScalar*,void*)
 
 PetscBool my_bc_evaluator( PetscScalar position[], PetscScalar *value, void *ctx ) 
 {
   PetscBool impose_dirichlet = PETSC_FALSE;
 
   if (position[0]<1.0) {
     *value = 10.0;
     impose_dirichlet = PETSC_TRUE;
   }
 
   return impose_dirichlet;
 }
 
*/ 
 
#undef __FUNCT__
#define __FUNCT__ "DMDABCListTraverse"
PetscErrorCode DMDABCListTraverse(BCList list,DM da,DMDABCListConstraintLoc doflocation,PetscInt dof_idx,PetscBool (*eval)(PetscScalar*,PetscScalar*,void*),void *ctx)
{
	PetscInt i,j,si,sj,m,n,M,N,ndof;
	DM cda;
	Vec coords;
	DMDACoor2d **LA_coords;	
	PetscInt L,*idx;
	PetscScalar pos[2];
	PetscScalar *vals,bc_val;
	PetscBool impose_dirichlet;
	PetscErrorCode ierr;
	
	ierr = DMDAGetInfo(da,0, &M,&N,0, 0,0,0, &ndof,0, 0,0,0, 0);CHKERRQ(ierr);
	if (dof_idx >= ndof) { SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONG,"dof_index >= dm->blocksize"); }
	
	ierr = DMDAGetCorners(da,&si,&sj,0,&m,&n,0);CHKERRQ(ierr);
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(da,&coords);CHKERRQ(ierr);
	if (!coords) { SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Coordinates must be set"); }
	ierr = DMDAVecGetArray(cda,coords,&LA_coords);CHKERRQ(ierr);
	
	ierr = BCListGetGlobalIndices(list,&L,&idx);
	ierr = BCListGetGlobalValues(list,&L,&vals);
    
	
	/* volume */
	if (doflocation==DMDABCList_INTERIOR_LOC) {
		for (j=sj; j<sj+n; j++) {
			for (i=si; i<si+m; i++) {
				PetscInt blockloc = (i-si) + (j-sj)*m;
				PetscInt loc = blockloc*ndof+dof_idx;
				pos[0] = LA_coords[j][i].x;
				pos[1] = LA_coords[j][i].y;
                
				
				impose_dirichlet = eval(pos,&bc_val,ctx);
				if (impose_dirichlet) {
					idx[loc] = BCList_DIRICHLET;
					vals[loc] = bc_val;
				}
				
			}
		}
	}

	
	/* i=0 plane (left) */
	if (doflocation==DMDABCList_IMIN_LOC) {
		if (si==0) {
			i = 0;
			for (j=sj; j<sj+n; j++) {
				PetscInt blockloc = (i-si) + (j-sj)*m;
				PetscInt loc = blockloc*ndof+dof_idx;
				pos[0] = LA_coords[j][i].x;
				pos[1] = LA_coords[j][i].y;
                
				
				impose_dirichlet = eval(pos,&bc_val,ctx);
				if (impose_dirichlet) {
					idx[loc] = BCList_DIRICHLET;
					vals[loc] = bc_val;
				}
				
			}
		}
	}
	
	/* i=si+m == M plane (right) */
	if (doflocation==DMDABCList_IMAX_LOC) {
		if (si+m==M) {
			i = si+m-1;
			for (j=sj; j<sj+n; j++) {
				PetscInt blockloc = (i-si) + (j-sj)*m;
				PetscInt loc = blockloc*ndof+dof_idx;
				pos[0] = LA_coords[j][i].x;
				pos[1] = LA_coords[j][i].y;
                
				
				impose_dirichlet = eval(pos,&bc_val,ctx);
				if (impose_dirichlet) {
					idx[loc] = BCList_DIRICHLET;
					vals[loc] = bc_val;
				}
				
			}
		}
	}
	
	/* j=0 plane (bottom) */
	if (doflocation==DMDABCList_JMIN_LOC) {
		if (sj==0) {
			j = 0;
			for (i=si; i<si+m; i++) {
				PetscInt blockloc = (i-si) + (j-sj)*m;
				PetscInt loc = blockloc*ndof+dof_idx;
				pos[0] = LA_coords[j][i].x;
				pos[1] = LA_coords[j][i].y;
				
                
				impose_dirichlet = eval(pos,&bc_val,ctx);
				if (impose_dirichlet) {
					idx[loc] = BCList_DIRICHLET;
					vals[loc] = bc_val;
				}
				
			}
		}
	}
	
		if (doflocation==DMDABCList_JMIN2_LOC) {
		if (sj==0) {
			j = 1;
			for (i=si; i<si+m; i++) {
			     if ((i != 0) && (i!=(M-1)) ) {
				PetscInt blockloc = (i-si) + (j-sj)*m;
				PetscInt loc = blockloc*ndof+dof_idx;
				pos[0] = LA_coords[j][i].x;
				pos[1] = LA_coords[j][i].y;
                     
				
				impose_dirichlet = eval(pos,&bc_val,ctx);
				if (impose_dirichlet) {
					idx[loc] = BCList_DIRICHLET;
					vals[loc] = bc_val;
				}
				}
			
			}
		}
	}
	
	/* j=sj+n == N plane (top) */
	if (doflocation==DMDABCList_JMAX_LOC) {
		if (sj+n==N) {
			j = sj+n-1;
			for (i=si; i<si+m; i++) {
				PetscInt blockloc = (i-si) + (j-sj)*m;
				PetscInt loc = blockloc*ndof+dof_idx;
				pos[0] = LA_coords[j][i].x;
				pos[1] = LA_coords[j][i].y;
				
               
				impose_dirichlet = eval(pos,&bc_val,ctx);
				if (impose_dirichlet) {
					idx[loc] = BCList_DIRICHLET;
					vals[loc] = bc_val;
                //}else {
                //    idx[loc] = BCList_UNCONSTRAINED;
                }
				
			}
		}
	}

  
	/* Apply condiion only at the node approximately in the middle of the left and righ face */
	/* i=si+m == M &  j = N-1/2 central node (right) */
	if (doflocation==DMDABCList_IMAX_MIDPOINT) {
		if (si+m==M) {
			i = si+m-1;
			for (j=sj; j<sj+n; j++) {
        if (j == (N-1)/2) {
          PetscInt blockloc = (i-si) + (j-sj)*m;
          PetscInt loc = blockloc*ndof+dof_idx;
          pos[0] = LA_coords[j][i].x;
          pos[1] = LA_coords[j][i].y;
          
        
          impose_dirichlet = eval(pos,&bc_val,ctx);
          if (impose_dirichlet) {
            idx[loc] = BCList_DIRICHLET;
            vals[loc] = bc_val;
          }
				}
			}
		}
	}

	/* i=0 j=N-1/2 central node (left) */
	if (doflocation==DMDABCList_IMIN_MIDPOINT) {
		if (si==0) {
			i = 0;
			for (j=sj; j<sj+n; j++) {
				if (j == (N-1)/2) { 
					PetscInt blockloc = (i-si) + (j-sj)*m;
					PetscInt loc = blockloc*ndof+dof_idx;
					pos[0] = LA_coords[j][i].x;
					pos[1] = LA_coords[j][i].y;
                    
                   
					impose_dirichlet = eval(pos,&bc_val,ctx);
					if (impose_dirichlet) {
						idx[loc] = BCList_DIRICHLET;
						vals[loc] = bc_val;
					}
				}	
			}
		}
	}
	
	/* j=0 plane (bottom) */
	if (doflocation==DMDABCList_JMIN_LOC_NC) {
		if (sj==0) {
			j = 0;
			for (i=si; i<si+m; i++) {
				PetscInt blockloc = (i-si) + (j-sj)*m;
				PetscInt loc = blockloc*ndof+dof_idx;
				if (i == 0 )  { continue; }
				if (i == 1 )  { continue; }
				if (i == M-1 ){ continue; }
				pos[0] = LA_coords[j][i].x;
				pos[1] = LA_coords[j][i].y;
				
				
                impose_dirichlet = eval(pos,&bc_val,ctx);
				if (impose_dirichlet) {
					idx[loc] = BCList_DIRICHLET;
					vals[loc] = bc_val;
				}
				
			}
		}
	}
	

	
	/* j=sj+n == N plane (top) */
	if (doflocation==DMDABCList_JMAX_LOC_NC) {
		if (sj+n==N) {
			j = sj+n-1;
			for (i=si; i<si+m; i++) {
				PetscInt blockloc = (i-si) + (j-sj)*m;
				PetscInt loc = blockloc*ndof+dof_idx;
				if (i == 0 )  { continue; }
				if (i == M-1 ){ continue; }
				pos[0] = LA_coords[j][i].x;
				pos[1] = LA_coords[j][i].y;
				
				
                impose_dirichlet = eval(pos,&bc_val,ctx);
				if (impose_dirichlet) {
					idx[loc] = BCList_DIRICHLET;
					vals[loc] = bc_val;
				}
				
			}
		}
	}




	/* Left and right faces, SKIPPING THE CORNERS at the top and bottom of the mesh */
	if (doflocation==DMDABCList_IMIN_LOC_NC) {
		if (si==0) {
			i = 0;
			
			for (j=sj; j<sj+n; j++) {
				PetscInt blockloc = (i-si) + (j-sj)*m;
				PetscInt loc = blockloc*ndof+dof_idx;

				if (j == 0 )  { continue; }
				if (j == N-1 ){ continue; }
				
				pos[0] = LA_coords[j][i].x;
				pos[1] = LA_coords[j][i].y;
				
				
                impose_dirichlet = eval(pos,&bc_val,ctx);
				if (impose_dirichlet) {
					idx[loc] = BCList_DIRICHLET;
					vals[loc] = bc_val;
				}
				
			}
		}
	}

	if (doflocation==DMDABCList_IMAX_LOC_NC) {
		if (si+m==M) {
			i = si+m-1;

			for (j=sj; j<sj+n; j++) {
				PetscInt blockloc = (i-si) + (j-sj)*m;
				PetscInt loc = blockloc*ndof+dof_idx;

				if (j <= 2 )  { continue; }
				if (j >= N-3 ){ continue; }
				
				pos[0] = LA_coords[j][i].x;
				pos[1] = LA_coords[j][i].y;
				
				
                impose_dirichlet = eval(pos,&bc_val,ctx);
				if (impose_dirichlet) {
					idx[loc] = BCList_DIRICHLET;
					vals[loc] = bc_val;
				}
			}
		}
	}
	

	

	/* Apply constraints only on the CORNER NODES */
	if (doflocation==DMDABCList_TOPRIGHTCORNERPOINT) {
		
		if ( (si+m==M) && (sj+n==N) ) {
			PetscInt blockloc,loc;
			i = si+m-1;
			j = sj+n-1;
			blockloc = (i-si) + (j-sj)*m;
			loc = blockloc*ndof+dof_idx;
			pos[0] = LA_coords[j][i].x;
			pos[1] = LA_coords[j][i].y;
            
            
			impose_dirichlet = eval(pos,&bc_val,ctx);
			if (impose_dirichlet) {
				idx[loc] = BCList_DIRICHLET;
				vals[loc] = bc_val;
			}
		}
	}
	
	if (doflocation==DMDABCList_TOPLEFTCORNERPOINT) {
		
		if ((si == 0) && (sj+n==N)) {
			PetscInt blockloc,loc;
			i = 0 ;
			j = sj+n-1;
			blockloc = (i-si) + (j-sj)*m;
			loc = blockloc*ndof+dof_idx;
			pos[0] = LA_coords[j][i].x;
			pos[1] = LA_coords[j][i].y;
            
			
            impose_dirichlet = eval(pos,&bc_val,ctx);
			if (impose_dirichlet) {
				idx[loc] = BCList_DIRICHLET;
				vals[loc] = bc_val;
			}
		}
	}
	
	if (doflocation==DMDABCList_BOTRIGHTCORNERPOINT) {
		
		if ((si+m==M) && (sj==0)) {
			PetscInt blockloc,loc;
			i = si+m-1;
			j = 0;
			blockloc = (i-si) + (j-sj)*m;
			loc = blockloc*ndof+dof_idx;
			pos[0] = LA_coords[j][i].x;
			pos[1] = LA_coords[j][i].y;
            
			
            impose_dirichlet = eval(pos,&bc_val,ctx);
			if (impose_dirichlet) {
				idx[loc] = BCList_DIRICHLET;
				vals[loc] = bc_val;
			}
		}
	}
	
	if (doflocation==DMDABCList_BOTLEFTCORNERPOINT) {
		
		if ((si == 0) && (sj==0)) {
			PetscInt blockloc,loc;
			i = 0 ;
			j = 0 ;
			
			
			blockloc = (i-si) + (j-sj)*m;
			loc = blockloc*ndof+dof_idx;
			pos[0] = LA_coords[j][i].x;
			pos[1] = LA_coords[j][i].y;
            
			
            impose_dirichlet = eval(pos,&bc_val,ctx);
			if (impose_dirichlet) {
				idx[loc] = BCList_DIRICHLET;
				vals[loc] = bc_val;
			}
		}
	}    

	
	
	ierr = BCListRestoreGlobalIndices(list,&L,&idx);
	ierr = DMDAVecRestoreArray(cda,coords,&LA_coords);CHKERRQ(ierr);
	ierr = BCListGlobalToLocal(list);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDABCListTraverse1D"
PetscErrorCode DMDABCListTraverse1D(BCList list,DM da,DMDABCListConstraintLoc doflocation,PetscInt dof_idx,PetscBool (*eval)(PetscScalar*,PetscScalar*,void*),void *ctx)
{
	PetscInt i,si,sj,m,n,M,N,ndof;
	DM cda;
	Vec coords;
	PetscScalar *LA_coords;	
	PetscInt L,*idx;
	PetscScalar pos[2];
	PetscScalar *vals,bc_val;
	PetscBool impose_dirichlet;
	PetscErrorCode ierr;
	
	ierr = DMDAGetInfo(da,0, &M,&N,0, 0,0,0, &ndof,0, 0,0,0, 0);CHKERRQ(ierr);
	if (dof_idx >= ndof) { SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONG,"dof_index >= dm->blocksize"); }
	if (N != 1) { SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONG,"Only valid for 1D DMDA"); }
	
	ierr = DMDAGetCorners(da,&si,&sj,0,&m,&n,0);CHKERRQ(ierr);
	if (n != 1) { SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONG,"Only valid for 1D DMDA"); }
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(da,&coords);CHKERRQ(ierr);
	if (!coords) { SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Coordinates must be set"); }
	ierr = VecGetArray(coords,&LA_coords);CHKERRQ(ierr);
	
	ierr = BCListGetGlobalIndices(list,&L,&idx);
	ierr = BCListGetGlobalValues(list,&L,&vals);
	
	/* volume */
	if (doflocation == DMDABCList_INTERIOR_LOC) {
		for (i=si; i<si+m; i++) {
			PetscInt blockloc = (i-si);
			PetscInt loc = blockloc*ndof+dof_idx;
			pos[0] = LA_coords[i];
			pos[1] = 0.0;
			
            
			impose_dirichlet = eval(pos,&bc_val,ctx);
			if (impose_dirichlet) {
				idx[loc] = BCList_DIRICHLET;
				vals[loc] = bc_val;
			}
		}
	}
	
	
	/* i=0 plane (left) */
	if (doflocation == DMDABCList_IMIN_LOC) {
		if (si == 0) {
			PetscInt blockloc;
			PetscInt loc;
			
			i = 0;
			blockloc = (i-si);
			loc = blockloc*ndof+dof_idx;
			pos[0] = LA_coords[i];
			pos[1] = 0.0;
			
          
			impose_dirichlet = eval(pos,&bc_val,ctx);
			if (impose_dirichlet) {
				idx[loc] = BCList_DIRICHLET;
				vals[loc] = bc_val;
			}
		}
	}
	
	/* i=si+m == M plane (right) */
	if (doflocation == DMDABCList_IMAX_LOC) {
		if (si+m == M) {
			PetscInt blockloc;
			PetscInt loc;
			
			i = si+m-1;
			blockloc = (i-si);
			loc = blockloc*ndof+dof_idx;
			pos[0] = LA_coords[i];
			pos[1] = 0.0;
			
           
			impose_dirichlet = eval(pos,&bc_val,ctx);
			if (impose_dirichlet) {
				idx[loc] = BCList_DIRICHLET;
				vals[loc] = bc_val;
			}
		}
	}
	
	if (doflocation == DMDABCList_JMIN_LOC) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONG,"DMDABCList_JMIN_LOC not valid for 1D DMDA");
	}
	
	if (doflocation == DMDABCList_JMAX_LOC) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONG,"DMDABCList_JMAX_LOC not valid for 1D DMDA");
	}
	
	if (doflocation == DMDABCList_IMAX_MIDPOINT) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONG,"DMDABCList_IMAX_MIDPOINT not valid for 1D DMDA");
	}
	
	if (doflocation == DMDABCList_IMIN_MIDPOINT) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONG,"DMDABCList_IMIN_MIDPOINT not valid for 1D DMDA");
	}
	
	if (doflocation == DMDABCList_IMIN_LOC_NC) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONG,"DMDABCList_IMIN_LOC_NC not valid for 1D DMDA");
	}
	
	if (doflocation == DMDABCList_IMAX_LOC_NC) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONG,"DMDABCList_IMAX_LOC_NC not valid for 1D DMDA");
	}
	
	if (doflocation == DMDABCList_TOPRIGHTCORNERPOINT) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONG,"DMDABCList_TOPRIGHTCORNERPOINT not valid for 1D DMDA");
	}
	
	if (doflocation == DMDABCList_TOPLEFTCORNERPOINT) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONG,"DMDABCList_TOPLEFTCORNERPOINT not valid for 1D DMDA");
	}
	
	if (doflocation == DMDABCList_BOTRIGHTCORNERPOINT) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONG,"DMDABCList_BOTRIGHTCORNERPOINT not valid for 1D DMDA");
	}
	
	if (doflocation == DMDABCList_BOTLEFTCORNERPOINT) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONG,"DMDABCList_BOTLEFTCORNERPOINT not valid for 1D DMDA");
	}    
	
	
	
	ierr = BCListRestoreGlobalIndices(list,&L,&idx);
	ierr = VecRestoreArray(coords,&LA_coords);CHKERRQ(ierr);
	ierr = BCListGlobalToLocal(list);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

/* read/write */
#undef __FUNCT__
#define __FUNCT__ "BCListGetGlobalIndices"
PetscErrorCode BCListGetGlobalIndices(BCList list,PetscInt *n,PetscInt **idx)
{
	if (n) {   *n   = list->L; }
	if (idx) { *idx = list->dofidx_global; }
	PetscFunctionReturn(0);
}
#undef __FUNCT__
#define __FUNCT__ "BCListRestoreGlobalIndices"
PetscErrorCode BCListRestoreGlobalIndices(BCList list,PetscInt *n,PetscInt **idx)
{
	PetscErrorCode ierr;
	if (idx) { 
		if (*idx != list->dofidx_global) {
			SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONG,"idx doesn't match");
		}
	}
	/* update cached info */
	ierr = BCListUpdateCache(list);CHKERRQ(ierr);
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BCListGetGlobalValues"
PetscErrorCode BCListGetGlobalValues(BCList list,PetscInt *n,PetscScalar **vals)
{
	if (n) {   *n     = list->L; }
	if (vals) { *vals = list->vals_global; }
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "BCListGetDofIdx"
PetscErrorCode BCListGetDofIdx(BCList list,PetscInt *Lg,PetscInt **dofidx_global,PetscInt *Ll,PetscInt **dofidx_local)
{
	if (Lg)            { *Lg   = list->L; }
	if (dofidx_global) { *dofidx_global = list->dofidx_global; }
	if (Ll)            { *Ll   = list->L_local; }
	if (dofidx_local)  { *dofidx_local = list->dofidx_local; }
	PetscFunctionReturn(0);
}

/* FILE: Basis functions */

void PTatinConstructNI_Q1_2D(const double _xi[],double Ni[])
{
  PetscScalar xi  = _xi[0];
  PetscScalar eta = _xi[1];
  
  Ni[0] = 0.25*(1.0-xi)*(1.0-eta); /*0-0*/
  Ni[1] = 0.25*(1.0+xi)*(1.0-eta); /*0-1*/
  
  Ni[2] = 0.25*(1.0-xi)*(1.0+eta); /*1-0*/
  Ni[3] = 0.25*(1.0+xi)*(1.0+eta); /*1-1*/
}

/*
 Q2 Element: Local basis function ordering
 6-----7------8 
 |     |      |
 |     |      |
 3-----4------5
 |     |      |
 |     |      |
 0-----1------2
 */
#define ConstructNI_Q2_2D(_xi,Ni)\
{\
PetscScalar xi  = _xi[0];\
PetscScalar eta = _xi[1];\
\
Ni[0] = 0.5*eta*(eta-1.0)       * 0.5*xi*(xi-1.0); /*0-0*/\
Ni[1] = 0.5*eta*(eta-1.0)       * (1.0+xi)*(1.0-xi); /*0-1*/\
Ni[2] = 0.5*eta*(eta-1.0)       * 0.5*(1.0+xi)*xi; /*0-2*/\
\
Ni[3] = (1.0+eta)*(1.0-eta) * 0.5*xi*(xi-1.0); /*1-0*/\
Ni[4] = (1.0+eta)*(1.0-eta) * (1.0+xi)*(1.0-xi); /*1-1*/\
Ni[5] = (1.0+eta)*(1.0-eta) * 0.5*(1.0+xi)*xi; /*1-2*/\
\
Ni[6] = 0.5*(1.0+eta)*eta  * 0.5*xi*(xi-1.0); /*2-0*/\
Ni[7] = 0.5*(1.0+eta)*eta  * (1.0+xi)*(1.0-xi); /*2-1*/\
Ni[8] = 0.5*(1.0+eta)*eta  * 0.5*(1.0+xi)*xi; /*2-2*/\
}

#define ConstructGNI_Q2_2D(_xi,GNi)\
{\
PetscScalar xi  = _xi[0];\
PetscScalar eta = _xi[1];\
\
GNi[0][0] = 0.5*eta*(eta-1.0)   * (xi-0.5); /*0-0*/\
GNi[0][1] = 0.5*eta*(eta-1.0)   * ( - 2.0 * xi ); /*0-1*/\
GNi[0][2] = 0.5*eta*(eta-1.0)   * 0.5*( 1.0 + 2.0 * xi ); /*0-2*/\
\
GNi[0][3] = (1.0+eta)*(1.0-eta) * (xi-0.5); /*1-0*/\
GNi[0][4] = (1.0+eta)*(1.0-eta) * ( - 2.0 * xi ); /*1-1*/\
GNi[0][5] = (1.0+eta)*(1.0-eta) * 0.5*( 1.0 + 2.0 * xi ); /*1-2*/\
\
GNi[0][6] = 0.5*(1.0+eta)*eta  * (xi-0.5); /*2-0*/\
GNi[0][7] = 0.5*(1.0+eta)*eta  * ( - 2.0 * xi ); /*2-1*/\
GNi[0][8] = 0.5*(1.0+eta)*eta  * 0.5*( 1.0 + 2.0 * xi ); /*2-2*/\
\
GNi[1][0] = (eta - 0.5) * 0.5*xi*(xi-1.0);   /*0-0*/\
GNi[1][1] = (eta - 0.5) * (1.0+xi)*(1.0-xi); /*0-1*/\
GNi[1][2] = (eta - 0.5) * 0.5*(1.0+xi)*xi;   /*0-2*/\
\
GNi[1][3] = (-2.0*eta) * 0.5*xi*(xi-1.0);   /*1-0*/\
GNi[1][4] = (-2.0*eta) * (1.0+xi)*(1.0-xi); /*1-1*/\
GNi[1][5] = (-2.0*eta) * 0.5*(1.0+xi)*xi;   /*1-2*/\
\
GNi[1][6] = 0.5*(1.0 + 2.0*eta) * 0.5*xi*(xi-1.0); /*2-0*/\
GNi[1][7] = 0.5*(1.0 + 2.0*eta) * (1.0+xi)*(1.0-xi); /*2-1*/\
GNi[1][8] = 0.5*(1.0 + 2.0*eta) * 0.5*(1.0+xi)*xi; /*2-2*/\
}

#define ConstructGNX_Q2_2D(_xi,GNx,coords,det_J)\
{\
const       PetscInt npe = 9;\
PetscScalar GNi[2][9];\
PetscScalar J00,J01,J10,J11,J;\
PetscScalar iJ00,iJ01,iJ10,iJ11;\
PetscInt    i;\
\
ConstructGNI_Q2_2D(_xi,GNi);\
J00 = J01 = J10 = J11 = 0.0;\
for( i=0; i<npe; i++ ) {\
double cx = coords[ 2*i + 0 ];\
double cy = coords[ 2*i + 1 ];\
\
J00 = J00 + GNi[0][i] * cx;  /* J_xx */\
J01 = J01 + GNi[0][i] * cy;  /* J_xy = dy/dxi */\
J10 = J10 + GNi[1][i] * cx;  /* J_yx = dx/deta */\
J11 = J11 + GNi[1][i] * cy;  /* J_yy */\
}\
J = (J00*J11) - (J01*J10);\
\
iJ00 =  J11/J;\
iJ01 = -J01/J;\
iJ10 = -J10/J;\
iJ11 =  J00/J;\
for( i=0; i<npe; i++ ) {\
GNx[0][i] = GNi[0][i] * iJ00 + GNi[1][i] * iJ01;\
GNx[1][i] = GNi[0][i] * iJ10 + GNi[1][i] * iJ11;\
}\
det_J = J;\
}

void PTatinConstructNI_Q2_2D(const double _xi[],double Ni[])
{
  PetscScalar xi  = _xi[0];
  PetscScalar eta = _xi[1];
  
  Ni[0] = 0.5*eta*(eta-1.0)       * 0.5*xi*(xi-1.0); /*0-0*/
  Ni[1] = 0.5*eta*(eta-1.0)       * (1.0+xi)*(1.0-xi); /*0-1*/
  Ni[2] = 0.5*eta*(eta-1.0)       * 0.5*(1.0+xi)*xi; /*0-2*/
  
  Ni[3] = (1.0+eta)*(1.0-eta) * 0.5*xi*(xi-1.0); /*1-0*/
  Ni[4] = (1.0+eta)*(1.0-eta) * (1.0+xi)*(1.0-xi); /*1-1*/
  Ni[5] = (1.0+eta)*(1.0-eta) * 0.5*(1.0+xi)*xi; /*1-2*/
  
  Ni[6] = 0.5*(1.0+eta)*eta  * 0.5*xi*(xi-1.0); /*2-0*/
  Ni[7] = 0.5*(1.0+eta)*eta  * (1.0+xi)*(1.0-xi); /*2-1*/
  Ni[8] = 0.5*(1.0+eta)*eta  * 0.5*(1.0+xi)*xi; /*2-2*/
}

void PTatinConstructGNI_Q2_2D(const double _xi[],double GNi[2][Q2_NODES_PER_EL_2D])
{
  PetscScalar xi  = _xi[0];
  PetscScalar eta = _xi[1];
  
  GNi[0][0] = 0.5*eta*(eta-1.0)   * (xi-0.5); /*0-0*/
  GNi[0][1] = 0.5*eta*(eta-1.0)   * ( - 2.0 * xi ); /*0-1*/
  GNi[0][2] = 0.5*eta*(eta-1.0)   * 0.5*( 1.0 + 2.0 * xi ); /*0-2*/
  
  GNi[0][3] = (1.0+eta)*(1.0-eta) * (xi-0.5); /*1-0*/
  GNi[0][4] = (1.0+eta)*(1.0-eta) * ( - 2.0 * xi ); /*1-1*/
  GNi[0][5] = (1.0+eta)*(1.0-eta) * 0.5*( 1.0 + 2.0 * xi ); /*1-2*/
  
  GNi[0][6] = 0.5*(1.0+eta)*eta  * (xi-0.5); /*2-0*/\
  GNi[0][7] = 0.5*(1.0+eta)*eta  * ( - 2.0 * xi ); /*2-1*/
  GNi[0][8] = 0.5*(1.0+eta)*eta  * 0.5*( 1.0 + 2.0 * xi ); /*2-2*/
  
  GNi[1][0] = (eta - 0.5) * 0.5*xi*(xi-1.0);   /*0-0*/
  GNi[1][1] = (eta - 0.5) * (1.0+xi)*(1.0-xi); /*0-1*/
  GNi[1][2] = (eta - 0.5) * 0.5*(1.0+xi)*xi;   /*0-2*/
  
  GNi[1][3] = (-2.0*eta) * 0.5*xi*(xi-1.0);   /*1-0*/
  GNi[1][4] = (-2.0*eta) * (1.0+xi)*(1.0-xi); /*1-1*/
  GNi[1][5] = (-2.0*eta) * 0.5*(1.0+xi)*xi;   /*1-2*/
  
  GNi[1][6] = 0.5*(1.0 + 2.0*eta) * 0.5*xi*(xi-1.0); /*2-0*/
  GNi[1][7] = 0.5*(1.0 + 2.0*eta) * (1.0+xi)*(1.0-xi); /*2-1*/
  GNi[1][8] = 0.5*(1.0 + 2.0*eta) * 0.5*(1.0+xi)*xi; /*2-2*/
}

void PTatinConstructGNX_Q2_2D(const double _xi[],double GNx[2][Q2_NODES_PER_EL_2D],double coords[],double *det_J)
{
  const       PetscInt npe = 9;
  PetscScalar GNi[2][9];
  PetscScalar J00,J01,J10,J11,J;
  PetscScalar iJ00,iJ01,iJ10,iJ11;
  PetscInt    i;
  
  ConstructGNI_Q2_2D(_xi,GNi);
  J00 = J01 = J10 = J11 = 0.0;
  for( i=0; i<npe; i++ ) {
    double cx = coords[ 2*i + 0 ];
    double cy = coords[ 2*i + 1 ];
    
    J00 = J00 + GNi[0][i] * cx;  /* J_xx */
    J01 = J01 + GNi[0][i] * cy;  /* J_xy = dy/dxi */
    J10 = J10 + GNi[1][i] * cx;  /* J_yx = dx/deta */
    J11 = J11 + GNi[1][i] * cy;  /* J_yy */
  }
  J = (J00*J11) - (J01*J10);
  
  iJ00 =  J11/J;
  iJ01 = -J01/J;
  iJ10 = -J10/J;
  iJ11 =  J00/J;
  for( i=0; i<npe; i++ ) {
    GNx[0][i] = GNi[0][i] * iJ00 + GNi[1][i] * iJ01;
    GNx[1][i] = GNi[0][i] * iJ10 + GNi[1][i] * iJ11;
  }
  *det_J = J;
}


void ConstructNi_P0_2D(const double _xi[],double coords[],double Ni[])
{
	//	printf("hey P0_2D\n");
	Ni[0] = 1.0;
}
void ConstructNi_P1L_2D(const double _xi[],double coords[],double Ni[])
{
	//	printf("hey P1L_2D\n");
	Ni[0] = 1.0;
	Ni[1] = _xi[0];
	Ni[2] = _xi[1];
}
void ConstructNi_P1G_2D(const double _xi[],double coords[],double Ni[])
{
	double Ni_geom[9];
	double _xg[] = {0.0,0.0};
	int i;
	
	//	printf("hey P1G_2D\n");
	ConstructNI_Q2_2D( _xi, Ni_geom );
	for( i=0; i<9; i++ ) {
		_xg[0] = _xg[0] + Ni_geom[i] * coords[2*i  ];
		_xg[1] = _xg[1] + Ni_geom[i] * coords[2*i+1];
	}
	//	printf("%f %f \n", _xg[0],_xg[1]);
	ConstructNi_P1L_2D( _xg, coords, Ni );
}
void ConstructNi_P1GRel_2D(const double _xi[],double coords[],double Ni[])
{
	double Ni_geom[9];
	double _xg[] = {0.0,0.0};
	double avg_x,avg_y,Lx,Ly;
	int i;
	
	//	printf("hey P1GRel_2D\n");
	ConstructNI_Q2_2D( _xi, Ni_geom );
	
	avg_x = avg_y = 0.0;
	for( i=0; i<9; i++ ) {
		_xg[0] = _xg[0] + Ni_geom[i] * coords[2*i  ];
		_xg[1] = _xg[1] + Ni_geom[i] * coords[2*i+1];
		
		avg_x = avg_x + coords[2*i  ];
		avg_y = avg_y + coords[2*i+1];
	}
	
	avg_x = (1.0/9.0) * avg_x;
	avg_y = (1.0/9.0) * avg_y;
	/*
	 6--7--8
	 3--4--5
	 0--1--2
	 */
	Lx = coords[2*5  ] - coords[2*3  ];
	Ly = coords[2*7+1] - coords[2*1+1];
	
	_xg[0] = ( _xg[0] - avg_x ) / Lx ;
	_xg[1] = ( _xg[1] - avg_y ) / Ly ;
	/*	
	 // -1 <= xi,eta <= 1.0 //
	 _xg[0] = 2.0*( _xg[0] - avg_x ) / Lx ;
	 _xg[1] = 2.0*( _xg[1] - avg_y ) / Ly ;
	 */
	ConstructNi_P1L_2D( _xg, coords, Ni );
}


/* FILE QuadratureStokes */
void QuadratureStokesCreateSubCells(PetscInt ngp,PetscScalar xi[][2],PetscScalar we[],PetscInt M[],PetscInt *_NGP,PetscScalar XI[][2],PetscScalar WE[])
{
	PetscInt i,NGP,p;
	PetscInt sx,sy;
	PetscScalar dxi,deta,vol;
	
	NGP = M[0]*M[1]*ngp;
	if (NGP > MAX_QUAD_PNTS) {
		SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Max quadrature points is 100");
	}
	
	p = 0;
	for (sx=0;sx<M[0];sx++) {
		for (sy=0;sy<M[1];sy++) {
			for (i=0;i<ngp;i++) {
				WE[p] = we[i];
				XI[p][0] = xi[i][0];
				XI[p][1] = xi[i][1];
				p++;
			}
		}
	}
	dxi  = 2.0/(PetscScalar)M[0];
	deta = 2.0/(PetscScalar)M[1];
	vol = dxi * deta;
	
	for (p=0;p<NGP;p++) {
		WE[p] = WE[p] * vol/4.0; /* scale volume */
	}
	/* scale positions */
	p = 0;
	for (sx=0;sx<M[0];sx++) {
		for (sy=0;sy<M[1];sy++) {
			PetscScalar shiftx,shifty;
			shiftx = -1.0 + 0.5 * dxi + sx * dxi;
			shifty = -1.0 + 0.5 * deta + sy * deta;
			for (i=0;i<ngp;i++) {
				XI[p][0] = shiftx + XI[p][0] / (PetscScalar)M[0];
				XI[p][1] = shifty + xi[i][1] / (PetscScalar)M[1];
				
				p++;
			}
		}
	}
	*_NGP = NGP;
}

void QuadratureCreateGauss_1pnt_2D(PetscInt *ngp,PetscScalar gp_xi[][2],PetscScalar gp_weight[])
{
	*ngp = 1;
	gp_xi[0][0] = 0.0;		gp_xi[0][1] = 0.0;
	gp_weight[0] = 1.0;
}
void QuadratureCreateGauss_2pnt_2D(PetscInt *ngp,PetscScalar gp_xi[][2],PetscScalar gp_weight[])
{
	*ngp = 4;
	gp_xi[0][0] = -0.57735026919;		gp_xi[0][1] = -0.57735026919;
	gp_xi[1][0] = -0.57735026919;		gp_xi[1][1] =  0.57735026919;
	gp_xi[2][0] =  0.57735026919;		gp_xi[2][1] =  0.57735026919;
	gp_xi[3][0] =  0.57735026919;		gp_xi[3][1] = -0.57735026919;
	gp_weight[0] = 1.0;
	gp_weight[1] = 1.0;
	gp_weight[2] = 1.0;
	gp_weight[3] = 1.0;
	
}
void QuadratureCreateGauss_3pnt_2D(PetscInt *ngp,PetscScalar gp_xi[][2],PetscScalar gp_weight[])
{
	*ngp = 9;
	gp_xi[0][0] = -0.774596669241483;     gp_xi[0][1] = -0.774596669241483;
	gp_xi[1][0] = -0.774596669241483;     gp_xi[1][1] =  0.0;
	gp_xi[2][0] = -0.774596669241483;   	gp_xi[2][1] =  0.774596669241483;
	
	gp_xi[3][0] = 0.0;               			gp_xi[3][1] = -0.774596669241483;
	gp_xi[4][0] = 0.0;                   	gp_xi[4][1] =  0.0;
	gp_xi[5][0] = 0.0;                 		gp_xi[5][1] =  0.774596669241483;
	
	gp_xi[6][0] = 0.774596669241483;    	gp_xi[6][1] = -0.774596669241483;
	gp_xi[7][0] = 0.774596669241483;     	gp_xi[7][1] =  0.0;
	gp_xi[8][0] = 0.774596669241483;     	gp_xi[8][1] =  0.774596669241483;
	
	gp_weight[0] = 0.3086419753086425;   	gp_weight[1] = 0.4938271604938276; 		gp_weight[2] = 0.3086419753086425;
	gp_weight[3] = 0.4938271604938276;  	gp_weight[4] = 0.7901234567901235;  	gp_weight[5] = 0.4938271604938276;
	gp_weight[6] = 0.3086419753086425;  	gp_weight[7] = 0.4938271604938276;  	gp_weight[8] = 0.3086419753086425;
	
}
void QuadratureCreateGauss_2x2pnt_2D(PetscInt *ngp,PetscScalar gp_xi[][2],PetscScalar gp_weight[])
{
	PetscInt M[2];
	PetscInt _ngp;
	PetscScalar _xi[4][2],_we[4];
	
	QuadratureCreateGauss_2pnt_2D(&_ngp,_xi,_we);
	
	M[0] = 2;
	M[1] = 2;
	QuadratureStokesCreateSubCells(_ngp,_xi,_we,M,ngp,gp_xi,gp_weight);	
}

void QuadratureCreateGauss_2x3pnt_2D(PetscInt *ngp,PetscScalar gp_xi[][2],PetscScalar gp_weight[])
{
	PetscInt M[2];
	PetscInt _ngp;
	PetscScalar _xi[3*3][2],_we[3*3];
	
	QuadratureCreateGauss_3pnt_2D(&_ngp,_xi,_we);
	
	M[0] = 2;
	M[1] = 2;
	QuadratureStokesCreateSubCells(_ngp,_xi,_we,M,ngp,gp_xi,gp_weight);	
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureCreateGauss_MxNppnt_2D"
PetscErrorCode QuadratureCreateGauss_MxNppnt_2D(PetscInt *ngp,PetscScalar gp_xi[][2],PetscScalar gp_weight[])
{
	PetscInt M[2];
	PetscInt _ngp;
	PetscScalar _xi[100*3][2],_we[10*10];
	PetscInt Mx,Np;
	
	Mx = 1;
	Np = 3;

	PetscOptionsGetInt(PETSC_NULL,"-quadrature_subcell_M",&Mx,0);
	PetscOptionsGetInt(PETSC_NULL,"-quadrature_subcell_Np",&Np,0);

	switch (Np) {
		case 1:
			QuadratureCreateGauss_1pnt_2D(&_ngp,_xi,_we);
			break;
		case 2:
			QuadratureCreateGauss_2pnt_2D(&_ngp,_xi,_we);
			break;
		case 3:
			QuadratureCreateGauss_3pnt_2D(&_ngp,_xi,_we);
			break;
		default:
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Sub cell quadrature rule only supports 1,2,3 point Gauss quadrature");
			break;
	}
	PetscPrintf(PETSC_COMM_WORLD,"QuadratureCreateGauss_MxNppnt_2D: Using %dx%d subcells per element with %dx%d point rule\n",Mx,Mx,Np,Np);
	
	M[0] = Mx;
	M[1] = Mx;
	QuadratureStokesCreateSubCells(_ngp,_xi,_we,M,ngp,gp_xi,gp_weight);	
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureStokesDestroy"
PetscErrorCode QuadratureStokesDestroy(QuadratureStokes *qq)
{
	PetscErrorCode ierr;
	PetscInt p,d;
	QuadratureStokes Q = *qq;
	
  PetscFunctionBegin;
	
	if (Q->ncells){ ierr = PetscFree(Q->cellproperties); CHKERRQ(ierr); }
	if (Q->xi){ ierr = PetscFree(Q->xi); CHKERRQ(ierr); }
	if (Q->weight){ ierr = PetscFree(Q->weight); CHKERRQ(ierr); }
	
	if (Q->U_Ni){ ierr = PetscFree(Q->U_Ni[0]); CHKERRQ(ierr); ierr = PetscFree(Q->U_Ni); CHKERRQ(ierr); }
	if (Q->p_Ni){ ierr = PetscFree(Q->p_Ni[0]); CHKERRQ(ierr); ierr = PetscFree(Q->p_Ni); CHKERRQ(ierr); }
	
	if (Q->U_GNi) {
		for (p=0;p<Q->ngp;p++) {
			for (d=0; d<NSD;d++) { ierr = PetscFree(Q->U_GNi[p][d]);CHKERRQ(ierr); }
			ierr = PetscFree( Q->U_GNi[p] );CHKERRQ(ierr);
		}
		ierr = PetscFree( Q->U_GNi );CHKERRQ(ierr);
	}
	
	ierr = PetscFree(Q);CHKERRQ(ierr);
	
	*qq = PETSC_NULL;
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureStokesGravityModelCoordinateAlignedSetValues"
PetscErrorCode QuadratureStokesGravityModelCoordinateAlignedSetValues(QuadratureStokes quadrature,PetscReal gx,PetscReal gy)
{
	PetscInt p,ngp,ncells;
	PetscErrorCode ierr;
	
  PetscFunctionBegin;	
	ngp    = quadrature->ngp;
	ncells = quadrature->ncells;
	for (p=0; p<ncells*ngp; p++) {
		quadrature->cellproperties[p].gravity[0] = gx;
		quadrature->cellproperties[p].gravity[1] = gy;
	}
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureStokesLoadGravityModel_CoordinateAligned"
PetscErrorCode QuadratureStokesLoadGravityModel_CoordinateAligned(QuadratureStokes quadrature)
{
	PetscReal gx,gy;
	PetscErrorCode ierr;
	
  PetscFunctionBegin;	
	gx =  0.0;
	gy = -1.0;
	ierr = PetscOptionsGetReal(PETSC_NULL,"-gravity_const_x",&gx,PETSC_NULL);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-gravity_const_y",&gy,PETSC_NULL);
	ierr = QuadratureStokesGravityModelCoordinateAlignedSetValues(quadrature,gx,gy);CHKERRQ(ierr);
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureStokesLoadGravityModel"
PetscErrorCode QuadratureStokesLoadGravityModel(QuadratureStokes quadrature)
{
	PetscErrorCode ierr;
	
  PetscFunctionBegin;
	ierr = QuadratureStokesLoadGravityModel_CoordinateAligned(quadrature);CHKERRQ(ierr);
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureStokesCreate"
PetscErrorCode QuadratureStokesCreate(QuadratureStokesRule rule,PetscInt ncells,QuadratureStokes *Q)
{
	QuadratureStokes quadrature;
	PetscInt p,ngp;
	PetscScalar gp_xi[MAX_QUAD_PNTS][2],gp_weight[MAX_QUAD_PNTS];
	PetscErrorCode ierr;
	
  PetscFunctionBegin;
	ierr = PetscMalloc( sizeof(struct _p_QuadratureStokes), &quadrature);CHKERRQ(ierr);
	
	PetscPrintf(PETSC_COMM_WORLD,"QuadratureStokesCreate:\n");
	switch (rule) {
		case QRule_GaussLegendre1:
			PetscPrintf(PETSC_COMM_WORLD,"\tUsing 1 pnt Gauss Legendre quadrature\n");
			QuadratureCreateGauss_1pnt_2D(&ngp,gp_xi,gp_weight);
			break;
			
		case QRule_GaussLegendre2:
			PetscPrintf(PETSC_COMM_WORLD,"\tUsing 2x2 pnt Gauss Legendre quadrature\n");
			QuadratureCreateGauss_2pnt_2D(&ngp,gp_xi,gp_weight);
			break;
			
		case QRule_GaussLegendre3:
			PetscPrintf(PETSC_COMM_WORLD,"\tUsing 3x3 pnt Gauss Legendre quadrature\n");
			QuadratureCreateGauss_3pnt_2D(&ngp,gp_xi,gp_weight);
			break;
			
		case QRule_Subcell2x2GaussLegendre3:
			PetscPrintf(PETSC_COMM_WORLD,"\tUsing 2x2 subcell, with 3x3 pnt Gauss Legendre quadrature in each\n");
			QuadratureCreateGauss_2x3pnt_2D(&ngp,gp_xi,gp_weight);
			break;
			
		case Qrule_SubcellMxMGaussLegendreNp:
			PetscPrintf(PETSC_COMM_WORLD,"\tUsing MxN subcell, with NpxNp pnt Gauss Legendre quadrature in each\n");
			ierr = QuadratureCreateGauss_MxNppnt_2D(&ngp,gp_xi,gp_weight);CHKERRQ(ierr);
			break;
			
		default: /* QRule_GaussLegendre3 */
			PetscPrintf(PETSC_COMM_WORLD,"\tUsing 3x3 pnt Gauss Legendre quadrature\n");
			QuadratureCreateGauss_3pnt_2D(&ngp,gp_xi,gp_weight);
			break;
	}
	
	quadrature->ncells = ncells;
	if (ncells!=0) {
		ierr = PetscMalloc( sizeof(GaussPointCoefficientsStokes)*ncells*ngp, &quadrature->cellproperties);CHKERRQ(ierr);
		ierr = PetscMemzero(quadrature->cellproperties,sizeof(GaussPointCoefficientsStokes)*ncells*ngp);CHKERRQ(ierr);
		/* init viscosity to one */
		for (p=0; p<ncells*ngp; p++) {
			quadrature->cellproperties[p].eta = 1.0;
		}
		PetscPrintf(PETSC_COMM_WORLD,"\tGaussPointCoefficientsStokes using %1.2lf MBytes \n", (double)(sizeof(GaussPointCoefficientsStokes)*ncells*ngp)*1.0e-6 );
	}
	
	/* allocate */
	quadrature->ngp = ngp;
	ierr = PetscMalloc( sizeof(PetscScalar)*ngp*NSD, &quadrature->xi );CHKERRQ(ierr);
	ierr = PetscMalloc( sizeof(PetscScalar)*ngp, &quadrature->weight );CHKERRQ(ierr);
	/* copy */
	for (p=0; p<ngp; p++) {
		quadrature->xi[NSD*p  ] = gp_xi[p][0];
		quadrature->xi[NSD*p+1] = gp_xi[p][1];
		
		quadrature->weight[p] = gp_weight[p];
	}
	
	ierr = QuadratureStokesLoadGravityModel(quadrature);CHKERRQ(ierr);
	
	*Q = quadrature;
  PetscFunctionReturn(0);
}

/*
 This is a bit shit as the pbasis cannot be defined locally on each element.
 The pressure space sometimes requries the global coordinates of the element.
 */
#undef __FUNCT__
#define __FUNCT__ "QuadratureStokesBasisSetUp"
PetscErrorCode QuadratureStokesBasisSetUp(QuadratureStokes Q,BasisType ubasis,BasisType pbasis)
{
	PetscInt nubasis,npbasis;
	PetscScalar *U_Ni,*p_Ni;
	PetscInt p,d;
	PetscErrorCode ierr;
	
  PetscFunctionBegin;
	nubasis = 0;
	switch (ubasis) {
		case Basis_Q1:
			nubasis = 4;	
			break;
		case Basis_Q2:
			nubasis = 9;	
			break;
		default:
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"No default basis for velocity can be determined");
			break;
	}
	
	npbasis = 0;
	switch (pbasis) {
		case Basis_P0:
			npbasis = 1;	
			break;
		case Basis_P1L:
			npbasis = 3;	
			break;
		case Basis_P1G:
			npbasis = 3;	
			break;
		case Basis_P1GR:
			npbasis = 3;	
			break;
		default:
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"No default basis for pressure can be determined");
			break;
	}
	Q->u_basis = nubasis;
	Q->p_basis = npbasis;
	
	/* basis */
	/* allocate */
	ierr = PetscMalloc( sizeof(PetscScalar)*Q->ngp*Q->u_basis, &U_Ni );CHKERRQ(ierr);
	ierr = PetscMalloc( sizeof(PetscScalar)*Q->ngp*Q->p_basis, &p_Ni );CHKERRQ(ierr);
	
	ierr = PetscMalloc( sizeof(PetscScalar*)*Q->ngp, &Q->U_Ni );CHKERRQ(ierr);
	for (p=0; p<Q->ngp; p++) {
		Q->U_Ni[p] = &U_Ni[p*Q->u_basis];
	}
	
	ierr = PetscMalloc( sizeof(PetscScalar*)*Q->ngp, &Q->p_Ni );CHKERRQ(ierr);
	for (p=0; p<Q->ngp; p++) {
		Q->p_Ni[p] = &p_Ni[p*Q->p_basis];
	}
	
	ierr = PetscMalloc( sizeof(PetscScalar**)*Q->ngp, &Q->U_GNi );CHKERRQ(ierr);
	for (p=0;p<Q->ngp;p++) {
		ierr = PetscMalloc( sizeof(PetscScalar*)*NSD, &Q->U_GNi[p] );CHKERRQ(ierr);
		for (d=0; d<NSD;d++) {
			ierr = PetscMalloc( sizeof(PetscScalar)*nubasis, &Q->U_GNi[p][d] );CHKERRQ(ierr);
		}
	}
	
	/* assign values */
	for (p=0; p<Q->ngp; p++) {
		PetscScalar xip[2];
		
		xip[0] = Q->xi[NSD*p  ];
		xip[1] = Q->xi[NSD*p+1];
		
		switch (ubasis) {
			case Basis_Q1:
				SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Basis_Q1 not implemented");
				break;
			case Basis_Q2:
				ConstructNI_Q2_2D(xip,Q->U_Ni[p]);
				ConstructGNI_Q2_2D(xip,Q->U_GNi[p]);
				break;
		}
		
		switch (pbasis) {
			case Basis_P0:
				ConstructNi_P0_2D(xip,PETSC_NULL,Q->p_Ni[p]);
				break;
			case Basis_P1L:
				ConstructNi_P1L_2D(xip,PETSC_NULL,Q->p_Ni[p]);
				break;
			case Basis_P1G:
				ConstructNi_P1L_2D(xip,PETSC_NULL,Q->p_Ni[p]);
				break;
			case Basis_P1GR:
				ConstructNi_P1L_2D(xip,PETSC_NULL,Q->p_Ni[p]);
				break;
		}
	}
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureStokesCoordinateSetUp"
PetscErrorCode QuadratureStokesCoordinateSetUp(QuadratureStokes Q,DM da)
{
	PetscErrorCode ierr;
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen,e,i,p,gp;
	const PetscInt *elnidx;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D];
	
  PetscFunctionBegin;
	/* setup for coords */
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(da,&gcoords);CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(da,&nel,&nen,&elnidx);CHKERRQ(ierr);
	p = 0;
	for (e=0;e<nel;e++) {
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx[nen*e],LA_gcoords);CHKERRQ(ierr);
		
		for (gp=0; gp<Q->ngp; gp++) {
			GaussPointCoefficientsStokes *point = &Q->cellproperties[p];
			PetscScalar xp,yp;
			
			xp = 0.0;
			yp = 0.0;
			for (i=0; i<Q->u_basis; i++) {
				xp = xp + Q->U_Ni[gp][i] * elcoords[NSD*i  ];
				yp = yp + Q->U_Ni[gp][i] * elcoords[NSD*i+1];
			}
			point->coord[0] = xp;
			point->coord[1] = yp;
			
			p++;
		}
	}
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MaterialPointCoordinateSetUp"
PetscErrorCode MaterialPointCoordinateSetUp(DataBucket db,DM da)
{
	PetscErrorCode ierr;
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen,e,i,p,n_mp_points;
	const PetscInt *elnidx;
	PetscScalar Ni_p[Q2_NODES_PER_EL_2D];
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D];
	DataField      PField_std;
	
	
  PetscFunctionBegin;
	/* setup for coords */
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(da,&gcoords);CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(da,&nel,&nen,&elnidx);CHKERRQ(ierr);

	DataBucketGetSizes(db,&n_mp_points,0,0);
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	
	for (p=0; p<n_mp_points; p++) {
		MPntStd     *material_point;
		double      xp[2];
		double      *xi;
		
		DataFieldAccessPoint(PField_std,p,(void**)&material_point);

		MPntStdGetField_local_element_index(material_point,&e);
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx[nen*e],LA_gcoords);CHKERRQ(ierr);

		MPntStdGetField_local_coord(material_point,&xi);
		PTatinConstructNI_Q2_2D(xi,Ni_p);
		
		xp[0] = 0.0;
		xp[1] = 0.0;
		for (i=0; i<Q2_NODES_PER_EL_2D; i++) {
			xp[0] = xp[0] + Ni_p[i] * elcoords[NSD*i  ];
			xp[1] = xp[1] + Ni_p[i] * elcoords[NSD*i+1];
		}
		
		MPntStdSetField_global_coord(material_point,xp);
		
	}
	
	DataFieldRestoreAccess(PField_std);

	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureStokesGetCell"
PetscErrorCode QuadratureStokesGetCell(QuadratureStokes Q,PetscInt cidx,GaussPointCoefficientsStokes **points)
{
  PetscFunctionBegin;
	if (cidx>=Q->ncells) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_SIZ,"cidx > max cells");
	}
	*points = &Q->cellproperties[cidx*Q->ngp];
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "QuadratureStokesView"
PetscErrorCode QuadratureStokesView(QuadratureStokes Q,const char prefix[])
{
  FILE           *fp;
  char           fname[PETSC_MAX_PATH_LEN];
  PetscMPIInt    rank;
	PetscInt       c,gp;
  PetscErrorCode ierr;
	
  PetscFunctionBegin;
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);
  ierr = PetscSNPrintf(fname,sizeof fname,"%s-p%1.4d.dat",prefix,rank);CHKERRQ(ierr);
  ierr = PetscFOpen(PETSC_COMM_SELF,fname,"w",&fp);CHKERRQ(ierr);
  if (!fp) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file");
	
  ierr = PetscFPrintf(PETSC_COMM_SELF,fp,"# QuadratureStokesView %1.4d  \n",rank);CHKERRQ(ierr);
  ierr = PetscFPrintf(PETSC_COMM_SELF,fp,"# ncells %d  \n",Q->ncells);CHKERRQ(ierr);
  ierr = PetscFPrintf(PETSC_COMM_SELF,fp,"# fields ( x, y, eta, Fu_x, Fu_y, Fp ) \n");CHKERRQ(ierr);
	
	for (c=0; c<Q->ncells; c++) {
		GaussPointCoefficientsStokes *point;
		ierr = QuadratureStokesGetCell(Q,c,&point);CHKERRQ(ierr);
		ierr = PetscFPrintf(PETSC_COMM_SELF,fp,"#cell %d\n",c);CHKERRQ(ierr);
		for (gp=0; gp<Q->ngp; gp++) {
			ierr = PetscFPrintf(PETSC_COMM_SELF,fp,"%1.4e %1.4e %1.4e %1.4e %1.4e %1.4e\n",point[gp].coord[0],point[gp].coord[1],point[gp].eta,point[gp].Fu[0],point[gp].Fu[1],point[gp].Fp);CHKERRQ(ierr);
		}
		ierr = PetscFPrintf(PETSC_COMM_SELF,fp,"\n");CHKERRQ(ierr);
	}
  
	ierr = PetscFClose(PETSC_COMM_SELF,fp);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

/* DA Q2 2D */
#undef __FUNCT__
#define __FUNCT__ "DMDAGetSizeElementQ2"
PetscErrorCode DMDAGetSizeElementQ2(DM da,PetscInt *MX,PetscInt *MY,PetscInt *MZ)
{
	const PetscInt order = 2;
	PetscInt M,N,P,mx,my,mz,width;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	ierr = DMDAGetInfo(da,0,&M,&N,&P, 0,0,0, 0,&width, 0,0,0, 0);CHKERRQ(ierr);
	if (width!=2) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Stencil width must be 2 for Q2");
	}
	
	/* M = order*mx+1 */
	if ( (M-1)%order != 0 ) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"DMDA is not compatible with Q2 elements in x direction");
	}
	if (MX) { *MX = (M-1)/order; }
	
	//
	if ( (N-1)%order != 0 ) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"DMDA is not compatible with Q2 elements in y direction");
	}
	if (MY) { *MY = (N-1)/order; }
	
	//
	if ( (P-1)%order != 0 ) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"DMDA is not compatible with Q2 elements in z direction");
	}
	if (MZ) { *MZ = (P-1)/order; }
	
	PetscFunctionReturn(0);
}
#undef __FUNCT__
#define __FUNCT__ "DMDAGetLocalSizeElementQ2"
PetscErrorCode DMDAGetLocalSizeElementQ2(DM da,PetscInt *mx,PetscInt *my,PetscInt *mz)
{
	const PetscInt order = 2;
	PetscInt i,j,k;
	PetscInt cntx,cnty,cntz;
	PetscInt si,sj,sk,m,n,p,M,N,P,width;
	PetscErrorCode ierr;
	int rank;
	
	PetscFunctionBegin;
	ierr = DMDAGetInfo(da,0,&M,&N,&P,0,0,0, 0,&width, 0,0,0, 0);CHKERRQ(ierr);
	ierr = DMDAGetCorners(da,&si,&sj,&sk,&m,&n,&p);CHKERRQ(ierr);
	if (width!=2) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Stencil width must be 2 for Q2");
	}
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	//printf("[%d]: i(%d->%d) : j(%d->%d) \n", rank,si,si+m,sj,sj+n);
	
	cntx = cnty = cntz = 0;
	for (i=si; i<si+m; i++) {
		if (i%order==0) { /*printf("rank%d claiming %d (x)\n",rank,i);*/ cntx++; }
	}
	if (si+m==M) {cntx--;}
	
	for (j=sj; j<sj+n; j++) {
		if (j%order==0) { /*printf("rank%d claiming %d (y)\n",rank,j);*/ cnty++; }
	}
	if (sj+n==N) {cnty--;}
	
	for (k=sk; k<sk+p; k++) {
		if (k%order==0) { cntz++; }
	}
	if (sk+p==P) {cntz--;}
	
	if (mx) { *mx = cntx; }
	if (my) { *my = cnty; }
	if (mz) { *mz = cntz; }
	PetscFunctionReturn(0);
}
#undef __FUNCT__
#define __FUNCT__ "DMDAGetCornersElementQ2"
PetscErrorCode DMDAGetCornersElementQ2(DM da,PetscInt *sei,PetscInt *sej,PetscInt *sek,PetscInt *mx,PetscInt *my,PetscInt *mz)
{
	const PetscInt order = 2;
	PetscInt i,j,k;
	PetscInt cntx,cnty,cntz;
	PetscInt si,sj,sk,m,n,p,M,N,P,width;
	PetscErrorCode ierr;
	int rank;
	
	PetscFunctionBegin;
	ierr = DMDAGetInfo(da,0,&M,&N,&P,0,0,0, 0,&width, 0,0,0, 0);CHKERRQ(ierr);
	ierr = DMDAGetCorners(da,&si,&sj,&sk,&m,&n,&p);CHKERRQ(ierr);
	if (width!=2) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Stencil width must be 2 for Q2");
	}
	
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	/*printf("[%d]: %d->%d : %d->%d \n", rank,si,si+m,sj,sj+n);*/
	
	cntx = cnty = cntz = 0;
	for (i=si; i<si+m; i++) {
		if (i%order==0) { break; }
	}
	if (sei) { *sei = i; }
	
	for (j=sj; j<sj+n; j++) {
		if (j%order==0) {  break; }
	}
	if (sej) { *sej = j; }
	
	for (k=sk; k<sk+p; k++) {
		if (k%order==0) { break; }
	}
	if (sek) { *sek = k; }
	
	ierr = DMDAGetLocalSizeElementQ2(da,mx,my,mz);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDAGetOwnershipRangesElementQ2"
PetscErrorCode DMDAGetOwnershipRangesElementQ2(DM da,PetscInt *m,PetscInt *n,PetscInt *p,PetscInt **si,PetscInt **sj,PetscInt **sk,PetscInt **_mx,PetscInt **_my,PetscInt **_mz)
{
	PetscMPIInt nproc,rank;
	MPI_Comm comm;
	PetscInt M,N,P,pM,pN,pP;
	PetscInt i,j,k,II,dim,esi,esj,esk,mx,my,mz;
	PetscInt *olx,*oly,*olz;
	PetscInt *lmx,*lmy,*lmz,*tmp;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	/* create file name */
	PetscObjectGetComm( (PetscObject)da, &comm );
	MPI_Comm_size( comm, &nproc );
	MPI_Comm_rank( comm, &rank );
	
	ierr = DMDAGetInfo( da, &dim, &M,&N,&P, &pM,&pN,&pP, 0, 0, 0,0,0, 0 );CHKERRQ(ierr);
	ierr = DMDAGetCornersElementQ2(da,&esi,&esj,&esk,&mx,&my,&mz);CHKERRQ(ierr);

	if (dim == 1) {
		pN = 1;
		pP = 1;
	}
	if (dim == 2) {
		pP = 1;
	}
	
	ierr = PetscMalloc( sizeof(PetscInt)*(nproc), &tmp );CHKERRQ(ierr);
	
	ierr = PetscMalloc( sizeof(PetscInt)*(pM+1), &olx );CHKERRQ(ierr);
	ierr = PetscMalloc( sizeof(PetscInt)*(pN+1), &oly );CHKERRQ(ierr);
	ierr = PetscMalloc( sizeof(PetscInt)*(pP+1), &olz );CHKERRQ(ierr);
	
	ierr = PetscMalloc( sizeof(PetscInt)*(pM+1), &lmx );CHKERRQ(ierr);
	ierr = PetscMalloc( sizeof(PetscInt)*(pN+1), &lmy );CHKERRQ(ierr);
	ierr = PetscMalloc( sizeof(PetscInt)*(pP+1), &lmz );CHKERRQ(ierr);
	
	if (dim >= 1) {
		MPI_Allgather ( &esi, 1, MPI_INT, tmp, 1, MPI_INT, comm );
		j = k = 0;
		for( i=0; i<pM; i++ ) {
			PetscInt procid = i + j*pM; /* convert proc(i,j,k) to pid */
			olx[i] = tmp[procid];
		}
		
		MPI_Allgather ( &mx, 1, MPI_INT, tmp, 1, MPI_INT, comm );
		j = k = 0;
		for( i=0; i<pM; i++ ) {
			PetscInt procid = i + j*pM; /* convert proc(i,j,k) to pid */
			lmx[i] = tmp[procid];
		}
	}

	if (dim >= 2 ) {
		MPI_Allgather ( &esj, 1, MPI_INT, tmp, 1, MPI_INT, comm );
		i = k = 0;
		for( j=0; j<pN; j++ ) {
			PetscInt procid = i + j*pM; /* convert proc(i,j,k) to pid */
			oly[j] = tmp[procid];
		}
		
		MPI_Allgather ( &my, 1, MPI_INT, tmp, 1, MPI_INT, comm );
		i = k = 0;
		for( j=0; j<pN; j++ ) {
			PetscInt procid = i + j*pM; /* convert proc(i,j,k) to pid */
			lmy[j] = tmp[procid];
		}
	}

	if (dim == 3 ) {
		MPI_Allgather ( &esk, 1, MPI_INT, tmp, 1, MPI_INT, comm );
		i = j = 0;
		for( k=0; k<pP; k++ ) {
			PetscInt procid = i + j*pM + k*pM*pN; /* convert proc(i,j,k) to pid */
			olz[k] = tmp[procid];
		}
		
		MPI_Allgather ( &mz, 1, MPI_INT, tmp, 1, MPI_INT, comm );
		i = j = 0;
		for( k=0; k<pP; k++ ) {
			PetscInt procid = i + j*pM + k*pM*pN; /* convert proc(i,j,k) to pid */
			lmz[k] = tmp[procid];
		}
	}

	if(m) { *m = pM; }
	if(n) { *n = pN; }
	if(p) { *p = pP; }

	if(si) { *si = olx; }
	if(sj) { *sj = oly; }
	if(sk) { *sk = olz; }

	if(_mx) { *_mx = lmx; }
	if(_my) { *_my = lmy; }
	if(_mz) { *_mz = lmz; }
	
	ierr = PetscFree(tmp);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDAGetElements_DA_Q2_2D"
PetscErrorCode DMDAGetElements_DA_Q2_2D(DM dm,PetscInt *nel,PetscInt *npe,const PetscInt **eidx)
{
  DM_DA          *da = (DM_DA*)dm->data;
	const PetscInt order = 2;
	PetscErrorCode ierr;
	PetscInt *idx,mx,my,_npe, M,N,P;
	PetscInt ei,ej,i,j,elcnt,esi,esj,gsi,gsj,nid[9],n,d,X,width;
	PetscInt *el;
	PetscInt *gidx,ngidx,dof;
	int rank;
	PetscFunctionBegin;
	
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	ierr = DMDAGetInfo(dm,0, &M,&N,&P, 0,0,0, 0,&width, 0,0,0, 0);CHKERRQ(ierr);
	if (width!=2) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Stencil width must be 2 for Q2");
	}
	
	_npe = (order + 1)*(order + 1);
  if (!da->e) {
		ierr = DMDAGetCornersElementQ2(dm,&esi,&esj,0,&mx,&my,0);CHKERRQ(ierr);
		ierr = PetscMalloc(sizeof(PetscInt)*(mx*my*_npe+1),&idx);CHKERRQ(ierr);
		ierr = DMDAGetGlobalIndices(dm,&ngidx,&gidx);CHKERRQ(ierr);
		ierr = DMDAGetGhostCorners(dm,&gsi,&gsj,0, &X,0,0);CHKERRQ(ierr);
		ierr = DMDAGetInfo(dm,0, 0,0,0, 0,0,0, &dof,0, 0,0,0, 0);CHKERRQ(ierr);
		
		/*
		 printf("[%d] ngidx=%d dof=%d\n", rank,ngidx,dof);
		 if(rank==0){
		 for(i=0;i<ngidx;i++) {
		 printf("gidx[%d]=%d\n",i,gidx[i]);
		 }
		 }
		 */
		
		elcnt = 0;
#if 0		
		for (ej=0; ej<my; ej++) {
			j = sj + 2*ej;
			for (ei=0; ei<mx; ei++) {
				i = si + 2*ei;
				el = &idx[_npe*elcnt];
				
				nid[0] = (i  ) + (j  ) *X; 
				nid[1] = (i+1) + (j  ) *X; 
				nid[2] = (i+2) + (j  ) *X; 
				
				nid[3] = (i  ) + (j+1) *X; 
				nid[4] = (i+1) + (j+1) *X; 
				nid[5] = (i+2) + (j+1) *X; 
				
				nid[6] = (i  ) + (j+2) *X; 
				nid[7] = (i+1) + (j+2) *X; 
				nid[8] = (i+2) + (j+2) *X; 
				
				for (n=0; n<_npe; n++) {
					el[n] = gidx[dof*nid[n]+0]/dof;
				}
				
				elcnt++;
			}
		}
#endif
		
		//		printf("[%d]: esi=%d, gsi=%d\n", rank,esi,gsi);
		for (ej=0; ej<my; ej++) {
			j = (esj-gsj) + 2*ej;
			for (ei=0; ei<mx; ei++) {
				i = (esi-gsi) + 2*ei;
				el = &idx[_npe*elcnt];
				
				//				printf("[%d]: i=%d \n", rank,i );
				nid[0] = (i  ) + (j  ) *X; 
				nid[1] = (i+1) + (j  ) *X; 
				nid[2] = (i+2) + (j  ) *X; 
				
				nid[3] = (i  ) + (j+1) *X; 
				nid[4] = (i+1) + (j+1) *X; 
				nid[5] = (i+2) + (j+1) *X; 
				
				nid[6] = (i  ) + (j+2) *X; 
				nid[7] = (i+1) + (j+2) *X; 
				nid[8] = (i+2) + (j+2) *X; 
				//				if(rank==0) printf("%d,%d, %d %d %d, %d %d %d , %d %d %d \n", ei,ej,
				//													 nid[0],nid[1],nid[2],nid[3],nid[4],nid[5],nid[6],nid[7],nid[8] );
				
				for (n=0; n<_npe; n++) {
					if (nid[n]>M*N*P) { 
						SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_CORRUPT,"Local indexing exceeds number of global nodes");
					}
					el[n] = nid[n]; //gidx[dof*nid[n]+0]/dof;
				}
				
				elcnt++;
			}
		}
		
		da->e  = idx;
		da->ne = elcnt;
	}
	
	*eidx = da->e;
	*npe = _npe;
	*nel = da->ne;
	
	PetscFunctionReturn(0);
}
#undef __FUNCT__
#define __FUNCT__ "DMDAGetElements_DA_P0MD_2D"
PetscErrorCode DMDAGetElements_DA_P0MD_2D(DM dm,PetscInt *nel,PetscInt *npe,const PetscInt **eidx)
{
  DM_DA          *da = (DM_DA*)dm->data;
	PetscErrorCode ierr;
	PetscInt *idx,mx,my,_npe;
	PetscInt ei,ej,i,j,elcnt,cnt,esi,esj,gsi,gsj,nid[100],d,X,width;
	PetscInt *el,M,N,P,dof;
	int rank;
	PetscFunctionBegin;
	
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	ierr = DMDAGetInfo(dm,0, &M,&N,&P, 0,0,0, 0,&width, 0,0,0, 0);CHKERRQ(ierr);
	if (width!=0) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Stencil width must be 0 for P0 with multi-dofs");
	}
	
	_npe = 1;
	ierr = DMDAGetInfo(dm,0, 0,0,0, 0,0,0, &dof,0, 0,0,0, 0);CHKERRQ(ierr);
  if (dof>100) { SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"dof > 100"); }
	
	if (!da->e) {
		ierr = DMDAGetCorners(dm,&esi,&esj,0,&mx,&my,0);CHKERRQ(ierr);
		ierr = PetscMalloc(sizeof(PetscInt)*(mx*my*_npe*dof+1),&idx);CHKERRQ(ierr); /* we add one extra space to allow for cases when a proc has zero elements, and we don't want to malloc 0 bytes */
		
		elcnt = 0;
		cnt = 0;
		for (ej=0; ej<my; ej++) {
			for (ei=0; ei<mx; ei++) {
				el = &idx[_npe*dof*elcnt];
				
				for (d=0; d<_npe*dof; d++) {
					nid[d] = (_npe*dof) * elcnt + d;
				}
				//				printf("[r=%d] %d,%d { %d %d %d } \n", rank,ei,ej,
				//																	 nid[0],nid[1],nid[2] );
				for (d=0; d<_npe*dof; d++) {
					if (nid[d]>M*N*P*dof) {
						SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_CORRUPT,"Local indexing exceeds number of global dofs");
					}
					el[d] = nid[d];
				}
				
				elcnt++;
			}
		}
		
		da->e  = idx;
		da->ne = elcnt;
	}
	
	*eidx = da->e;
	*npe = _npe * dof;
	*nel = da->ne;
	
	PetscFunctionReturn(0);
}
#undef __FUNCT__
#define __FUNCT__ "DMDAGetElements_DA_Q2"
PetscErrorCode DMDAGetElements_DA_Q2(DM dm,PetscInt *nel,PetscInt *nen,const PetscInt *e[])
{
  DM_DA          *da = (DM_DA*)dm->data;
  PetscErrorCode ierr;
  PetscFunctionBegin;
  if (da->dim==-1) {
    *nel = 0; *nen = 0; *e = PETSC_NULL;
  } else if (da->dim==1) {
    SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"DMDA doesn't support Q2 in 1D");
  } else if (da->dim==2) {
    ierr = DMDAGetElements_DA_Q2_2D(dm,nel,nen,e);CHKERRQ(ierr);
  } else if (da->dim==3) {
    SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"DMDA doesn't support Q2 in 3D");
  } else {
    SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_ARG_CORRUPT,"DMDA dimension not 1, 2, or 3, it is %D\n",da->dim);
  }
	PetscFunctionReturn(0);
}
#undef __FUNCT__
#define __FUNCT__ "DMDAGetElements_DA_P1"
PetscErrorCode DMDAGetElements_DA_P1(DM dm,PetscInt *nel,PetscInt *nen,const PetscInt *e[])
{
  DM_DA          *da = (DM_DA*)dm->data;
  PetscErrorCode ierr;
  PetscFunctionBegin;
  if (da->dim==-1) {
    *nel = 0; *nen = 0; *e = PETSC_NULL;
  } else if (da->dim==1) {
    SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"DMDA doesn't support P1 in 1D");
  } else if (da->dim==2) {
    ierr = DMDAGetElements_DA_P0MD_2D(dm,nel,nen,e);CHKERRQ(ierr);
  } else if (da->dim==3) {
    SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"DMDA doesn't support P1 in 3D");
  } else {
    SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_ARG_CORRUPT,"DMDA dimension not 1, 2, or 3, it is %D\n",da->dim);
  }
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDAGetElements_pTatin"
PetscErrorCode DMDAGetElements_pTatin(DM dm,PetscInt *nel,PetscInt *nen,const PetscInt *e[])
{
  PetscErrorCode ierr;
  PetscInt dof,sw;
  static PetscInt beenHereQ2 = 0;
  static PetscInt beenHereP0 = 0;
  
	ierr = DMDAGetInfo(dm, 0, 0,0,0, 0,0,0, &dof,&sw, 0,0,0, 0);CHKERRQ(ierr);
  
  if (sw == 2) {
    if (!beenHereQ2) { PetscPrintf(PETSC_COMM_WORLD,"ASSUMING stencil width=2 implies Q2 basis...\n"); }
    ierr = DMDAGetElements_DA_Q2(dm,nel,nen,e);CHKERRQ(ierr);
    beenHereQ2 = 1;
  } else if (sw == 0) {
    if (!beenHereP0) { PetscPrintf(PETSC_COMM_WORLD,"ASSUMING stencil width=0 implies P0 basis...\n"); }
    ierr = DMDAGetElements_DA_P1(dm,nel,nen,e);CHKERRQ(ierr);
    beenHereP0 = 1;
  } else {
    ierr = DMDAGetElements(dm,nel,nen,e);CHKERRQ(ierr);
  }
  
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "Q2GetElementLocalIndicesDOF"
PetscErrorCode Q2GetElementLocalIndicesDOF(PetscInt el_localIndices[],PetscInt ndof,PetscInt elnid[])
{
	PetscInt n,d;
	PetscErrorCode ierr;
	PetscFunctionBegin;
	for (d=0; d<ndof; d++) {
		for (n=0; n<U_BASIS_FUNCTIONS; n++) {
			el_localIndices[ndof*n+d] = ndof*elnid[n]+d;
		}
	}		
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "StokesVelocity_GetElementLocalIndices"
PetscErrorCode StokesVelocity_GetElementLocalIndices(PetscInt el_localIndices[],PetscInt elnid[])
{
	PetscInt n;
	PetscErrorCode ierr;
	PetscFunctionBegin;
	for (n=0; n<U_BASIS_FUNCTIONS; n++) {
		el_localIndices[2*n  ] = 2*elnid[n]  ;
		el_localIndices[2*n+1] = 2*elnid[n]+1;
	}
	PetscFunctionReturn(0);
}
#undef __FUNCT__
#define __FUNCT__ "StokesPressure_GetElementLocalIndices"
PetscErrorCode StokesPressure_GetElementLocalIndices(PetscInt el_localIndices[],PetscInt elnid[])
{
	PetscInt n;
	PetscErrorCode ierr;
	PetscFunctionBegin;
	for (n=0; n<P_BASIS_FUNCTIONS; n++) {
		el_localIndices[n] = elnid[n];
	}
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDAGetElementCoordinatesQ2_2D"
PetscErrorCode DMDAGetElementCoordinatesQ2_2D(PetscScalar elcoords[],PetscInt elnid[],PetscScalar LA_gcoords[])
{
	PetscInt n;
	PetscErrorCode ierr;
	PetscFunctionBegin;
	for (n=0; n<9; n++) {
		elcoords[2*n  ] = LA_gcoords[2*elnid[n]  ];
		elcoords[2*n+1] = LA_gcoords[2*elnid[n]+1];
	}
	PetscFunctionReturn(0);
}
#undef __FUNCT__
#define __FUNCT__ "DMDAGetScalarElementField_2D"
PetscErrorCode DMDAGetScalarElementField_2D(PetscScalar elfield[],PetscInt npe,PetscInt elnid[],PetscScalar LA_gfield[])
{
	PetscInt n;
	PetscErrorCode ierr;
	PetscFunctionBegin;
	for (n=0; n<npe; n++) {
		elfield[n] = LA_gfield[elnid[n]];
	}
	PetscFunctionReturn(0);
}
#undef __FUNCT__
#define __FUNCT__ "DMDAGetScalarElementFieldQ2_2D"
PetscErrorCode DMDAGetScalarElementFieldQ2_2D(PetscScalar elfield[],PetscInt elnid[],PetscScalar LA_gfield[])
{
	PetscInt n;
	PetscErrorCode ierr;
	PetscFunctionBegin;
	for (n=0; n<9; n++) {
		elfield[n] = LA_gfield[elnid[n]];
	}
	PetscFunctionReturn(0);
}
#undef __FUNCT__
#define __FUNCT__ "DMDAGetVectorElementFieldQ2_2D"
PetscErrorCode DMDAGetVectorElementFieldQ2_2D(PetscScalar elfield[],PetscInt elnid[],PetscScalar LA_gfield[])
{
	PetscInt n;
	PetscErrorCode ierr;
	PetscFunctionBegin;
	for (n=0; n<9; n++) {
		elfield[2*n  ] = LA_gfield[2*elnid[n]  ];
		elfield[2*n+1] = LA_gfield[2*elnid[n]+1];
	}
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "GetElementEqnIndicesQ2"
PetscErrorCode GetElementEqnIndicesQ2(PetscInt elnidx[],PetscInt globalindices[],PetscInt eldofidx[])
{
	PetscInt nid,idx,n,c;
	
	PetscFunctionBegin;
	n = 0; c = 0;
  nid = elnidx[n++];	eldofidx[c++] = globalindices[2*nid];	  eldofidx[c++] = globalindices[2*nid+1];
  nid = elnidx[n++];	eldofidx[c++] = globalindices[2*nid];	  eldofidx[c++] = globalindices[2*nid+1];
  nid = elnidx[n++];	eldofidx[c++] = globalindices[2*nid];	  eldofidx[c++] = globalindices[2*nid+1];
	
  nid = elnidx[n++];	eldofidx[c++] = globalindices[2*nid];	  eldofidx[c++] = globalindices[2*nid+1];
  nid = elnidx[n++];	eldofidx[c++] = globalindices[2*nid];	  eldofidx[c++] = globalindices[2*nid+1];
  nid = elnidx[n++];	eldofidx[c++] = globalindices[2*nid];	  eldofidx[c++] = globalindices[2*nid+1];
	
	nid = elnidx[n++];	eldofidx[c++] = globalindices[2*nid];	  eldofidx[c++] = globalindices[2*nid+1];
  nid = elnidx[n++];	eldofidx[c++] = globalindices[2*nid];	  eldofidx[c++] = globalindices[2*nid+1];
  nid = elnidx[n++];	eldofidx[c++] = globalindices[2*nid];	  eldofidx[c++] = globalindices[2*nid+1];
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "GetElementEqnIndicesPressure"
PetscErrorCode GetElementEqnIndicesPressure(PetscInt npe,PetscInt elnidx[],PetscInt globalindices[],PetscInt eldofidx[])
{
	PetscInt nid,idx,n;
	
	PetscFunctionBegin;
	for (n=0; n<npe; n++) {
		nid = elnidx[n];
		eldofidx[n] = globalindices[nid];
	}
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDASetElementType_Q2"
PetscErrorCode  DMDASetElementType_Q2(DM da)
{
  DM_DA          *dd = (DM_DA*)da->data;
  PetscErrorCode ierr;
	
  PetscFunctionBegin;
  PetscValidHeaderSpecific(da,DM_CLASSID,1);
	//  PetscValidLogicalCollectiveEnum(da,etype,2);
  if (dd->elementtype) {
    ierr = PetscFree(dd->e);CHKERRQ(ierr);
		//    dd->elementtype = etype;
    dd->ne          = 0; 
    dd->e           = PETSC_NULL;
  }
  //da->ops->getelements = DMGetElements_DA_Q2;
	
  PetscFunctionReturn(0);
}
#undef __FUNCT__
#define __FUNCT__ "DMDASetElementType_P1"
PetscErrorCode  DMDASetElementType_P1(DM da)
{
  DM_DA          *dd = (DM_DA*)da->data;
  PetscErrorCode ierr;
	
  PetscFunctionBegin;
  PetscValidHeaderSpecific(da,DM_CLASSID,1);
	//  PetscValidLogicalCollectiveEnum(da,etype,2);
  if (dd->elementtype) {
    ierr = PetscFree(dd->e);CHKERRQ(ierr);
		//    dd->elementtype = etype;
    dd->ne          = 0; 
    dd->e           = PETSC_NULL;
  }
	//da->ops->getelements = DMGetElements_DA_P1;
	
  PetscFunctionReturn(0);
}

/* FILE: Stokes */
/*
 is_A12 PETSC_TRUE => Gradient u x p 
 is_A12 PETSC_FALSE => Divergence p x u 
 */
#undef __FUNCT__
#define __FUNCT__ "Assemble_Stokes_EmptyA12_A21_Q2"
PetscErrorCode Assemble_Stokes_EmptyA12_A21_Q2(DM dau,DM dap,PetscBool is_A12,Mat A)
{	
	PetscErrorCode ierr;
	PetscInt e,n;
	PetscScalar Ae[Q2_NODES_PER_EL_2D*2 * P_BASIS_FUNCTIONS];
	/* velocity */
	PetscInt u_nel,u_nen; /* num vel elements, nodes per ele */
	const PetscInt *u_elnidx; /* element -> node indices */
	PetscInt *u_gidx, u_elgidx[2*Q2_NODES_PER_EL_2D]; /* global indices and element counterpart (ghosted) */
	/* pressure */
	PetscInt p_nel,p_nen;
	const PetscInt *p_elnidx;
	PetscInt *p_gidx, p_elgidx[P_BASIS_FUNCTIONS];
	
	PetscFunctionBegin;
	/* setup eqnums */
	ierr = DMDAGetGlobalIndices(dau,0,&u_gidx);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dau,&u_nel,&u_nen,&u_elnidx);CHKERRQ(ierr);
	
	ierr = DMDAGetGlobalIndices(dap,0,&p_gidx);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&p_nel,&p_nen,&p_elnidx);CHKERRQ(ierr);
	
	/* initialise element stiffness matrix */
	PetscMemzero( Ae, sizeof(PetscScalar)* Q2_NODES_PER_EL_2D*2 * P_BASIS_FUNCTIONS );
	for (e=0;e<u_nel;e++) {
		/* insert element matrix into global matrix */
		ierr = GetElementEqnIndicesQ2((PetscInt*)&u_elnidx[u_nen*e],u_gidx,u_elgidx);CHKERRQ(ierr);
		ierr = GetElementEqnIndicesPressure(P_BASIS_FUNCTIONS,(PetscInt*)&p_elnidx[p_nen*e],p_gidx,p_elgidx);CHKERRQ(ierr);
		if (is_A12) {
			ierr = MatSetValues( A, Q2_NODES_PER_EL_2D*2,u_elgidx, P_BASIS_FUNCTIONS,p_elgidx, Ae, ADD_VALUES );CHKERRQ(ierr);
		} else {
			ierr = MatSetValues( A, P_BASIS_FUNCTIONS,p_elgidx, Q2_NODES_PER_EL_2D*2,u_elgidx, Ae, ADD_VALUES );CHKERRQ(ierr);
		}
		
	}
	/* no bc modifications */
	ierr = MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	PetscFunctionReturn(0);
}

/*
 If you want to use the off-diagonal matrices with MatGetLocalSubMatrix(), then you
 will have to add calls to MatSetLocalToGlobalMapping().
 */
#undef __FUNCT__  
#define __FUNCT__ "DMGetMatrix_DMCompositeStokesQkPm"
PetscErrorCode DMGetMatrix_DMCompositeStokesQkPm(DM pack,const MatType mtype,Mat *B)
{
	PetscBool      same;
	const MatType Aii_type;
  DM             dau,dap;
  DMDALocalInfo  infou,infop;
	Mat            A,Auu,Aup,Apu,bA[2][2];
	IS             *is;
	MPI_Comm       comm;
	PetscInt       mu,Mu,mp,Mp,i,j;
	PetscInt dim,col_da_dof,col_da_sw,row_da_dof,row_da_sw,nnz_d,nnz_od;
	Vec X,u,p;
	PetscErrorCode ierr;
	
	/* is composite */
	same = PETSC_FALSE;
	ierr = PetscTypeCompare((PetscObject)pack,DMCOMPOSITE,&same);CHKERRQ(ierr);
	if (!same) PetscFunctionReturn(0);
	/* is MATNEST */
	same = PETSC_FALSE;
	ierr = PetscStrcmp(mtype,MATNEST,&same);CHKERRQ(ierr);
	if (!same) PetscFunctionReturn(0);
	
  /* Fetch the DA's */
  ierr = DMCompositeGetEntries(pack,&dau,&dap);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dau,&infou);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dap,&infop);CHKERRQ(ierr);
	
	/* test verification functions */
/*
	{
		Vec X,Xu,Xp;
		PetscBool isvalid;
		
		ierr = DMGetGlobalVector(pack,&X);CHKERRQ(ierr);
		ierr = DMGetGlobalVector(dau,&Xu);CHKERRQ(ierr);
		ierr = DMGetGlobalVector(dap,&Xp);CHKERRQ(ierr);

		isvalid = PETSC_FALSE;
		ierr = DMVerifyGlobalVector(pack,X,&isvalid);CHKERRQ(ierr);
		if (isvalid) { printf("X goes with pack \n"); } else { printf("X DOES NOT go with pack \n"); }
		isvalid = PETSC_FALSE;
		ierr = DMVerifyGlobalVector(dau,X,&isvalid);CHKERRQ(ierr);
		if (isvalid) { printf("X goes with dau \n"); } else { printf("X DOES NOT go with dau \n"); }
		isvalid = PETSC_FALSE;
		ierr = DMVerifyGlobalVector(dap,X,&isvalid);CHKERRQ(ierr);
		if (isvalid) { printf("X goes with dap \n"); } else { printf("X DOES NOT go with dap \n"); }

		
		isvalid = PETSC_FALSE;
		ierr = DMVerifyGlobalVector(pack,Xu,&isvalid);CHKERRQ(ierr);
		if (isvalid) { printf("Xu goes with pack \n"); } else { printf("Xu DOES NOT go with pack \n"); }
		isvalid = PETSC_FALSE;
		ierr = DMVerifyGlobalVector(dau,Xu,&isvalid);CHKERRQ(ierr);
		if (isvalid) { printf("Xu goes with dau \n"); } else { printf("Xu DOES NOT go with dau \n"); }
		isvalid = PETSC_FALSE;
		ierr = DMVerifyGlobalVector(dap,Xu,&isvalid);CHKERRQ(ierr);
		if (isvalid) { printf("Xu goes with dap \n"); } else { printf("Xu DOES NOT go with dap \n"); }

		
		isvalid = PETSC_FALSE;
		ierr = DMVerifyGlobalVector(pack,Xp,&isvalid);CHKERRQ(ierr);
		if (isvalid) { printf("Xp goes with pack \n"); } else { printf("Xp DOES NOT go with pack \n"); }
		isvalid = PETSC_FALSE;
		ierr = DMVerifyGlobalVector(dau,Xp,&isvalid);CHKERRQ(ierr);
		if (isvalid) { printf("Xp goes with dau \n"); } else { printf("Xp DOES NOT go with dau \n"); }
		isvalid = PETSC_FALSE;
		ierr = DMVerifyGlobalVector(dap,Xp,&isvalid);CHKERRQ(ierr);
		if (isvalid) { printf("Xp goes with dap \n"); } else { printf("Xp DOES NOT go with dap \n"); }

		
		ierr = DMRestoreGlobalVector(dau,&Xp);CHKERRQ(ierr);
		ierr = DMRestoreGlobalVector(dap,&Xu);CHKERRQ(ierr);
		ierr = DMRestoreGlobalVector(pack,&X);CHKERRQ(ierr);
	}
*/
	 
	/* Create submatrices */
	comm = ((PetscObject)pack)->comm;
	ierr = DMGetMatrix(dau,MATAIJ,&Auu);CHKERRQ(ierr);  ierr = MatSetOptionsPrefix(Auu,"Auu");CHKERRQ(ierr);
	ierr = MatCreate(comm,&Aup);CHKERRQ(ierr);          ierr = MatSetOptionsPrefix(Aup,"Aup");CHKERRQ(ierr);
	ierr = MatCreate(comm,&Apu);CHKERRQ(ierr);          ierr = MatSetOptionsPrefix(Apu,"Apu");CHKERRQ(ierr);
	
	/* Sizes: DM manages the diagonal blocks */
	ierr = DMCreateGlobalVector(pack,&X);CHKERRQ(ierr);
	ierr = DMCompositeGetAccess(pack,X,&u,&p);CHKERRQ(ierr);
	ierr = VecGetSize(u,&Mu);CHKERRQ(ierr);
	ierr = VecGetLocalSize(u,&mu);CHKERRQ(ierr);
	ierr = VecGetSize(p,&Mp);CHKERRQ(ierr);
	ierr = VecGetLocalSize(p,&mp);CHKERRQ(ierr);
	ierr = DMCompositeRestoreAccess(pack,X,&u,&p);CHKERRQ(ierr);
	ierr = VecDestroy(&X);CHKERRQ(ierr);
	
	ierr = MatSetSizes(Aup,mu,mp,Mu,Mp);CHKERRQ(ierr);
	ierr = MatSetSizes(Apu,mp,mu,Mp,Mu);CHKERRQ(ierr);
	
	/* Types: */
	ierr = MatGetType(Auu,&Aii_type);CHKERRQ(ierr);
	ierr = MatSetType(Aup,Aii_type);CHKERRQ(ierr);
	ierr = MatSetType(Apu,Aii_type);CHKERRQ(ierr);
	
	
	/* Preallocation: DM manages the diagonal blocks */
	ierr = DMDAGetInfo(dap, &dim, 0,0,0, 0,0,0, &col_da_dof,&col_da_sw, 0,0,0, 0);CHKERRQ(ierr);
	nnz_d = (PetscInt)(pow( 2.0, (double)dim )) * col_da_dof;
	nnz_od = (PetscInt)(pow( 2.0, (double)(dim-1) )) * col_da_dof;
	/*PetscPrintf(PETSC_COMM_WORLD,"A12: nnz_d = %d \n", nnz_d );*/
	/*PetscPrintf(PETSC_COMM_WORLD,"A12: nnz_od = %d \n", nnz_od );*/
	ierr = MatSeqAIJSetPreallocation(Aup,nnz_d,PETSC_NULL);CHKERRQ(ierr);
	ierr = MatMPIAIJSetPreallocation(Aup,nnz_d,PETSC_NULL,nnz_od,PETSC_NULL);CHKERRQ(ierr);
	/* fill in non-zero structure */
	ierr = Assemble_Stokes_EmptyA12_A21_Q2(dau,dap,PETSC_TRUE,Aup);CHKERRQ(ierr);
	
	ierr = DMDAGetInfo(dau, 0, 0,0,0, 0,0,0, &col_da_dof,&col_da_sw, 0,0,0, 0);CHKERRQ(ierr);
	nnz_d = (PetscInt)(pow( col_da_sw+1, (double)dim )) * col_da_dof;
	nnz_od = (col_da_sw + 1) * col_da_dof;
	/*PetscPrintf(PETSC_COMM_WORLD,"A21: nnz_d = %d \n", nnz_d );*/
	/*PetscPrintf(PETSC_COMM_WORLD,"A21: nnz_od = %d \n", nnz_od );*/
	ierr = MatSeqAIJSetPreallocation(Apu,nnz_d,PETSC_NULL);CHKERRQ(ierr);
	ierr = MatMPIAIJSetPreallocation(Apu,nnz_d,PETSC_NULL,nnz_od,PETSC_NULL);CHKERRQ(ierr);
	/* fill in non-zero structure */
	ierr = Assemble_Stokes_EmptyA12_A21_Q2(dau,dap,PETSC_FALSE,Apu);CHKERRQ(ierr);
	
	/* We do this after we have filled in the non-zero structure */
	ierr = MatSetOption(Aup,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_TRUE);CHKERRQ(ierr);
	ierr = MatSetOption(Apu,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_TRUE);CHKERRQ(ierr);
	
	/* Create nest */
	ierr = DMCompositeGetGlobalISs(pack,&is);CHKERRQ(ierr);
	
	bA[0][0] = Auu; bA[0][1] = Aup;
	bA[1][0] = Apu; bA[1][1] = PETSC_NULL;
  ierr = MatCreateNest(PETSC_COMM_WORLD,2,is,2,is,&bA[0][0],&A);CHKERRQ(ierr);
	ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	
	*B = A;
	
	for (i=0; i<2; i++) {
		for (j=0; j<2; j++) {
			if (bA[i][j]) { ierr = MatDestroy(&bA[i][j]);CHKERRQ(ierr); }
		}
	}
	ierr = ISDestroy(&is[0]);CHKERRQ(ierr);
	ierr = ISDestroy(&is[1]);CHKERRQ(ierr);
	ierr = PetscFree(is);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}
#undef __FUNCT__  
#define __FUNCT__ "DMGetMatrix_DMCompositeStokesPCQkPm"
PetscErrorCode DMGetMatrix_DMCompositeStokesPCQkPm(DM pack,const MatType mtype,Mat *B)
{
	PetscBool      same;
	const MatType Aii_type;
  DM             dau,dap;
  DMDALocalInfo  infou,infop;
	Mat            A,Auu,Aup,Apu,App,bA[2][2];
	IS             *is;
	MPI_Comm       comm;
	PetscInt       mu,Mu,mp,Mp,i,j;
	PetscInt dim,col_da_dof,col_da_sw,row_da_dof,row_da_sw,nnz_d,nnz_od;
	Vec X,u,p;
	PetscBool upper,lower,full;
	PetscErrorCode ierr;
	
	/* is composite */
	same = PETSC_FALSE;
	ierr = PetscTypeCompare((PetscObject)pack,DMCOMPOSITE,&same);CHKERRQ(ierr);
	if (!same) PetscFunctionReturn(0);
	/* is MATNEST */
	same = PETSC_FALSE;
	ierr = PetscStrcmp(mtype,MATNEST,&same);CHKERRQ(ierr);
	if (!same) PetscFunctionReturn(0);
	
  /* Fetch the DA's */
  ierr = DMCompositeGetEntries(pack,&dau,&dap);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dau,&infou);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dap,&infop);CHKERRQ(ierr);
	
	/* Create submatrices */
	comm = ((PetscObject)pack)->comm;
	ierr = DMGetMatrix(dau,MATAIJ,&Auu);CHKERRQ(ierr);    ierr = MatSetOptionsPrefix(Auu,"Buu");CHKERRQ(ierr);
	ierr = MatCreate(comm,&Aup);CHKERRQ(ierr);            ierr = MatSetOptionsPrefix(Aup,"Bup");CHKERRQ(ierr);
	ierr = MatCreate(comm,&Apu);CHKERRQ(ierr);            ierr = MatSetOptionsPrefix(Apu,"Bpu");CHKERRQ(ierr);
	ierr = DMGetMatrix(dap,MATAIJ,&App);CHKERRQ(ierr);    ierr = MatSetOptionsPrefix(App,"S*");CHKERRQ(ierr);
	
	/* Sizes: DM manages the diagonal blocks */
	ierr = DMCreateGlobalVector(pack,&X);CHKERRQ(ierr);
	ierr = DMCompositeGetAccess(pack,X,&u,&p);CHKERRQ(ierr);
	ierr = VecGetSize(u,&Mu);CHKERRQ(ierr);
	ierr = VecGetLocalSize(u,&mu);CHKERRQ(ierr);
	ierr = VecGetSize(p,&Mp);CHKERRQ(ierr);
	ierr = VecGetLocalSize(p,&mp);CHKERRQ(ierr);
	ierr = DMCompositeRestoreAccess(pack,X,&u,&p);CHKERRQ(ierr);
	ierr = VecDestroy(&X);CHKERRQ(ierr);
	
	ierr = MatSetSizes(Aup,mu,mp,Mu,Mp);CHKERRQ(ierr);
	ierr = MatSetSizes(Apu,mp,mu,Mp,Mu);CHKERRQ(ierr);
	
	/* Types: */
	ierr = MatGetType(Auu,&Aii_type);CHKERRQ(ierr);
	ierr = MatSetType(Aup,Aii_type);CHKERRQ(ierr);
	ierr = MatSetType(Apu,Aii_type);CHKERRQ(ierr);
	
	/* Preallocation: DM manages the diagonal blocks */
	upper = lower = full = PETSC_FALSE;
	ierr = PetscOptionsGetBool(PETSC_NULL,"-dmcomposite_pc_upper",&upper,PETSC_NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetBool(PETSC_NULL,"-dmcomposite_pc_lower",&lower,PETSC_NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetBool(PETSC_NULL,"-dmcomposite_pc_full",&full,PETSC_NULL);CHKERRQ(ierr);
	if (full){ upper = PETSC_TRUE; lower = PETSC_TRUE; }
	
	if (upper) {
		ierr = DMDAGetInfo(dap, &dim, 0,0,0, 0,0,0, &col_da_dof,&col_da_sw, 0,0,0, 0);CHKERRQ(ierr);
		nnz_d = (PetscInt)(pow( 2.0, (double)dim )) * col_da_dof;
		nnz_od = (PetscInt)(pow( 2.0, (double)(dim-1) )) * col_da_dof;
		/*printf("A12: nnz_d = %d \n", nnz_d );*/
		/*printf("A12: nnz_od = %d \n", nnz_od );*/
		ierr = MatSeqAIJSetPreallocation(Aup,nnz_d,PETSC_NULL);CHKERRQ(ierr);
		ierr = MatMPIAIJSetPreallocation(Aup,nnz_d,PETSC_NULL,nnz_od,PETSC_NULL);CHKERRQ(ierr);
		/* fill in non-zero structure */
		ierr = Assemble_Stokes_EmptyA12_A21_Q2(dau,dap,PETSC_TRUE,Aup);CHKERRQ(ierr);
	}
	if (lower) { 
		ierr = DMDAGetInfo(dau, 0, 0,0,0, 0,0,0, &col_da_dof,&col_da_sw, 0,0,0, 0);CHKERRQ(ierr);
		nnz_d = (PetscInt)(pow( col_da_sw+1, (double)dim )) * col_da_dof;
		nnz_od = (col_da_sw + 1) * col_da_dof;
		/*printf("A21: nnz_d = %d \n", nnz_d );*/
		/*printf("A21: nnz_od = %d \n", nnz_od );*/
		ierr = MatSeqAIJSetPreallocation(Apu,nnz_d,PETSC_NULL);CHKERRQ(ierr);
		ierr = MatMPIAIJSetPreallocation(Apu,nnz_d,PETSC_NULL,nnz_od,PETSC_NULL);CHKERRQ(ierr);
		/* fill in non-zero structure */
		ierr = Assemble_Stokes_EmptyA12_A21_Q2(dau,dap,PETSC_FALSE,Apu);CHKERRQ(ierr);
	}
	
	/* Create nest */
	ierr = DMCompositeGetGlobalISs(pack,&is);CHKERRQ(ierr);
	
	bA[0][0] = Auu; bA[0][1] = Aup;
	bA[1][0] = Apu; bA[1][1] = App;
  ierr = MatCreateNest(PETSC_COMM_WORLD,2,is,2,is,&bA[0][0],&A);CHKERRQ(ierr);
	ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	
	*B = A;
	
	for (i=0; i<2; i++) {
		for (j=0; j<2; j++) {
			if (bA[i][j]) { ierr = MatDestroy(&bA[i][j]);CHKERRQ(ierr); }
		}
	}
	ierr = ISDestroy(&is[0]);CHKERRQ(ierr);
	ierr = ISDestroy(&is[1]);CHKERRQ(ierr);
	ierr = PetscFree(is);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "DMCompositeStokes_MatCreate"
PetscErrorCode DMCompositeStokes_MatCreate(DM pack,
																					 const MatType mtypeA11,Mat *A11,
																					 const MatType mtypeA12,Mat *A12,
																					 const MatType mtypeA21,Mat *A21,
																					 const MatType mtypeS,Mat *A22)
{
	PetscBool      same;
  DM             dau,dap;
	Mat            Auu,Aup,Apu,App;
	IS             *is;
	MPI_Comm       comm;
	PetscInt       mu,Mu,mp,Mp,i,j;
	PetscInt dim,col_da_dof,col_da_sw,row_da_dof,row_da_sw,nnz_d,nnz_od;
	Vec X,u,p;
	PetscErrorCode ierr;
	
	/* is composite */
	same = PETSC_FALSE;
	ierr = PetscTypeCompare((PetscObject)pack,DMCOMPOSITE,&same);CHKERRQ(ierr);
	if (!same) PetscFunctionReturn(0);
	
  /* Fetch the DA's */
  ierr = DMCompositeGetEntries(pack,&dau,&dap);CHKERRQ(ierr);
	
	/* Create submatrices */
	comm = ((PetscObject)pack)->comm;
	if (A11) {
		ierr = DMGetMatrix(dau,mtypeA11,&Auu);CHKERRQ(ierr);  ierr = MatSetOptionsPrefix(Auu,"Auu");CHKERRQ(ierr);
	}
	if (A12) {
		ierr = MatCreate(comm,&Aup);CHKERRQ(ierr);          ierr = MatSetOptionsPrefix(Aup,"Aup");CHKERRQ(ierr);
	}
	if (A21) {
		ierr = MatCreate(comm,&Apu);CHKERRQ(ierr);          ierr = MatSetOptionsPrefix(Apu,"Apu");CHKERRQ(ierr);
	}
	if (A22) {
		ierr = DMGetMatrix(dap,mtypeA11,&App);CHKERRQ(ierr);  ierr = MatSetOptionsPrefix(App,"S*");CHKERRQ(ierr);
	}
	
	/* Sizes: DM manages the diagonal blocks */
	ierr = DMCreateGlobalVector(pack,&X);CHKERRQ(ierr);
	ierr = DMCompositeGetAccess(pack,X,&u,&p);CHKERRQ(ierr);
	ierr = VecGetSize(u,&Mu);CHKERRQ(ierr);
	ierr = VecGetLocalSize(u,&mu);CHKERRQ(ierr);
	ierr = VecGetSize(p,&Mp);CHKERRQ(ierr);
	ierr = VecGetLocalSize(p,&mp);CHKERRQ(ierr);
	ierr = DMCompositeRestoreAccess(pack,X,&u,&p);CHKERRQ(ierr);
	ierr = VecDestroy(&X);CHKERRQ(ierr);
	
	if (A12) {
		ierr = MatSetSizes(Aup,mu,mp,Mu,Mp);CHKERRQ(ierr);
	}
	if (A21) {
		ierr = MatSetSizes(Apu,mp,mu,Mp,Mu);CHKERRQ(ierr);
	}
	
	/* Types: */
	if (A12) {
		ierr = MatSetType(Aup,mtypeA12);CHKERRQ(ierr);
	}
	if (A21) {
		ierr = MatSetType(Apu,mtypeA21);CHKERRQ(ierr);
	}
	
	
	/* Preallocation: DM manages the diagonal blocks */
	if (A12) {
		ierr = DMDAGetInfo(dap, &dim, 0,0,0, 0,0,0, &col_da_dof,&col_da_sw, 0,0,0, 0);CHKERRQ(ierr);
		nnz_d = (PetscInt)(pow( 2.0, (double)dim )) * col_da_dof;
		nnz_od = (PetscInt)(pow( 2.0, (double)(dim-1) )) * col_da_dof;
		/*PetscPrintf(PETSC_COMM_WORLD,"A12: nnz_d = %d \n", nnz_d );*/
		/*PetscPrintf(PETSC_COMM_WORLD,"A12: nnz_od = %d \n", nnz_od );*/
		ierr = MatSeqAIJSetPreallocation(Aup,nnz_d,PETSC_NULL);CHKERRQ(ierr);
		ierr = MatMPIAIJSetPreallocation(Aup,nnz_d,PETSC_NULL,nnz_od,PETSC_NULL);CHKERRQ(ierr);
		/* fill in non-zero structure */
		ierr = Assemble_Stokes_EmptyA12_A21_Q2(dau,dap,PETSC_TRUE,Aup);CHKERRQ(ierr);

		/* We do this after we have filled in the non-zero structure */
		ierr = MatSetOption(Aup,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_TRUE);CHKERRQ(ierr);
	}
	
	if (A21) {
		ierr = DMDAGetInfo(dau, &dim, 0,0,0, 0,0,0, &col_da_dof,&col_da_sw, 0,0,0, 0);CHKERRQ(ierr);
		nnz_d = (PetscInt)(pow( col_da_sw+1, (double)dim )) * col_da_dof;
		nnz_od = (col_da_sw + 1) * col_da_dof;
		/*PetscPrintf(PETSC_COMM_WORLD,"A21: nnz_d = %d \n", nnz_d );*/
		/*PetscPrintf(PETSC_COMM_WORLD,"A21: nnz_od = %d \n", nnz_od );*/
		ierr = MatSeqAIJSetPreallocation(Apu,nnz_d,PETSC_NULL);CHKERRQ(ierr);
		ierr = MatMPIAIJSetPreallocation(Apu,nnz_d,PETSC_NULL,nnz_od,PETSC_NULL);CHKERRQ(ierr);
		/* fill in non-zero structure */
		ierr = Assemble_Stokes_EmptyA12_A21_Q2(dau,dap,PETSC_FALSE,Apu);CHKERRQ(ierr);

		/* We do this after we have filled in the non-zero structure */
		ierr = MatSetOption(Apu,MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_TRUE);CHKERRQ(ierr);
	}
	

	if (A11) {
		*A11 = Auu;
	}
	if (A12) {
		*A12 = Aup;
	}
	if (A21) {
		*A21 = Apu;
	}
	if (A22) {
		*A22 = App;
	}
		
	PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "DMCompositeStokesCreateNest"
PetscErrorCode DMCompositeStokesCreateNest(DM pack,Mat Auu,Mat Aup,Mat Apu,Mat App,Mat *B)
{
	PetscErrorCode ierr;
	IS             *is;
	Mat            A,bA[2][2];
	
  PetscFunctionBegin;
	/* Create nest */
	ierr = DMCompositeGetGlobalISs(pack,&is);CHKERRQ(ierr);
	
	bA[0][0] = Auu; bA[0][1] = Aup;
	bA[1][0] = Apu; bA[1][1] = App;
  ierr = MatCreateNest(PETSC_COMM_WORLD,2,is,2,is,&bA[0][0],&A);CHKERRQ(ierr);
	ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	
	*B = A;
	
	ierr = ISDestroy(&is[0]);CHKERRQ(ierr);
	ierr = ISDestroy(&is[1]);CHKERRQ(ierr);
	ierr = PetscFree(is);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "DMDASetValuesLocalStencil_AddValues_DOF"
PetscErrorCode DMDASetValuesLocalStencil_AddValues_DOF(PetscScalar *fields_F,PetscInt ndof,PetscInt eqn[],PetscScalar Fe[])
{
  PetscInt n,d,el_idx,idx;
	
  PetscFunctionBegin;
	for (d=0; d<ndof; d++) {
		for (n = 0; n<U_BASIS_FUNCTIONS; n++) {
			el_idx = ndof*n + d;
			idx    = eqn[el_idx];
			fields_F[idx] += Fe[el_idx];
		}
	}
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDASetValuesLocalStencil_AddValues_Stokes_Velocity"
PetscErrorCode DMDASetValuesLocalStencil_AddValues_Stokes_Velocity(PetscScalar *fields_F,PetscInt u_eqn[],PetscScalar Fe_u[])
{
  PetscInt n,idx;
	
  PetscFunctionBegin;
  for (n = 0; n<U_BASIS_FUNCTIONS; n++) {
		idx = u_eqn[2*n  ];
    fields_F[idx] += Fe_u[NSD*n  ];
		
		idx = u_eqn[2*n+1];
    fields_F[idx] += Fe_u[NSD*n+1];
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDASetValuesLocalStencil_AddValues_Stokes_Pressure"
PetscErrorCode DMDASetValuesLocalStencil_AddValues_Stokes_Pressure(PetscScalar *fields_F,PetscInt p_eqn[],PetscScalar Fe_p[])
{
  PetscInt n,idx;
	
  PetscFunctionBegin;
  for (n = 0; n<P_BASIS_FUNCTIONS; n++) {
		idx = p_eqn[n];
    fields_F[idx] += Fe_p[n];
  }
  PetscFunctionReturn(0);
}

/* matrix free stuff */
/*
 if (isbc_local[i] == dirch) y[i] = xbc[i]
 else                        y[i] = y[i] 
 */
#undef __FUNCT__
#define __FUNCT__ "BCListInsert"
PetscErrorCode BCListInsert(BCList list,Vec y)
{
	PetscInt m,k,L;
	PetscInt *idx;
	PetscScalar *LA_x,*LA_y;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	ierr = BCListGetGlobalIndices(list,&L,&idx);CHKERRQ(ierr);
	ierr = BCListGetGlobalValues(list,&L,&LA_x);CHKERRQ(ierr);
	ierr = VecGetArray(y,&LA_y);CHKERRQ(ierr);
	ierr = VecGetLocalSize(y,&m);CHKERRQ(ierr);
	for (k=0; k<m; k++) {
		if (idx[k]==BCList_DIRICHLET) {
			LA_y[k] = LA_x[k];
		}
	}
	ierr = VecRestoreArray(y,&LA_y);CHKERRQ(ierr);
	ierr = BCListRestoreGlobalIndices(list,&L,&idx);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

/*
 if (isbc_local[i] == dirch) y[i] = xbc[i]
 else                        y[i] = y[i] 
 */
#undef __FUNCT__
#define __FUNCT__ "BCListInsertLocal"
PetscErrorCode BCListInsertLocal(BCList list,Vec y)
{
	PetscInt M,k,L;
	const PetscInt *idx;
	PetscScalar *LA_x,*LA_y;
	PetscBool is_seq = PETSC_FALSE;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	L    = list->L_local;
	idx  = list->dofidx_local;
	LA_x = list->vals_local;
	ierr = VecGetArray(y,&LA_y);CHKERRQ(ierr);
	ierr = VecGetSize(y,&M);CHKERRQ(ierr);
	
	/* debug error checking */
	if (L!=M) { SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_SIZ,"Sizes do not match"); };
	ierr = PetscTypeCompare((PetscObject)y,VECSEQ,&is_seq);CHKERRQ(ierr);
	if (!is_seq) { SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONG,"Vec must be VECSEQ, i.e. a local (ghosted) vec"); };
	
	for (k=0; k<M; k++) {
		if (idx[k]==BCList_DIRICHLET) {
			LA_y[k] = LA_x[k];
		}
	}
	ierr = VecRestoreArray(y,&LA_y);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

/*
 Apply's
 F = scale(X-phi) where ever a dirichlet is encountered.
 */
#undef __FUNCT__
#define __FUNCT__ "BCListResidualDirichlet"
PetscErrorCode BCListResidualDirichlet(BCList list,Vec X,Vec F)
{
	PetscInt m,k,L;
	const PetscInt *idx;
	PetscScalar *LA_S,*LA_X,*LA_F,*LA_phi;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	if (!X) { SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_NULL,"Vec X cannot be PETSC_NULL"); }
	if (!F) { SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_NULL,"Vec F cannot be PETSC_NULL"); }
	
	L      = list->L;
	idx    = list->dofidx_global;
	LA_phi = list->vals_global;
	LA_S   = list->scale_global;
	
	ierr = VecGetArray(X,&LA_X);CHKERRQ(ierr);
	ierr = VecGetArray(F,&LA_F);CHKERRQ(ierr);
	
	/* debug error checking */
	ierr = VecGetLocalSize(X,&m);CHKERRQ(ierr);
	if (L!=m) { SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_SIZ,"Sizes do not match (X)"); };
	ierr = VecGetLocalSize(F,&m);CHKERRQ(ierr);
	if (L!=m) { SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_SIZ,"Sizes do not match (F)"); };
	
	for (k=0; k<m; k++) {
		if (idx[k]==BCList_DIRICHLET) {
			LA_F[k] = LA_S[k]*(LA_X[k] - LA_phi[k]);
		}
	}
	ierr = VecRestoreArray(X,&LA_X);CHKERRQ(ierr);
	ierr = VecRestoreArray(F,&LA_F);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormFunctionLocal_U"
PetscErrorCode FormFunctionLocal_U(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[],PetscScalar Ru[])
{	
	PetscErrorCode ierr;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
	PetscScalar GNIu[MAX_QUAD_PNTS][2][Q2_NODES_PER_EL_2D],NIp[MAX_QUAD_PNTS][P_BASIS_FUNCTIONS];
	PetscScalar NIu[MAX_QUAD_PNTS][U_BASIS_FUNCTIONS];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen_u,nen_p,e,n,k;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D],el_eta[MAX_QUAD_PNTS];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	PetscScalar Fe[Q2_NODES_PER_EL_2D*2],Be[Q2_NODES_PER_EL_2D*2];
	PetscInt vel_el_lidx[U_BASIS_FUNCTIONS*2];
	PetscInt *gidx,elgidx[2*Q2_NODES_PER_EL_2D];
	PetscInt nbcs,i,j;
	const PetscInt *ubcidx;
	Vec diag;
	PetscScalar *LA_diag;
	PetscLogDouble t0,t1;
	BCList u_bclist = user->u_bclist;
	GaussPointCoefficientsStokes *gausspoints;
	PetscFunctionBegin;
	
	/* quadrature */
	/*QuadratureCreateGauss_3pnt_2D(&ngp,gp_xi,gp_weight);*/
	ngp       = user->Q->ngp;
	gp_xi     = user->Q->xi;
	gp_weight = user->Q->weight;
	for (k=0; k<ngp; k++) {
		PetscScalar *xip = &gp_xi[2*k];
		
		ConstructNI_Q2_2D(xip,NIu[k]);
		ConstructGNI_Q2_2D(xip,GNIu[k]);
	}
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetGlobalIndices(dau,0,&gidx);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
	PetscGetTime(&t0);
	for (e=0;e<nel;e++) {
		PetscScalar int_P, int_divu;
		
		ierr = StokesVelocity_GetElementLocalIndices(vel_el_lidx,(PetscInt*)&elnidx_u[nen_u*e]);CHKERRQ(ierr);
		//		PetscErrorCode StokesPressure_GetElementLocalIndices(PetscInt el_localIndices[],PetscInt elnid[])
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],u);CHKERRQ(ierr);
		ierr = DMDAGetScalarElementField_2D(elp,nen_p,(PetscInt*)&elnidx_p[nen_p*e],p);CHKERRQ(ierr);
		
		for( i=0; i<Q2_NODES_PER_EL_2D; i++ ) {
			ux[i] = elu[2*i  ];
			uy[i] = elu[2*i+1];
			
			xc[i] = elcoords[2*i  ];
			yc[i] = elcoords[2*i+1];
		}
		
		for (n=0; n<ngp; n++) {
			PetscScalar *xip = &gp_xi[2*n];
			ConstructNi_pressure(xip,elcoords,NIp[n]);
		}
		
		/* insert element matrix into global matrix */
		//		ierr = GetElementEqnIndicesQ2((PetscInt*)&elnidx_u[nen_u*e],gidx,elgidx);CHKERRQ(ierr);
		
		/* initialise element stiffness matrix */
		PetscMemzero( Fe, sizeof(PetscScalar)* Q2_NODES_PER_EL_2D*2 );
		PetscMemzero( Be, sizeof(PetscScalar)* Q2_NODES_PER_EL_2D*2 );
		
		/* evaluate the viscosity */
		for (n=0; n<ngp; n++) {
			el_eta[n] = gausspoints[n].eta;
		}
		
		int_P = int_divu = 0.0;
		for (n=0; n<ngp; n++) {
			PetscScalar  J[2][2], iJ[2][2];
			PetscScalar  exx,eyy,exy;
			PetscScalar  sxx,syy,sxy,pressure_gp;
			PetscScalar fac,d1,d2,J_p,ojp;
			
			/* coord transformation */
			for( i=0; i<2; i++ ) {
				for( j=0; j<2; j++ ) { J[i][j] = 0.0; }
			}
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				J[0][0] += GNIu[n][0][k] * xc[k] ;
				J[0][1] += GNIu[n][0][k] * yc[k] ;
				
				J[1][0] += GNIu[n][1][k] * xc[k] ;
				J[1][1] += GNIu[n][1][k] * yc[k] ;
			}
			J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
			ojp = 1.0/J_p;
			iJ[0][0] =  J[1][1]*ojp;
			iJ[0][1] = -J[0][1]*ojp;
			iJ[1][0] = -J[1][0]*ojp;
			iJ[1][1] =  J[0][0]*ojp;
			
			/* global derivs */
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				nx[k] = iJ[0][0]*GNIu[n][0][k] + iJ[0][1]*GNIu[n][1][k];
				ny[k] = iJ[1][0]*GNIu[n][0][k] + iJ[1][1]*GNIu[n][1][k];
			}
			
			
			fac = gp_weight[n] * J_p;
			
			/* pressure */
			pressure_gp = 0.0;
			for( i=0; i<P_BASIS_FUNCTIONS; i++ ) {
				pressure_gp += NIp[n][i] * elp[i];
			}
			pressure_gp = pressure_gp * fac;
			
			/* strain rate, B u */
			exx = eyy = exy = 0.0;
			for( i=0; i<Q2_NODES_PER_EL_2D; i++ ) {
				exx += nx[i] * ux[i];
				eyy += ny[i] * uy[i];
				exy += ny[i] * ux[i] + nx[i] * uy[i];
			}
			int_P    += fac * pressure_gp;
			int_divu += fac * (exx + eyy);
			
			/* constituive */
			d1 = 2.0 * el_eta[n] * fac;
			d2 =       el_eta[n] * fac;
			
			/* stress */
			sxx = d1 * exx+gausspoints[n].stress_old[0]*fac;
			syy = d1 * eyy+gausspoints[n].stress_old[1]*fac;
			sxy = d2 * exy+gausspoints[n].stress_old[2]*fac;
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) { 
				Fe[2*k]   += nx[k]*(sxx-pressure_gp)   + ny[k]*sxy; 
				Fe[2*k+1] += ny[k]*(syy-pressure_gp)   + nx[k]*sxy; 
			}
			/*
			 for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) { 
			 Fe[2*k]   -= nx[k]*pressure_gp; 
			 Fe[2*k+1] -= ny[k]*pressure_gp; 
			 }
			 */
			
			/* compute any body force terms here */
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) { 
				Be[2*k  ] = Be[2*k  ] + fac * NIu[n][k] * gausspoints[n].Fu[0];
				Be[2*k+1] = Be[2*k+1] + fac * NIu[n][k] * gausspoints[n].Fu[1];
			}
		}
#if 0
		{
			/* 
			 P +ve in compression
			 eta_bulk = (2 eta div(u) - P)/div(u) => bulk viscosity
			 lambda = eta_bulk - 2 eta / 3
			 */
			PetscScalar eta_gp = 1.0;
			PetscScalar lambda,eta_bulk;
			eta_bulk = (2.0 * eta_gp * int_divu - int_P) / int_divu;
			lambda = eta_bulk - (2.0/3.0) * eta_gp;
			if (eta_bulk<0.0) printf("******* etaB -ve *******");
			if (lambda<0.0) printf("******* lambda -ve *******");
			printf("eta = %1.4e, p = %1.4e, div(u) = %1.4e, eta_bulk = %1.4e, lambda = %1.4e \n", eta_gp,int_P,int_divu,eta_bulk,lambda );
		}
#endif
		
		/* combine body force with A.x */
		for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) { 
			Fe[2*k  ] = Fe[2*k  ] - Be[2*k  ];
			Fe[2*k+1] = Fe[2*k+1] - Be[2*k+1];
		}

#if 0
		for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) { 
			if (fabs(Fe[2*k]) > 1.0e10 ) {
				printf("e=%d \n", e );
				for (n=0; n<ngp; n++) {
					printf("[%d] gausspoints[n].eta = %1.4e \n", n,gausspoints[n].eta );
					printf("[%d] gausspoints[n].Fu[0] = %1.4e \n", n,gausspoints[n].Fu[0] );
					printf("[%d] gausspoints[n].Fu[1] = %1.4e \n", n,gausspoints[n].Fu[1] );
				}
			}
			if (fabs(Fe[2*k+1]) > 1.0e10 ) {
				printf("e=%d \n", e );
				for (n=0; n<ngp; n++) {
					printf("[%d] gausspoints[n].eta = %1.4e \n", n,gausspoints[n].eta );
					printf("[%d] gausspoints[n].Fu[0] = %1.4e \n", n,gausspoints[n].Fu[0] );
					printf("[%d] gausspoints[n].Fu[1] = %1.4e \n", n,gausspoints[n].Fu[1] );
				}
			}
		}
#endif
		
		ierr = DMDASetValuesLocalStencil_AddValues_Stokes_Velocity(Ru, vel_el_lidx,Fe);CHKERRQ(ierr);
	}
	
	PetscGetTime(&t1);
	//PetscPrintf(PETSC_COMM_WORLD,"Assemble Ru, = %1.4e (sec)\n",t1-t0);
	
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormFunctionLocal_U_tractionBC"
PetscErrorCode FormFunctionLocal_U_tractionBC(pTatinCtx user,DM dau,PetscScalar velocity[],DM dap,PetscScalar pressure[],PetscScalar Ru[])
{	
	PetscErrorCode ierr;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
	PetscScalar GNIu[MAX_QUAD_PNTS][2][Q2_NODES_PER_EL_2D],NIp[MAX_QUAD_PNTS][P_BASIS_FUNCTIONS];
	PetscScalar NIu[MAX_QUAD_PNTS][U_BASIS_FUNCTIONS];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen_u,nen_p,k;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	PetscScalar elu_edge[Q2_NODES_PER_EL_1D*2];
	PetscScalar Fe[Q2_NODES_PER_EL_2D*2],Be[Q2_NODES_PER_EL_1D*2];
	PetscInt vel_el_lidx[U_BASIS_FUNCTIONS*2];
	PetscInt *gidx,elgidx[2*Q2_NODES_PER_EL_2D];
	PetscInt nbcs,i,j;
	const PetscInt *ubcidx;
	PetscLogDouble t0,t1;

	PetscInt edge,nfaces,fe,e,p;
	SurfaceQPointCoefficientsStokes *qpoint;
	PetscScalar surfJ;
	ConformingElementFamily element;
	SurfaceQuadratureStokes sQ;
	double NIu_surf[3][Q2_NODES_PER_EL_1D]; /* ngp x nodes_1d */
	QPoint1d *gp1;
																					 
	PetscFunctionBegin;
	
	/* quadrature */
	ngp       = user->surfQ[0]->ngp;
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetGlobalIndices(dau,0,&gidx);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
	PetscGetTime(&t0);
	
	for (edge=0; edge<QUAD_EDGES; edge++) {
		int *face_local_indices;

		element = user->surfQ[edge]->e;
		nfaces  = user->surfQ[edge]->nfaces;
		sQ      = user->surfQ[edge];
		gp1     = user->surfQ[edge]->gp1;
		
		/* evaluate the quadrature points using the 1D basis for this edge */
		for (p=0; p<ngp; p++) {
			element->basis_NI_1D(&gp1[p],NIu_surf[p]);
		}
		
		face_local_indices = element->edge_node_list[edge];
		//PetscPrintf(PETSC_COMM_WORLD,"Traction: Face[%d] %d %d %d \n", edge,face_local_indices[0],face_local_indices[1],face_local_indices[2]);
	
		
		for (fe=0; fe<nfaces; fe++) { /* for all elements on this domain face */
			/* get element index of the face element we want to integrate */
			e = sQ->cell_list[fe];
			
			ierr = StokesVelocity_GetElementLocalIndices(vel_el_lidx,(PetscInt*)&elnidx_u[nen_u*e]);CHKERRQ(ierr);
			
			ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
			ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],velocity);CHKERRQ(ierr);
			ierr = DMDAGetScalarElementField_2D(elp,nen_p,(PetscInt*)&elnidx_p[nen_p*e],pressure);CHKERRQ(ierr);

			ierr = SurfaceQuadratureStokesGetCell(sQ,fe,&qpoint);CHKERRQ(ierr);
			/* from the velocities defined over the entire element, extract those on just this edge */ 
			element->extract_edge_field(element,sQ->edge_id,2, elu, elu_edge);
			/*
			printf("elu_vol_x[] = [ {0,1,2}[%+1.4lf , %+1.4lf , %+1.4lf] , {3,4,5}[%+1.4lf , %+1.4lf , %+1.4lf] , {6,7,8}[%+1.4lf , %+1.4lf , %+1.4lf] ] \n",
						 elu[2*0], elu[2*1],elu[2*2],elu[2*3],elu[2*4],elu[2*5],elu[2*6],elu[2*7],elu[2*8] );
			printf("elu_vol_y[] = [ {0,1,2}[%+1.4lf , %+1.4lf , %+1.4lf] , {3,4,5}[%+1.4lf , %+1.4lf , %+1.4lf] , {6,7,8}[%+1.4lf , %+1.4lf , %+1.4lf] ] \n",
						 elu[2*0+1], elu[2*1+1],elu[2*2+1],elu[2*3+1],elu[2*4+1],elu[2*5+1],elu[2*6+1],elu[2*7+1],elu[2*8+1] );

			printf("elu_edge_x[] =  {0,1,2}[%+1.4lf , %+1.4lf , %+1.4lf]  \n",
						 elu_edge[2*0], elu_edge[2*1],elu_edge[2*2] );
			printf("elu_edge_y[] =  {0,1,2}[%+1.4lf , %+1.4lf , %+1.4lf]  \n",
						 elu_edge[2*0+1], elu_edge[2*1+1],elu_edge[2*2+1] );
			*/
			
			/* initialise element stiffness matrix */
			PetscMemzero( Fe, sizeof(PetscScalar)* Q2_NODES_PER_EL_2D*2 );
			PetscMemzero( Be, sizeof(PetscScalar)* Q2_NODES_PER_EL_1D*2 );
			
			for (p=0; p<ngp; p++) {
				double normal[2],tangent[2];
				
				element->compute_surface_geometry_2D(	
																						 element, 
																						 elcoords,    // should contain 9 points with dimension 2 (x,y) // 
																						 sQ->edge_id,	 // edge index 0,1,2,3 //
																						 &gp1[p], // should contain 1 point with dimension 1 (xi)   //
																						 normal,tangent, &surfJ ); // n0[],t0 contains 1 point with dimension 2 (x,y) //
				// ctx->surfQ[k]->gp1[gp].w * F_dot_n * surfJ
				//printf("edge[%d]: face_idx[%d]: cell_idx[%d]: gp_idx[%d]: surfJ = %lf \n", edge,fe,e,p,surfJ );
				
				/* compute any body force terms here */
				for( k=0; k<Q2_NODES_PER_EL_1D; k++ ) { 
					Be[2*k  ] = Be[2*k  ] - gp1[p].w * NIu_surf[p][k] * qpoint[p].traction[0] * surfJ;
					Be[2*k+1] = Be[2*k+1] - gp1[p].w * NIu_surf[p][k] * qpoint[p].traction[1] * surfJ;
				}
			}
			
			/* combine body force with A.x */
			for( k=0; k<Q2_NODES_PER_EL_1D; k++ ) { 
				int nidx2d;
				
				/* map 1D index over element edge to 2D element space */
				nidx2d = face_local_indices[k];
				Fe[2*nidx2d  ] = Be[2*k  ];
				Fe[2*nidx2d+1] = Be[2*k+1];
			}
			
			
			ierr = DMDASetValuesLocalStencil_AddValues_Stokes_Velocity(Ru, vel_el_lidx,Fe);CHKERRQ(ierr);

		} /* end on face list */
	}	/* end on domain boundaries */
	PetscGetTime(&t1);
	//PetscPrintf(PETSC_COMM_WORLD,"Assemble Ru_traction, = %1.4e (sec)\n",t1-t0);
	
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormFunctionLocal_P"
PetscErrorCode FormFunctionLocal_P(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[],PetscScalar Rp[])
{	
	PetscErrorCode ierr;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
	PetscScalar GNI[MAX_QUAD_PNTS][2][Q2_NODES_PER_EL_2D],NIp[MAX_QUAD_PNTS][P_BASIS_FUNCTIONS];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen_u,nen_p,e,n,k;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	PetscScalar Fe[P_BASIS_FUNCTIONS];
	PetscScalar Be[P_BASIS_FUNCTIONS];
	PetscInt p_el_lidx[P_BASIS_FUNCTIONS];
	PetscInt *gidx,elgidx[2*Q2_NODES_PER_EL_2D];
	PetscInt *gidx_p,elgidx_p[P_BASIS_FUNCTIONS];
	PetscInt nbcs,i,j;
	const PetscInt *ubcidx;
	Vec diag;
	PetscScalar *LA_diag;
	PetscLogDouble t0,t1;
	BCList u_bclist = user->u_bclist;
	GaussPointCoefficientsStokes *gausspoints;
	
	PetscFunctionBegin;
	/* quadrature */
	ngp       = user->Q->ngp;
	gp_xi     = user->Q->xi;
	gp_weight = user->Q->weight;
	for (k=0; k<ngp; k++) {
		PetscScalar *xip = &gp_xi[2*k];
		ConstructGNI_Q2_2D(xip,GNI[k]);
	}
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetGlobalIndices(dau,0,&gidx);CHKERRQ(ierr);
	ierr = DMDAGetGlobalIndices(dap,0,&gidx_p);CHKERRQ(ierr);
	/* zero entries */
	//	ierr = VecZeroEntries(Rp);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
	PetscGetTime(&t0);
	for (e=0;e<nel;e++) {
		ierr = StokesPressure_GetElementLocalIndices(p_el_lidx,(PetscInt*)&elnidx_p[nen_p*e]);CHKERRQ(ierr);
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],u);CHKERRQ(ierr);
		ierr = DMDAGetScalarElementField_2D(elp,nen_p,(PetscInt*)&elnidx_p[nen_p*e],p);CHKERRQ(ierr);
		
		for( i=0; i<Q2_NODES_PER_EL_2D; i++ ) {
			ux[i] = elu[2*i  ];
			uy[i] = elu[2*i+1];
			
			xc[i] = elcoords[2*i  ];
			yc[i] = elcoords[2*i+1];
		}
		
		for (n=0; n<ngp; n++) {
			PetscScalar *xip = &gp_xi[2*n];
			ConstructNi_pressure(xip,elcoords,NIp[n]);
		}
		
		/* insert element matrix into global matrix */
		//		ierr = GetElementEqnIndicesQ2((PetscInt*)&elnidx_u[nen_u*e],gidx,elgidx);CHKERRQ(ierr);
		//		ierr = GetElementEqnIndicesPressure(nen_p,(PetscInt*)&elnidx_p[nen_p*e],gidx_p,elgidx_p);CHKERRQ(ierr);
		
		/* initialise element stiffness matrix */
		PetscMemzero( Fe, sizeof(PetscScalar)* P_BASIS_FUNCTIONS );
		PetscMemzero( Be, sizeof(PetscScalar)* P_BASIS_FUNCTIONS );
		
		for (n=0; n<ngp; n++) {
			PetscScalar  J[2][2], iJ[2][2];
			PetscScalar  div_u_gp;
			PetscScalar fac,J_p,ojp;
			
			/* coord transformation */
			for( i=0; i<2; i++ ) {
				for( j=0; j<2; j++ ) { J[i][j] = 0.0; }
			}
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				J[0][0] += GNI[n][0][k] * xc[k] ;
				J[0][1] += GNI[n][0][k] * yc[k] ;
				
				J[1][0] += GNI[n][1][k] * xc[k] ;
				J[1][1] += GNI[n][1][k] * yc[k] ;
			}
			J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
			ojp = 1.0/J_p;
			iJ[0][0] =  J[1][1]*ojp;
			iJ[0][1] = -J[0][1]*ojp;
			iJ[1][0] = -J[1][0]*ojp;
			iJ[1][1] =  J[0][0]*ojp;
			
			/* global derivs */
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				nx[k] = iJ[0][0]*GNI[n][0][k] + iJ[0][1]*GNI[n][1][k];
				ny[k] = iJ[1][0]*GNI[n][0][k] + iJ[1][1]*GNI[n][1][k];
			}
			fac = gp_weight[n] * J_p;
			
			/* div(u) */
			div_u_gp = 0.0;
			for( i=0; i<U_BASIS_FUNCTIONS; i++ ) {
				div_u_gp += ( nx[i] * ux[i] + ny[i] * uy[i] );
			}
			div_u_gp = -div_u_gp * fac; /* note the -ve sign here */
			
			for( k=0; k<P_BASIS_FUNCTIONS; k++ ) { 
				Fe[k] += NIp[n][k] * div_u_gp; 
			}
			
			/* compute any body force terms here */
			for( k=0; k<P_BASIS_FUNCTIONS; k++ ) { 
				Be[k] = Be[k] + fac * NIp[n][k] * gausspoints[n].Fp;
			}
			
		}
		
		/* combine body force with A.x */
		for( k=0; k<P_BASIS_FUNCTIONS; k++ ) { 
			Fe[k] = Fe[k] - Be[k];
		}
		
		ierr = DMDASetValuesLocalStencil_AddValues_Stokes_Pressure(Rp, p_el_lidx,Fe);CHKERRQ(ierr);
	}
	PetscGetTime(&t1);
//	PetscPrintf(PETSC_COMM_WORLD,"Assemble Rp, = %1.4e (sec)\n",t1-t0);
	
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormScalingLocal_U_etaMassMatrixDiagonal"
PetscErrorCode FormScalingLocal_U_etaMassMatrixDiagonal(pTatinCtx user,DM dau,PetscScalar Ru[])
{	
	PetscErrorCode ierr;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
	PetscScalar GNIu[MAX_QUAD_PNTS][2][Q2_NODES_PER_EL_2D];
	PetscScalar NIu[MAX_QUAD_PNTS][U_BASIS_FUNCTIONS];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen_u,e,n,k;
	const PetscInt *elnidx_u;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D],el_eta[MAX_QUAD_PNTS];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar Fe[Q2_NODES_PER_EL_2D*2];
	PetscInt vel_el_lidx[U_BASIS_FUNCTIONS*2];
	PetscInt *gidx;
	PetscInt i,j;
	PetscLogDouble t0,t1;
	GaussPointCoefficientsStokes *gausspoints;
	PetscScalar volume,avg_eta;
	PetscFunctionBegin;
	
	/* quadrature */
	/*QuadratureCreateGauss_3pnt_2D(&ngp,gp_xi,gp_weight);*/
	ngp       = user->Q->ngp;
	gp_xi     = user->Q->xi;
	gp_weight = user->Q->weight;
	for (k=0; k<ngp; k++) {
		PetscScalar *xip = &gp_xi[2*k];
		
		ConstructNI_Q2_2D(xip,NIu[k]);
		ConstructGNI_Q2_2D(xip,GNIu[k]);
	}
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetGlobalIndices(dau,0,&gidx);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);

	PetscGetTime(&t0);
	for (e=0;e<nel;e++) {
		ierr = StokesVelocity_GetElementLocalIndices(vel_el_lidx,(PetscInt*)&elnidx_u[nen_u*e]);CHKERRQ(ierr);

		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		
		for( i=0; i<Q2_NODES_PER_EL_2D; i++ ) {
			xc[i] = elcoords[2*i  ];
			yc[i] = elcoords[2*i+1];
		}
		
		/* initialise element stiffness matrix */
		PetscMemzero( Fe, sizeof(PetscScalar)* Q2_NODES_PER_EL_2D*2 );
		
		/* evaluate the viscosity */
		for (n=0; n<ngp; n++) {
			el_eta[n] = gausspoints[n].eta;
		}
		
		volume = 0.0;
		avg_eta = 0.0;
		for (n=0; n<ngp; n++) {
			PetscScalar  J[2][2], iJ[2][2];
			PetscScalar fac,J_p,ojp;
			
			/* coord transformation */
			for( i=0; i<2; i++ ) {
				for( j=0; j<2; j++ ) { J[i][j] = 0.0; }
			}
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				J[0][0] += GNIu[n][0][k] * xc[k] ;
				J[0][1] += GNIu[n][0][k] * yc[k] ;
				
				J[1][0] += GNIu[n][1][k] * xc[k] ;
				J[1][1] += GNIu[n][1][k] * yc[k] ;
			}
			J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
			fac = gp_weight[n] * J_p;

			/* integrate volume and eta */
			volume  = volume + 1.0 * fac;
			avg_eta = avg_eta + el_eta[n] * fac;
			/* compute N_i.N_j for each u,v dof */
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) { 
				Fe[2*k]   += NIu[n][k] * NIu[n][k] * fac; 
				Fe[2*k+1] += NIu[n][k] * NIu[n][k] * fac; 
			}
		}

		/* eta */
		avg_eta = avg_eta / volume;
		avg_eta = 1.0;
		/* scale I by the averga viscosity */
		for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) { 
			Fe[2*k  ] = avg_eta * Fe[2*k  ] ;
			Fe[2*k+1] = avg_eta * Fe[2*k+1] ;
		}
		
		ierr = DMDASetValuesLocalStencil_AddValues_Stokes_Velocity(Ru, vel_el_lidx,Fe);CHKERRQ(ierr);
	}
	
	PetscGetTime(&t1);
	PetscPrintf(PETSC_COMM_WORLD,"Assemble Scaling Ru, = %1.4e (sec)\n",t1-t0);
	
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "FormScaling_U_etaMassMatrixDiagonal"
PetscErrorCode FormScaling_U_etaMassMatrixDiagonal(pTatinCtx user,DM dau,BCList bclist)
{
	Vec scaleU,scaleU_loc;
	PetscScalar *LA_scaleUloc,*LA_scaleU;
	PetscInt i,n;
	PetscErrorCode ierr;
	
	
	ierr = DMGetLocalVector(dau,&scaleU_loc);CHKERRQ(ierr);
	ierr = VecZeroEntries(scaleU_loc);CHKERRQ(ierr);
	
	ierr = VecGetArray(scaleU_loc,&LA_scaleUloc);CHKERRQ(ierr);
	ierr = FormScalingLocal_U_etaMassMatrixDiagonal(user,dau,LA_scaleUloc);CHKERRQ(ierr);
	ierr = VecRestoreArray(scaleU_loc,&LA_scaleUloc);CHKERRQ(ierr);

	ierr = DMGetGlobalVector(dau,&scaleU);CHKERRQ(ierr);

	ierr = DMLocalToGlobalBegin(dau,scaleU_loc,ADD_VALUES,scaleU);CHKERRQ(ierr);
	ierr = DMLocalToGlobalEnd(dau,scaleU_loc,ADD_VALUES,scaleU);CHKERRQ(ierr);
	ierr = VecView(scaleU,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
	
	ierr = VecGetLocalSize(scaleU,&n);CHKERRQ(ierr);
	if (n != bclist->L) {
		SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Inconsistent size of scaling vector detected");
	}
	ierr = VecGetArray(scaleU,&LA_scaleU);CHKERRQ(ierr);
	for (i=0; i<n; i++) {
//		bclist->scale_global[i] = LA_scaleU[i];
	}
	ierr = VecRestoreArray(scaleU,&LA_scaleU);CHKERRQ(ierr);
	
	ierr = DMRestoreLocalVector(dau,&scaleU_loc);CHKERRQ(ierr);
	ierr = DMRestoreGlobalVector(dau,&scaleU);CHKERRQ(ierr);

	PetscFunctionReturn(0);
}


/*
 Computes r = Ax - b
 SNES will scale by -1, F = -r = b - Ax
 Thus, in OUR function, dirichlet slots become A_ii(x_i - phi)
 In SNES, these become A_ii(phi-x_i), and the updates on the dirichlet slots will be
 A_ii d_i = -F_i 
 = A_ii(phi-x_i)
 Then the update will be 
 x_i^new = x_i + d_i
 = x_i + inv(A_ii) A_ii(phi-x_i)
 = x_i + phi - x_i
 = phi
 */
#undef __FUNCT__  
#define __FUNCT__ "FormFunction_Stokes"
PetscErrorCode FormFunction_Stokes(SNES snes,Vec X,Vec F,void *ctx)
{
  pTatinCtx   user = (pTatinCtx)ctx;
  DM                dau,dap;
  DMDALocalInfo     infou,infop;
  PetscErrorCode    ierr;
  Vec               Uloc,Ploc,FUloc,FPloc;
	Vec               u,p,Fu,Fp;
  PetscScalar       *LA_Uloc,*LA_Ploc;
  PetscScalar       *LA_FUloc,*LA_FPloc;
	
  PetscFunctionBegin;
  ierr = DMCompositeGetEntries(user->pack,&dau,&dap);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dau,&infou);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dap,&infop);CHKERRQ(ierr);
	
  ierr = DMCompositeGetLocalVectors(user->pack,&Uloc,&Ploc);CHKERRQ(ierr);
  ierr = DMCompositeGetLocalVectors(user->pack,&FUloc,&FPloc);CHKERRQ(ierr);
	
	/* get the local (ghosted) entries for each physics */
	ierr = DMCompositeScatter(user->pack,X,Uloc,Ploc);CHKERRQ(ierr);
	/* insert boundary conditions into local vectors */
	ierr = BCListInsertLocal(user->u_bclist,Uloc);CHKERRQ(ierr);
	
	ierr = VecGetArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
	ierr = VecGetArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
	
	/* compute Ax - b */
	ierr = VecZeroEntries(FUloc);CHKERRQ(ierr);
	ierr = VecZeroEntries(FPloc);CHKERRQ(ierr);
	ierr = VecGetArray(FUloc,&LA_FUloc);CHKERRQ(ierr);
	ierr = VecGetArray(FPloc,&LA_FPloc);CHKERRQ(ierr);
	
	/* ======================================== */
	/*         UPDATE NON-LINEARITIES           */
	/* evaluate rheology and rhs using X        */

	/* map marker eta to quadrature points */

	/* map marker force to quadrature points */
	
	/* ======================================== */
	
	ierr = pTatin_EvaluateRheologyNonlinearities(user,dau,LA_Uloc,dap,LA_Ploc);CHKERRQ(ierr);
	ierr = pTatin_Stokes_EvaluateSurfaceNonlinearities(user,dau,LA_Uloc,dap,LA_Ploc);CHKERRQ(ierr);
	
	/* Form scaling for momentum */
	//ierr = FormScaling_U_etaMassMatrixDiagonal(user,dau,user->u_bclist);CHKERRQ(ierr);
	
	ierr = FormFunctionLocal_U(user,dau,LA_Uloc,dap,LA_Ploc,LA_FUloc);CHKERRQ(ierr);
	ierr = FormFunctionLocal_U_tractionBC(user,dau,LA_Uloc,dap,LA_Ploc,LA_FUloc);CHKERRQ(ierr);
	
	ierr = FormFunctionLocal_P(user,dau,LA_Uloc,dap,LA_Ploc,LA_FPloc);CHKERRQ(ierr);
	
	ierr = VecRestoreArray(FPloc,&LA_FPloc);CHKERRQ(ierr);
	ierr = VecRestoreArray(FUloc,&LA_FUloc);CHKERRQ(ierr);
	ierr = VecRestoreArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
	ierr = VecRestoreArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
	
	/* do global fem summation */
	ierr = VecZeroEntries(F);CHKERRQ(ierr);
	ierr = DMCompositeGather(user->pack,F,ADD_VALUES,FUloc,FPloc);CHKERRQ(ierr);
	
  ierr = DMCompositeRestoreLocalVectors(user->pack,&FUloc,&FPloc);CHKERRQ(ierr);
  ierr = DMCompositeRestoreLocalVectors(user->pack,&Uloc,&Ploc);CHKERRQ(ierr);
	
	/* modify F for the boundary conditions, F_k = scale_k(x_k - phi_k) */
	ierr = DMCompositeGetAccess(user->pack,F,&Fu,&Fp);CHKERRQ(ierr);
	ierr = DMCompositeGetAccess(user->pack,X,&u,&p);CHKERRQ(ierr);
	
	ierr = BCListResidualDirichlet(user->u_bclist,u,Fu);CHKERRQ(ierr);
	
	ierr = DMCompositeRestoreAccess(user->pack,X,&u,&p);CHKERRQ(ierr);
	ierr = DMCompositeRestoreAccess(user->pack,F,&Fu,&Fp);CHKERRQ(ierr);

#if 0	
	{
		PetscInt i,N;
		PetscScalar *LA_F;
		PetscReal val;

		if (user->step==16) {
			printf("FF\n"); 
			VecView(F,PETSC_VIEWER_STDOUT_WORLD);
			printf("XX\n"); 
			VecView(X,PETSC_VIEWER_STDOUT_WORLD);
		}
		ierr = VecNorm(F,NORM_2,&val);CHKERRQ(ierr);
		if (PetscIsInfOrNanScalar(val)) { printf("FF\n"); VecView(F,PETSC_VIEWER_STDOUT_WORLD); SETERRQ(((PetscObject)F)->comm,PETSC_ERR_FP,"F.F: Infinite or not-a-number generated in norm"); }
		ierr = VecNorm(X,NORM_2,&val);CHKERRQ(ierr);
		if (PetscIsInfOrNanScalar(val)) { printf("XX\n"); VecView(X,PETSC_VIEWER_STDOUT_WORLD); SETERRQ(((PetscObject)X)->comm,PETSC_ERR_FP,"X.X: Infinite or not-a-number generated in norm"); }
		
		
		ierr = VecGetSize(F,&N);CHKERRQ(ierr);
		ierr = VecGetArray(F,&LA_F);CHKERRQ(ierr);
		for (i=0; i<N; i++) {
			int val;
			val = isnan((double)LA_F[i]); if (val==FP_NAN) { printf("F[%d] is NaN \n", i); exit(0); }
			val = isinf((double)LA_F[i]); if (val==FP_INFINITE) { printf("F[%d] is Inf \n", i); exit(0); }
			if (PetscIsInfOrNanScalar(LA_F[i])) { VecView(F,PETSC_VIEWER_STDOUT_WORLD); SETERRQ(((PetscObject)F)->comm,PETSC_ERR_FP,"FF: Infinite or not-a-number generated in norm"); }
		}
		ierr = VecRestoreArray(F,&LA_F);CHKERRQ(ierr);
		ierr = VecGetArray(X,&LA_F);CHKERRQ(ierr);
		for (i=0; i<N; i++) {
			int val;
			val = isnan((double)LA_F[i]); if (val==FP_NAN) { printf("X[%d] is NaN \n", i); exit(0); }
			val = isinf((double)LA_F[i]); if (val==FP_INFINITE) { printf("X[%d] is Inf \n", i); exit(0); }
			if (PetscIsInfOrNanScalar(LA_F[i])) { VecView(X,PETSC_VIEWER_STDOUT_WORLD); SETERRQ(((PetscObject)X)->comm,PETSC_ERR_FP,"XX: Infinite or not-a-number generated in norm"); }
		}
		ierr = VecRestoreArray(X,&LA_F);CHKERRQ(ierr);

	}
#endif
	
  PetscFunctionReturn(0);
}

PetscInt ASS_MAP_wIwDI_uJuDJ(PetscInt wi, PetscInt wd, PetscInt w_NPE, PetscInt w_dof,
														 PetscInt ui, PetscInt ud, PetscInt u_NPE, PetscInt u_dof )
{
	PetscInt ij;
	PetscInt r,c,nr,nc;
	
	nr = w_NPE * w_dof;
	nc = u_NPE * u_dof;
	
	r = w_dof * wi + wd;
	c = u_dof * ui + ud;
	
	ij = r*nc + c;
	return ij;
}

void AElement_Stokes_A11_Q2_2D(PetscScalar Ke[],PetscScalar coords[],PetscScalar eta[],PetscInt ngp,PetscScalar gp_xi[],PetscScalar gp_weight[] )
{
	PetscInt p,d,i,j,k;
	PetscScalar GNx_p[2][Q2_NODES_PER_EL_2D];
	PetscScalar J_p, tildeD[3], Ni[Q2_NODES_PER_EL_2D],eta_p,xp,yp;
	PetscScalar B[3][2*Q2_NODES_PER_EL_2D];
	
	
	/* evaluate integral */
	for( p=0; p<ngp; p++ ) {
		PetscScalar *xip = &gp_xi[NSD*p];
		
		ConstructGNX_Q2_2D(xip,GNx_p,coords,J_p);
		
		for( i=0; i<Q2_NODES_PER_EL_2D; i++ ) {
			PetscScalar d_dx_i = GNx_p[0][i];
			PetscScalar d_dy_i = GNx_p[1][i];
			
			B[0][2*i] = d_dx_i;		B[0][2*i+1] = 0.0;
			B[1][2*i] = 0.0;		  B[1][2*i+1] = d_dy_i;
			B[2][2*i] = d_dy_i;		B[2][2*i+1] = d_dx_i;
		}
		
		for( i=0; i<3; i++ ) {
			tildeD[i] = 0.0;
		}
		eta_p = eta[p];
		tildeD[0] = gp_weight[p] * J_p * 2.0 * eta_p;
		tildeD[1] = gp_weight[p] * J_p * 2.0 * eta_p;
		tildeD[2] = gp_weight[p] * J_p * 1.0 * eta_p;
		
		/* form Bt tildeD B */
		/* 
		 Ke_ij = Bt_ik . D_kl . B_lj 
		 = B_ki . D_kl . B_lj
		 = B_ki . D_kk . B_kj
		 */
		/*
		 for( i=0; i<9*2; i++ ) {
		 for( j=0; j<9*2; j++ ) { 
		 for( k=0; k<3; k++ ) { // Note D is diagonal for stokes //
		 Ke[18*i+j] = Ke[18*i+j] + B[k][i] * tildeD[k] * B[k][j];
		 }
		 }
		 }
		 */
		// build upper triangular part only //
		for( i=0; i<9*2; i++ ) {
			for( j=i; j<9*2; j++ ) { 
				for( k=0; k<3; k++ ) { // Note D is diagonal for stokes //
					Ke[18*i+j] = Ke[18*i+j] + B[k][i] * tildeD[k] * B[k][j];
				}
			}
		}
	}
	
	// copy lower triangular part //
	for( i=0; i<9*2; i++ ) {
		for( j=i; j<9*2; j++ ) { 
			Ke[18*j+i] = Ke[18*i+j];
		}
	}
}

void AElement_Stokes_A12_Q2_2D( PetscScalar Ke[], PetscScalar coords[],PetscInt ngp, PetscScalar gp_xi[], PetscScalar gp_weight[] )
{
	PetscInt p,i,j,di;
	PetscScalar Ni_p[Q2_NODES_PER_EL_2D];
	PetscScalar GNx_p[2][Q2_NODES_PER_EL_2D];
	PetscScalar J_p, fac;
	PetscInt vnpe, veqns;
	PetscInt pnpe, peqns;
	
	vnpe   = Q2_NODES_PER_EL_2D;
	veqns  = vnpe * 2; 
	
	pnpe   = P_BASIS_FUNCTIONS;
	peqns  = pnpe * 1;
	
	/* evaluate integral */
	for( p=0; p<ngp; p++ ) {
		PetscScalar *xip = &gp_xi[NSD*p];
		
		ConstructNi_pressure( xip, coords, Ni_p );
		ConstructGNX_Q2_2D(xip,GNx_p,coords,J_p);
		
		fac = gp_weight[p] * J_p;
		
		for( i=0; i<vnpe; i++ ) { // u nodes
			for( di=0; di<NSD; di++ ) { // u dofs
				for( j=0; j<pnpe; j++ ) {  // p nodes, p dofs = 1 (ie no loop) 
					PetscInt IJ;
					IJ = ASS_MAP_wIwDI_uJuDJ( i,di,vnpe,2 , j,0,pnpe,1 );
					
					Ke[IJ] = Ke[IJ] - GNx_p[di][i] * Ni_p[j] * fac;
				}
			}
		}
	}
}
void AElement_Stokes_A21_Q2_2D( PetscScalar De[], PetscScalar coords[],PetscInt ngp, PetscScalar gp_xi[], PetscScalar gp_weight[] )
{
	PetscScalar Ge[U_DOFS*U_BASIS_FUNCTIONS*P_DOFS*P_BASIS_FUNCTIONS];
	PetscInt i,j;
	PetscInt nr_g, nc_g;
	PetscInt vnpe, veqns;
	PetscInt pnpe, peqns;
	
	vnpe   = Q2_NODES_PER_EL_2D;
	veqns  = vnpe * 2; 
	pnpe   = P_BASIS_FUNCTIONS;
	peqns  = pnpe * 1;
	
	PetscMemzero( Ge, sizeof(PetscScalar)* U_DOFS*Q2_NODES_PER_EL_2D*P_DOFS*P_BASIS_FUNCTIONS );
	AElement_Stokes_A12_Q2_2D( Ge, coords,ngp,gp_xi,gp_weight );
	
	nr_g = veqns;
	nc_g = peqns;
	
	for( i=0; i<nr_g; i++ ){
		for( j=0; j<nc_g; j++ ) {
			De[nr_g*j+i] = Ge[nc_g*i+j];
		}}
}

void AElement_Stokes_B22_P1_2D(PetscScalar Ke[],PetscScalar coords[],PetscScalar eta[],PetscInt ngp,PetscScalar gp_xi[],PetscScalar gp_weight[] )
{
	PetscInt p,d,i,j,k;
	PetscScalar Ni_p[P_BASIS_FUNCTIONS],GNx_p[2][Q2_NODES_PER_EL_2D];
	PetscScalar eta_avg,fac,J_p;
	
	/* scale */
	eta_avg = 0.0;
	
	/* evaluate integral */
	for( p=0; p<ngp; p++ ) {
		PetscScalar *xip = &gp_xi[NSD*p];
		
		ConstructNi_pressure(xip,coords,Ni_p);
		ConstructGNX_Q2_2D(xip,GNx_p,coords,J_p);
		fac = gp_weight[p] * J_p;
		
		eta_avg += eta[p];
		
		// build upper triangular part only //
		for( i=0; i<P_BASIS_FUNCTIONS; i++ ) {
			for( j=i; j<P_BASIS_FUNCTIONS; j++ ) { 
				Ke[P_BASIS_FUNCTIONS*i+j] = Ke[P_BASIS_FUNCTIONS*i+j] - Ni_p[i] * Ni_p[j] * fac;
			}
		}
		
	}
	
	eta_avg = (1.0/((PetscScalar)ngp)) * eta_avg;
	fac = 1.0/eta_avg;
	
	// scale and then copy lower triangular part //
	for( i=0; i<P_BASIS_FUNCTIONS; i++ ) {
		for( j=i; j<P_BASIS_FUNCTIONS; j++ ) { 
			Ke[P_BASIS_FUNCTIONS*i+j] = Ke[P_BASIS_FUNCTIONS*i+j] * fac;
			Ke[P_BASIS_FUNCTIONS*j+i] = Ke[P_BASIS_FUNCTIONS*i+j];
		}
	}
}

#undef __FUNCT__
#define __FUNCT__ "Assemble_Stokes_A11_Q2"
PetscErrorCode Assemble_Stokes_A11_Q2(pTatinCtx user,DM dau,PetscScalar vel_field[],DM dap,PetscScalar pressure_field[],Mat Auu)
{	
	PetscErrorCode ierr;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen,e,n;
	const PetscInt *elnidx;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D],el_eta[MAX_QUAD_PNTS];
	PetscScalar Ae[Q2_NODES_PER_EL_2D*2 * Q2_NODES_PER_EL_2D*2];
	PetscInt *gidx,elgidx[2*Q2_NODES_PER_EL_2D],elgidx_bc[2*Q2_NODES_PER_EL_2D];
	PetscInt nbcs,i;
	const PetscInt *ubcidx;
	Vec diag;
	PetscScalar *LA_diag;
	PetscLogDouble t0,t1;
	BCList u_bclist = user->u_bclist;
	PetscInt Len_local, *gidx_bclocal;
	GaussPointCoefficientsStokes *gausspoints;

	PetscFunctionBegin;
//	ierr = UpdateRheology(user,dau,vel_field,dap,pressure_field);CHKERRQ(ierr);
	
	/* zero entries */
	ierr = MatZeroEntries(Auu);CHKERRQ(ierr);
	
	/* quadrature */
	//	QuadratureCreateGauss_3pnt_2D(&ngp,gp_xi,gp_weight);
	ngp       = user->Q->ngp;
	gp_xi     = user->Q->xi;
	gp_weight = user->Q->weight;
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetGlobalIndices(dau,0,&gidx);CHKERRQ(ierr);
	ierr = BCListGetDofIdx(u_bclist,0,0,&Len_local,&gidx_bclocal);CHKERRQ(ierr);
	
	//	ierr = DMGetElements_DA_Q2(dau,&nel,&nen,&elnidx);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen,&elnidx);CHKERRQ(ierr);
	PetscGetTime(&t0);
	for (e=0;e<nel;e++) {
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx[nen*e],LA_gcoords);CHKERRQ(ierr);
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		/*
		 printf("e[%d]: (0)%d %d %d (3)%d (6)%d \n", e, elnidx[nen*e],elnidx[nen*e+1],elnidx[nen*e+2],
		 elnidx[nen*e+3],elnidx[nen*e+6]);
		 */
		
		/* initialise element stiffness matrix */
		PetscMemzero( Ae, sizeof(PetscScalar)* Q2_NODES_PER_EL_2D*2 * Q2_NODES_PER_EL_2D*2 );
		
		/* evaluate the viscosity */
		for (n=0; n<ngp; n++) {
			el_eta[n] = gausspoints[n].eta;
		}
		
		/* form element stiffness matrix */
		AElement_Stokes_A11_Q2_2D( Ae, elcoords, el_eta, ngp,gp_xi,gp_weight );
#if 0		
		if (e==0) {
			PetscInt I,J,N;
			N = U_BASIS_FUNCTIONS*U_DOFS;
			PetscPrintf(PETSC_COMM_WORLD,"Ae = \n");
			for( I=0; I<U_BASIS_FUNCTIONS*U_DOFS; I++ ) {
				for( J=0; J<U_BASIS_FUNCTIONS*U_DOFS; J++ ) {
					PetscPrintf(PETSC_COMM_WORLD,"%1.4e ", Ae[J+I*N] );
				} PetscPrintf(PETSC_COMM_WORLD,"\n");
			} PetscPrintf(PETSC_COMM_WORLD,"\n");
		}
#endif		
		/* insert element matrix into global matrix */
		ierr = GetElementEqnIndicesQ2((PetscInt*)&elnidx[nen*e],gidx,elgidx);CHKERRQ(ierr);
		ierr = GetElementEqnIndicesQ2((PetscInt*)&elnidx[nen*e],gidx_bclocal,elgidx_bc);CHKERRQ(ierr);
		
		/*
		 printf("e[%d]: gi(0){%d,%d} {%d,%d} {%d,%d} \n", 
		 e, elgidx[2*0],elgidx[2*0+1],
		 elgidx[2*1],elgidx[2*1+1],
		 elgidx[2*2],elgidx[2*2+1] );
		 printf("      gi(3){%d,%d} {%d,%d} {%d,%d} \n", 
		 elgidx[2*3],elgidx[2*3+1],
		 elgidx[2*4],elgidx[2*4+1],
		 elgidx[2*5],elgidx[2*5+1] );		
		 printf("      gi(6){%d,%d} {%d,%d} {%d,%d} \n", 
		 elgidx[2*6],elgidx[2*6+1],
		 elgidx[2*7],elgidx[2*7+1],
		 elgidx[2*8],elgidx[2*8+1] );		
		 */
		
		//		printf("elgidx_bc = { %d %d %d : %d %d %d : %d %d %d } \n", elgidx_bc[0],elgidx_bc[2],elgidx_bc[4],elgidx_bc[6],elgidx_bc[8], elgidx_bc[10],elgidx_bc[12],elgidx_bc[14],elgidx_bc[16]);
		ierr = MatSetValues( Auu, Q2_NODES_PER_EL_2D*2,elgidx_bc, Q2_NODES_PER_EL_2D*2,elgidx_bc, Ae, ADD_VALUES );CHKERRQ(ierr);
		//		ierr = MatSetValues( Auu, Q2_NODES_PER_EL_2D*2,elgidx, Q2_NODES_PER_EL_2D*2,elgidx, Ae, ADD_VALUES );CHKERRQ(ierr);
	}
	ierr = MatAssemblyBegin(Auu, MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(Auu, MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
	PetscGetTime(&t1);
//	PetscPrintf(PETSC_COMM_WORLD,"Assemble A11, Q2 = %1.4e (sec)\n",t1-t0);
	
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	
	
	/* this is simply a crappy way to insert bc's */	
	
#if 0	
	ierr = MatGetVecs(Auu,&diag,0);CHKERRQ(ierr);
	ierr = MatGetDiagonal(Auu,diag);CHKERRQ(ierr);
	ierr = VecGetArray(diag,&LA_diag);CHKERRQ(ierr);
	
	/* force bc's */
	PetscGetTime(&t0);
	ierr = BCListGetIndicesRO(user->u_bclist,&nbcs,&ubcidx);CHKERRQ(ierr);
	for( i=0; i<nbcs; i++ ) {
		if (ubcidx[i]==BCList_DIRICHLET) {
			ierr = MatZeroRowsColumns( Auu, 1, &user->u_bclist->gidx[i], LA_diag[i], PETSC_NULL,PETSC_NULL );CHKERRQ(ierr);
		}
	}
	PetscGetTime(&t1);
//	PetscPrintf(PETSC_COMM_WORLD,"Assemble A11 bcs, Q2 = %1.4e (sec)\n",t1-t0);
	ierr = BCListRestoreIndicesRO(user->u_bclist,&nbcs,&ubcidx);CHKERRQ(ierr);
	ierr = VecRestoreArray(diag,&LA_diag);CHKERRQ(ierr);
	ierr = VecDestroy(&diag);
#endif	
	
	/* This is wrong. The local insertition should be using indices which contain the ghosts */	
#if 0
	{
		PetscInt Len_global,*gidx_bcglobal;
		PetscBool is_dirich;
		PetscInt mm,nn;
//		ierr = BCListGetDofIdx(u_bclist,0,0,&Len_global,&gidx_bcglobal);CHKERRQ(ierr);
		ierr = BCListGetDofIdx(u_bclist,&Len_global,&gidx_bcglobal,0,0);CHKERRQ(ierr);
		ierr = MatGetLocalSize(Auu,&mm,&nn);CHKERRQ(ierr);
		for (i=0; i<Len_global; i++) {
			ierr = BCListIsDirichlet(gidx_bcglobal[i],&is_dirich);CHKERRQ(ierr);
			if (is_dirich) {
				//				ierr = MatSetValueLocal(Auu,i,i,1.0,INSERT_VALUES);CHKERRQ(ierr);
				ierr = MatSetValueLocal(Auu,i,i,u_bclist->scale_global[i],INSERT_VALUES);CHKERRQ(ierr);
			}
		}
	}
#endif	
	
	/* works, but this requires communication */
#if 1
	{
		PetscInt Len_ghost,*gidx_bcghost;
		PetscBool is_dirich;
		PetscInt nproc,*procs,*mapL,**mapIdx;
		ISLocalToGlobalMapping map;
		PetscMPIInt rank;
		
		MPI_Comm_rank( ((PetscObject)dau)->comm,&rank);
		ierr = BCListGetDofIdx(u_bclist,0,0,&Len_ghost,&gidx_bcghost);CHKERRQ(ierr);
		
		ierr = DMGetLocalToGlobalMapping(dau,&map);CHKERRQ(ierr);
		ierr = ISLocalToGlobalMappingGetInfo(map,&nproc,&procs,&mapL,&mapIdx);CHKERRQ(ierr);
		
		if (Len_ghost!=mapL[rank]) {
			//			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"map indices are fucked up");
		}
		for (i=0; i<Len_ghost; i++) {
			ierr = BCListIsDirichlet(gidx_bcghost[i],&is_dirich);CHKERRQ(ierr);
			if (is_dirich) {
				ierr = MatSetValueLocal(Auu,i,i,1.0,INSERT_VALUES);CHKERRQ(ierr);
			}
		}
	}
#endif	
	
	
	/* Works, but it's not the way I want to do it. It doesn't require any communication though */
#if 0
	{
		PetscInt Len_local,*gidx_bclocal;
		PetscInt *alldofs;
		PetscBool is_dirich;
		PetscInt mm,nn,dcount;
		ierr = DMDAGetGlobalIndices(dau,&dcount,&alldofs);CHKERRQ(ierr);
		ierr = BCListGetDofIdx(u_bclist,0,0,&Len_local,&gidx_bclocal);CHKERRQ(ierr);
		ierr = MatGetLocalSize(Auu,&mm,&nn);CHKERRQ(ierr);
		if (dcount!=Len_local) {
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"local count is fucked up");
		}
		for (i=0; i<Len_local; i++) {
			ierr = BCListIsDirichlet(gidx_bclocal[i],&is_dirich);CHKERRQ(ierr);
			if (is_dirich) {
				ierr = MatSetValue(Auu,alldofs[i],alldofs[i],1.0,INSERT_VALUES);CHKERRQ(ierr);
			}
		}
	}
#endif

/* scaled variant */
#if 0
	{
		
		
	}
#endif
	
	
	ierr = MatAssemblyBegin(Auu, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(Auu, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	
	//	ierr = PetscViewerSetFormat(PETSC_VIEWER_STDOUT_WORLD,PETSC_VIEWER_ASCII_DENSE);CHKERRQ(ierr);
	//ierr = MatView(Auu,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

void AElement_Stokes_auglagB22_P1_2D(PetscBool invert,PetscScalar lambda,PetscScalar Ke[],PetscScalar coords[],PetscScalar eta[],PetscInt ngp,PetscScalar gp_xi[],PetscScalar gp_weight[] )
{
	PetscInt p,d,i,j,k;
	PetscScalar Ni_p[P_BASIS_FUNCTIONS],GNx_p[2][Q2_NODES_PER_EL_2D];
	PetscScalar eta_avg,fac,J_p;
	PetscScalar Me[P_BASIS_FUNCTIONS][P_BASIS_FUNCTIONS];
	PetscScalar invMe[P_BASIS_FUNCTIONS][P_BASIS_FUNCTIONS];
	
	
	for( i=0; i<P_BASIS_FUNCTIONS; i++ ) {
		for( j=0; j<P_BASIS_FUNCTIONS; j++ ) { 
			Me[i][j] = 0.0;
		}
	}
	
	/* scale */
	eta_avg = 0.0;
	
	/* evaluate integral */
	for( p=0; p<ngp; p++ ) {
		PetscScalar *xip = &gp_xi[NSD*p];
		
		ConstructNi_pressure(xip,coords,Ni_p);
		ConstructGNX_Q2_2D(xip,GNx_p,coords,J_p);
		fac = gp_weight[p] * J_p;
		
		eta_avg += eta[p];
		
		// build upper triangular part only //
		for( i=0; i<P_BASIS_FUNCTIONS; i++ ) {
			for( j=i; j<P_BASIS_FUNCTIONS; j++ ) { 
				Me[i][j] = Me[i][j] + lambda * Ni_p[i] * Ni_p[j] * fac;
			}
		}
	}
	
	eta_avg = (1.0/((PetscScalar)ngp)) * eta_avg;
	fac = 1.0/eta_avg;
	
	// copy lower triangular part //
	for( i=0; i<P_BASIS_FUNCTIONS; i++ ) {
		for( j=i; j<P_BASIS_FUNCTIONS; j++ ) { 
			Me[i][j] = fac * Me[i][j];

			Me[j][i] = Me[i][j];
		}
	}

	// invert //
	if (invert == PETSC_TRUE) {
		ElementHelper_matrix_inverse_3x3(Me,invMe);

		for( i=0; i<P_BASIS_FUNCTIONS; i++ ) {
			for( j=0; j<P_BASIS_FUNCTIONS; j++ ) { 
				Ke[P_BASIS_FUNCTIONS*i+j] = invMe[i][j];
			}
		}
	} else {
		for( i=0; i<P_BASIS_FUNCTIONS; i++ ) {
			for( j=0; j<P_BASIS_FUNCTIONS; j++ ) { 
				Ke[P_BASIS_FUNCTIONS*i+j] = Me[i][j];
			}
		}
	}
	

}


#undef __FUNCT__
#define __FUNCT__ "Assemble_Stokes_B11_AugLag_Q2"
PetscErrorCode Assemble_Stokes_B11_AugLag_Q2(pTatinCtx user,DM dau,PetscScalar vel_field[],DM dap,PetscScalar pressure_field[],Mat Auu,PetscScalar lambda)
{	
	PetscErrorCode ierr;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen,e,n;
	const PetscInt *elnidx;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D],el_eta[MAX_QUAD_PNTS];
	PetscScalar Ae[Q2_NODES_PER_EL_2D*2 * Q2_NODES_PER_EL_2D*2];
	PetscInt *gidx,elgidx[2*Q2_NODES_PER_EL_2D],elgidx_bc[2*Q2_NODES_PER_EL_2D];
	PetscInt nbcs,i,j,k,l;
	const PetscInt *ubcidx;
	Vec diag;
	PetscScalar *LA_diag;
	PetscLogDouble t0,t1;
	BCList u_bclist = user->u_bclist;
	PetscInt Len_local, *gidx_bclocal;
	GaussPointCoefficientsStokes *gausspoints;
	
	PetscScalar aug[Q2_NODES_PER_EL_2D*2 * Q2_NODES_PER_EL_2D*2];
	PetscScalar Ge[Q2_NODES_PER_EL_2D*2 * P_BASIS_FUNCTIONS];
	PetscScalar De[Q2_NODES_PER_EL_2D*2 * P_BASIS_FUNCTIONS];
	PetscScalar Me[P_BASIS_FUNCTIONS * P_BASIS_FUNCTIONS];
	
	PetscFunctionBegin;
	
	/* zero entries */
	ierr = MatZeroEntries(Auu);CHKERRQ(ierr);
	
	/* quadrature */
	//	QuadratureCreateGauss_3pnt_2D(&ngp,gp_xi,gp_weight);
	ngp       = user->Q->ngp;
	gp_xi     = user->Q->xi;
	gp_weight = user->Q->weight;
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetGlobalIndices(dau,0,&gidx);CHKERRQ(ierr);
	ierr = BCListGetDofIdx(u_bclist,0,0,&Len_local,&gidx_bclocal);CHKERRQ(ierr);
	
	//	ierr = DMGetElements_DA_Q2(dau,&nel,&nen,&elnidx);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen,&elnidx);CHKERRQ(ierr);
	PetscGetTime(&t0);
	for (e=0;e<nel;e++) {
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx[nen*e],LA_gcoords);CHKERRQ(ierr);
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		/* initialise element stiffness matrix */
		PetscMemzero( Ae, sizeof(PetscScalar)* Q2_NODES_PER_EL_2D*2 * Q2_NODES_PER_EL_2D*2 );
		PetscMemzero( aug, sizeof(PetscScalar)* Q2_NODES_PER_EL_2D*2 * Q2_NODES_PER_EL_2D*2 );

		PetscMemzero( Ge, sizeof(PetscScalar)* Q2_NODES_PER_EL_2D*2 * P_BASIS_FUNCTIONS );
		PetscMemzero( De, sizeof(PetscScalar)* Q2_NODES_PER_EL_2D*2 * P_BASIS_FUNCTIONS );
		PetscMemzero( Me, sizeof(PetscScalar)* P_BASIS_FUNCTIONS * P_BASIS_FUNCTIONS );
		
		/* evaluate the viscosity */
		for (n=0; n<ngp; n++) {
			el_eta[n] = gausspoints[n].eta;
		}
		
		/* form element stiffness matrix */
		AElement_Stokes_A11_Q2_2D( Ae, elcoords, el_eta, ngp,gp_xi,gp_weight );

		AElement_Stokes_A12_Q2_2D( Ge, elcoords, ngp,gp_xi,gp_weight );
		AElement_Stokes_A21_Q2_2D( De, elcoords, ngp,gp_xi,gp_weight );
		AElement_Stokes_auglagB22_P1_2D( PETSC_TRUE, lambda,Me, elcoords, el_eta, ngp,gp_xi,gp_weight );

		// au = Ge_{ik}.invMe_{kl}.De_{lj}
		for (i=0; i<18; i++) {
			for (k=0; k<3; k++) {
				for (l=0; l<3; l++) {
					for (j=0; j<18; j++) {
						aug[18*i+j] = aug[18*i+j] + Ge[3*i+k] * Me[3*k+l] * De[18*l+j];
					}
				}
			}			
		}
		
		for (i=0; i<18*18; i++) {
			Ae[i] = Ae[i] + aug[i];
		}	
			
		/* insert element matrix into global matrix */
		ierr = GetElementEqnIndicesQ2((PetscInt*)&elnidx[nen*e],gidx,elgidx);CHKERRQ(ierr);
		ierr = GetElementEqnIndicesQ2((PetscInt*)&elnidx[nen*e],gidx_bclocal,elgidx_bc);CHKERRQ(ierr);
		
		ierr = MatSetValues( Auu, Q2_NODES_PER_EL_2D*2,elgidx_bc, Q2_NODES_PER_EL_2D*2,elgidx_bc, Ae, ADD_VALUES );CHKERRQ(ierr);
	}
	ierr = MatAssemblyBegin(Auu, MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(Auu, MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
	PetscGetTime(&t1);
	//	PetscPrintf(PETSC_COMM_WORLD,"Assemble A11, Q2 = %1.4e (sec)\n",t1-t0);
	
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	/* works, but this requires communication */
#if 1
	{
		PetscInt Len_ghost,*gidx_bcghost;
		PetscBool is_dirich;
		PetscInt nproc,*procs,*mapL,**mapIdx;
		ISLocalToGlobalMapping map;
		PetscMPIInt rank;
		
		MPI_Comm_rank( ((PetscObject)dau)->comm,&rank);
		ierr = BCListGetDofIdx(u_bclist,0,0,&Len_ghost,&gidx_bcghost);CHKERRQ(ierr);
		
		ierr = DMGetLocalToGlobalMapping(dau,&map);CHKERRQ(ierr);
		ierr = ISLocalToGlobalMappingGetInfo(map,&nproc,&procs,&mapL,&mapIdx);CHKERRQ(ierr);
		
		if (Len_ghost!=mapL[rank]) {
			//			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"map indices are fucked up");
		}
		for (i=0; i<Len_ghost; i++) {
			ierr = BCListIsDirichlet(gidx_bcghost[i],&is_dirich);CHKERRQ(ierr);
			if (is_dirich) {
				ierr = MatSetValueLocal(Auu,i,i,1.0,INSERT_VALUES);CHKERRQ(ierr);
			}
		}
	}
#endif	
	
	
	
	
	ierr = MatAssemblyBegin(Auu, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(Auu, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	
	//	ierr = PetscViewerSetFormat(PETSC_VIEWER_STDOUT_WORLD,PETSC_VIEWER_ASCII_DENSE);CHKERRQ(ierr);
	//ierr = MatView(Auu,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "Assemble_Stokes_B22_AugLag_P1"
PetscErrorCode Assemble_Stokes_B22_AugLag_P1(pTatinCtx user,DM dau,const PetscScalar u[],DM dap,const PetscScalar p[],Mat Bpp,PetscScalar lambda)
{	
	PetscErrorCode ierr;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel_u,nel_p,nen_u,nen_p,e,n;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D],el_eta[MAX_QUAD_PNTS];
	PetscScalar Ae[P_BASIS_FUNCTIONS * P_BASIS_FUNCTIONS];
	PetscInt *gidx_u,*gidx_p,elgidx_p[P_BASIS_FUNCTIONS];
	PetscInt nbcs,i;
	const PetscInt *ubcidx;
	Vec diag;
	PetscScalar *LA_diag;
	PetscLogDouble t0,t1;
	GaussPointCoefficientsStokes *gausspoints;

	PetscFunctionBegin;
	
	/* zero entries */
	ierr = MatZeroEntries(Bpp);CHKERRQ(ierr);
	
	/* quadrature */
	//	QuadratureCreateGauss_3pnt_2D(&ngp,gp_xi,gp_weight);
	ngp       = user->Q->ngp;
	gp_xi     = user->Q->xi;
	gp_weight = user->Q->weight;
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA(dau,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetGlobalIndices(dau,0,&gidx_u);CHKERRQ(ierr);
	ierr = DMDAGetGlobalIndices(dap,0,&gidx_p);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel_u,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel_p,&nen_p,&elnidx_p);CHKERRQ(ierr);
	if (nel_u	!= nel_p) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Velocity DA is not compatible with pressure DA\n");
	}
	
	PetscGetTime(&t0);
	for (e=0;e<nel_u;e++) {
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		
		/* initialise element stiffness matrix */
		PetscMemzero( Ae, sizeof(PetscScalar)* P_BASIS_FUNCTIONS * P_BASIS_FUNCTIONS );
		
		/* evaluate the viscosity */
		for (n=0; n<ngp; n++) {
			el_eta[n] = gausspoints[n].eta;
		}
		
		/* form element stiffness matrix */
//		AElement_Stokes_auglagB22_P1_2D( PETSC_FALSE, Ae, elcoords, el_eta, ngp,gp_xi,gp_weight );
		/* use -pc_type mat */
		AElement_Stokes_auglagB22_P1_2D( PETSC_TRUE, lambda, Ae, elcoords, el_eta, ngp,gp_xi,gp_weight );
		
		/* insert element matrix into global matrix */
		ierr = GetElementEqnIndicesPressure(nen_p,(PetscInt*)&elnidx_p[nen_p*e],gidx_p,elgidx_p);CHKERRQ(ierr);
		//		printf("[e=%d]: %d %d %d \n", e, elgidx_p[0],elgidx_p[1],elgidx_p[2] );
		
		ierr = MatSetValues( Bpp, P_BASIS_FUNCTIONS,elgidx_p, P_BASIS_FUNCTIONS,elgidx_p, Ae, ADD_VALUES );CHKERRQ(ierr);
	}
	ierr = MatAssemblyBegin(Bpp, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(Bpp, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	PetscGetTime(&t1);
	//	PetscPrintf(PETSC_COMM_WORLD,"Assemble B22, P1 = %1.4e (sec)\n",t1-t0);
	
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "Assemble_Stokes_A12_Q2"
PetscErrorCode Assemble_Stokes_A12_Q2(pTatinCtx user,DM dau,const PetscScalar velocity[],DM dap,const PetscScalar pressure[],Mat A)
{	
	PetscErrorCode ierr;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt e,n;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar Ae[Q2_NODES_PER_EL_2D*2 * P_BASIS_FUNCTIONS];
	PetscLogDouble t0,t1;
	/* velocity */
	PetscInt u_nel,u_nen; /* num vel elements, nodes per ele */
	BCList u_bclist = user->u_bclist;
	const PetscInt *u_elnidx; /* element -> node indices */
	PetscInt *u_gidx, u_elgidx[2*Q2_NODES_PER_EL_2D]; /* global indices and element counterpart (ghosted) */
	PetscInt u_Len_local, *u_gidx_bclocal, u_elgidx_bc[2*Q2_NODES_PER_EL_2D]; /* global indices and element counterpart with bc's masked out (ghosted) */
	/* pressure */
	PetscInt p_nel,p_nen;
	BCList p_bclist = user->p_bclist; /* this will be NULL */
	const PetscInt *p_elnidx;
	PetscInt *p_gidx, p_elgidx[P_BASIS_FUNCTIONS];
	
	PetscFunctionBegin;
	
	/* zero entries */
	ierr = MatZeroEntries(A);CHKERRQ(ierr);
	
	/* quadrature */
	//	QuadratureCreateGauss_3pnt_2D(&ngp,gp_xi,gp_weight);
	ngp       = user->Q->ngp;
	gp_xi     = user->Q->xi;
	gp_weight = user->Q->weight;
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	/* setup eqnums */
	ierr = DMDAGetGlobalIndices(dau,0,&u_gidx);CHKERRQ(ierr);
	ierr = BCListGetDofIdx(u_bclist,0,0,&u_Len_local,&u_gidx_bclocal);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dau,&u_nel,&u_nen,&u_elnidx);CHKERRQ(ierr);
	
	ierr = DMDAGetGlobalIndices(dap,0,&p_gidx);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&p_nel,&p_nen,&p_elnidx);CHKERRQ(ierr);
	
	PetscGetTime(&t0);
	for (e=0;e<u_nel;e++) {
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&u_elnidx[u_nen*e],LA_gcoords);CHKERRQ(ierr);
		
		/* initialise element stiffness matrix */
		PetscMemzero( Ae, sizeof(PetscScalar)* Q2_NODES_PER_EL_2D*2 * P_BASIS_FUNCTIONS );
		
		/* form element stiffness matrix */
		AElement_Stokes_A12_Q2_2D( Ae, elcoords, ngp,gp_xi,gp_weight );
#if 0				
		if (e==0) {
			PetscInt I,J,N;
			N = P_BASIS_FUNCTIONS;
			PetscPrintf(PETSC_COMM_WORLD,"Ge = \n");
			for( I=0; I<U_BASIS_FUNCTIONS*U_DOFS; I++ ) {
				for( J=0; J<P_BASIS_FUNCTIONS; J++ ) {
					PetscPrintf(PETSC_COMM_WORLD,"%1.4e ", Ae[J+I*N] );
				} PetscPrintf(PETSC_COMM_WORLD,"\n");
			} PetscPrintf(PETSC_COMM_WORLD,"\n");
		}
#endif		
		/* insert element matrix into global matrix */
		ierr = GetElementEqnIndicesQ2((PetscInt*)&u_elnidx[u_nen*e],u_gidx_bclocal,u_elgidx_bc);CHKERRQ(ierr);
		ierr = GetElementEqnIndicesPressure(P_BASIS_FUNCTIONS,(PetscInt*)&p_elnidx[p_nen*e],p_gidx,p_elgidx);CHKERRQ(ierr);
		ierr = MatSetValues( A, Q2_NODES_PER_EL_2D*2,u_elgidx_bc, P_BASIS_FUNCTIONS,p_elgidx, Ae, ADD_VALUES );CHKERRQ(ierr);
	}
	ierr = MatAssemblyBegin(A, MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(A, MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
	PetscGetTime(&t1);
//	PetscPrintf(PETSC_COMM_WORLD,"Assemble A12, Q2 = %1.4e (sec)\n",t1-t0);
	
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	/* no bc modifications */
	ierr = MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "Assemble_Stokes_A21_Q2"
PetscErrorCode Assemble_Stokes_A21_Q2(pTatinCtx user,DM dau,const PetscScalar velocity[],DM dap,const PetscScalar pressure[],Mat A)
{	
	PetscErrorCode ierr;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt e,n;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar Ae[Q2_NODES_PER_EL_2D*2 * P_BASIS_FUNCTIONS];
	PetscLogDouble t0,t1;
	/* velocity */
	PetscInt u_nel,u_nen; /* num vel elements, nodes per ele */
	BCList u_bclist = user->u_bclist;
	const PetscInt *u_elnidx; /* element -> node indices */
	PetscInt *u_gidx, u_elgidx[2*Q2_NODES_PER_EL_2D]; /* global indices and element counterpart (ghosted) */
	PetscInt u_Len_local, *u_gidx_bclocal, u_elgidx_bc[2*Q2_NODES_PER_EL_2D]; /* global indices and element counterpart with bc's masked out (ghosted) */
	/* pressure */
	PetscInt p_nel,p_nen;
	BCList p_bclist = user->p_bclist; /* this will be NULL */
	const PetscInt *p_elnidx;
	PetscInt *p_gidx, p_elgidx[P_BASIS_FUNCTIONS];
	
	PetscFunctionBegin;
	
	/* zero entries */
	ierr = MatZeroEntries(A);CHKERRQ(ierr);
	
	/* quadrature */
	//	QuadratureCreateGauss_3pnt_2D(&ngp,gp_xi,gp_weight);
	ngp       = user->Q->ngp;
	gp_xi     = user->Q->xi;
	gp_weight = user->Q->weight;
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	/* setup eqnums */
	ierr = DMDAGetGlobalIndices(dau,0,&u_gidx);CHKERRQ(ierr);
	ierr = BCListGetDofIdx(u_bclist,0,0,&u_Len_local,&u_gidx_bclocal);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dau,&u_nel,&u_nen,&u_elnidx);CHKERRQ(ierr);
	
	ierr = DMDAGetGlobalIndices(dap,0,&p_gidx);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&p_nel,&p_nen,&p_elnidx);CHKERRQ(ierr);
	
	PetscGetTime(&t0);
	for (e=0;e<u_nel;e++) {
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&u_elnidx[u_nen*e],LA_gcoords);CHKERRQ(ierr);
		
		/* initialise element stiffness matrix */
		PetscMemzero( Ae, sizeof(PetscScalar)* Q2_NODES_PER_EL_2D*2 * P_BASIS_FUNCTIONS );
		
		/* form element stiffness matrix */
		AElement_Stokes_A21_Q2_2D( Ae, elcoords, ngp,gp_xi,gp_weight );
		
		/* insert element matrix into global matrix */
		ierr = GetElementEqnIndicesQ2((PetscInt*)&u_elnidx[u_nen*e],u_gidx_bclocal,u_elgidx_bc);CHKERRQ(ierr);
		ierr = GetElementEqnIndicesPressure(P_BASIS_FUNCTIONS,(PetscInt*)&p_elnidx[p_nen*e],p_gidx,p_elgidx);CHKERRQ(ierr);
		ierr = MatSetValues( A, P_BASIS_FUNCTIONS,p_elgidx, Q2_NODES_PER_EL_2D*2,u_elgidx_bc, Ae, ADD_VALUES );CHKERRQ(ierr);
	}
	ierr = MatAssemblyBegin(A, MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(A, MAT_FLUSH_ASSEMBLY);CHKERRQ(ierr);
	PetscGetTime(&t1);
//	PetscPrintf(PETSC_COMM_WORLD,"Assemble A21, Q2 = %1.4e (sec)\n",t1-t0);
	
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	/* no bc modifications */
	ierr = MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "Assemble_Stokes_B22_P1"
PetscErrorCode Assemble_Stokes_B22_P1(pTatinCtx user,DM dau,const PetscScalar u[],DM dap,const PetscScalar p[],Mat Bpp)
{	
	PetscErrorCode ierr;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel_u,nel_p,nen_u,nen_p,e,n;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D],el_eta[MAX_QUAD_PNTS];
	PetscScalar Ae[P_BASIS_FUNCTIONS * P_BASIS_FUNCTIONS];
	PetscInt *gidx_u,*gidx_p,elgidx_p[P_BASIS_FUNCTIONS];
	PetscInt nbcs,i;
	const PetscInt *ubcidx;
	Vec diag;
	PetscScalar *LA_diag;
	PetscLogDouble t0,t1;
	GaussPointCoefficientsStokes *gausspoints;
	PetscFunctionBegin;
	
	/* zero entries */
	ierr = MatZeroEntries(Bpp);CHKERRQ(ierr);
	
	/* quadrature */
	//	QuadratureCreateGauss_3pnt_2D(&ngp,gp_xi,gp_weight);
	ngp       = user->Q->ngp;
	gp_xi     = user->Q->xi;
	gp_weight = user->Q->weight;
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA(dau,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetGlobalIndices(dau,0,&gidx_u);CHKERRQ(ierr);
	ierr = DMDAGetGlobalIndices(dap,0,&gidx_p);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel_u,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel_p,&nen_p,&elnidx_p);CHKERRQ(ierr);
	if (nel_u	!= nel_p) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Velocity DA is not compatible with pressure DA\n");
	}
	
	PetscGetTime(&t0);
	for (e=0;e<nel_u;e++) {
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		
		/* initialise element stiffness matrix */
		PetscMemzero( Ae, sizeof(PetscScalar)* P_BASIS_FUNCTIONS * P_BASIS_FUNCTIONS );
		
		/* evaluate the viscosity */
		for (n=0; n<ngp; n++) {
			el_eta[n] = gausspoints[n].eta;
		}
		
		/* form element stiffness matrix */
		AElement_Stokes_B22_P1_2D( Ae, elcoords, el_eta, ngp,gp_xi,gp_weight );
		
		/* insert element matrix into global matrix */
		ierr = GetElementEqnIndicesPressure(nen_p,(PetscInt*)&elnidx_p[nen_p*e],gidx_p,elgidx_p);CHKERRQ(ierr);
		//		printf("[e=%d]: %d %d %d \n", e, elgidx_p[0],elgidx_p[1],elgidx_p[2] );
		
		ierr = MatSetValues( Bpp, P_BASIS_FUNCTIONS,elgidx_p, P_BASIS_FUNCTIONS,elgidx_p, Ae, ADD_VALUES );CHKERRQ(ierr);
	}
	ierr = MatAssemblyBegin(Bpp, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(Bpp, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	PetscGetTime(&t1);
//	PetscPrintf(PETSC_COMM_WORLD,"Assemble B22, P1 = %1.4e (sec)\n",t1-t0);
	
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

/*
 
 Added an augmented lagrangian precondtioner

 B = [ A + (eta/lambda) G.inv(M).D , G        ]
		 [ 0                      (lambda/eta).M  ]
 

 When implemented, I actually assembled
 
 B = [ A + (eta/lambda) G.inv(M).D , G              ]
     [ 0                      inv( (lambda/eta).M)  ]

 this is possible as the mass matrix for the P1 basis can
 be invert at the element level. Be sure to use the preoconditioner
 
   -fieldsplit_p_pc_type mat
 
   -stokes_pc_auglag
  -stokes_pc_auglag_penalty <1.0e-6>
 
 */
#undef __FUNCT__  
#define __FUNCT__ "FormJacobian_Stokes"
PetscErrorCode FormJacobian_Stokes(SNES snes,Vec X,Mat *A,Mat *B,MatStructure *mstr,void *ctx)
{
  pTatinCtx   user = (pTatinCtx)ctx;
  DM                dau,dap;
  DMDALocalInfo     infou,infop;
  PetscScalar       *u,*p;
	IS                *is;
  Vec               Uloc,Ploc;
	PetscBool         is_mffd = PETSC_FALSE;
	PetscBool         use_auglag_pc = PETSC_FALSE;
	PetscScalar       lambda = 1.0e-6;
  PetscErrorCode    ierr;
	
  PetscFunctionBegin;
  ierr = DMCompositeGetEntries(user->pack,&dau,&dap);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dau,&infou);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dap,&infop);CHKERRQ(ierr);
  ierr = DMCompositeGetLocalVectors(user->pack,&Uloc,&Ploc);CHKERRQ(ierr);
	
	ierr = DMCompositeScatter(user->pack,X,Uloc,Ploc);CHKERRQ(ierr);
	ierr = VecGetArray(Uloc,&u);CHKERRQ(ierr);
	ierr = VecGetArray(Ploc,&p);CHKERRQ(ierr);
	ierr = DMCompositeGetGlobalISs(user->pack,&is);CHKERRQ(ierr);
	
	/* Jacobian */
	ierr = pTatin_EvaluateRheologyNonlinearities(user,dau,u,dap,p);CHKERRQ(ierr);

	ierr = PetscTypeCompare((PetscObject)(*A),MATMFFD,&is_mffd);CHKERRQ(ierr);
	if (!is_mffd) {
		Mat Auu,Aup,Apu;
		
		ierr = MatGetSubMatrix(*A,is[0],is[0],MAT_INITIAL_MATRIX,&Auu);CHKERRQ(ierr);
		ierr = MatGetSubMatrix(*A,is[0],is[1],MAT_INITIAL_MATRIX,&Aup);CHKERRQ(ierr);
		ierr = MatGetSubMatrix(*A,is[1],is[0],MAT_INITIAL_MATRIX,&Apu);CHKERRQ(ierr);

		ierr = Assemble_Stokes_A11_Q2(user,dau,u,dap,p,Auu);CHKERRQ(ierr);
		ierr = Assemble_Stokes_A12_Q2(user,dau,u,dap,p,Aup);CHKERRQ(ierr);
		ierr = Assemble_Stokes_A21_Q2(user,dau,u,dap,p,Apu);CHKERRQ(ierr);

		ierr = MatDestroy(&Auu);CHKERRQ(ierr);
		ierr = MatDestroy(&Aup);CHKERRQ(ierr);
		ierr = MatDestroy(&Apu);CHKERRQ(ierr);
	}
	
	ierr = MatAssemblyBegin(*A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd  (*A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	
	/* preconditioner for Jacobian */
	{
		Mat Buu,Bup,Bpu,Bpp;
		PetscBool upper,lower,full;
		
		upper = lower = full = PETSC_FALSE;
		ierr = PetscOptionsGetBool(PETSC_NULL,"-dmcomposite_pc_upper",&upper,PETSC_NULL);CHKERRQ(ierr);
		ierr = PetscOptionsGetBool(PETSC_NULL,"-dmcomposite_pc_lower",&lower,PETSC_NULL);CHKERRQ(ierr);
		ierr = PetscOptionsGetBool(PETSC_NULL,"-dmcomposite_pc_full",&full,PETSC_NULL);CHKERRQ(ierr);
		if (full){ upper = PETSC_TRUE; lower = PETSC_TRUE; }
		
		ierr = MatGetSubMatrix(*B,is[0],is[0],MAT_INITIAL_MATRIX,&Buu);CHKERRQ(ierr);
		ierr = MatGetSubMatrix(*B,is[0],is[1],MAT_INITIAL_MATRIX,&Bup);CHKERRQ(ierr);
		ierr = MatGetSubMatrix(*B,is[1],is[0],MAT_INITIAL_MATRIX,&Bpu);CHKERRQ(ierr);
		ierr = MatGetSubMatrix(*B,is[1],is[1],MAT_INITIAL_MATRIX,&Bpp);CHKERRQ(ierr);
		
		if (upper) { ierr = Assemble_Stokes_A12_Q2(user,dau,u,dap,p,Bup);CHKERRQ(ierr); }
		if (lower) { ierr = Assemble_Stokes_A21_Q2(user,dau,u,dap,p,Bpu);CHKERRQ(ierr); }

		PetscOptionsGetBool(PETSC_NULL,"-stokes_pc_auglag",&use_auglag_pc,PETSC_NULL);
		if (use_auglag_pc==PETSC_TRUE) {
			PetscOptionsGetScalar(PETSC_NULL,"-stokes_pc_auglag_penalty",&lambda,PETSC_NULL);
			ierr = Assemble_Stokes_B11_AugLag_Q2(user,dau,u,dap,p,Buu,lambda);CHKERRQ(ierr);
			ierr = Assemble_Stokes_B22_AugLag_P1(user,dau,u,dap,p,Bpp,lambda);CHKERRQ(ierr);
		} else {
			ierr = Assemble_Stokes_A11_Q2(user,dau,u,dap,p,Buu);CHKERRQ(ierr);
			ierr = Assemble_Stokes_B22_P1(user,dau,u,dap,p,Bpp);CHKERRQ(ierr);
		}
		
		ierr = MatDestroy(&Buu);CHKERRQ(ierr);
		ierr = MatDestroy(&Bup);CHKERRQ(ierr);
		ierr = MatDestroy(&Bpu);CHKERRQ(ierr);
		ierr = MatDestroy(&Bpp);CHKERRQ(ierr);		
  }
  ierr = MatAssemblyBegin(*B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd  (*B,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	
	*mstr = DIFFERENT_NONZERO_PATTERN;
	
	/* clean up */
	ierr = ISDestroy(&is[0]);CHKERRQ(ierr);
	ierr = ISDestroy(&is[1]);CHKERRQ(ierr);
	ierr = PetscFree(is);CHKERRQ(ierr);
	
	ierr = VecRestoreArray(Uloc,&u);CHKERRQ(ierr);
	ierr = VecRestoreArray(Ploc,&p);CHKERRQ(ierr);
	
  ierr = DMCompositeRestoreLocalVectors(user->pack,&Uloc,&Ploc);CHKERRQ(ierr);
	
  PetscFunctionReturn(0);
}

/* dump the quadrature points at each nonlinear iteration */
#undef __FUNCT__  
#define __FUNCT__ "pTatinSNESMonitorGP"
PetscErrorCode pTatinSNESMonitorGP(SNES snes,PetscInt its,PetscReal norm,void *data)
{
	PetscErrorCode ierr;
	pTatinCtx ctx;
	char *prefix;
	Vec X;
	
	ctx = (pTatinCtx)data;
	ierr = SNESGetSolution(snes,&X);CHKERRQ(ierr);
	
	asprintf(&prefix,"pim-snes%1.4d",its);
	ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,prefix);CHKERRQ(ierr);
	free(prefix);
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatinSNESICMonitorGP"
PetscErrorCode pTatinSNESICMonitorGP(SNES snes,PetscInt its,PetscReal norm,void *data)
{
	PetscErrorCode ierr;
	pTatinCtx ctx;
	char *prefix;
	Vec X;
	
	ctx = (pTatinCtx)data;
	ierr = SNESGetSolution(snes,&X);CHKERRQ(ierr);
	
	asprintf(&prefix,"pim-ic%1.4d_snes%1.4d",ctx->continuation_m,its);
	ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,prefix);CHKERRQ(ierr);
	free(prefix);
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatinSNESMonitorUPSplits"
PetscErrorCode pTatinSNESMonitorUPSplits(SNES snes,PetscInt its,PetscReal norm,void *data)
{
	PetscErrorCode ierr;
	pTatinCtx ctx;
	Vec X,Xu,Xp,F,Fu,Fp;
	PetscReal nFu,nFp,nXu,nXp;
	PetscReal nFuI,nFpI,nXuI,nXpI;
	
	PetscFunctionBegin;
	ctx = (pTatinCtx)data;
	ierr = SNESGetSolution(snes,&X);CHKERRQ(ierr);
	ierr = SNESGetFunction(snes,&F,PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);

	ierr = DMCompositeGetAccess(ctx->pack,F,&Fu,&Fp);CHKERRQ(ierr);
	ierr = DMCompositeGetAccess(ctx->pack,X,&Xu,&Xp);CHKERRQ(ierr);
	
	ierr = VecNorm(Fu,NORM_2,&nFu);CHKERRQ(ierr);
	ierr = VecNorm(Fp,NORM_2,&nFp);CHKERRQ(ierr);
	ierr = VecNorm(Xu,NORM_2,&nXu);CHKERRQ(ierr);
	ierr = VecNorm(Xp,NORM_2,&nXp);CHKERRQ(ierr);

	ierr = VecNorm(Fu,NORM_INFINITY,&nFuI);CHKERRQ(ierr);
	ierr = VecNorm(Fp,NORM_INFINITY,&nFpI);CHKERRQ(ierr);
	ierr = VecNorm(Xu,NORM_INFINITY,&nXuI);CHKERRQ(ierr);
	ierr = VecNorm(Xp,NORM_INFINITY,&nXpI);CHKERRQ(ierr);
	
	PetscPrintf(PETSC_COMM_WORLD,"%3D SNES Component norms: F_2 [ %1.4e, %1.4e ]; F_inf [ %1.4e, %1.4e ]; X_2 [ %1.4e, %1.4e ]; X_inf [ %1.4e, %1.4e ] \n",
							its,nFu,nFp,nFuI,nFpI,nXu,nXp,nXuI,nXpI);

	ierr = DMCompositeRestoreAccess(ctx->pack,X,&Xu,&Xp);CHKERRQ(ierr);
	ierr = DMCompositeRestoreAccess(ctx->pack,F,&Fu,&Fp);CHKERRQ(ierr);
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatinSNESMonitorUPSplits_2"
PetscErrorCode pTatinSNESMonitorUPSplits_2(SNES snes,PetscInt its,PetscReal norm,void *data)
{
	PetscErrorCode ierr;
	pTatinCtx ctx;
	Vec X,Xu,Xp,F,Fu,Fp,deltaX,deltaXu,deltaXp;
	PetscReal nFu[2],nFp[3],nXu[2],nXp[3],ndelXu[2],ndelXp[3];
	
	PetscFunctionBegin;
	ctx = (pTatinCtx)data;
	ierr = SNESGetSolution(snes,&X);CHKERRQ(ierr);
	ierr = SNESGetFunction(snes,&F,PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
	ierr = SNESGetSolutionUpdate(snes,&deltaX);CHKERRQ(ierr);
	
	ierr = DMCompositeGetAccess(ctx->pack,F,&Fu,&Fp);CHKERRQ(ierr);
	ierr = DMCompositeGetAccess(ctx->pack,X,&Xu,&Xp);CHKERRQ(ierr);
	ierr = DMCompositeGetAccess(ctx->pack,deltaX,&deltaXu,&deltaXp);CHKERRQ(ierr);
	
	ierr = VecStrideNormAll(Fu,NORM_2,nFu);CHKERRQ(ierr);
	ierr = VecStrideNormAll(Xu,NORM_2,nXu);CHKERRQ(ierr);

	ierr = VecStrideNormAll(Fp,NORM_2,nFp);CHKERRQ(ierr);
	ierr = VecStrideNormAll(Xp,NORM_2,nXp);CHKERRQ(ierr);

	ierr = VecStrideNormAll(deltaXu,NORM_2,ndelXu);CHKERRQ(ierr);
	ierr = VecStrideNormAll(deltaXp,NORM_2,ndelXp);CHKERRQ(ierr);
	
	PetscPrintf(PETSC_COMM_WORLD,"%3D SNES Component norms: F_2 [ %1.4e, %1.4e : %1.4e, %1.4e, %1.4e ]; \n",
							its,nFu[0],nFu[1],nFp[0],nFp[1],nFp[2] );
	PetscPrintf(PETSC_COMM_WORLD,"                          X_2 [ %1.4e, %1.4e : %1.4e, %1.4e, %1.4e ]; \n",
							nXu[0],nXu[1],nXp[0],nXp[1],nXp[2] );
	PetscPrintf(PETSC_COMM_WORLD,"                          deltaX_2 [ %1.4e, %1.4e : %1.4e, %1.4e, %1.4e ]\n",
							ndelXu[0],ndelXu[1],ndelXp[0],ndelXp[1],ndelXp[2] );

	if (its!=0) {
		PetscPrintf(PETSC_COMM_WORLD,"                          (dX/X)_2 [ %1.4e, %1.4e : %1.4e, %1.4e, %1.4e ]\n",
								ndelXu[0]/nXu[0],ndelXu[1]/nXu[1],ndelXp[0]/nXp[0],ndelXp[1]/nXp[1],ndelXp[2]/nXp[2] );
		PetscPrintf(PETSC_COMM_WORLD,"                          (X/dX)_2 [ %1.4e, %1.4e : %1.4e, %1.4e, %1.4e ]\n",
								nXu[0]/ndelXu[0],nXu[1]/ndelXu[1],nXp[0]/ndelXp[0],nXp[1]/ndelXp[1],nXp[2]/ndelXp[2] );
	}
	
	ierr = DMCompositeRestoreAccess(ctx->pack,deltaX,&deltaXu,&deltaXp);CHKERRQ(ierr);
	ierr = DMCompositeRestoreAccess(ctx->pack,X,&Xu,&Xp);CHKERRQ(ierr);
	ierr = DMCompositeRestoreAccess(ctx->pack,F,&Fu,&Fp);CHKERRQ(ierr);
	
  PetscFunctionReturn(0);
}

/* -snes_cvg_cmpsplit -snes_cmpsplit_xtol */
typedef struct {
	PetscInt ncomponents;
	PetscReal c_fa_tol,c_fr_tol,c_x_tol; /* |f_i| < c_fa_tol; |f_i| < c_fr_tol |f0_i|; |delx_i| < c_x_tol |x_i| */
	PetscReal nFu0[2],nFp0[3];
	pTatinCtx user;
	PetscBool deactivate_monitor;
	PetscBool ignore_allu;
	PetscBool ignore_allp;
	PetscBool ignore_dpdxdpdy;
	
} pTatinConvergenceCtx;

#undef __FUNCT__  
#define __FUNCT__ "pTatinSNESConvergenceTest_UiPi"
PetscErrorCode pTatinSNESConvergenceTest_UiPi(SNES snes,PetscInt its,PetscReal xnorm,PetscReal pnorm,PetscReal fnorm,SNESConvergedReason *reason,void *data)
{
	PetscErrorCode ierr;
	Vec X,Xu,Xp,F,Fu,Fp,deltaX,deltaXu,deltaXp;
	PetscReal nFu[2],nFp[3],nXu[2],nXp[3],ndelXu[2],ndelXp[3];
	pTatinConvergenceCtx *ctx;
	DM stokes_pack;
	PetscInt F_abs,F_rel,X_abs;
	PetscInt F_abs_max,F_rel_max,X_abs_max;
	
	PetscFunctionBegin;
	ctx = (pTatinConvergenceCtx*)data;
	stokes_pack = ctx->user->pack;
	
	ierr = SNESGetSolution(snes,&X);CHKERRQ(ierr);
	ierr = SNESGetFunction(snes,&F,PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
	ierr = SNESGetSolutionUpdate(snes,&deltaX);CHKERRQ(ierr);
	
	ierr = DMCompositeGetAccess(stokes_pack,F,&Fu,&Fp);CHKERRQ(ierr);
	ierr = DMCompositeGetAccess(stokes_pack,X,&Xu,&Xp);CHKERRQ(ierr);
	ierr = DMCompositeGetAccess(stokes_pack,deltaX,&deltaXu,&deltaXp);CHKERRQ(ierr);
	
	if (ctx->ignore_allu==PETSC_FALSE) {
		ierr = VecStrideNormAll(Fu,NORM_2,nFu);CHKERRQ(ierr);
		ierr = VecStrideNormAll(Xu,NORM_2,nXu);CHKERRQ(ierr);
		ierr = VecStrideNormAll(deltaXu,NORM_2,ndelXu);CHKERRQ(ierr);
	}
	
	if (ctx->ignore_allp==PETSC_FALSE) {
		if (ctx->ignore_dpdxdpdy==PETSC_FALSE) {
			ierr = VecStrideNormAll(Fp,NORM_2,nFp);CHKERRQ(ierr);
			ierr = VecStrideNormAll(Xp,NORM_2,nXp);CHKERRQ(ierr);
			ierr = VecStrideNormAll(deltaXp,NORM_2,ndelXp);CHKERRQ(ierr);
		}else {
			ierr = VecStrideNorm(Fp,0,NORM_2,&nFp[0]);CHKERRQ(ierr);
			ierr = VecStrideNorm(Xp,0,NORM_2,&nXp[0]);CHKERRQ(ierr);
			ierr = VecStrideNorm(deltaXp,0,NORM_2,&ndelXp[0]);CHKERRQ(ierr);
		}
	}
	
	ierr = DMCompositeRestoreAccess(stokes_pack,deltaX,&deltaXu,&deltaXp);CHKERRQ(ierr);
	ierr = DMCompositeRestoreAccess(stokes_pack,X,&Xu,&Xp);CHKERRQ(ierr);
	ierr = DMCompositeRestoreAccess(stokes_pack,F,&Fu,&Fp);CHKERRQ(ierr);
	
	if (its==0) {
		ctx->nFu0[0] = nFu[0];
		ctx->nFu0[1] = nFu[1];
		
//		ctx->nXu0[0] = nXu[0];
//		ctx->nXu0[1] = nXu[1];

		ctx->nFp0[0] = nFp[0];
		ctx->nFp0[1] = nFp[1];
		ctx->nFp0[2] = nFp[2];

//		ctx->nXp0[0] = nXp[0];
//		ctx->nXp0[1] = nXp[1];
//		ctx->nXp0[2] = nXp[2];
	}

	/* F absolute */
	F_abs = 0; F_abs_max = 0;
	if (ctx->ignore_allu==PETSC_FALSE) {
		if (nFu[0]<ctx->c_fa_tol) { F_abs++; }
		if (nFu[1]<ctx->c_fa_tol) { F_abs++; }
		F_abs_max = F_abs_max + 2;
	}
	if (ctx->ignore_allp==PETSC_FALSE) {
		if (ctx->ignore_dpdxdpdy==PETSC_FALSE) {
			if (nFp[0]<ctx->c_fa_tol) { F_abs++; }
			if (nFp[1]<ctx->c_fa_tol) { F_abs++; }
			if (nFp[2]<ctx->c_fa_tol) { F_abs++; }
			F_abs_max = F_abs_max + 3;
		} else {
			if (nFp[0]<ctx->c_fa_tol) { F_abs++; }
			F_abs_max = F_abs_max + 1;
		}
	}
	
	F_rel = 0; F_rel_max = 0;
	if (ctx->ignore_allu==PETSC_FALSE) {
		if (nFu[0]<ctx->c_fr_tol*ctx->nFu0[0]) { F_rel++; }
		if (nFu[1]<ctx->c_fr_tol*ctx->nFu0[1]) { F_rel++; }
		F_rel_max = F_rel_max+ 2;
	}
	if (ctx->ignore_allp==PETSC_FALSE) {
		if (ctx->ignore_dpdxdpdy==PETSC_FALSE) {
			if (nFp[0]<ctx->c_fr_tol*ctx->nFp0[0]) { F_rel++; }
			if (nFp[1]<ctx->c_fr_tol*ctx->nFp0[1]) { F_rel++; }
			if (nFp[2]<ctx->c_fr_tol*ctx->nFp0[2]) { F_rel++; }
			F_rel_max = F_rel_max+ 3;
		} else {
			if (nFp[0]<ctx->c_fr_tol*ctx->nFp0[0]) { F_rel++; }
			F_rel_max = F_rel_max+ 1;
		}
	}

	X_abs = 0; X_abs_max = 0;
	if (ctx->ignore_allu==PETSC_FALSE) {
		if (ndelXu[0]<ctx->c_x_tol*nXu[0]) { X_abs++; }
		if (ndelXu[1]<ctx->c_x_tol*nXu[1]) { X_abs++; }
		X_abs_max = X_abs_max + 2;
	}
	if (ctx->ignore_allp==PETSC_FALSE) {
		if (ctx->ignore_dpdxdpdy==PETSC_FALSE) {
			if (ndelXp[0]<ctx->c_x_tol*nXp[0]) { X_abs++; }
			if (ndelXp[1]<ctx->c_x_tol*nXp[1]) { X_abs++; }
			if (ndelXp[2]<ctx->c_x_tol*nXp[2]) { X_abs++; }
			X_abs_max = X_abs_max + 3;
		} else {
			if (ndelXp[0]<ctx->c_x_tol*nXp[0]) { X_abs++; }
			X_abs_max = X_abs_max + 1;
		}
	}
	
	*reason = SNES_CONVERGED_ITERATING;
	
	if (ctx->ignore_allu==PETSC_FALSE) {
		if (nFu[0] != nFu[0]) { *reason = SNES_DIVERGED_FNORM_NAN; }
		if (nFu[1] != nFu[1]) { *reason = SNES_DIVERGED_FNORM_NAN; }
	}
	if (ctx->ignore_allp==PETSC_FALSE) {
		if (ctx->ignore_dpdxdpdy==PETSC_FALSE) {
			if (nFp[0] != nFp[0]) { *reason = SNES_DIVERGED_FNORM_NAN; }
			if (nFp[1] != nFp[1]) { *reason = SNES_DIVERGED_FNORM_NAN; }
			if (nFp[2] != nFp[2]) { *reason = SNES_DIVERGED_FNORM_NAN; }
		} else {
			if (nFp[0] != nFp[0]) { *reason = SNES_DIVERGED_FNORM_NAN; }
		}
	}
		
	if (F_abs==F_abs_max) {
		*reason = SNES_CONVERGED_FNORM_ABS;
	} else if (F_rel==F_rel_max) {
		*reason = SNES_CONVERGED_FNORM_RELATIVE;
	} else if ( (X_abs==X_abs_max) && (its!=0) ) {
		*reason = SNES_CONVERGED_PNORM_RELATIVE;
	}

	if ( (ctx->ignore_allu==PETSC_FALSE) && (ctx->ignore_allp==PETSC_FALSE) && (ctx->ignore_dpdxdpdy==PETSC_FALSE) ) {
	
		PetscPrintf(PETSC_COMM_WORLD,"____________________________________________________________________________________________________________________________________\n");
		PetscPrintf(PETSC_COMM_WORLD,"  %3D ptatinSNEScvg Component norms: Fa_2 [ (%1.4e, %1.4e) (%1.4e, %1.4e, %1.4e) ]; %D+; atol = %1.4e\n",
								its,nFu[0],nFu[1],nFp[0],nFp[1],nFp[2], F_abs, ctx->c_fa_tol );
		PetscPrintf(PETSC_COMM_WORLD,"                                     Fr_2 [ (%1.4e, %1.4e) (%1.4e, %1.4e, %1.4e) ]; %D+; rtol = %1.4e\n",
								nFu[0]/ctx->nFu0[0],nFu[1]/ctx->nFu0[1], nFp[0]/ctx->nFp0[0],nFp[1]/ctx->nFp0[1],nFp[2]/ctx->nFp0[2], F_rel, ctx->c_fr_tol );
		if (its!=0) {
			PetscPrintf(PETSC_COMM_WORLD,"                                     Xr_2 [ (%1.4e, %1.4e) (%1.4e, %1.4e, %1.4e) ]; %D+; xtol = %1.4e\n",
									 ndelXu[0]/nXu[0],ndelXu[1]/nXu[1],ndelXp[0]/nXp[0],ndelXp[1]/nXp[1],ndelXp[2]/nXp[2], X_abs, ctx->c_x_tol );
		}
	} else {
		
		PetscPrintf(PETSC_COMM_WORLD,"____________________________________________________________________________________________________________________________________\n");
		PetscPrintf(PETSC_COMM_WORLD,"  %3D ptatinSNEScvg Component norms: Fa_2 ", its );

		if (ctx->ignore_allu==PETSC_FALSE) {
			PetscPrintf(PETSC_COMM_WORLD,"  u:(%1.4e, %1.4e) ",nFu[0],nFu[1] );
		}
		if (ctx->ignore_allp==PETSC_FALSE) {
			if (ctx->ignore_dpdxdpdy==PETSC_FALSE) {
				PetscPrintf(PETSC_COMM_WORLD,"  p:(%1.4e, %1.4e, %1.4e) ",nFp[0],nFp[1],nFp[2] );
			} else {
				PetscPrintf(PETSC_COMM_WORLD,"  p:(%1.4e) ",nFp[0] );
			}
		}
		PetscPrintf(PETSC_COMM_WORLD,"  %D+/%D; atol = %1.4e \n", F_abs, F_abs_max, ctx->c_fa_tol );
		
	
		PetscPrintf(PETSC_COMM_WORLD,"                                     Fr_2 ");
		if (ctx->ignore_allu==PETSC_FALSE) {
			PetscPrintf(PETSC_COMM_WORLD,"  u:(%1.4e, %1.4e) ",nFu[0]/ctx->nFu0[0],nFu[1]/ctx->nFu0[1] );
		}
		if (ctx->ignore_allp==PETSC_FALSE) {
			if (ctx->ignore_dpdxdpdy==PETSC_FALSE) {
				PetscPrintf(PETSC_COMM_WORLD,"  p:(%1.4e, %1.4e, %1.4e) ",nFp[0]/ctx->nFp0[0],nFp[1]/ctx->nFp0[1],nFp[2]/ctx->nFp0[2] );
			} else {
				PetscPrintf(PETSC_COMM_WORLD,"  p:(%1.4e) ",nFp[0]/ctx->nFp0[0] );
			}
		}
		PetscPrintf(PETSC_COMM_WORLD,"  %D+/%D; rtol = %1.4e \n", F_rel, F_rel_max, ctx->c_fr_tol );
		
		if (its!=0) {
			PetscPrintf(PETSC_COMM_WORLD,"                                     Xr_2 ");
			if (ctx->ignore_allu==PETSC_FALSE) {
				PetscPrintf(PETSC_COMM_WORLD,"  u:(%1.4e, %1.4e) ",ndelXu[0]/nXu[0],ndelXu[1]/nXu[1] );
			}
			if (ctx->ignore_allp==PETSC_FALSE) {
				if (ctx->ignore_dpdxdpdy==PETSC_FALSE) {
					PetscPrintf(PETSC_COMM_WORLD,"  p:(%1.4e, %1.4e, %1.4e) ",ndelXp[0]/nXp[0],ndelXp[1]/nXp[1],ndelXp[2]/nXp[2] );
				} else {
					PetscPrintf(PETSC_COMM_WORLD,"  p:(%1.4e) ",ndelXp[0]/nXp[0] );
				}
			}
			PetscPrintf(PETSC_COMM_WORLD,"  %D+/%D; rtol = %1.4e \n", X_abs, X_abs_max, ctx->c_x_tol );
		}		
	}
	
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatinKSPMonitorStokesBlocks"
PetscErrorCode pTatinKSPMonitorStokesBlocks(KSP ksp,PetscInt n,PetscReal rnorm,void *data)
{
	PetscErrorCode ierr;
	pTatinCtx ctx;
	PetscReal norms[3];
	Vec X,Xu,Xp,v,w;
	Mat A;
	
	PetscFunctionBegin;
	ctx = (pTatinCtx)data;
	ierr = KSPGetOperators(ksp,&A,0,0);CHKERRQ(ierr);
	ierr = MatGetVecs(A,&w,&v);CHKERRQ(ierr);
	
	ierr = KSPBuildResidual(ksp,v,w,&X);CHKERRQ(ierr);
	ierr = DMCompositeGetAccess(ctx->pack,X,&Xu,&Xp);CHKERRQ(ierr);
	
	ierr = VecStrideNorm(Xu,0,NORM_2,&norms[0]);CHKERRQ(ierr);
	ierr = VecStrideNorm(Xu,1,NORM_2,&norms[1]);CHKERRQ(ierr);
	ierr = VecNorm(Xp,NORM_2,&norms[2]);CHKERRQ(ierr);
	
	ierr = DMCompositeRestoreAccess(ctx->pack,X,&Xu,&Xp);CHKERRQ(ierr);
	ierr = VecDestroy(&v);CHKERRQ(ierr);
	ierr = VecDestroy(&w);CHKERRQ(ierr);
	
	PetscPrintf(PETSC_COMM_WORLD,"%3D KSP Component U,V,P residual norm [ %1.12e, %1.12e, %1.12e ]\n",n,norms[0],norms[1],norms[2]);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatinSetSNESMonitor"
PetscErrorCode pTatinSetSNESMonitor(SNES snes,pTatinCtx ctx)
{
	PetscBool flg,val= PETSC_FALSE;
	PetscErrorCode ierr;


  val= PETSC_FALSE;
	ierr = PetscOptionsGetBool(PETSC_NULL,"-ptatin_snes_monitor_gp",&val,&flg);CHKERRQ(ierr);
	if (val) {
		PetscPrintf(PETSC_COMM_WORLD,"Activating monitor: pTatinSNESMonitorGP \n");
		ierr = SNESMonitorSet(snes,pTatinSNESMonitorGP,(void*)ctx,PETSC_NULL);CHKERRQ(ierr);
	}

  val= PETSC_FALSE;
	ierr = PetscOptionsGetBool(PETSC_NULL,"-ptatin_ksp_monitor_up",&val,&flg);CHKERRQ(ierr);
	if (val) {
		KSP ksp;
		
		PetscPrintf(PETSC_COMM_WORLD,"Activating monitor: pTatinKSPMonitorStokesBlocks \n");
		ierr = SNESGetKSP(snes,&ksp);CHKERRQ(ierr);
		ierr = KSPMonitorSet(ksp,pTatinKSPMonitorStokesBlocks,(void*)ctx,PETSC_NULL);CHKERRQ(ierr);
	}
	
  val= PETSC_FALSE;
	ierr = PetscOptionsGetBool(PETSC_NULL,"-ptatin_snes_monitor_up",&val,&flg);CHKERRQ(ierr);
	if (val) {
		PetscPrintf(PETSC_COMM_WORLD,"Activating monitor: pTatinSNESMonitorUPSplits \n");
		ierr = SNESMonitorSet(snes,pTatinSNESMonitorUPSplits,(void*)ctx,PETSC_NULL);CHKERRQ(ierr);
	}

  val= PETSC_FALSE;
	ierr = PetscOptionsGetBool(PETSC_NULL,"-ptatin_snes_monitor_up_2",&val,&flg);CHKERRQ(ierr);
	if (val) {
		PetscPrintf(PETSC_COMM_WORLD,"Activating monitor: pTatinSNESMonitorUPSplits_2 \n");
		ierr = SNESMonitorSet(snes,pTatinSNESMonitorUPSplits_2,(void*)ctx,PETSC_NULL);CHKERRQ(ierr);
	}

	
	/* convergence test */
  val= PETSC_FALSE;
	ierr = PetscOptionsGetBool(PETSC_NULL,"-ptatin_snes_cvg_cmpsplit",&val,&flg);CHKERRQ(ierr);
	if (val) {
		pTatinConvergenceCtx *cvgctx;
		
		PetscPrintf(PETSC_COMM_WORLD,"Activating monitor: pTatinSNESConvergenceTest_UiPi \n");

		ierr = PetscMalloc(sizeof(pTatinConvergenceCtx),&cvgctx);CHKERRQ(ierr);
		cvgctx->user = ctx;
		cvgctx->ncomponents = 5; /* u,v,p,dpdx,dpdy */
		cvgctx->c_fa_tol = 1.0e-10;
		cvgctx->c_fr_tol = 1.0e-6;
		cvgctx->c_x_tol  = 1.0e-4; 

		cvgctx->ignore_allu = PETSC_FALSE;
		cvgctx->ignore_allp = PETSC_FALSE;
		cvgctx->ignore_dpdxdpdy = PETSC_FALSE;
		
		ierr = PetscOptionsGetReal(PETSC_NULL,"-snes_cmpsplit_atol",&cvgctx->c_fa_tol,0);CHKERRQ(ierr);
		ierr = PetscOptionsGetReal(PETSC_NULL,"-snes_cmpsplit_rtol",&cvgctx->c_fr_tol,0);CHKERRQ(ierr);
		ierr = PetscOptionsGetReal(PETSC_NULL,"-snes_cmpsplit_xtol",&cvgctx->c_x_tol,0);CHKERRQ(ierr);
		
		ierr = PetscOptionsGetBool(PETSC_NULL,"-snes_cmpsplit_ignore_u",&cvgctx->ignore_allu,&flg);CHKERRQ(ierr);
		ierr = PetscOptionsGetBool(PETSC_NULL,"-snes_cmpsplit_ignore_p",&cvgctx->ignore_allp,&flg);CHKERRQ(ierr);
		ierr = PetscOptionsGetBool(PETSC_NULL,"-snes_cmpsplit_ignore_gradp",&cvgctx->ignore_dpdxdpdy,&flg);CHKERRQ(ierr);
		
		ierr = SNESSetConvergenceTest(snes,pTatinSNESConvergenceTest_UiPi,(void*)cvgctx,PETSC_NULL);CHKERRQ(ierr);
		
	}
  PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatinSetSNESContMonitor"
PetscErrorCode pTatinSetSNESContMonitor(SNES snes,pTatinCtx ctx)
{
	PetscBool flg,val= PETSC_FALSE;
	PetscErrorCode ierr;
  
  val= PETSC_FALSE;
	ierr = PetscOptionsGetBool(PETSC_NULL,"-ptatin_ic_snes_monitor_gp",&val,&flg);CHKERRQ(ierr);
	if (val) {
		PetscPrintf(PETSC_COMM_WORLD,"Activating monitor: pTatinSNESICMonitorGP \n");
		ierr = SNESMonitorSet(snes,pTatinSNESICMonitorGP,(void*)ctx,PETSC_NULL);CHKERRQ(ierr);
	}
  
  val= PETSC_FALSE;
	ierr = PetscOptionsGetBool(PETSC_NULL,"-ptatin_ic_ksp_monitor_up",&val,&flg);CHKERRQ(ierr);
	if (val) {
		KSP ksp;
		
		PetscPrintf(PETSC_COMM_WORLD,"Activating monitor: pTatinKSPMonitorStokesBlocks \n");
		ierr = SNESGetKSP(snes,&ksp);CHKERRQ(ierr);
		ierr = KSPMonitorSet(ksp,pTatinKSPMonitorStokesBlocks,(void*)ctx,PETSC_NULL);CHKERRQ(ierr);
	}
	
  val= PETSC_FALSE;
	ierr = PetscOptionsGetBool(PETSC_NULL,"-ptatin_ic_snes_monitor_up",&val,&flg);CHKERRQ(ierr);
	if (val) {
		PetscPrintf(PETSC_COMM_WORLD,"Activating monitor: pTatinSNESMonitorUPSplits \n");
		ierr = SNESMonitorSet(snes,pTatinSNESMonitorUPSplits,(void*)ctx,PETSC_NULL);CHKERRQ(ierr);
	}
	
  PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "pTatin2dSolverStatistics"
PetscErrorCode pTatin2dSolverStatistics(pTatinCtx ctx,SNES snes,Vec X)
{
	Vec velocity,pressure;
	Vec Residual,Rmom,Rcont;
	PetscReal nrmU,nrmP,nrm;
	PetscInt loc;
	PetscReal val;
	Vec Div;
	PetscScalar sum;
	Mat A21;
	DM multipys_pack;
	KSP ksp;
	Mat A;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	multipys_pack = ctx->pack;	
	
	/* statistics */
	ierr = DMCompositeGetAccess(multipys_pack,X,&velocity,&pressure);CHKERRQ(ierr);
	ierr = VecNorm(velocity,NORM_INFINITY,&nrmU);CHKERRQ(ierr);
	PetscPrintf(PETSC_COMM_WORLD,"|u|_inf = %1.4e\n",nrmU);
	ierr = VecNorm(pressure,NORM_INFINITY,&nrmP);CHKERRQ(ierr);
	PetscPrintf(PETSC_COMM_WORLD,"|p|_inf = %1.4e\n",nrmP);
	ierr = DMCompositeRestoreAccess(multipys_pack,X,&velocity,&pressure);CHKERRQ(ierr);
	
	ierr = VecDuplicate(X,&Residual);CHKERRQ(ierr);
	ierr = SNESComputeFunction(snes,X,Residual);CHKERRQ(ierr);
	ierr = VecNorm(Residual,NORM_INFINITY,&nrm);CHKERRQ(ierr);
	PetscPrintf(PETSC_COMM_WORLD,"|F|_inf = %1.4e\n",nrm);
	
	ierr = DMCompositeGetAccess(multipys_pack,Residual,&velocity,&pressure);CHKERRQ(ierr);
	ierr = VecNorm(velocity,NORM_INFINITY,&nrmU);CHKERRQ(ierr);
	PetscPrintf(PETSC_COMM_WORLD,"|Fu|_inf = %1.4e\n",nrmU);
	ierr = VecNorm(pressure,NORM_INFINITY,&nrmP);CHKERRQ(ierr);
	PetscPrintf(PETSC_COMM_WORLD,"|Fp|_inf = %1.4e\n",nrmP);
	ierr = DMCompositeRestoreAccess(multipys_pack,Residual,&velocity,&pressure);CHKERRQ(ierr);
	
	ierr = VecDestroy(&Residual);CHKERRQ(ierr);
	
	ierr = SNESGetKSP(snes,&ksp);CHKERRQ(ierr);
	ierr = KSPGetOperators(ksp,&A,0,0);CHKERRQ(ierr);
	if (!ctx->use_mf_stokes) {
		ierr = DMCompositeGetAccess(multipys_pack,X,&velocity,&pressure);CHKERRQ(ierr);
		ierr = MatNestGetSubMat(A,1,0,&A21);CHKERRQ(ierr);
		ierr = VecDuplicate(pressure,&Rcont);CHKERRQ(ierr);
		ierr = MatMult(A21,velocity,Rcont);CHKERRQ(ierr);
		ierr = VecNorm(Rcont,NORM_INFINITY,&nrmP);CHKERRQ(ierr);
		PetscPrintf(PETSC_COMM_WORLD,"|A21.u|_inf = %1.4e\n",nrmP);
		ierr = VecDestroy(&Rcont);CHKERRQ(ierr);
		ierr = DMCompositeRestoreAccess(multipys_pack,X,&velocity,&pressure);CHKERRQ(ierr);
	}
	
	ierr = DMCompositeGetAccess(multipys_pack,X,&velocity,&pressure);CHKERRQ(ierr);
	ierr = VecDuplicate(pressure,&Div);CHKERRQ(ierr);
	
	if (!ctx->use_mf_stokes) {		
		ierr = MatNestGetSubMat(A,1,0,&A21);CHKERRQ(ierr);
		ierr = MatMult(A21,velocity,Div);CHKERRQ(ierr);

		PetscPrintf( PETSC_COMM_WORLD, "------------------------------------------\n" );
		PetscPrintf( PETSC_COMM_WORLD, "  divergence: \n" );
		VecMin( Div, &loc, &val );
		PetscPrintf( PETSC_COMM_WORLD, "    Div_min = %12.12e \n", val );
		VecMax( Div, &loc, &val );
		PetscPrintf( PETSC_COMM_WORLD, "    Div_max = %12.12e \n", val );
		VecNorm( Div, NORM_2, &val );
		PetscPrintf( PETSC_COMM_WORLD, "    |Div|_2 = %12.12e \n", val );
		VecNorm( Div, NORM_1, &val );
		PetscPrintf( PETSC_COMM_WORLD, "    |Div|_1 = %12.12e \n", val );
	}	
	
	PetscPrintf( PETSC_COMM_WORLD, "  velocity: \n" );
	VecMin( velocity, &loc, &val );
	PetscPrintf( PETSC_COMM_WORLD, "    u_min   = %12.12e \n", val );
	VecMax( velocity, &loc, &val );
	PetscPrintf( PETSC_COMM_WORLD, "    u_max   = %12.12e \n", val );
	VecNorm( velocity, NORM_2, &val );
	PetscPrintf( PETSC_COMM_WORLD, "    |u|_2   = %12.12e \n", val );
	VecNorm( velocity, NORM_1, &val );
	PetscPrintf( PETSC_COMM_WORLD, "    |u|_1   = %12.12e \n", val );
	
	PetscPrintf( PETSC_COMM_WORLD, "  pressure: \n" );
	VecMin( pressure, &loc, &val );
	PetscPrintf( PETSC_COMM_WORLD, "    p_min   = %12.12e \n", val );
	VecMax( pressure, &loc, &val );
	PetscPrintf( PETSC_COMM_WORLD, "    p_max   = %12.12e \n", val );
	VecNorm( pressure, NORM_2, &val );
	PetscPrintf( PETSC_COMM_WORLD, "    |p|_2   = %12.12e \n", val );
	VecNorm( pressure, NORM_1, &val );
	PetscPrintf( PETSC_COMM_WORLD, "    |p|_1   = %12.12e \n", val );
	VecSum( pressure, &sum );
	PetscPrintf( PETSC_COMM_WORLD, "    sum(p)   = %12.12e \n", sum );
	PetscPrintf( PETSC_COMM_WORLD, "------------------------------------------\n" );
	
	ierr = DMCompositeRestoreAccess(multipys_pack,X,&velocity,&pressure);CHKERRQ(ierr);
	ierr = VecDestroy(&Div);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2dCreateQuadratureStokes"
PetscErrorCode pTatin2dCreateQuadratureStokes(pTatinCtx ctx)
{
	DM dav;
	PetscInt nel,nen,e;
	const PetscInt *elnidx;
	QuadratureStokes Q;
	QuadratureStokesRule qrule;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	dav = ctx->dav;
	if (!dav) { SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"dav must be set"); }
	
	ierr = DMDAGetElements_pTatin(dav,&nel,&nen,&elnidx);CHKERRQ(ierr);
	qrule = QRule_GaussLegendre3;
	ierr = PetscOptionsGetInt(PETSC_NULL,"-quadrature_rule",(PetscInt*)&qrule,0);CHKERRQ(ierr);
	ierr = QuadratureStokesCreate(qrule,nel,&Q);CHKERRQ(ierr);
	if (P_BASIS_FUNCTIONS==1) { QuadratureStokesBasisSetUp(Q,Basis_Q2,Basis_P0);CHKERRQ(ierr); }
	else {                      QuadratureStokesBasisSetUp(Q,Basis_Q2,Basis_P1L);CHKERRQ(ierr); }
	ierr = QuadratureStokesCoordinateSetUp(Q,dav);CHKERRQ(ierr);

	ctx->Q = Q;
	
	/* surface quadrature */
	for (e=0; e<QUAD_EDGES; e++) {
		ierr = SurfaceQuadratureStokesCreate(dav,e,&ctx->surfQ[e]);CHKERRQ(ierr);
	}
	for (e=0; e<QUAD_EDGES; e++) {
		ierr = SurfaceQuadratureStokesGeometrySetUp(ctx->surfQ[e],dav);CHKERRQ(ierr);
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2dCreateBoundaList"
PetscErrorCode pTatin2dCreateBoundaList(pTatinCtx ctx)
{
	DM dav;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	/* vel bc's */
	dav = ctx->dav;
	if (!dav) { SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"dav must be set"); }
	
	ierr = DMDABCListCreate(dav,&ctx->u_bclist);CHKERRQ(ierr);
	/* pressure bc's */
	ctx->p_bclist = PETSC_NULL;
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2dCreateQ2PmMesh"
PetscErrorCode pTatin2dCreateQ2PmMesh(pTatinCtx ctx)
{
	DM dav,dap,multipys_pack;
	PetscInt vbasis_dofs;
	PetscInt pbasis_dofs;
	const PetscInt *lxp,*lyp;
	PetscInt MX,MY,p,Mp,Np,*lxv,*lyv,i;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	MX = ctx->mx;
	MY = ctx->my;
	
	/* pressure */
	pbasis_dofs = P_BASIS_FUNCTIONS;
	ierr = DMDACreate2d( PETSC_COMM_WORLD, DMDA_BOUNDARY_NONE,DMDA_BOUNDARY_NONE, DMDA_STENCIL_BOX, MX,MY, PETSC_DECIDE,PETSC_DECIDE, pbasis_dofs,0, 0,0, &dap );CHKERRQ(ierr);
	ierr = DMDASetElementType_P1(dap);CHKERRQ(ierr);
	ierr = DMDAGetOwnershipRanges(dap,&lxp,&lyp,PETSC_NULL);CHKERRQ(ierr);
	ierr = DMDAGetInfo(dap,0,0,0,0,&Mp,&Np,PETSC_NULL,0,0, 0,0,0, 0);CHKERRQ(ierr);
	ierr = PetscMalloc(sizeof(PetscInt)*Mp,&lxv);CHKERRQ(ierr);
	ierr = PetscMalloc(sizeof(PetscInt)*Np,&lyv);CHKERRQ(ierr);
	for (p=0; p<Mp; p++) {
		lxv[p] = lxp[p] * 2;
	} lxv[Mp-1]++;
	for (p=0; p<Np; p++) {
		lyv[p] = lyp[p] * 2;
	} lyv[Np-1]++;

	/* velocity */
	vbasis_dofs = 2;
	ierr = DMDACreate2d( PETSC_COMM_WORLD, DMDA_BOUNDARY_NONE,DMDA_BOUNDARY_NONE, DMDA_STENCIL_BOX, 2*MX+1,2*MY+1, Mp,Np, vbasis_dofs,2, lxv,lyv, &dav );CHKERRQ(ierr);
	ierr = DMDASetElementType_Q2(dav);CHKERRQ(ierr);
	ierr = PetscFree(lxv);CHKERRQ(ierr);
	ierr = PetscFree(lyv);CHKERRQ(ierr);

	/* set an initial geometry */
	ierr = DMDASetUniformCoordinates(dav,0.0,1.0, 0.0,1.0, 0.0,0.0);CHKERRQ(ierr);

	/* stokes */
	ierr = DMCompositeCreate(PETSC_COMM_WORLD,&multipys_pack);CHKERRQ(ierr);
	ierr = DMCompositeAddDM(multipys_pack,dav);CHKERRQ(ierr);	
	ierr = DMCompositeAddDM(multipys_pack,dap);CHKERRQ(ierr);	

  ierr = DMDASetFieldName(dap,0,"P1_p");CHKERRQ(ierr);
  ierr = DMDASetFieldName(dap,1,"P1_dpdx");CHKERRQ(ierr);
  ierr = DMDASetFieldName(dap,2,"P1_dpdy");CHKERRQ(ierr);
	ierr = DMDASetFieldName(dav,0,"velocity_u1");CHKERRQ(ierr);
	ierr = DMDASetFieldName(dav,1,"velocity_u2");CHKERRQ(ierr);
  ierr = PetscObjectSetOptionsPrefix((PetscObject)dap,"p_");CHKERRQ(ierr);
  ierr = PetscObjectSetOptionsPrefix((PetscObject)dav,"u_");CHKERRQ(ierr);
  ierr = PetscObjectSetOptionsPrefix((PetscObject)multipys_pack,"multipys_pack_");CHKERRQ(ierr);
	
	ctx->dav  = dav;
	ctx->dap  = dap;
	ctx->pack = multipys_pack;
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SwarmDMDA2dDataExchangerCreate"
PetscErrorCode SwarmDMDA2dDataExchangerCreate(DM da,DataEx *_de)
{
	DataEx de;
	const PetscInt *neighborranks;
	PetscInt neighborranks2[9],neighborcount;
	PetscInt i;
	PetscLogDouble t0,t1;
	PetscMPIInt rank;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	MPI_Comm_rank(((PetscObject)da)->comm,&rank);
	ierr = DMDAGetNeighbors(da,&neighborranks);CHKERRQ(ierr);
	
	neighborcount = 0;
	for (i=0; i<9; i++) {
		neighborranks2[i] = -1;
		if ( (neighborranks[i]>=0) && (neighborranks[i]!=rank) ) {
			neighborranks2[neighborcount] = neighborranks[i];
			neighborcount++;
		}
	}
	
	PetscGetTime(&t0);
	de = DataExCreate(((PetscObject)da)->comm,0);
	//	de = DataExCreate(PETSC_COMM_WORLD,0);
	ierr = DataExTopologyInitialize(de);CHKERRQ(ierr);
	for (i=0; i<neighborcount; i++) {
		ierr = DataExTopologyAddNeighbour(de,neighborranks2[i]);CHKERRQ(ierr);
	}
	ierr = DataExTopologyFinalize(de);CHKERRQ(ierr);
	PetscGetTime(&t1);
	PetscPrintf(de->comm,"DataEx Communication setup time = %1.4e\n",t1-t0);
	
	*_de = de;
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "pTatin2dCreatePassiveMarkers"
PetscErrorCode pTatin2dCreatePassiveMarkers(pTatinCtx ctx)
{
	DataBucket     db;
	DataEx         ex;	
	PetscLogDouble t0,t1;
	PetscInt       lmx,lmy;
	PetscBool      flg;
	PetscErrorCode ierr;
	PetscReal jmin[NSD],jmax[NSD];
	pTatinUnits    *units; 

	PetscFunctionBegin;
	/* register marker structures here */
	PetscGetTime(&t0);
	DataBucketCreate(&db);
	DataBucketRegisterField(db,MPntStd_classname,sizeof(MPntStd),PETSC_NULL);
	DataBucketRegisterField(db,MPntPPassive_classname,sizeof(MPntPPassive),PETSC_NULL);
	DataBucketFinalize(db);
	
	/* Choose coordinate layout for material points, -mp_layout 0,1 */
	/* set initial size */
    ierr = DMDAGetLocalSizeElementQ2(ctx->dav,&lmx,&lmy,PETSC_NULL);CHKERRQ(ierr);
	DataBucketSetInitialSizes(db,lmx*lmy*4,1000);
	PetscGetTime(&t1);
	PetscPrintf(PETSC_COMM_WORLD,"[[Passive Swarm initialization: %1.4lf sec]]\n", t1-t0);
	units       = &ctx->units;

		/* defaults for lattice layout */
		PetscInt   Nxp[] = {2,2}; /* change with -lattice_layout_N{x,y,z} */
		PetscReal  perturb = 0.0;   /* change with -lattice_layout_perturb */
		PetscOptionsGetReal("passiveSwarm_","-lattice_layout_perturb", &perturb, PETSC_NULL );
	    PetscOptionsGetInt("passiveSwarm_","-lattice_layout_Nx", &Nxp[0], PETSC_NULL );
	    PetscOptionsGetInt("passiveSwarm_","-lattice_layout_Ny", &Nxp[1], PETSC_NULL );

		if (perturb<0.0) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot use a negative perturbation");
	}
	if (perturb>1.0) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot use a perturbation greater than 1.0");
	}
	
	    DMDAGetBoundingBox(ctx->dav,jmin,jmax);
	    flg = PETSC_FALSE;
	    PetscOptionsGetReal("passiveSwarm_","-min_X", &jmin[0], &flg );
	    if (flg) UnitsApplyInverseScaling(units->si_length,jmin[0],&jmin[0]);
	    flg = PETSC_FALSE;
      	PetscOptionsGetReal("passiveSwarm_","-min_Y", &jmin[1], &flg );
      	if (flg) UnitsApplyInverseScaling(units->si_length,jmin[1],&jmin[1]);
	    flg = PETSC_FALSE;
	    PetscOptionsGetReal("passiveSwarm_","-max_X", &jmax[0], &flg );
	    if (flg) UnitsApplyInverseScaling(units->si_length,jmax[0],&jmax[0]);
	    flg = PETSC_FALSE;
	    PetscOptionsGetReal("passiveSwarm_","-max_Y", &jmax[1], &flg );
	    if (flg) UnitsApplyInverseScaling(units->si_length,jmax[1],&jmax[1]);
		
		PetscGetTime(&t0);
	{	
		ierr = SwarmMPntStd_CoordAssignment_LatticeLayout2d(ctx->dav,Nxp,perturb,db,jmin,jmax);CHKERRQ(ierr);
	
	}
	PetscGetTime(&t1);
	PetscPrintf(PETSC_COMM_WORLD,"[[Passive Swarm->coordinate assignment: %1.4lf sec]]\n", t1-t0);
	
	
	/* create the data exchanger need for parallel particle movement */
	ierr = SwarmDMDA2dDataExchangerCreate(ctx->dav,&ex);CHKERRQ(ierr);
	
	ctx->db_passive = db;
	ctx->ex_passive = ex;
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "pTatin2dCreateMaterialPoints"
PetscErrorCode pTatin2dCreateMaterialPoints(pTatinCtx ctx)
{
	DataBucket     db;
	DataEx         ex;	
	PetscLogDouble t0,t1;
	PetscInt       lmx,lmy;
	PetscBool      flg,store_melt,store_ages;
	PetscErrorCode ierr;
	PetscBool active;
	PetscReal jmin[NSD],jmax[NSD]; 

	PetscFunctionBegin;
	/* register marker structures here */
	PetscGetTime(&t0);
	DataBucketCreate(&db);
	DataBucketRegisterField(db,MPntStd_classname,sizeof(MPntStd),PETSC_NULL);
	DataBucketRegisterField(db,MPntPStokes_classname,sizeof(MPntPStokes),PETSC_NULL);
	DataBucketRegisterField(db,MPntPStokesPl_classname,sizeof(MPntPStokesPl),PETSC_NULL);
    PetscPrintf(PETSC_COMM_WORLD,"[pTatin]: creating material points \n");
    PetscPrintf(PETSC_COMM_WORLD,"[pTatin] rheol type %d \n", ctx->rheology_constants.rheology_type);
    if (ctx->rheology_constants.rheology_type ==7)
    DataBucketRegisterField(db,MPntPStokesElas_classname,sizeof(MPntPStokesElas),PETSC_NULL);
    
    
    ierr = pTatinPhysCompActivated(ctx,PhysComp_Darcy,&active);CHKERRQ(ierr);
	
	if (active) {
		DataBucketRegisterField(db,MPntPDarcy_classname,sizeof(MPntPDarcy),PETSC_NULL);
	}
    
	/* load material properties for energy equation */
	ierr = pTatinPhysCompActivated(ctx,PhysComp_Energy,&active);CHKERRQ(ierr);
	if (active) {
		DataBucketRegisterField(db,MPntPThermal_classname,sizeof(MPntPThermal),PETSC_NULL);
                store_melt = PETSC_FALSE;
	        ierr = PetscOptionsGetBool(PETSC_NULL,"-store_melt",&store_melt,&flg);CHKERRQ(ierr);
                if (store_melt){
                    DataBucketRegisterField(db,MPntPStokesMelt_classname,sizeof(MPntPStokesMelt),PETSC_NULL);
                    PetscPrintf(PETSC_COMM_WORLD,"[pTatin]: melt tool box toolbox activated \n");
 	            }	
 	            store_ages = PETSC_FALSE;
 	        ierr = PetscOptionsGetBool(PETSC_NULL,"-store_ages",&store_ages,&flg);CHKERRQ(ierr);    
 	            if (store_ages){
 	              DataBucketRegisterField(db,MPntPChrono_classname,sizeof(MPntPChrono),PETSC_NULL);
 	              PetscPrintf(PETSC_COMM_WORLD,"[pTatin]: thermochronology toolbox activated \n");
 	            }
	}
	
	DataBucketFinalize(db);
	
	/* Choose type of projection (for eta and rho) */
	ctx->coefficient_projection_type = 0;
	ierr = PetscOptionsGetInt(PETSC_NULL,"-coefficient_projection_type",&ctx->coefficient_projection_type,&flg);CHKERRQ(ierr);
	switch (ctx->coefficient_projection_type) {
		case 0:
			PetscPrintf(PETSC_COMM_WORLD,"  MaterialPointsStokes: Using P0 projection\n");
			break;
		case 1:
			PetscPrintf(PETSC_COMM_WORLD,"  MaterialPointsStokes: Using Q1 projection\n");
			break;
		case 2:
			PetscPrintf(PETSC_COMM_WORLD,"  MaterialPointsStokes: Using Q2 projection\n");
			break;
		default:
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER," -coefficient_projection_type = {0,1,2} implying {P0,Q1,Q2}");
			break;
	}
	
	
	/* Choose coordinate layout for material points, -mp_layout 0,1 */
	/* set initial size */
  ierr = DMDAGetLocalSizeElementQ2(ctx->dav,&lmx,&lmy,PETSC_NULL);CHKERRQ(ierr);
	DataBucketSetInitialSizes(db,lmx*lmy*4,1000);
	PetscGetTime(&t1);
	PetscPrintf(PETSC_COMM_WORLD,"[[Swarm initialization: %1.4lf sec]]\n", t1-t0);
	
	PetscGetTime(&t0);
	{
		PetscInt mplayout = 0;

		PetscOptionsGetInt(PETSC_NULL,"-mp_layout",&mplayout,PETSC_NULL);
		if (mplayout==0) {
		   /* defaults for lattice layout */
		    PetscInt   Nxp[] = {2,2}; /* change with -lattice_layout_N{x,y,z} */
		    PetscReal  perturb = 0.1;   /* change with -lattice_layout_perturb */
		   
		    PetscOptionsGetReal(PETSC_NULL,"-lattice_layout_perturb", &perturb, PETSC_NULL );
	        PetscOptionsGetInt(PETSC_NULL,"-lattice_layout_Nx", &Nxp[0], PETSC_NULL );
	        PetscOptionsGetInt(PETSC_NULL,"-lattice_layout_Ny", &Nxp[1], PETSC_NULL );
		    if (perturb<0.0) {
		     SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot use a negative perturbation");
	        }
	        if (perturb>1.0) {
		      SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot use a perturbation greater than 1.0");
	        }
	        
			DMDAGetBoundingBox(ctx->dav,jmin,jmax);
			
			ierr = SwarmMPntStd_CoordAssignment_LatticeLayout2d(ctx->dav,Nxp,perturb,db,jmin,jmax);CHKERRQ(ierr);
		} else if (mplayout==1) {
		    /* defaults for random layout */
		    PetscInt   nPerCell = 9; /* change with -random_layout_Np */
		    PetscOptionsGetInt(PETSC_NULL,"-random_layout_Np", &nPerCell, PETSC_NULL );
			
			ierr = SwarmMPntStd_CoordAssignment_RandomLayout2d(ctx->dav,nPerCell,db);CHKERRQ(ierr);
		}
	}
	PetscGetTime(&t1);
	PetscPrintf(PETSC_COMM_WORLD,"[[Swarm->coordinate assignment: %1.4lf sec]]\n", t1-t0);
	
	
	/* create the data exchanger need for parallel particle movement */
	ierr = SwarmDMDA2dDataExchangerCreate(ctx->dav,&ex);CHKERRQ(ierr);
	
	ctx->db = db;
	ctx->ex = ex;
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2dUpdateMaterialPoints"
PetscErrorCode pTatin2dUpdateMaterialPoints(pTatinCtx ctx,Vec X)
{
	DataBucket     db;
	BTruth found;
	PetscErrorCode ierr;
	
	ierr = SwarmUpdateProperties_MPntStd(ctx->db,ctx,X);CHKERRQ(ierr);
	ierr = SwarmUpdateProperties_MPntPStokes(ctx->db,ctx,X);CHKERRQ(ierr);
	DataBucketQueryDataFieldByName(ctx->db,MPntPStokesPl_classname,&found);
	if(found==BTRUE) {
		ierr = SwarmUpdateProperties_MPntPStokesPl(ctx->db,ctx,X);CHKERRQ(ierr);
	}
    DataBucketQueryDataFieldByName(ctx->db,MPntPStokesElas_classname,&found);
    if(found==BTRUE) {
        ierr = SwarmUpdateProperties_MPntPStokes_el(ctx->db,ctx,X);CHKERRQ(ierr);
    }
    
	DataBucketQueryDataFieldByName(ctx->db,MPntPStokesMelt_classname,&found);
	if(found==BTRUE) {
    ierr = SwarmUpdateProperties_MPntPStokesMelt(ctx->db,ctx,X);CHKERRQ(ierr);
	}
	
	DataBucketQueryDataFieldByName(ctx->db,MPntPChrono_classname,&found);
	if(found==BTRUE) {
    ierr = SwarmUpdateProperties_MPntPChrono(ctx->db,ctx,X);CHKERRQ(ierr);
	}

	DataBucketQueryDataFieldByName(ctx->db,MPntPDarcy_classname,&found);
	if(found==BTRUE) {
    ierr = SwarmUpdateProperties_MPntPDarcy(ctx->db,ctx,X);CHKERRQ(ierr);
	}
	/*
    DataBucketQueryDataFieldByName(ctx->db,MPntPThermoDynamic_classname,&found);
	if(found==BTRUE) {
    ierr = SwarmUpdateProperties_MPntPThermoDynamic(ctx->db,ctx,X);CHKERRQ(ierr);
	}
    */

	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2dUpdatePassiveMarkers"
PetscErrorCode pTatin2dUpdatePassiveMarkers(pTatinCtx ctx,Vec X)
{

	
	PetscErrorCode ierr;
	
	ierr = SwarmUpdateProperties_MPntStd(ctx->db_passive,ctx,X);CHKERRQ(ierr);
	ierr = SwarmUpdateProperties_MPntPPassive(ctx->db_passive,ctx,X);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2dParseOptions"
PetscErrorCode pTatin2dParseOptions(pTatinCtx ctx)
{
	PetscInt mx,my;
	PetscBool flg;
  char optionsfile[PETSC_MAX_PATH_LEN];
	PetscErrorCode ierr;
	
	/* parse options */
	ctx->mx = 4;
	ctx->my = 4;
	PetscOptionsGetInt(PETSC_NULL,"-mx",&ctx->mx,0);
	PetscOptionsGetInt(PETSC_NULL,"-my",&ctx->my,0);
	
	ierr = PetscOptionsGetBool(PETSC_NULL,"-use_mf_stokes",&ctx->use_mf_stokes,&flg);CHKERRQ(ierr);
	ierr = PetscOptionsGetBool(PETSC_NULL,"-pre_use_mf_stokes",&ctx->pre_use_mf_stokes,&flg);CHKERRQ(ierr);
	ierr = PetscOptionsGetBool(PETSC_NULL,"-with_statistics",&ctx->solverstatistics,&flg);CHKERRQ(ierr);
		
	/* time stepping */
	PetscOptionsGetInt(PETSC_NULL,"-nsteps",&ctx->nsteps,&flg);
	PetscOptionsGetReal(PETSC_NULL,"-dt_min",&ctx->dt_min,&flg);
	PetscOptionsGetReal(PETSC_NULL,"-dt_max",&ctx->dt_max,&flg);
	PetscOptionsGetReal(PETSC_NULL,"-time_max",&ctx->time_max,&flg);
	PetscOptionsGetReal(PETSC_NULL,"-courant_surf",&ctx->courant_surf,&flg);
	PetscOptionsGetReal(PETSC_NULL,"-max_disp_y",&ctx->max_disp_y,&flg);
    PetscOptionsGetInt(PETSC_NULL,"-output_frequency",&ctx->output_frequency,&flg);
	
	PetscOptionsGetInt(PETSC_NULL,"-ve_incremental",&ctx->ve_incremental,&flg);
  
	
	sprintf(optionsfile,"%s/ptatin.options",ctx->outputpath);
	ierr = pTatinWriteOptionsFile(optionsfile);CHKERRQ(ierr);
	
	ierr = pTatinModelLoad(ctx);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2dDestroyContext"
PetscErrorCode pTatin2dDestroyContext(pTatinCtx *ctx)
{
	pTatinCtx user = *ctx;
	PetscInt e;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;

	if (user->ex) { /*ierr = DataExView(user->ex);CHKERRQ(ierr);*/ ierr = DataExDestroy(user->ex);CHKERRQ(ierr); }
	if (user->db) { DataBucketDestroy(&user->db); }
	
	if (user->Q) { ierr = QuadratureStokesDestroy(&user->Q);CHKERRQ(ierr); }
	for (e=0; e<QUAD_EDGES; e++) {
		if (user->surfQ[e]) { ierr = SurfaceQuadratureStokesDestroy(&user->surfQ[e]);CHKERRQ(ierr); }
	}
	if (user->p_bclist) { ierr = BCListDestroy(&user->p_bclist);CHKERRQ(ierr); }
	if (user->u_bclist) { ierr = BCListDestroy(&user->u_bclist);CHKERRQ(ierr); }
  if (user->pack) { ierr = DMDestroy(&user->pack);CHKERRQ(ierr); }
	if (user->dap) { ierr = DMDestroy(&user->dap);CHKERRQ(ierr); }
  if (user->dav) { ierr = DMDestroy(&user->dav);CHKERRQ(ierr); }

	if (user->phys_energy) {
		ierr = PhysComp_EnergyEquationDestroy(&user->phys_energy);CHKERRQ(ierr);
	}
	
	if (user->phys_spm) {
		ierr = PhysComp_SPMEquationDestroy(&user->phys_spm);CHKERRQ(ierr);
	}
	
	if (user->phys_darcy) {
		ierr = PhysComp_DarcyEquationDestroy(&user->phys_darcy);CHKERRQ(ierr);
	}
	
	ierr = pTatinUnitsDestroy(&user->units);CHKERRQ(ierr);
	
	ierr = PetscContainerDestroy(&user->model_data);CHKERRQ(ierr);

	ierr = PetscLogView(user->log);CHKERRQ(ierr);
	ierr = pTatinLogCloseFile(user);CHKERRQ(ierr);


	/* destroy context */
	ierr = PetscFree(user);CHKERRQ(ierr);
	
	*ctx = PETSC_NULL;
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatinUnitsCreate"
PetscErrorCode pTatinUnitsCreate(pTatinUnits *units)
{
	PetscErrorCode ierr;
	PetscReal      scale_U,scale_L,scale_P,scale_t,scale_T,scale_ETA;
	PetscBool      has_time,has_vel;
	
	UnitsCreateSIVelocity(&units->si_velocity);		                UnitsSetScale(units->si_velocity,1.0);
	UnitsCreateSILength(&units->si_length);		                    UnitsSetScale(units->si_length,1.0);
	UnitsCreateSITime(&units->si_time);		                        UnitsSetScale(units->si_time,1.0);
	
	UnitsCreateSIViscosity(&units->si_viscosity);                 UnitsSetScale(units->si_viscosity,1.0);
	UnitsCreateSIStress(&units->si_stress);                       UnitsSetScale(units->si_stress,1.0);

	UnitsCreate(&units->si_force_per_volume,"force_per_volume","N/m^3",1.0);   UnitsSetScale(units->si_force_per_volume,1.0);
	UnitsCreate(&units->si_strainrate,"strain_rate","1/s",1.0);   UnitsSetScale(units->si_strainrate,1.0);
    UnitsCreate(&units->si_diffusivity,"diffusivity","m^2/s",1.0);   UnitsSetScale(units->si_diffusivity,1.0);
	UnitsCreateSITemperature(&units->si_temperature);             UnitsSetScale(units->si_temperature,1.0);
    UnitsCreate(&units->si_heatsource,"heat_production","K/s",1.0);   UnitsSetScale(units->si_heatsource,1.0);
    UnitsCreate(&units->si_permeability,"permeability","m2",1.0);   UnitsSetScale(units->si_permeability,1.0);
    UnitsCreate(&units->si_heatcapacity,"heat_capacity","m^2 s-2  K-1",1.0);   UnitsSetScale(units->si_heatcapacity,1.0);
    UnitsCreate(&units->si_acceleration,"acceleration","m^2 s-1 ",1.0);   UnitsSetScale(units->si_acceleration,1.0);
    UnitsCreate(&units->si_density,"density","kg m-3 ",1.0);   UnitsSetScale(units->si_density,1.0);
	scale_U   = 1.0;
	scale_L   = 1.0;
	scale_t   = 1.0;
	scale_ETA = 1.0;
	scale_P   = 1.0;
	scale_T   = 1.0;

	has_time = has_vel = PETSC_FALSE;
	ierr = PetscOptionsGetReal(PETSC_NULL,"-stokes_scale_velocity",&scale_U,&has_vel);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-stokes_scale_time",&scale_t,&has_time);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-stokes_scale_length",&scale_L,PETSC_NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-stokes_scale_viscosity",&scale_ETA,PETSC_NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-energy_scale_T",&scale_T,PETSC_NULL);CHKERRQ(ierr);

	if (has_time && has_vel) {
		SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot specificy BOTH -stokes_scale_velocity and -stokes_scale_time");
	}
	if (has_vel) {
		UnitsSetScale(units->si_velocity,scale_U);
		scale_t = scale_L / scale_U;
		UnitsSetScale(units->si_time,scale_t);
	}
	if (has_time) {
		UnitsGetScale(units->si_time,&scale_t);
		scale_U = scale_L / scale_t;
		UnitsSetScale(units->si_velocity,scale_U);
	}
	UnitsSetScale(units->si_length,scale_L);
	UnitsSetScale(units->si_viscosity,scale_ETA);

	UnitsSetScale(units->si_stress,scale_ETA/scale_t);
	UnitsSetScale(units->si_force_per_volume,scale_ETA / scale_t/scale_L);
    UnitsSetScale(units->si_permeability,scale_L*scale_L);	
	UnitsSetScale(units->si_temperature,scale_T);
	UnitsSetScale(units->si_heatsource,scale_T/scale_t);
	UnitsSetScale(units->si_strainrate,1.0/scale_t);
    UnitsSetScale(units->si_diffusivity,scale_L*scale_L/scale_t);
    UnitsSetScale(units->si_heatcapacity,scale_L*scale_L/scale_t/scale_t/scale_T);
    UnitsSetScale(units->si_acceleration,scale_L/scale_t/scale_t);
    UnitsSetScale(units->si_density,scale_ETA/scale_L/scale_L*scale_t);
	/* helphers for visualization */
	UnitsCreate(&units->velocity_si2mm_per_year,"velocity","mm/yr",1.0/( 1.0e3*(60.0*60.0*24.0*365.0) ) );
	UnitsCreate(&units->pressure_si2MPa,"pressure","MPa",1.0e-6);

	UnitsView(units->si_velocity);
	UnitsView(units->si_length);
	UnitsView(units->si_viscosity);
	UnitsView(units->si_stress);
	UnitsView(units->si_force_per_volume);
	UnitsView(units->si_time);
	UnitsView(units->si_strainrate);
    UnitsView(units->si_heatcapacity);
    UnitsView(units->si_heatsource);
    UnitsView(units->si_density);
    UnitsView(units->si_acceleration);
	
    PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatinUnitsDestroy"
PetscErrorCode pTatinUnitsDestroy(pTatinUnits *units)
{
	UnitsDestroy(&units->si_velocity);
	UnitsDestroy(&units->si_length);	
	UnitsDestroy(&units->si_time);
	UnitsDestroy(&units->si_viscosity);
	UnitsDestroy(&units->si_stress);
	UnitsDestroy(&units->si_force_per_volume);
	UnitsDestroy(&units->si_strainrate);
	UnitsDestroy(&units->si_temperature);
	UnitsDestroy(&units->si_diffusivity);
    UnitsDestroy(&units->si_heatcapacity);
    UnitsDestroy(&units->si_heatsource);
	PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "pTatin2dCreateContext"
PetscErrorCode pTatin2dCreateContext(pTatinCtx *ctx)
{
	PetscBool flg;
	PetscInt e;
	pTatinCtx user;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
  ierr = PetscNew(struct _p_pTatinCtx,&user);CHKERRQ(ierr);
	
	/* init */
  user->pack     = PETSC_NULL; /* DM composite for velocity and pressure */
  user->dav      = PETSC_NULL; /* DM for velocity */
  user->dap      = PETSC_NULL; /* DM for pressure */
	user->u_bclist = PETSC_NULL; /* bc's for velocity */
	user->p_bclist = PETSC_NULL;
	user->Q        = PETSC_NULL; /* quadrature rule */
	for (e=0; e<QUAD_EDGES; e++) {
		user->surfQ[e] = PETSC_NULL;
	}
	//user->data     = PETSC_NULL; /* user data */
	/* set defaults */
	user->mx               = 4;
	user->my               = 4;
	user->use_mf_stokes    = PETSC_FALSE;
	user->solverstatistics = PETSC_FALSE;
	
	user->continuation_m   = 1;
	user->continuation_M   = 1;
	
	/* time step control */
	user->nsteps           = 1;
	user->dt_max           = 1.0e30;
	user->dt_min           = 1.0e-30;
	user->dt               = user->dt_min;
	user->output_frequency = 1;
	user->time_max         = 1.0e32;
	user->time             = 0.0;
    user->courant_surf     = 1.0;
	user->max_disp_y       = 1.0e30;
  user->step             = 0;
  user->ve_incremental   = 0;
  user->dt_adv           = user->dt_min;
  
  ierr = RheologyConstantsInitialise(&user->rheology_constants);CHKERRQ(ierr);
  ierr = SurfaceRheologyConstantsInitialise(&user->surf_rheology_constants);CHKERRQ(ierr);
	
	ierr = pTatinUnitsCreate(&user->units);CHKERRQ(ierr);
	
	ierr = PetscContainerCreate(PETSC_COMM_WORLD,&user->model_data);CHKERRQ(ierr);

	flg = PETSC_FALSE;
	ierr = PetscOptionsGetString(PETSC_NULL,"-output_path",user->outputpath,PETSC_MAX_PATH_LEN-1,&flg);CHKERRQ(ierr);
	if (flg == PETSC_FALSE) { 
		sprintf(user->outputpath,"./output");
	}
	ierr = pTatinCreateDirectory(user->outputpath);CHKERRQ(ierr);

	ierr = pTatinLogOpenFile(user);CHKERRQ(ierr);
	ierr = pTatinLogHeader(user);CHKERRQ(ierr);
	
	*ctx = user;

	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatinCtxGetModelData"
PetscErrorCode pTatinCtxGetModelData(pTatinCtx ctx,const char name[],void **data)
{
	PetscErrorCode ierr;
	PetscContainer container;
	
  PetscFunctionBegin;
	ierr = PetscObjectQuery((PetscObject)ctx->model_data,name,(PetscObject*)&container);CHKERRQ(ierr);
	if (!container) SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONG,"No data with name \"%s\" was composed with ctx->model_data",name);
	ierr = PetscContainerGetPointer(container,data);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatinCtxAttachModelData"
PetscErrorCode pTatinCtxAttachModelData(pTatinCtx ctx,const char name[],void *data)
{
	PetscContainer container;
	PetscErrorCode ierr;
	
  PetscFunctionBegin;
  ierr = PetscContainerCreate(PETSC_COMM_WORLD,&container);CHKERRQ(ierr);
  ierr = PetscContainerSetPointer(container,(void*)data);CHKERRQ(ierr);
	
	ierr = PetscObjectCompose((PetscObject)ctx->model_data,name,(PetscObject)container);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "DMDAViewGnuplot2d"
PetscErrorCode DMDAViewGnuplot2d(DM da,Vec fields,const char comment[],const char prefix[])
{
  DM             cda;
  Vec            coords,local_fields;
  DMDACoor2d       **_coords;
  FILE           *fp;
  char           fname[PETSC_MAX_PATH_LEN];
  PetscMPIInt    rank;
  PetscInt       si,sj,nx,ny,i,j;
  PetscInt       n_dofs,d;
  PetscScalar    *_fields;
  PetscErrorCode ierr;
	
  PetscFunctionBegin;
  MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  ierr = PetscSNPrintf(fname,sizeof fname,"%s-p%1.4d.dat",prefix,rank);CHKERRQ(ierr);
  ierr = PetscFOpen(PETSC_COMM_SELF,fname,"w",&fp);CHKERRQ(ierr);
  if (!fp) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file");
	
  ierr = PetscFPrintf(PETSC_COMM_SELF,fp,"### %s (processor %1.4d) ### \n",comment,rank);CHKERRQ(ierr);
  ierr = DMDAGetInfo(da,0,0,0,0,0,0,0,&n_dofs,0, 0,0,0, 0);CHKERRQ(ierr);
  ierr = PetscFPrintf(PETSC_COMM_SELF,fp,"### x y ");CHKERRQ(ierr);
  for (d = 0; d < n_dofs; d++) {
    const char *field_name;
    ierr = DMDAGetFieldName(da,d,&field_name);CHKERRQ(ierr);
    ierr = PetscFPrintf(PETSC_COMM_SELF,fp,"%s ",field_name);CHKERRQ(ierr);
  }
  ierr = PetscFPrintf(PETSC_COMM_SELF,fp,"###\n");CHKERRQ(ierr);
	
	
  ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
  ierr = DMDAGetGhostedCoordinates(da,&coords);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(cda,coords,&_coords);CHKERRQ(ierr);
  ierr = DMDAGetGhostCorners(cda,&si,&sj,0,&nx,&ny,0);CHKERRQ(ierr);
	
  ierr = DMGetLocalVector(da,&local_fields);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(da,fields,INSERT_VALUES,local_fields);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(da,fields,INSERT_VALUES,local_fields);CHKERRQ(ierr);
  ierr = VecGetArray(local_fields,&_fields);CHKERRQ(ierr);
	
	
  for (j = sj; j < sj+ny; j++) {
    for (i = si; i < si+nx; i++) {
      PetscScalar coord_x,coord_y;
      PetscScalar field_d;
			
      coord_x = _coords[j][i].x;
      coord_y = _coords[j][i].y;
			
      ierr = PetscFPrintf(PETSC_COMM_SELF,fp,"%1.6e %1.6e ",PetscRealPart(coord_x),PetscRealPart(coord_y));CHKERRQ(ierr);
      for (d = 0; d < n_dofs; d++) {
        field_d = _fields[ n_dofs*((i-si)+(j-sj)*(nx))+d ];
        ierr    = PetscFPrintf(PETSC_COMM_SELF,fp,"%1.6e ",PetscRealPart(field_d));CHKERRQ(ierr);
      }
      ierr = PetscFPrintf(PETSC_COMM_SELF,fp,"\n");CHKERRQ(ierr);
    }
  }
  ierr = VecRestoreArray(local_fields,&_fields);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(da,&local_fields);CHKERRQ(ierr);
	
  ierr = DMDAVecRestoreArray(cda,coords,&_coords);CHKERRQ(ierr);
	
  ierr = PetscFClose(PETSC_COMM_SELF,fp);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDAViewPressureGnuplot2d"
PetscErrorCode DMDAViewPressureGnuplot2d(DM da,DM dap,Vec X,const char prefix[])
{
  FILE           *fp;
  char           fname[PETSC_MAX_PATH_LEN];
  PetscMPIInt    rank;
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel_u,nel_p,nen_u,nen_p,e,i;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D],elpressure[P_BASIS_FUNCTIONS];
	PetscScalar *LA_localX;
	Vec localX;
  PetscErrorCode ierr;
	
  PetscFunctionBegin;
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);
  ierr = PetscSNPrintf(fname,sizeof fname,"%s-p%1.4d.dat",prefix,rank);CHKERRQ(ierr);
  ierr = PetscFOpen(PETSC_COMM_SELF,fname,"w",&fp);CHKERRQ(ierr);
  if (!fp) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_USER,"Cannot open file");
	
	
  PetscFunctionBegin;
	/* setup for coords */
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(da,&gcoords);CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	/* setup pressure */
	ierr = DMGetLocalVector(dap,&localX);CHKERRQ(ierr);
	ierr = DMGlobalToLocalBegin(dap,X,INSERT_VALUES,localX);CHKERRQ(ierr);
	ierr = DMGlobalToLocalEnd(  dap,X,INSERT_VALUES,localX);CHKERRQ(ierr);
	ierr = VecGetArray(localX,&LA_localX);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(da ,&nel_u,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel_p,&nen_p,&elnidx_p);CHKERRQ(ierr);
	
  ierr = PetscFPrintf(PETSC_COMM_SELF,fp,"# pressure field %1.4d  \n",rank);CHKERRQ(ierr);
	ierr = PetscFPrintf(PETSC_COMM_SELF,fp,"# ncells %d  \n",nel_p);CHKERRQ(ierr);
  ierr = PetscFPrintf(PETSC_COMM_SELF,fp,"# fields ( x, y, pressure ) \n");CHKERRQ(ierr);
	
	for (e=0;e<nel_p;e++) {
		PetscScalar NIu[U_BASIS_FUNCTIONS];
		PetscScalar NIp[P_BASIS_FUNCTIONS];
		PetscScalar pressure,xpos[NSD];
		PetscScalar xip[NSD];
		
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		ierr = DMDAGetScalarElementField_2D(elpressure,nen_p,(PetscInt*)&elnidx_p[nen_p*e],LA_localX);CHKERRQ(ierr);
		
		xip[0] = xip[1] = 0.0;
		ConstructNI_Q2_2D(xip,NIu);
		ConstructNi_pressure(xip,elcoords,NIp);
		
		xpos[0] = xpos[1] = 0.0;
		for (i=0; i<nen_u; i++) {
			xpos[0] += NIu[i] * elcoords[2*i  ];
			xpos[1] += NIu[i] * elcoords[2*i+1];
		}
		
		pressure = 0.0;
		for (i=0; i<nen_p; i++) {
			pressure += NIp[i] * elpressure[i];
		}
		ierr = PetscFPrintf(PETSC_COMM_SELF,fp,"%1.6e %1.6e %1.6e \n",xpos[0],xpos[1],pressure);CHKERRQ(ierr);
		
	}
	ierr = VecRestoreArray(localX,&LA_localX);CHKERRQ(ierr);
	ierr = DMRestoreLocalVector(dap,&localX);CHKERRQ(ierr);
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = PetscFClose(PETSC_COMM_SELF,fp);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_StokesFieldOutput_DefaultGnuplot"
PetscErrorCode pTatin2d_StokesFieldOutput_DefaultGnuplot(pTatinCtx ctx,Vec X,const char prefix[])
{
	Vec velocity,pressure;
  char           u_fname[PETSC_MAX_PATH_LEN];
  char           p_fname[PETSC_MAX_PATH_LEN];
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	ierr = DMCompositeGetAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
	
	if (prefix) {
		ierr = PetscSNPrintf(u_fname,sizeof u_fname,"%s_stokes_u",prefix);CHKERRQ(ierr);
		ierr = PetscSNPrintf(p_fname,sizeof p_fname,"%s_stokes_p",prefix);CHKERRQ(ierr);
	}else {
		ierr = PetscSNPrintf(u_fname,sizeof u_fname,"stokes_u");CHKERRQ(ierr);
		ierr = PetscSNPrintf(p_fname,sizeof p_fname,"stokes_p");CHKERRQ(ierr);
	}
	ierr = DMDAViewGnuplot2d(ctx->dav,velocity,"Velocity solution for Stokes eqn.",u_fname);CHKERRQ(ierr);
	ierr = DMDAViewPressureGnuplot2d(ctx->dav,ctx->dap,pressure,p_fname);CHKERRQ(ierr);
	ierr = DMCompositeRestoreAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelOutput_Default"
PetscErrorCode pTatin2d_ModelOutput_Default(pTatinCtx ctx,Vec X,const char prefix[])
{
	PetscErrorCode ierr;
  char           fname[PETSC_MAX_PATH_LEN];
	PetscFunctionBegin;
	
	ierr = pTatin2d_StokesFieldOutput_DefaultGnuplot(ctx,X,prefix);CHKERRQ(ierr);
	if (prefix) {
		ierr = PetscSNPrintf(fname,sizeof fname,"%s_stokes_coeff",prefix);CHKERRQ(ierr);
	}else {
		ierr = PetscSNPrintf(fname,sizeof fname,"stokes_coeff");CHKERRQ(ierr);
	}
	ierr = QuadratureStokesView(ctx->Q,fname);CHKERRQ(ierr);
	PetscFunctionReturn(0);
}

/* ===== test_mf ====== */
#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_testMF"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_testMF(pTatinCtx ctx)
{
	BCList         ubclist;
	PetscScalar    bcval;
	DM             dav;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ubclist = ctx->u_bclist;
	dav     = ctx->dav;
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}


/* API for users */

/*define you BC function here*/
#undef __FUNCT__  
#define __FUNCT__ "BCListEvaluator_Poly"
PetscBool BCListEvaluator_Poly(PetscScalar pos[],PetscScalar *value,void *ctx)
{
	PetscBool apply_constraint=PETSC_FALSE;
	BC_Poly data = (BC_Poly)ctx;
	
	if ((pos[data->dim] >= data->beg) && (pos[data->dim] <= data->end) ) {
    *value = data->a+data->b*(pos[data->dim])+data->c*(pos[data->dim]*(pos[data->dim]));
		apply_constraint = PETSC_TRUE;		
	}
	
	return apply_constraint;
}


#undef __FUNCT__  
#define __FUNCT__ "BCListEvaluator_Geomod3"
PetscBool BCListEvaluator_Geomod3(PetscScalar pos[],PetscScalar *value,void *ctx)
{
	PetscBool apply_constraint=PETSC_FALSE;
	BC_Geomod3 data = (BC_Geomod3)ctx;
	PetscScalar posdisc;  
	
	posdisc = data->x0 + data->vx*data->time; 
	
	if (pos[data->dim] >= posdisc) {
	 	  *value = data->vx;
	 	   apply_constraint = PETSC_TRUE;
	} 
				
	return apply_constraint;
}

#undef __FUNCT__
#define __FUNCT__ "BCListEvaluator_Geomod3R"
PetscBool BCListEvaluator_Geomod3R(PetscScalar pos[],PetscScalar *value,void *ctx)
{
    PetscBool apply_constraint=PETSC_FALSE;
    BC_Geomod3 data = (BC_Geomod3)ctx;
    PetscScalar posdisc;
    
    posdisc = data->x0 + data->vx*data->time;
    
    if (pos[data->dim] <= posdisc) {
        *value = data->vx;
        apply_constraint = PETSC_TRUE;
    }
				
    return apply_constraint;
}

#undef __FUNCT__
#define __FUNCT__ "BCListEvaluator_none"
PetscBool BCListEvaluator_none(PetscScalar pos[],PetscScalar *value,void *ctx)
{
    PetscBool apply_constraint=PETSC_FALSE;
    
    return apply_constraint;
}


#undef __FUNCT__
#define __FUNCT__ "BCListEvaluator_Geomod3_fric"
PetscBool BCListEvaluator_Geomod3_fric(PetscScalar pos[],PetscScalar *value,void *ctx)
{
	PetscBool apply_constraint=PETSC_FALSE;
	BC_Geomod3 data = (BC_Geomod3)ctx;
	PetscScalar posdisc;  
	
	posdisc = data->x0 + data->vx*data->time; 
	
	if (pos[data->dim] >= posdisc) {
	 	  *value = data->vx;
	 	  apply_constraint = PETSC_TRUE;
	 	   
	} else if (pos[data->dim] < (posdisc - data->x0*0.005/0.23)) {
	 	  *value = 0.0;
	 	  apply_constraint = PETSC_TRUE;
	}
	
	
	return apply_constraint;
}


#undef __FUNCT__  
#define __FUNCT__ "BCListEvaluator_Step"
PetscBool BCListEvaluator_Step(PetscScalar pos[],PetscScalar *value,void *ctx)
{
	PetscBool apply_constraint=PETSC_FALSE;
	BC_Step   data = (BC_Step)ctx;
	
	if ((pos[data->dim] >= data->beg) && (pos[data->dim] <= data->end) ) {
	  if (pos[data->dim] > data->b){
		  *value = data->v0;
		} else if (pos[data->dim] < data->a) {
		  *value = data->v1;
		} else {
		  *value =(data->v1-data->v0)/(data->b-data->a)*(pos[data->dim]-data->a)+data->v0;
		}
		apply_constraint = PETSC_TRUE;		
	}
	return apply_constraint;
}

#undef __FUNCT__  
#define __FUNCT__ "BCListEvaluator_Alabeaumont"
PetscBool BCListEvaluator_Alabeaumont( PetscScalar position[], PetscScalar *value, void *ctx ) 
{
    PetscBool impose_dirichlet = PETSC_TRUE;
    BC_Alabeaumont data = (BC_Alabeaumont)ctx;
    
    if (position[data->dim]>data->x0) {
        *value = data->v0;
       
    }else if (position[data->dim]<data->x1){
        *value = data->v1;
    }else{
        *value = data->v1+(position[data->dim]-data->x1)*(data->v0-data->v1)/(data->x0-data->x1);
    }
    
    impose_dirichlet = PETSC_TRUE;
    return impose_dirichlet;
}

#undef __FUNCT__  
#define __FUNCT__ "BCListEvaluator_Godfinger"
PetscBool BCListEvaluator_Godfinger( PetscScalar position[], PetscScalar *value, void *ctx ) 
{
    PetscBool    impose_dirichlet = PETSC_FALSE;
    BC_Godfinger data = (BC_Godfinger)ctx;
    
    if (position[0] >= data->xi & position[0] < data->x0 ) {
        *value = -data->vx;
        impose_dirichlet = PETSC_TRUE;
    }else if (position[0] > data->x0 & position[0] <= data->xe ){
        *value = data->vx;
        impose_dirichlet = PETSC_TRUE;
    }
    
    return impose_dirichlet;
}


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_UpdateCoordinatesStd"
PetscErrorCode pTatin2d_UpdateCoordinatesStd(DM pack,Vec X,PetscReal dt_adv)
{
	PetscErrorCode  ierr;
	Vec coordinates,gcoords;
	DM dac,dav,dap;
	Vec Xu,Xp;

	ierr = DMCompositeGetAccess(pack,X,&Xu,&Xp);CHKERRQ(ierr);
	ierr = DMCompositeGetEntries(pack,&dav,&dap);CHKERRQ(ierr);
	
	ierr = DMDAGetCoordinates(dav,&coordinates);CHKERRQ(ierr);
  
	ierr = VecAXPY(coordinates,dt_adv,Xu);CHKERRQ(ierr); /* x = x + dt.v  */
	
	ierr = DMDAGetCoordinateDA(dav,&dac);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(dav,&gcoords);CHKERRQ(ierr);
	ierr = DMGlobalToLocalBegin(dac,coordinates,INSERT_VALUES,gcoords);CHKERRQ(ierr);
	ierr = DMGlobalToLocalEnd(dac,coordinates,INSERT_VALUES,gcoords);CHKERRQ(ierr);
	
	ierr = DMCompositeRestoreAccess(pack,X,&Xu,&Xp);CHKERRQ(ierr);

	PetscFunctionReturn(0);
}


/*
 For multiple, decoupled physics, several time scales are likely to be present.
 In general, we are unlikely to be able to know, from each physics, what all the time scales are going to be.

 Probably what we will do is this
 
 Solve physics_1,
 Compute appropriate time step for physics_1, dt_1

 Solve physics_2,
 Compute appropriate time step for physics_2, dt_2
 
 Solve physics_3,
 Compute appropriate time step for physics_3, dt_3
 
 dt = min( dt_1, dt_2, dt_3 ) subject to some global min/max cut offs.
*/
#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_SetTimestep"
PetscErrorCode pTatin2d_SetTimestep(pTatinCtx ctx,const char timescale_name[],PetscReal dt_trial)
{
	
  PetscErrorCode  ierr;
	Vec coordinates,gcoords;
	DM dac,dav,dap;
	Vec Xu,Xp;
	PetscReal dt_current;
	
	
	PetscPrintf(PETSC_COMM_WORLD,"====================================================\n");
	PetscPrintf(PETSC_COMM_WORLD,"  TimeStep control: \n" );
	PetscPrintf(PETSC_COMM_WORLD,"    Suggesting suitable step length for time scale \"%s\" \n", timescale_name );
	
	dt_current = ctx->dt;
	PetscPrintf(PETSC_COMM_WORLD,"   dt current    = %1.4e \n", dt_current );
	PetscPrintf(PETSC_COMM_WORLD,"   dt trial      = %1.4e \n", dt_trial );
	
	if (dt_trial < dt_current) {
		PetscPrintf(PETSC_COMM_WORLD,"   accepting trial step length suggested \n" );
		ctx->dt = dt_trial;
	}
	dt_current = ctx->dt;

	/* apply limiters */
  if (dt_current < ctx->dt_min) {
		dt_current = ctx->dt_min;
		PetscPrintf(PETSC_COMM_WORLD,"      dt < dt_min : limited to = %1.4e \n", dt_current );
	}
  if (dt_current > ctx->dt_max) {
		dt_current = ctx->dt_max;
		PetscPrintf(PETSC_COMM_WORLD,"      dt > dt_max : restricted to = %1.4e \n", dt_current );
	}
	ctx->dt = dt_current;
	PetscPrintf(PETSC_COMM_WORLD,"   dt selected   = %1.4e \n", ctx->dt );
	PetscPrintf(PETSC_COMM_WORLD,"====================================================\n");
	
  
	PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ComputeTimestep"
PetscErrorCode pTatin2d_ComputeTimestep(pTatinCtx ctx,DM pack,Vec X)
{
	
  PetscErrorCode  ierr;
	Vec coordinates,gcoords;
	DM dac,dav,dap;
	Vec Xu,Xp;
	PetscReal dt_local;
	
	dt_local = ctx->dt;
  
	PetscPrintf(PETSC_COMM_WORLD,"  *** dt computed = %1.4e \n", dt_local );
	
  if (dt_local < ctx->dt_min) {
		dt_local = ctx->dt_min;
		PetscPrintf(PETSC_COMM_WORLD,"      dt < min : limited to = %1.4e \n", dt_local );
	}
	
  if (dt_local > ctx->dt_max) {
		dt_local = ctx->dt_max;
		PetscPrintf(PETSC_COMM_WORLD,"      dt > max : restricted to = %1.4e \n", dt_local );
	}
	
	ctx->dt = dt_local;
  
	if (ctx->ve_incremental != 1) {
    ctx->dt_adv = dt_local;
  } else {
    ctx->dt_adv = 1.0; 
  	PetscPrintf(PETSC_COMM_WORLD,"       Solving for displacement setting dt_adv to 1.0 \n");
  }

  
	
	PetscFunctionReturn(0);
}

/* don't write out options this way, they cannot be loaded from file */
/*
 ierr = PetscOptionsGetAll(&copts);CHKERRQ(ierr);
 PetscPrintf(PETSC_COMM_WORLD,"All opts: %s \n", copts);
 ierr = PetscOptionsInsertFile(PETSC_COMM_WORLD,"test.opts",PETSC_FALSE);CHKERRQ(ierr);
 */
PetscErrorCode pTatinWriteOptionsFile(const char filename[])
{
	PetscViewer viewer;
	char username[PETSC_MAX_PATH_LEN];
	char date[PETSC_MAX_PATH_LEN];
	char machine[PETSC_MAX_PATH_LEN];
	PetscErrorCode ierr;
	
	if (!filename) {
		ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD,"ptatin.options",&viewer);CHKERRQ(ierr);
	} else {
		ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD,filename,&viewer);CHKERRQ(ierr);
	}
	
	/* write header into options file */
	ierr = PetscGetUserName(username,PETSC_MAX_PATH_LEN-1);CHKERRQ(ierr);
	ierr = PetscGetDate(date,PETSC_MAX_PATH_LEN-1);CHKERRQ(ierr);
	ierr = PetscGetHostName(machine,PETSC_MAX_PATH_LEN-1);CHKERRQ(ierr);
	ierr = PetscViewerASCIIPrintf(viewer,"## =============================================================== \n");CHKERRQ(ierr);
	ierr = PetscViewerASCIIPrintf(viewer,"##\n");CHKERRQ(ierr);
	ierr = PetscViewerASCIIPrintf(viewer,"##   pTatin options file\n");CHKERRQ(ierr);
	ierr = PetscViewerASCIIPrintf(viewer,"##\n");CHKERRQ(ierr);
	ierr = PetscViewerASCIIPrintf(viewer,"##   Generated by user: %s\n",username);CHKERRQ(ierr);
	ierr = PetscViewerASCIIPrintf(viewer,"##   Date             : %s\n",date);CHKERRQ(ierr);
	ierr = PetscViewerASCIIPrintf(viewer,"##   Machine          : %s\n",machine);CHKERRQ(ierr);
	ierr = PetscViewerASCIIPrintf(viewer,"##\n");CHKERRQ(ierr);
	ierr = PetscViewerASCIIPrintf(viewer,"## =============================================================== \n");CHKERRQ(ierr);
	
	/* write options */
	ierr = PetscOptionsView(viewer);CHKERRQ(ierr);
	
	ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "pTatinPhysCompActivated"
PetscErrorCode pTatinPhysCompActivated(pTatinCtx ctx,PhysComponents phys,PetscBool *active)
{
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	*active = PETSC_FALSE;
	switch (phys) {
		case PhysComp_Stokes:
			*active = PETSC_TRUE; /* always active */
			break;
		case PhysComp_Energy:
			if (ctx->phys_energy != PETSC_NULL) {
				*active = PETSC_TRUE;
			}
		    break;
		case PhysComp_SPM:
			if (ctx->phys_spm != PETSC_NULL) {
				*active = PETSC_TRUE;
			}
			break;
		case PhysComp_Darcy:
			if (ctx->phys_darcy != PETSC_NULL) {
				*active = PETSC_TRUE;
			}	
			break;
		default:
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"No default physics component prescribed");
			break;
	}
	
	PetscFunctionReturn(0);
}

#include "src/snes/impls/ls/lsimpl.h"
#undef __FUNCT__  
#define __FUNCT__ "SNESLineSearchRelaxed"
PetscErrorCode  SNESLineSearchRelaxed(SNES snes,void *lsctx,Vec x,Vec f,Vec g,Vec y,Vec w,PetscReal fnorm,PetscReal xnorm,PetscReal *ynorm,PetscReal *gnorm,PetscBool  *flag)
{
  PetscErrorCode ierr;
  SNES_LS        *neP = (SNES_LS*)snes->data;
  PetscBool      changed_w = PETSC_FALSE,changed_y = PETSC_FALSE;
	PetscScalar    picard_eps = 1.0;
	
  PetscFunctionBegin;
	
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-snes_picard_relaxation",&picard_eps,PETSC_NULL);CHKERRQ(ierr);
	
  *flag = PETSC_TRUE; 
  ierr = PetscLogEventBegin(SNES_LineSearch,snes,x,f,g);CHKERRQ(ierr);
  ierr = VecNorm(y,NORM_2,ynorm);CHKERRQ(ierr);         /* ynorm = || y || */
  ierr = VecWAXPY(w,-picard_eps,y,x);CHKERRQ(ierr);            /* w <- x - eps.y   */
  if (neP->postcheckstep) {
		ierr = (*neP->postcheckstep)(snes,x,y,w,neP->postcheck,&changed_y,&changed_w);CHKERRQ(ierr);
  }
  if (changed_y) {
    ierr = VecWAXPY(w,-picard_eps,y,x);CHKERRQ(ierr);            /* w <- x - eps.y   */
  }
  ierr = SNESComputeFunction(snes,w,g);CHKERRQ(ierr);
  if (!snes->domainerror) {
    ierr = VecNorm(g,NORM_2,gnorm);CHKERRQ(ierr);  /* gnorm = || g || */
    if (PetscIsInfOrNanReal(*gnorm)) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_FP,"User provided compute function generated a Not-a-Number");
  }
  ierr = PetscLogEventEnd(SNES_LineSearch,snes,x,f,g);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "SNESLineSearchAdaptiveRelaxed"
PetscErrorCode  SNESLineSearchAdaptiveRelaxed(SNES snes,void *lsctx,Vec x,Vec f,Vec g,Vec y,Vec w,PetscReal fnorm,PetscReal xnorm,PetscReal *ynorm,PetscReal *gnorm,PetscBool  *flag)
{
  PetscErrorCode ierr;
  SNES_LS        *neP = (SNES_LS*)snes->data;
  PetscBool      changed_w = PETSC_FALSE,changed_y = PETSC_FALSE;
	static PetscScalar    picard_eps_init = 1.0;
//	static PetscScalar    picard_eps_reduction = 0.05; /* fixed reduction */
	static PetscScalar    picard_eps_reduction = 0.80; /* percentage reduction */
	PetscInt       iteration;
	PetscScalar    picard_eps;
	static PetscReal flast = 0.0;
	
  PetscFunctionBegin;
	
	ierr = SNESGetIterationNumber(snes,&iteration);CHKERRQ(ierr);

	/* get initial eps */
	if (iteration==0) {
		/* reset the defaults  for the next time we call SNESSolve */
		picard_eps_init = 1.0;
		picard_eps_reduction = 0.80;
		/* check for command line args */
		ierr = PetscOptionsGetScalar(PETSC_NULL,"-snes_picard_relaxation",&picard_eps_init,PETSC_NULL);CHKERRQ(ierr);
		ierr = PetscOptionsGetScalar(PETSC_NULL,"-snes_picard_relaxation_reduction",&picard_eps_reduction,PETSC_NULL);CHKERRQ(ierr);

		PetscPrintf(((PetscObject)snes)->comm,"  SNESLineSearchAdaptiveRelaxed iteration = %d \n", iteration );
		PetscPrintf(((PetscObject)snes)->comm,"    picard_relaxation           %1.4e \n", picard_eps_init );
		PetscPrintf(((PetscObject)snes)->comm,"    picard_relaxation_reduction %1.4e \n", picard_eps_reduction );
	}
	picard_eps = picard_eps_init;

	if (iteration!=0) {
#if 0		
		if (fnorm>flast) {
			PetscScalar picard_eps_old = picard_eps_init;
			
			picard_eps_init = PetscMax(1.0e-6,picard_eps_init - picard_eps_reduction);
			picard_eps = picard_eps_init;
			PetscPrintf(((PetscObject)snes)->comm,"  SNESLineSearchAdaptiveRelaxed iteration = %d \n", iteration );
			PetscPrintf(((PetscObject)snes)->comm,"    fnorm increased, reducing picard_eps from %1.4e to %1.4e \n", picard_eps_old, picard_eps );
		}
#endif
		if (fnorm>flast) {
			PetscScalar picard_eps_old = picard_eps_init;
			
			picard_eps_init = PetscMax(1.0e-6,picard_eps_init*picard_eps_reduction);
			picard_eps = picard_eps_init;
			PetscPrintf(((PetscObject)snes)->comm,"  SNESLineSearchAdaptiveRelaxed iteration = %d \n", iteration );
			PetscPrintf(((PetscObject)snes)->comm,"    fnorm increased, reducing picard_eps from %1.4e to %1.4e \n", picard_eps_old, picard_eps );
			PetscPrintf(((PetscObject)snes)->comm,"    using picard eps = %1.4e \n", picard_eps);
		}

	}
	
  *flag = PETSC_TRUE; 
  ierr = PetscLogEventBegin(SNES_LineSearch,snes,x,f,g);CHKERRQ(ierr);
  ierr = VecNorm(y,NORM_2,ynorm);CHKERRQ(ierr);         /* ynorm = || y || */
  ierr = VecWAXPY(w,-picard_eps,y,x);CHKERRQ(ierr);            /* w <- x - eps.y   */
  if (neP->postcheckstep) {
		ierr = (*neP->postcheckstep)(snes,x,y,w,neP->postcheck,&changed_y,&changed_w);CHKERRQ(ierr);
  }
  if (changed_y) {
    ierr = VecWAXPY(w,-picard_eps,y,x);CHKERRQ(ierr);            /* w <- x - eps.y   */
  }
  ierr = SNESComputeFunction(snes,w,g);CHKERRQ(ierr);
  if (!snes->domainerror) {
    ierr = VecNorm(g,NORM_2,gnorm);CHKERRQ(ierr);  /* gnorm = || g || */
    if (PetscIsInfOrNanReal(*gnorm)) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_FP,"User provided compute function generated a Not-a-Number");
  }
  ierr = PetscLogEventEnd(SNES_LineSearch,snes,x,f,g);CHKERRQ(ierr);

	flast = fnorm;
  
	PetscFunctionReturn(0);
}

/* configure uu split for galerkin multi-grid */
#undef __FUNCT__  
#define __FUNCT__ "_StokesKSPFSMGuSetUp"
PetscErrorCode _StokesKSPFSMGuSetUp(DM dm,KSP stokes_ksp,PetscInt nlevels,Mat interpolation[])
{
	PetscBool same;
	PetscInt  k,nsplits;
	PC        stokes_pc,pc_u;
	KSP       *sub_ksp,ksp_i;
	PetscErrorCode ierr;
	
	ierr = KSPGetPC(stokes_ksp,&stokes_pc);CHKERRQ(ierr);
	
	ierr = PetscTypeCompare((PetscObject)stokes_pc,PCFIELDSPLIT,&same);CHKERRQ(ierr);
	if (!same) {
		PetscPrintf(PETSC_COMM_WORLD,"StokesKSPFSMGuSetUp: requires you use option -%s_pc_type fieldsplit \n",((PetscObject)stokes_ksp)->prefix);
		PetscFunctionReturn(0);
	}
	
	/* force setup to enable access to the fieldsplit ksp/pc's */
	ierr = KSPSetUp(stokes_ksp);CHKERRQ(ierr);
	
	ierr = PCFieldSplitGetSubKSP(stokes_pc,&nsplits,&sub_ksp);CHKERRQ(ierr);			
	ierr = KSPGetPC(sub_ksp[0],&pc_u);CHKERRQ(ierr);
	
	ierr = PetscTypeCompare((PetscObject)pc_u,PCMG,&same);CHKERRQ(ierr);
	if (same) {
		ierr = PCMGSetLevels(pc_u,nlevels,PETSC_NULL);CHKERRQ(ierr);
		ierr = PCMGSetType(pc_u,PC_MG_MULTIPLICATIVE);CHKERRQ(ierr);
		ierr = PCMGSetGalerkin(pc_u,PETSC_TRUE);CHKERRQ(ierr);
		
		for( k=1; k<nlevels; k++ ){
			ierr = PCMGSetInterpolation(pc_u,k,interpolation[k]);CHKERRQ(ierr);
		}
	}
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "StokesKSPFSMGuSetUp"
PetscErrorCode StokesKSPFSMGuSetUp(DM dm,KSP stokes_ksp,PetscInt nlevels,Mat interpolation[])
{
	PetscBool same;
	PetscInt  k,nsplits;
	PC        stokes_pc,pc_u;
	KSP       *sub_ksp,ksp_i;
	PetscErrorCode ierr;
	
	ierr = KSPGetPC(stokes_ksp,&stokes_pc);CHKERRQ(ierr);
	
	ierr = PetscTypeCompare((PetscObject)stokes_pc,PCFIELDSPLIT,&same);CHKERRQ(ierr);
	if (!same) {
		PetscPrintf(PETSC_COMM_WORLD,"StokesKSPFSMGuSetUp: requires you use option -%s_pc_type fieldsplit \n",((PetscObject)stokes_ksp)->prefix);
		PetscFunctionReturn(0);
	}
	
	/* force setup to enable access to the fieldsplit ksp/pc's */
	ierr = KSPSetUp(stokes_ksp);CHKERRQ(ierr);
	
	ierr = PCFieldSplitGetSubKSP(stokes_pc,&nsplits,&sub_ksp);CHKERRQ(ierr);			
	ierr = KSPGetPC(sub_ksp[0],&pc_u);CHKERRQ(ierr);
	
	ierr = PetscTypeCompare((PetscObject)pc_u,PCMG,&same);CHKERRQ(ierr);
	if (same) {
		IS *is;
		Mat B,Buu;
		KSP smoother;
		
		ierr = DMCompositeGetGlobalISs(dm,&is);CHKERRQ(ierr);
		ierr = KSPGetOperators(stokes_ksp,0,&B,0);CHKERRQ(ierr);
		ierr = MatGetSubMatrix(B,is[0],is[0],MAT_INITIAL_MATRIX,&Buu);CHKERRQ(ierr);
		
		ierr = KSPSetOperators(sub_ksp[0],Buu,Buu,SAME_NONZERO_PATTERN);CHKERRQ(ierr);
		
		ierr = PCMGSetLevels(pc_u,nlevels,PETSC_NULL);CHKERRQ(ierr);
		ierr = PCMGSetType(pc_u,PC_MG_MULTIPLICATIVE);CHKERRQ(ierr);
		ierr = PCMGSetGalerkin(pc_u,PETSC_TRUE);CHKERRQ(ierr);
		
		ierr = PCMGGetSmoother(pc_u,nlevels-1,&smoother);CHKERRQ(ierr);
		ierr = KSPSetOperators(smoother,Buu,Buu,SAME_NONZERO_PATTERN);CHKERRQ(ierr);
		
		for( k=1; k<nlevels; k++ ){
			ierr = PCMGSetInterpolation(pc_u,k,interpolation[k]);CHKERRQ(ierr);
		}
	}
	PetscFunctionReturn(0);
}
