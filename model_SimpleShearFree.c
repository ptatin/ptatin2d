/*
 
 Model Description:

 hese functions define the setup for an isoviscous simple shear problem with free surface in a gravity field assuming le shear is distributed
 the solution for pressure is  P = rho g y 
 Domain is [0,1,-1, 0] with free surface at the jmax, free slip at the jmin 
 and imin and imax face have the same gradient of viscosity along y.
 
  Input / command line parameters:
 -simpleShearFree_eta   : viscosity (default =1.0);; 
 -simpleShearFree_body  : rho*g (default =-1.0); 
 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"

/* ================= SimpleShearFree ================= */

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelOutput_SimpleShearFree"
PetscErrorCode pTatin2d_ModelOutput_SimpleShearFree(pTatinCtx ctx,Vec X,const char prefix[])
{
	PetscErrorCode ierr;

	ierr = pTatin2d_ModelOutput_Default(ctx,X,prefix);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_SimpleShearFree"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_SimpleShearFree(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
	
	ierr = DMDASetUniformCoordinates(ctx->dav,0.0,1.0, -1.0 ,0.0, 0.0,0.0);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_SimpleShearFree"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_SimpleShearFree(pTatinCtx ctx)
{
	PetscInt e,ncells;
	PetscInt p,ngp;
	GaussPointCoefficientsStokes *gausspoints;
	PetscReal opts_eta,opts_body;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	opts_eta  = 1.0;
	opts_body = 1.0;

	
	ierr = PetscOptionsGetReal(PETSC_NULL,"-SimpleShearFree_eta",&opts_eta,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-SimpleShearFree_body",&opts_body,0);CHKERRQ(ierr);

	
	ncells = ctx->Q->ncells;
	ngp    = ctx->Q->ngp;
	ctx->rheology_constants.rheology_type = RHEOLOGY_VISCOUS;

	for (e=0;e<ncells;e++) {
		ierr = QuadratureStokesGetCell(ctx->Q,e,&gausspoints);CHKERRQ(ierr);
		
		for (p=0; p<ngp; p++) {
			/*viscosity*/
			gausspoints[p].eta     = opts_eta;
			gausspoints[p].eta_ref = opts_eta;
			/* rhs */
			gausspoints[p].Fu[0] = 0.0;
			gausspoints[p].Fu[1] = opts_body;
		}
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_SimpleShearFree"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_SimpleShearFree(pTatinCtx ctx)
{
	BCList         ubclist;
	DM             dav;
	PetscErrorCode ierr;
	BC_Poly        Poly_data;
	PetscScalar    bcval;	
	PetscFunctionBegin;
	
	ubclist = ctx->u_bclist;
	dav     = ctx->dav;

	/* allocate memory for the user structure for the bc info */
	ierr = PetscMalloc(sizeof(struct _p_BC_Poly),&Poly_data);CHKERRQ(ierr);
	Poly_data->dim =1;
	Poly_data->beg =-1.0;
	Poly_data->end =0.0;
	Poly_data->a   =1.0;	 
	Poly_data->b   =1.0;
	Poly_data->c   =0.0;	 
	ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_Poly,(void*)Poly_data);CHKERRQ(ierr);	
	
	ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_Poly,(void*)Poly_data);CHKERRQ(ierr);
	
	bcval = 0;
	ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	
	/* clean up user struct */
	ierr = PetscFree(Poly_data);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}





