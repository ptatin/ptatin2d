/*
 *  stokes_visco_plasticT.h
 *  
 *
 *  Created by laetitia le pourhiet on 6/11/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef __pTatin_stokes_visco_plasticT_h__
#define __pTatin_stokes_visco_plasticT_h__

PetscErrorCode EvaluateRheologyNonlinearitiesMarkers_ViscoPlasticT(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[],DM daT,PetscScalar LA_T[]);
PetscErrorCode EvaluateRheologyNonlinearitiesMarkers_ViscoPlasticStrainWeakeningT(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[],DM daT,PetscScalar LA_T[]);

#endif
