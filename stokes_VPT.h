//
//  stokes_VPT.h
//  
//
//  Created by le pourhiet laetitia on 6/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef _stokes_VPT_h
#define _stokes_VPT_h


PetscErrorCode StokesEvaluateRheologyNonLinearitiesQuadraturePoints_VPT(pTatinCtx user,DM dau,PetscScalar LA_u[],DM dap,PetscScalar LA_p[]);
PetscErrorCode StokesEvaluateRheologyNonLinearitiesMarkers_VPT(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[]);

PetscErrorCode TkUpdateRheologyMarkerStokes_VPT(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[]);
PetscErrorCode TkUpdateRheologyQuadraturePoints_VPT(pTatinCtx user,DM dau);

PetscErrorCode ApplyContinuationViscousCutOffMarker(pTatinCtx user);
PetscErrorCode ApplyContinuationViscousCutOffQuadrature(pTatinCtx user);

PetscErrorCode Compute_melt_fraction(RheologyConstants *rheology,PetscInt phase_mp,PetscScalar T_mp,PetscScalar P_mp,float *melt_fraction);
PetscErrorCode ApplyPlasticSofteningMarker(RheologyConstants *rheology,PetscInt phase_mp, PetscScalar *Co, PetscScalar *phi, MPntPStokesPl *mpprop_stokespl, PetscScalar strainrateinv_mp,PetscInt step);
PetscErrorCode ApplyPlasticSofteningQuadraturePoint(RheologyConstants *rheology,PetscInt phase_mp,PetscScalar *pCo, PetscScalar *pphi, PetscScalar eplastic);

#endif
