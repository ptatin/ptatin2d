/*
 *  AVD2d.h
 *  
 *
 *  Created by Dave A. May on 11/16/11.
 *  Copyright 2011 Geophysical Fluid Dynamics. All rights reserved.
 *
 *  The algorithm computes an Approximate Voronoi Diagram (AVD) in 2D using a given set of point coordinates.
 *  
 *  The AVD algorithm, is described in:
 *    M. Velic, D.A. May & L. Moresi,
 *    "A Fast Robust Algorithm for Computing Discrete Voronoi Diagrams",
 *    Journal of Mathematical Modelling and Algorithms,
 *    Volume 8, Number 3, 343-355, DOI: 10.1007/s10852-008-9097-6
 *
 *
 *  Notes:
 *    This implementation uses von-Neumann neighbourhoods for boundary chain growth. 
 *    Do not be tempted to implement "diagonal" neighbourhood growth cycles - this will greatly increase the
 *    size of the boundary chain (and thus memory usage will increase and CPU time will decrease).
 *
 *  Compilation:
 *    gcc -O3 -c AVD2d.c
 *
 */

#ifndef __AVD2d_h__
#define __AVD2d_h__

typedef long int Aint;
#define AVD_TRUE  'T'
#define AVD_FALSE 'F'
#define AVD_CELL_MASK -2
#define AVD_CELL_UNCLAIMED -1

#define AVD_VON_NEUMANN_NEIGHBOURS

#define __AVD_DEBUG_MODE


typedef struct _p_AVDCell2d *AVDCell2d;
typedef struct _p_AVDChain *AVDChain;
typedef struct _p_AVDPoint2d *AVDPoint2d;
typedef struct _p_AVD2d *AVD2d;
	
struct _p_AVDCell2d {
	Aint p;     /* particle index which this cell is attributed to */
	Aint index; /* i + j.mx + k.mx.my */
	Aint i,j; /* cell index i,j,k*/
	char done;
};


struct _p_AVDChain {
	Aint p;
	Aint index;
	Aint length; /* current length of the boundary chain */
	Aint num_claimed;
	Aint total_claimed;
	Aint new_boundary_cells_malloced;
	Aint new_claimed_cells_malloced; 
	Aint *new_boundary_cells;
	Aint *new_claimed_cells;
	char done;
};

struct _p_AVDPoint2d {
	double x,y;
	Aint phase;
};

struct _p_AVD2d {
	double x0,x1,y0,y1; /* size of domain */
	double dx,dy;
	Aint buffer;
	Aint mx,my; /* user specified resolution */
	Aint mx_mesh,my_mesh; /* computational resolution, mx_mesh = mx + 1 */
	AVDCell2d cells;
	Aint npoints;
	AVDChain chains;
	AVDPoint2d points;
};

void AVDCell2dDestroy(AVDCell2d *C);
void AVDCell2dReset(AVD2d A);
void AVDChainDestroy(const Aint npoints,AVDChain *CH);
void AVDPoint2dCreate(const Aint npoints, AVDPoint2d *P);
void AVDPoint2dDestroy(AVDPoint2d *P);
void AVD2dCreate(const Aint mx,const Aint my,const Aint buffer,AVD2d *A);
void AVD2dDestroy(AVD2d *A);
void AVD2dSetDomainSize(AVD2d A,const double x0,const double x1,const double y0,const double y1);
void AVD2dSetPoints(AVD2d A,const Aint npoints,AVDPoint2d points);
void AVD2dClaimCells(AVD2d A,const Aint p_i);
void AVD2dUpdateChain(AVD2d A,const Aint p_i);
void AVD2dInit(AVD2d A,const Aint npoints,AVDPoint2d points);
void AVD2dReportMemory(AVD2d A);
void AVDGetTime(double *time);

/* viewers */
void AVD2dPointsView_VTU(AVD2d A,const char name[]);
void AVD2dCellsView_VTR(AVD2d A,const char name[]);
void AVD2dCellsView_appendedVTR(AVD2d A,const char name[]);
void AVD2dCellsView_VTS(AVD2d A,const char name[]);
void AVD2dCellsView_VTU(AVD2d A,const char name[]);

#endif
