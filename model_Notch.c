/*
 
 Model Description:
 Example reading geometry from file.
 This example uses "sticky-air". We delibrately define a mesh which is larger in the vertical
 direction than the phase map, just to demonstrate how to deal with the case when you query
 the phase map for information outside of the domain defined by the phase map.
 
 
 Input / command line parameters:
 
 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"

/* ========= Notch: Simple Notch model.. for more complex config a MAP_LVP model will be created soon ========== */
/*  for the moment only free surface and free slip with initial surface being horizontal, 
 TODO: 1 / Map_LVP model
 2 / create a surface adaptor 
 */ 

/* default scales for non-dimensionalisation */
const double km2m                 = 1.0e3;
const double Ma2sec               = 1.0e6 * (365.0 * 24.0 * 60.0 * 60.0 );
const double cm_per_yer2m_per_sec = 1.0e-2 / ( 365.0 * 24.0 * 60.0 * 60.0 ) ;

void GetScaling(PetscReal *length,PetscReal *viscosity,PetscReal *velocity,PetscReal *stress,PetscReal *density)
{
	PetscReal density_bar;
	PetscReal length_bar;
	PetscReal viscosity_bar;
	PetscReal velocity_bar;
	PetscReal time_bar;
	PetscReal pressure_bar;
	
	length_bar    = 10.0 * 1.0e3; /* 10 km */
	viscosity_bar = 1.0e25;
	velocity_bar  = 2.0e-11;
	time_bar      = length_bar / velocity_bar;
	pressure_bar  = viscosity_bar*velocity_bar / length_bar;
	density_bar   = viscosity_bar*velocity_bar / ( length_bar * length_bar );
	
	if(length)    *length    = length_bar;
	if(viscosity) *viscosity = viscosity_bar;
	if(velocity)  *velocity  = velocity_bar;
	if(stress)    *stress    = pressure_bar;
	if(density)   *density   = density_bar;
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelOutput_Notch"
PetscErrorCode pTatin2d_ModelOutput_Notch(pTatinCtx ctx,Vec X,const char prefix[])
{
	char *pvdfilename;
	PetscErrorCode ierr;
	
	
	if (prefix == PETSC_NULL) {
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,"qpoints");CHKERRQ(ierr);
		ierr = pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(ctx,ctx->outputpath,"faceqpoints");CHKERRQ(ierr);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,"vp");CHKERRQ(ierr);
		ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,"int_fields");CHKERRQ(ierr);
		ierr = SwarmOutputParaView_MPntPStokes(ctx->db,ctx->outputpath,"mpoints_stk");CHKERRQ(ierr);
	} else { /* exanple of adding a different prefix to the file names - not sure this is useful */
		char *name;
		
		///////////////////////////////////////////////////////////////////////////
		// PVD
		asprintf(&pvdfilename,"%s/timeseries_qpoints.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;
			
			asprintf(&vtkfilename, "%s_qpoints.pvtu",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			free(vtkfilename);
		}
		free(pvdfilename);
		// PVTU
		asprintf(&name,"%s_qpoints",prefix);
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
		
		///////////////////////////////////////////////////////////////////////////		
		// PVD
		/*		
		 if (ctx->step==0) {
		 ierr = ParaviewPVDOpen("faceqpoints_timeseries.pvd");CHKERRQ(ierr);
		 }
		 else {
		 char *vtkfilename;
		 char *filename;
		 
		 asprintf(&vtkfilename, "%s_faceqpoints.pvtu",prefix);
		 ierr = ParaviewPVDAppend("faceqpoints_timeseries.pvd",ctx->time, vtkfilename, ctx->outputpath);CHKERRQ(ierr);
		 free(vtkfilename);
		 }
		 */ 
		// PVTU
		asprintf(&name,"%s_faceqpoints",prefix);
		ierr = pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
		
		///////////////////////////////////////////////////////////////////////////
		// PVD
		asprintf(&pvdfilename,"%s/timeseries_vp.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;
			
			asprintf(&vtkfilename, "%s_vp.pvts",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			free(vtkfilename);
		}
		free(pvdfilename);
		// PVTS
		asprintf(&name,"%s_vp",prefix);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
		
		///////////////////////////////////////////////////////////////////////////
		// PVD
		asprintf(&pvdfilename,"%s/timeseries_int_fields.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;
			
			asprintf(&vtkfilename, "%s_int_fields.pvts",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			free(vtkfilename);
		}
		free(pvdfilename);
		// PVTS
		asprintf(&name,"%s_int_fields",prefix);
		ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
		
		///////////////////////////////////////////////////////////////////////////
		// PVD
		asprintf(&pvdfilename,"%s/timeseries_mpoints_stk.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;
			
			asprintf(&vtkfilename, "%s_mpoints_stk.pvtu",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			free(vtkfilename);
		}
		free(pvdfilename);
		// PVTU
		asprintf(&name,"%s_mpoints_stk",prefix);
		/* stokes fields eta,rho */
//		ierr = SwarmOutputParaView_MPntPStokes(ctx->db,ctx->outputpath,name);CHKERRQ(ierr);
		/* stokes fields + plastic fields */
		ierr = SwarmOutputParaView_MPntPStokesPl(ctx->db,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
		//////////////
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_Notch"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_Notch(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	PetscFunctionBegin;
	PetscReal  opts_Lx,opts_Ly,Lbar;	
	PetscInt i,j,si,sj,m,n,M,N,ndof;
	DM cda;
	Vec coords;
	DMDACoor2d **LA_coords;	
	/*set the model size */ 
	opts_Lx   = 60.0 * 1.0e3;
	opts_Ly   = 15.0 * 1.0e3;
	GetScaling(&Lbar,0,0,0,0);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_Lx",&opts_Lx,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_Ly",&opts_Ly,0);CHKERRQ(ierr);  
	opts_Lx = opts_Lx / Lbar;
	opts_Ly = opts_Ly / Lbar;
	ierr = DMDASetUniformCoordinates(ctx->dav,0.0,opts_Lx, 0.0,opts_Ly, 0.0,0.0);CHKERRQ(ierr);
	
  /* to perturb coordinates kept as reminder for future fusion with map lv model */
#if 0  
  ierr = DMDAGetInfo(ctx->dav,0, &M,&N,0, 0,0,0, &ndof,0, 0,0,0, 0);CHKERRQ(ierr);
	ierr = DMDAGetCorners(ctx->dav,&si,&sj,0,&m,&n,0);CHKERRQ(ierr);
	ierr = DMDAGetCoordinateDA(ctx->dav,&cda);CHKERRQ(ierr);
	ierr = DMDAGetCoordinates(ctx->dav,&coords);CHKERRQ(ierr);
	if (!coords) { SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Coordinates must be set"); }
	ierr = DMDAVecGetArray(cda,coords,&LA_coords);CHKERRQ(ierr);
  
	ierr = DMDASetUniformCoordinates(ctx->dav,phasemap->x0,phasemap->x1, phasemap->y0,phasemap->y1, 0.0,0.0);CHKERRQ(ierr);
#endif	
	/* perturb the bottom */
#if 0			
  if (sj==0) {
    j = 0;
    for (i=si; i<si+m; i++) {
      LA_coords[j][i].y = LA_coords[j][i].y+0.05 * opts_Ly/(ctx->my)*sin(3.14*LA_coords[j][i].x/opts_Lx*4) ;
		}
  }
#endif  
#if 0	
	/* perturb the top */
	
  if (sj+n==N) {
    j = sj+n-1;
    for (i=si; i<si+m; i++) {
			LA_coords[j][i].y = LA_coords[j][i].y-0.05 * opts_Ly/(ctx->my)*sin(3.14*LA_coords[j][i].x/opts_Lx*4) ;			
    }
  }
#endif
  
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_Notch"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_Notch(pTatinCtx ctx)
{
	PetscInt               e,p,n_mp_points;
	DataBucket             db;
	DataField              PField_std,PField_stokes,PField_stokespl;
	PetscScalar            eta,rho;
	PetscReal  opts_LayerVis, opts_LayerCo, opts_LayerPhi, opts_LayerTens, opts_LayerRho, opts_LayerHS;
	PetscReal  opts_NotchVis, opts_NotchCo, opts_NotchPhi, opts_NotchTens, opts_NotchRho, opts_NotchHS;
	PetscReal  opts_NotchMaxX,opts_NotchMinX,opts_NotchMaxY;
	RheologyConstants *rheology;
	int                    phase_init, phase, phase_index, is_valid, i;
	PetscReal              Lbar,ETAbar,Vbar,Pbar,RHObar;
	PetscErrorCode         ierr;
	
	PetscFunctionBegin;
	
	
	
	rheology  = &ctx->rheology_constants;
	ctx->rheology_constants.rheology_type = RHEOLOGY_VPT_STD;
// _STRAIN_WEAKENING;
	
	GetScaling(&Lbar,&ETAbar,&Vbar,&Pbar,&RHObar);

	ierr = QuadratureStokesGravityModelCoordinateAlignedSetValues(ctx->Q,0.0,-10.0/RHObar);CHKERRQ(ierr);
	
	
	opts_LayerRho  = 2700.0;
	ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_LayerRho",&opts_LayerRho,0);CHKERRQ(ierr);

	opts_LayerVis  = 1.0e25;
	ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_LayerVis",&opts_LayerVis,0);CHKERRQ(ierr);	
	
	opts_LayerCo   = 10.0 * 1.0e6;
	ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_LayerCo",&opts_LayerCo,0);CHKERRQ(ierr);
	
	opts_LayerPhi  = 30.0;
	ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_LayerPhi",&opts_LayerPhi,0);CHKERRQ(ierr); 
	
	opts_LayerHS   = 1000.0 * 1.0e6;
	ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_LayerHS",&opts_LayerHS,0);CHKERRQ(ierr); 	
	
	opts_NotchTens = 1.0 * 1.0e3;
	ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_LayerTens",&opts_LayerTens,0);CHKERRQ(ierr);	
	


	opts_NotchRho  = 2700.0;
	ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_NotchRho",&opts_NotchRho,0);CHKERRQ(ierr);

	opts_NotchVis  = 1.0e20;
	ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_NotchVis",&opts_NotchVis,0);CHKERRQ(ierr);
	
	opts_NotchCo   = 1.0e32;
	ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_NotchCo",&opts_NotchCo,0);CHKERRQ(ierr);
	
	opts_NotchPhi  = 0.0;
	ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_NotchPhi",&opts_NotchPhi,0);CHKERRQ(ierr);	  
	
	opts_NotchHS   = 1.0e32;
	ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_NotchHS",&opts_LayerHS,0);CHKERRQ(ierr); 	
	
	opts_NotchTens = 1.0e32;
	ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_NotchTens",&opts_LayerTens,0);CHKERRQ(ierr);	
	
	
	rheology->const_eta0[0]             = opts_LayerVis   / ETAbar;
	rheology->mises_tau_yield[0]        = opts_LayerCo    / Pbar;
	rheology->dp_pressure_dependance[0] = opts_LayerPhi; 
	rheology->tens_cutoff[0]            = opts_LayerTens  / Pbar;
	rheology->Hst_cutoff[0]             = opts_LayerHS    / Pbar;

	rheology->soft_min_strain_cutoff[0] = 0.0; 
	rheology->soft_max_strain_cutoff[0] = 0.1;	
	rheology->soft_Co_inf[0]            = (opts_LayerCo/1000.0) / Pbar;
	rheology->soft_phi_inf[0]           = opts_LayerPhi;
	rheology->const_rho0[0]             = opts_LayerRho;

	rheology->viscous_type[0]           = VISCOUS_CONSTANT;
	rheology->plastic_type[0]           = PLASTIC_DP;
	rheology->softening_type[0]         = SOFTENING_NONE;
	


	rheology->const_eta0[1]             = opts_NotchVis  / ETAbar;
	rheology->mises_tau_yield[1]        = opts_NotchCo   / Pbar;
	rheology->dp_pressure_dependance[1] = opts_NotchPhi; 
	rheology->tens_cutoff[1]            = opts_NotchTens  / Pbar; 
	rheology->Hst_cutoff[1]             = opts_NotchHS    / Pbar;

	rheology->soft_min_strain_cutoff[1] = 1.0e100; 
	rheology->soft_max_strain_cutoff[1] = 1.0e100;	
	rheology->soft_Co_inf[1]            = opts_NotchCo    / Pbar;
	rheology->soft_phi_inf[1]           = opts_NotchPhi;
	rheology->const_rho0[1]             = opts_NotchRho;

	rheology->viscous_type[1]           = VISCOUS_CONSTANT;
	rheology->plastic_type[1]           = PLASTIC_NONE;
	rheology->softening_type[1]         = SOFTENING_NONE;



	//Set global Cut off for viscosity here but it is maybe not the best place
	rheology->eta_upper_cutoff_global = opts_LayerVis / ETAbar;
	rheology->eta_lower_cutoff_global = opts_NotchVis / ETAbar;
	
	opts_NotchMaxY   = 500.0;
	opts_NotchMinX   = 30.0*1.0e3 - 0.5 * 500.0;
	opts_NotchMaxX   = 30.0*1.0e3 + 0.5 * 500.0;
	ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_NotchMaxY",&opts_NotchMaxY,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_NotchMinX",&opts_NotchMinX,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-notch_NotchMaxX",&opts_NotchMaxX,0);CHKERRQ(ierr);
  opts_NotchMaxY = opts_NotchMaxY / Lbar;
  opts_NotchMinX = opts_NotchMinX / Lbar;
  opts_NotchMaxX = opts_NotchMaxX / Lbar;
	
	
	/* define phase on material points */
	db = ctx->db;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));
	
	/* get the plastic variables */
	DataBucketGetDataFieldByName(db,MPntPStokesPl_classname,&PField_stokespl);
	DataFieldGetAccess(PField_stokespl);
	DataFieldVerifyAccess(PField_stokespl,sizeof(MPntPStokesPl));
	
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	for (p=0; p<n_mp_points; p++) {
		MPntStd       *material_point;
		MPntPStokes   *mpprop_stokes; /* stokes variables */
		MPntPStokesPl *mpprop_stokespl; /* plastic variables */
		double  *position;
		
		DataFieldAccessPoint(PField_std,p,(void**)&material_point);
		DataFieldAccessPoint(PField_stokes,p,(void**)&mpprop_stokes);
		DataFieldAccessPoint(PField_stokespl,p,(void**)&mpprop_stokespl);
		
		/* Access using the getter function  (recommeneded for beginner user) */
		MPntStdGetField_global_coord(material_point,&position);
		
		/* Layer default */
		phase = 0;
		rho   = opts_LayerRho; 
		eta   = rheology->const_eta0[0];
		
		if ((position[1] < opts_NotchMaxY) && (position[0] > opts_NotchMinX) && (position[0] < opts_NotchMaxX) ) {
			/* point located in the notch */
			phase = 1;
			rho   = opts_NotchRho; 
			eta   = rheology->const_eta0[1];
			
		}	
		/* asign phases and initializes viscosity to ref viscosity*/
		MPntStdSetField_phase_index(material_point,phase);
		MPntPStokesSetField_density(mpprop_stokes,rho);
		MPntPStokesSetField_eta_effective(mpprop_stokes,eta);

	}
	
	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokes);
	DataFieldRestoreAccess(PField_stokespl);
	
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_Notch"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_Notch(pTatinCtx ctx)
{
	BCList         ubclist;
	PetscScalar    bcval;
	DM             dav;
	PetscErrorCode ierr;
  PetscReal jmin[3],jmax[3],Vbar; 
	PetscScalar opts_srH, opts_Lx,opts_Ly;
  PetscInt opts_bcs;
	PetscScalar alpha_m;
	
	PetscFunctionBegin;
	
	ubclist = ctx->u_bclist;
	dav     = ctx->dav;
  
  opts_srH = -2.0 * 1.0e-11;
 	ierr = PetscOptionsGetScalar(PETSC_NULL,"-vx",&opts_srH,0);CHKERRQ(ierr);
	GetScaling(0,0,&Vbar,0,0);
	opts_srH = opts_srH/Vbar;
  
	opts_bcs = 0;
  ierr = PetscOptionsGetInt(PETSC_NULL,"-bc_type",&opts_bcs,0);CHKERRQ(ierr);
  
  
  DMDAGetBoundingBox(dav,jmin,jmax);
  opts_Lx = jmax[0]-jmin[0]; 
  opts_Ly = jmax[1]-jmin[1];
  PetscPrintf(PETSC_COMM_WORLD,"domain x := [ %1.4e , %1.4e ] \n", jmin[0], jmax[0] );
  PetscPrintf(PETSC_COMM_WORLD,"domain y := [ %1.4e , %1.4e ] \n", jmin[1], jmax[1] );
	
  switch(opts_bcs){
		// free surface and free slip all side with symetric horizontal velocity
    case 0 : 
		{
      bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
      bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

      break; 
    }
      
		// incompressible symetric bottom fixed 
    case 1 : 
		{
      bcval = -opts_srH*2.0*opts_Ly/opts_Lx;  
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
      bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

      break; 
    }
      
		// incompressible right side  
		case 2 : 
		{
      bcval = -opts_srH*opts_Ly/opts_Lx;  
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
      bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      
			break;
		}
      
		// incompressible 4 sides  
    case 3 : {      
      bcval = -opts_srH*opts_Ly/opts_Lx;  
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      
      bcval = opts_srH*opts_Ly/opts_Lx;  
			ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      
      bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
      bcval =  opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

      break;
    }
      
		// free slip everywhere  
    case 4 : 
		{
      bcval = 0.0;  
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      break;      
		}
			
		// free surface both side with mid point fixed 
    case 5 :  
		{
      bcval = opts_srH;  
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      bcval = 0.0;  
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_MIDPOINT,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
      ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_MIDPOINT,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);      

      break;
		}
			
		// free surface right side  
    case 6 : 
		{
      bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
      bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

      break;
		}  
      
    // free surface left side  
    case 7 : 
		{
      bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
      bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			bcval = 0.0;       ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

      break;
		}
      
		default: 
		{
			SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"Boundary condition specified is unknown");
			break;
		}
  } 
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelUpdateMeshGeometry_Notch"
PetscErrorCode pTatin2d_ModelUpdateMeshGeometry_Notch(pTatinCtx ctx,Vec X)
{
	PetscReal step;
	Vec velocity,pressure;
	DM dav,dap;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	step = ctx->dt;
  
	ierr = DMCompositeGetEntries(ctx->pack,&dav,&dap);CHKERRQ(ierr);
	ierr = DMCompositeGetAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
  
  //	ierr = UpdateMeshGeometry_FullLagSurface(dav,velocity,step);CHKERRQ(ierr);
  //  ierr = UpdateMeshGeometry_VerticalDisplacement(dav,velocity,step);CHKERRQ(ierr);
	ierr = UpdateMeshGeometry_FullLagSurfaceJMAX_RemeshInteriorJMIN_JMAX(dav,velocity,step);CHKERRQ(ierr);
	
	ierr = DMCompositeRestoreAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
  
	PetscFunctionReturn(0);
}
