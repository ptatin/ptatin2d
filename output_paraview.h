
#ifndef __output_paraview_h_
#define __output_paraview_h_

#include <pTatin2d.h>

typedef enum { 
	PField_Pressure=0, 
	PField_Viscosity, 
	PField_Sxx,PField_Sxy,PField_Syy,
	PField_Txx,PField_Txy,PField_Tyy,
	PField_Exx,PField_Exy,PField_Eyy,
	PField_S2,
	PField_T2,
	PField_E2,
	__PField_TERMINATOR__ } PTatinField;

PetscErrorCode pTatinGenerateParallelVTKName(const char prefix[],const char suffix[],char **name);
PetscErrorCode pTatinGenerateVTKName(const char prefix[],const char suffix[],char **name);
PetscErrorCode CellIntegration(PetscScalar celldata[],PTatinField FIELD,pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[]);
PetscErrorCode DAQ2PieceExtendForGhostLevelZero( FILE *vtk_fp, int indent_level, DM dau, const char local_file_prefix[] );

PetscErrorCode pTatinOutputParaViewQuadratureStokesPointsRaw(pTatinCtx user,const char path[],const char name[]);
PetscErrorCode pTatinOutputParaViewMeshVelocityPressure(pTatinCtx user,Vec X,const char path[],const char prefix[]);
PetscErrorCode pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(pTatinCtx user,Vec X,const char path[],const char prefix[]);

PetscErrorCode pTatinCreateOutputDirectory(void);

PetscErrorCode ParaviewPVDOpen(const char pvdfilename[]);
PetscErrorCode ParaviewPVDAppend(const char pvdfilename[],double time,const char datafile[], const char DirectoryName[]);

#endif
