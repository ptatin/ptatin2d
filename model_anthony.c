/*
  
 
 Input / command line parameters:
 -Ttop
 -Tbot 
 -mx
 -my
 -vx -3.000000e-10
 
-ptatin_model anthony
-output_frequency 1
-output_path anthony
-remesh_type 2
-store_ages
-stokes_scale_length 1.000000e+04
-stokes_scale_velocity 1.000000e-10
-stokes_scale_viscosity 1.000000e+27
-dt_max 3e11
-nsteps 500
-time_max 2.000000e+15
 

 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"

#define WRITE_VP
#define WRITE_TFIELD
#define WRITE_MPOINTS
//#define WRITE_QPOINTS
#define WRITE_TCELL
//#define WRITE_INT_FIELDS
//#define WRITE_FACEQPOINTS


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_anthony"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_anthony(pTatinCtx ctx)
{
//	PhaseMap phasemap; //use a phase map
//	TopoMap topomap;   //use initial topogrpahy
	PetscInt M,N;      /* number of elments in x and y */
	PetscErrorCode ierr;
	PetscReal Ox,Oy,Lx,Ly,gravity_acceleration;
	PetscScalar OOx[21],OOy[21];
	PetscInt Nx[20], Ny[20],test,ndomainx,ndomainy;
    PetscInt nlimx,nlimy, i,j;
    PhysCompEnergyCtx phys_energy;
    DM daT;
	pTatinUnits *units;
	PetscFunctionBegin;
	units = &ctx->units;
	PetscBool              notuniform_mesh=PETSC_FALSE;
    
	/* set gravity */
    gravity_acceleration = 9.81;
    ierr = QuadratureStokesGravityModelCoordinateAlignedSetValues(ctx->Q,0.0, -gravity_acceleration);CHKERRQ(ierr);
    /* set model size*/
    Ox = 0.0;
    Oy = -200.e3;
    Lx = 600.e3;
    Ly = 0.0;
	ierr  = PetscOptionsGetReal(PETSC_NULL,"-Ox",&Ox,0);CHKERRQ(ierr);
	ierr  = PetscOptionsGetReal(PETSC_NULL,"-Oy",&Oy,0);CHKERRQ(ierr);
	ierr  = PetscOptionsGetReal(PETSC_NULL,"-Lx",&Lx,0);CHKERRQ(ierr);
	ierr  = PetscOptionsGetReal(PETSC_NULL,"-Ly",&Ly,0);CHKERRQ(ierr);
	PetscPrintf(PETSC_COMM_WORLD,"ApplyInitialMeshGeometry: options Domain_SI = [ %1.4e , %1.4e ] x [ %1.4e , %1.4e ] \n", Ox, Lx,Oy,Ly );	
	UnitsApplyInverseScaling(units->si_length,Ox,&Ox);
	UnitsApplyInverseScaling(units->si_length,Oy,&Oy);
	UnitsApplyInverseScaling(units->si_length,Lx,&Lx);
	UnitsApplyInverseScaling(units->si_length,Ly,&Ly);
	PetscPrintf(PETSC_COMM_WORLD,"ApplyInitialMeshGeometry : options Domain_ND = [ %1.4e , %1.4e ] x [ %1.4e , %1.4e ] \n", Ox, Lx,Oy,Ly );
    /* create 2D mesh*/
	ierr = DMDASetUniformCoordinates(ctx->dav,Ox,Lx,Oy,Ly, 0.0,0.0);CHKERRQ(ierr);
	
	/* if activated (-not_uniform_mesh 1) this option will create the mesh described below */
	ierr = PetscOptionsGetBool(PETSC_NULL,"-not_uniform_mesh",&notuniform_mesh,0);CHKERRQ(ierr);
	if (notuniform_mesh){
		PetscPrintf(PETSC_COMM_WORLD,"Not Uniform mesh has been activated\n");
	/* PetscOptionsSetValue("-refine_n_domain_x","5");
    PetscOptionsSetValue("-refine_n_domain_y","3");
    PetscOptionsSetValue("-refine_x_dom_el","10,3,250,3,10");
    PetscOptionsSetValue("-refine_y_dom_el","9,30,20");
    PetscOptionsSetValue("-refine_x_lim","0.0,3.0,3.5,12.5,13.0,16.0");
    PetscOptionsSetValue("-refine_y_lim","-3.0,-1.2,-0.2,0.0");
	*/
    
		PetscOptionsGetInt(PETSC_NULL,"-refine_n_domain_x",&ndomainx,0);
		PetscOptionsGetInt(PETSC_NULL,"-refine_n_domain_y",&ndomainy,0);
	
		nlimx=ndomainx+1;
		nlimy=ndomainy+1;
		PetscOptionsGetRealArray(PETSC_NULL,"-refine_x_lim",OOx,&nlimx,PETSC_NULL);
		PetscOptionsGetRealArray(PETSC_NULL,"-refine_y_lim",OOy,&nlimy,PETSC_NULL);
    
		PetscOptionsGetIntArray(PETSC_NULL,"-refine_x_dom_el",Nx,&ndomainx,PETSC_NULL);
		PetscOptionsGetIntArray(PETSC_NULL,"-refine_y_dom_el",Ny,&ndomainy,PETSC_NULL);
    
		test=0;
		for (i=0;i<ndomainx;i++) { test = test+ Nx[i];}
		if (test != ctx->mx) {SETERRQ2(PETSC_COMM_WORLD,PETSC_ERR_SUP,"mx = %d is not equal to the number of element = %d in the refined mesh", ctx->mx, test);}
		test=0;
		for (j=0;j<ndomainy;j++) { test += Ny[j];}
		if (test != ctx->my) {SETERRQ2(PETSC_COMM_WORLD,PETSC_ERR_SUP,"my = %d is not equal to the number of element = %d in the refined mesh", ctx->my, test);}
		if (nlimx > 20 | nlimy >20) {SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"maximum refinement domain is set to 20 in the current version of the code");}

		ierr = DMDASetRefinedCoordinates(ctx->dav); CHKERRQ(ierr);
	}
	
	/* create 2D mesh for temperature aligned on the veocity mesh*/
    phys_energy = ctx->phys_energy;
    daT         = phys_energy->daT;
	ierr = DMDAProjectCoordinatesQ2toQ1_2d(ctx->dav,daT);CHKERRQ(ierr);
    
	PetscFunctionReturn(0);
}



#undef __FUNCT__ 
#define __FUNCT__ "pTatin2d_ModelSetMaterialProperty_anthony"
PetscErrorCode pTatin2d_ModelSetMaterialProperty_anthony(pTatinCtx ctx)

{
	RheologyConstants      *rheology;
	PetscInt               nphase, i, viscous_type, density_type;
	PetscErrorCode         ierr;
	char   		       *option_name;
	PetscBool              found;
	PetscBool              add_salt = PETSC_FALSE;
	PetscBool              phase_change=PETSC_FALSE;	
	char file[PETSC_MAX_PATH_LEN];
	char name[PETSC_MAX_PATH_LEN],mapname[PETSC_MAX_PATH_LEN];
	pTatinUnits *units;
	TempMap map;

	
	PetscFunctionBegin;
	
	ierr = PetscOptionsGetBool(0,"-add_salt",&add_salt,0);CHKERRQ(ierr);
	
    ctx->rheology_constants.rheology_type = 6;// use stockes vpt standard
	if(add_salt){
		nphase = 8;
	} else {
		nphase = 6;    /* or marker index  */
    }
	
    rheology   = &ctx->rheology_constants;
    rheology->eta_upper_cutoff_global = 1.e25;
	rheology->eta_lower_cutoff_global = 1.e19;
	ierr       = PetscOptionsGetReal(PETSC_NULL,"-eta_max_cut_off",&rheology->eta_upper_cutoff_global,0);CHKERRQ(ierr);
	ierr       = PetscOptionsGetReal(PETSC_NULL,"-eta_min_cut_off",&rheology->eta_lower_cutoff_global,0);CHKERRQ(ierr);
	PetscPrintf(PETSC_COMM_WORLD,"ApplyViscouscuttoff: eta min =  %1.2e , eta_max =  %1.2e \n", rheology->eta_lower_cutoff_global, rheology->eta_upper_cutoff_global);
	
    for (i=0;i<nphase; i++){
	rheology->temp_kappa[i]      = 1e-6;
	rheology->temp_prod[i]       = 0.;
//    rheology->density_type[i]    = 1;
    rheology->temp_alpha[i]      = 3e-5;
    rheology->temp_beta[i]       = 1e-11;
//    rheology->viscous_type[i]    = 2;
    rheology->arrh_Tref[i]       = 273;
	rheology->plastic_type[i]    = 2;
    rheology->melt_type[i]       = 0;
    rheology->tens_cutoff[i]     = 1.e7;
    rheology->Hst_cutoff[i]      = 4.e8;
    rheology->softening_type[i]  = 1;
	
	density_type = 0; 
	asprintf(&option_name, "-density_%d",i);
	ierr = PetscOptionsGetInt("GENE_",option_name,&density_type,&found);CHKERRQ(ierr);
	rheology->density_type[i] = density_type; 
	free(option_name);
			
	switch (rheology->density_type[i]) {
						
		case DENSITY_CONSTANT:
		{
			asprintf(&option_name, "-rho_%d",i);
			ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->const_rho0[i]),&found);CHKERRQ(ierr);
			if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
			free(option_name);
		}
			break;
			
		case DENSITY_FUNCTION:
		{
			asprintf(&option_name, "-rho_%d",i);
			ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->const_rho0[i]),&found);CHKERRQ(ierr);
			if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
			free(option_name);
			
			asprintf(&option_name, "-alpha_%d",i);
			ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->temp_alpha[i]),&found);CHKERRQ(ierr);
			if (found == PETSC_FALSE) {
				PetscPrintf(PETSC_COMM_WORLD,"no thermal expension coefficient found for rock type %d I set it to 0 \n",i);
				rheology->temp_alpha[i] = 0;
			}
			free(option_name);
			
			asprintf(&option_name, "-beta_%d",i);
			ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->temp_beta[i]),&found);CHKERRQ(ierr);
			if (found == PETSC_FALSE) {
				PetscPrintf(PETSC_COMM_WORLD,"no compressibility coefficient found for rock type %d I set it to 0 \n",i);
				rheology->temp_beta[i] = 0;
			}
			free(option_name);
		}
			break;   
			
		case DENSITY_TABLE:
		{   
			char file[PETSC_MAX_PATH_LEN];
			char name[PETSC_MAX_PATH_LEN],densmapname[PETSC_MAX_PATH_LEN];
			pTatinUnits *units;
			TempMap map;
			
			units   = &ctx->units;
			asprintf(&option_name, "-densmapfile_%d",i);
			found = PETSC_FALSE;
			ierr = PetscOptionsGetString("GENE_",option_name,file,PETSC_MAX_PATH_LEN-1,&found);CHKERRQ(ierr);
			free(option_name);
			
			if (found == PETSC_TRUE) {
				sprintf(name,"%s.densmap",file);
				TempMapLoadFromFile(name,&map);
				DensMapApplyScale(map,units);
				
				//sprintf(name,"%s_dens_map.gp",file);
				//TempMapViewGnuplot(name,map);
				
				sprintf(densmapname,"densmap_%d",i);
				ierr = pTatinCtxAttachMap(ctx,map,densmapname);CHKERRQ(ierr);
			} else { 
				SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"No density file found for rocktype %d \n",i);  
			}
		}
			break;
			
		default:
			break;
	}
				
	viscous_type = 0; 
	asprintf(&option_name, "-viscous_%d",i);
	ierr       = PetscOptionsGetInt("GENE_",option_name,&viscous_type,&found);CHKERRQ(ierr);
	free(option_name);
				
	rheology->viscous_type[i] = viscous_type; 
				
	switch (rheology->viscous_type[i]) {
  
		case VISCOUS_CONSTANT:
		{
			asprintf(&option_name, "-eta_%d",i);
			ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->const_eta0[i]),&found);CHKERRQ(ierr);
			if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
			free(option_name);
			
		}
			break;
						
		case VISCOUS_FRANKK: 
		{	
			asprintf(&option_name, "-eta_%d",i);
			ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->const_eta0[i]),&found);CHKERRQ(ierr);
			if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
			free(option_name);
			
			asprintf(&option_name, "-theta_%d",i);
			ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->temp_theta[i]),&found);CHKERRQ(ierr);
			if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
			free(option_name);
		}
			break;   
						
		case VISCOUS_ARRHENIUS:
		{

			asprintf(&option_name, "-nexp_%d",i);
			ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->arrh_nexp[i]),&found);CHKERRQ(ierr);
			if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
			free(option_name);
			
			asprintf(&option_name, "-entalpy_%d",i);
			ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->arrh_entalpy[i]),&found);CHKERRQ(ierr);
			if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
			free(option_name);
			
			asprintf(&option_name, "-preexpA_%d",i);
			ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->arrh_preexpA[i]),&found);CHKERRQ(ierr);
			if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
			free(option_name);
			
			asprintf(&option_name, "-Ascale_%d",i);
			ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->arrh_Ascale[i]),&found);CHKERRQ(ierr);
			if (found == PETSC_FALSE){ 
				PetscPrintf(PETSC_COMM_WORLD,"User didn't provide scaling factor for A for phase %d assuming 1 \n",i);
				rheology->arrh_Ascale[i] =1.0;
			}
			free(option_name);

			asprintf(&option_name, "-Vmol_%d",i);
			ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->arrh_Vmol[i]),&found);CHKERRQ(ierr);
			if (found == PETSC_FALSE){ 
				PetscPrintf(PETSC_COMM_WORLD,"User didn't provide activation volume for phase %d assuming 0 \n",i);
				rheology->arrh_Vmol[i] =0.0;
			}
			free(option_name);
			
		}
			break;
		case VISCOUS_POWERLAW:
		{
			
			asprintf(&option_name, "-nexp_%d",i);
			ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->arrh_nexp[i]),&found);CHKERRQ(ierr);
			if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
			free(option_name);
			
			asprintf(&option_name, "-preexpA_%d",i);
			ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->arrh_preexpA[i]),&found);CHKERRQ(ierr);
			if (found == PETSC_FALSE) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"Expected user to provide value to option %s \n",option_name);
			free(option_name);
		}
			break;
		case VISCOUS_ARRHENIUS_MIXING:
		
		{
			char file[PETSC_MAX_PATH_LEN];
			char name[PETSC_MAX_PATH_LEN],mapname[PETSC_MAX_PATH_LEN];
			pTatinUnits *units;
			TempMap map;
				
			asprintf(&option_name, "-Ascale_%d",i);
			ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->arrh_Ascale[i]),&found);CHKERRQ(ierr);
			if (found == PETSC_FALSE){ 
				PetscPrintf(PETSC_COMM_WORLD,"User didn't provide scaling factor for A for phase %d assuming 1 \n",i);
				rheology->arrh_Ascale[i] =1.0;
			}
			free(option_name);

			asprintf(&option_name, "-Vmol_%d",i);
			found = PETSC_FALSE;
			ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->arrh_Vmol[i]),&found);CHKERRQ(ierr);
			if (found == PETSC_FALSE){ 
				PetscPrintf(PETSC_COMM_WORLD,"User didn't provide activation volume for phase %d assuming 0 \n",i);
				rheology->arrh_Vmol[i] =0.0;
			}
			free(option_name);
			
			asprintf(&option_name, "-Ascale_%d",i);
			found = PETSC_FALSE;
			ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->arrh_Ascale[i]),&found);CHKERRQ(ierr);
			if (found == PETSC_FALSE){ 
				PetscPrintf(PETSC_COMM_WORLD,"User didn't provide scaling factor for A for phase %d assuming 1 \n",i);
				rheology->arrh_Ascale[i] =1.0;
			}
			free(option_name);
			
			units   = &ctx->units;
			asprintf(&option_name, "-rheolmixingfile_%d",i);
			found = PETSC_FALSE;
			ierr = PetscOptionsGetString("GENE_",option_name,file,PETSC_MAX_PATH_LEN-1,&found);CHKERRQ(ierr);
			free(option_name);
			
			if (found == PETSC_TRUE) {
				sprintf(name,"%s.Qmap",file);
				TempMapLoadFromFile(name,&map);
				RheolMapApplyScale(map,units);
				sprintf(mapname,"Qmap_%d",i);
				ierr = pTatinCtxAttachMap(ctx,map,mapname);CHKERRQ(ierr);
				
				sprintf(name,"%s.Amap",file);
				TempMapLoadFromFile(name,&map);
				RheolMapApplyScale(map,units);
				sprintf(mapname,"Amap_%d",i);
				ierr = pTatinCtxAttachMap(ctx,map,mapname);CHKERRQ(ierr);
				
				sprintf(name,"%s.nexpmap",file);
				TempMapLoadFromFile(name,&map);
				sprintf(mapname,"nexpmap_%d",i);
				ierr = pTatinCtxAttachMap(ctx,map,mapname);CHKERRQ(ierr);
				
				//free(mapname);
				//free(name);
				
			} else { 
				SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_USER,"No rheol mixing files found for rocktype %d \n",i);  
			}
			}
			
			break;		
		default:
			break;
	}

	asprintf(&option_name, "-Co_%d",i);
	ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->mises_tau_yield[i]),&found);CHKERRQ(ierr);
	free(option_name);
	asprintf(&option_name, "-Phi_%d",i);
	ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->dp_pressure_dependance[i]),&found);CHKERRQ(ierr);
	free(option_name);
	asprintf(&option_name, "-Co_inf_%d",i);
	ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->soft_Co_inf[i]),&found);CHKERRQ(ierr);
	free(option_name);
	asprintf(&option_name, "-Phi_inf_%d",i);
	ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->soft_phi_inf[i]),&found);CHKERRQ(ierr);
	free(option_name);
	asprintf(&option_name, "-eps_min_%d",i);
	ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->soft_min_strain_cutoff[i]),&found);CHKERRQ(ierr);
	free(option_name);
	asprintf(&option_name, "-eps_max_%d",i);
	ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->soft_max_strain_cutoff[i]),&found);CHKERRQ(ierr);
	free(option_name);
	asprintf(&option_name, "-kappa_%d",i);
	ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->temp_kappa[i]),&found);CHKERRQ(ierr);
	free(option_name);
	asprintf(&option_name, "-hprod_%d",i);
	ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->temp_prod[i]),&found);CHKERRQ(ierr);
	free(option_name);
	asprintf(&option_name, "-Hst_cutoff_%d",i);
	ierr = PetscOptionsGetReal("GENE_",option_name,&(rheology->Hst_cutoff[i]),&found);CHKERRQ(ierr);
	free(option_name);
    }
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_anthony"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_anthony(pTatinCtx ctx)
{
    PetscErrorCode         ierr;
    
    pTatinUnits *units;
    
    PetscFunctionBegin;
    units   = &ctx->units;
    UnitsApplyInverseScaling(units->si_time,ctx->dt_min,&ctx->dt_min);
    UnitsApplyInverseScaling(units->si_time,ctx->dt,&ctx->dt);
    UnitsApplyInverseScaling(units->si_time,ctx->dt_adv,&ctx->dt_adv);
    UnitsApplyInverseScaling(units->si_time,ctx->dt_max,&ctx->dt_max);
    UnitsApplyInverseScaling(units->si_time,ctx->time_max,&ctx->time_max);
    UnitsApplyInverseScaling(units->si_length,ctx->max_disp_y,&ctx->max_disp_y);
    
    ierr = pTatin2d_ModelSetMaterialProperty_anthony(ctx); CHKERRQ(ierr);
    ierr = pTatin2d_ModelSetMarker_anthony(ctx); CHKERRQ(ierr);
    ierr= pTatin2d_ModelSetInitialTemperatureOnNodes_anthony(ctx); CHKERRQ(ierr);
    ierr= pTatin2d_ModelSetSPMParameters_GENE(ctx); CHKERRQ(ierr);
   
    
    PetscFunctionReturn(0);
}



#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetMarker_anthony"
PetscErrorCode pTatin2d_ModelSetMarker_anthony(pTatinCtx ctx)
{

	PetscInt               p,n_mp_points;
	DataBucket             db;
    DataField              PField_std, PField_stokes,PField_stokespl, PField_thermal, PField_chrono;
	int                    phase;
	PetscScalar            ymoho,yconrad,yc_moy,y_salt,y_sed,ylab, y_prod, h_prod, eps_rand, eps_max, eps_init, mu, sigma, sigma2_1, eta_ND,rho_ND,kappa_ND,prod_ND;
	DM da; 
	PetscErrorCode ierr;
    RheologyConstants      *rheology;
	pTatinUnits *units;
	PetscBool              noise_strain=PETSC_FALSE;
	PetscBool              add_salt = PETSC_FALSE;

	
	PetscFunctionBegin;
    rheology   = &ctx->rheology_constants;
	units=&ctx->units;
	da=ctx->dav;
	
	ierr = PetscOptionsGetBool(0,"-add_salt",&add_salt,0);CHKERRQ(ierr);
	
	ymoho   = -35.0e3; // SI units
	yconrad = -20.0e3; // SI units
	yc_moy  = -10.0e3; // SI units
	y_salt  = -6.0e3;  // SI units
	y_sed   = -4.0e3; // SI units
    ylab    = -120.0e3; // SI units
    ierr    = PetscOptionsGetReal(PETSC_NULL,"-conrad",&yconrad,0);CHKERRQ(ierr);
    ierr    = PetscOptionsGetReal(PETSC_NULL,"-moho",&ymoho,0);CHKERRQ(ierr);
	ierr    = PetscOptionsGetReal(PETSC_NULL,"-c_moy",&yc_moy,0);CHKERRQ(ierr);
    ierr    = PetscOptionsGetReal(PETSC_NULL,"-lab",&ylab,0);CHKERRQ(ierr);
	ierr    = PetscOptionsGetReal(PETSC_NULL,"-salt",&y_salt,0);CHKERRQ(ierr);
	ierr    = PetscOptionsGetReal(PETSC_NULL,"-sed_pre_rift",&y_sed,0);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_WORLD,"Initial layering : Conrad =  %1.2e , Moho =  %1.2e , LAB =  %1.2e \n", yconrad, ymoho, ylab);
    UnitsApplyInverseScaling(units->si_length,yconrad,&yconrad);
	UnitsApplyInverseScaling(units->si_length,ymoho,&ymoho);
	UnitsApplyInverseScaling(units->si_length,yc_moy ,&yc_moy);
	UnitsApplyInverseScaling(units->si_length,ylab ,&ylab);
	UnitsApplyInverseScaling(units->si_length,y_salt ,&y_salt);
	UnitsApplyInverseScaling(units->si_length,y_sed ,&y_sed);
    
    h_prod   =  1.e-12; // SI units
    y_prod   = -10.0e3; // SI units
    ierr = PetscOptionsGetScalar(PETSC_NULL,"-h_prod",&h_prod,PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsGetScalar(PETSC_NULL,"-y_prod",&y_prod,PETSC_NULL);CHKERRQ(ierr);
	PetscPrintf(PETSC_COMM_WORLD,"Initial layering : y_prod =  %1.2e , h_prod =  %1.2e \n", y_prod, h_prod);
    UnitsApplyInverseScaling(units->si_length,y_prod,&y_prod);
    UnitsApplyInverseScaling(units->si_heatsource,h_prod,&h_prod);
    
    eps_max = 1.0;
    ierr = PetscOptionsGetScalar(PETSC_NULL,"-eps_max",&eps_max,PETSC_NULL);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_WORLD,"Initial plastic strain noise amplitude =  %1.2e  \n", eps_max);
	
	sigma   = 50.e+3; // SI units
	mu      = 300.e+3; // SI units
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-noise_sigma",&sigma,PETSC_NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-noise_mu",&mu,PETSC_NULL);CHKERRQ(ierr);
	PetscPrintf(PETSC_COMM_WORLD,"Initial plastic strain noise Gaussian, sigma =  %1.2e mu = %1.2e \n", sigma,mu);
	UnitsApplyInverseScaling(units->si_length,sigma,&sigma);
    UnitsApplyInverseScaling(units->si_length,mu,&mu);
	
	/* define properties on material points */
	db = ctx->db;

    DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
    DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
    DataFieldGetAccess(PField_stokes);
    DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));

    DataBucketGetDataFieldByName(db,MPntPStokesPl_classname,&PField_stokespl);
    DataFieldGetAccess(PField_stokespl);
    DataFieldVerifyAccess(PField_stokespl,sizeof(MPntPStokesPl));
    
    DataBucketGetDataFieldByName(db,MPntPThermal_classname,&PField_thermal);
    DataFieldGetAccess(PField_thermal);
    DataFieldVerifyAccess(PField_thermal,sizeof(MPntPThermal));
    
    DataBucketGetDataFieldByName(db,MPntPChrono_classname,&PField_chrono);
    DataFieldGetAccess(PField_chrono);
    DataFieldVerifyAccess(PField_chrono,sizeof(MPntPChrono));

    
    DataBucketGetSizes(db,&n_mp_points,0,0);
	for (p=0; p<n_mp_points; p++) {
		MPntStd       *material_point; // pos phase index
        MPntPStokes   *mpprop_stokes; // eta rho
		double        *position;
        MPntPChrono   *mpprop_chrono; /* ages variables */
        MPntPThermal  *material_point_thermal;
        MPntPStokesPl *mpprop_stokespl;
		DataFieldAccessPoint(PField_std,p,(void**)&material_point);		
        MPntStdGetField_global_coord(material_point,&position);
		/*set phase index*/
        if (position[1] < ylab){
            phase = 0;
		} else if (position[1] < ymoho){
			phase = 1;
        } else if (position[1] < yconrad){               
			phase = 2;
        } else if (position[1] < yc_moy){					
			phase = 3;
		} else {
			if (add_salt){
				if (position[1] < y_salt){
					phase = 4;
				} else if (position[1] < y_sed){							
					phase = 5;
				} else {
					phase = 6;
				}	
			} else {
			phase = 4;
			}			
		}
		
        MPntStdSetField_phase_index(material_point,phase);
        /* set eta and rho */
        DataFieldAccessPoint(PField_stokes,p,(void**)&mpprop_stokes);
        UnitsApplyInverseScaling(units->si_viscosity,1.e+21,&eta_ND);
        UnitsApplyInverseScaling(units->si_force_per_volume,rheology->const_rho0[phase],&rho_ND);
        MPntPStokesSetField_eta_effective(mpprop_stokes,eta_ND);
        MPntPStokesSetField_density(mpprop_stokes,rho_ND);
		
        /* set kappa and Hprod */
        DataFieldAccessPoint(PField_thermal,p,(void**)&material_point_thermal);
        UnitsApplyInverseScaling(units->si_diffusivity,rheology->temp_kappa[phase],&kappa_ND);
        MPntPThermalSetField_diffusivity(material_point_thermal,kappa_ND);
        prod_ND = h_prod*exp(-position[1]/y_prod);
        MPntPThermalSetField_heat_prod(material_point_thermal,prod_ND);
		
        /* set initial plastic strain */
        DataFieldAccessPoint(PField_stokespl,p,(void**)&mpprop_stokespl);
		
		ierr = PetscOptionsGetBool(PETSC_NULL,"-noise_strain",&noise_strain,0);CHKERRQ(ierr);
		if(noise_strain){
		/* Gaussian repartition centered on mu with an amplitude sigma*/
		sigma2_1 = -1.0/(2*pow(sigma,2));
        eps_rand  = eps_max * rand() / (RAND_MAX + 1.0);
		eps_init  = eps_rand*exp(sigma2_1*pow(position[0]-mu,2.0));
		}else{
		eps_init = 0.0;
		}
        MPntPStokesPlSetField_plastic_strain(mpprop_stokespl,eps_init);
        MPntPStokesPlSetField_yield_indicator(mpprop_stokespl,0.0);
        
        
        DataFieldAccessPoint(PField_chrono,p,(void**)&mpprop_chrono);
        MPntPChronoSetField_age120(mpprop_chrono,-1.0);
        MPntPChronoSetField_age350(mpprop_chrono,-1.0);
        MPntPChronoSetField_age800(mpprop_chrono,-1.0);

    }
	DataFieldRestoreAccess(PField_std);
    DataFieldRestoreAccess(PField_stokes);
    DataFieldRestoreAccess(PField_stokespl);
    DataFieldRestoreAccess(PField_thermal);
    DataFieldRestoreAccess(PField_chrono);
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetInitialTemperatureOnNodes_anthony"
PetscErrorCode pTatin2d_ModelSetInitialTemperatureOnNodes_anthony(pTatinCtx ctx)
/* define properties on material points */
{
	PetscErrorCode         ierr;
	DM da;
	Vec T;
	PhysCompEnergyCtx phys_energy;
	PetscInt i,j,n,k,si,sj,nx,ny;
	Vec coords;
	DMDACoor2d **LA_coords;
	DM cda;
	PetscScalar **LA_T;
    PetscScalar Ttop,Tbot,Tlitho,Tradio,dtdy,jmin[2],jmax[2],age,kappa,ylab,pp[100],characteristic_time,h_prod,y_prod;
	RheologyConstants      *rheology;
	pTatinUnits *units;
	PetscBool              addanomaly=PETSC_FALSE;
	
	PetscFunctionBegin;
	
	phys_energy = ctx->phys_energy;
	rheology    = &ctx->rheology_constants;
	
	da          = phys_energy->daT;
	T           = phys_energy->T;
	//kappa       = rheology->temp_kappa[1];
	//ylab        = rheology->ylab;
	//h_prod      = rheology->h_prod;
	//y_prod      = rheology->y_prod;
	
	units       = &ctx->units;
	
	Tbot   = 1340.0;
	Ttop   = 0.0;
	Tlitho = 1300.0;
	age    = 100.0;
	h_prod = 1e-12;
	y_prod = -10.0e3;
	ylab   = -120.0e3;
	kappa  = 1.0e-6;

	ierr = PetscOptionsGetScalar(PETSC_NULL,"-Ttop",&Ttop,PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsGetScalar(PETSC_NULL,"-Tbot",&Tbot,PETSC_NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-Tlitho",&Tlitho,PETSC_NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-Thermal_age_Myr",&age,PETSC_NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-h_prod",&h_prod,PETSC_NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-y_prod",&y_prod,PETSC_NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetScalar(PETSC_NULL,"-ylab",&ylab,PETSC_NULL);CHKERRQ(ierr);
	
	age = age * 3.14e13; 
	
    UnitsApplyInverseScaling(units->si_time,age,&age);
    UnitsApplyInverseScaling(units->si_diffusivity,kappa,&kappa);
	UnitsApplyInverseScaling(units->si_length,y_prod,&y_prod);
    UnitsApplyInverseScaling(units->si_heatsource,h_prod,&h_prod);
	UnitsApplyInverseScaling(units->si_length,ylab,&ylab);
	
	ierr = DMDAGetCoordinates(da,&coords);CHKERRQ(ierr);
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,coords,&LA_coords);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(da,T,&LA_T);CHKERRQ(ierr);
	ierr = DMDAGetCorners(da,&si,&sj,0 , &nx,&ny,0);CHKERRQ(ierr);
	
	DMDAGetBoundingBox(da,jmin,jmax);
	
	Tradio = (h_prod*pow(y_prod,2.0))/kappa;
	dtdy   = (Tlitho-Tradio-Ttop)/(-ylab);
	
	
	
	ierr = PetscOptionsGetBool(0,"-add_Tanomaly",&addanomaly,0);CHKERRQ(ierr);
	
	if (addanomaly){
		PetscScalar age_anom,Cx,Wx,age_new; 
		PetscBool   found;
		
		age_anom = 50;
		Cx       = 300e3;
		Wx       = 25e3;
		
		ierr = PetscOptionsGetReal(0,"-Tanomaly_Centre",&Cx,&found);CHKERRQ(ierr);
		ierr = PetscOptionsGetReal(0,"-Tanomaly_Width",&Wx,&found);CHKERRQ(ierr);	
		ierr = PetscOptionsGetReal(0,"-age_anomaly",&age_anom,&found);CHKERRQ(ierr);
		
		age_anom = age_anom * 3.14e13;
		
		UnitsApplyInverseScaling(units->si_time,age_anom,&age_anom);
		UnitsApplyInverseScaling(units->si_length,Cx,&Cx);
		UnitsApplyInverseScaling(units->si_length,Wx,&Wx);
		
		for( j=sj; j<sj+ny; j++ ) {
			for( i=si; i<si+nx; i++ ) {
				double position[2];
				position[0] = LA_coords[j][i].x;
				position[1] = LA_coords[j][i].y;
			
				age_new=(1-exp(-pow((position[0]-Cx)/(sqrt(2.0)*Wx),2))*(age-age_anom)/age)*age;
				characteristic_time = age_new/pow(ylab,2)*kappa;
			
				if (position[1] > ylab){
				
					if (Ttop-dtdy*position[1]+Tradio*(1-exp(position[1]/(-y_prod))) < Tlitho){
						LA_T[j][i] = Ttop-dtdy*position[1]+Tradio*(1-exp(position[1]/(-y_prod))) + ((sin(M_PI*(-ylab+position[1])/-ylab*1)*(exp(-pow((1*M_PI),2)*characteristic_time))*2/M_PI*(Tlitho-Ttop)));
					}else{
						LA_T[j][i] = Tlitho + ((sin(M_PI*(-ylab+position[1])/-ylab*1)*(exp(-pow((1*M_PI),2)*characteristic_time))*2/M_PI*(Tlitho-Ttop)));
					}
				
					for (k=2;k<=100;k++){
						LA_T[j][i] = LA_T[j][i] + ((sin(M_PI*(-ylab+position[1])/-ylab*k)*(pow((-1),(k-1))/k*exp(-pow((k*M_PI),2)*characteristic_time))*2/M_PI*(Tlitho-Ttop)));
					}
				}else{ /* Adiabatic gradient from Tlitho to Tbot */
					LA_T[j][i] = -((Tbot-Tlitho)/(jmin[1]-ylab))*(ylab-position[1])+Tlitho;
				} 
			
			}
		
		}
	}else{

		characteristic_time = age/pow(ylab,2)*kappa;
	
		for( j=sj; j<sj+ny; j++ ) {
			for( i=si; i<si+nx; i++ ) {
				double position[2];
				position[0] = LA_coords[j][i].x;
				position[1] = LA_coords[j][i].y;
				/* linear gradient from top 0 to bottom temp Tic*/ 
				//LA_T[j][i] = (jmax[1]-position[1])/(jmax[1]-jmin[1])*(Tbot-Ttop)+Ttop;
		
			/* Continental geotherm from Burov and Diament 1995 Appendix */
			if (position[1] > ylab){
				
				if (Ttop-dtdy*position[1]+Tradio*(1-exp(position[1]/(-y_prod))) < Tlitho){
					LA_T[j][i] = Ttop-dtdy*position[1]+Tradio*(1-exp(position[1]/(-y_prod))) + ((sin(M_PI*(-ylab+position[1])/-ylab*1)*(exp(-pow((1*M_PI),2)*characteristic_time))*2/M_PI*(Tlitho-Ttop)));
					}else{
						LA_T[j][i] = Tlitho + ((sin(M_PI*(-ylab+position[1])/-ylab*1)*(exp(-pow((1*M_PI),2)*characteristic_time))*2/M_PI*(Tlitho-Ttop)));
					}
				
					for (k=2;k<=100;k++){
						LA_T[j][i] = LA_T[j][i] + (sin(M_PI*(-ylab+position[1])/-ylab*k)*(pow((-1),(k-1))/k*exp(-pow((k*M_PI),2)*characteristic_time))*2/M_PI*(Tlitho-Ttop));
					}
			}else{ /* Adiabatic gradient from Tlitho to Tbot */
				LA_T[j][i] = -((Tbot-Tlitho)/(jmin[1]-ylab))*(ylab-position[1])+Tlitho;
				} 
			
			}
		}
	}
	
	ierr = DMDAVecRestoreArray(da,T,&LA_T);CHKERRQ(ierr);
	ierr = DMDAVecRestoreArray(cda,coords,&LA_coords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_anthony"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_anthony(pTatinCtx ctx)
{
	BCList         ubclist;
	PetscScalar    bcval;
	DM             dav;
	PetscErrorCode ierr;
    DM da;
    Vec T;
	PetscReal   jmin[3],jmax[3];
    PetscScalar opts_srH,v_init,opts_Lx,opts_Ly;
    PhysCompEnergyCtx phys_energy;

	PetscScalar alpha_m;
	pTatinUnits *units;
	PetscBool              invert=PETSC_FALSE;
	
    
	PetscFunctionBegin;
	
	units   = &ctx->units;
	ubclist = ctx->u_bclist;
	dav     = ctx->dav;
    opts_srH = 1.6e-10;
    ierr = PetscOptionsGetScalar(PETSC_NULL,"-vx",&opts_srH,0);CHKERRQ(ierr);
	v_init   = opts_srH;
    UnitsApplyInverseScaling(units->si_velocity,opts_srH,&opts_srH);
    DMDAGetBoundingBox(dav,jmin,jmax);
    opts_Lx = jmax[0]-jmin[0];
    opts_Ly = jmax[1]-jmin[1];

	/* if activated (-bc_invert) this option will invert the boudary velocity */
	ierr = PetscOptionsGetBool(PETSC_NULL,"-bc_invert",&invert,0);CHKERRQ(ierr);
	
	if (invert){
		PetscScalar timeSI,time_pause,time_pause_start,time_invert_start,time_invert,time_post_oro_start,time_post_oro,opts_srH_P,opts_srH_I,v_invert,opts_srH_PO;
		
		PetscPrintf(PETSC_COMM_WORLD,"pTatin2d_MODEL_APPLYBC_anthony in bcInversion\n");
		UnitsApplyScaling(units->si_time,ctx->time,&timeSI);
		
		time_pause_start    = 0.02; /* Time at which v starts to decrease */
		time_pause          = 0.05; /* Time at which v = 0 or at least v = vmin */
        time_invert_start   = 0.06; /* Time for inversion to start, v starts to increase */
		time_invert         = 0.08; /* Time for velocity of inversion reaches its max value */
		time_post_oro_start = 0.09; /* Time to start velocity decrease for post orogenic phase*/
		time_post_oro       = 0.1; /* Time at wich velocity is post orogenic */
		
		ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_pause_time_start",&time_pause_start,0);CHKERRQ(ierr);
		ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_pause_time",&time_pause,0);CHKERRQ(ierr);
		ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_invert_time_start",&time_invert_start,0);CHKERRQ(ierr);
		ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_invert_time",&time_invert,0);CHKERRQ(ierr);
		ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_post_oro_time_start",&time_post_oro_start,0);CHKERRQ(ierr);
		ierr = PetscOptionsGetScalar(PETSC_NULL,"-bc_post_oro_time",&time_post_oro,0);CHKERRQ(ierr);
		
		time_invert       = time_invert * 3.14e13;
		time_invert_start = time_invert_start * 3.14e13;
		time_pause        = time_pause * 3.14e13;
		time_pause_start  = time_pause_start * 3.14e13;
		time_post_oro_start  = time_post_oro_start * 3.14e13;
		time_post_oro  = time_post_oro * 3.14e13;
		
		if (timeSI >= time_pause_start && timeSI < time_pause){ /* Linear decrease of velocity from v_init to vx_P */
			opts_srH_P = 0.0;
			ierr = PetscOptionsGetScalar(PETSC_NULL,"-vx_P",&opts_srH_P,0);CHKERRQ(ierr);
            
			opts_srH = ((opts_srH_P-v_init)/time_pause)*(timeSI-time_pause);
			
			PetscPrintf(PETSC_COMM_WORLD,"Velocity is decreasing: vx = %1.2e \n", opts_srH);
			UnitsApplyInverseScaling(units->si_velocity,opts_srH,&opts_srH);
        }
		if (timeSI >= time_pause && timeSI < time_invert_start){ /* Velocity is set to vx_P */
            opts_srH = 0.0;
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-vx_P",&opts_srH,0);CHKERRQ(ierr);
			PetscPrintf(PETSC_COMM_WORLD,"Model is relaxing velocity is, vx = %1.2e \n", opts_srH);
            UnitsApplyInverseScaling(units->si_velocity,opts_srH,&opts_srH);
        }
        if (timeSI >= time_invert_start && timeSI < time_invert){ /* Inversion starts, Linear increase of velocity from vx_P to vx_I */
            opts_srH_I = -1.6e-10;
			opts_srH_P = 0.0;
			ierr = PetscOptionsGetScalar(PETSC_NULL,"-vx_P",&opts_srH_P,0);CHKERRQ(ierr);
			ierr = PetscOptionsGetScalar(PETSC_NULL,"-vx_I",&opts_srH_I,0);CHKERRQ(ierr);
			
			opts_srH = ((opts_srH_P-opts_srH_I)/(time_invert_start-time_invert))*(timeSI-time_invert_start);
			
            PetscPrintf(PETSC_COMM_WORLD,"Inversion has started, Velocity is increasing: vx = %1.2e \n", opts_srH);
            UnitsApplyInverseScaling(units->si_velocity,opts_srH,&opts_srH);
        }
		if (timeSI >= time_invert && timeSI < time_post_oro_start){ /* Velocity is set to vx_I */
			opts_srH = -1.6e-10;
			ierr = PetscOptionsGetScalar(PETSC_NULL,"-vx_I",&opts_srH,0);CHKERRQ(ierr);
			v_invert = opts_srH;
			PetscPrintf(PETSC_COMM_WORLD,"Inversion is going on, Velocity is: vx = %1.2e \n", opts_srH);
            UnitsApplyInverseScaling(units->si_velocity,opts_srH,&opts_srH);
		}
		if (timeSI >= time_post_oro_start && timeSI < time_post_oro){ /* Velocity starts to decrease for post orogenic phase */
			opts_srH_PO = 0.0;
			ierr = PetscOptionsGetScalar(PETSC_NULL,"-vx_PO",&opts_srH_PO,0);CHKERRQ(ierr);
            
			opts_srH = ((opts_srH_PO-v_invert)/time_post_oro)*(timeSI-time_post_oro);
			
			PetscPrintf(PETSC_COMM_WORLD,"Post orogenic phase has begun velocity is decreasing: vx = %1.2e \n", opts_srH);
			UnitsApplyInverseScaling(units->si_velocity,opts_srH,&opts_srH);
		}
		if (timeSI >= time_post_oro){
			opts_srH = 0.0;
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-vx_PO",&opts_srH,0);CHKERRQ(ierr);
			PetscPrintf(PETSC_COMM_WORLD,"Model is in post orogenic phase velocity is, vx = %1.2e \n", opts_srH);
            UnitsApplyInverseScaling(units->si_velocity,opts_srH,&opts_srH);
		}
    }
    	
    /* velocity at the bottom to fill up or empty down with material that leaves on the side*/
    bcval = 2*opts_srH*opts_Ly/opts_Lx;
    ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
			
    bcval = -opts_srH; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
    bcval = opts_srH;  ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
    
    /* check for energy solver */
    
            phys_energy = ctx->phys_energy;
            da          = phys_energy->daT;
            

            bcval = 0.0;
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-Ttop",&bcval,0);CHKERRQ(ierr);
            ierr = DMDABCListTraverse(phys_energy->T_bclist,da,DMDABCList_JMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
            bcval = 1340.0;
            ierr = PetscOptionsGetScalar(PETSC_NULL,"-Tbot",&bcval,0);CHKERRQ(ierr);
            ierr = DMDABCListTraverse(phys_energy->T_bclist,da,DMDABCList_JMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
                    
    
    PetscFunctionReturn(0);
}