/*
 
 Model Description:

 Domain is [1,1]^2 with free slip boundaries.
 
 Input / command line parameters:
 -sinker_eta0 : 
 -sinker_eta1 : 
 -sinker_rho0 : 
 -sinker_rho1 : 
 -sinker_dxdy : 
 
 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"

/* ================= Sinker ================= */

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelOutput_Sinker"
PetscErrorCode pTatin2d_ModelOutput_Sinker(pTatinCtx ctx,Vec X,const char prefix[])
{
	char *pvdfilename;
	PetscErrorCode ierr;
	PetscReal p_max,p_min,v_max,vx_max,vy_max,v_min,vx_min,vy_min;
	Vec velocity,pressure;
	PetscReal opts_dxdy;
	PetscReal opts_domain_dxdy;

	
	if (prefix == PETSC_NULL) {
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,"qpoints");CHKERRQ(ierr);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,"vp");CHKERRQ(ierr);
		ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,"int_fields");CHKERRQ(ierr);
		ierr = SwarmOutputParaView_MPntPStokes(ctx->db,ctx->outputpath,"mpoints_stk");CHKERRQ(ierr);
	} else { /* exanple of adding a different prefix to the file names - not sure this is useful */
		char *name;
		
		asprintf(&name,"%s_qpoints",prefix);
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);

		asprintf(&name,"%s_vp",prefix);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);

		asprintf(&name,"%s_int_fields",prefix);
		ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
		//////////////
		asprintf(&pvdfilename,"%s/timeseries_mpoints_stk.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;

			asprintf(&vtkfilename, "%s_mpoints_stk.pvtu",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			free(vtkfilename);
		}
		/////////

		asprintf(&name,"%s_mpoints_stk",prefix);
		ierr = SwarmOutputParaView_MPntPStokes(ctx->db,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
	}

	
	ierr = DMCompositeGetAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);

	ierr = VecMax(velocity,PETSC_NULL,&v_max);CHKERRQ(ierr);
	ierr = VecStrideMax(velocity,0,PETSC_NULL,&vx_max);CHKERRQ(ierr);
	ierr = VecStrideMax(velocity,1,PETSC_NULL,&vy_max);CHKERRQ(ierr);

	ierr = VecMin(velocity,PETSC_NULL,&v_min);CHKERRQ(ierr);
	ierr = VecStrideMin(velocity,0,PETSC_NULL,&vx_min);CHKERRQ(ierr);
	ierr = VecStrideMin(velocity,1,PETSC_NULL,&vy_min);CHKERRQ(ierr);
	
	ierr = VecMax(pressure,PETSC_NULL,&p_max);CHKERRQ(ierr);
	ierr = VecMin(pressure,PETSC_NULL,&p_min);CHKERRQ(ierr);
	
	ierr = DMCompositeRestoreAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
	
	
	opts_dxdy = 0.2;
	ierr = PetscOptionsGetReal(PETSC_NULL,"-sinker_dxdy",&opts_dxdy,0);CHKERRQ(ierr);
	opts_domain_dxdy = 1.0;
	ierr = PetscOptionsGetReal(PETSC_NULL,"-sinker_domain_dxdy",&opts_domain_dxdy,0);CHKERRQ(ierr);

	PetscPrintf(PETSC_COMM_WORLD,"domain    = %1.4e \n", opts_domain_dxdy );
	PetscPrintf(PETSC_COMM_WORLD,"inclusion = %1.4e \n", opts_dxdy );

	PetscPrintf(PETSC_COMM_WORLD,"Abox      = %1.4e \n", opts_domain_dxdy*opts_domain_dxdy );
	PetscPrintf(PETSC_COMM_WORLD,"Asquare   = %1.4e \n", opts_dxdy*opts_dxdy );
	PetscPrintf(PETSC_COMM_WORLD,"ratio     = %1.4e \n", (opts_dxdy*opts_dxdy)/(opts_domain_dxdy*opts_domain_dxdy) );
	
	
	PetscPrintf(PETSC_COMM_WORLD,"v_max  = %1.4e \n", v_max );
	PetscPrintf(PETSC_COMM_WORLD,"vx_max = %1.4e \n", vx_max );
	PetscPrintf(PETSC_COMM_WORLD,"vy_max = %1.4e \n", vy_max );
	PetscPrintf(PETSC_COMM_WORLD,"p_max  = %1.4e \n", p_max );

	PetscPrintf(PETSC_COMM_WORLD,"v_min  = %1.4e \n", v_min );
	PetscPrintf(PETSC_COMM_WORLD,"vx_min = %1.4e \n", vx_min );
	PetscPrintf(PETSC_COMM_WORLD,"vy_min = %1.4e \n", vy_min );
	PetscPrintf(PETSC_COMM_WORLD,"p_min  = %1.4e \n", p_min );
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_Sinker"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_Sinker(pTatinCtx ctx)
{
	PetscErrorCode ierr;
	PetscReal opts_domain_dxdy;
	PetscContainer t1,t2,t3;
	PetscFunctionBegin;
	
	opts_domain_dxdy = 1.0;
	ierr = PetscOptionsGetReal(PETSC_NULL,"-sinker_domain_dxdy",&opts_domain_dxdy,0);CHKERRQ(ierr);
	ierr = DMDASetUniformCoordinates(ctx->dav,-0.5*opts_domain_dxdy,0.5*opts_domain_dxdy, -0.5*opts_domain_dxdy,0.5*opts_domain_dxdy, 0.0,0.0);CHKERRQ(ierr);
	
	/* test */
/*
  ierr = PetscContainerCreate(PETSC_COMM_WORLD,&t1);CHKERRQ(ierr);
  ierr = PetscContainerCreate(PETSC_COMM_WORLD,&t2);CHKERRQ(ierr);
  ierr = PetscContainerCreate(PETSC_COMM_WORLD,&t3);CHKERRQ(ierr);

	double *d1,*d2,*d3;
	d1= malloc(sizeof(double));
	d2= malloc(sizeof(double));
	d3= malloc(sizeof(double));
	*d1 = 2.2;
	*d2 = 4.2;
	*d3 = 6.2;
  ierr = PetscContainerSetPointer(t1,(void*)d1);CHKERRQ(ierr);
  ierr = PetscContainerSetPointer(t2,(void*)d2);CHKERRQ(ierr);
  ierr = PetscContainerSetPointer(t3,(void*)d3);CHKERRQ(ierr);

	ierr = PetscObjectCompose((PetscObject)ctx->model_data,"md1",(PetscObject)t1);CHKERRQ(ierr);
	ierr = PetscObjectCompose((PetscObject)ctx->model_data,"md2",(PetscObject)t2);CHKERRQ(ierr);
	ierr = PetscObjectCompose((PetscObject)ctx->model_data,"md3",(PetscObject)t3);CHKERRQ(ierr);
*/
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_Sinker"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_Sinker(pTatinCtx ctx)
{
	PetscInt               e,ncells,n_mp_points;
	PetscInt               p,ngp;
	GaussPointCoefficientsStokes *gausspoints;
	PetscReal              opts_eta0,opts_eta1;
	PetscReal              opts_rho0,opts_rho1,opts_dxdy;
	DataBucket             db;
	DataField              PField_std,PField_stokes;
	double                 eta,rho;
	int                    phase_init,phase;
	PetscErrorCode         ierr;
	
	PetscFunctionBegin;

	/*
	{
		double *d1,*d2,*d3;
		PetscContainer container;
		
		ierr = PetscObjectQuery((PetscObject)ctx->model_data,"md1",(PetscObject*)&container);CHKERRQ(ierr);
		if (!container) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONG,"No m1 composed with given model_data");
		ierr = PetscContainerGetPointer(container,(void**)&d1);CHKERRQ(ierr);

		ierr = PetscObjectQuery((PetscObject)ctx->model_data,"md2",(PetscObject*)&container);CHKERRQ(ierr);
		if (!container) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONG,"No m2 composed with given model_data");
		ierr = PetscContainerGetPointer(container,(void**)&d2);CHKERRQ(ierr);

		ierr = PetscObjectQuery((PetscObject)ctx->model_data,"md3",(PetscObject*)&container);CHKERRQ(ierr);
		if (!container) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONG,"No m3 composed with given model_data");
		ierr = PetscContainerGetPointer(container,(void**)&d3);CHKERRQ(ierr);
		
		printf("d1,d2,d3 = %lf, %lf, %lf \n", *d1,*d2,*d3 );
	}
	*/
	 
	opts_eta0 = 1.0;
	opts_eta1 = 1.0e1;
	opts_rho0 = 0.0;
	opts_rho1 = 1.0;
	opts_dxdy = 0.2;
	
	ierr = PetscOptionsGetReal(PETSC_NULL,"-sinker_eta0",&opts_eta0,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-sinker_eta1",&opts_eta1,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-sinker_rho0",&opts_rho0,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-sinker_rho1",&opts_rho1,0);CHKERRQ(ierr);
	ierr = PetscOptionsGetReal(PETSC_NULL,"-sinker_dxdy",&opts_dxdy,0);CHKERRQ(ierr);
	
	ncells = ctx->Q->ncells;
	ngp    = ctx->Q->ngp;
	ctx->rheology_constants.rheology_type = RHEOLOGY_VISCOUS;

	/* define properties on material points */
	db = ctx->db;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));

	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));
	
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	ierr = QuadratureStokesGravityModelCoordinateAlignedSetValues(ctx->Q,0.0,-9.8);CHKERRQ(ierr);
	
	for (p=0; p<n_mp_points; p++) {
		MPntStd     *material_point;
		MPntPStokes *mpprop_stokes;
		double  *position;
		
		DataFieldAccessPoint(PField_std,p,(void**)&material_point);
		DataFieldAccessPoint(PField_stokes,p,(void**)&mpprop_stokes);

		position = material_point->coor;

		MPntStdGetField_phase_index(material_point,&phase_init);
		
		rho = opts_rho0;
		eta = opts_eta0;
		phase = 0;
		if ( fabs(position[0]) < 0.5*opts_dxdy) {
			if ( fabs(position[1]) < 0.5*opts_dxdy) {
				phase = 1;
				rho = opts_rho1;
				eta = opts_eta1;
			}
		}
		
		/* user the setters provided for you */
		MPntStdSetField_phase_index(material_point,phase);
		MPntPStokesSetField_eta_effective(mpprop_stokes,eta);
		MPntPStokesSetField_density(mpprop_stokes,rho);
	}

	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokes);
	
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_Sinker"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_Sinker(pTatinCtx ctx)
{
	BCList         ubclist;
	PetscScalar    bcval;
	DM             dav;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ubclist = ctx->u_bclist;
	dav     = ctx->dav;
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}


