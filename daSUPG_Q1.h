

#ifndef __daSUPG_Q1_h__
#define __daSUPG_Q1_h__

#include "pTatin2d.h"

typedef struct _DMDASUPGParameters *DMDASUPGParameters;

struct _DMDASUPGParameters {
  PetscInt    n_corrector_steps;
  PetscReal   courant_factor;
  PetscInt    approximation_type;
  PetscScalar theta;
  PetscScalar dt;
	PetscInt    noutput;
	DM          daT;
  Vec         phi,phi_last;
  Mat         J;
	Vec         F;
	Vec         kappa,V; /* coefficients */
	BCList      phi_bclist;
};

#define NODES_PER_EL_Q1  4
#define GAUSS_POINTS_2x2 4



PetscErrorCode DMDACreateSUPGWorkVectors(DM dm,Vec *V,Vec *phi);
PetscErrorCode _DMDACreateSUPG(DM da,DMDASUPGParameters *supg);
PetscErrorCode DMDACreateSUPG2d(MPI_Comm comm,PetscInt Mx,PetscInt My,DM *da);
PetscErrorCode DMDACreateSUPG2dFromDM(DM dacoarse,PetscInt Refx,PetscInt Refy,DM *da);
PetscErrorCode DMDADestroySUPG(DM *da);
PetscErrorCode DASUPGGetParameters(DM da,DMDASUPGParameters *p);
PetscErrorCode DASUPGView(DM da,Vec kappa,Vec V,Vec phi,PetscInt step);

PetscErrorCode SUPGFormJacobian(PetscReal time,Vec X,PetscReal dt,Mat *A,Mat *B,MatStructure *mstr,void *ctx);
PetscErrorCode SUPGFormFunction(PetscReal time,Vec X,PetscReal dt,Vec F,void *ctx);

PetscErrorCode DASUPG2dComputeTimestep(DM da,PetscScalar Cr,Vec kappa,Vec V,PetscScalar *DT);

#endif

