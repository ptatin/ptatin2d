/*
 *  stokes_visco_elasto_plastic.c
 *  
 *
 *  Created by laetitia le pourhiet on 7/17/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */


#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"
#include "rheology.h"
#include "stokes_visco_plastic.h"
#include "stokes_visco_elasto_plastic.h"

//#define OBJECTIVE_UPDATE_JAUMANN
//#define OBJECTIVE_UPDATE_OLDYROD

#define SymTensXX 0
#define SymTensYY 1
#define SymTensXY 2
#define SymTensYX 2

#define TensXX 0
#define TensXY 1
#define TensYX 2 
#define TensYY 3



void StokesIsotropicVE2d_Compute_Stress(PetscReal dt_adv,PetscReal gradU[],
                                        PetscReal stress_old[], PetscReal stress[],PetscReal theta,PetscReal eta)
{
  PetscReal exx_gp,eyy_gp,exy_gp;
  
  exx_gp = gradU[TensXX];
  eyy_gp = gradU[TensYY];
  exy_gp = 0.5*( gradU[TensXY] + gradU[TensYX] );
  
  
  stress[SymTensXX] = 2 * eta * exx_gp + theta * stress_old[SymTensXX];
  stress[SymTensYY] = 2 * eta * eyy_gp + theta * stress_old[SymTensYY];
  stress[SymTensXY] = 2 * eta * exy_gp + theta * stress_old[SymTensXY];
  
#ifdef OBJECTIVE_UPDATE_JAUMANN
  Objective_Update_Jaumann(theta_gp,dt_adv,gradU,stress)
#endif  
#ifdef OBJECTIVE_UPDATE_OLDYROD
  Objective_Update_Oldyrod(theta_gp,dt_adv,gradU,stress)  
#endif
  
}

/*
 void StokesIsotropicVEP2d_Compute_VEP(
 pTatinCtx user,
 RheologyConstants *rheology,PetscInt phase_gp, 
 PetscReal position[],PetscReal velocity[],PetscReal gradU[],PetscReal pressure,PetscReal pls,
 PetscReal stress_old[], PetscReal stress[], PetscReal *theta, PetscReal *eta)
 {
 PetscBool yielded = PETSC_FALSE;
 PetscReal tau_yield;
 PetscReal stress_pred[3];
 
 StokesIsotropic2d_Compute_VE(user->dt, rheology, phase_gp, theta, eta);   
 
 StokesIsotropicVE2d_Compute_Stress(user,rheology,phase_gp,position, velocity, gradU, pressure, stress_old, stress_pred,*eta,*theta);
 
 Stokes2d_Check_Yield_DP(rheology,phase_gp, stress_pred,pressure,pls,&tau_yield, &yielded);
 
 if (yielded){
 StokesIsotropic_Rigid_plastic(tau_yield,gradU,eta);
 *theta = 0.0;
 }
 
 }
 */

void Stokes2d_Check_Yield_DP(RheologyConstants *rheology, PetscInt phase_gp, 
                             PetscReal stress_pred[], PetscReal pressure,PetscReal pls, 
                             PetscReal *tau_yield, PetscBool *yielded)

{
  PetscReal trace, stress_inv; 
  PetscReal phi, A,B;
  PetscReal char_strain,soft;
  
  
  *yielded = PETSC_FALSE;
  trace      = (stress_pred[SymTensXX]+stress_pred[SymTensYY])*0.5;
  
  stress_pred[SymTensXX] = stress_pred[SymTensXX] - trace; 
  stress_pred[SymTensYY] = stress_pred[SymTensYY] - trace;
  pressure               = pressure + trace;  // CHECK THE SIGN HERE 
  
  stress_inv =      stress_pred[SymTensXX]*stress_pred[SymTensXX]
  +     stress_pred[SymTensYY]*stress_pred[SymTensYY]
  + 2 * stress_pred[SymTensXY]*stress_pred[SymTensXY]; 
  
  stress_inv = sqrt(0.5*stress_inv);
  
  /* softening*/ 
  soft = 0.0; 
  if (rheology->gamma_soft[phase_gp] > 0){
    char_strain = pls/rheology->gamma_soft[phase_gp];
    if (char_strain > 1) char_strain = 1;
    soft = char_strain*(rheology->mises_tau_yield_inf[phase_gp]-rheology->mises_tau_yield[phase_gp]);
  }
  
  phi           = rheology->dp_pressure_dependance[phase_gp];
  A             = sin(phi);
  B             = (rheology->mises_tau_yield[phase_gp]+soft)*cos(phi);
  *tau_yield    = A * pressure + B;
  
  /* check for failure in tension */
  
  if ( *tau_yield < rheology->tens_cutoff[phase_gp]){
    *tau_yield = rheology->tens_cutoff[phase_gp];
  }
  
  if (*tau_yield < stress_inv) {
    *yielded = PETSC_TRUE;
  }
  
  
}

void StokesIsotropic_Rigid_plastic(PetscReal tau_yield, PetscReal *eta , PetscReal gradU[])
{
  PetscReal strainrate_inv, exx_gp, eyy_gp, exy_gp;
  strainrate_inv =      gradU[TensXX] * gradU[TensXX]
  +       gradU[TensYY] * gradU[TensYY]
  +      (gradU[TensYX] + gradU[TensXY]) * (gradU[TensXY] + gradU[TensYX]); 
  
  strainrate_inv = sqrt( 0.5 * strainrate_inv );
  
  //PetscPrintf(PETSC_COMM_WORLD," strainrate_inv:  %f ", strainrate_inv);
  /*
   exx_gp = gradU[TensXX];
   eyy_gp = gradU[TensYY];
   exy_gp = 0.5* ( gradU[TensXY] + gradU[TensYX] );
   
   strainrate_inv = sqrt(0.25*(exx_gp-eyy_gp)*(exx_gp-eyy_gp)+exy_gp*exy_gp);
   
   PetscPrintf(PETSC_COMM_WORLD," 2:  %f  \n", strainrate_inv);
   */
  (*eta)        = 0.5 * tau_yield/strainrate_inv;  
  
}

// VISCO-ELASTIC ONLY
void StokesIsotropic2d_Compute_VE(
                                  PetscReal dt,PetscReal dt_adv,
                                  RheologyConstants *rheology,PetscInt phase, 
                                  PetscReal *theta,PetscReal *eta)
{
  PetscReal G = rheology->const_shearmod[phase];
  
  Stokes2d_Compute_Creep(rheology,phase,eta);
  
  (*theta)  = 1.0 / ( 1.0 + G*dt/(*eta) ); 
  (*eta)    =  G * (*theta) * dt_adv;
  // (*eta)    =  G * (*theta);
  
}


void Stokes2d_Compute_Creep(
                            RheologyConstants *rheology, PetscInt phase,
                            PetscReal *eta)
{
  *eta = rheology->const_eta0[phase];
}



void Objective_Update_Oldyrod(PetscReal theta_gp,PetscReal dt_adv,PetscReal gradU[],PetscReal stress[])
{
  
  PetscReal Wxy_gp,WW[2][2],TT[2][2],RR[2][2];
  PetscInt ii,jj,k;
  PetscReal LL[2][2];
  PetscReal facJau,AA;
  
  // -L.T - T.LT
  
  LL[0][0] = gradU[TensXX];
  LL[0][1] = gradU[TensXY];
  LL[1][0] = gradU[TensYX];
  LL[1][1] = gradU[TensYY];
  
  TT[0][0] = stress[SymTensXX];
  TT[0][1] = stress[SymTensXY];
  TT[1][0] = stress[SymTensYX];
  TT[1][1] = stress[SymTensYY];
  
  AA = 1.0; // Oldroyd-B
  RR[0][0] = RR[0][1] = RR[1][0] = RR[1][1] = 0.0;
  for (k=0; k<2; k++) {
    ii = 0; jj = 0;  RR[ii][jj] += AA*(LL[ii][k]*TT[k][jj] + TT[ii][k]*LL[jj][k]);
    ii = 0; jj = 1;  RR[ii][jj] += AA*(LL[ii][k]*TT[k][jj] + TT[ii][k]*LL[jj][k]);
    ii = 1; jj = 0;  RR[ii][jj] += AA*(LL[ii][k]*TT[k][jj] + TT[ii][k]*LL[jj][k]);
    ii = 1; jj = 1;  RR[ii][jj] += AA*(LL[ii][k]*TT[k][jj] + TT[ii][k]*LL[jj][k]);
  }
  facJau = dt_adv * theta_gp;
  
  stress[SymTensXX] -= facJau * RR[0][0];
  stress[SymTensXY] -= facJau * RR[0][1];
  stress[SymTensYY] -= facJau * RR[1][1];
}


void Objective_Update_Jaumann (PetscReal theta_gp,PetscReal dt_adv,PetscReal gradU[],PetscReal stress[])
{
  
  
  PetscReal Wxy_gp,WW[2][2],TT[2][2],RR[2][2];
  PetscInt ii,jj,k;
  PetscReal facJau,AA;
  
  
  // W.T + T.W
  
  WW[0][0] = WW[0][1] = WW[1][0] = WW[1][1] = 0.0;
  Wxy_gp = gradU[1] - gradU[2]; // du/dx, du/dy, dv/dx, dv/dy
  
  WW[0][1] =  Wxy_gp;
  WW[1][0] = -Wxy_gp;
  
  TT[0][0] = stress[SymTensXX];
  TT[0][1] = stress[SymTensXY];
  TT[1][0] = stress[SymTensYX];
  TT[1][1] = stress[SymTensYY];
  
  RR[0][0] = RR[0][1] = RR[1][0] = RR[1][1] = 0.0;
  for (k=0; k<2; k++) {
    ii = 0; jj = 0;  RR[ii][jj] += TT[ii][k] * WW[k][jj] - WW[ii][k] * TT[k][jj];
    ii = 0; jj = 1;  RR[ii][jj] += TT[ii][k] * WW[k][jj] - WW[ii][k] * TT[k][jj];
    ii = 1; jj = 0;  RR[ii][jj] += TT[ii][k] * WW[k][jj] - WW[ii][k] * TT[k][jj];
    ii = 1; jj = 1;  RR[ii][jj] += TT[ii][k] * WW[k][jj] - WW[ii][k] * TT[k][jj];
  }
  facJau = dt_adv*theta_gp;
  
  stress[SymTensXX] -= facJau * RR[0][0];
  stress[SymTensXY] -= facJau * RR[0][1];
  stress[SymTensYY] -= facJau * RR[1][1];
}


#undef __FUNCT__
#define __FUNCT__ "EvaluateRheologyNonlinearitiesQuadratureStokes_ViscoElastoPlastic_A"
PetscErrorCode EvaluateRheologyNonlinearitiesQuadratureStokes_ViscoElastoPlastic_A(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[])
{	
	PetscErrorCode ierr;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
	PetscScalar GNIu[MAX_QUAD_PNTS][2][Q2_NODES_PER_EL_2D],NIp[MAX_QUAD_PNTS][P_BASIS_FUNCTIONS];
	PetscScalar NIu[MAX_QUAD_PNTS][U_BASIS_FUNCTIONS];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen_u,nen_p,e,n,k;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D],el_eta[MAX_QUAD_PNTS];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	PetscScalar Fe[Q2_NODES_PER_EL_2D*2],Be[Q2_NODES_PER_EL_2D*2];
	PetscInt vel_el_lidx[U_BASIS_FUNCTIONS*2];
	PetscInt *gidx,elgidx[2*Q2_NODES_PER_EL_2D];
	PetscInt nbcs,i,j;
	const PetscInt *ubcidx;
	Vec diag;
	PetscScalar *LA_diag;
	PetscLogDouble t0,t1;
	BCList u_bclist = user->u_bclist;
	GaussPointCoefficientsStokes *gausspoints;
	PetscReal  gradU[4];
	PetscScalar  sxx_gp,syy_gp,sxy_gp,u_gp,v_gp,xpos_gp,ypos_gp,trace_p,pressure_gp;
	PetscScalar min_eta,max_eta;
  PetscScalar strainrateinv_gp,stressinv_gp;
  PetscInt npoints,npoints_yielded,npoints_yielded_tens;
  PetscScalar min_eta_g,max_eta_g; 
  PetscInt npoints_yielded_g, npoints_yielded_tens_g,npoints_g;
	static int beenHere = 1;
  RheologyConstants *rheology;
	PetscScalar alpha_m,eta_eff_continuation, min_eta_cut, max_eta_cut;
  PetscScalar soft,stress_pred[3]; 
	PetscReal THETA_EFF, ETA_EFF; 
	PetscFunctionBegin;
	
	/* quadrature */
	/*QuadratureCreateGauss_3pnt_2D(&ngp,gp_xi,gp_weight);*/
  rheology  = &user->rheology_constants;
	ngp       = user->Q->ngp;
	gp_xi     = user->Q->xi;
	gp_weight = user->Q->weight;
	for (k=0; k<ngp; k++) {
		PetscScalar *xip = &gp_xi[2*k];
		
		PTatinConstructNI_Q2_2D(xip,NIu[k]);
		PTatinConstructGNI_Q2_2D(xip,GNIu[k]);
	}
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetGlobalIndices(dau,0,&gidx);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
	
	min_eta = 1.e100;
  max_eta = 1.e-100;
  npoints = 0;
  npoints_yielded = 0;
  npoints_yielded_tens = 0;
	PetscGetTime(&t0);
  
  // continuation parameter //
  /*  
   min_eta_cut = rheology->eta_lower_cutoff_global;
   max_eta_cut = rheology->eta_upper_cutoff_global;  
   alpha_m = (double)(user->continuation_m)/( (double)(user->continuation_M) );
   alpha_m = 1;
   eta_eff_continuation = min_eta_cut + (max_eta_cut-min_eta_cut)*alpha_m;
   PetscPrintf(PETSC_COMM_WORLD," continuation viscosity cut-off:  %f  \n", eta_eff_continuation);
   //continuation_Co  = maxCo  - (maxCo-minCo)*alpha_m;
   */
  
  for (e=0;e<nel;e++) {
		
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		ierr = StokesVelocity_GetElementLocalIndices(vel_el_lidx,(PetscInt*)&elnidx_u[nen_u*e]);CHKERRQ(ierr);
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],u);CHKERRQ(ierr);
		ierr = DMDAGetScalarElementField_2D(elp,nen_p,(PetscInt*)&elnidx_p[nen_p*e],p);CHKERRQ(ierr);
		
		/* get element velocity */
		for( i=0; i<Q2_NODES_PER_EL_2D; i++ ) {
			ux[i] = elu[2*i  ];
			uy[i] = elu[2*i+1];
			
			xc[i] = elcoords[2*i  ];
			yc[i] = elcoords[2*i+1];
		}
		/* get element pressure */
		for (n=0; n<ngp; n++) {
			PetscScalar *xip = &gp_xi[2*n];
			
			PTatinConstructNI_Q2_2D(xip,NIu[n]);
			ConstructNi_pressure(xip,elcoords,NIp[n]);
		}
		
		for (n=0; n<ngp; n++) {
			PetscScalar  J[2][2], iJ[2][2];
			PetscScalar J_p,ojp;
      PetscInt phase_gp = gausspoints[n].phase;
			PetscScalar tau_yield, eta0;
      PetscBool yielded; 
      PetscScalar A,B,phi,tauyield_gp,eta_gp;
      
			/* coord transformation */
			for( i=0; i<2; i++ ) {
				for( j=0; j<2; j++ ) { J[i][j] = 0.0; }
			}
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				J[0][0] += GNIu[n][0][k] * xc[k] ;
				J[0][1] += GNIu[n][0][k] * yc[k] ;
				
				J[1][0] += GNIu[n][1][k] * xc[k] ;
				J[1][1] += GNIu[n][1][k] * yc[k] ;
			}
			J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
			ojp = 1.0/J_p;
			iJ[0][0] =  J[1][1]*ojp;
			iJ[0][1] = -J[0][1]*ojp;
			iJ[1][0] = -J[1][0]*ojp;
			iJ[1][1] =  J[0][0]*ojp;
			
			/* global derivs at each quadrature point */
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				nx[k] = iJ[0][0]*GNIu[n][0][k] + iJ[0][1]*GNIu[n][1][k];
				ny[k] = iJ[1][0]*GNIu[n][0][k] + iJ[1][1]*GNIu[n][1][k];
			}

			/* x,y and u,v and strain rate (e= B u) at gp */
      xpos_gp = ypos_gp = 0.0;
			u_gp = v_gp = 0.0;
			gradU[TensXX] = gradU[TensYY] = gradU[TensXY] = gradU[TensYX] = 0.0;
			for( i=0; i<U_BASIS_FUNCTIONS; i++ ) {
				xpos_gp   += NIu[n][i] * xc[i];
				ypos_gp   += NIu[n][i] * yc[i];
				
				u_gp   += NIu[n][i] * ux[i];
				v_gp   += NIu[n][i] * uy[i];
				
				gradU[TensXX] += nx[i] * ux[i];
				gradU[TensYY] += ny[i] * uy[i];
        gradU[TensXY] += ny[i] * ux[i];
        gradU[TensYX] += nx[i] * uy[i];
			}
      strainrateinv_gp =     gradU[TensXX] * gradU[TensXX]
      +       gradU[TensYY] * gradU[TensYY]
      +      (gradU[TensXY] + gradU[TensYX]) * (gradU[TensXY] + gradU[TensYX]); 
      strainrateinv_gp = sqrt( 0.5 * strainrateinv_gp );
      
      /* pressure at gp */
      pressure_gp = 0.0;
      for( i=0; i<P_BASIS_FUNCTIONS; i++ ) {
        pressure_gp += NIp[n][i] * elp[i];
      }
      
      StokesIsotropicVE2d_Compute_Stress(user->dt_adv, gradU, gausspoints[n].stress_old, stress_pred,gausspoints->theta,gausspoints->eta);
    
      pressure_gp = pressure_gp + 0.5 * (stress_pred[0]+stress_pred[1]); 
      
      
      /* update the viscosity, fu_x, fu_y, fp  */
			StokesIsotropic2d_Compute_VE(user->dt,user->dt_adv,rheology,phase_gp, &THETA_EFF,&ETA_EFF);
      //   PetscPrintf(PETSC_COMM_WORLD,"I have a viscosity of %1.2e  et un relax de %1.2e \n",ETA_EFF, THETA_EFF);   
   
      /* plasticity correction*/
      StokesIsotropicVE2d_Compute_Stress(user->dt_adv, gradU, gausspoints[n].stress_old, stress_pred,THETA_EFF,ETA_EFF);
      
      sxx_gp = stress_pred[0];
      syy_gp = stress_pred[1];
      sxy_gp = stress_pred[2];
      
      
      trace_p = 0.5 * ( sxx_gp + syy_gp );
      sxx_gp = sxx_gp - trace_p;
      syy_gp = syy_gp - trace_p;
      
      stressinv_gp = sqrt(0.25*(sxx_gp-syy_gp)*(sxx_gp-syy_gp)+sxy_gp*sxy_gp);
   
      eta_gp = ETA_EFF;
         
          
 //     Stokes2d_Check_Yield_DP(rheology,phase_gp, stress_pred,pressure_gp,gausspoints[n].pls,&tau_yield, &yielded);
   
      soft = 0.0; 
      gausspoints[n].plsr = 0.0;
      if (rheology->gamma_soft[phase_gp] > 0){
        PetscReal char_strain = gausspoints[n].pls/rheology->gamma_soft[phase_gp];
        if (char_strain > 1) char_strain = 1;
        soft = char_strain*(rheology->mises_tau_yield_inf[phase_gp]-rheology->mises_tau_yield[phase_gp]);
      }
      phi         = rheology->dp_pressure_dependance[phase_gp];
      A           = sin(phi);
      B           = (rheology->mises_tau_yield[phase_gp]+soft)*cos(phi);
      tauyield_gp = A * pressure_gp + B;
      
      /* check for failure in tension */
      
      if ( tauyield_gp < rheology->tens_cutoff[phase_gp]){
        tauyield_gp = rheology->tens_cutoff[phase_gp];
      }   
      
      if (stressinv_gp > tauyield_gp){
        /* failure in shear */
#if 0 
        ETA_EFF = 0.5*tauyield_gp/strainrateinv_gp;
        gausspoints[n].plsr = (1.0-ETA_EFF/rheology->const_eta0[phase_gp]);
        npoints_yielded++;
        THETA_EFF = 0.0;
#endif
        
#if 1   
        PetscReal blah;
        //ETA_EFF = 0.5*tauyield_gp/strainrateinv_gp;
        //gausspoints[n].plsr = (1.0-ETA_EFF/rheology->const_eta0[phase_gp]);
        npoints_yielded++;
        blah = sqrt(0.25*(gausspoints[n].stress_old[0]-gausspoints[n].stress_old[1])*(gausspoints[n].stress_old[0]-gausspoints[n].stress_old[1])
                            +gausspoints[n].stress_old[2]*gausspoints[n].stress_old[2]);
        THETA_EFF = (tauyield_gp - 2 * ETA_EFF * strainrateinv_gp)/stressinv_gp;
        gausspoints[n].plsr = 0; 
#endif        
        
        
        if (abs(stressinv_gp-rheology->tens_cutoff[phase_gp])<1.e-12) npoints_yielded_tens++;
      }
      
      /* set the viscosity on the quadrature point */  
      gausspoints[n].eta     = ETA_EFF;			
      gausspoints[n].theta   = THETA_EFF;
     
      
      /* monitor bounds */
      if (gausspoints[n].eta > max_eta) { max_eta = gausspoints[n].eta; }
      if (gausspoints[n].eta < min_eta) { min_eta = gausspoints[n].eta; }
      
      /* update point counter */
      npoints++;
    }
  }
  
  
  /* Apply viscosity limiters ? */
  if (rheology->apply_viscosity_cutoff_global) {
    PetscPrintf(PETSC_COMM_WORLD,"Update rheology (visco-elasto-plastic): Enforcing global viscosity cut-off\n");
    for (e=0;e<nel;e++) {
      ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
      for (n=0; n<ngp; n++) {
        
        if (gausspoints[n].eta > rheology->eta_upper_cutoff_global) {
          gausspoints[n].eta = rheology->eta_upper_cutoff_global;
        }
        if (gausspoints[n].eta < rheology->eta_lower_cutoff_global) {
          gausspoints[n].eta = rheology->eta_lower_cutoff_global;
        }
        
      }
    }
  }
  if (rheology->apply_viscosity_cutoff) {
    PetscPrintf(PETSC_COMM_WORLD,"Update rheology (visco-elasto-plastic): Enforcing per-phase viscosity cut-off\n");
    for (e=0;e<nel;e++) {
      ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
      for (n=0; n<ngp; n++) {
        PetscInt phase_p;
        
        //phase_p = gausspoints[n].phase_index;
        if (gausspoints[n].eta > rheology->eta_upper_cutoff[n]) {
          gausspoints[n].eta = rheology->eta_upper_cutoff[n];
        }
        if (gausspoints[n].eta < rheology->eta_lower_cutoff[n]) {
          gausspoints[n].eta = rheology->eta_lower_cutoff[n];
        }
        
      }
    }
  }
  
  
  
  PetscGetTime(&t1);
  MPI_Allreduce(&min_eta,&min_eta_g,1, MPI_DOUBLE, MPI_MIN, PETSC_COMM_WORLD);
  MPI_Allreduce(&max_eta,&max_eta_g,1, MPI_DOUBLE, MPI_MAX, PETSC_COMM_WORLD);
  MPI_Allreduce(&npoints_yielded,&npoints_yielded_g,1, MPI_INT, MPI_SUM, PETSC_COMM_WORLD);
  MPI_Allreduce(&npoints_yielded_tens,&npoints_yielded_tens_g,1, MPI_INT, MPI_SUM, PETSC_COMM_WORLD);
  MPI_Allreduce(&npoints,&npoints_g,1, MPI_INT, MPI_SUM, PETSC_COMM_WORLD);	
  PetscPrintf(PETSC_COMM_WORLD,"Update rheology (visco-elasto-plastic): npoints %d; npoints_yielded %d; npoints_yielded_tens %d; (min,max)_eta %1.2e,%1.2e; log10(max/min) %1.2e; cpu time %1.2e (sec)\n",
              npoints_g, npoints_yielded_g, npoints_yielded_tens_g, min_eta_g, max_eta_g, log10(max_eta_g/min_eta_g), t1-t0 );
  
  ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
  
  beenHere++;
  
  PetscFunctionReturn(0);
}




#undef __FUNCT__
#define __FUNCT__ "EvaluateRheologyNonlinearitiesQuadratureStokes_ViscoElastoPlastic"
PetscErrorCode EvaluateRheologyNonlinearitiesQuadratureStokes_ViscoElastoPlastic(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[])
{	
	PetscInt continuation_option = 0;
	PetscBool flg;
  PetscErrorCode ierr;
  
	PetscFunctionBegin;
  /* apply rheology */
	ierr = PetscOptionsGetInt(PETSC_NULL,"-eval_rheology_vep",&continuation_option,&flg);CHKERRQ(ierr);
	/* make a selction from possible continuation algorithms here */
	
  ierr = EvaluateRheologyNonlinearitiesQuadratureStokes_ViscoElastoPlastic_A(user,dau,u,dap,p);CHKERRQ(ierr);
  
  /* apply limiters ?*/
  
  /* apply continuation ?*/
  
  PetscFunctionReturn(0);
}










#undef __FUNCT__
#define __FUNCT__ "TkUpdateRheologyQuadratureStokes_ViscoElastoPlastic_stress_history"
PetscErrorCode TkUpdateRheologyQuadratureStokes_ViscoElastoPlastic_stress_history(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[])
{	
	PetscErrorCode ierr;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
	PetscScalar GNIu[MAX_QUAD_PNTS][2][Q2_NODES_PER_EL_2D],NIp[MAX_QUAD_PNTS][P_BASIS_FUNCTIONS];
	PetscScalar NIu[MAX_QUAD_PNTS][U_BASIS_FUNCTIONS];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen_u,nen_p,e,n,k;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D],el_eta[MAX_QUAD_PNTS];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	PetscScalar Fe[Q2_NODES_PER_EL_2D*2],Be[Q2_NODES_PER_EL_2D*2];
	PetscInt vel_el_lidx[U_BASIS_FUNCTIONS*2];
	PetscInt *gidx,elgidx[2*Q2_NODES_PER_EL_2D];
	PetscInt nbcs,i,j;
	const PetscInt *ubcidx;
	Vec diag;
	PetscScalar *LA_diag;
	PetscLogDouble t0,t1;
	BCList u_bclist = user->u_bclist;
	GaussPointCoefficientsStokes *gausspoints;
	PetscReal  gradU[4];
	PetscScalar  sxx_gp,syy_gp,sxy_gp,u_gp,v_gp,xpos_gp,ypos_gp,trace_p,pressure_gp;
	PetscScalar min_eta,max_eta;
  PetscScalar strainrateinv_gp,stressinv_gp;
  PetscInt npoints,npoints_yielded,npoints_yielded_tens;
  PetscScalar min_eta_g,max_eta_g; 
  PetscInt npoints_yielded_g, npoints_yielded_tens_g,npoints_g;
	static int beenHere = 1;
  RheologyConstants *rheology;
	PetscScalar alpha_m,eta_eff_continuation, min_eta_cut, max_eta_cut;
  PetscScalar soft,stress_pred[3]; 
	PetscReal THETA_EFF, ETA_EFF; 
	PetscFunctionBegin;
	
	/* quadrature */
	/*QuadratureCreateGauss_3pnt_2D(&ngp,gp_xi,gp_weight);*/
  rheology  = &user->rheology_constants;
	ngp       = user->Q->ngp;
	gp_xi     = user->Q->xi;
	gp_weight = user->Q->weight;
	for (k=0; k<ngp; k++) {
		PetscScalar *xip = &gp_xi[2*k];
		
		PTatinConstructNI_Q2_2D(xip,NIu[k]);
		PTatinConstructGNI_Q2_2D(xip,GNIu[k]);
	}
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetGlobalIndices(dau,0,&gidx);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
	
	PetscGetTime(&t0);
  
  for (e=0;e<nel;e++) {
		
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		ierr = StokesVelocity_GetElementLocalIndices(vel_el_lidx,(PetscInt*)&elnidx_u[nen_u*e]);CHKERRQ(ierr);
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],u);CHKERRQ(ierr);
		ierr = DMDAGetScalarElementField_2D(elp,nen_p,(PetscInt*)&elnidx_p[nen_p*e],p);CHKERRQ(ierr);
		
		/* get element velocity */
		for( i=0; i<Q2_NODES_PER_EL_2D; i++ ) {
			ux[i] = elu[2*i  ];
			uy[i] = elu[2*i+1];
			
			xc[i] = elcoords[2*i  ];
			yc[i] = elcoords[2*i+1];
		}
		/* get element pressure */
		for (n=0; n<ngp; n++) {
			PetscScalar *xip = &gp_xi[2*n];
			
			PTatinConstructNI_Q2_2D(xip,NIu[n]);
			ConstructNi_pressure(xip,elcoords,NIp[n]);
		}
		
		for (n=0; n<ngp; n++) {
			PetscScalar  J[2][2], iJ[2][2];
			PetscScalar J_p,ojp;      
			PetscScalar trace;
      /* coord transformation */
			for( i=0; i<2; i++ ) {
				for( j=0; j<2; j++ ) { J[i][j] = 0.0; }
			}
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				J[0][0] += GNIu[n][0][k] * xc[k] ;
				J[0][1] += GNIu[n][0][k] * yc[k] ;
				
				J[1][0] += GNIu[n][1][k] * xc[k] ;
				J[1][1] += GNIu[n][1][k] * yc[k] ;
			}
			J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
			ojp = 1.0/J_p;
			iJ[0][0] =  J[1][1]*ojp;
			iJ[0][1] = -J[0][1]*ojp;
			iJ[1][0] = -J[1][0]*ojp;
			iJ[1][1] =  J[0][0]*ojp;
			
			/* global derivs at each quadrature point */
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				nx[k] = iJ[0][0]*GNIu[n][0][k] + iJ[0][1]*GNIu[n][1][k];
				ny[k] = iJ[1][0]*GNIu[n][0][k] + iJ[1][1]*GNIu[n][1][k];
			}
      
			gradU[TensXX] = gradU[TensYY] = gradU[TensXY] = gradU[TensYX] = 0.0;
			for( i=0; i<U_BASIS_FUNCTIONS; i++ ) {
				gradU[TensXX] += nx[i] * ux[i];
				gradU[TensYY] += ny[i] * uy[i];
        gradU[TensXY] += ny[i] * ux[i];
        gradU[TensYX] += nx[i] * uy[i];
			}
			
      StokesIsotropicVE2d_Compute_Stress(user->dt_adv, gradU, gausspoints[n].stress_old, stress_pred,gausspoints[n].theta,gausspoints[n].eta);
      
      trace = (stress_pred[SymTensXX]+stress_pred[SymTensYY])*0.5;
      
      gausspoints[n].stress_old[SymTensXX] = stress_pred[SymTensXX] - trace;
      gausspoints[n].stress_old[SymTensYY] = stress_pred[SymTensYY] - trace;
      gausspoints[n].stress_old[SymTensXY] = stress_pred[SymTensXY];
      
    }
  }
  
  ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
  
  beenHere++;
  
  PetscFunctionReturn(0);
}





#undef __FUNCT__
#define __FUNCT__ "pTatin_TkUpdateRheologyQuadratureStokes_ViscoElastoPlastic"
PetscErrorCode pTatin_TkUpdateRheologyQuadratureStokes_ViscoElastoPlastic(pTatinCtx user,
                                                                    DM dau,PetscScalar LA_Uloc[],DM dap,PetscScalar LA_Ploc[])
{	
  RheologyConstants *rheology;
	PetscReal gmin[3],gmax[3];
	PetscBool unload = PETSC_FALSE;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
  /* update rheology variable such as plastic strain for sofetening */  
  ierr = pTatin_TkUpdateRheologyQuadratureStokes_ViscoPlastic(user,dau);CHKERRQ(ierr);
	
  /* update stress */ 
  ierr = pTatin_EvaluateRheologyNonlinearities(user,dau,LA_Uloc,dap,LA_Ploc);CHKERRQ(ierr);
  ierr = TkUpdateRheologyQuadratureStokes_ViscoElastoPlastic_stress_history(user,dau,LA_Uloc,dap,LA_Ploc);CHKERRQ(ierr);  
	
  rheology = &user->rheology_constants;
  ierr = DMDAGetBoundingBox(dau,gmin,gmax);CHKERRQ(ierr);
  /*
   if (user->step > 10) {
   unload = PETSC_TRUE;
   PetscPrintf(PETSC_COMM_WORLD,"Unloading commenced...\n");
   }
   */
  if (unload) {
    PetscInt e,ncells;
    PetscInt p,ngp;
    GaussPointCoefficientsStokes *gausspoints;
		
    ncells = user->Q->ncells;
    ngp    = user->Q->ngp;
		
    ncells = user->Q->ncells;
    ngp    = user->Q->ngp;
    for (e=0;e<ncells;e++) {
      ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
      for (p=0; p<ngp; p++) {
        gausspoints[p].Fu[1] = 0.0;
      }
    }
  }
  
  PetscFunctionReturn(0);
}


/*this is not compatible with the rest any more ... */
#undef __FUNCT__
#define __FUNCT__ "FormFunctionLocal_Stokes_ViscoElastoPlastic_GradSigma"
PetscErrorCode FormFunctionLocal_Stokes_ViscoElastoPlastic_GradSigma(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[],PetscScalar Ru[])
{	
	PetscErrorCode ierr;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
	PetscScalar GNIu[MAX_QUAD_PNTS][2][Q2_NODES_PER_EL_2D];
	PetscScalar NIu[MAX_QUAD_PNTS][U_BASIS_FUNCTIONS];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen_u,e,n,k,i,j;
	const PetscInt *elnidx_u;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2];
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	PetscScalar Fe[Q2_NODES_PER_EL_2D*2];
	PetscInt vel_el_lidx[U_BASIS_FUNCTIONS*2];
	PetscInt *gidx;
	PetscLogDouble t0,t1;
	GaussPointCoefficientsStokes *gausspoints;
  RheologyConstants *rheology;
  PetscScalar THETA,ETA;
	PetscFunctionBegin;
	
	/* quadrature */
  rheology  = &user->rheology_constants;
	ngp       = user->Q->ngp;
	gp_xi     = user->Q->xi;
	gp_weight = user->Q->weight;
	for (k=0; k<ngp; k++) {
		PetscScalar *xip = &gp_xi[2*k];		
		PTatinConstructNI_Q2_2D(xip,NIu[k]);
		PTatinConstructGNI_Q2_2D(xip,GNIu[k]);
	}
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);	
	ierr = DMDAGetGlobalIndices(dau,0,&gidx);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
  
	PetscGetTime(&t0);
	for (e=0;e<nel;e++) {
		
		ierr = StokesVelocity_GetElementLocalIndices(vel_el_lidx,(PetscInt*)&elnidx_u[nen_u*e]);CHKERRQ(ierr);
		//		PetscErrorCode StokesPressure_GetElementLocalIndices(PetscInt el_localIndices[],PetscInt elnid[])
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],u);CHKERRQ(ierr);
		
		for( i=0; i<Q2_NODES_PER_EL_2D; i++ ) {
			ux[i] = elu[2*i  ];
			uy[i] = elu[2*i+1];
			
			xc[i] = elcoords[2*i  ];
			yc[i] = elcoords[2*i+1];
		}
    
		/* insert element matrix into global matrix */
		//		ierr = GetElementEqnIndicesQ2((PetscInt*)&elnidx_u[nen_u*e],gidx,elgidx);CHKERRQ(ierr);
		
		/* initialise element stiffness matrix */
		PetscMemzero( Fe, sizeof(PetscScalar)* Q2_NODES_PER_EL_2D*NSD );
    
		for (n=0; n<ngp; n++) {
			PetscScalar  J[2][2], iJ[2][2];
			PetscScalar  sxx,syy,sxy;
			PetscScalar fac,J_p,ojp,d0;
      PetscInt phase_gp = gausspoints[n].phase;
			
      /* coord transformation */
			for( i=0; i<2; i++ ) {
				for( j=0; j<2; j++ ) { J[i][j] = 0.0; }
			}
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				J[0][0] += GNIu[n][0][k] * xc[k] ;
				J[0][1] += GNIu[n][0][k] * yc[k] ;
				
				J[1][0] += GNIu[n][1][k] * xc[k] ;
				J[1][1] += GNIu[n][1][k] * yc[k] ;
			}
			J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
			ojp = 1.0/J_p;
			iJ[0][0] =  J[1][1]*ojp;
			iJ[0][1] = -J[0][1]*ojp;
			iJ[1][0] = -J[1][0]*ojp;
			iJ[1][1] =  J[0][0]*ojp;
			
			/* global derivs */
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				nx[k] = iJ[0][0]*GNIu[n][0][k] + iJ[0][1]*GNIu[n][1][k];
				ny[k] = iJ[1][0]*GNIu[n][0][k] + iJ[1][1]*GNIu[n][1][k];
			}
      
      // ADD FUNCTION CALL HERE
      //      d0 = 1.0 + rheology->const_shearmod[phase_gp]/rheology->const_eta0[phase_gp]*user->dt;
      //			fac = gp_weight[n] * J_p/d0;
      //			StokesIsotropic2d_Compute_VE(user->dt,rheology,phase_gp, &THETA,&ETA);
      //			fac = gp_weight[n] * J_p * THETA;
      
      fac = gp_weight[n] * J_p * gausspoints[n].theta;
      //		PetscPrintf(PETSC_COMM_WORLD,"[C] d0 = %lf :: 1/THETA,ETA = %lf,%lf \n", d0,1.0/THETA,ETA );
      
      
			/* stress */
			sxx = gausspoints[n].stress_old[0];
			syy = gausspoints[n].stress_old[1];
			sxy = gausspoints[n].stress_old[2];
      
#ifdef OBJECTIVE_UPDATE_JAUMANN
      { // W.T + T.W
        PetscScalar Wxy_gp,WW[2][2],TT[2][2],RR[2][2];
        PetscScalar facJau;
        PetscInt ii,jj;
        
        WW[0][0] = WW[0][1] = WW[1][0] = WW[1][1] = 0.0;
        Wxy_gp = 0.0;
        for( i=0; i<U_BASIS_FUNCTIONS; i++ ) {
          Wxy_gp += 0.5 * ( ny[i] * ux[i] - nx[i] * uy[i] );
        }
        
        WW[0][1] =  Wxy_gp;
        WW[1][0] = -Wxy_gp;
        
        TT[0][0] = sxx;
        TT[0][1] = sxy;
        TT[1][0] = sxy;
        TT[1][1] = syy;
        
        RR[0][0] = RR[0][1] = RR[1][0] = RR[1][1] = 0.0;
        for (k=0; k<2; k++) {
          ii = 0; jj = 0;  RR[ii][jj] += TT[ii][k] * WW[k][jj] - WW[ii][k] * TT[k][jj];
          ii = 0; jj = 1;  RR[ii][jj] += TT[ii][k] * WW[k][jj] - WW[ii][k] * TT[k][jj];
          ii = 1; jj = 0;  RR[ii][jj] += TT[ii][k] * WW[k][jj] - WW[ii][k] * TT[k][jj];
          ii = 1; jj = 1;  RR[ii][jj] += TT[ii][k] * WW[k][jj] - WW[ii][k] * TT[k][jj];
        }
        //        facJau = fac * user->dt / d0;
        facJau = fac * user->dt * THETA;
        for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) { 
          Fe[2*k]   -= facJau * (nx[k]*(RR[0][0]) + ny[k]*RR[0][1]); 
          Fe[2*k+1] -= facJau * (ny[k]*(RR[1][0]) + nx[k]*RR[1][1]); 
        }
      }   
#endif
#ifdef OBJECTIVE_UPDATE_OLDYROD
      { // -L.T - T.LT
        PetscScalar LL[2][2],TT[2][2],RR[2][2];
        PetscScalar facJau,AA;
        PetscInt ii,jj;
        
        LL[0][0] = LL[0][1] = LL[1][0] = LL[1][1] = 0.0;
        for( i=0; i<U_BASIS_FUNCTIONS; i++ ) {
          LL[0][0] += nx[i] * ux[i];
          LL[0][1] += ny[i] * ux[i];
          LL[1][0] += nx[i] * uy[i];
          LL[1][1] += ny[i] * uy[i];
        }
        TT[0][0] = sxx;
        TT[0][1] = sxy;
        TT[1][0] = sxy;
        TT[1][1] = syy;
        
        AA = 1.0; // Oldroyd-B
        RR[0][0] = RR[0][1] = RR[1][0] = RR[1][1] = 0.0;
        for (k=0; k<2; k++) {
          ii = 0; jj = 0;  RR[ii][jj] += AA*(LL[ii][k]*TT[k][jj] + TT[ii][k]*LL[jj][k]);
          ii = 0; jj = 1;  RR[ii][jj] += AA*(LL[ii][k]*TT[k][jj] + TT[ii][k]*LL[jj][k]);
          ii = 1; jj = 0;  RR[ii][jj] += AA*(LL[ii][k]*TT[k][jj] + TT[ii][k]*LL[jj][k]);
          ii = 1; jj = 1;  RR[ii][jj] += AA*(LL[ii][k]*TT[k][jj] + TT[ii][k]*LL[jj][k]);
        }
        //        facJau = fac * user->dt / d0;
        facJau = fac * user->dt * THETA;
        for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) { 
          Fe[2*k]   -= facJau * (nx[k]*(RR[0][0]) + ny[k]*RR[0][1]); 
          Fe[2*k+1] -= facJau * (ny[k]*(RR[1][0]) + nx[k]*RR[1][1]); 
        }
      }         
#endif
      
			
      
      for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) { 
				Fe[2*k]   += fac * (nx[k]*(sxx)   + ny[k]*sxy); 
				Fe[2*k+1] += fac * (ny[k]*(syy)   + nx[k]*sxy); 
			}
  	}
    
		ierr = DMDASetValuesLocalStencil_AddValues_Stokes_Velocity(Ru, vel_el_lidx,Fe);CHKERRQ(ierr);
	}
	
	PetscGetTime(&t1);
  //	PetscPrintf(PETSC_COMM_WORLD,"Assemble Ru, = %1.4e (sec)\n",t1-t0);
	
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}



#undef __FUNCT__
#define __FUNCT__ "FormFunctionLocal_Visco_Elasto_Plastic_U"
PetscErrorCode FormFunctionLocal_Visco_Elasto_Plastic_U(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[],PetscScalar Ru[])
{	
	PetscErrorCode ierr;
	PetscInt ngp;
	PetscScalar *gp_xi,*gp_weight;
	PetscScalar GNIu[MAX_QUAD_PNTS][2][Q2_NODES_PER_EL_2D],NIp[MAX_QUAD_PNTS][P_BASIS_FUNCTIONS];
	PetscScalar NIu[MAX_QUAD_PNTS][U_BASIS_FUNCTIONS];
	DM cda;
	Vec gcoords;
	PetscScalar *LA_gcoords;
	PetscInt nel,nen_u,nen_p,e,n,k;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	PetscScalar elcoords[2*Q2_NODES_PER_EL_2D],el_eta[MAX_QUAD_PNTS];
	PetscScalar elu[Q2_NODES_PER_EL_2D*2],elp[P_BASIS_FUNCTIONS];
	PetscScalar ux[Q2_NODES_PER_EL_2D],uy[Q2_NODES_PER_EL_2D];
	PetscScalar xc[Q2_NODES_PER_EL_2D],yc[Q2_NODES_PER_EL_2D];
	PetscScalar nx[Q2_NODES_PER_EL_2D],ny[Q2_NODES_PER_EL_2D];
	PetscScalar Fe[Q2_NODES_PER_EL_2D*2],Be[Q2_NODES_PER_EL_2D*2];
	PetscInt vel_el_lidx[U_BASIS_FUNCTIONS*2];
	PetscInt *gidx,elgidx[2*Q2_NODES_PER_EL_2D];
	PetscInt nbcs,i,j;
	const PetscInt *ubcidx;
	Vec diag;
	PetscScalar *LA_diag;
	PetscLogDouble t0,t1;
	BCList u_bclist = user->u_bclist;
	GaussPointCoefficientsStokes *gausspoints;
  PetscScalar theta_gp; 
	PetscFunctionBegin;
	
	/* quadrature */
	/*QuadratureCreateGauss_3pnt_2D(&ngp,gp_xi,gp_weight);*/
	ngp       = user->Q->ngp;
	gp_xi     = user->Q->xi;
	gp_weight = user->Q->weight;
	for (k=0; k<ngp; k++) {
		PetscScalar *xip = &gp_xi[2*k];
		
		PTatinConstructNI_Q2_2D(xip,NIu[k]);
		PTatinConstructGNI_Q2_2D(xip,GNIu[k]);
	}
	
	/* setup for coords */
	ierr = DMDAGetCoordinateDA( dau, &cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates( dau,&gcoords );CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetGlobalIndices(dau,0,&gidx);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
	PetscGetTime(&t0);
	for (e=0;e<nel;e++) {
		PetscScalar int_P, int_divu;
		
		ierr = StokesVelocity_GetElementLocalIndices(vel_el_lidx,(PetscInt*)&elnidx_u[nen_u*e]);CHKERRQ(ierr);
		//		PetscErrorCode StokesPressure_GetElementLocalIndices(PetscInt el_localIndices[],PetscInt elnid[])
		ierr = QuadratureStokesGetCell(user->Q,e,&gausspoints);CHKERRQ(ierr);
		
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		ierr = DMDAGetVectorElementFieldQ2_2D(elu,(PetscInt*)&elnidx_u[nen_u*e],u);CHKERRQ(ierr);
		ierr = DMDAGetScalarElementField_2D(elp,nen_p,(PetscInt*)&elnidx_p[nen_p*e],p);CHKERRQ(ierr);
		
		for( i=0; i<Q2_NODES_PER_EL_2D; i++ ) {
			ux[i] = elu[2*i  ];
			uy[i] = elu[2*i+1];
			
			xc[i] = elcoords[2*i  ];
			yc[i] = elcoords[2*i+1];
		}
		
		for (n=0; n<ngp; n++) {
			PetscScalar *xip = &gp_xi[2*n];
			ConstructNi_pressure(xip,elcoords,NIp[n]);
		}
		
		/* insert element matrix into global matrix */
		//		ierr = GetElementEqnIndicesQ2((PetscInt*)&elnidx_u[nen_u*e],gidx,elgidx);CHKERRQ(ierr);
		
		/* initialise element stiffness matrix */
		PetscMemzero( Fe, sizeof(PetscScalar)* Q2_NODES_PER_EL_2D*2 );
		PetscMemzero( Be, sizeof(PetscScalar)* Q2_NODES_PER_EL_2D*2 );
		
		/* evaluate the viscosity */
		for (n=0; n<ngp; n++) {
			el_eta[n] = gausspoints[n].eta;
		}
		
		int_P = int_divu = 0.0;
		for (n=0; n<ngp; n++) {
			PetscScalar  J[2][2], iJ[2][2];
			PetscScalar  exx,eyy,exy;
			PetscScalar  sxx,syy,sxy,pressure_gp;
			PetscScalar fac,d1,d2,J_p,ojp;
			PetscScalar stress[3],gradU[4]; 
      
			/* coord transformation */
			for( i=0; i<2; i++ ) {
				for( j=0; j<2; j++ ) { J[i][j] = 0.0; }
			}
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				J[0][0] += GNIu[n][0][k] * xc[k] ;
				J[0][1] += GNIu[n][0][k] * yc[k] ;
				
				J[1][0] += GNIu[n][1][k] * xc[k] ;
				J[1][1] += GNIu[n][1][k] * yc[k] ;
			}
			J_p = (J[0][0]*J[1][1]) - (J[0][1]*J[1][0]);
			ojp = 1.0/J_p;
			iJ[0][0] =  J[1][1]*ojp;
			iJ[0][1] = -J[0][1]*ojp;
			iJ[1][0] = -J[1][0]*ojp;
			iJ[1][1] =  J[0][0]*ojp;
			
			/* global derivs */
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) {
				nx[k] = iJ[0][0]*GNIu[n][0][k] + iJ[0][1]*GNIu[n][1][k];
				ny[k] = iJ[1][0]*GNIu[n][0][k] + iJ[1][1]*GNIu[n][1][k];
			}
			
			
			fac = gp_weight[n] * J_p;
			
			/* pressure */
			pressure_gp = 0.0;
			for( i=0; i<P_BASIS_FUNCTIONS; i++ ) {
				pressure_gp += NIp[n][i] * elp[i];
			}
			pressure_gp = pressure_gp * fac;
			
			/* strain rate, B u */
			exx = eyy = exy = 0.0;
			for( i=0; i<Q2_NODES_PER_EL_2D; i++ ) {
				exx += nx[i] * ux[i];
				eyy += ny[i] * uy[i];
				exy += ny[i] * ux[i] + nx[i] * uy[i];
			}
			int_P    += fac * pressure_gp;
			int_divu += fac * (exx + eyy);
      ////////////////////////////////////////////////////////////////////////			
      
      gradU[TensXX] = gradU[TensYY] = gradU[TensXY] = gradU[TensYX] = 0.0;
			for( i=0; i<Q2_NODES_PER_EL_2D; i++ ) {
				gradU[TensXX] += nx[i] * ux[i];
        gradU[TensXY] += ny[i] * ux[i];
        gradU[TensYX] += nx[i] * uy[i];
				gradU[TensYY] += ny[i] * uy[i];
			}
      
      StokesIsotropicVE2d_Compute_Stress(user->dt_adv,gradU, gausspoints[n].stress_old,
                                         stress,gausspoints[n].theta,gausspoints[n].eta);
      
      /* stress */
			sxx = fac * stress[SymTensXX];
			syy = fac * stress[SymTensYY];
			sxy = fac * stress[SymTensXY];
      
      ///////////////////////////////////////////////////////////////////////////////
      
			/* constituive */
			// d1 = 2.0 * el_eta[n] * fac;
			// d2 =       el_eta[n] * fac;
			//theta_gp = gausspoints[n].theta * fac;
			/* stress */
			//sxx = d1 * exx + theta_gp*gausspoints[n].stress_old[SymTensXX];
			//syy = d1 * eyy + theta_gp*gausspoints[n].stress_old[SymTensYY];
			//sxy = d2 * exy + theta_gp*gausspoints[n].stress_old[SymTensXY];
      
      
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) { 
				Fe[2*k]   += nx[k]*(sxx-pressure_gp)   + ny[k]*sxy; 
				Fe[2*k+1] += ny[k]*(syy-pressure_gp)   + nx[k]*sxy; 
			}
      
			/*
			 for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) { 
			 Fe[2*k]   -= nx[k]*pressure_gp; 
			 Fe[2*k+1] -= ny[k]*pressure_gp; 
			 }
			 */
			
			/* compute any body force terms here */
			for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) { 
				Be[2*k  ] = Be[2*k  ] + fac * NIu[n][k] * gausspoints[n].Fu[0];
				Be[2*k+1] = Be[2*k+1] + fac * NIu[n][k] * gausspoints[n].Fu[1];
			}
		}
#if 0
		{
			/* 
			 P +ve in compression
			 eta_bulk = (2 eta div(u) - P)/div(u) => bulk viscosity
			 lambda = eta_bulk - 2 eta / 3
			 */
			PetscScalar eta_gp = 1.0;
			PetscScalar lambda,eta_bulk;
			eta_bulk = (2.0 * eta_gp * int_divu - int_P) / int_divu;
			lambda = eta_bulk - (2.0/3.0) * eta_gp;
			if (eta_bulk<0.0) printf("******* etaB -ve *******");
			if (lambda<0.0) printf("******* lambda -ve *******");
			printf("eta = %1.4e, p = %1.4e, div(u) = %1.4e, eta_bulk = %1.4e, lambda = %1.4e \n", eta_gp,int_P,int_divu,eta_bulk,lambda );
		}
#endif
		
		/* combine body force with A.x */
		for( k=0; k<Q2_NODES_PER_EL_2D; k++ ) { 
			Fe[2*k  ] = Fe[2*k  ] - Be[2*k  ];
			Fe[2*k+1] = Fe[2*k+1] - Be[2*k+1];
		}
		
		ierr = DMDASetValuesLocalStencil_AddValues_Stokes_Velocity(Ru, vel_el_lidx,Fe);CHKERRQ(ierr);
	}
	
	PetscGetTime(&t1);
	PetscPrintf(PETSC_COMM_WORLD,"Assemble Ru, = %1.4e (sec)\n",t1-t0);
	
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}












/*
 Computes r = Ax - b
 SNES will scale by -1, F = -r = b - Ax
 Thus, in OUR function, dirichlet slots become A_ii(x_i - phi)
 In SNES, these become A_ii(phi-x_i), and the updates on the dirichlet slots will be
 A_ii d_i = -F_i 
 = A_ii(phi-x_i)
 Then the update will be 
 x_i^new = x_i + d_i
 = x_i + inv(A_ii) A_ii(phi-x_i)
 = x_i + phi - x_i
 = phi
 */
#undef __FUNCT__  
#define __FUNCT__ "FormFunction_Stokes_ViscoElastoPlastic"
PetscErrorCode FormFunction_Stokes_ViscoElastoPlastic(SNES snes,Vec X,Vec F,void *ctx)
{
  pTatinCtx   user = (pTatinCtx)ctx;
  DM                dau,dap;
  DMDALocalInfo     infou,infop;
  PetscErrorCode    ierr;
  Vec               Uloc,Ploc,FUloc,FPloc;
	Vec               u,p,Fu,Fp;
  PetscScalar       *LA_Uloc,*LA_Ploc;
  PetscScalar       *LA_FUloc,*LA_FPloc;
	
  PetscFunctionBegin;
  ierr = DMCompositeGetEntries(user->pack,&dau,&dap);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dau,&infou);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dap,&infop);CHKERRQ(ierr);
	
  ierr = DMCompositeGetLocalVectors(user->pack,&Uloc,&Ploc);CHKERRQ(ierr);
  ierr = DMCompositeGetLocalVectors(user->pack,&FUloc,&FPloc);CHKERRQ(ierr);
	
	/* get the local (ghosted) entries for each physics */
	ierr = DMCompositeScatter(user->pack,X,Uloc,Ploc);CHKERRQ(ierr);
	/* insert boundary conditions into local vectors */
	ierr = BCListInsertLocal(user->u_bclist,Uloc);CHKERRQ(ierr);
	
	ierr = VecGetArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
	ierr = VecGetArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
	
	/* compute Ax - b */
	ierr = VecZeroEntries(FUloc);CHKERRQ(ierr);
	ierr = VecZeroEntries(FPloc);CHKERRQ(ierr);
	ierr = VecGetArray(FUloc,&LA_FUloc);CHKERRQ(ierr);
	ierr = VecGetArray(FPloc,&LA_FPloc);CHKERRQ(ierr);
	
	/* ======================================== */
	/*         UPDATE NON-LINEARITIES           */
	/* evaluate rheology and rhs using X        */
  
	/* map marker eta to quadrature points */
  
	/* map marker force to quadrature points */
	
	/* ======================================== */
	
	ierr = pTatin_EvaluateRheologyNonlinearities(user,dau,LA_Uloc,dap,LA_Ploc);CHKERRQ(ierr);
  ierr = FormFunctionLocal_Visco_Elasto_Plastic_U(user,dau,LA_Uloc,dap,LA_Ploc,LA_FUloc);CHKERRQ(ierr);
	// Form scaling for momentum 
	//ierr = FormScaling_U_etaMassMatrixDiagonal(user,dau,user->u_bclist);CHKERRQ(ierr);
	
	//ierr = FormFunctionLocal_U(user,dau,LA_Uloc,dap,LA_Ploc,LA_FUloc);CHKERRQ(ierr);
  
	ierr = FormFunctionLocal_P(user,dau,LA_Uloc,dap,LA_Ploc,LA_FPloc);CHKERRQ(ierr);
	//ierr = FormFunctionLocal_Stokes_ViscoElastoPlastic_GradSigma(user,dau,LA_Uloc,dap,LA_Ploc,LA_FUloc);CHKERRQ(ierr);
	
  
  ierr = VecRestoreArray(FPloc,&LA_FPloc);CHKERRQ(ierr);
	ierr = VecRestoreArray(FUloc,&LA_FUloc);CHKERRQ(ierr);
	ierr = VecRestoreArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
	ierr = VecRestoreArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
	
	/* do global fem summation */
	ierr = VecZeroEntries(F);CHKERRQ(ierr);
	ierr = DMCompositeGather(user->pack,F,ADD_VALUES,FUloc,FPloc);CHKERRQ(ierr);
	
  ierr = DMCompositeRestoreLocalVectors(user->pack,&FUloc,&FPloc);CHKERRQ(ierr);
  ierr = DMCompositeRestoreLocalVectors(user->pack,&Uloc,&Ploc);CHKERRQ(ierr);
	
	/* modify F for the boundary conditions, F_k = scale_k(x_k - phi_k) */
	ierr = DMCompositeGetAccess(user->pack,F,&Fu,&Fp);CHKERRQ(ierr);
	ierr = DMCompositeGetAccess(user->pack,X,&u,&p);CHKERRQ(ierr);
	
	ierr = BCListResidualDirichlet(user->u_bclist,u,Fu);CHKERRQ(ierr);
	
	ierr = DMCompositeRestoreAccess(user->pack,X,&u,&p);CHKERRQ(ierr);
	ierr = DMCompositeRestoreAccess(user->pack,F,&Fu,&Fp);CHKERRQ(ierr);
	
	
  PetscFunctionReturn(0);
}
