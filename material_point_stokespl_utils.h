
#ifndef __MATERIAL_POINT_PSTOKESPL_UTILS_H__
#define __MATERIAL_POINT_PSTOKESPL_UTILS_H__

#include "swarm_fields.h"
#include "data_exchanger.h"
#include "MPntStd_def.h"
#include "MPntPStokes_def.h"
#include "MPntPStokesPl_def.h"

PetscErrorCode SwarmOutputParaView_MPntPStokesPl(DataBucket db,const char path[],const char prefix[]);
PetscErrorCode SwarmUpdateProperties_MPntPStokesPl(DataBucket db,pTatinCtx ctx,Vec X);
PetscErrorCode SwarmUpdatePosition_Communication_Generic(DM da,DataBucket db,DataEx de);


#endif