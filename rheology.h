/*
 *  rheology.h
 *  
 *
 *  Created by laetitia le pourhiet on 6/11/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef __pTatin_rheology_h__
#define __pTatin_rheology_h__

#include "stokes_viscous.h"
#include "stokes_visco_plastic.h"
#include "stokes_visco_elasto_plastic.h"


typedef enum { 
	RHEOLOGY_VISCOUS=0, 
	RHEOLOGY_VISCO_PLASTIC, 
	RHEOLOGY_VISCO_PLASTIC_STRAIN_WEAKENING, 
	RHEOLOGY_VISCO_PLASTICY,
	RHEOLOGY_VISCO_PLASTIC_T, 
	RHEOLOGY_VISCO_PLASTIC_STRAIN_WEAKENING_T,
	RHEOLOGY_VPT_STD,
	RHEOLOGY_VISCO_ELASTIC_PLASTIC 
} RheologyType;

typedef enum { 
	VISCOUS_CONSTANT=0,
	VISCOUS_FRANKK,
	VISCOUS_ARRHENIUS,
	VISCOUS_POWERLAW,
	VISCOUS_ARRHENIUS_MIXING,
    VISCOUS_IO
} ViscousType;

typedef enum { 
	SOFTENING_NONE=0,
	SOFTENING_LINEAR,
	SOFTENING_EXPONENTIAL,
	HARDENING_SOFTENING
} SofteningType;

typedef enum { 
	PLASTIC_NONE=0,
	PLASTIC_M,
	PLASTIC_DP,
    PLASTIC_DAMAGE
} PlasticType;

typedef enum { 
	MELT_NONE=0,
	MELT_MUMU,
    MELT_P
} MeltType;

typedef enum { 
	AVERAGE_TYPE_ARITHMETIC=0, AVERAGE_TYPE_HARMONIC,AVERAGE_TYPE_GEOMETRIC 
} MaterialCoeffAvergeType;

typedef enum { 
	DENSITY_CONSTANT=0,
	DENSITY_FUNCTION,
	DENSITY_TABLE
} DensityType;

#define MAX_PHASE 100

struct _p_RheologyConstants {
    PetscBool apply_viscosity_cutoff_global;
    PetscBool store_melt;
    PetscBool store_plastic;
    PetscReal eta_lower_cutoff_global, eta_upper_cutoff_global;
    PetscBool apply_viscosity_cutoff;
    PetscReal eta_lower_cutoff[MAX_PHASE], eta_upper_cutoff[MAX_PHASE];
    
    /* rheology; "constant" ==>> const (short name) */
    PetscReal const_eta0[MAX_PHASE];
    PetscReal const_rho0[MAX_PHASE];
	/* not in used in the marker version, elastic const,would require storing stress on marker... oh la la */
    PetscReal const_shearmod[MAX_PHASE];    
    /* rheology; "von mises" ==>> mises (short name) */
    PetscReal mises_tau_yield[MAX_PHASE]; /* would be nice to refactor it to DP_Co */ 
    PetscReal dp_pressure_dependance[MAX_PHASE]; /* would be nice to refactor it to DP_phi */
    PetscReal tens_cutoff[MAX_PHASE]; /* would be nice to refactor it to DP_tens_cutoff */
    PetscReal Hst_cutoff [MAX_PHASE]; /* would be nice to refactor it to DP_Hst_cutoff */
	
    PetscReal gamma_soft[MAX_PHASE]; /* not in used in the marker version */ 
    PetscReal mises_tau_yield_inf[MAX_PHASE]; /* not in used in the marker version */
	
    /* rheology; "strain_weakening" ==> soft (short name) */
    PetscReal soft_min_strain_cutoff [MAX_PHASE];
    PetscReal soft_max_strain_cutoff [MAX_PHASE];
    PetscReal soft_Co_inf [MAX_PHASE];
    PetscReal soft_phi_inf [MAX_PHASE];
    /* rheology; "Temperature dependance" ==> temp (short name) */
    PetscReal temp_theta[MAX_PHASE];
    PetscReal temp_alpha[MAX_PHASE];
    PetscReal temp_beta[MAX_PHASE];  
    PetscReal temp_kappa[MAX_PHASE];
    PetscReal temp_prod[MAX_PHASE];
    /* rheology; "Arrhenius" ==> arrh (short name) */
    PetscReal arrh_entalpy[MAX_PHASE];
    PetscReal arrh_preexpA[MAX_PHASE];
    PetscReal arrh_nexp[MAX_PHASE];
    PetscReal arrh_Vmol[MAX_PHASE];
    PetscReal arrh_Tref[MAX_PHASE];
    PetscReal arrh_Ascale[MAX_PHASE];
    /* rheology; "PlasticDammage" ==> dam (short name) */
    PetscScalar dam_eta_h_0[MAX_PHASE];
    PetscScalar dam_eta_h_inf[MAX_PHASE];
    PetscScalar dam_eps_max[MAX_PHASE];
    PetscScalar dam_eps_min[MAX_PHASE];
    PetscScalar dam_healing_rate[MAX_PHASE];
    PetscInt dam_factor[MAX_PHASE];
    /* rheology; "MELT" ==> melt (short name) */
	PetscScalar melt_eta[MAX_PHASE];
	PetscScalar melt_rho[MAX_PHASE];
    PetscScalar melt_Tsol[MAX_PHASE];
    PetscScalar melt_Tinf[MAX_PHASE];
    PetscInt meltP_type[MAX_PHASE];
	RheologyType   rheology_type;
	ViscousType    viscous_type[MAX_PHASE];
	SofteningType  softening_type[MAX_PHASE];
	PlasticType    plastic_type[MAX_PHASE];
	DensityType    density_type[MAX_PHASE];
    MeltType       melt_type[MAX_PHASE];
};


#define MAX_SURFACE_PHASE 100

typedef enum { 
	STRESS_NULL=0,
	STRESS_VOLUMETRIC_VISCOSITY,
	STRESS_DP,
	STRESS_NONE
} SurfaceStressType;


typedef enum { 
	SURF_RHEOLOGY_NONE=0, 
	SURF_RHEOLOGY_SLIDING,
	SURF_RHEOLOGY_STRESS
} SurfaceRheologyType;

struct _p_SurfaceRheologyConstants {
	SurfaceRheologyType rheology_type;
	/* sliding parameters */
	PetscReal sliding_beta; /* (I - \vtr n \cross \vtr n).(\sigma \vtr n + \beta \vtr u )|_base = 0 */
	SurfaceStressType stress_type[MAX_SURFACE_PHASE];
	/* DP parameters */
    PetscReal DP_Co[MAX_SURFACE_PHASE];
    PetscReal DP_phi[MAX_SURFACE_PHASE];
};


PetscErrorCode RheologyConstantsInitialise(RheologyConstants *R);
PetscErrorCode RheologyConstantsEnforceRheologyConst(RheologyConstants *R,const PetscInt phase_index);
PetscErrorCode TkUpdateRheologyQuadratureStokes(pTatinCtx user,DM dau,DM dap,Vec X);
PetscErrorCode pTatin_EvaluateRheologyNonlinearitiesQuadratureStokes(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[]);
PetscErrorCode pTatin_EvaluateRheologyNonlinearities(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[]);
PetscErrorCode pTatin_UpdateRheologyNonlinearities(pTatinCtx user,Vec X);

PetscErrorCode SurfaceRheologyConstantsInitialise(SurfaceRheologyConstants *R);
PetscErrorCode pTatin_Stokes_EvaluateSurfaceNonlinearities(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[]);

#endif
