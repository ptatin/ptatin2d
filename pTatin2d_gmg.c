
static const char help[] = "Stokes solver using Q2-Pm1 mixed finite elements.\n"
"2D prototype of the (p)ragmatic version of Tatin. (pTatin2d_v0.0)\n\n";

#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"
#include "pTatin2d_models.h"


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_material_points_std"
PetscErrorCode pTatin2d_material_points_std(int argc,char **argv)
{
	PetscInt        k,nlevels;
	DM							*dav_hierarchy;
	Mat             *interpolation;
	DM              multipys_pack;
	IS              *isg;
	SNES            snes;
	Mat             A,B,Auu,Aup,Apu,Spp;
	Vec             X,F;
	KSP             ksp;
	PC              pc;
	pTatinCtx user;
	PetscInt        e,tk;
	PetscReal       time;
	PetscErrorCode  ierr;
	
	
	ierr = pTatin2dCreateContext(&user);CHKERRQ(ierr);
	ierr = pTatin2dParseOptions(user);CHKERRQ(ierr);
	/*
	 {
	 PetscBool load_checkpoint=PETSC_FALSE;
	 
	 PetscOptionsGetBool(PETSC_NULL,"-load_cp",&load_checkpoint,0);
	 if (load_checkpoint==PETSC_TRUE) {
	 printf("loading checkpoint \n");
	 ierr = pTatinCheckpointLoad(user,PETSC_NULL);CHKERRQ(ierr);
	 }
	 }
	 */	
	ierr = pTatin2dCreateQ2PmMesh(user);CHKERRQ(ierr);
	
	user->dav->ops->coarsenhierarchy = DMCoarsenHierarchy2_DA;
	
  nlevels = 1;
	PetscOptionsGetInt(PETSC_NULL,"-dau_nlevels",&nlevels,0);
	ierr = PetscMalloc(sizeof(DM)*nlevels,&dav_hierarchy);CHKERRQ(ierr);
	dav_hierarchy[ nlevels-1 ] = user->dav;
	ierr = PetscObjectReference((PetscObject)user->dav);CHKERRQ(ierr);
	
	/* option 1 - simply coarsen nlevels - 1 times */
	{
		DM *coarsened_list;
		ierr = PetscMalloc(sizeof(DM)*(nlevels-1),&coarsened_list);CHKERRQ(ierr);
		ierr = DMCoarsenHierarchy(user->dav,nlevels-1,coarsened_list);CHKERRQ(ierr);
		for (k=0; k<nlevels-1; k++) {
			dav_hierarchy[ nlevels-2-k ] = coarsened_list[k];
		}
		PetscFree(coarsened_list);

		for (k=0; k<nlevels; k++) {
			PetscPrintf(PETSC_COMM_WORLD,"level[%d] :\n", k );
			ierr = DMView(dav_hierarchy[k],PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
		}
	}

	ierr = PetscMalloc(sizeof(Mat)*nlevels,&interpolation);CHKERRQ(ierr);
	interpolation[0] = PETSC_NULL;
	for (k=0; k<nlevels-1; k++) {
		ierr = DMGetInterpolation(dav_hierarchy[k],dav_hierarchy[k+1],&interpolation[k+1],PETSC_NULL);CHKERRQ(ierr);
	}
	
	
	
	ierr = pTatin2dCreateBoundaList(user);CHKERRQ(ierr);
	ierr = pTatin2dCreateQuadratureStokes(user);CHKERRQ(ierr);
	
	ierr = pTatin2dCreateMaterialPoints(user);CHKERRQ(ierr);
	
	/* mesh geometry */
	ierr = pTatin2d_ModelApplyInitialMeshGeometry(user);CHKERRQ(ierr);
	
	/* update coordinates of coarse grids */
	/* NOT NECESSARY FOR GALERKIN */
	
	/* interpolate point coordinates (needed if mesh was modified) */
	ierr = QuadratureStokesCoordinateSetUp(user->Q,user->dav);CHKERRQ(ierr);
	ierr = MaterialPointCoordinateSetUp(user->db,user->dav);CHKERRQ(ierr);
	
	for (e=0; e<QUAD_EDGES; e++) {
		ierr = SurfaceQuadratureStokesGeometrySetUp(user->surfQ[e],user->dav);CHKERRQ(ierr);
	}
	
	
	/* material geometry */
	ierr = pTatin2d_ModelApplyInitialMaterialGeometry(user);CHKERRQ(ierr);
	
	/* boundary conditions */
	ierr = pTatin2d_ModelApplyBoundaryCondition(user);CHKERRQ(ierr);
	
	/*
	 ierr = pTatin2d_TestSurfaceQuadratureStokes(user);CHKERRQ(ierr);
	 ierr = pTatin2d_TestSurfaceQuadratureStokes2(user);CHKERRQ(ierr);
	 */
	
	multipys_pack = user->pack;
	ierr = DMCreateGlobalVector(multipys_pack,&X);CHKERRQ(ierr);
  ierr = VecDuplicate(X,&F);CHKERRQ(ierr);
	
	ierr = pTatin2d_ModelOutput(user,X,"ic_bc");CHKERRQ(ierr);
	
/*
	if (!user->use_mf_stokes) { // 559576260 ==> 227702832
		ierr = DMGetMatrix_DMCompositeStokesQkPm(multipys_pack,MATNEST,&A);CHKERRQ(ierr);
	}
	ierr = DMGetMatrix_DMCompositeStokesPCQkPm(multipys_pack,MATNEST,&B);CHKERRQ(ierr);
*/

	Auu = Aup = Apu = Spp = PETSC_NULL;
	ierr = DMCompositeStokes_MatCreate(multipys_pack,MATAIJ,&Auu,MATAIJ,&Aup,MATAIJ,PETSC_NULL,MATAIJ,&Spp);CHKERRQ(ierr);
	ierr = DMCompositeStokes_MatCreate(multipys_pack,MATAIJ,PETSC_NULL,MATAIJ,PETSC_NULL,MATAIJ,&Apu,MATAIJ,PETSC_NULL);CHKERRQ(ierr);

	{
		PetscBool upper,lower,full;
		
		upper = lower = full = PETSC_FALSE;
		ierr = PetscOptionsGetBool(PETSC_NULL,"-dmcomposite_pc_upper",&upper,PETSC_NULL);CHKERRQ(ierr);
		ierr = PetscOptionsGetBool(PETSC_NULL,"-dmcomposite_pc_lower",&lower,PETSC_NULL);CHKERRQ(ierr);
		ierr = PetscOptionsGetBool(PETSC_NULL,"-dmcomposite_pc_full",&full,PETSC_NULL);CHKERRQ(ierr);

		if ( (!full) && (!upper) && (!lower) ) {
			full = PETSC_TRUE;
			PetscPrintf(PETSC_COMM_WORLD,"Assuming Stokes preconditioner will be full: B = [ Auu, Aup ; Apu, Spp* ] \n");
		}
		
		if (full) {
			ierr = DMCompositeStokesCreateNest(multipys_pack,Auu,Aup,Apu,Spp,&B);CHKERRQ(ierr);
		} else if (upper) {
			ierr = DMCompositeStokesCreateNest(multipys_pack,Auu,Aup,PETSC_NULL,Spp,&B);CHKERRQ(ierr);
		} else if (lower) {
			ierr = DMCompositeStokesCreateNest(multipys_pack,Auu,PETSC_NULL,Apu,Spp,&B);CHKERRQ(ierr);
		}
	}

	if (!user->use_mf_stokes) {
		ierr = DMCompositeStokesCreateNest(multipys_pack,Auu,Aup,Apu,PETSC_NULL,&A);CHKERRQ(ierr);
	}
	
	/* ====== Generate an initial condition for Newton ====== */
#if 1
	// parameter continuation //
	{
		PetscInt k,maxit;
		SNES snesIC;
	  Mat JA,JB;
		KSP kspIC;
		PC pcIC;
		MatStructure flg;
		
		
		ierr = PetscOptionsGetInt(PETSC_NULL,"-continuation_M",&user->continuation_M,PETSC_NULL);CHKERRQ(ierr);
		user->continuation_m = 0;
		if (user->continuation_M!=0) {
			PetscInt max_its,max_lits,max_nfuncevals;
			
			max_its = max_lits = max_nfuncevals = 0;
			for (k=0; k<=user->continuation_M; k++) {
				PetscInt its,lits,nfuncevals;
				PetscScalar alpha_m;
				Vec velocity_last;
				
				
				/* force creation each time */
//			ierr = DMGetMatrix_DMCompositeStokesQkPm(multipys_pack,MATNEST,&JA);CHKERRQ(ierr);
				ierr = DMCompositeStokesCreateNest(multipys_pack,Auu,Aup,Apu,PETSC_NULL,&JA);CHKERRQ(ierr);
				
				ierr = SNESCreate(PETSC_COMM_WORLD,&snesIC);CHKERRQ(ierr);
				ierr = SNESSetOptionsPrefix(snesIC,"ic_");CHKERRQ(ierr);
				ierr = SNESSetFunction(snesIC,F,FormFunction_Stokes,user);CHKERRQ(ierr);  
				if (user->rheology_constants.rheology_type == RHEOLOGY_VISCO_ELASTIC_PLASTIC ) {
					ierr = SNESSetFunction(snesIC,F,FormFunction_Stokes_ViscoElastoPlastic,user);CHKERRQ(ierr);  
				}
				
				ierr = SNESSetJacobian(snesIC,JA,B,FormJacobian_Stokes,user);CHKERRQ(ierr);
				ierr = SNESSetFromOptions(snesIC);CHKERRQ(ierr);
				ierr = SNESGetTolerances(snesIC,0,0,0,&maxit,0);CHKERRQ(ierr);
				if (maxit==0) {
					PetscPrintf(PETSC_COMM_WORLD,"snesIC: skipping initial solve for Newton\n");
				}
				ierr = pTatinSetSNESContMonitor(snesIC,user);CHKERRQ(ierr);
				
				ierr = SNESGetKSP(snesIC,&kspIC);CHKERRQ(ierr);
				ierr = KSPGetPC(kspIC,&pcIC);CHKERRQ(ierr);
				
				ierr = DMCompositeGetGlobalISs(multipys_pack,&isg);CHKERRQ(ierr);
				ierr = PCFieldSplitSetIS(pcIC,"u",isg[0]);CHKERRQ(ierr);
				ierr = PCFieldSplitSetIS(pcIC,"p",isg[1]);CHKERRQ(ierr);
				
				/* configure uu split for galerkin multi-grid */
				ierr = StokesKSPFSMGuSetUp(multipys_pack,kspIC,nlevels,interpolation);CHKERRQ(ierr);
				
				alpha_m = (double)(user->continuation_m)/( (double)(user->continuation_M) );
				PetscPrintf(PETSC_COMM_WORLD,"[%d] alpha_m = %lf \n", k,alpha_m);
        /* boundary conditions */
        ierr = pTatin2d_ModelApplyBoundaryCondition(user);CHKERRQ(ierr);
				
				ierr = SNESSolve(snesIC,0,X);CHKERRQ(ierr);
				user->continuation_m++;
				
				ierr = SNESGetIterationNumber(snesIC,&its);CHKERRQ(ierr);
				ierr = SNESGetLinearSolveIterations(snesIC,&lits);CHKERRQ(ierr);
				ierr = SNESGetNumberFunctionEvals(snesIC,&nfuncevals);CHKERRQ(ierr);
				PetscPrintf(PETSC_COMM_WORLD,"SNES_continuation[%d/%d](StokesIC) Statistics \n", k,user->continuation_M);
				PetscPrintf(PETSC_COMM_WORLD,"  nonlinear its    = %D \n", its);
				PetscPrintf(PETSC_COMM_WORLD,"  total linear its = %D \n", lits);
				PetscPrintf(PETSC_COMM_WORLD,"  total func evals = %D \n", nfuncevals);
				
				max_its        += its;
				max_lits       += lits;
				max_nfuncevals += nfuncevals;

				/* force destroy */
				ierr = MatDestroy(&JA);CHKERRQ(ierr);CHKERRQ(ierr);
				ierr = SNESDestroy(&snesIC);CHKERRQ(ierr);
				ierr = ISDestroy(&isg[0]);CHKERRQ(ierr);
				ierr = ISDestroy(&isg[1]);CHKERRQ(ierr);
				ierr = PetscFree(isg);CHKERRQ(ierr);
				
			}	
			PetscPrintf(PETSC_COMM_WORLD,"SNES_continuation[final](StokesIC) Statistics \n");
			PetscPrintf(PETSC_COMM_WORLD,"  sum nonlinear its    = %D \n", max_its);
			PetscPrintf(PETSC_COMM_WORLD,"  sum total linear its = %D \n", max_lits);
			PetscPrintf(PETSC_COMM_WORLD,"  sum total func evals = %D \n", max_nfuncevals);
		}
		/* force the the counters to match so alpha_m = 1 on subsequent solves */
		user->continuation_m = user->continuation_M;
		
	}
	ierr = pTatin2d_ModelOutput(user,X,"continuation");CHKERRQ(ierr);
#endif	
	
	
	time = 0.0;
	ierr = pTatin2d_ComputeTimestep(user,user->pack,X);CHKERRQ(ierr);
	for (tk=1; tk<=user->nsteps; tk++) {
		time = time + user->dt;
		
    user->time = time; 
    user->step = tk;
		
    PetscPrintf(PETSC_COMM_WORLD,"step = %1.5d, time = %1.4e, dt = %1.4e \n", tk, time, user->dt );
    
    
    /* boundary conditions */
    ierr = pTatin2d_ModelApplyBoundaryCondition(user);CHKERRQ(ierr);
    ierr = pTatin2d_ModelApplyMaterialBoundaryCondition(user);CHKERRQ(ierr);
		
		/* creation */
		ierr = SNESCreate(PETSC_COMM_WORLD,&snes);CHKERRQ(ierr);
		
		ierr = SNESSetFunction(snes,F,FormFunction_Stokes,user);CHKERRQ(ierr);  
		if (user->rheology_constants.rheology_type == RHEOLOGY_VISCO_ELASTIC_PLASTIC ) {
			ierr = SNESSetFunction(snes,F,FormFunction_Stokes_ViscoElastoPlastic,user);CHKERRQ(ierr);  
		}
		
		if (user->use_mf_stokes) {
			ierr = SNESSetJacobian(snes,B,B,FormJacobian_Stokes,user);CHKERRQ(ierr);
		} else {
			ierr = SNESSetJacobian(snes,A,B,FormJacobian_Stokes,user);CHKERRQ(ierr);
		}
		ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
		
    if (tk>1){
      PetscReal rtol; 
      SNESGetTolerances(snes,PETSC_NULL,&rtol,PETSC_NULL,PETSC_NULL,PETSC_NULL);
      ierr = PetscOptionsGetReal(PETSC_NULL,"-tk_snes_rtol",&rtol,PETSC_NULL);CHKERRQ(ierr);  
      SNESSetTolerances(snes,PETSC_DEFAULT,rtol,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);
    }
    
		/* configure for fieldsplit */
		ierr = SNESGetKSP(snes,&ksp);CHKERRQ(ierr);
		ierr = KSPGetPC(ksp,&pc);CHKERRQ(ierr);
		
		ierr = DMCompositeGetGlobalISs(multipys_pack,&isg);CHKERRQ(ierr);
		ierr = PCFieldSplitSetIS(pc,"u",isg[0]);CHKERRQ(ierr);
		ierr = PCFieldSplitSetIS(pc,"p",isg[1]);CHKERRQ(ierr);

		/* configure uu split for galerkin multi-grid */
		ierr = StokesKSPFSMGuSetUp(multipys_pack,ksp,nlevels,interpolation);CHKERRQ(ierr);
		
		ierr = pTatinSetSNESMonitor(snes,user);CHKERRQ(ierr);
		//if (tk==1) {
		ierr = SNESSolve(snes,PETSC_NULL,X);CHKERRQ(ierr);
		//}
		
		/* statistics */
		if (user->solverstatistics) {
			PetscInt its,lits,nfuncevals;
			ierr = SNESGetIterationNumber(snes,&its);CHKERRQ(ierr);
			ierr = SNESGetLinearSolveIterations(snes,&lits);CHKERRQ(ierr);
			ierr = SNESGetNumberFunctionEvals(snes,&nfuncevals);CHKERRQ(ierr);
			PetscPrintf(PETSC_COMM_WORLD,"SNES(Stokes) Statistics \n");
			PetscPrintf(PETSC_COMM_WORLD,"  nonlinear its    = %D \n", its);
			PetscPrintf(PETSC_COMM_WORLD,"  total linear its = %D \n", lits);
			PetscPrintf(PETSC_COMM_WORLD,"  total func evals = %D \n", nfuncevals);
			
			pTatin2dSolverStatistics(user,snes,X);
		}
		
		/* update coordinates */
		{			
			Vec velocity,pressure;
			PetscReal step;
			
			ierr = DMCompositeGetAccess(user->pack,X,&velocity,&pressure);CHKERRQ(ierr);
			
			ierr = SwarmUpdatePosition_ComputeCourantStep_3(user->dav,velocity,&step);CHKERRQ(ierr);
			//PetscPrintf(PETSC_COMM_WORLD,"[3] timestep (courant) = %1.4e \n", step );
			
			ierr = SwarmUpdatePosition_ComputeCourantStep(user->dav,velocity,&step);CHKERRQ(ierr);
			//PetscPrintf(PETSC_COMM_WORLD,"[1] timestep (courant) = %1.4e \n", step );
			
			ierr = SwarmUpdatePosition_ComputeCourantStep_2(user->dav,velocity,&step);CHKERRQ(ierr);
			//PetscPrintf(PETSC_COMM_WORLD,"[2] timestep (courant) = %1.4e \n", step );
			
			ierr = DMCompositeRestoreAccess(user->pack,X,&velocity,&pressure);CHKERRQ(ierr);
			user->dt = (1.0/20.0) * step;
		}
		ierr = pTatin2d_ComputeTimestep(user,user->pack,X);CHKERRQ(ierr);
		
		//ierr = TkUpdateRheologyQuadratureStokes(user,user->dav,user->dap,X);CHKERRQ(ierr); // THIS NEEDS TO BE WRAPPED UP DAVE
		ierr = pTatin2dUpdateMaterialPoints(user,X);CHKERRQ(ierr);
		
		/* ADVECT MARKERS */
		{
			int npoints;
			MPntStd *mp_std;
			DataField PField;
			Vec velocity,pressure;
			
			DataBucketGetSizes(user->db,&npoints,PETSC_NULL,PETSC_NULL);
			DataBucketGetDataFieldByName(user->db, MPntStd_classname ,&PField);
			mp_std = PField->data;
			
			ierr = DMCompositeGetAccess(user->pack,X,&velocity,&pressure);CHKERRQ(ierr);
			ierr = SwarmUpdatePosition_MPntStd_Euler(user->dav,velocity,user->dt,npoints,mp_std);CHKERRQ(ierr);
			ierr = DMCompositeRestoreAccess(user->pack,X,&velocity,&pressure);CHKERRQ(ierr);
		}
		
		/* UPDATE MESH - advect + remesh */
		/*
		 ierr = pTatin2d_UpdateCoordinatesStd(user->pack,X,user->dt);CHKERRQ(ierr);
		 ierr = QuadratureStokesCoordinateSetUp(user->Q,user->dav);CHKERRQ(ierr);
		 */
		
		//
		ierr = pTatin2d_ModelUpdateMeshGeometry(user,X);CHKERRQ(ierr);
    ierr = QuadratureStokesCoordinateSetUp(user->Q,user->dav);CHKERRQ(ierr);
		for (e=0; e<QUAD_EDGES; e++) {
			ierr = SurfaceQuadratureStokesGeometrySetUp(user->surfQ[e],user->dav);CHKERRQ(ierr);
		}
		//
		
		/* UPDATE local coordinates */
		{
			int p,npoints;
			MPntStd *mp_std;
			DataField PField;
			double tolerance;
			int max_its;
			Truth use_nonzero_guess, monitor, log;
			DM cda;
			Vec gcoords;
			PetscScalar *LA_gcoords;
			const PetscInt *elnidx_u;
			PetscInt nel,nen_u;
			PetscInt *gidx;
			PetscInt lmx,lmy;
			
			/* get marker fields */
			DataBucketGetSizes(user->db,&npoints,PETSC_NULL,PETSC_NULL);
			DataBucketGetDataFieldByName(user->db, MPntStd_classname ,&PField);
			mp_std = PField->data;
			
			/* setup for coords */
			ierr = DMDAGetCoordinateDA(user->dav,&cda);CHKERRQ(ierr);
			ierr = DMDAGetGhostedCoordinates(user->dav,&gcoords);CHKERRQ(ierr);
			ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
			
			ierr = DMDAGetGlobalIndices(user->dav,0,&gidx);CHKERRQ(ierr);
			ierr = DMDAGetElements_pTatin(user->dav,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
			
			ierr = DMDAGetLocalSizeElementQ2(user->dav,&lmx,&lmy,PETSC_NULL);CHKERRQ(ierr);
			
			/* point location parameters */
			tolerance         = 1.0e-10;
			max_its           = 10;
			use_nonzero_guess = _TRUE;
			monitor           = _FALSE;
			log               = _FALSE;
			
			InverseMappingDomain_2dQ2( 		 tolerance, max_its,
																use_nonzero_guess, 
																monitor, log,
																(const double*)LA_gcoords, (const int)lmx,(const int)lmy, (const int*)elnidx_u,
																npoints, mp_std );
			
			ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
			
		}
		
//		ierr = SwarmUpdatePosition_Communication_MPntPStokes(user->dav,user->db,user->ex);CHKERRQ(ierr);
		ierr = SwarmUpdatePosition_Communication_Generic(user->dav,user->db,user->ex);CHKERRQ(ierr);
		
		/* remove all points which didn't find a home */
		{
			int p,npoints,escaped;
			MPntStd *mp_std;
			DataField PField;
			PetscMPIInt rank;
			
			MPI_Comm_rank(((PetscObject)user->dav)->comm,&rank);
			
			/* get marker fields */
			DataBucketGetSizes(user->db,&npoints,PETSC_NULL,PETSC_NULL);
			DataBucketGetDataFieldByName(user->db, MPntStd_classname ,&PField);
			mp_std = PField->data;
			
			escaped = 0;
			for (p=0; p<npoints; p++) {
				if (mp_std[p].wil == -1) {
					escaped++;
				}
			}
			//PetscPrintf(PETSC_COMM_SELF,"[%d] MPntStd: npoints = %d : found_locally = %d \n", rank, npoints, npoints-escaped );
			
			/* remove points which left processor */
			if (escaped!=0) {
				PetscPrintf(PETSC_COMM_SELF,"  *** MPntStd removal: Identified %d points which are not contained on subdomain (after communication) \n", escaped );
			}
			if (escaped!=0) {
				for (p=0; p<npoints; p++) {
					if (mp_std[p].wil == -1) {
						printf("############################ Point %d not located in domain (%1.6e , %1.6e) ############################# \n",p,mp_std[p].coor[0],mp_std[p].coor[1]);
						
						/* kill point */
						DataBucketRemovePointAtIndex(user->db,p);
						DataBucketGetSizes(user->db,&npoints,0,0); /* you need to update npoints as the list size decreases! */
						p--; /* check replacement point */
					}
				}
			}
			
		}
		
		/* pop control */
		//ierr = MPPC_AVDPatch(7,100,1, 10,10, 0.0,user->dav,user->db);CHKERRQ(ierr);
		ierr = MaterialPointPopulationControl(user);CHKERRQ(ierr);
		
		
		/* output */
		if (tk%user->output_frequency==0) {
			char *prefix;
			asprintf(&prefix,"step%1.5d",tk);
			ierr = pTatin2d_ModelOutput(user,X,prefix);CHKERRQ(ierr);
			free(prefix);
		}
		/*		
		 {
		 PetscBool write_checkpoint=PETSC_FALSE;
		 
		 PetscOptionsGetBool(PETSC_NULL,"-write_cp",&write_checkpoint,0);
		 if (write_checkpoint==PETSC_TRUE) {
		 printf("writing checkpoint \n");
		 ierr = pTatinCheckpointWrite(user,PETSC_NULL);CHKERRQ(ierr);
		 }
		 }
		 */		
		/* tidy up */
		ierr = ISDestroy(&isg[0]);CHKERRQ(ierr);
		ierr = ISDestroy(&isg[1]);CHKERRQ(ierr);
		ierr = PetscFree(isg);CHKERRQ(ierr);
		ierr = SNESDestroy(&snes);CHKERRQ(ierr);
	}
	
	for (k=1; k<nlevels; k++) {
		ierr = MatDestroy(&interpolation[k]);CHKERRQ(ierr);
	}
	ierr = PetscFree(interpolation);CHKERRQ(ierr);

	for (k=0; k<nlevels; k++) {
		ierr = DMDestroy(&dav_hierarchy[k]);CHKERRQ(ierr);
	}
	ierr = PetscFree(dav_hierarchy);CHKERRQ(ierr);
	
	if (Auu) { ierr = MatDestroy(&Auu);CHKERRQ(ierr); }
	if (Aup) { ierr = MatDestroy(&Aup);CHKERRQ(ierr); }
	if (Apu) { ierr = MatDestroy(&Apu);CHKERRQ(ierr); }
	if (Spp) { ierr = MatDestroy(&Spp);CHKERRQ(ierr); }
	
	if (!user->use_mf_stokes) {
		ierr = MatDestroy(&A);CHKERRQ(ierr);
	}
  ierr = MatDestroy(&B);CHKERRQ(ierr);
  ierr = VecDestroy(&X);CHKERRQ(ierr);
  ierr = VecDestroy(&F);CHKERRQ(ierr);
	
	ierr = pTatin2dDestroyContext(&user);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc,char **argv)
{
	PetscErrorCode ierr;
	
	ierr = PetscInitialize(&argc,&argv,0,help);CHKERRQ(ierr);

	ierr = pTatinWriteOptionsFile(PETSC_NULL);CHKERRQ(ierr);
	
	ierr = pTatin2d_material_points_std(argc,argv);CHKERRQ(ierr);

	ierr = PetscFinalize();CHKERRQ(ierr);
	return 0;
}
