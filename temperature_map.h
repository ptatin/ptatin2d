
#ifndef __TEMP_MAP_H__
#define __TEMP_MAP_H__

typedef enum { TEMP_MAP_POINT_OUTSIDE=-1, TEMP_MAP_POINT_INSIDE=1 } TempMapLocationIndicator;

typedef struct _p_TempMap *TempMap;
struct _p_TempMap {
	double x0,y0,x1,y1;
	double dx,dy;
	int mx,my;
	double *data;
};

void TempMapCreate(TempMap *map);
void TempMapDestroy(TempMap *map);
void TempMapGetIndex(TempMap pm,const int i,const int j, int *index);
void TempMapLoadFromFile(const char filename[],TempMap *map);
void TempMapLoadFromFile_ASCII_ZIPPED(const char filename[],TempMap *map);
void TempMapLoadFromFile_ASCII(const char filename[],TempMap *map);
void TempMapLoadFromFileScale(const char filename[],TempMap *map,pTatinUnits *units);
void TempMapLoadFromFileScale_ASCII_ZIPPED(const char filename[],TempMap *map,pTatinUnits *units);
void TempMapLoadFromFileScale_ASCII(const char filename[],TempMap *map,pTatinUnits *units);
void TempMapGetTemperature(TempMap Tempmap,double xp[],double *Temp);
void TempMapViewGnuplot(const char filename[],TempMap Tempmap);
void TempMapGetDouble(TempMap Tempmap,double xp[],double *Temp);
void RheolMapGetDouble(TempMap Tempmap,double xp[],double *Temp);
PetscErrorCode pTatinCtxGetMap(pTatinCtx ctx,TempMap *map, char *name);
PetscErrorCode pTatinCtxAttachMap(pTatinCtx ctx,TempMap map, char *name);
void TempMapApplyScale(TempMap map,pTatinUnits *units);
void DensMapApplyScale(TempMap map,pTatinUnits *units);
void ProdMapApplyScale(TempMap map,pTatinUnits *units);
void RheolMapApplyScale(TempMap map,pTatinUnits *units);
PetscErrorCode pTatinCtxAttachTempMap(pTatinCtx ctx,TempMap map);
PetscErrorCode pTatinCtxGetTempMap(pTatinCtx ctx,TempMap *map);

#endif