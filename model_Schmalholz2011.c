/*
 
 Model Description:
 Example reading geometry, rheology and everything from file and command line.
 Model size must be smaller than pmap.
 In case of an air layer, it must be prescribed in the map file! (diff with MAP_LV)  
 
 
 Input / command line parameters:
 
 
 
 
 
 
 
 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h" 
#include "private/daimpl.h"

#include "pTatin2d.h"
//#include "pTatin2d_models.h"
#define WRITE_VP
//#define WRITE_TFIELD
#define WRITE_MPOINTS
#define WRITE_QPOINTS
//#define WRITE_TCELL
#define WRITE_INT_FIELDS
//#define WRITE_FACEQPOINTS



#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_Schmalholz2011"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_Schmalholz2011(pTatinCtx ctx)
{
	PhaseMap phasemap;
	PetscInt M,N,experiment;
	PetscBool flg;
	PetscBool found;
	PetscBool half_model=PETSC_FALSE;
	PetscErrorCode ierr;
	PetscReal Ox,Oy,Lx,Ly,gravity_acceleration;
	char name[PETSC_MAX_PATH_LEN];
	char map_file[PETSC_MAX_PATH_LEN];
	char topo_file[PETSC_MAX_PATH_LEN];
	pTatinUnits *units;	
	
	PetscFunctionBegin;
	units   = &ctx->units;
	
	/* make a regular non dimensional grid from option file parameters*/
	found = PETSC_TRUE; 
    gravity_acceleration = 9.81; 
	ierr = QuadratureStokesGravityModelCoordinateAlignedSetValues(ctx->Q,0.0, -gravity_acceleration);CHKERRQ(ierr);
    
    ierr  = PetscOptionsGetBool("Schmalholz2011_","-half_model",&half_model,0);CHKERRQ(ierr);
			Ox = 0.0;
    		Oy = -6.6e5;
    		Lx = 1.0e6;
    		Ly = 0.0;
	if (half_model){
			Lx = 0.5e6;
			}
	PetscPrintf(PETSC_COMM_WORLD,"ApplyInitialMeshGeometry_Schmalholz2011: options Domain_SI = [ %1.4e , %1.4e ] x [ %1.4e , %1.4e ] \n", Ox, Lx,Oy,Ly );	
	UnitsApplyInverseScaling(units->si_length,Ox,&Ox);
	UnitsApplyInverseScaling(units->si_length,Oy,&Oy);
	UnitsApplyInverseScaling(units->si_length,Lx,&Lx);
	UnitsApplyInverseScaling(units->si_length,Ly,&Ly);
	
	PetscPrintf(PETSC_COMM_WORLD,"ApplyInitialMeshGeometry_Schmalholz2011: options Domain_ND = [ %1.4e , %1.4e ] x [ %1.4e , %1.4e ] \n", Ox, Lx,Oy,Ly );
	ierr = DMDASetUniformCoordinates(ctx->dav,Ox,Lx,Oy,Ly, 0.0,0.0);CHKERRQ(ierr);
		
	PetscFunctionReturn(0);
}
/*pTatin2d_ModelDeformUpperSurfaceFromOptions_GENE
 this function is used to deform the upper surface of the mesh 
 */
 
#undef __FUNCT__ 
#define __FUNCT__ "pTatin2d_ModelSetMaterialProperty_Schmalholz2011"
PetscErrorCode pTatin2d_ModelSetMaterialProperty_Schmalholz2011(pTatinCtx ctx)

{
	RheologyConstants      *rheology;
	PetscInt               nphase, i,rheol_type, M,N,P,air,experiment;
	PetscErrorCode         ierr;
	char   				   *option_name;
	PetscBool              found; 
	DM             dav,dap;    
    
	PetscFunctionBegin;
	
    ierr = DMCompositeGetEntries(ctx->pack,&dav,&dap);CHKERRQ(ierr);
    ierr = DMDAGetInfo(dav,0,&M,&N,&P,0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);	
    
	rheology   = &ctx->rheology_constants;
	rheology->rheology_type = 6;
    
    rheology->eta_upper_cutoff_global = 1.e25;
	rheology->eta_lower_cutoff_global = 1.e18;
	ierr       = PetscOptionsGetReal("GENE_","-eta_max_cut_off",&rheology->eta_upper_cutoff_global,0);CHKERRQ(ierr);
	ierr       = PetscOptionsGetReal("GENE_","-eta_min_cut_off",&rheology->eta_lower_cutoff_global,0);CHKERRQ(ierr);
	experiment = 1; 
	PetscOptionsGetInt("Schmalholz2011_","-experiment",&experiment,0);

		nphase = 2;
	   for (i=0; i< nphase ;i++) {
		
        rheology->density_type[i]           = 0; 
        rheology->viscous_type[i]           = VISCOUS_POWERLAW; 
		rheology->plastic_type[i]           = 0;
        rheology->softening_type[i]         = 0;
    }
    rheology->const_rho0[0]             = 3150.0;
    rheology->const_rho0[1]             = 3300.0;

	switch(experiment){
	
	case 1 :
        rheology->const_eta0[0]             = 1.0e21;
        rheology->viscous_type[0]           = VISCOUS_CONSTANT; 
        rheology->arrh_preexpA[1]           = 4.75e11;
        rheology->arrh_nexp[1]              = 4;
		break;
	
	case 2 : 
		rheology->arrh_preexpA[0]           = 4.54e10;
		rheology->arrh_nexp[0]              = 3;
        rheology->arrh_preexpA[1]           = 4.75e11;
        rheology->arrh_nexp[1]              = 4;
		break;
		
	}
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelSetIndexOnMarker_Schmalholz2011"
PetscErrorCode pTatin2d_ModelSetIndexOnMarker_Schmalholz2011(pTatinCtx ctx)
{
	PetscInt               e,p,n_mp_points;
	DataBucket             db;
	DataField              PField_std,PField_stokes;
	int                    phase_init, phase, phase_index, is_valid, i;
	PetscScalar            lithosphere_depth,slab_halfwidth,slab_length,slab_loc;
	PetscErrorCode         ierr;
	pTatinUnits *units;	
	
	PetscFunctionBegin;
	units   = &ctx->units;
	
	
	
	//Set global Cut off for viscosity here but it is maybe not the best place
	lithosphere_depth = -80.0e3;
	slab_halfwidth    =  40.0e3;
	slab_length       = 250.0e3; 
	slab_loc          = 500.0e3; 
	ierr = PetscOptionsGetScalar("Schmalholz2011","-slab_length",&slab_length,0);CHKERRQ(ierr);
    UnitsApplyInverseScaling(units->si_length,lithosphere_depth,&lithosphere_depth);
	UnitsApplyInverseScaling(units->si_length,slab_halfwidth,&slab_halfwidth);
	UnitsApplyInverseScaling(units->si_length,slab_length,&slab_length);
	UnitsApplyInverseScaling(units->si_length,slab_loc,&slab_loc);
	

	/* define phase on material points */
	db = ctx->db;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
		
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	for (p=0; p<n_mp_points; p++) {
		MPntStd       *material_point;
		double  *position;
	    phase = 0; 	
		DataFieldAccessPoint(PField_std,p,(void**)&material_point);
		
		/* Access using the getter function  (recommeneded for beginner user) */
		MPntStdGetField_global_coord(material_point,&position);
		
		if (position[1] > lithosphere_depth){
			/* point located in the notch */
			phase = 1;
		}else {
			if (position[1] > (lithosphere_depth-slab_length)){
				if (fabs(position[0]-slab_loc) < slab_halfwidth){
					phase=1;
				}
			}
		}
			
		/* asign phases and initializes viscosity to ref viscosity*/
		MPntStdSetField_phase_index(material_point,phase);
	}
	
	DataFieldRestoreAccess(PField_std);
	
	PetscFunctionReturn(0);
}




#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_Schmalholz2011"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_Schmalholz2011(pTatinCtx ctx)
{
	BCList         ubclist;
	PetscScalar    bcval;
	DM             dav;
	PetscErrorCode ierr;
	
	PetscReal jmin[3],jmax[3]; 
	
	PetscInt opts_bcs,experiment;
	PetscBool  free_slip=PETSC_FALSE; 
	PetscBool  half_model=PETSC_FALSE; 
	pTatinUnits *units;
    
    PetscFunctionBegin;

	
	units   = &ctx->units;
	ubclist = ctx->u_bclist;
	dav     = ctx->dav;
	
	ierr  = PetscOptionsGetBool("Schmalholz2011_","-free_slip",&free_slip,0);CHKERRQ(ierr);
	ierr  = PetscOptionsGetBool("Schmalholz2011_","-half_model",&half_model,0);CHKERRQ(ierr);
	bcval = 0.0; 
	ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
	ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
	ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
	if (free_slip){
	ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
	}
	if (!half_model){
	   ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	}
    PetscFunctionReturn(0);
}



#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelUpdateMeshGeometry_Schmalholz2011"
PetscErrorCode pTatin2d_ModelUpdateMeshGeometry_Schmalholz2011(pTatinCtx ctx,Vec X)
{
	static int     beenhere=0;
	PetscReal      step;
	Vec            velocity,pressure;
	DM             dav,dap;
	PetscScalar    Unormal[2],Vnormal_base;
	PetscInt       remesh_every;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	step = ctx->dt;
	
	ierr = DMCompositeGetEntries(ctx->pack,&dav,&dap);CHKERRQ(ierr);
	ierr = DMCompositeGetAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
	
	remesh_every = 1; 
	PetscOptionsGetInt(PETSC_NULL,"-remesh_every",&remesh_every,0);
	
		
		  if (fmod(ctx->step,remesh_every)==0){
			ierr = UpdateMeshGeometry_FullLagSurfaceJMAX_RemeshInteriorJMIN_JMAX(dav,velocity,step);CHKERRQ(ierr);
		    PetscPrintf(PETSC_COMM_WORLD,"RemeshType \"UpdateMeshGeometry_FullLagSurfaceJMAX_RemeshInteriorJMIN_JMAX\"\n");
		   }else{
	        PetscPrintf(PETSC_COMM_WORLD," RemeshType \"UpdateMeshGeometry_FullLagSurface\"\n");
		    ierr = UpdateMeshGeometry_FullLagSurface(dav,velocity,step);CHKERRQ(ierr);
		   }
	
	ierr = DMCompositeRestoreAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
	
    
	beenhere = 1;
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "Schmalholz2011ComputeDissipation"
PetscErrorCode Schmalholz2011ComputeDissipation(pTatinCtx ctx,Vec X,PetscReal *_dissipation_all,PetscReal *_dissipation_slab)
{
	PetscErrorCode ierr;
	DM dau,dap,cda;
	ConformingElementFamily element;
	PetscInt gp,e,nel,nen_u,nen_p;
	const PetscInt *elnidx_u;
	const PetscInt *elnidx_p;
	Vec Uloc,Ploc,gcoords;
	PetscScalar *LA_Uloc,*LA_Ploc;
	PetscScalar    *LA_gcoords;
	PetscScalar    NIu[Q2_NODES_PER_EL_2D], NIp[P_BASIS_FUNCTIONS];
	double __GNIu[2*Q2_NODES_PER_EL_2D], __GNXu[2*Q2_NODES_PER_EL_2D];
	double *GNIu[2];
	double *GNXu[2];
	PetscScalar    volJ,elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar    el_vel[2*Q2_NODES_PER_EL_2D];
	PetscScalar    el_p[P_BASIS_FUNCTIONS];
	PetscReal integral_local[2],integral[2];
	GaussPointCoefficientsStokes *gausspoints;
	
	
	GNIu[0] = &__GNIu[0];
	GNIu[1] = &__GNIu[Q2_NODES_PER_EL_2D*1];
	GNXu[0] = &__GNXu[0];
	GNXu[1] = &__GNXu[Q2_NODES_PER_EL_2D*1];
	
	
	ierr = DMCompositeGetEntries(ctx->pack,&dau,&dap);CHKERRQ(ierr);
	ierr = DMCompositeGetLocalVectors(ctx->pack,&Uloc,&Ploc);CHKERRQ(ierr);
	
	ierr = DMCompositeScatter(ctx->pack,X,Uloc,Ploc);CHKERRQ(ierr);
	ierr = VecGetArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
	ierr = VecGetArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
	
	
	
	ierr = DMDAGetCoordinateDA(dau,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(dau,&gcoords);CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	ierr = DMDAGetElements_pTatin(dap,&nel,&nen_p,&elnidx_p);CHKERRQ(ierr);
	
	
	element = ctx->surfQ[0]->e; /* take any of these - we are just going to call the BasisFuncGrad() and ComputeGeom functions */
	
	integral_local[0] = 0.0;
	integral_local[1] = 0.0;
	
	for (e=0; e<nel; e++) {
		GaussPointCoefficientsStokes *point;
		PetscInt ii,jj;
		PetscReal E[2][2],pqp,sigma[2][2],eta_qp,rho_qp;
	    PetscReal integral_el= 0.0;
	    PetscScalar ispureslab = 0.0;
	    
	    
	    
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		
		ierr = DMDAGetVectorElementFieldQ2_2D(el_vel,(PetscInt*)&elnidx_u[nen_u*e],LA_Uloc);CHKERRQ(ierr);
		ierr = DMDAGetScalarElementField_2D(el_p,nen_p,(PetscInt*)&elnidx_p[nen_p*e],LA_Ploc);CHKERRQ(ierr);
		
		ierr = QuadratureStokesGetCell(ctx->Q,e,&gausspoints);CHKERRQ(ierr);
		
		for (ii=0; ii<2*U_BASIS_FUNCTIONS; ii++) {
			UnitsApplyScaling(ctx->units.si_length,elcoords[ii],&elcoords[ii]);
			UnitsApplyScaling(ctx->units.si_velocity,el_vel[ii],&el_vel[ii]);
		}
		for (jj=0; jj<P_BASIS_FUNCTIONS; jj++) {
			UnitsApplyScaling(ctx->units.si_stress,el_p[jj],&el_p[jj]);
		}
		
		for (gp=0; gp<ctx->Q->ngp; gp++) {
			QPoint2d tmp_point;
			PetscScalar *xi_p;
            
			
			xi_p = &ctx->Q->xi[2*gp];
			
			tmp_point.xi  = xi_p[0];
			tmp_point.eta = xi_p[1];
			tmp_point.w   = ctx->Q->weight[gp];
			
			element->basis_NI_2D(&tmp_point,NIu);
			element->basis_GNI_2D(&tmp_point,GNIu);
			element->compute_volume_geometry_2D(elcoords,(const double**)GNIu,GNXu,&volJ);
			
			ConstructNi_pressure(xi_p,elcoords,NIp);
			
			E[0][0] = E[0][1] = E[1][0] = E[1][1] = 0.0;
			for (ii=0; ii<U_BASIS_FUNCTIONS; ii++) {
				PetscScalar vx = el_vel[2*ii  ];
				PetscScalar vy = el_vel[2*ii+1];
				
				E[0][0] += 0.5 * ( GNXu[0][ii] * vx + GNXu[0][ii] * vx );
				E[0][1] += 0.5 * ( GNXu[1][ii] * vx + GNXu[0][ii] * vy );
                
				E[1][0] += 0.5 * ( GNXu[0][ii] * vy + GNXu[1][ii] * vx );
				E[1][1] += 0.5 * ( GNXu[1][ii] * vy + GNXu[1][ii] * vy );
				
			}
			pqp = 0.0;
			for (jj=0; jj<P_BASIS_FUNCTIONS; jj++) {
				pqp += NIp[jj] * el_p[jj];
			}
			
			eta_qp = gausspoints[gp].eta;
			UnitsApplyScaling(ctx->units.si_viscosity,eta_qp,&eta_qp);
			
			
			rho_qp = gausspoints[gp].Fu[1]/gausspoints[gp].gravity[1];
			UnitsApplyScaling(ctx->units.si_force_per_volume,rho_qp,&rho_qp);
			
			if (rho_qp > 3299.0){
                ispureslab = 1.0;
            }
			
			sigma[0][0] = 2.0 * eta_qp * E[0][0] - pqp;
			sigma[1][1] = 2.0 * eta_qp * E[1][1] - pqp;
            
			sigma[0][1] = 2.0 * eta_qp * E[0][1];
			sigma[1][0] = 2.0 * eta_qp * E[1][0];
			
			integral_el = integral_el + tmp_point.w * 0.5 * ( sigma[0][0]*E[0][0] + sigma[0][1]*E[0][1] + sigma[1][0]*E[1][0] + sigma[1][1]*E[1][1] ) * volJ;
		}
		
        integral_local[0]   = integral_local[0] + integral_el;
        integral_local[1]   = integral_local[1] + ispureslab*integral_el;
		
	}
	
	ierr = MPI_Allreduce(&integral_local,&integral,2,MPIU_REAL,MPI_SUM,PETSC_COMM_WORLD);CHKERRQ(ierr);
	*_dissipation_all  = integral[0];
	*_dissipation_slab = integral[1];
	
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	ierr = VecRestoreArray(Ploc,&LA_Ploc);CHKERRQ(ierr);
	ierr = VecRestoreArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
	ierr = DMCompositeRestoreLocalVectors(ctx->pack,&Uloc,&Ploc);CHKERRQ(ierr);
	
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "Schmalholz2011ComputeVRMS"
PetscErrorCode Schmalholz2011ComputeVRMS(pTatinCtx ctx,Vec X,PetscReal *_vrms_all,PetscReal *_vrms_slab)
{
	PetscErrorCode ierr;
	DM dau,dap,cda;
	ConformingElementFamily element;
	PetscInt gp,e,nel,nen_u;
	const PetscInt *elnidx_u;
	Vec Uloc,Ploc,gcoords;
	PetscScalar *LA_Uloc;
	PetscScalar    *LA_gcoords;
	PetscScalar    NIu[Q2_NODES_PER_EL_2D];
	double __GNIu[2*Q2_NODES_PER_EL_2D], __GNXu[2*Q2_NODES_PER_EL_2D];
	double *GNIu[2];
	double *GNXu[2];
	PetscScalar    volJ,elcoords[2*Q2_NODES_PER_EL_2D];
	PetscScalar    el_vel[2*Q2_NODES_PER_EL_2D];
	PetscReal integral_local[4],integral[4];
	
	pTatinUnits *units;
	GaussPointCoefficientsStokes *gausspoints;
	
	units   = &ctx->units;
	
	GNIu[0] = &__GNIu[0];
	GNIu[1] = &__GNIu[Q2_NODES_PER_EL_2D*1];
	GNXu[0] = &__GNXu[0];
	GNXu[1] = &__GNXu[Q2_NODES_PER_EL_2D*1];
	
	
	ierr = DMCompositeGetEntries(ctx->pack,&dau,&dap);CHKERRQ(ierr);
	ierr = DMCompositeGetLocalVectors(ctx->pack,&Uloc,&Ploc);CHKERRQ(ierr);
	
	ierr = DMCompositeScatter(ctx->pack,X,Uloc,Ploc);CHKERRQ(ierr);
	ierr = VecGetArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
	
	ierr = DMDAGetCoordinateDA(dau,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(dau,&gcoords);CHKERRQ(ierr);
	ierr = VecGetArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetElements_pTatin(dau,&nel,&nen_u,&elnidx_u);CHKERRQ(ierr);
	
	
	element = ctx->surfQ[0]->e; /* take any of these - we are just going to call the BasisFuncGrad() and ComputeGeom functions */
	
	integral_local[0] = integral_local[1] = 0.0;
	integral_local[2] = integral_local[3] = 0.0;
	
	for (e=0; e<nel; e++) {
		GaussPointCoefficientsStokes *point;
		PetscInt ii,jj;
		PetscReal uqp,vqp,rho_qp;
        PetscScalar integral_elem[2];
        PetscScalar  ispureslab = 0.0;
        
        integral_elem[0] = 0.0;
	    integral_elem[1] = 0.0;
	    
		ierr = DMDAGetElementCoordinatesQ2_2D(elcoords,(PetscInt*)&elnidx_u[nen_u*e],LA_gcoords);CHKERRQ(ierr);
		
		ierr = DMDAGetVectorElementFieldQ2_2D(el_vel,(PetscInt*)&elnidx_u[nen_u*e],LA_Uloc);CHKERRQ(ierr);
		
		
		for (ii=0; ii<2*U_BASIS_FUNCTIONS; ii++) {
			UnitsApplyScaling(ctx->units.si_length,elcoords[ii],&elcoords[ii]);
			UnitsApplyScaling(ctx->units.si_velocity,el_vel[ii],&el_vel[ii]);
		}
		ierr = QuadratureStokesGetCell(ctx->Q,e,&gausspoints);CHKERRQ(ierr);
		
		for (gp=0; gp<ctx->Q->ngp; gp++) {
			QPoint2d tmp_point;
			PetscScalar *xi_p;
			PetscScalar rho_qp;
			rho_qp = gausspoints[gp].Fu[1]/gausspoints[gp].gravity[1];
			UnitsApplyScaling(ctx->units.si_force_per_volume,rho_qp,&rho_qp);
			
			if (rho_qp > 3299.0){
                ispureslab = 1.0;
            }
            
			
			xi_p = &ctx->Q->xi[2*gp];
			
			tmp_point.xi  = xi_p[0];
			tmp_point.eta = xi_p[1];
			tmp_point.w   = ctx->Q->weight[gp];
			
			
			element->basis_NI_2D(&tmp_point,NIu);
			element->basis_GNI_2D(&tmp_point,GNIu);
			element->compute_volume_geometry_2D(elcoords,(const double**)GNIu,GNXu,&volJ);
			
			uqp = vqp = 0.0;
			for (ii=0; ii<U_BASIS_FUNCTIONS; ii++) {
				PetscScalar vx = el_vel[2*ii  ];
				PetscScalar vy = el_vel[2*ii+1];
				uqp += NIu[ii] * vx;
				vqp += NIu[ii] * vy;
			}
			
		   	integral_elem[0] = integral_elem[0] + tmp_point.w * (uqp*uqp + vqp*vqp) * volJ;
			integral_elem[1] = integral_elem[1] + tmp_point.w * 1.0 * volJ;
			
		}
		
	    integral_local[0] = integral_local[0] + integral_elem[0]; // vrms over whole domain
		integral_local[1] = integral_local[1] + integral_elem[1]; // vol domain

		integral_local[2] = integral_local[2] + ispureslab*integral_elem[0]; // vrms over the slab domain
		integral_local[3] = integral_local[3] + ispureslab*integral_elem[1]; // vol slab
	}
	
	ierr = MPI_Allreduce(integral_local,integral,4,MPIU_REAL,MPI_SUM,PETSC_COMM_WORLD);CHKERRQ(ierr);
	*_vrms_all  = sqrt( integral[0]  / integral[1]);
	*_vrms_slab = sqrt( integral[2]  / integral[3]);
	
	ierr = VecRestoreArray(gcoords,&LA_gcoords);CHKERRQ(ierr);
	ierr = VecRestoreArray(Uloc,&LA_Uloc);CHKERRQ(ierr);
	ierr = DMCompositeRestoreLocalVectors(ctx->pack,&Uloc,&Ploc);CHKERRQ(ierr);
	
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "Schmalholz2011ModelDiagnostics"
PetscErrorCode Schmalholz2011ModelDiagnostics(pTatinCtx ctx,Vec X)
{
	static PetscBool beenhere = PETSC_FALSE;
	FILE *fp = NULL;
	char filename[PETSC_MAX_PATH_LEN];
	PetscInt experiment,coeff;
	PetscBool bc;
	PetscReal diss,diss_slab,vrms,vrms_slab,time;
	PetscMPIInt rank;
	PetscErrorCode ierr;
	
	PetscOptionsGetInt("Schmalholz2011_","-experiment",&experiment,0);
	PetscOptionsGetBool("Schmalholz2011_","-free_slip",&bc,0);
    PetscOptionsGetInt(0,"-coefficient_average_type",&coeff,0);
    
	
	if (bc) {
        if (experiment == 1) {
            sprintf(filename,"%s/exp1a-%dx%d-coeff%d-diag.dat",ctx->outputpath,ctx->mx,ctx->my,coeff);
        }else if (experiment == 2){
            sprintf(filename,"%s/exp1b-%dx%d-coeff%d-diag.dat",ctx->outputpath,ctx->mx,ctx->my,coeff);
        }
	}else{
        if (experiment == 1) {
            sprintf(filename,"%s/exp2a-%dx%d-coeff%d-diag.dat",ctx->outputpath,ctx->mx,ctx->my,coeff);
        }else if (experiment == 2){
            sprintf(filename,"%s/exp2b-%dx%d-coeff%d-diag.dat",ctx->outputpath,ctx->mx,ctx->my,coeff);
        }
	}
	
	ierr = MPI_Comm_rank(PETSC_COMM_WORLD,&rank);CHKERRQ(ierr);
    
	if (beenhere == PETSC_FALSE) {
		if (rank == 0) {
            fp = fopen(filename,"w");
            fprintf(fp,"# Schmalholz2011 diagnostics \n");
            fprintf(fp,"# time (sec)  dissipation (W/m) dissipation slab (W/m) vrms (m/2) vrms slab (m/2)    \n");
		}
		beenhere = PETSC_TRUE;
	} else {
		if (rank == 0) {
            fp = fopen(filename,"a");
		}
	}
	if (rank == 0) {
        if (fp == NULL) { SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER,"Cannot open Schmalholz2011 diagnostic file"); }
	}
    
	
	/* compute dissipation */
	diss = diss_slab = 0.0;
	ierr = Schmalholz2011ComputeDissipation(ctx,X,&diss,&diss_slab);CHKERRQ(ierr);
	
	/* compute velocity rms */
	vrms = vrms_slab = 0.0;
	ierr = Schmalholz2011ComputeVRMS(ctx,X,&vrms,&vrms_slab);CHKERRQ(ierr);
	
	UnitsApplyScaling(ctx->units.si_time,ctx->time,&time);
    
	if (rank == 0) {
        fprintf(fp,"%1.9e %1.9e %1.9e %1.9e %1.9e \n",time,diss,diss_slab,vrms,vrms_slab);
        fclose(fp);
	}	
    
	PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelOutput_Schmalholz2011"
PetscErrorCode pTatin2d_ModelOutput_Schmalholz2011(pTatinCtx ctx,Vec X,const char prefix[])
{
	static int     beenhere = 0;
	char           pvdfilename[PETSC_MAX_PATH_LEN];
	PetscBool      active,use_dimensional_units;
	char           vtkfilename[PETSC_MAX_PATH_LEN];
	char           filename[PETSC_MAX_PATH_LEN];
	char           name[PETSC_MAX_PATH_LEN];
	PetscErrorCode ierr;
	
	ierr = Schmalholz2011ModelDiagnostics(ctx,X);CHKERRQ(ierr);

	use_dimensional_units = PETSC_FALSE;
	ierr = PetscOptionsGetBool(PETSC_NULL,"-use_dimensional_units",&use_dimensional_units,PETSC_NULL);CHKERRQ(ierr);
	
	if (use_dimensional_units) {
		ierr = pTatin_MaterialPointScaleProperties(ctx->db,&ctx->units);CHKERRQ(ierr);
	}
	
	if (prefix == PETSC_NULL) {
#ifdef WRITE_QPOINTS
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,"qpoints");CHKERRQ(ierr);
#endif
#ifdef WRITE_FACEQPOINTS
		ierr = pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(ctx,ctx->outputpath,"faceqpoints");CHKERRQ(ierr);
#endif
#ifdef WRITE_VP
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,"vp");CHKERRQ(ierr);
#endif
#ifdef WRITE_INT_FIELDS
		ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,"int_fields");CHKERRQ(ierr);
#endif
#ifdef WRITE_MPOINTS
		ierr = SwarmOutputParaView_MPntPStokes(ctx->db,ctx->outputpath,"mpoints_stk");CHKERRQ(ierr);
#endif
	} else { /* example of adding a different prefix to the file names - not sure this is useful */
		
#ifdef WRITE_QPOINTS
		sprintf(pvdfilename,"%s/timeseries_qpoints.pvd",ctx->outputpath);
		if (beenhere==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		sprintf(vtkfilename, "%s_qpoints.pvtu",prefix);
		ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
		
		sprintf(name,"%s_qpoints",prefix);
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
#endif
		
#ifdef WRITE_FACEQPOINTS
		/*		
		 if (beenhere==0) {
		 ierr = ParaviewPVDOpen("faceqpoints_timeseries.pvd");CHKERRQ(ierr);
		 }
		 sprintf(vtkfilename, "%s_faceqpoints.pvtu",prefix);
		 ierr = ParaviewPVDAppend("faceqpoints_timeseries.pvd",ctx->time, vtkfilename, ctx->outputpath);CHKERRQ(ierr);
		 */ 
		sprintf(name,"%s_faceqpoints",prefix);
		ierr = pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
#endif
        
#ifdef WRITE_VP
		sprintf(pvdfilename,"%s/timeseries_vp.pvd",ctx->outputpath);
		if (beenhere==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		sprintf(vtkfilename, "%s_vp.pvts",prefix);
		ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
		
		sprintf(name,"%s_vp",prefix);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
#endif
        
#ifdef WRITE_INT_FIELDS
		sprintf(pvdfilename,"%s/timeseries_int_fields.pvd",ctx->outputpath);
		if (beenhere==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		sprintf(vtkfilename, "%s_int_fields.pvts",prefix);
		ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
		
		sprintf(name,"%s_int_fields",prefix);
		ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
#endif
		
#ifdef WRITE_MPOINTS
		sprintf(pvdfilename,"%s/timeseries_mpoints_stk.pvd",ctx->outputpath);
		if (beenhere==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		sprintf(vtkfilename, "%s_mpoints_stk.pvtu",prefix);
		ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
		
		sprintf(name,"%s_mpoints_stk",prefix);
		
	    ierr = GENE_SwarmOutputParaView_MPntP(ctx->db,ctx->outputpath,name);CHKERRQ(ierr);
		
#endif
		
	}
	
	
	if (use_dimensional_units) {
		ierr = pTatin_MaterialPointUnScaleProperties(ctx->db,&ctx->units);CHKERRQ(ierr);
	}
	
	beenhere = 1;
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_Schmalholz2011"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_Schmalholz2011(pTatinCtx ctx)
{
	PetscErrorCode         ierr;
	
	pTatinUnits *units;	
	
	PetscFunctionBegin;
	units   = &ctx->units;  
	UnitsApplyInverseScaling(units->si_time,ctx->dt_min,&ctx->dt_min);
	UnitsApplyInverseScaling(units->si_time,ctx->dt,&ctx->dt);
	UnitsApplyInverseScaling(units->si_time,ctx->dt_adv,&ctx->dt_adv);
	UnitsApplyInverseScaling(units->si_time,ctx->dt_max,&ctx->dt_max);
	UnitsApplyInverseScaling(units->si_time,ctx->time_max,&ctx->time_max);
	UnitsApplyInverseScaling(units->si_length,ctx->max_disp_y,&ctx->max_disp_y);
	
	
	ierr = pTatin2d_ModelSetMaterialProperty_Schmalholz2011(ctx); CHKERRQ(ierr);
	ierr = pTatin2d_ModelSetIndexOnMarker_Schmalholz2011(ctx);CHKERRQ(ierr);
	ierr = pTatin2d_ModelSetInitialStokesVariableOnMarker_GENE(ctx); CHKERRQ(ierr);
   // ierr = pTatin2d_ModelSetInitialPlsVariableOnMarker_GENE(ctx); CHKERRQ(ierr);
	
	
	PetscFunctionReturn(0);
}

