/*
 *  stokes_viscous.h
 *  
 *
 *
 */

#ifndef __pTatin_stokes_viscous_h__
#define __pTatin_stokes_viscous_h__

PetscErrorCode pTatin_TkUpdateRheologyQuadratureStokes_Viscous(void);
PetscErrorCode EvaluateRheologyNonlinearitiesQuadratureStokes_Viscous(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[]);
PetscErrorCode EvaluateRheologyNonlinearitiesMarkers_Viscous(pTatinCtx user,DM dau,PetscScalar u[],DM dap,PetscScalar p[]);

#endif
