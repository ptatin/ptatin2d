
#include <petsc.h>
#include <petscvec.h>
#include <petscdm.h>

#include "dmda_compare.h"


#define __DMDACompareStructures(var,v1,v2)PetscSynchronizedPrintf( comm, "[%d]: DMDA->%s are different { %d , %d } \n", rank, (var), (int)((v1)),(int)((v2)) )

#undef __FUNCT__
#define __FUNCT__ "DMDACompareStructures"
PetscErrorCode DMDACompareStructures(DM da1,DM da2,PetscBool *flg)
{
	PetscInt si1,sj1,sk1 , si2,sj2,sk2;
	PetscInt mx1,my1,mz1 , mx2,my2,mz2;
	PetscInt M1,N1,P1 , M2,N2,P2;
	PetscInt cx1,cy1,cz1 , cx2,cy2,cz2;
	PetscInt dim1 , dim2;
	PetscInt sw1 , sw2;
	DMDABoundaryType wrap1X,wrap1Y,wrap1Z, wrap2X,wrap2Y,wrap2Z;
	DMDAStencilType st1 , st2;
	MPI_Comm comm;
	PetscMPIInt rank;
	PetscErrorCode ierr;
	
	
	PetscFunctionBegin;
	*flg = PETSC_TRUE;

	ierr = DMDAGetInfo( da1, &dim1, &M1,&N1,&P1, &cx1,&cy1,&cz1, 0, &sw1, &wrap1X,&wrap1Y,&wrap1Z, &st1 );CHKERRQ(ierr);
	ierr = DMDAGetInfo( da2, &dim2, &M2,&N2,&P2, &cx2,&cy2,&cz2, 0, &sw2, &wrap2X,&wrap2Y,&wrap2Z, &st2 );CHKERRQ(ierr);
	
	ierr = DMDAGetCorners( da1, &si1,&sj1,&sk1 , &mx1,&my1,&mz1 );CHKERRQ(ierr);
	ierr = DMDAGetCorners( da2, &si2,&sj2,&sk2 , &mx2,&my2,&mz2 );CHKERRQ(ierr);
	
	
	PetscObjectGetComm( (PetscObject)da1, &comm );
	MPI_Comm_rank( comm, &rank );
	
	if( dim1 != dim2 ) {	PetscSynchronizedPrintf( comm, "[%d]: DA->dim are different { %d , %d } \n", dim1,dim2 );		*flg = PETSC_FALSE;	}
	
	if( M1 != M2 ) {	__DMDACompareStructures( "M",M1,M2 );		*flg = PETSC_FALSE;	}
	if( N1 != N2 ) {	__DMDACompareStructures( "N",N1,N2 );		*flg = PETSC_FALSE;	}
	if( P1 != P2 ) {	__DMDACompareStructures( "P",P1,P2 );		*flg = PETSC_FALSE;	}
	
	if( cx1 != cx2 ) {	__DMDACompareStructures( "px",cx1,cx2 );		*flg = PETSC_FALSE;	}
	if( cy1 != cy2 ) {	__DMDACompareStructures( "py",cy1,cy2 );		*flg = PETSC_FALSE;	}
	if( cz1 != cz2 ) {	__DMDACompareStructures( "pz",cz1,cz2 );		*flg = PETSC_FALSE;	}
	
	if( sw1 != sw2 ) {		__DMDACompareStructures( "stencil_width",sw1,sw2 );		*flg = PETSC_FALSE;	}

	if( wrap1X != wrap2X ) {  __DMDACompareStructures( "wrapX",wrap1X,wrap2X );		*flg = PETSC_FALSE;	}
        if( wrap1Y != wrap2Y ) {  __DMDACompareStructures( "wrapY",wrap1Y,wrap2Y );             *flg = PETSC_FALSE;     }
        if( wrap1Z != wrap2Z ) {  __DMDACompareStructures( "wrapZ",wrap1Z,wrap2Z );             *flg = PETSC_FALSE;     }

	
	if( si1 != si2 ) {	__DMDACompareStructures( "si",si1,si2 );		*flg = PETSC_FALSE;	}
	if( sj1 != sj2 ) {	__DMDACompareStructures( "sj",sj1,sj2 );		*flg = PETSC_FALSE;	}
	if( sk1 != sk2 ) {	__DMDACompareStructures( "sk",sk1,sk2 );		*flg = PETSC_FALSE;	}
	
	if( mx1 != mx2 ) {	__DMDACompareStructures( "mx",mx1,mx2 );		*flg = PETSC_FALSE;	}
	if( my1 != my2 ) {	__DMDACompareStructures( "my",my1,my2 );		*flg = PETSC_FALSE;	}
	if( mz1 != mz2 ) {	__DMDACompareStructures( "mz",mz1,mz2 );		*flg = PETSC_FALSE;	}
	
	
	if( *flg == PETSC_FALSE ) {
		PetscSynchronizedFlush( comm );
	}
	
	PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "DMDA_CheckNodeIndex2d"
PetscErrorCode DMDA_CheckNodeIndex2d(DM da,PetscBool ghosted,PetscInt i,PetscInt j)
{
	PetscInt si,sj,nx,ny;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	ierr = DMDAGetCorners(da,&si,&sj,0,&nx,&ny,0);CHKERRQ(ierr);
	if(ghosted==PETSC_TRUE) {
		ierr = DMDAGetGhostCorners(da,&si,&sj,0,&nx,&ny,0);CHKERRQ(ierr);
	}
	if( i<si ) SETERRQ2( ((PetscObject)da)->comm, PETSC_ERR_USER, "i=%D < start_index_i(%D)", i,si );
	if( j<sj ) SETERRQ2( ((PetscObject)da)->comm, PETSC_ERR_USER, "j=%D < start_index_j(%D)", j,sj );
	
	if( i>=(si+nx) ) SETERRQ2( ((PetscObject)da)->comm, PETSC_ERR_USER, "i=%D >= end_index_i(%D)", i,si+nx );
	if( j>=(sj+ny) ) SETERRQ2( ((PetscObject)da)->comm, PETSC_ERR_USER, "j=%D >= end_index_j(%D)", j,sj+ny );
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMDA_CheckNodeIndex3d"
PetscErrorCode DMDA_CheckNodeIndex3d(DM da,PetscBool ghosted,PetscInt i,PetscInt j,PetscInt k )
{
	PetscErrorCode ierr;
	PetscInt si,sj,sk,nx,ny,nz;
	
	PetscFunctionBegin;
	
	ierr = DMDAGetCorners(da,&si,&sj,&sk,&nx,&ny,&nz);CHKERRQ(ierr);
	if(ghosted==PETSC_TRUE) {
		ierr = DMDAGetGhostCorners(da,&si,&sj,0,&nx,&ny,0);CHKERRQ(ierr);
	}
	if( i<si ) SETERRQ2( ((PetscObject)da)->comm, PETSC_ERR_USER, "i=%D < start_index_i(%D)", i,si );
	if( j<sj ) SETERRQ2( ((PetscObject)da)->comm, PETSC_ERR_USER, "j=%D < start_index_j(%D)", j,sj );
	if( k<sk ) SETERRQ2( ((PetscObject)da)->comm, PETSC_ERR_USER, "k=%D < start_index_k(%D)", k,sk );
	
	if( i>=(si+nx) ) SETERRQ2( ((PetscObject)da)->comm, PETSC_ERR_USER, "i=%D >= end_index_i(%D)", i,si+nx );
	if( j>=(sj+ny) ) SETERRQ2( ((PetscObject)da)->comm, PETSC_ERR_USER, "j=%D >= end_index_j(%D)", j,sj+ny );
	if( k>=(sk+nz) ) SETERRQ2( ((PetscObject)da)->comm, PETSC_ERR_USER, "k=%D >= end_index_k(%D)", k,sk+nz );
	
	PetscFunctionReturn(0);
}


PetscErrorCode DMGetGlobalVectorSize(DM dm,PetscInt *_G,PetscInt *_g);

#undef __FUNCT__
#define __FUNCT__ "DMGetGlobalVectorSize_DMDA"
PetscErrorCode DMGetGlobalVectorSize_DMDA(DM dm,PetscInt *_G,PetscInt *_g)
{
	PetscErrorCode ierr;
	PetscInt ni,nj,nk,dof,dim;
	PetscInt Ni,Nj,Nk;
	PetscInt G,g;
	
	PetscFunctionBegin;

	ierr = DMDAGetInfo(dm,&dim,&Ni,&Nj,&Nk,PETSC_NULL,PETSC_NULL,PETSC_NULL,&dof,PETSC_NULL,PETSC_NULL,PETSC_NULL,PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
	ierr = DMDAGetCorners(dm,PETSC_NULL,PETSC_NULL,PETSC_NULL,&ni,&nj,&nk);CHKERRQ(ierr);
	
	switch (dim) {
		case 1:
			G = Ni;
			g = ni;
			break;
		case 2:
			G = Ni * Nj;
			g = ni * nj;
			break;
		case 3:
			G = Ni * Nj * Nk;
			g = ni * nj * nk;
			break;
		default:
			SETERRQ(((PetscObject)dm)->comm,PETSC_ERR_SUP,"DM(dim) must be 1,2 or 3");
			break;
	}
	G = G * dof;
	g = g * dof;
	
	*_G = *_G + G;
	*_g = *_g + g;
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMGetGlobalVectorSize_DMCOMPOSITE"
PetscErrorCode DMGetGlobalVectorSize_DMCOMPOSITE(DM dm,PetscInt *_G,PetscInt *_g)
{
	PetscErrorCode ierr;
	DM da[5];
	PetscInt nDM,s;
	
	PetscFunctionBegin;
	
	ierr = DMCompositeGetNumberDM(dm,&nDM);CHKERRQ(ierr);
	switch (nDM) {
			
		case 1:
			ierr = DMCompositeGetEntries(dm,&da[0]);CHKERRQ(ierr);
			break;
		case 2:
			ierr = DMCompositeGetEntries(dm,&da[0],&da[1]);CHKERRQ(ierr);
			
			break;
		case 3:
			ierr = DMCompositeGetEntries(dm,&da[0],&da[1],&da[2]);CHKERRQ(ierr);
			
			break;
		case 4:
			ierr = DMCompositeGetEntries(dm,&da[0],&da[1],&da[2],&da[3]);CHKERRQ(ierr);
			
			break;
		case 5:
			ierr = DMCompositeGetEntries(dm,&da[0],&da[1],&da[2],&da[3],&da[4]);CHKERRQ(ierr);
			
			break;
		default:
			SETERRQ(((PetscObject)dm)->comm,PETSC_ERR_SUP,"Max sub DM's in DMComposite is 5");
			break;
	}
	
	for (s=0; s<nDM; s++) {
		ierr = DMGetGlobalVectorSize(da[s],_G,_g);CHKERRQ(ierr);
	}
	
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMGetGlobalVectorSize"
PetscErrorCode DMGetGlobalVectorSize(DM dm,PetscInt *_G,PetscInt *_g)
{
	PetscErrorCode ierr;
	DM da[5];
	PetscInt nDM;
	const DMType dtype;
	PetscBool sameDA,sameCOMP;
	
	PetscFunctionBegin;
	

	ierr = DMGetType(dm,&dtype);CHKERRQ(ierr);
	sameDA = PETSC_FALSE;
	ierr = PetscTypeCompare((PetscObject)dm,"da",&sameDA);CHKERRQ(ierr);
	if (sameDA) {
		ierr = DMGetGlobalVectorSize_DMDA(dm,_G,_g);CHKERRQ(ierr);
	}
	
	sameCOMP = PETSC_FALSE;
	ierr = PetscTypeCompare((PetscObject)dm,"composite",&sameCOMP);CHKERRQ(ierr);
	if (sameCOMP) {
		ierr = DMGetGlobalVectorSize_DMCOMPOSITE(dm,_G,_g);CHKERRQ(ierr);
	}
	
	if ( (!sameDA) && (!sameCOMP) ) {
		SETERRQ(((PetscObject)dm)->comm,PETSC_ERR_SUP,"Unknown DM - no method to verify global vector");
	}
	
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMVerifyGlobalVector"
PetscErrorCode DMVerifyGlobalVector(DM dm,Vec X,PetscBool *isvalid)
{
	PetscErrorCode ierr;
	PetscInt M,m;
	PetscInt G,g;
	
	PetscFunctionBegin;
	
	G = 0;
	g = 0;
	ierr = DMGetGlobalVectorSize(dm,&G,&g);CHKERRQ(ierr);
	
	ierr = VecGetSize(X,&M);CHKERRQ(ierr);
	ierr = VecGetLocalSize(X,&m);CHKERRQ(ierr);
	
	*isvalid = PETSC_FALSE;
	if ( (M==G) && (m==g) ) {
		*isvalid = PETSC_TRUE;
	}
	
	PetscFunctionReturn(0);
}

/* 
 Check dm2 is contained with dm1 
 Checks the global indices by default.
 Checks ghosted indices if check_ghosted = PETSC_TRUE
 */
#undef __FUNCT__
#define __FUNCT__ "DMVerifyOverlappingIndices"
PetscErrorCode DMVerifyOverlappingIndices(DM dm1,DM dm2,PetscBool check_ghosted,PetscBool *isvalid)
{
	PetscErrorCode ierr;
	PetscInt si1,sj1,sk1,nx1,ny1,nz1;
	PetscInt si2,sj2,sk2,nx2,ny2,nz2;
	
	PetscFunctionBegin;
	
	*isvalid = PETSC_TRUE;

	if (!check_ghosted) {
		ierr = DMDAGetCorners(dm1,&si1,&sj1,&sk1 , &nx1,&ny1,&nz1);CHKERRQ(ierr);
		ierr = DMDAGetCorners(dm2,&si2,&sj2,&sk2 , &nx2,&ny2,&nz2);CHKERRQ(ierr);
	} else {
		ierr = DMDAGetGhostCorners(dm1,&si1,&sj1,&sk1 , &nx1,&ny1,&nz1);CHKERRQ(ierr);
		ierr = DMDAGetGhostCorners(dm2,&si2,&sj2,&sk2 , &nx2,&ny2,&nz2);CHKERRQ(ierr);
	}

	if ( (si1>si2) & (si1+nx1<si2+nx2) ) {
		*isvalid = PETSC_FALSE;
	}
	if ( (sj1>sj2) & (sj1+ny1<sj2+ny2) ) {
		*isvalid = PETSC_FALSE;
	}
	if ( (sk1>sk2) & (sk1+nz1<sk2+nz2) ) {
		*isvalid = PETSC_FALSE;
	}
	
	
	
	PetscFunctionReturn(0);
}


