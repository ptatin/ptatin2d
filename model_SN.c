/*
 
 Model Description:
 Example reading geometry from file.
 This example uses "sticky-air". We delibrately define a mesh which is larger in the vertical
 direction than the phase map, just to demonstrate how to deal with the case when you query
 the phase map for information outside of the domain defined by the phase map.
 
 
 Input / command line parameters:
 
 */

#include "petsc.h"
#include "petscvec.h"
#include "petscmat.h"
#include "petscksp.h"
#include "petscdm.h"
#include "private/daimpl.h" 

#include "pTatin2d.h"

/* ================= SN: Sierra Nevada example reading geometry from phase map ================= */

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelOutput_SN"
PetscErrorCode pTatin2d_ModelOutput_SN(pTatinCtx ctx,Vec X,const char prefix[])
{
	char *pvdfilename;
	PetscErrorCode ierr;

	
	if (prefix == PETSC_NULL) {
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,"qpoints");CHKERRQ(ierr);
		ierr = pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(ctx,ctx->outputpath,"faceqpoints");CHKERRQ(ierr);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,"vp");CHKERRQ(ierr);
		ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,"int_fields");CHKERRQ(ierr);
		ierr = SwarmOutputParaView_MPntPStokes(ctx->db,ctx->outputpath,"mpoints_stk");CHKERRQ(ierr);
	} else { /* exanple of adding a different prefix to the file names - not sure this is useful */
		char *name;
		
		///////////////////////////////////////////////////////////////////////////
		// PVD
		asprintf(&pvdfilename,"%s/timeseries_qpoints.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;
			
			asprintf(&vtkfilename, "%s_qpoints.pvtu",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			free(vtkfilename);
		}
		free(pvdfilename);
		// PVTU
		asprintf(&name,"%s_qpoints",prefix);
		ierr = pTatinOutputParaViewQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);

		///////////////////////////////////////////////////////////////////////////		
		// PVD
/*		
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen("faceqpoints_timeseries.pvd");CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;
			
			asprintf(&vtkfilename, "%s_faceqpoints.pvtu",prefix);
			ierr = ParaviewPVDAppend("faceqpoints_timeseries.pvd",ctx->time, vtkfilename, ctx->outputpath);CHKERRQ(ierr);
			free(vtkfilename);
		}
*/ 
		// PVTU
		asprintf(&name,"%s_faceqpoints",prefix);
		ierr = pTatinOutputParaViewSurfaceQuadratureStokesPointsRaw(ctx,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);

		///////////////////////////////////////////////////////////////////////////
		// PVD
		asprintf(&pvdfilename,"%s/timeseries_vp.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;
			
			asprintf(&vtkfilename, "%s_vp.pvts",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			free(vtkfilename);
		}
		free(pvdfilename);
		// PVTS
		asprintf(&name,"%s_vp",prefix);
		ierr = pTatinOutputParaViewMeshVelocityPressure(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);

		///////////////////////////////////////////////////////////////////////////
		// PVD
		asprintf(&pvdfilename,"%s/timeseries_int_fields.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;
			
			asprintf(&vtkfilename, "%s_int_fields.pvts",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			free(vtkfilename);
		}
		free(pvdfilename);
		// PVTS
		asprintf(&name,"%s_int_fields",prefix);
		ierr = pTatinParaViewOutputQuadratureStokesPointsIntegratedFields(ctx,X,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);

		///////////////////////////////////////////////////////////////////////////
		// PVD
		asprintf(&pvdfilename,"%s/timeseries_mpoints_stk.pvd",ctx->outputpath);
		if (ctx->step==0) {
			ierr = ParaviewPVDOpen(pvdfilename);CHKERRQ(ierr);
		}
		else {
			char *vtkfilename;
			char *filename;
			
			asprintf(&vtkfilename, "%s_mpoints_stk.pvtu",prefix);
			ierr = ParaviewPVDAppend(pvdfilename,ctx->time, vtkfilename, "");CHKERRQ(ierr);
			free(vtkfilename);
		}
		free(pvdfilename);
		// PVTU
		asprintf(&name,"%s_mpoints_stk",prefix);
		ierr = SwarmOutputParaView_MPntPStokes(ctx->db,ctx->outputpath,name);CHKERRQ(ierr);
		free(name);
		//////////////
	}
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "deform_upper_surface"
PetscErrorCode deform_upper_surface(DM da)
{
	DM         cda;
	Vec        coordinates,gcoords;
	DMDACoor2d **LA_gcoords;
	PetscInt   i,j,si,sj,ni,nj,M,N;
	
	PetscErrorCode ierr;
	PetscFunctionBegin;
	
	ierr = DMDAGetCoordinateDA(da,&cda);CHKERRQ(ierr);
	ierr = DMDAGetGhostedCoordinates(da,&gcoords);CHKERRQ(ierr);
	ierr = DMDAVecGetArray(cda,gcoords,&LA_gcoords);CHKERRQ(ierr);
	ierr = DMDAGetCorners(cda,&si,&sj,0,&ni,&nj,0);CHKERRQ(ierr);
	ierr = DMDAGetInfo(da,0,&M,&N,0, 0,0,0, 0,0,0, 0,0,0);CHKERRQ(ierr);

	j = sj+nj-1;
	if (j == N-1) {
		for (i=si; i<si+ni; i++) {
			PetscScalar xn,yn;
			xn = LA_gcoords[j][i].x;
			yn = LA_gcoords[j][i].y;
			
			//LA_gcoords[j][i].y = 0.6 * sin(xn) + 5.5;
			LA_gcoords[j][i].y = -0.01 * cos(2.0 * M_PI * xn * 0.5/10.0) + 3.8;
		}
	}
	
	ierr = DMDAVecRestoreArray(cda,gcoords,&LA_gcoords);CHKERRQ(ierr);
	
	ierr = DMDAGetCoordinates(da,&coordinates);CHKERRQ(ierr);
	ierr = DMLocalToGlobalBegin(cda,gcoords,INSERT_VALUES,coordinates);CHKERRQ(ierr);
	ierr = DMLocalToGlobalEnd  (cda,gcoords,INSERT_VALUES,coordinates);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMeshGeometry_SN"
PetscErrorCode pTatin2d_ModelApplyInitialMeshGeometry_SN(pTatinCtx ctx)
{
	PhaseMap phasemap;
	PetscInt M,N;
	PetscErrorCode ierr;
	PetscFunctionBegin;
	
	/* check if ctx->data is NULL */
	/*
	if (ctx->data != PETSC_NULL) {
		SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user context would be empty");
	}
	*/
	PhaseMapLoadFromFile("./inputdata/SN_model_geometry.pmap",&phasemap);
	PhaseMapViewGnuplot("./inputdata/SN_phase_map.gp",phasemap);
	ierr = pTatinCtxAttachPhaseMap(ctx,phasemap);CHKERRQ(ierr);
	
	PetscPrintf(PETSC_COMM_WORLD,"ApplyInitialMeshGeometry_SN: Domain = [ %1.4e , %1.4e ] x [ %1.4e , %1.4e ] \n", phasemap->x0,phasemap->x1, phasemap->y0,phasemap->y1+0.5 );
	ierr = DMDASetUniformCoordinates(ctx->dav,phasemap->x0,phasemap->x1, phasemap->y0,phasemap->y1+0.5, 0.0,0.0);CHKERRQ(ierr);

	//
	// remeshing test // 
	ierr = deform_upper_surface(ctx->dav);CHKERRQ(ierr);
	ierr = DMDAGetInfo(ctx->dav,0,&M,&N,0,0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);
	ierr = DMDARemeshSetUniformCoordinatesBetweenJLayers2d(ctx->dav,0,N);CHKERRQ(ierr);
	//
	 
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyInitialMaterialGeometry_SN"
PetscErrorCode pTatin2d_ModelApplyInitialMaterialGeometry_SN(pTatinCtx ctx)
{
	PhaseMap               phasemap;
	PetscInt               e,p,n_mp_points;
	DataBucket             db;
	DataField              PField_std,PField_stokes;
	PetscScalar            eta_phase[6],rho_phase[6];
	PetscScalar            eta,rho,eta_scale;
	int                    phase_init, phase, phase_index, is_valid;
	PetscErrorCode         ierr;
	
	PetscFunctionBegin;
	
	ctx->rheology_constants.rheology_type = RHEOLOGY_VISCOUS;
	
	/* check if ctx->data is NULL */
/*
	if (ctx->data == PETSC_NULL) {
		SETERRQ(PETSC_COMM_SELF,PETSC_ERR_SUP,"Expected user context would be pointing to a phasemap");
	}
	phasemap = (PhaseMap)ctx->data;
*/
	/* 
	 Two ways to access model data
	 1. via the specific data attach/get function
	 2. via the generic ptatin ctx model data method where you have to specify a textual name to retrieve the object
	*/
	phasemap = PETSC_NULL;
	ierr = pTatinCtxGetPhaseMap(ctx,&phasemap);CHKERRQ(ierr);
	phasemap = PETSC_NULL;
	ierr = pTatinCtxGetModelData(ctx,"phasemap",(void**)&phasemap);CHKERRQ(ierr);
	
	eta_scale = 1.0e22;
	eta_phase[0] = 1.0e21 / eta_scale; // [bump]
	eta_phase[1] = 1.0e21 / eta_scale; // [mantle]
	eta_phase[2] = 1.0e22 / eta_scale; // [upper crust]
	eta_phase[3] = 1.0e19 / eta_scale; // [lower crust - right]
	eta_phase[4] = 1.0e22 / eta_scale; // [lower crust - left]
	eta_phase[5] = 1.0e18 / eta_scale; // [air]

	rho_phase[0] = 3.4;
	rho_phase[1] = 3.3; 
	rho_phase[2] = 2.7;
	rho_phase[3] = 2.7;
	rho_phase[4] = 2.9;
	rho_phase[5] = 0.0;
	
	
	/* define properties on material points */
	db = ctx->db;
	DataBucketGetDataFieldByName(db,MPntStd_classname,&PField_std);
	DataFieldGetAccess(PField_std);
	DataFieldVerifyAccess(PField_std,sizeof(MPntStd));
	
	DataBucketGetDataFieldByName(db,MPntPStokes_classname,&PField_stokes);
	DataFieldGetAccess(PField_stokes);
	DataFieldVerifyAccess(PField_stokes,sizeof(MPntPStokes));
	
	
	DataBucketGetSizes(db,&n_mp_points,0,0);
	
	for (p=0; p<n_mp_points; p++) {
		MPntStd     *material_point;
		MPntPStokes *mpprop_stokes;
		double  *position;
		
		DataFieldAccessPoint(PField_std,p,(void**)&material_point);
		DataFieldAccessPoint(PField_stokes,p,(void**)&mpprop_stokes);
		
		/* Access using the getter function provided for you (recommeneded for beginner user) */
		MPntStdGetField_global_coord(material_point,&position);
		// OR
		/* Access directly into material point struct (not recommeneded for beginner user) */
		/* Direct access only recommended for efficiency */
		/* position = material_point->coor; */
		
		MPntStdGetField_phase_index(material_point,&phase_init);
		
		phase = 5; /* air, default */

		PhaseMapGetPhaseIndex(phasemap,position,&phase_index);
		PhaseMapCheckValidity(phasemap,phase_index,&is_valid);
		if (is_valid==1) { /* point located in the phase map */
			phase = phase_index;
		}

		eta = eta_phase[phase];
		rho = rho_phase[phase];
		
		/* user the setters provided for you */
		MPntStdSetField_phase_index(material_point,phase);
		
		MPntPStokesSetField_eta_effective(mpprop_stokes,eta);
		MPntPStokesSetField_density(mpprop_stokes,rho);
	}
	
	DataFieldRestoreAccess(PField_std);
	DataFieldRestoreAccess(PField_stokes);
	
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelApplyBoundaryCondition_SN"
PetscErrorCode pTatin2d_ModelApplyBoundaryCondition_SN(pTatinCtx ctx)
{
	BCList         ubclist;
	PetscScalar    bcval;
	DM             dav;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	
	ubclist = ctx->u_bclist;
	dav     = ctx->dav;
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);	
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_IMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMIN_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);

	/* free surface */
//	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
//	bcval = 0.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,1,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	
//	bcval = 1.0; ierr = DMDABCListTraverse(ubclist,dav,DMDABCList_JMAX_LOC,0,BCListEvaluator_constant,(void*)&bcval);CHKERRQ(ierr);
	
	PetscFunctionReturn(0);
}

#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelUpdateMeshGeometry_SN"
PetscErrorCode pTatin2d_ModelUpdateMeshGeometry_SN(pTatinCtx ctx,Vec X)
{
	PetscReal step;
	Vec velocity,pressure;
	DM dav,dap;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	step = ctx->dt;

	ierr = DMCompositeGetEntries(ctx->pack,&dav,&dap);CHKERRQ(ierr);
	ierr = DMCompositeGetAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);

//	ierr = UpdateMeshGeometry_FullLagSurface(dav,velocity,step);CHKERRQ(ierr);
//	ierr = UpdateMeshGeometry_VerticalDisplacement(dav,velocity,step);CHKERRQ(ierr);
	ierr = UpdateMeshGeometry_FullLagSurfaceJMAX_RemeshInteriorJMIN_JMAX(dav,velocity,step);CHKERRQ(ierr);
	
	ierr = DMCompositeRestoreAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);

	PetscFunctionReturn(0);
}


#undef __FUNCT__  
#define __FUNCT__ "pTatin2d_ModelUpdateMeshGeometry_SN_local_global"
PetscErrorCode pTatin2d_ModelUpdateMeshGeometry_SN_local_global(pTatinCtx ctx,Vec X)
{
	Vec velocity,pressure,coordinates,vel_advect_mesh;
	PetscInt M,N;
	PetscReal step;
	PetscErrorCode ierr;
	
	PetscFunctionBegin;
	step = ctx->dt;

	ierr = DMCompositeGetAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
	
	ierr = DMGetGlobalVector(ctx->dav,&vel_advect_mesh);CHKERRQ(ierr);
	
	ierr = VecCopy(velocity,vel_advect_mesh);CHKERRQ(ierr);
	ierr = VecStrideScale(vel_advect_mesh,0,0.0);CHKERRQ(ierr); /* zero out the x-componenet */
	
	ierr = DMDAGetCoordinates(ctx->dav,&coordinates);CHKERRQ(ierr);
	ierr = VecAXPY(coordinates,step,vel_advect_mesh);CHKERRQ(ierr); /* x = x + dt.vel_advect_mesh */

	ierr = DMRestoreGlobalVector(ctx->dav,&vel_advect_mesh);CHKERRQ(ierr);

	ierr = DMCompositeRestoreAccess(ctx->pack,X,&velocity,&pressure);CHKERRQ(ierr);
	//
	//
	ierr = DMGetLocalVector(ctx->dav,&vel_advect_mesh);CHKERRQ(ierr);
	ierr = DMGlobalToLocalBegin(ctx->dav,velocity,INSERT_VALUES,vel_advect_mesh);CHKERRQ(ierr);
	ierr = DMGlobalToLocalEnd(  ctx->dav,velocity,INSERT_VALUES,vel_advect_mesh);CHKERRQ(ierr);

	ierr = VecStrideScale(vel_advect_mesh,0,0.0);CHKERRQ(ierr); /* zero out the x-componenet */

	ierr = DMDAGetGhostedCoordinates(ctx->dav,&coordinates);CHKERRQ(ierr);
	ierr = VecAXPY(coordinates,step,vel_advect_mesh);CHKERRQ(ierr); /* x = x + dt.vel_advect_mesh */
	
	ierr = DMRestoreLocalVector(ctx->dav,&vel_advect_mesh);CHKERRQ(ierr);
	
	
	PetscFunctionReturn(0);
}



