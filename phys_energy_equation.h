/*

Design choices:
 + Store the velocity vector, u_minus_V
 This is the vector which will be used by the adv-diff solver and should
 contain the difference between the fluid velocity u, and corrections from
 mesh moments (the arbitrariness) V.
 + The other reason to store V is that it is likely to be represented in
 function spaces. I.e., on the stokes solver side its a Q2 space, on the SUPG
 side it will be represented as a Q1 space.
 
 
*/
#ifndef __phys_energy_equation_h__
#define __phys_energy_equation_h__

typedef struct _p_PhysCompEnergyCtx      *PhysCompEnergyCtx;
typedef struct _p_QuadratureVolumeEnergy *QuadratureVolumeEnergy;
typedef struct _p_CoefficientsEnergyEq   CoefficientsEnergyEq;

struct _p_PhysCompEnergyCtx {
  DM                      daT;
	Vec                     T,u_minus_V;
	BCList                  T_bclist;
	QuadratureVolumeEnergy  Q;
	PetscInt                mx,my; /* global mesh size */
	/* SUPG DATA */
	Vec                     Told; /* old temperature solution vector */
	Vec                     Xold; /* old coordinate vector */
};



struct _p_CoefficientsEnergyEq {
  PetscScalar diffusivity;
  PetscScalar heat_source;
};

struct _p_QuadratureVolumeEnergy {
	/* for each element */
	PetscInt    ngp;
	PetscScalar *xi;
	PetscScalar *weight;
	/* basis function evaluations */
	PetscInt             u_basis;
	PetscScalar          **U_Ni; /* U_Ni[p][...] */
	PetscScalar          ***U_GNi;
	PetscInt             ncells;
	CoefficientsEnergyEq *cellproperties;
};	


PetscErrorCode QuadratureVolumeEnergyGetCell(QuadratureVolumeEnergy Q,PetscInt cidx,CoefficientsEnergyEq **points);
PetscErrorCode QuadratureVolumeEnergyCreate(QuadratureStokesRule rule,PetscInt ncells,QuadratureVolumeEnergy *Q);
PetscErrorCode PhysComp_EnergyEquationCreate1(PhysCompEnergyCtx *p);
PetscErrorCode PhysComp_EnergyEquationCreate2(DM davq2,PhysCompEnergyCtx *p);
PetscErrorCode PhysComp_EnergyEquationDestroy(PhysCompEnergyCtx *p);

PetscErrorCode PhysCompView_EnergyEquation(PhysCompEnergyCtx phys,Vec X,const char fname[]);

PetscErrorCode pTatinLoadPhysics_EnergyEquation(pTatinCtx ctx);

PetscErrorCode DMDAProjectVelocityQ2toQ1_2d(DM daq2,Vec vq2,DM daq1,Vec vq1);

PetscErrorCode SUPGFormJacobian_qp(PetscReal time,Vec X,PetscReal dt,Mat *A,Mat *B,MatStructure *mstr,void *ctx);
PetscErrorCode SUPGFormFunction_qp(PetscReal time,Vec X,PetscReal dt,Vec F,void *ctx);

PetscErrorCode DASUPG2dComputeTimestep_qp(PhysCompEnergyCtx data,PetscScalar Cr,Vec V,PetscScalar *DT);

/* vis */
/* nodal temperature */
PetscErrorCode pTatinOutputQ1Mesh2dNodalFieldVTS(DM da,const PetscInt ncomponenets,Vec X,const char filename[],const char fieldname[]);
PetscErrorCode pTatinOutputParaViewMeshTemperature(pTatinCtx user,Vec X,const char path[],const char prefix[]);

/* cell props */
PetscErrorCode pTatinParaViewOutputQuadratureThermalAveragedFields(pTatinCtx user,const char path[],const char prefix[]);

#endif

